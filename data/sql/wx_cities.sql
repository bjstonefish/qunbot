set names utf8;
CREATE TABLE IF NOT EXISTS `miao_wx_cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coverimg` varchar(200) DEFAULT '',
  `city_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `wx_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'coverimg', '1', '封面图片', 'string', 'WxCity', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-25 11:57:45', '2015-01-25 11:57:45', NULL),
(NULL, 'id', '1', '编号', 'integer', 'WxCity', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-25 11:57:45', '2015-01-25 11:57:45', NULL),
(NULL, 'city_id', '1', '所属城市编号', 'integer', 'WxCity', NULL, '11', 6, '1', '1', 'City', 'id', 'name', 0, '1', '', '0', '', '', '', 'select', '', '1', '', NULL, '', '', '', 0, '2015-01-25 11:57:45', '2015-01-25 13:06:15', NULL),
(NULL, 'creator', '1', '编创建者', 'integer', 'WxCity', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-25 11:57:45', '2015-01-25 11:57:45', NULL),
(NULL, 'status', '1', '状态', 'integer', 'WxCity', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-25 11:57:45', '2015-01-25 11:57:45', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'WxCity', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-25 11:57:45', '2015-01-25 11:57:45', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'WxCity', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-25 11:57:45', '2015-01-25 11:57:45', NULL),
(NULL, 'wx_id', '1', 'wx编号', 'integer', 'WxCity', 'en_us', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-01-25 12:00:35', '2015-01-25 12:00:35', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'WxCity', 'Wx城市', '', 'default', '', 27, '2015-01-25 11:57:45', '2015-01-25 12:41:27', 'miao_wx_cities', '', '', '', '0', 2, 0, '0', '0', '');
