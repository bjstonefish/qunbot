set names utf8;
CREATE TABLE IF NOT EXISTS `miao_inspections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `customer_id` int(11) DEFAULT '0',
  `val1` varchar(20) DEFAULT '',
  `val2` varchar(20) DEFAULT '',
  `val3` varchar(20) DEFAULT '',
  `val4` varchar(20) DEFAULT '',
  `val5` varchar(20) DEFAULT '',
  `val6` varchar(20) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Inspection', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 16:51:33', '2017-02-09 16:51:33', NULL),
(NULL, 'name', '1', '检查类型名称', 'string', 'Inspection', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2017-02-09 16:51:33', '2017-02-09 17:49:52', NULL),
(NULL, 'cate_id', '1', '检查类型编号', 'integer', 'Inspection', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2017-02-09 16:51:33', '2017-02-09 17:49:27', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'Inspection', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 16:51:33', '2017-02-09 16:51:33', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Inspection', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 16:51:33', '2017-02-09 16:51:33', NULL),
(NULL, 'val1', '1', '检查记录1', 'string', 'Inspection', NULL, '20', 0, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 17:56:16', '2017-02-09 17:56:16', NULL),
(NULL, 'customer_id', '1', '客户编号', 'integer', 'Inspection', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-02-09 17:54:19', '2017-02-09 17:54:19', NULL),
(NULL, 'val2', '1', '检查记录2', 'string', 'Inspection', NULL, '20', 0, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 17:56:16', '2017-02-09 17:56:16', NULL),
(NULL, 'val3', '1', '检查记录3', 'string', 'Inspection', NULL, '20', 0, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 17:56:16', '2017-02-09 17:56:16', NULL),
(NULL, 'val4', '1', '检查记录4', 'string', 'Inspection', NULL, '20', 0, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 17:56:16', '2017-02-09 17:56:16', NULL),
(NULL, 'val5', '1', '检查记录5', 'string', 'Inspection', NULL, '20', 0, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 17:56:17', '2017-02-09 17:56:17', NULL),
(NULL, 'val6', '1', '检查记录6', 'string', 'Inspection', NULL, '20', 0, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 17:56:17', '2017-02-09 17:56:17', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Inspection', '身体检查记录', '', 'default', '', 0, '2017-02-09 16:51:33', '2017-02-10 10:15:52', 'miao_inspections', '', 'branch', '', '0', NULL, NULL, '0', '0', '');
