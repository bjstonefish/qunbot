set names utf8;
CREATE TABLE IF NOT EXISTS `miao_event_handles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `handle_name` varchar(20) DEFAULT NULL,
  `last_event_id` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'EventHandle', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-11-15 23:55:20', '2014-11-15 23:55:20', NULL),
(NULL, 'handle_name', '1', '处理器名称', 'string', 'EventHandle', NULL, '20', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-11-15 23:55:20', '2014-11-15 23:56:26', NULL),
(NULL, 'last_event_id', '1', '最后已处理事件编号', 'integer', 'EventHandle', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', '', '', '1', '', NULL, '', '', '', 0, '2014-11-15 23:55:20', '2014-11-15 23:57:37', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'EventHandle', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-11-15 23:55:20', '2014-11-15 23:55:20', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'EventHandle', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-11-15 23:55:20', '2014-11-15 23:55:20', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'EventHandle', '事件处理机', '', 'default', '', 27, '2014-11-15 23:55:20', '2014-11-15 23:55:20', 'miao_event_handles', '', '', '', '0', NULL, 0, '0', '0', '');
