set names utf8;
CREATE TABLE IF NOT EXISTS `miao_little_apps` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(300) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `published` tinyint(3) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `view_nums` int(11) DEFAULT '0',
  `qrcode` varchar(300) DEFAULT '',
  `summary` varchar(500) DEFAULT '',
  `content` text,
  `thirdid` varchar(32) DEFAULT '',
  `third_type` varchar(32) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'LittleApp', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-01-10 10:49:36', '2017-01-10 10:49:36', NULL),
(NULL, 'name', '1', '名称', 'string', 'LittleApp', NULL, '200', 5, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-01-10 10:49:36', '2017-01-10 10:49:36', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'LittleApp', NULL, '300', 5, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-01-10 10:49:36', '2017-01-10 10:49:36', NULL),
(NULL, 'cate_id', '1', '分类', 'integer', 'LittleApp', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-01-10 10:49:36', '2017-01-10 10:49:36', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'LittleApp', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-01-10 10:49:36', '2017-01-10 10:49:36', NULL),
(NULL, 'published', '1', '是否发布', 'integer', 'LittleApp', NULL, '11', 3, '1', '1', '', NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-01-10 10:49:36', '2017-01-10 10:49:36', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'LittleApp', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-01-10 10:49:36', '2017-01-10 10:49:36', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'LittleApp', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-01-10 10:49:36', '2017-01-10 10:49:36', NULL),
(NULL, 'view_nums', '1', '查看量', 'integer', 'LittleApp', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '', 0, '2017-01-10 10:51:28', '2017-01-10 10:51:28', NULL),
(NULL, 'qrcode', '1', '二维码', 'string', 'LittleApp', 'zh_cn', '300', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'file', '', '1', '', NULL, '', '', '', 0, '2017-01-10 10:52:02', '2017-01-10 10:52:02', NULL),
(NULL, 'summary', '1', '摘要', 'string', 'LittleApp', 'zh_cn', '500', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2017-01-10 10:53:46', '2017-01-10 10:53:46', NULL),
(NULL, 'content', '1', '详细介绍', 'content', 'LittleApp', 'zh_cn', '65535', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'ckeditor', '', '1', '', NULL, '', '', '', 0, '2017-01-10 10:54:07', '2017-01-10 10:54:07', NULL),
(NULL, 'thirdid', '1', '第三方编号', 'string', 'LittleApp', 'zh_cn', '32', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-01-10 10:56:14', '2017-01-10 10:56:14', NULL),
(NULL, 'third_type', '1', '第三方类型', 'string', 'LittleApp', 'zh_cn', '32', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-01-10 10:56:33', '2017-01-10 10:56:33', NULL),
(NULL, 'page_imgs', '0', '界面图片', 'string', 'LittleApp', 'zh_cn', '300', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'file', '', '1', '', NULL, '', '', '', 0, '2017-01-10 11:21:12', '2017-01-10 11:21:12', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'LittleApp', '小程序', '', 'default', '', 0, '2017-01-10 10:49:36', '2017-01-10 20:38:20', 'miao_little_apps', '', '', '', '0', 1, NULL, '0', '0', '');
