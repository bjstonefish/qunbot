set names utf8;
CREATE TABLE IF NOT EXISTS `miao_bot_messages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `message` mediumtext COLLATE utf8mb4_unicode_ci,
  `media_file` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `bot_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `bot_group_id` int(11) DEFAULT '0',
  `retry` int(11) DEFAULT '0',
  `type` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `sendtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'status', '1', ' 状态', 'integer', 'BotMessage', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '0=>未发送\r\n1=>已发送', '0', '', '', 'equal', 'select', '0', '1', '', NULL, '', '', '', 0, '2017-06-12 06:26:24', '2017-06-12 06:28:53', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'BotMessage', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-12 06:26:24', '2017-06-12 06:26:24', NULL),
(NULL, 'bot_id', '1', '机器人编号', 'integer', 'BotMessage', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2017-06-12 06:26:24', '2017-06-12 06:28:09', NULL),
(NULL, 'media_file', '1', '多媒体文件地址', 'string', 'BotMessage', NULL, '300', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'file', '', '1', '', NULL, '', '', '', 0, '2017-06-12 06:26:24', '2017-06-12 06:40:13', NULL),
(NULL, 'message', '1', '消息内容', 'content', 'BotMessage', NULL, '65535', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'textarea', '', '1', '', NULL, '', '', '', 0, '2017-06-12 06:26:24', '2017-06-12 06:27:33', NULL),
(NULL, 'id', '1', '编号', 'integer', 'BotMessage', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-12 06:26:24', '2017-06-12 06:26:24', NULL),
(NULL, 'sendtime', '1', '发送时间', 'datetime', 'BotMessage', NULL, '19', 0, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-12 06:39:12', '2017-06-12 06:39:12', NULL),
(NULL, 'type', '1', '消息类型', 'string', 'BotMessage', NULL, '10', 0, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-12 06:39:12', '2017-06-12 06:39:12', NULL),
(NULL, 'bot_group_id', '1', '群编号', 'integer', 'BotMessage', NULL, '11', 0, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-12 06:39:12', '2017-06-12 06:39:12', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'BotMessage', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 1, '2017-06-12 06:26:24', '2017-06-12 06:26:24', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'BotMessage', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-12 06:26:24', '2017-06-12 06:26:24', NULL),
(NULL, 'retry', '1', '重试次数', 'integer', 'BotMessage', NULL, '11', 0, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-12 06:39:12', '2017-06-12 06:39:12', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'BotMessage', '群发消息', '', 'default', '', 0, '2017-06-12 06:26:24', '2017-06-12 06:26:24', 'miao_bot_messages', '', '', '', '0', NULL, NULL, '0', '0', '');
