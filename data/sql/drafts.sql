set names utf8;
CREATE TABLE IF NOT EXISTS `miao_drafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `model` varchar(32) DEFAULT '',
  `data_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  KEY `model` (`model`,`creator`,`data_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'name', '1', '名称', 'string', 'draft', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-09-14 13:56:09', '2015-09-14 13:56:09', NULL),
(NULL, 'id', '1', '编号', 'integer', 'draft', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-09-14 13:56:09', '2015-09-14 13:56:09', NULL),
(NULL, 'model', '1', '数据所属模型', 'string', 'draft', NULL, '32', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-09-14 13:56:09', '2015-09-14 13:58:56', NULL),
(NULL, 'data_id', '1', '数据编号', 'integer', 'draft', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', '', '', '1', '', NULL, '', '', '', 0, '2015-09-14 13:56:09', '2015-09-14 13:58:20', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'draft', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-09-14 13:56:09', '2015-09-14 13:56:09', NULL),
(NULL, 'status', '1', '状态', 'integer', 'draft', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-09-14 13:56:09', '2015-09-14 13:56:09', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'draft', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-09-14 13:56:09', '2015-09-14 13:56:09', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'draft', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-09-14 13:56:09', '2015-09-14 13:56:09', NULL),
(NULL, 'content', '1', '内容', 'content', 'Draft', 'zh_cn', '65535', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2015-09-14 13:57:40', '2015-09-14 13:57:40', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'draft', '草稿', '', 'default', '', 27, '2015-09-14 13:56:09', '2015-09-14 13:56:09', 'miao_drafts', '', '', '', '0', NULL, NULL, '0', '0', '');
