set names utf8;
CREATE TABLE IF NOT EXISTS `miao_article_contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seotitle` varchar(255) DEFAULT NULL,
  `seokeywords` varchar(255) DEFAULT NULL,
  `seodescription` varchar(255) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', 'id', 'integer', 'ArticleContent', NULL, '11', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'hidden', '', '1', '', NULL, '', '', '', 0, '2014-07-16 23:47:24', '2014-12-01 07:18:14', NULL),
(NULL, 'seotitle', '1', 'seotitle', 'string', 'ArticleContent', NULL, '255', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2014-07-16 23:47:24', '2014-12-01 07:17:57', NULL),
(NULL, 'seokeywords', '1', 'seokeywords', 'string', 'ArticleContent', NULL, '255', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2014-07-16 23:47:24', '2014-12-01 07:18:28', NULL),
(NULL, 'seodescription', '1', 'seodescription', 'string', 'ArticleContent', NULL, '255', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2014-07-16 23:47:24', '2014-12-01 07:18:52', NULL),
(NULL, 'content', '1', '文章内容', 'content', 'ArticleContent', NULL, '65535', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'ckeditor', '', '1', '', NULL, '', '', '', 0, '2014-07-16 23:51:23', '2014-12-01 07:19:30', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'ArticleContent', '文章内容', 'onetomany', 'default', '', 27, '2014-07-16 23:45:21', '2014-12-01 07:49:48', 'miao_article_contents', '', '', '', '0', NULL, 0, '0', '0', '');
