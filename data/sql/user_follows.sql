set names utf8;
CREATE TABLE IF NOT EXISTS `miao_user_follows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wx_id` int(11) DEFAULT '0',
  `coverimg` varchar(200) DEFAULT '',
  `cate_id` varchar(20) DEFAULT NULL,
  `uid` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `published` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'UserFollow', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-07-05 15:56:14', '2014-07-05 15:56:14', NULL),
(NULL, 'wx_id', '1', '关注媒体编号', 'integer', 'UserFollow', NULL, '11', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', 'none', '', 0, '2014-07-05 15:56:14', '2014-07-05 16:00:09', ''),
(NULL, 'coverimg', '1', '封面图片', 'string', 'UserFollow', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 1, '2014-07-05 15:56:14', '2014-07-05 15:56:14', NULL),
(NULL, 'cate_id', '1', '分类', 'string', 'UserFollow', NULL, '20', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', 'implode', '', 0, '2014-07-05 15:56:14', '2014-07-05 17:21:33', ''),
(NULL, 'uid', '1', '用户编号', 'integer', 'UserFollow', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-07-05 15:56:14', '2014-07-05 16:00:39', ''),
(NULL, 'status', '1', '状态', 'integer', 'UserFollow', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 1, '2014-07-05 15:56:14', '2014-07-05 15:56:14', NULL),
(NULL, 'published', '1', '是否发布', 'integer', 'UserFollow', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 1, '2014-07-05 15:56:14', '2014-07-05 15:56:14', NULL),
(NULL, 'deleted', '1', '是否删除', 'integer', 'UserFollow', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-07-05 15:56:14', '2014-07-05 15:56:14', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'UserFollow', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-07-05 15:56:14', '2014-07-05 15:56:14', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'UserFollow', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-07-05 15:56:14', '2014-07-05 15:56:14', NULL),
(NULL, 'name', '1', '备注名称', 'string', 'UserFollow', 'zh_cn', '20', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-07-05 17:19:55', '2014-07-05 17:19:55', '');
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'UserFollow', '用户关注', '', 'default', '', 27, '2014-07-05 15:56:14', '2014-07-05 15:56:14', 'miao_user_follows', '', 'self', '', '0', NULL, 0, '0', '0', '');
