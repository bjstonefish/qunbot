set names utf8;
CREATE TABLE IF NOT EXISTS `miao_wx_vote_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_openid` varchar(44) DEFAULT '',
  `wx_id` int(11) DEFAULT '0',
  `vote_id` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wx_id` (`wx_id`,`vote_id`,`user_openid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'WxVoteLog', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-04 10:48:41', '2015-06-04 10:48:41', NULL),
(NULL, 'user_openid', '1', '名称', 'string', 'WxVoteLog', NULL, '44', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-04 10:48:41', '2015-06-05 23:18:52', NULL),
(NULL, 'wx_id', '1', '微信编号', 'integer', 'WxVoteLog', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-04 10:48:41', '2015-06-05 23:16:58', NULL),
(NULL, 'vote_id', '1', '投票编号', 'integer', 'WxVoteLog', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-04 10:48:41', '2015-06-05 23:19:19', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'WxVoteLog', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-04 10:48:41', '2015-06-04 10:48:41', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'WxVoteLog', '微信投票记录', '', 'default', '', 27, '2015-06-04 10:48:41', '2015-06-04 10:48:41', 'miao_wx_vote_logs', '', '', '', '0', NULL, NULL, '0', '0', '');
