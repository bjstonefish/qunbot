set names utf8;
CREATE TABLE IF NOT EXISTS `miao_operate_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `creator` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `params` varchar(1024) DEFAULT NULL,
  `postflg` tinyint(1) DEFAULT '0',
  `app_dir` varchar(15) DEFAULT NULL,
  `model` varchar(32) DEFAULT NULL,
  `data_id` int(11) DEFAULT '0',
  `action` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `model` (`model`,`data_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'data_id', '1', '数据编号', 'integer', 'OperateLog', NULL, '11', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-11-26 11:25:11', '2014-11-26 11:25:11', NULL),
(NULL, 'action', '1', '当前操作', 'string', 'OperateLog', 'en_us', '20', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-11-26 18:30:23', '2014-11-26 18:30:23', NULL),
(NULL, 'model', '1', '数据所属模块', 'string', 'OperateLog', NULL, '32', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-11-26 11:25:09', '2014-11-26 11:25:09', NULL),
(NULL, 'id', '1', '编号', 'integer', 'OperateLog', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-21 16:26:13', '2014-08-21 16:26:13', NULL),
(NULL, 'name', '1', '操作', 'string', 'OperateLog', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', '', '', '1', '', NULL, '', '', '形如 controller/action', 0, '2014-08-21 16:26:13', '2014-08-21 16:34:51', ''),
(NULL, 'creator', '1', '编创建者', 'integer', 'OperateLog', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-21 16:26:13', '2014-08-21 16:26:13', NULL),
(NULL, 'postflg', '1', '是否有数据提交', 'integer', 'OperateLog', 'zh_cn', '1', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-08-23 08:17:39', '2014-08-23 08:17:39', ''),
(NULL, 'created', '1', '创建时间', 'datetime', 'OperateLog', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-21 16:26:13', '2014-08-21 16:26:13', NULL),
(NULL, 'params', '1', '参数', 'string', 'OperateLog', 'zh_cn', '1024', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '仅记录GET参数，忽略POST参数，防止泄露提交信息造成不安全。', 0, '2014-08-21 16:33:17', '2014-08-23 07:19:56', ''),
(NULL, 'app_dir', '1', 'app路径', 'string', 'OperateLog', 'zh_cn', '15', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-09-26 23:38:10', '2014-09-26 23:38:10', '');
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'OperateLog', '操作记录', '', 'default', '', 27, '2014-08-21 16:26:13', '2014-08-21 16:26:13', 'miao_operate_logs', '', '', '', '0', 100, 0, '0', '0', '');
