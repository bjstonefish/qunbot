set names utf8;
CREATE TABLE IF NOT EXISTS `miao_survey_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `model` varchar(60) DEFAULT NULL,
  `data_id` int(11) DEFAULT NULL,
  `option_id` varchar(60) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `vote_nums` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'SurveyStat', 'zh_cn', '11', 11, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2010-09-12 20:03:10', '2010-09-12 20:03:10', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'SurveyStat', 'zh_cn', NULL, 2, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2010-09-12 20:03:10', '2010-09-12 20:03:10', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'SurveyStat', 'zh_cn', NULL, 1, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2010-09-12 20:03:10', '2010-09-12 20:03:10', NULL),
(NULL, 'model', '1', '模块', 'string', 'SurveyStat', 'zh_cn', '60', 10, '1', '1', '', NULL, NULL, NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2010-09-12 20:04:01', '2010-09-12 20:04:01', NULL),
(NULL, 'data_id', '1', '数据编号', 'integer', 'SurveyStat', 'zh_cn', '11', 9, '1', '1', '', NULL, NULL, NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2010-09-12 20:04:33', '2010-09-12 20:04:33', NULL),
(NULL, 'option_id', '1', '选项编号', 'string', 'SurveyStat', 'zh_cn', '60', 8, '1', '1', '', NULL, NULL, NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '选项编号不同于选项表中的编号。对多维评价表选项编号可能为 z-y-z。\n选项编号为字符串型，而非整型', 0, '2010-09-12 20:06:51', '2010-09-12 20:06:51', NULL),
(NULL, 'question_id', '1', '问题编号', 'integer', 'SurveyStat', 'zh_cn', '11', 7, '1', '1', '', NULL, NULL, NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2010-09-12 20:07:29', '2010-09-12 20:07:29', NULL),
(NULL, 'vote_nums', '1', '选项选择次数', 'integer', 'SurveyStat', 'zh_cn', '11', 6, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2010-09-12 20:08:06', '2016-08-12 22:01:59', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'SurveyStat', '调查统计', '', 'default', '', 27, '2010-09-12 20:03:10', '2016-08-12 21:42:56', 'miao_survey_stats', '', '', '', '0', NULL, 0, '0', '0', '');
