set names utf8;
CREATE TABLE IF NOT EXISTS `miao_wx_coupons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `coupon_code` varchar(32) DEFAULT NULL,
  `wx_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` smallint(3) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `promotion_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'WxCoupon', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-02-05 19:02:35', '2015-02-05 19:02:35', NULL),
(NULL, 'name', '1', '活动名称', 'string', 'WxCoupon', NULL, '100', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-02-05 19:02:35', '2015-02-05 19:28:55', NULL),
(NULL, 'coupon_code', '1', '优惠码', 'string', 'WxCoupon', NULL, '32', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-02-05 19:02:35', '2015-02-05 19:11:53', NULL),
(NULL, 'wx_id', '1', 'wx编号', 'integer', 'WxCoupon', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-02-05 19:02:35', '2015-02-05 19:06:07', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'WxCoupon', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-02-05 19:02:35', '2015-02-05 19:02:35', NULL),
(NULL, 'status', '1', '使用状态', 'integer', 'WxCoupon', NULL, '3', 3, '1', '1', '', '', '', 0, '1', '0=>否\r\n1=>是', '0', '', '', '', 'select', '0', '1', '', NULL, '', '', '0为未使用，1为已使用，-1过期作废', 0, '2015-02-05 19:02:35', '2015-02-05 19:13:40', NULL),
(NULL, 'created', '1', '领取时间', 'datetime', 'WxCoupon', NULL, '19', 2, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'datetime', '', '1', '', NULL, '', '', '', 0, '2015-02-05 19:02:35', '2015-02-05 19:15:15', NULL),
(NULL, 'updated', '1', '使用时间', 'datetime', 'WxCoupon', NULL, '19', 1, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'datetime', '', '1', '', NULL, '', '', '', 0, '2015-02-05 19:02:35', '2015-02-05 19:15:30', NULL),
(NULL, 'promotion_id', '1', '活动编号', 'integer', 'WxCoupon', 'en_us', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '', 0, '2015-02-05 19:19:23', '2015-02-05 19:19:23', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'WxCoupon', '优惠券', '', 'default', '', 27, '2015-02-05 19:02:35', '2015-02-05 19:02:35', 'miao_wx_coupons', '', '', '', '0', 2, 0, '0', '0', '');
