set names utf8;
CREATE TABLE IF NOT EXISTS `miao_view_tracks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT '0',
  `updated` datetime DEFAULT NULL,
  `model` char(24) DEFAULT NULL,
  `data_id` int(11) DEFAULT '0',
  `view_nums` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`model`,`data_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'ViewTrack', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-10-24 18:03:48', '2014-10-24 18:03:48', NULL),
(NULL, 'view_nums', '1', '访问次数', 'integer', 'ViewTrack', NULL, '11', 2, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-10-24 18:11:59', '2014-10-24 18:11:59', NULL),
(NULL, 'user_id', '1', '用户编号', 'integer', 'ViewTrack', NULL, '11', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', '', '', '1', '', NULL, '', '', '', 0, '2014-10-24 18:03:48', '2014-10-24 18:06:07', ''),
(NULL, 'updated', '1', '最后访问时间', 'datetime', 'ViewTrack', NULL, '10', 1, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'datetime', '', '1', '', NULL, '', '', '', 0, '2014-10-24 18:03:48', '2014-10-24 18:41:22', ''),
(NULL, 'data_id', '1', '数据编号', 'integer', 'ViewTrack', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-10-24 18:11:59', '2014-10-24 18:11:59', NULL),
(NULL, 'model', '1', '所属模块', 'char', 'ViewTrack', NULL, '24', 4, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-10-24 18:11:59', '2014-10-24 18:11:59', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'ViewTrack', '访问轨迹', '', 'default', '', 27, '2014-10-24 18:03:48', '2014-10-24 18:03:48', 'miao_view_tracks', '', 'self', '', '0', 2, 0, '0', '0', '');
