set names utf8;
CREATE TABLE IF NOT EXISTS `miao_team_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'TeamMember', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-12-04 09:52:53', '2014-12-04 09:52:53', NULL),
(NULL, 'team_id', '1', '所属团队', 'integer', 'TeamMember', NULL, '11', 6, '1', '1', 'Team', 'id', 'name', 0, '1', '', '0', '', '', '', 'select', '', '1', '', NULL, '', '', '', 0, '2014-12-04 09:52:53', '2014-12-04 09:54:45', NULL),
(NULL, 'user_id', '1', '用户编号', 'integer', 'TeamMember', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-12-04 09:52:53', '2014-12-04 09:55:14', NULL),
(NULL, 'status', '1', '状态', 'integer', 'TeamMember', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '0=>过期成员\r\n1=>正式成员', '0', '', '', '', 'select', '0', '1', '', NULL, '', '', '', 0, '2014-12-04 09:52:53', '2014-12-24 14:38:54', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'TeamMember', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-12-04 09:52:53', '2014-12-04 09:52:53', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'TeamMember', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-12-04 09:52:53', '2014-12-04 09:52:53', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'TeamMember', '团队成员', '', 'default', '', 27, '2014-12-04 09:52:53', '2014-12-04 09:52:53', 'miao_team_members', '', '', '', '0', 10, NULL, '0', '0', '');
