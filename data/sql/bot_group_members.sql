set names utf8;
CREATE TABLE IF NOT EXISTS `miao_bot_group_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bot_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT '0',
  `role` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `group_id` int(11) DEFAULT '0',
  `groupname` varchar(60) DEFAULT '',
  `nickname` varchar(60) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'BotGroupMember', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-12-04 09:52:53', '2014-12-04 09:52:53', NULL),
(NULL, 'bot_id', '1', '机器人编号', 'integer', 'BotGroupMember', NULL, '11', 6, '1', '1', '', 'id', 'name', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-12-04 09:52:53', '2017-07-20 08:48:47', NULL),
(NULL, 'user_id', '1', '用户编号', 'integer', 'BotGroupMember', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-12-04 09:52:53', '2014-12-04 09:55:14', NULL),
(NULL, 'role', '1', '角色', 'integer', 'BotGroupMember', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '0=>普通成员\r\n1=>超级管理成员\r\n2=>管理成员', '0', '', '', '', 'select', '0', '1', '', NULL, '', '', '', 0, '2014-12-04 09:52:53', '2017-07-20 08:51:56', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'BotGroupMember', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-12-04 09:52:53', '2014-12-04 09:52:53', NULL),
(NULL, 'group_id', '1', '群编号', 'integer', 'BotGroupMember', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-07-20 08:50:16', '2017-07-20 08:50:16', NULL),
(NULL, 'groupname', '1', '群名', 'string', 'BotGroupMember', 'zh_cn', '60', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-07-20 08:55:34', '2017-07-20 08:55:34', NULL),
(NULL, 'nickname', '1', '昵称', 'string', 'BotGroupMember', 'zh_cn', '60', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-07-20 08:55:45', '2017-07-20 08:55:45', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'BotGroupMember', '群成员', '', 'default', '', 0, '2017-07-20 08:42:04', '2017-07-20 08:42:04', 'miao_bot_group_members', '', '', '', '0', NULL, NULL, '0', '0', '');
