set names utf8;
CREATE TABLE IF NOT EXISTS `miao_recycles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(200) DEFAULT '',
  `deleted` tinyint(1) DEFAULT '0',
  `updated` datetime DEFAULT NULL,
  `model` varchar(20) DEFAULT NULL,
  `data_id` int(11) DEFAULT '0',
  `content` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `model` (`model`,`data_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'name', '1', '名称', 'string', 'Recycle', NULL, '200', 7, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-08 23:03:29', '2014-08-08 23:03:29', NULL),
(NULL, 'id', '1', '编号', 'integer', 'Recycle', NULL, '11', 8, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-08 23:03:29', '2014-08-08 23:03:29', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'Recycle', NULL, '200', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-08 23:03:29', '2014-08-08 23:03:29', NULL),
(NULL, 'data_id', '1', '数据id', 'integer', 'Recycle', 'zh_cn', '11', 4, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-08-08 23:24:21', '2014-08-08 23:24:21', ''),
(NULL, 'content', '1', '数据内容', 'mediumtext', 'Recycle', 'zh_cn', '65535', 3, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '数据内容，使用serialize压缩', 0, '2014-08-08 23:33:03', '2014-08-08 23:33:03', ''),
(NULL, 'deleted', '1', '是否删除', 'integer', 'Recycle', NULL, '11', 2, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-08 23:03:29', '2014-08-08 23:03:29', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Recycle', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-08 23:03:29', '2014-08-08 23:03:29', NULL),
(NULL, 'model', '1', '所属模块', 'string', 'Recycle', 'zh_cn', '20', 5, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', 'none', '', 0, '2014-08-08 23:11:30', '2014-08-08 23:11:30', '');
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Recycle', '回收站', '', 'default', '', 27, '2014-08-08 23:03:29', '2014-08-08 23:03:29', 'miao_recycles', '', '', '', '0', 100, 0, '0', '0', '');
