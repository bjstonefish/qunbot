set names utf8;
CREATE TABLE IF NOT EXISTS `miao_user_cate_i18ns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(200) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `foreign_key` int(11) DEFAULT '0',
  `locale` char(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'UserCateI18n', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-02-12 14:13:13', '2015-02-12 14:13:13', NULL),
(NULL, 'name', '1', '名称', 'string', 'UserCateI18n', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-02-12 14:13:13', '2015-02-12 14:13:13', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'UserCateI18n', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 1, '2015-02-12 14:13:13', '2015-02-12 14:13:13', NULL),
(NULL, 'cate_id', '1', '分类', 'integer', 'UserCateI18n', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 1, '2015-02-12 14:13:13', '2015-02-12 14:13:13', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'UserCateI18n', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-02-12 14:13:13', '2015-02-12 14:13:13', NULL),
(NULL, 'status', '1', '状态', 'integer', 'UserCateI18n', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 1, '2015-02-12 14:13:13', '2015-02-12 14:13:13', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'UserCateI18n', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-02-12 14:13:13', '2015-02-12 14:13:13', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'UserCateI18n', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-02-12 14:13:13', '2015-02-12 14:13:13', NULL),
(NULL, 'foreign_key', '1', '外键数据id', 'integer', 'UserCateI18n', 'en_us', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-02-12 14:15:32', '2015-02-12 14:15:32', NULL),
(NULL, 'locale', '1', '数据语言版本', 'char', 'UserCateI18n', 'en_us', '5', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-02-12 14:52:51', '2015-02-12 14:52:51', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'UserCateI18n', '用户分类语言版本', '', 'default', '', 27, '2015-02-12 14:13:13', '2015-02-12 14:13:13', 'miao_user_cate_i18ns', '', '', '', '0', 2, NULL, '0', '0', '');
