set names utf8;
CREATE TABLE IF NOT EXISTS `miao_single_colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `color` varchar(30) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `click_nums` int(11) DEFAULT '0',
  `remark` varchar(60) DEFAULT '',
  `r` smallint(3) DEFAULT '0',
  `g` smallint(3) DEFAULT '0',
  `b` smallint(3) DEFAULT '0',
  `a` decimal(11,5) DEFAULT '0.00000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `color` (`color`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
