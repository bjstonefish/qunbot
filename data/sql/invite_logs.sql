set names utf8;
CREATE TABLE IF NOT EXISTS `miao_invite_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT '0',
  `invitor_id` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `invitor_id` (`invitor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
