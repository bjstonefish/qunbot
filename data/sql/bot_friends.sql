set names utf8;
CREATE TABLE IF NOT EXISTS `miao_bot_friends` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nickname` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `username` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `bot_id` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bot_id` (`bot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'BotFriend', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-28 13:29:30', '2017-06-28 13:29:30', NULL),
(NULL, 'nickname', '1', '名称', 'string', 'BotFriend', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2017-06-28 13:29:30', '2017-06-28 13:30:17', NULL),
(NULL, 'username', '1', '用户名', 'string', 'BotFriend', NULL, '300', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2017-06-28 13:29:30', '2017-06-28 13:30:44', NULL),
(NULL, 'bot_id', '1', '机器人编号', 'integer', 'BotFriend', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', '', '', '1', '', NULL, '', '', '', 0, '2017-06-28 13:29:30', '2017-06-28 13:31:02', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'BotFriend', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-28 13:29:30', '2017-06-28 13:29:30', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'BotFriend', '机器人好友', '', 'default', '', 0, '2017-06-28 13:29:30', '2017-06-28 13:29:30', 'miao_bot_friends', '', '', '', '0', 100, NULL, '0', '0', '');
