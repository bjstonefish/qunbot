<?php
$datas = array (
  0 => 
  array (
    'Aro' => 
    array (
      'id' => '1',
      'parent_id' => NULL,
      'model' => 'Role',
      'foreign_key' => '1',
      'alias' => '',
      'lft' => '3',
      'rght' => '6',
      'name' => NULL,
    ),
    'children' => 
    array (
      0 => 
      array (
        'Aro' => 
        array (
          'id' => '65',
          'parent_id' => '1',
          'model' => 'Staff',
          'foreign_key' => '1',
          'alias' => '',
          'lft' => '4',
          'rght' => '5',
          'name' => NULL,
        ),
        'children' => 
        array (
        ),
      ),
    ),
  ),
  1 => 
  array (
    'Aro' => 
    array (
      'id' => '2',
      'parent_id' => NULL,
      'model' => 'Role',
      'foreign_key' => '2',
      'alias' => '',
      'lft' => '7',
      'rght' => '12',
      'name' => NULL,
    ),
    'children' => 
    array (
      0 => 
      array (
        'Aro' => 
        array (
          'id' => '72',
          'parent_id' => '2',
          'model' => 'Staff',
          'foreign_key' => '73',
          'alias' => '',
          'lft' => '10',
          'rght' => '11',
          'name' => NULL,
        ),
        'children' => 
        array (
        ),
      ),
      1 => 
      array (
        'Aro' => 
        array (
          'id' => '73',
          'parent_id' => '2',
          'model' => 'Staff',
          'foreign_key' => '74',
          'alias' => '',
          'lft' => '8',
          'rght' => '9',
          'name' => NULL,
        ),
        'children' => 
        array (
        ),
      ),
    ),
  ),
  2 => 
  array (
    'Aro' => 
    array (
      'id' => '4',
      'parent_id' => NULL,
      'model' => 'Role',
      'foreign_key' => '4',
      'alias' => '',
      'lft' => '1',
      'rght' => '2',
      'name' => NULL,
    ),
    'children' => 
    array (
    ),
  ),
  3 => 
  array (
    'Aro' => 
    array (
      'id' => '116',
      'parent_id' => NULL,
      'model' => 'Role',
      'foreign_key' => '101',
      'alias' => '',
      'lft' => '13',
      'rght' => '14',
      'name' => NULL,
    ),
    'children' => 
    array (
    ),
  ),
  4 => 
  array (
    'Aro' => 
    array (
      'id' => '117',
      'parent_id' => NULL,
      'model' => 'Role',
      'foreign_key' => '8',
      'alias' => '',
      'lft' => '15',
      'rght' => '16',
      'name' => NULL,
    ),
    'children' => 
    array (
    ),
  ),
  5 => 
  array (
    'Aro' => 
    array (
      'id' => '118',
      'parent_id' => NULL,
      'model' => 'Role',
      'foreign_key' => '3',
      'alias' => '',
      'lft' => '17',
      'rght' => '18',
      'name' => NULL,
    ),
    'children' => 
    array (
    ),
  ),
);


	saveTreeItems($datas,'Aro');
