set names utf8;
CREATE TABLE IF NOT EXISTS `miao_medication_records` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `customer_id` int(11) DEFAULT '0',
  `started` date DEFAULT NULL,
  `ended` date DEFAULT NULL,
  `guige` float(10,2) DEFAULT '0.00',
  `count` int(11) DEFAULT '0',
  `perdose` float(10,2) DEFAULT '0.00',
  `frequency` smallint(3) DEFAULT '0',
  `remind` tinyint(1) DEFAULT '0',
  `remind_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'customer_id', '1', '客户编号', 'integer', 'MedicationRecord', 'zh_cn', '11', 12, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-02-10 08:46:44', '2017-02-10 08:46:44', NULL),
(NULL, 'id', '1', '编号', 'integer', 'MedicationRecord', NULL, '11', 14, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 17:24:23', '2017-02-09 17:24:23', NULL),
(NULL, 'name', '1', '药品名称', 'string', 'MedicationRecord', NULL, '200', 10, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2017-02-09 17:24:23', '2017-02-10 08:45:37', NULL),
(NULL, 'cate_id', '1', '药品编号', 'integer', 'MedicationRecord', NULL, '11', 11, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2017-02-09 17:24:23', '2017-02-10 08:46:06', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'MedicationRecord', NULL, '11', 13, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 17:24:23', '2017-02-09 17:24:23', NULL),
(NULL, 'ended', '1', '预计结束时间', 'date', 'MedicationRecord', NULL, '10', 8, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'date', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-10 09:11:31', '2017-02-10 09:11:31', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'MedicationRecord', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 17:24:23', '2017-02-09 17:24:23', NULL),
(NULL, 'guige', '1', '规格', 'float', 'MedicationRecord', NULL, '10,2', 7, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-10 09:11:31', '2017-02-10 09:11:31', NULL),
(NULL, 'started', '1', '开始服用时间', 'date', 'MedicationRecord', NULL, '10', 9, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'date', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-10 09:11:31', '2017-02-10 09:11:31', NULL),
(NULL, 'count', '1', '总数', 'integer', 'MedicationRecord', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-10 09:11:31', '2017-02-10 09:11:31', NULL),
(NULL, 'perdose', '1', '每次剂量', 'float', 'MedicationRecord', NULL, '10,2', 5, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-10 09:11:31', '2017-02-10 09:11:31', NULL),
(NULL, 'frequency', '1', '每日次数', 'integer', 'MedicationRecord', NULL, '3', 4, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-10 09:11:31', '2017-02-10 09:11:31', NULL),
(NULL, 'remind', '1', '是否提醒', 'integer', 'MedicationRecord', NULL, '1', 3, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'checkbox', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-10 09:11:31', '2017-02-10 09:11:31', NULL),
(NULL, 'remind_date', '1', '提醒日期', 'date', 'MedicationRecord', NULL, '10', 2, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'date', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-10 09:11:31', '2017-02-10 09:11:31', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'MedicationRecord', '用药记录', '', 'default', '', 0, '2017-02-09 17:24:23', '2017-02-10 10:16:04', 'miao_medication_records', '', 'branch', '', '0', NULL, NULL, '0', '0', '');
