set names utf8;
CREATE TABLE IF NOT EXISTS `miao_questionnaire_surveys` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `questionnaire_id` int(11) DEFAULT '0',
  `survey_id` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'QuestionnaireSurvey', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-18 17:57:29', '2016-11-18 17:57:29', NULL),
(NULL, 'questionnaire_id', '1', '考试问卷编号', 'integer', 'QuestionnaireSurvey', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2016-11-18 17:57:29', '2016-11-18 17:59:17', NULL),
(NULL, 'survey_id', '1', '调查试题编号', 'integer', 'QuestionnaireSurvey', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2016-11-18 17:57:29', '2016-11-18 17:58:54', NULL),
(NULL, 'status', '1', '状态', 'integer', 'QuestionnaireSurvey', NULL, '11', 3, '1', '1', '', NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-18 17:57:29', '2016-11-18 17:57:29', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'QuestionnaireSurvey', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-18 17:57:29', '2016-11-18 17:57:29', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'QuestionnaireSurvey', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-18 17:57:29', '2016-11-18 17:57:29', NULL),
(NULL, 'sort', '1', '排序先后', 'integer', 'QuestionnaireSurvey', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '值越大的排越前面', 0, '2017-04-18 17:37:18', '2017-04-18 17:37:18', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'QuestionnaireSurvey', '问卷试题', '', 'default', '', 0, '2016-11-18 17:57:29', '2016-11-18 17:57:29', 'miao_questionnaire_surveys', '', '', '', '0', NULL, NULL, '0', '0', '');
