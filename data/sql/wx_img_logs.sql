set names utf8;
CREATE TABLE IF NOT EXISTS `miao_wx_img_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(200) DEFAULT '',
  `wximg_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `wx_id` int(11) DEFAULT '0',
  `content` text,
  `wx_openid` char(44) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'WxImgLog', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-28 11:32:33', '2015-06-28 11:32:33', NULL),
(NULL, 'name', '1', '名称', 'string', 'WxImgLog', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-28 11:32:33', '2015-06-28 11:32:33', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'WxImgLog', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-28 11:32:33', '2015-06-28 11:32:33', NULL),
(NULL, 'wximg_id', '1', '图片编号', 'integer', 'WxImgLog', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-28 11:32:33', '2015-06-28 11:33:49', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'WxImgLog', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-28 11:32:33', '2015-06-28 11:32:33', NULL),
(NULL, 'status', '1', '状态', 'integer', 'WxImgLog', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-28 11:32:33', '2015-06-28 11:32:33', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'WxImgLog', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-28 11:32:33', '2015-06-28 11:32:33', NULL),
(NULL, 'wx_id', '1', '所属微信', 'integer', 'WxImgLog', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-28 11:34:31', '2015-06-28 11:34:31', NULL),
(NULL, 'content', '1', '用户输入', 'content', 'WxImgLog', 'zh_cn', '65535', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2015-06-28 11:35:50', '2015-06-28 11:35:50', NULL),
(NULL, 'wx_openid', '1', '微信编号', 'char', 'WxImgLog', 'zh_cn', '44', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-28 11:43:08', '2015-06-28 11:43:08', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'WxImgLog', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-28 11:32:33', '2015-06-28 11:32:33', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'WxImgLog', '微信配图生成记录', '', 'default', '', 27, '2015-06-28 11:32:33', '2015-06-28 11:32:33', 'miao_wx_img_logs', '', '', '', '0', NULL, NULL, '0', '0', '');
