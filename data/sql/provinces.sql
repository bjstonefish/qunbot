set names utf8;
CREATE TABLE IF NOT EXISTS `miao_provinces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `published` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `locale` char(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Province', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-05-21 17:48:21', '2014-05-21 17:48:21', NULL),
(NULL, 'name', '1', '省/州名称', 'string', 'Province', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', '', '', '1', '', NULL, '', '', '', 0, '2014-05-21 17:48:21', '2015-01-23 12:08:27', ''),
(NULL, 'country', '1', '国家', 'string', 'Province', NULL, '30', 6, '1', '1', '', 'id', 'name', 0, '1', 'China=>China\r\nAustralia=>Australia', '0', '', '', '', 'input', '', '1', '', NULL, '', 'none', '', 0, '2014-05-21 17:48:21', '2015-01-23 12:08:01', ''),
(NULL, 'published', '1', '是否发布', 'integer', 'Province', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '0=>否\r\n1=>是', '0', '', '', '', 'select', '1', '1', '', NULL, '', '', '', 0, '2014-05-21 17:48:21', '2014-05-21 17:48:21', ''),
(NULL, 'created', '1', '创建时间', 'datetime', 'Province', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-05-21 17:48:21', '2014-05-21 17:48:21', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Province', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-05-21 17:48:21', '2014-05-21 17:48:21', NULL),
(NULL, 'locale', '1', '语言版本', 'char', 'Province', 'en_us', '5', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', 'none', '', 0, '2015-01-23 11:32:13', '2015-01-23 11:32:13', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Province', '省/州', '', '', '', 27, '2015-01-23 12:04:46', '2015-01-23 12:04:46', 'miao_provinces', '', '', '', '0', 2, 1, '0', '0', '');
