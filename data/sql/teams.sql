set names utf8;
CREATE TABLE IF NOT EXISTS `miao_teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(200) DEFAULT NULL,
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'cate_id', '1', '分类', 'integer', 'Team', NULL, '11', 7, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-12-04 09:48:15', '2014-12-04 09:48:15', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'Team', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'file', '', '1', '', NULL, '', '', '', 0, '2014-12-04 09:48:15', '2014-12-23 13:42:46', NULL),
(NULL, 'name', '1', '名称', 'string', 'Team', NULL, '200', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-12-04 09:48:15', '2014-12-04 09:48:15', NULL),
(NULL, 'creator', '1', '编创建者', 'integer', 'Team', NULL, '11', 4, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-12-04 09:48:15', '2014-12-04 09:48:15', NULL),
(NULL, 'id', '1', '编号', 'integer', 'Team', NULL, '11', 8, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-12-04 09:48:15', '2014-12-04 09:48:15', NULL),
(NULL, 'status', '1', '状态', 'integer', 'Team', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-12-04 09:48:15', '2014-12-04 09:48:15', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Team', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-12-04 09:48:15', '2014-12-04 09:48:15', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Team', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-12-04 09:48:15', '2014-12-04 09:48:15', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Team', '团队', '', 'default', '', 27, '2014-12-04 09:48:15', '2014-12-04 09:48:15', 'miao_teams', '', 'branch', '', '0', 10, NULL, '0', '0', '');
