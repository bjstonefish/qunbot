set names utf8;
CREATE TABLE IF NOT EXISTS `miao_express_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `order_id` int(11) DEFAULT '0',
  `status` varchar(20) DEFAULT '',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'ExpressLog', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-20 20:43:10', '2015-06-20 20:43:10', NULL),
(NULL, 'name', '1', '名称', 'string', 'ExpressLog', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-20 20:43:10', '2015-06-20 20:43:10', NULL),
(NULL, 'order_id', '1', '订单编号', 'integer', 'ExpressLog', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-20 20:43:10', '2015-06-20 21:36:05', NULL),
(NULL, 'status', '1', '状态', 'string', 'ExpressLog', NULL, '20', 3, '1', '1', '', '', '', 0, '1', '0=>否\r\n1=>是', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-20 20:43:10', '2015-06-20 22:17:15', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'ExpressLog', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-20 20:43:10', '2015-06-20 20:43:10', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`) VALUES (NULL, 'ExpressLog', '订单快递记录', '', 'default', '', 27, '2015-06-20 20:43:10', '2015-06-20 20:43:10', 'miao_express_logs', '', '', '', '0', NULL, NULL);
