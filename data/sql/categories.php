<?php
$datas = array (
  0 => 
  array (
    'Category' => 
    array (
      'id' => '110',
      'name' => '站点导航',
      'parent_id' => NULL,
      'left' => '9',
      'right' => '78',
      'slug' => 'www',
      'visible' => '0',
      'description' => '',
      'seotitle' => '',
      'seokeywords' => '',
      'seodescription' => '',
      'link' => '',
      'updated' => '2014-04-13 18:25:21',
      'created' => '2013-03-13 21:44:52',
      'content' => '',
      'locale' => 'zh_cn',
      'model' => 'Category',
      'template' => 'categories/_list',
      'submenu' => '',
      'coverimg' => '/img/logo.png',
      'domain' => 'www.m.com',
      'theme' => '',
      'view_template' => '',
      'view_layout' => NULL,
      'prefix_icon' => NULL,
      'model_conditions' => NULL,
      'is_index' => '0',
    ),
    'Uploadfile' => 
    array (
    ),
    'CategoryI18n' => 
    array (
      'zh_cn' => 
      array (
        'id' => '110',
        'name' => '站点导航',
        'parent_id' => NULL,
        'left' => '9',
        'right' => '78',
        'slug' => 'www',
        'visible' => '0',
        'description' => '',
        'seotitle' => '',
        'seokeywords' => '',
        'seodescription' => '',
        'link' => '',
        'updated' => '2014-04-13 18:25:21',
        'created' => '2013-03-13 21:44:52',
        'content' => '',
        'locale' => 'zh_cn',
        'model' => 'Category',
        'template' => 'categories/_list',
        'submenu' => '',
        'coverimg' => '/img/logo.png',
        'domain' => 'www.m.com',
        'theme' => '',
        'view_template' => '',
        'view_layout' => NULL,
        'prefix_icon' => NULL,
        'model_conditions' => NULL,
        'is_index' => '0',
      ),
    ),
    'Tag' => 
    array (
    ),
    'children' => 
    array (
      0 => 
      array (
        'Category' => 
        array (
          'id' => '31',
          'name' => '解决方案',
          'parent_id' => '110',
          'left' => '18',
          'right' => '23',
          'slug' => 'solutions',
          'visible' => '1',
          'description' => '为二位',
          'seotitle' => '',
          'seokeywords' => '',
          'seodescription' => '',
          'link' => '',
          'updated' => '2014-11-20 17:27:41',
          'created' => '2010-03-30 15:10:03',
          'content' => '<h3>行业<span>解决方案:</span></h3>

<div>
<div><img alt="行业解决方案" src="http://www.neusoft.com/upload/images/20110311/1299810603851.jpg" /></div>

<div>
<p>东软面向行业客户，提供安全、可靠、高质量、易扩展的行业解决方案，帮助您实现信息化管理最佳实践，以满足您业务快速发展的不同需求。</p>

<p>行业解决方案涵盖的领域包括：电信、电力、金融、政府（社会保障、财政、税务、公共安全、国土资源、海洋、质量监督检验检疫、工商、电子政务、环保、农业等）、制造与商贸流通业、医疗卫生、教育、交通等行业。</p>

<p><a href="http://www.neusoft.com/cn/solutions/0268/">更多 &gt;&gt;</a></p>
</div>

<div>&nbsp;</div>
</div>

<p>&nbsp;</p>

<div style="margin-top: : 5px; width: 690px; border-top: #dddddd 1px solid">&nbsp;</div>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h3>产品工程<span>解决方案:</span></h3>

<p>&nbsp;</p>

<div>
<div><img alt="产品工程解决方案" src="http://www.neusoft.com/upload/images/20110311/1299811015702.jpg" /></div>

<div>
<p>东软可提供多种多样的产品工程解决方案及相关服务，包括产品架构、设计、嵌入式软件开发、测试、维护和产品支持等。</p>

<p>东软的嵌入式软件系统在世界著名的数字家庭产品、移动终端、车载信息产品、IT产品等众多产品中运行。我们的客户遍布世界各地。</p>

<p><a href="http://www.neusoft.com/cn/solutions/0246/">更多 &gt;&gt;</a></p>
</div>

<div>&nbsp;</div>
</div>
',
          'locale' => 'zh_cn',
          'model' => '',
          'template' => 'categories/_list',
          'submenu' => ' <style>
.solutions li, .service li, .product li {
    clear: both;
    padding: 0;
    width: 99%;
}
.solutions {
    border-bottom: 5px solid #0188CC;
    border-right: 1px solid #000000;
    clear: both;
    height: 287px;
    overflow: hidden;
    width: 620px;
}
.solutions_list {
    clear: both;
    height: 65px;
    margin-bottom: 15px;
}
.solutions_left {
    border-right: 1px solid #E0E0E0;
    float: left;
    line-height: 20px;
    text-indent: 20px;
    width: 190px;
}
.solutions_mid {
    border-right: 1px solid #E0E0E0;
    float: left;
    line-height: 20px;
    text-indent: 20px;
    width: 195px;
}
.solutions_right {
    float: left;
    line-height: 20px;
    margin-left: 15px;
    text-indent: 5px;
    width: 205px;
}
</style><div class="downmenu dropdown-menu dmwidth1">
  <div class="solutions">
    <h2><a href="http://www.neusoft.com/cn/solutions/" >东软解决方案</a></h2>
    <h3><a href="http://www.neusoft.com/cn/solutions/0268/">行业解决方案</a></h3>
    <div class="solutions_list">
      <ul class="solutions_left">
        <li><a href="http://www.neusoft.com/cn/solutions/0446/">电信</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/0448/">能源</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/0449/">金融</a></li>
      </ul>
      <ul class="solutions_mid">
        <li><a href="http://www.neusoft.com/cn/solutions/0447/">政府</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/1475/">制造业</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/0450/">商贸流通业</a></li>
      </ul>
      <ul class="solutions_right">
        <li><a href="http://www.neusoft.com/cn/solutions/0451/">医疗卫生</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/0452/">教育</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/0453/">交通</a></li>
      </ul>
      <div class="clear"></div>
    </div>
    <h3><a href="http://www.neusoft.com/cn/solutions/0246/">产品工程解决方案</a></h3>
    <div class="solutions_list">
      <ul class="solutions_left">
        <li><a href="http://www.neusoft.com/cn/solutions/0458/">汽车电子</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/0457/">移动终端</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/0456/">数字家庭产品</a></li>
      </ul>
      <ul class="solutions_mid">
        <li><a href="http://www.neusoft.com/cn/solutions/0459/">IT产品</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/1453/">广电设备与系统</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/1454/">零售电子</a></li>
      </ul>
      <ul class="solutions_right">
        <li><a href="http://www.neusoft.com/cn/solutions/1455/">工业控制</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/1044/">医疗产品</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/0721/">网络安全</a></li>
      </ul>
      <div class="clear"></div>
    </div>
  </div>
</div>',
          'coverimg' => '/files/201411/c691e275293999308321932c.png',
          'domain' => '',
          'theme' => '',
          'view_template' => '',
          'view_layout' => '',
          'prefix_icon' => NULL,
          'model_conditions' => NULL,
          'is_index' => '0',
        ),
        'Uploadfile' => 
        array (
        ),
        'CategoryI18n' => 
        array (
          'en_us' => 
          array (
            'id' => '1',
            'name' => 'Solutions',
            'deleted' => '0',
            'created' => '2014-11-10 14:27:44',
            'updated' => '2014-11-10 14:27:44',
            'locale' => 'en_us',
            'foreign_key' => '31',
            'content' => '<p><span style="color:rgb(204, 0, 0); font-family:arial,helvetica; font-size:13px; line-height:18px">Solutions</span><span style="color:rgb(51, 51, 51); font-family:arial,helvetica; font-size:13px; line-height:18px">&nbsp;</span><span style="color:rgb(204, 0, 0); font-family:arial,helvetica; font-size:13px; line-height:18px">Enterprise</span><span style="color:rgb(51, 51, 51); font-family:arial,helvetica; font-size:13px; line-height:18px">&nbsp;is&nbsp;an&nbsp;international&nbsp;IT&nbsp;services&nbsp;company,&nbsp;offering&nbsp;clients&nbsp;innovative&nbsp;andcost-effective&nbsp;technology&nbsp;services&nbsp;and&nbsp;</span><span style="color:rgb(204, 0, 0); font-family:arial,helvetica; font-size:13px; line-height:18px">solutions</span><span style="color:rgb(51, 51, 51); font-family:arial,helvetica; font-size:13px; line-height:18px">.</span></p>
',
          ),
          'zh_cn' => 
          array (
            'id' => '31',
            'name' => '解决方案',
            'parent_id' => '110',
            'left' => '18',
            'right' => '23',
            'slug' => 'solutions',
            'visible' => '1',
            'description' => '为二位',
            'seotitle' => '',
            'seokeywords' => '',
            'seodescription' => '',
            'link' => '',
            'updated' => '2014-11-20 17:27:41',
            'created' => '2010-03-30 15:10:03',
            'content' => '<h3>行业<span>解决方案:</span></h3>

<div>
<div><img alt="行业解决方案" src="http://www.neusoft.com/upload/images/20110311/1299810603851.jpg" /></div>

<div>
<p>东软面向行业客户，提供安全、可靠、高质量、易扩展的行业解决方案，帮助您实现信息化管理最佳实践，以满足您业务快速发展的不同需求。</p>

<p>行业解决方案涵盖的领域包括：电信、电力、金融、政府（社会保障、财政、税务、公共安全、国土资源、海洋、质量监督检验检疫、工商、电子政务、环保、农业等）、制造与商贸流通业、医疗卫生、教育、交通等行业。</p>

<p><a href="http://www.neusoft.com/cn/solutions/0268/">更多 &gt;&gt;</a></p>
</div>

<div>&nbsp;</div>
</div>

<p>&nbsp;</p>

<div style="margin-top: : 5px; width: 690px; border-top: #dddddd 1px solid">&nbsp;</div>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h3>产品工程<span>解决方案:</span></h3>

<p>&nbsp;</p>

<div>
<div><img alt="产品工程解决方案" src="http://www.neusoft.com/upload/images/20110311/1299811015702.jpg" /></div>

<div>
<p>东软可提供多种多样的产品工程解决方案及相关服务，包括产品架构、设计、嵌入式软件开发、测试、维护和产品支持等。</p>

<p>东软的嵌入式软件系统在世界著名的数字家庭产品、移动终端、车载信息产品、IT产品等众多产品中运行。我们的客户遍布世界各地。</p>

<p><a href="http://www.neusoft.com/cn/solutions/0246/">更多 &gt;&gt;</a></p>
</div>

<div>&nbsp;</div>
</div>
',
            'locale' => 'zh_cn',
            'model' => '',
            'template' => 'categories/_list',
            'submenu' => ' <style>
.solutions li, .service li, .product li {
    clear: both;
    padding: 0;
    width: 99%;
}
.solutions {
    border-bottom: 5px solid #0188CC;
    border-right: 1px solid #000000;
    clear: both;
    height: 287px;
    overflow: hidden;
    width: 620px;
}
.solutions_list {
    clear: both;
    height: 65px;
    margin-bottom: 15px;
}
.solutions_left {
    border-right: 1px solid #E0E0E0;
    float: left;
    line-height: 20px;
    text-indent: 20px;
    width: 190px;
}
.solutions_mid {
    border-right: 1px solid #E0E0E0;
    float: left;
    line-height: 20px;
    text-indent: 20px;
    width: 195px;
}
.solutions_right {
    float: left;
    line-height: 20px;
    margin-left: 15px;
    text-indent: 5px;
    width: 205px;
}
</style><div class="downmenu dropdown-menu dmwidth1">
  <div class="solutions">
    <h2><a href="http://www.neusoft.com/cn/solutions/" >东软解决方案</a></h2>
    <h3><a href="http://www.neusoft.com/cn/solutions/0268/">行业解决方案</a></h3>
    <div class="solutions_list">
      <ul class="solutions_left">
        <li><a href="http://www.neusoft.com/cn/solutions/0446/">电信</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/0448/">能源</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/0449/">金融</a></li>
      </ul>
      <ul class="solutions_mid">
        <li><a href="http://www.neusoft.com/cn/solutions/0447/">政府</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/1475/">制造业</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/0450/">商贸流通业</a></li>
      </ul>
      <ul class="solutions_right">
        <li><a href="http://www.neusoft.com/cn/solutions/0451/">医疗卫生</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/0452/">教育</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/0453/">交通</a></li>
      </ul>
      <div class="clear"></div>
    </div>
    <h3><a href="http://www.neusoft.com/cn/solutions/0246/">产品工程解决方案</a></h3>
    <div class="solutions_list">
      <ul class="solutions_left">
        <li><a href="http://www.neusoft.com/cn/solutions/0458/">汽车电子</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/0457/">移动终端</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/0456/">数字家庭产品</a></li>
      </ul>
      <ul class="solutions_mid">
        <li><a href="http://www.neusoft.com/cn/solutions/0459/">IT产品</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/1453/">广电设备与系统</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/1454/">零售电子</a></li>
      </ul>
      <ul class="solutions_right">
        <li><a href="http://www.neusoft.com/cn/solutions/1455/">工业控制</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/1044/">医疗产品</a></li>
        <li><a href="http://www.neusoft.com/cn/solutions/0721/">网络安全</a></li>
      </ul>
      <div class="clear"></div>
    </div>
  </div>
</div>',
            'coverimg' => '/files/201411/c691e275293999308321932c.png',
            'domain' => '',
            'theme' => '',
            'view_template' => '',
            'view_layout' => '',
            'prefix_icon' => NULL,
            'model_conditions' => NULL,
            'is_index' => '0',
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
          0 => 
          array (
            'Category' => 
            array (
              'id' => '91',
              'name' => '行业解决方案',
              'parent_id' => '31',
              'left' => '19',
              'right' => '20',
              'slug' => '',
              'visible' => '1',
              'description' => '',
              'seotitle' => '',
              'seokeywords' => '',
              'seodescription' => '',
              'link' => '',
              'updated' => '2012-09-04 22:44:31',
              'created' => '2012-08-05 21:15:42',
              'content' => '<p>
	http://www.topwom.com/</p>
<p>
	&nbsp;</p>
<p>
	&nbsp;</p>
<p>
	http://www.topwom.com/</p>
',
              'locale' => 'zh_cn',
              'model' => '',
              'template' => 'regions/_list',
              'submenu' => '',
              'coverimg' => '0',
              'domain' => NULL,
              'theme' => NULL,
              'view_template' => NULL,
              'view_layout' => NULL,
              'prefix_icon' => NULL,
              'model_conditions' => NULL,
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '91',
                'name' => '行业解决方案',
                'parent_id' => '31',
                'left' => '19',
                'right' => '20',
                'slug' => '',
                'visible' => '1',
                'description' => '',
                'seotitle' => '',
                'seokeywords' => '',
                'seodescription' => '',
                'link' => '',
                'updated' => '2012-09-04 22:44:31',
                'created' => '2012-08-05 21:15:42',
                'content' => '<p>
	http://www.topwom.com/</p>
<p>
	&nbsp;</p>
<p>
	&nbsp;</p>
<p>
	http://www.topwom.com/</p>
',
                'locale' => 'zh_cn',
                'model' => '',
                'template' => 'regions/_list',
                'submenu' => '',
                'coverimg' => '0',
                'domain' => NULL,
                'theme' => NULL,
                'view_template' => NULL,
                'view_layout' => NULL,
                'prefix_icon' => NULL,
                'model_conditions' => NULL,
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          1 => 
          array (
            'Category' => 
            array (
              'id' => '92',
              'name' => '产品工程解决方案',
              'parent_id' => '31',
              'left' => '21',
              'right' => '22',
              'slug' => 'linkus',
              'visible' => '1',
              'description' => '',
              'seotitle' => '',
              'seokeywords' => '',
              'seodescription' => '',
              'link' => '',
              'updated' => '2012-09-04 22:44:47',
              'created' => '2012-08-05 21:17:56',
              'content' => '<p>
	　新浪科技讯 北京时间8月3日上午消息，Facebook今天推出了一个名为Facebook Stories的全新网站，鼓励用户分享使用Facebook完成的那些特别的故事。</p>
<p>
	　　Stories是一个独立于Facebook的网站。在Stories网站上提交故事，用户必须先在自己的Facebook账户接受一个Facebook应用授权。Stories每月会确立一个主题，讲述不同的关于社交网络如何影响个人生活的故事。</p>
',
              'locale' => 'zh_cn',
              'model' => '',
              'template' => 'regions/_list',
              'submenu' => '',
              'coverimg' => '0',
              'domain' => NULL,
              'theme' => NULL,
              'view_template' => NULL,
              'view_layout' => NULL,
              'prefix_icon' => NULL,
              'model_conditions' => NULL,
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '92',
                'name' => '产品工程解决方案',
                'parent_id' => '31',
                'left' => '21',
                'right' => '22',
                'slug' => 'linkus',
                'visible' => '1',
                'description' => '',
                'seotitle' => '',
                'seokeywords' => '',
                'seodescription' => '',
                'link' => '',
                'updated' => '2012-09-04 22:44:47',
                'created' => '2012-08-05 21:17:56',
                'content' => '<p>
	　新浪科技讯 北京时间8月3日上午消息，Facebook今天推出了一个名为Facebook Stories的全新网站，鼓励用户分享使用Facebook完成的那些特别的故事。</p>
<p>
	　　Stories是一个独立于Facebook的网站。在Stories网站上提交故事，用户必须先在自己的Facebook账户接受一个Facebook应用授权。Stories每月会确立一个主题，讲述不同的关于社交网络如何影响个人生活的故事。</p>
',
                'locale' => 'zh_cn',
                'model' => '',
                'template' => 'regions/_list',
                'submenu' => '',
                'coverimg' => '0',
                'domain' => NULL,
                'theme' => NULL,
                'view_template' => NULL,
                'view_layout' => NULL,
                'prefix_icon' => NULL,
                'model_conditions' => NULL,
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
        ),
      ),
      1 => 
      array (
        'Category' => 
        array (
          'id' => '90',
          'name' => '房地产资讯',
          'parent_id' => '110',
          'left' => '24',
          'right' => '39',
          'slug' => 'estate',
          'visible' => '1',
          'description' => '',
          'seotitle' => '',
          'seokeywords' => '',
          'seodescription' => '',
          'link' => '',
          'updated' => '2013-03-13 21:45:32',
          'created' => '2012-01-28 20:34:43',
          'content' => '',
          'locale' => 'zh_cn',
          'model' => 'EstateArticle',
          'template' => 'categories/_list',
          'submenu' => '',
          'coverimg' => '0',
          'domain' => NULL,
          'theme' => NULL,
          'view_template' => NULL,
          'view_layout' => NULL,
          'prefix_icon' => NULL,
          'model_conditions' => NULL,
          'is_index' => '0',
        ),
        'Uploadfile' => 
        array (
        ),
        'CategoryI18n' => 
        array (
          'zh_cn' => 
          array (
            'id' => '90',
            'name' => '房地产资讯',
            'parent_id' => '110',
            'left' => '24',
            'right' => '39',
            'slug' => 'estate',
            'visible' => '1',
            'description' => '',
            'seotitle' => '',
            'seokeywords' => '',
            'seodescription' => '',
            'link' => '',
            'updated' => '2013-03-13 21:45:32',
            'created' => '2012-01-28 20:34:43',
            'content' => '',
            'locale' => 'zh_cn',
            'model' => 'EstateArticle',
            'template' => 'categories/_list',
            'submenu' => '',
            'coverimg' => '0',
            'domain' => NULL,
            'theme' => NULL,
            'view_template' => NULL,
            'view_layout' => NULL,
            'prefix_icon' => NULL,
            'model_conditions' => NULL,
            'is_index' => '0',
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
          0 => 
          array (
            'Category' => 
            array (
              'id' => '102',
              'name' => '经济环境',
              'parent_id' => '90',
              'left' => '25',
              'right' => '26',
              'slug' => 'estate_economic_environment',
              'visible' => '1',
              'description' => '',
              'seotitle' => NULL,
              'seokeywords' => NULL,
              'seodescription' => NULL,
              'link' => '',
              'updated' => '2012-08-31 10:06:33',
              'created' => '2012-08-31 10:06:12',
              'content' => '',
              'locale' => 'zh_cn',
              'model' => 'EstateArticle',
              'template' => 'regions/_list',
              'submenu' => NULL,
              'coverimg' => '0',
              'domain' => NULL,
              'theme' => NULL,
              'view_template' => NULL,
              'view_layout' => NULL,
              'prefix_icon' => NULL,
              'model_conditions' => NULL,
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '102',
                'name' => '经济环境',
                'parent_id' => '90',
                'left' => '25',
                'right' => '26',
                'slug' => 'estate_economic_environment',
                'visible' => '1',
                'description' => '',
                'seotitle' => NULL,
                'seokeywords' => NULL,
                'seodescription' => NULL,
                'link' => '',
                'updated' => '2012-08-31 10:06:33',
                'created' => '2012-08-31 10:06:12',
                'content' => '',
                'locale' => 'zh_cn',
                'model' => 'EstateArticle',
                'template' => 'regions/_list',
                'submenu' => NULL,
                'coverimg' => '0',
                'domain' => NULL,
                'theme' => NULL,
                'view_template' => NULL,
                'view_layout' => NULL,
                'prefix_icon' => NULL,
                'model_conditions' => NULL,
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          1 => 
          array (
            'Category' => 
            array (
              'id' => '103',
              'name' => '政策',
              'parent_id' => '90',
              'left' => '27',
              'right' => '28',
              'slug' => 'estate_policy',
              'visible' => '1',
              'description' => '',
              'seotitle' => NULL,
              'seokeywords' => NULL,
              'seodescription' => NULL,
              'link' => '',
              'updated' => '2012-08-31 10:07:43',
              'created' => '2012-08-31 10:07:43',
              'content' => '',
              'locale' => 'zh_cn',
              'model' => 'EstateArticle',
              'template' => 'regions/_list',
              'submenu' => NULL,
              'coverimg' => '0',
              'domain' => NULL,
              'theme' => NULL,
              'view_template' => NULL,
              'view_layout' => NULL,
              'prefix_icon' => NULL,
              'model_conditions' => NULL,
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '103',
                'name' => '政策',
                'parent_id' => '90',
                'left' => '27',
                'right' => '28',
                'slug' => 'estate_policy',
                'visible' => '1',
                'description' => '',
                'seotitle' => NULL,
                'seokeywords' => NULL,
                'seodescription' => NULL,
                'link' => '',
                'updated' => '2012-08-31 10:07:43',
                'created' => '2012-08-31 10:07:43',
                'content' => '',
                'locale' => 'zh_cn',
                'model' => 'EstateArticle',
                'template' => 'regions/_list',
                'submenu' => NULL,
                'coverimg' => '0',
                'domain' => NULL,
                'theme' => NULL,
                'view_template' => NULL,
                'view_layout' => NULL,
                'prefix_icon' => NULL,
                'model_conditions' => NULL,
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          2 => 
          array (
            'Category' => 
            array (
              'id' => '104',
              'name' => '城市建设',
              'parent_id' => '90',
              'left' => '29',
              'right' => '30',
              'slug' => 'estate_urban_construction',
              'visible' => '1',
              'description' => '',
              'seotitle' => NULL,
              'seokeywords' => NULL,
              'seodescription' => NULL,
              'link' => '',
              'updated' => '2012-08-31 10:08:46',
              'created' => '2012-08-31 10:08:46',
              'content' => '',
              'locale' => 'zh_cn',
              'model' => 'EstateArticle',
              'template' => 'regions/_list',
              'submenu' => NULL,
              'coverimg' => '0',
              'domain' => NULL,
              'theme' => NULL,
              'view_template' => NULL,
              'view_layout' => NULL,
              'prefix_icon' => NULL,
              'model_conditions' => NULL,
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '104',
                'name' => '城市建设',
                'parent_id' => '90',
                'left' => '29',
                'right' => '30',
                'slug' => 'estate_urban_construction',
                'visible' => '1',
                'description' => '',
                'seotitle' => NULL,
                'seokeywords' => NULL,
                'seodescription' => NULL,
                'link' => '',
                'updated' => '2012-08-31 10:08:46',
                'created' => '2012-08-31 10:08:46',
                'content' => '',
                'locale' => 'zh_cn',
                'model' => 'EstateArticle',
                'template' => 'regions/_list',
                'submenu' => NULL,
                'coverimg' => '0',
                'domain' => NULL,
                'theme' => NULL,
                'view_template' => NULL,
                'view_layout' => NULL,
                'prefix_icon' => NULL,
                'model_conditions' => NULL,
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          3 => 
          array (
            'Category' => 
            array (
              'id' => '105',
              'name' => '市场动态',
              'parent_id' => '90',
              'left' => '31',
              'right' => '32',
              'slug' => 'estate_market_dynamics',
              'visible' => '1',
              'description' => '',
              'seotitle' => NULL,
              'seokeywords' => NULL,
              'seodescription' => NULL,
              'link' => '',
              'updated' => '2012-08-31 10:15:37',
              'created' => '2012-08-31 10:15:37',
              'content' => '',
              'locale' => 'zh_cn',
              'model' => 'EstateArticle',
              'template' => 'regions/_list',
              'submenu' => NULL,
              'coverimg' => '0',
              'domain' => NULL,
              'theme' => NULL,
              'view_template' => NULL,
              'view_layout' => NULL,
              'prefix_icon' => NULL,
              'model_conditions' => NULL,
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '105',
                'name' => '市场动态',
                'parent_id' => '90',
                'left' => '31',
                'right' => '32',
                'slug' => 'estate_market_dynamics',
                'visible' => '1',
                'description' => '',
                'seotitle' => NULL,
                'seokeywords' => NULL,
                'seodescription' => NULL,
                'link' => '',
                'updated' => '2012-08-31 10:15:37',
                'created' => '2012-08-31 10:15:37',
                'content' => '',
                'locale' => 'zh_cn',
                'model' => 'EstateArticle',
                'template' => 'regions/_list',
                'submenu' => NULL,
                'coverimg' => '0',
                'domain' => NULL,
                'theme' => NULL,
                'view_template' => NULL,
                'view_layout' => NULL,
                'prefix_icon' => NULL,
                'model_conditions' => NULL,
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          4 => 
          array (
            'Category' => 
            array (
              'id' => '106',
              'name' => '企业',
              'parent_id' => '90',
              'left' => '33',
              'right' => '34',
              'slug' => 'estate_enterprise',
              'visible' => '1',
              'description' => '',
              'seotitle' => NULL,
              'seokeywords' => NULL,
              'seodescription' => NULL,
              'link' => '',
              'updated' => '2012-08-31 10:16:45',
              'created' => '2012-08-31 10:16:45',
              'content' => '',
              'locale' => 'zh_cn',
              'model' => 'EstateArticle',
              'template' => 'regions/_list',
              'submenu' => NULL,
              'coverimg' => '0',
              'domain' => NULL,
              'theme' => NULL,
              'view_template' => NULL,
              'view_layout' => NULL,
              'prefix_icon' => NULL,
              'model_conditions' => NULL,
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '106',
                'name' => '企业',
                'parent_id' => '90',
                'left' => '33',
                'right' => '34',
                'slug' => 'estate_enterprise',
                'visible' => '1',
                'description' => '',
                'seotitle' => NULL,
                'seokeywords' => NULL,
                'seodescription' => NULL,
                'link' => '',
                'updated' => '2012-08-31 10:16:45',
                'created' => '2012-08-31 10:16:45',
                'content' => '',
                'locale' => 'zh_cn',
                'model' => 'EstateArticle',
                'template' => 'regions/_list',
                'submenu' => NULL,
                'coverimg' => '0',
                'domain' => NULL,
                'theme' => NULL,
                'view_template' => NULL,
                'view_layout' => NULL,
                'prefix_icon' => NULL,
                'model_conditions' => NULL,
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          5 => 
          array (
            'Category' => 
            array (
              'id' => '107',
              'name' => '热点项目',
              'parent_id' => '90',
              'left' => '35',
              'right' => '36',
              'slug' => 'estate_hot_project',
              'visible' => '1',
              'description' => '',
              'seotitle' => NULL,
              'seokeywords' => NULL,
              'seodescription' => NULL,
              'link' => '',
              'updated' => '2012-08-31 10:17:20',
              'created' => '2012-08-31 10:17:20',
              'content' => '',
              'locale' => 'zh_cn',
              'model' => 'EstateArticle',
              'template' => 'regions/_list',
              'submenu' => NULL,
              'coverimg' => '0',
              'domain' => NULL,
              'theme' => NULL,
              'view_template' => NULL,
              'view_layout' => NULL,
              'prefix_icon' => NULL,
              'model_conditions' => NULL,
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '107',
                'name' => '热点项目',
                'parent_id' => '90',
                'left' => '35',
                'right' => '36',
                'slug' => 'estate_hot_project',
                'visible' => '1',
                'description' => '',
                'seotitle' => NULL,
                'seokeywords' => NULL,
                'seodescription' => NULL,
                'link' => '',
                'updated' => '2012-08-31 10:17:20',
                'created' => '2012-08-31 10:17:20',
                'content' => '',
                'locale' => 'zh_cn',
                'model' => 'EstateArticle',
                'template' => 'regions/_list',
                'submenu' => NULL,
                'coverimg' => '0',
                'domain' => NULL,
                'theme' => NULL,
                'view_template' => NULL,
                'view_layout' => NULL,
                'prefix_icon' => NULL,
                'model_conditions' => NULL,
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          6 => 
          array (
            'Category' => 
            array (
              'id' => '108',
              'name' => '业界观点',
              'parent_id' => '90',
              'left' => '37',
              'right' => '38',
              'slug' => 'estate_point_view',
              'visible' => '1',
              'description' => '',
              'seotitle' => NULL,
              'seokeywords' => NULL,
              'seodescription' => NULL,
              'link' => '',
              'updated' => '2012-08-31 10:17:52',
              'created' => '2012-08-31 10:17:52',
              'content' => '',
              'locale' => 'zh_cn',
              'model' => 'EstateArticle',
              'template' => 'regions/_list',
              'submenu' => NULL,
              'coverimg' => '0',
              'domain' => NULL,
              'theme' => NULL,
              'view_template' => NULL,
              'view_layout' => NULL,
              'prefix_icon' => NULL,
              'model_conditions' => NULL,
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '108',
                'name' => '业界观点',
                'parent_id' => '90',
                'left' => '37',
                'right' => '38',
                'slug' => 'estate_point_view',
                'visible' => '1',
                'description' => '',
                'seotitle' => NULL,
                'seokeywords' => NULL,
                'seodescription' => NULL,
                'link' => '',
                'updated' => '2012-08-31 10:17:52',
                'created' => '2012-08-31 10:17:52',
                'content' => '',
                'locale' => 'zh_cn',
                'model' => 'EstateArticle',
                'template' => 'regions/_list',
                'submenu' => NULL,
                'coverimg' => '0',
                'domain' => NULL,
                'theme' => NULL,
                'view_template' => NULL,
                'view_layout' => NULL,
                'prefix_icon' => NULL,
                'model_conditions' => NULL,
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
        ),
      ),
      2 => 
      array (
        'Category' => 
        array (
          'id' => '93',
          'name' => '新闻',
          'parent_id' => '110',
          'left' => '10',
          'right' => '15',
          'slug' => 'news',
          'visible' => '1',
          'description' => '',
          'seotitle' => '',
          'seokeywords' => '',
          'seodescription' => '',
          'link' => '',
          'updated' => '2014-11-20 17:54:45',
          'created' => '2012-08-11 21:18:18',
          'content' => '',
          'locale' => 'zh_cn',
          'model' => 'Article',
          'template' => 'categories/_list',
          'submenu' => '',
          'coverimg' => '0',
          'domain' => '',
          'theme' => '',
          'view_template' => 'view',
          'view_layout' => '',
          'prefix_icon' => 'fa fa-newspaper-o',
          'model_conditions' => NULL,
          'is_index' => '0',
        ),
        'Uploadfile' => 
        array (
        ),
        'CategoryI18n' => 
        array (
          'zh_cn' => 
          array (
            'id' => '93',
            'name' => '新闻',
            'parent_id' => '110',
            'left' => '10',
            'right' => '15',
            'slug' => 'news',
            'visible' => '1',
            'description' => '',
            'seotitle' => '',
            'seokeywords' => '',
            'seodescription' => '',
            'link' => '',
            'updated' => '2014-11-20 17:54:45',
            'created' => '2012-08-11 21:18:18',
            'content' => '',
            'locale' => 'zh_cn',
            'model' => 'Article',
            'template' => 'categories/_list',
            'submenu' => '',
            'coverimg' => '0',
            'domain' => '',
            'theme' => '',
            'view_template' => 'view',
            'view_layout' => '',
            'prefix_icon' => 'fa fa-newspaper-o',
            'model_conditions' => NULL,
            'is_index' => '0',
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
          0 => 
          array (
            'Category' => 
            array (
              'id' => '94',
              'name' => '公司新闻',
              'parent_id' => '93',
              'left' => '11',
              'right' => '12',
              'slug' => 'company',
              'visible' => '1',
              'description' => '',
              'seotitle' => '',
              'seokeywords' => '',
              'seodescription' => '',
              'link' => '',
              'updated' => '2013-03-13 21:42:38',
              'created' => '2012-08-16 22:45:57',
              'content' => '',
              'locale' => 'zh_cn',
              'model' => 'Article',
              'template' => 'categories/_list',
              'submenu' => '',
              'coverimg' => '0',
              'domain' => NULL,
              'theme' => NULL,
              'view_template' => NULL,
              'view_layout' => NULL,
              'prefix_icon' => NULL,
              'model_conditions' => NULL,
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '94',
                'name' => '公司新闻',
                'parent_id' => '93',
                'left' => '11',
                'right' => '12',
                'slug' => 'company',
                'visible' => '1',
                'description' => '',
                'seotitle' => '',
                'seokeywords' => '',
                'seodescription' => '',
                'link' => '',
                'updated' => '2013-03-13 21:42:38',
                'created' => '2012-08-16 22:45:57',
                'content' => '',
                'locale' => 'zh_cn',
                'model' => 'Article',
                'template' => 'categories/_list',
                'submenu' => '',
                'coverimg' => '0',
                'domain' => NULL,
                'theme' => NULL,
                'view_template' => NULL,
                'view_layout' => NULL,
                'prefix_icon' => NULL,
                'model_conditions' => NULL,
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          1 => 
          array (
            'Category' => 
            array (
              'id' => '95',
              'name' => '行业新闻',
              'parent_id' => '93',
              'left' => '13',
              'right' => '14',
              'slug' => '',
              'visible' => '1',
              'description' => '',
              'seotitle' => NULL,
              'seokeywords' => NULL,
              'seodescription' => NULL,
              'link' => '',
              'updated' => '2012-08-16 22:46:28',
              'created' => '2012-08-16 22:46:28',
              'content' => '',
              'locale' => 'zh_cn',
              'model' => 'Article',
              'template' => 'regions/_list',
              'submenu' => NULL,
              'coverimg' => '0',
              'domain' => NULL,
              'theme' => NULL,
              'view_template' => NULL,
              'view_layout' => NULL,
              'prefix_icon' => NULL,
              'model_conditions' => NULL,
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '95',
                'name' => '行业新闻',
                'parent_id' => '93',
                'left' => '13',
                'right' => '14',
                'slug' => '',
                'visible' => '1',
                'description' => '',
                'seotitle' => NULL,
                'seokeywords' => NULL,
                'seodescription' => NULL,
                'link' => '',
                'updated' => '2012-08-16 22:46:28',
                'created' => '2012-08-16 22:46:28',
                'content' => '',
                'locale' => 'zh_cn',
                'model' => 'Article',
                'template' => 'regions/_list',
                'submenu' => NULL,
                'coverimg' => '0',
                'domain' => NULL,
                'theme' => NULL,
                'view_template' => NULL,
                'view_layout' => NULL,
                'prefix_icon' => NULL,
                'model_conditions' => NULL,
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
        ),
      ),
      3 => 
      array (
        'Category' => 
        array (
          'id' => '114',
          'name' => '功能展示',
          'parent_id' => '110',
          'left' => '40',
          'right' => '71',
          'slug' => 'examples',
          'visible' => '1',
          'description' => '',
          'seotitle' => '',
          'seokeywords' => '',
          'seodescription' => '',
          'link' => '',
          'updated' => '2014-04-13 11:02:30',
          'created' => '2014-04-13 11:02:30',
          'content' => '',
          'locale' => 'zh_cn',
          'model' => 'Category',
          'template' => '',
          'submenu' => '',
          'coverimg' => '',
          'domain' => '',
          'theme' => '',
          'view_template' => '',
          'view_layout' => NULL,
          'prefix_icon' => NULL,
          'model_conditions' => NULL,
          'is_index' => '0',
        ),
        'Uploadfile' => 
        array (
        ),
        'CategoryI18n' => 
        array (
          'zh_cn' => 
          array (
            'id' => '114',
            'name' => '功能展示',
            'parent_id' => '110',
            'left' => '40',
            'right' => '71',
            'slug' => 'examples',
            'visible' => '1',
            'description' => '',
            'seotitle' => '',
            'seokeywords' => '',
            'seodescription' => '',
            'link' => '',
            'updated' => '2014-04-13 11:02:30',
            'created' => '2014-04-13 11:02:30',
            'content' => '',
            'locale' => 'zh_cn',
            'model' => 'Category',
            'template' => '',
            'submenu' => '',
            'coverimg' => '',
            'domain' => '',
            'theme' => '',
            'view_template' => '',
            'view_layout' => NULL,
            'prefix_icon' => NULL,
            'model_conditions' => NULL,
            'is_index' => '0',
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
          0 => 
          array (
            'Category' => 
            array (
              'id' => '100',
              'name' => '音乐在线',
              'parent_id' => '114',
              'left' => '41',
              'right' => '42',
              'slug' => 'download',
              'visible' => '1',
              'description' => '',
              'seotitle' => '',
              'seokeywords' => '',
              'seodescription' => '',
              'link' => '',
              'updated' => '2014-04-13 11:02:55',
              'created' => '2012-08-29 22:21:06',
              'content' => '',
              'locale' => 'zh_cn',
              'model' => 'Download',
              'template' => 'categories/_list',
              'submenu' => '',
              'coverimg' => '0',
              'domain' => '',
              'theme' => '',
              'view_template' => '',
              'view_layout' => NULL,
              'prefix_icon' => NULL,
              'model_conditions' => NULL,
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '100',
                'name' => '音乐在线',
                'parent_id' => '114',
                'left' => '41',
                'right' => '42',
                'slug' => 'download',
                'visible' => '1',
                'description' => '',
                'seotitle' => '',
                'seokeywords' => '',
                'seodescription' => '',
                'link' => '',
                'updated' => '2014-04-13 11:02:55',
                'created' => '2012-08-29 22:21:06',
                'content' => '',
                'locale' => 'zh_cn',
                'model' => 'Download',
                'template' => 'categories/_list',
                'submenu' => '',
                'coverimg' => '0',
                'domain' => '',
                'theme' => '',
                'view_template' => '',
                'view_layout' => NULL,
                'prefix_icon' => NULL,
                'model_conditions' => NULL,
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          1 => 
          array (
            'Category' => 
            array (
              'id' => '101',
              'name' => '产品展示',
              'parent_id' => '114',
              'left' => '45',
              'right' => '48',
              'slug' => 'products',
              'visible' => '1',
              'description' => '',
              'seotitle' => '',
              'seokeywords' => '',
              'seodescription' => '',
              'link' => '',
              'updated' => '2015-01-22 08:02:59',
              'created' => '2012-08-30 14:28:39',
              'content' => '',
              'locale' => 'zh_cn',
              'model' => 'Product',
              'template' => 'products/_photolist',
              'submenu' => '',
              'coverimg' => '0',
              'domain' => '',
              'theme' => '',
              'view_template' => '',
              'view_layout' => 'products',
              'prefix_icon' => '',
              'model_conditions' => '',
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'en_us' => 
              array (
                'id' => '7',
                'name' => '',
                'deleted' => '0',
                'created' => '2015-01-20 18:51:22',
                'updated' => '2015-01-22 08:02:59',
                'locale' => 'en_us',
                'foreign_key' => '101',
                'content' => '',
              ),
              'zh_cn' => 
              array (
                'id' => '101',
                'name' => '产品展示',
                'parent_id' => '114',
                'left' => '45',
                'right' => '48',
                'slug' => 'products',
                'visible' => '1',
                'description' => '',
                'seotitle' => '',
                'seokeywords' => '',
                'seodescription' => '',
                'link' => '',
                'updated' => '2015-01-22 08:02:59',
                'created' => '2012-08-30 14:28:39',
                'content' => '',
                'locale' => 'zh_cn',
                'model' => 'Product',
                'template' => 'products/_photolist',
                'submenu' => '',
                'coverimg' => '0',
                'domain' => '',
                'theme' => '',
                'view_template' => '',
                'view_layout' => 'products',
                'prefix_icon' => '',
                'model_conditions' => '',
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Category' => 
                array (
                  'id' => '126',
                  'name' => 'TEST Cate',
                  'parent_id' => '101',
                  'left' => '46',
                  'right' => '47',
                  'slug' => 'test_products',
                  'visible' => '1',
                  'description' => '',
                  'seotitle' => '',
                  'seokeywords' => '',
                  'seodescription' => '',
                  'link' => '',
                  'updated' => '2015-01-22 11:59:38',
                  'created' => '2015-01-21 20:15:31',
                  'content' => '',
                  'locale' => 'en_us',
                  'model' => 'Product',
                  'template' => 'categories/_list',
                  'submenu' => '',
                  'coverimg' => '',
                  'domain' => '',
                  'theme' => '',
                  'view_template' => '',
                  'view_layout' => '',
                  'prefix_icon' => '',
                  'model_conditions' => '',
                  'is_index' => '0',
                ),
                'Uploadfile' => 
                array (
                ),
                'CategoryI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '9',
                    'name' => '',
                    'deleted' => '0',
                    'created' => '2015-01-21 20:15:31',
                    'updated' => '2015-01-22 11:59:38',
                    'locale' => 'zh_cn',
                    'foreign_key' => '126',
                    'content' => '',
                  ),
                  'en_us' => 
                  array (
                    'id' => '126',
                    'name' => 'TEST Cate',
                    'parent_id' => '101',
                    'left' => '46',
                    'right' => '47',
                    'slug' => 'test_products',
                    'visible' => '1',
                    'description' => '',
                    'seotitle' => '',
                    'seokeywords' => '',
                    'seodescription' => '',
                    'link' => '',
                    'updated' => '2015-01-22 11:59:38',
                    'created' => '2015-01-21 20:15:31',
                    'content' => '',
                    'locale' => 'en_us',
                    'model' => 'Product',
                    'template' => 'categories/_list',
                    'submenu' => '',
                    'coverimg' => '',
                    'domain' => '',
                    'theme' => '',
                    'view_template' => '',
                    'view_layout' => '',
                    'prefix_icon' => '',
                    'model_conditions' => '',
                    'is_index' => '0',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
          2 => 
          array (
            'Category' => 
            array (
              'id' => '109',
              'name' => '图片赏析',
              'parent_id' => '114',
              'left' => '43',
              'right' => '44',
              'slug' => 'photos',
              'visible' => '1',
              'description' => '',
              'seotitle' => '',
              'seokeywords' => '',
              'seodescription' => '',
              'link' => '',
              'updated' => '2015-01-19 16:45:13',
              'created' => '2012-08-31 16:29:48',
              'content' => '',
              'locale' => 'zh_cn',
              'model' => 'Photo',
              'template' => 'categories/_masonry_photo',
              'submenu' => '',
              'coverimg' => '0',
              'domain' => '',
              'theme' => '',
              'view_template' => '',
              'view_layout' => '',
              'prefix_icon' => '',
              'model_conditions' => '',
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'en_us' => 
              array (
                'id' => '6',
                'name' => 'Photos',
                'deleted' => '0',
                'created' => '2015-01-19 16:45:13',
                'updated' => '2015-01-19 16:45:13',
                'locale' => 'en_us',
                'foreign_key' => '109',
                'content' => '',
              ),
              'zh_cn' => 
              array (
                'id' => '109',
                'name' => '图片赏析',
                'parent_id' => '114',
                'left' => '43',
                'right' => '44',
                'slug' => 'photos',
                'visible' => '1',
                'description' => '',
                'seotitle' => '',
                'seokeywords' => '',
                'seodescription' => '',
                'link' => '',
                'updated' => '2015-01-19 16:45:13',
                'created' => '2012-08-31 16:29:48',
                'content' => '',
                'locale' => 'zh_cn',
                'model' => 'Photo',
                'template' => 'categories/_masonry_photo',
                'submenu' => '',
                'coverimg' => '0',
                'domain' => '',
                'theme' => '',
                'view_template' => '',
                'view_layout' => '',
                'prefix_icon' => '',
                'model_conditions' => '',
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          3 => 
          array (
            'Category' => 
            array (
              'id' => '120',
              'name' => '求租求购',
              'parent_id' => '114',
              'left' => '49',
              'right' => '50',
              'slug' => 'want',
              'visible' => '1',
              'description' => '',
              'seotitle' => '',
              'seokeywords' => '',
              'seodescription' => '',
              'link' => '',
              'updated' => '2014-06-01 19:42:44',
              'created' => '2014-06-01 19:42:44',
              'content' => '',
              'locale' => 'zh_cn',
              'model' => 'Want',
              'template' => 'categories/_list',
              'submenu' => '',
              'coverimg' => '',
              'domain' => '',
              'theme' => '',
              'view_template' => '',
              'view_layout' => NULL,
              'prefix_icon' => NULL,
              'model_conditions' => NULL,
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '120',
                'name' => '求租求购',
                'parent_id' => '114',
                'left' => '49',
                'right' => '50',
                'slug' => 'want',
                'visible' => '1',
                'description' => '',
                'seotitle' => '',
                'seokeywords' => '',
                'seodescription' => '',
                'link' => '',
                'updated' => '2014-06-01 19:42:44',
                'created' => '2014-06-01 19:42:44',
                'content' => '',
                'locale' => 'zh_cn',
                'model' => 'Want',
                'template' => 'categories/_list',
                'submenu' => '',
                'coverimg' => '',
                'domain' => '',
                'theme' => '',
                'view_template' => '',
                'view_layout' => NULL,
                'prefix_icon' => NULL,
                'model_conditions' => NULL,
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          4 => 
          array (
            'Category' => 
            array (
              'id' => '121',
              'name' => '出租管理',
              'parent_id' => '114',
              'left' => '51',
              'right' => '52',
              'slug' => 'rent',
              'visible' => '1',
              'description' => '',
              'seotitle' => '',
              'seokeywords' => '',
              'seodescription' => '',
              'link' => '',
              'updated' => '2014-06-01 19:53:15',
              'created' => '2014-06-01 19:53:15',
              'content' => '',
              'locale' => 'zh_cn',
              'model' => 'Rent',
              'template' => 'categories/_list',
              'submenu' => '',
              'coverimg' => '',
              'domain' => '',
              'theme' => '',
              'view_template' => '',
              'view_layout' => NULL,
              'prefix_icon' => NULL,
              'model_conditions' => NULL,
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '121',
                'name' => '出租管理',
                'parent_id' => '114',
                'left' => '51',
                'right' => '52',
                'slug' => 'rent',
                'visible' => '1',
                'description' => '',
                'seotitle' => '',
                'seokeywords' => '',
                'seodescription' => '',
                'link' => '',
                'updated' => '2014-06-01 19:53:15',
                'created' => '2014-06-01 19:53:15',
                'content' => '',
                'locale' => 'zh_cn',
                'model' => 'Rent',
                'template' => 'categories/_list',
                'submenu' => '',
                'coverimg' => '',
                'domain' => '',
                'theme' => '',
                'view_template' => '',
                'view_layout' => NULL,
                'prefix_icon' => NULL,
                'model_conditions' => NULL,
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          5 => 
          array (
            'Category' => 
            array (
              'id' => '122',
              'name' => '二手交易',
              'parent_id' => '114',
              'left' => '53',
              'right' => '54',
              'slug' => 'second_trade',
              'visible' => '1',
              'description' => '',
              'seotitle' => '',
              'seokeywords' => '',
              'seodescription' => '',
              'link' => '',
              'updated' => '2014-06-02 21:34:29',
              'created' => '2014-06-02 21:34:29',
              'content' => '',
              'locale' => 'zh_cn',
              'model' => 'Trade',
              'template' => 'categories/_list',
              'submenu' => '',
              'coverimg' => '',
              'domain' => '',
              'theme' => '',
              'view_template' => '',
              'view_layout' => NULL,
              'prefix_icon' => NULL,
              'model_conditions' => NULL,
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '122',
                'name' => '二手交易',
                'parent_id' => '114',
                'left' => '53',
                'right' => '54',
                'slug' => 'second_trade',
                'visible' => '1',
                'description' => '',
                'seotitle' => '',
                'seokeywords' => '',
                'seodescription' => '',
                'link' => '',
                'updated' => '2014-06-02 21:34:29',
                'created' => '2014-06-02 21:34:29',
                'content' => '',
                'locale' => 'zh_cn',
                'model' => 'Trade',
                'template' => 'categories/_list',
                'submenu' => '',
                'coverimg' => '',
                'domain' => '',
                'theme' => '',
                'view_template' => '',
                'view_layout' => NULL,
                'prefix_icon' => NULL,
                'model_conditions' => NULL,
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          6 => 
          array (
            'Category' => 
            array (
              'id' => '124',
              'name' => '编辑器样式',
              'parent_id' => '114',
              'left' => '55',
              'right' => '56',
              'slug' => '135editor',
              'visible' => '1',
              'description' => '',
              'seotitle' => '',
              'seokeywords' => '',
              'seodescription' => '',
              'link' => '',
              'updated' => '2014-12-14 20:49:55',
              'created' => '2014-08-11 23:44:22',
              'content' => '',
              'locale' => 'zh_cn',
              'model' => 'EditorStyle',
              'template' => 'categories/_list',
              'submenu' => '',
              'coverimg' => '',
              'domain' => '',
              'theme' => '',
              'view_template' => '',
              'view_layout' => '',
              'prefix_icon' => '',
              'model_conditions' => NULL,
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'en_us' => 
              array (
                'id' => '2',
                'name' => '',
                'deleted' => '0',
                'created' => '2014-12-09 10:40:14',
                'updated' => '2014-12-14 20:49:55',
                'locale' => 'en_us',
                'foreign_key' => '124',
                'content' => '',
              ),
              'zh_cn' => 
              array (
                'id' => '124',
                'name' => '编辑器样式',
                'parent_id' => '114',
                'left' => '55',
                'right' => '56',
                'slug' => '135editor',
                'visible' => '1',
                'description' => '',
                'seotitle' => '',
                'seokeywords' => '',
                'seodescription' => '',
                'link' => '',
                'updated' => '2014-12-14 20:49:55',
                'created' => '2014-08-11 23:44:22',
                'content' => '',
                'locale' => 'zh_cn',
                'model' => 'EditorStyle',
                'template' => 'categories/_list',
                'submenu' => '',
                'coverimg' => '',
                'domain' => '',
                'theme' => '',
                'view_template' => '',
                'view_layout' => '',
                'prefix_icon' => '',
                'model_conditions' => NULL,
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          7 => 
          array (
            'Category' => 
            array (
              'id' => '125',
              'name' => '微信图文美化编辑器',
              'parent_id' => '114',
              'left' => '57',
              'right' => '58',
              'slug' => 'beautify_editor',
              'visible' => '1',
              'description' => '',
              'seotitle' => '',
              'seokeywords' => '',
              'seodescription' => '',
              'link' => '',
              'updated' => '2016-07-10 10:05:01',
              'created' => '2014-09-04 15:32:52',
              'content' => '<p>快加油</p>',
              'locale' => 'zh_cn',
              'model' => 'Category',
              'template' => 'categories/_list',
              'submenu' => '',
              'coverimg' => '',
              'domain' => '',
              'theme' => '',
              'view_template' => '',
              'view_layout' => 'false',
              'prefix_icon' => '',
              'model_conditions' => '',
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'en_us' => 
              array (
                'id' => '3',
                'name' => '',
                'deleted' => '0',
                'created' => '2014-12-28 11:59:56',
                'updated' => '2014-12-28 11:59:56',
                'locale' => 'en_us',
                'foreign_key' => '125',
                'content' => '',
              ),
              'zh_cn' => 
              array (
                'id' => '125',
                'name' => '微信图文美化编辑器',
                'parent_id' => '114',
                'left' => '57',
                'right' => '58',
                'slug' => 'beautify_editor',
                'visible' => '1',
                'description' => '',
                'seotitle' => '',
                'seokeywords' => '',
                'seodescription' => '',
                'link' => '',
                'updated' => '2016-07-10 10:05:01',
                'created' => '2014-09-04 15:32:52',
                'content' => '<p>快加油</p>',
                'locale' => 'zh_cn',
                'model' => 'Category',
                'template' => 'categories/_list',
                'submenu' => '',
                'coverimg' => '',
                'domain' => '',
                'theme' => '',
                'view_template' => '',
                'view_layout' => 'false',
                'prefix_icon' => '',
                'model_conditions' => '',
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          8 => 
          array (
            'Category' => 
            array (
              'id' => '127',
              'name' => '使用功能购买',
              'parent_id' => '114',
              'left' => '59',
              'right' => '60',
              'slug' => 'roles',
              'visible' => '1',
              'description' => '',
              'seotitle' => '',
              'seokeywords' => '',
              'seodescription' => '',
              'link' => '',
              'updated' => '2015-05-16 10:08:46',
              'created' => '2015-03-18 11:08:47',
              'content' => '<p><br></p>',
              'locale' => 'en_us',
              'model' => 'Role',
              'template' => 'roles/_list',
              'submenu' => '',
              'coverimg' => '',
              'domain' => '',
              'theme' => '',
              'view_template' => '',
              'view_layout' => '',
              'prefix_icon' => '',
              'model_conditions' => '{"group":1}',
              'is_index' => NULL,
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '11',
                'name' => '',
                'deleted' => '0',
                'created' => '2015-03-18 11:08:47',
                'updated' => '2015-05-16 10:08:46',
                'locale' => 'zh_cn',
                'foreign_key' => '127',
                'content' => '',
              ),
              'en_us' => 
              array (
                'id' => '127',
                'name' => '使用功能购买',
                'parent_id' => '114',
                'left' => '59',
                'right' => '60',
                'slug' => 'roles',
                'visible' => '1',
                'description' => '',
                'seotitle' => '',
                'seokeywords' => '',
                'seodescription' => '',
                'link' => '',
                'updated' => '2015-05-16 10:08:46',
                'created' => '2015-03-18 11:08:47',
                'content' => '<p><br></p>',
                'locale' => 'en_us',
                'model' => 'Role',
                'template' => 'roles/_list',
                'submenu' => '',
                'coverimg' => '',
                'domain' => '',
                'theme' => '',
                'view_template' => '',
                'view_layout' => '',
                'prefix_icon' => '',
                'model_conditions' => '{"group":1}',
                'is_index' => NULL,
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          9 => 
          array (
            'Category' => 
            array (
              'id' => '128',
              'name' => '编辑器插件',
              'parent_id' => '114',
              'left' => '61',
              'right' => '62',
              'slug' => 'open_editor',
              'visible' => '1',
              'description' => '',
              'seotitle' => '',
              'seokeywords' => '',
              'seodescription' => '',
              'link' => '',
              'updated' => '2016-05-12 11:55:33',
              'created' => '2016-05-12 11:55:33',
              'content' => '',
              'locale' => 'en_us',
              'model' => '',
              'template' => 'categories/_list',
              'submenu' => '',
              'coverimg' => '',
              'domain' => '',
              'theme' => '',
              'view_template' => '',
              'view_layout' => '',
              'prefix_icon' => '',
              'model_conditions' => '',
              'is_index' => NULL,
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'en_us' => 
              array (
                'id' => '128',
                'name' => '编辑器插件',
                'parent_id' => '114',
                'left' => '61',
                'right' => '62',
                'slug' => 'open_editor',
                'visible' => '1',
                'description' => '',
                'seotitle' => '',
                'seokeywords' => '',
                'seodescription' => '',
                'link' => '',
                'updated' => '2016-05-12 11:55:33',
                'created' => '2016-05-12 11:55:33',
                'content' => '',
                'locale' => 'en_us',
                'model' => '',
                'template' => 'categories/_list',
                'submenu' => '',
                'coverimg' => '',
                'domain' => '',
                'theme' => '',
                'view_template' => '',
                'view_layout' => '',
                'prefix_icon' => '',
                'model_conditions' => '',
                'is_index' => NULL,
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          10 => 
          array (
            'Category' => 
            array (
              'id' => '129',
              'name' => '图片素材',
              'parent_id' => '114',
              'left' => '63',
              'right' => '64',
              'slug' => 'pictures',
              'visible' => '1',
              'description' => '',
              'seotitle' => '',
              'seokeywords' => '',
              'seodescription' => '',
              'link' => '',
              'updated' => '2016-06-28 06:59:31',
              'created' => '2016-06-24 06:53:30',
              'content' => '',
              'locale' => 'zh_cn',
              'model' => 'Image',
              'template' => 'categories/_masonry_photo',
              'submenu' => '',
              'coverimg' => '',
              'domain' => '',
              'theme' => '',
              'view_template' => 'view_135editor',
              'view_layout' => '',
              'prefix_icon' => '',
              'model_conditions' => '',
              'is_index' => NULL,
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '129',
                'name' => '图片素材',
                'parent_id' => '114',
                'left' => '63',
                'right' => '64',
                'slug' => 'pictures',
                'visible' => '1',
                'description' => '',
                'seotitle' => '',
                'seokeywords' => '',
                'seodescription' => '',
                'link' => '',
                'updated' => '2016-06-28 06:59:31',
                'created' => '2016-06-24 06:53:30',
                'content' => '',
                'locale' => 'zh_cn',
                'model' => 'Image',
                'template' => 'categories/_masonry_photo',
                'submenu' => '',
                'coverimg' => '',
                'domain' => '',
                'theme' => '',
                'view_template' => 'view_135editor',
                'view_layout' => '',
                'prefix_icon' => '',
                'model_conditions' => '',
                'is_index' => NULL,
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          11 => 
          array (
            'Category' => 
            array (
              'id' => '130',
              'name' => '编辑器最新样式',
              'parent_id' => '114',
              'left' => '65',
              'right' => '66',
              'slug' => 'new_matter',
              'visible' => '1',
              'description' => '',
              'seotitle' => '',
              'seokeywords' => '',
              'seodescription' => '',
              'link' => '',
              'updated' => '2016-07-10 13:59:31',
              'created' => '2016-07-10 06:49:55',
              'content' => NULL,
              'locale' => NULL,
              'model' => '',
              'template' => 'categories/_list',
              'submenu' => '',
              'coverimg' => '',
              'domain' => '',
              'theme' => '',
              'view_template' => 'view_135_new_matter',
              'view_layout' => '',
              'prefix_icon' => '',
              'model_conditions' => '',
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          12 => 
          array (
            'Category' => 
            array (
              'id' => '131',
              'name' => '编辑器随机样式',
              'parent_id' => '114',
              'left' => '67',
              'right' => '68',
              'slug' => 'rand_matter',
              'visible' => '1',
              'description' => '',
              'seotitle' => '',
              'seokeywords' => '',
              'seodescription' => '',
              'link' => '',
              'updated' => '2016-07-10 14:08:12',
              'created' => '2016-07-10 06:50:51',
              'content' => '',
              'locale' => NULL,
              'model' => '',
              'template' => 'categories/_list',
              'submenu' => '',
              'coverimg' => '',
              'domain' => '',
              'theme' => '',
              'view_template' => 'view_135_rand_matter',
              'view_layout' => '',
              'prefix_icon' => '',
              'model_conditions' => '',
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
          13 => 
          array (
            'Category' => 
            array (
              'id' => '132',
              'name' => '视频素材',
              'parent_id' => '114',
              'left' => '69',
              'right' => '70',
              'slug' => 'videos',
              'visible' => '1',
              'description' => '',
              'seotitle' => '',
              'seokeywords' => '',
              'seodescription' => '',
              'link' => '',
              'updated' => '2016-07-30 11:11:56',
              'created' => '2016-07-30 11:10:57',
              'content' => NULL,
              'locale' => NULL,
              'model' => 'Video',
              'template' => 'categories/_masonry_photo',
              'submenu' => '',
              'coverimg' => '0',
              'domain' => '',
              'theme' => '',
              'view_template' => '',
              'view_layout' => '',
              'prefix_icon' => '',
              'model_conditions' => '',
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
        ),
      ),
      4 => 
      array (
        'Category' => 
        array (
          'id' => '117',
          'name' => '使用帮助',
          'parent_id' => '110',
          'left' => '72',
          'right' => '73',
          'slug' => 'help',
          'visible' => '1',
          'description' => '',
          'seotitle' => '',
          'seokeywords' => '',
          'seodescription' => '',
          'link' => '',
          'updated' => '2014-04-13 17:36:52',
          'created' => '2014-04-13 15:29:43',
          'content' => '',
          'locale' => 'zh_cn',
          'model' => 'Book',
          'template' => 'categories/_list',
          'submenu' => '',
          'coverimg' => '',
          'domain' => '',
          'theme' => '',
          'view_template' => '',
          'view_layout' => NULL,
          'prefix_icon' => NULL,
          'model_conditions' => NULL,
          'is_index' => '0',
        ),
        'Uploadfile' => 
        array (
        ),
        'CategoryI18n' => 
        array (
          'zh_cn' => 
          array (
            'id' => '117',
            'name' => '使用帮助',
            'parent_id' => '110',
            'left' => '72',
            'right' => '73',
            'slug' => 'help',
            'visible' => '1',
            'description' => '',
            'seotitle' => '',
            'seokeywords' => '',
            'seodescription' => '',
            'link' => '',
            'updated' => '2014-04-13 17:36:52',
            'created' => '2014-04-13 15:29:43',
            'content' => '',
            'locale' => 'zh_cn',
            'model' => 'Book',
            'template' => 'categories/_list',
            'submenu' => '',
            'coverimg' => '',
            'domain' => '',
            'theme' => '',
            'view_template' => '',
            'view_layout' => NULL,
            'prefix_icon' => NULL,
            'model_conditions' => NULL,
            'is_index' => '0',
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
        ),
      ),
      5 => 
      array (
        'Category' => 
        array (
          'id' => '118',
          'name' => '微信公众号',
          'parent_id' => '110',
          'left' => '74',
          'right' => '77',
          'slug' => 'weixin',
          'visible' => '1',
          'description' => '',
          'seotitle' => '',
          'seokeywords' => '',
          'seodescription' => '',
          'link' => '',
          'updated' => '2014-09-02 22:16:37',
          'created' => '2014-04-25 05:34:30',
          'content' => '',
          'locale' => 'zh_cn',
          'model' => 'Wx',
          'template' => 'wxes/_list',
          'submenu' => '',
          'coverimg' => '',
          'domain' => '',
          'theme' => '',
          'view_template' => '',
          'view_layout' => NULL,
          'prefix_icon' => NULL,
          'model_conditions' => NULL,
          'is_index' => '0',
        ),
        'Uploadfile' => 
        array (
        ),
        'CategoryI18n' => 
        array (
          'zh_cn' => 
          array (
            'id' => '118',
            'name' => '微信公众号',
            'parent_id' => '110',
            'left' => '74',
            'right' => '77',
            'slug' => 'weixin',
            'visible' => '1',
            'description' => '',
            'seotitle' => '',
            'seokeywords' => '',
            'seodescription' => '',
            'link' => '',
            'updated' => '2014-09-02 22:16:37',
            'created' => '2014-04-25 05:34:30',
            'content' => '',
            'locale' => 'zh_cn',
            'model' => 'Wx',
            'template' => 'wxes/_list',
            'submenu' => '',
            'coverimg' => '',
            'domain' => '',
            'theme' => '',
            'view_template' => '',
            'view_layout' => NULL,
            'prefix_icon' => NULL,
            'model_conditions' => NULL,
            'is_index' => '0',
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
          0 => 
          array (
            'Category' => 
            array (
              'id' => '119',
              'name' => '公众号推荐',
              'parent_id' => '118',
              'left' => '75',
              'right' => '76',
              'slug' => 'weixin_recommend',
              'visible' => '1',
              'description' => '',
              'seotitle' => '',
              'seokeywords' => '',
              'seodescription' => '',
              'link' => '',
              'updated' => '2014-05-21 11:27:45',
              'created' => '2014-05-21 11:27:45',
              'content' => '',
              'locale' => 'zh_cn',
              'model' => 'Category',
              'template' => 'regions/_singlecontent',
              'submenu' => '',
              'coverimg' => '',
              'domain' => '',
              'theme' => '',
              'view_template' => '',
              'view_layout' => NULL,
              'prefix_icon' => NULL,
              'model_conditions' => NULL,
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '119',
                'name' => '公众号推荐',
                'parent_id' => '118',
                'left' => '75',
                'right' => '76',
                'slug' => 'weixin_recommend',
                'visible' => '1',
                'description' => '',
                'seotitle' => '',
                'seokeywords' => '',
                'seodescription' => '',
                'link' => '',
                'updated' => '2014-05-21 11:27:45',
                'created' => '2014-05-21 11:27:45',
                'content' => '',
                'locale' => 'zh_cn',
                'model' => 'Category',
                'template' => 'regions/_singlecontent',
                'submenu' => '',
                'coverimg' => '',
                'domain' => '',
                'theme' => '',
                'view_template' => '',
                'view_layout' => NULL,
                'prefix_icon' => NULL,
                'model_conditions' => NULL,
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
        ),
      ),
      6 => 
      array (
        'Category' => 
        array (
          'id' => '123',
          'name' => '首页',
          'parent_id' => '110',
          'left' => '16',
          'right' => '17',
          'slug' => 'index',
          'visible' => '0',
          'description' => '',
          'seotitle' => '',
          'seokeywords' => '',
          'seodescription' => '',
          'link' => '',
          'updated' => '2015-04-15 16:32:30',
          'created' => '2014-06-10 21:42:06',
          'content' => '<p><br></p>',
          'locale' => 'zh_cn',
          'model' => '',
          'template' => 'categories/_list',
          'submenu' => '',
          'coverimg' => '',
          'domain' => '',
          'theme' => '',
          'view_template' => '',
          'view_layout' => 'false',
          'prefix_icon' => '',
          'model_conditions' => '',
          'is_index' => '0',
        ),
        'Uploadfile' => 
        array (
          0 => 
          array (
            'id' => '57',
            'data_id' => '123',
            'modelclass' => 'Category',
            'fieldname' => 'banner',
            'sortorder' => '0',
            'created' => '2014-11-20 15:25:39',
            'updated' => '2015-04-15 16:32:30',
            'thumb' => '/files/201411/thumb_s/fbabea53d4e8736dbd5eb9f1.jpg',
            'name' => 'LIBERAL.jpg',
            'fspath' => '/files/201411/fbabea53d4e8736dbd5eb9f1.jpg',
            'type' => 'image/jpeg',
            'size' => '89938',
            'trash' => '0',
            'description' => '',
            'mid_thumb' => '/files/201411/thumb_m/fbabea53d4e8736dbd5eb9f1.jpg',
            'version' => '',
            'user_id' => '0',
            'link' => '',
          ),
          1 => 
          array (
            'id' => '60',
            'data_id' => '123',
            'modelclass' => 'Category',
            'fieldname' => 'banner',
            'sortorder' => '0',
            'created' => '2014-11-20 16:13:05',
            'updated' => '2015-04-15 16:32:30',
            'thumb' => '/files/201411/thumb_s/5ca1d2d0b9213ad79a86ed4f.jpg',
            'name' => '` GLADYS LIU.jpg',
            'fspath' => '/files/201411/5ca1d2d0b9213ad79a86ed4f.jpg',
            'type' => 'image/jpeg',
            'size' => '195418',
            'trash' => '0',
            'description' => '',
            'mid_thumb' => '/files/201411/thumb_m/5ca1d2d0b9213ad79a86ed4f.jpg',
            'version' => '',
            'user_id' => '0',
            'link' => '',
          ),
          2 => 
          array (
            'id' => '61',
            'data_id' => '123',
            'modelclass' => 'Category',
            'fieldname' => 'banner',
            'sortorder' => '0',
            'created' => '2014-11-20 16:13:07',
            'updated' => '2015-04-15 16:32:30',
            'thumb' => '/files/201411/thumb_s/7497da37d857f5fd680cfe17.jpg',
            'name' => '710438.jpg',
            'fspath' => '/files/201411/7497da37d857f5fd680cfe17.jpg',
            'type' => 'image/jpeg',
            'size' => '321214',
            'trash' => '0',
            'description' => '',
            'mid_thumb' => '/files/201411/thumb_m/7497da37d857f5fd680cfe17.jpg',
            'version' => '',
            'user_id' => '0',
            'link' => '',
          ),
        ),
        'CategoryI18n' => 
        array (
          'en_us' => 
          array (
            'id' => '4',
            'name' => '',
            'deleted' => '0',
            'created' => '2015-01-13 22:12:45',
            'updated' => '2015-04-15 16:32:30',
            'locale' => 'en_us',
            'foreign_key' => '123',
            'content' => '',
          ),
          'zh_cn' => 
          array (
            'id' => '123',
            'name' => '首页',
            'parent_id' => '110',
            'left' => '16',
            'right' => '17',
            'slug' => 'index',
            'visible' => '0',
            'description' => '',
            'seotitle' => '',
            'seokeywords' => '',
            'seodescription' => '',
            'link' => '',
            'updated' => '2015-04-15 16:32:30',
            'created' => '2014-06-10 21:42:06',
            'content' => '<p><br></p>',
            'locale' => 'zh_cn',
            'model' => '',
            'template' => 'categories/_list',
            'submenu' => '',
            'coverimg' => '',
            'domain' => '',
            'theme' => '',
            'view_template' => '',
            'view_layout' => 'false',
            'prefix_icon' => '',
            'model_conditions' => '',
            'is_index' => '0',
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
        ),
      ),
    ),
  ),
  1 => 
  array (
    'Category' => 
    array (
      'id' => '111',
      'name' => '默认站点2',
      'parent_id' => NULL,
      'left' => '79',
      'right' => '86',
      'slug' => 'site2',
      'visible' => '1',
      'description' => '',
      'seotitle' => '',
      'seokeywords' => '',
      'seodescription' => '',
      'link' => '/tech.html',
      'updated' => '2013-03-16 12:16:22',
      'created' => '2013-03-13 21:46:00',
      'content' => '',
      'locale' => 'zh_cn',
      'model' => 'website',
      'template' => 'categories/_list',
      'submenu' => '',
      'coverimg' => '/img/logo-1.png',
      'domain' => 'www.mm.com',
      'theme' => '',
      'view_template' => '',
      'view_layout' => NULL,
      'prefix_icon' => NULL,
      'model_conditions' => NULL,
      'is_index' => '0',
    ),
    'Uploadfile' => 
    array (
    ),
    'CategoryI18n' => 
    array (
      'zh_cn' => 
      array (
        'id' => '111',
        'name' => '默认站点2',
        'parent_id' => NULL,
        'left' => '79',
        'right' => '86',
        'slug' => 'site2',
        'visible' => '1',
        'description' => '',
        'seotitle' => '',
        'seokeywords' => '',
        'seodescription' => '',
        'link' => '/tech.html',
        'updated' => '2013-03-16 12:16:22',
        'created' => '2013-03-13 21:46:00',
        'content' => '',
        'locale' => 'zh_cn',
        'model' => 'website',
        'template' => 'categories/_list',
        'submenu' => '',
        'coverimg' => '/img/logo-1.png',
        'domain' => 'www.mm.com',
        'theme' => '',
        'view_template' => '',
        'view_layout' => NULL,
        'prefix_icon' => NULL,
        'model_conditions' => NULL,
        'is_index' => '0',
      ),
    ),
    'Tag' => 
    array (
    ),
    'children' => 
    array (
      0 => 
      array (
        'Category' => 
        array (
          'id' => '1',
          'name' => '购物',
          'parent_id' => '111',
          'left' => '80',
          'right' => '81',
          'slug' => 'taobaoke',
          'visible' => '1',
          'description' => '',
          'seotitle' => '',
          'seokeywords' => '',
          'seodescription' => '',
          'link' => '',
          'updated' => '2013-06-28 00:00:33',
          'created' => '0000-00-00 00:00:00',
          'content' => '',
          'locale' => 'zh_cn',
          'model' => '',
          'template' => '',
          'submenu' => '',
          'coverimg' => '0',
          'domain' => '',
          'theme' => '',
          'view_template' => '',
          'view_layout' => NULL,
          'prefix_icon' => NULL,
          'model_conditions' => NULL,
          'is_index' => '0',
        ),
        'Uploadfile' => 
        array (
        ),
        'CategoryI18n' => 
        array (
          'zh_cn' => 
          array (
            'id' => '1',
            'name' => '购物',
            'parent_id' => '111',
            'left' => '80',
            'right' => '81',
            'slug' => 'taobaoke',
            'visible' => '1',
            'description' => '',
            'seotitle' => '',
            'seokeywords' => '',
            'seodescription' => '',
            'link' => '',
            'updated' => '2013-06-28 00:00:33',
            'created' => '0000-00-00 00:00:00',
            'content' => '',
            'locale' => 'zh_cn',
            'model' => '',
            'template' => '',
            'submenu' => '',
            'coverimg' => '0',
            'domain' => '',
            'theme' => '',
            'view_template' => '',
            'view_layout' => NULL,
            'prefix_icon' => NULL,
            'model_conditions' => NULL,
            'is_index' => '0',
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
        ),
      ),
      1 => 
      array (
        'Category' => 
        array (
          'id' => '112',
          'name' => '站点2新闻',
          'parent_id' => '111',
          'left' => '82',
          'right' => '85',
          'slug' => 'site2_news',
          'visible' => '1',
          'description' => '',
          'seotitle' => '',
          'seokeywords' => '',
          'seodescription' => '',
          'link' => '',
          'updated' => '2013-03-13 21:55:23',
          'created' => '2013-03-13 21:55:23',
          'content' => '',
          'locale' => 'zh_cn',
          'model' => 'Article',
          'template' => 'categories/_list',
          'submenu' => '',
          'coverimg' => '0',
          'domain' => NULL,
          'theme' => NULL,
          'view_template' => NULL,
          'view_layout' => NULL,
          'prefix_icon' => NULL,
          'model_conditions' => NULL,
          'is_index' => '0',
        ),
        'Uploadfile' => 
        array (
        ),
        'CategoryI18n' => 
        array (
          'zh_cn' => 
          array (
            'id' => '112',
            'name' => '站点2新闻',
            'parent_id' => '111',
            'left' => '82',
            'right' => '85',
            'slug' => 'site2_news',
            'visible' => '1',
            'description' => '',
            'seotitle' => '',
            'seokeywords' => '',
            'seodescription' => '',
            'link' => '',
            'updated' => '2013-03-13 21:55:23',
            'created' => '2013-03-13 21:55:23',
            'content' => '',
            'locale' => 'zh_cn',
            'model' => 'Article',
            'template' => 'categories/_list',
            'submenu' => '',
            'coverimg' => '0',
            'domain' => NULL,
            'theme' => NULL,
            'view_template' => NULL,
            'view_layout' => NULL,
            'prefix_icon' => NULL,
            'model_conditions' => NULL,
            'is_index' => '0',
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
          0 => 
          array (
            'Category' => 
            array (
              'id' => '113',
              'name' => '科技新闻',
              'parent_id' => '112',
              'left' => '83',
              'right' => '84',
              'slug' => 'tech',
              'visible' => '1',
              'description' => '',
              'seotitle' => '',
              'seokeywords' => '',
              'seodescription' => '',
              'link' => '',
              'updated' => '2013-03-13 21:58:53',
              'created' => '2013-03-13 21:58:34',
              'content' => '',
              'locale' => 'zh_cn',
              'model' => 'Article',
              'template' => 'categories/_list',
              'submenu' => '',
              'coverimg' => '0',
              'domain' => NULL,
              'theme' => NULL,
              'view_template' => NULL,
              'view_layout' => NULL,
              'prefix_icon' => NULL,
              'model_conditions' => NULL,
              'is_index' => '0',
            ),
            'Uploadfile' => 
            array (
            ),
            'CategoryI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '113',
                'name' => '科技新闻',
                'parent_id' => '112',
                'left' => '83',
                'right' => '84',
                'slug' => 'tech',
                'visible' => '1',
                'description' => '',
                'seotitle' => '',
                'seokeywords' => '',
                'seodescription' => '',
                'link' => '',
                'updated' => '2013-03-13 21:58:53',
                'created' => '2013-03-13 21:58:34',
                'content' => '',
                'locale' => 'zh_cn',
                'model' => 'Article',
                'template' => 'categories/_list',
                'submenu' => '',
                'coverimg' => '0',
                'domain' => NULL,
                'theme' => NULL,
                'view_template' => NULL,
                'view_layout' => NULL,
                'prefix_icon' => NULL,
                'model_conditions' => NULL,
                'is_index' => '0',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  2 => 
  array (
    'Category' => 
    array (
      'id' => '115',
      'name' => '关于我们',
      'parent_id' => NULL,
      'left' => '87',
      'right' => '88',
      'slug' => 'about-us',
      'visible' => '1',
      'description' => '',
      'seotitle' => '',
      'seokeywords' => '',
      'seodescription' => '',
      'link' => '',
      'updated' => '2014-04-13 11:06:00',
      'created' => '2014-04-13 11:06:00',
      'content' => '',
      'locale' => 'zh_cn',
      'model' => 'Category',
      'template' => 'regions/_singlecontent',
      'submenu' => '',
      'coverimg' => '',
      'domain' => '',
      'theme' => '',
      'view_template' => '',
      'view_layout' => NULL,
      'prefix_icon' => NULL,
      'model_conditions' => NULL,
      'is_index' => '0',
    ),
    'Uploadfile' => 
    array (
    ),
    'CategoryI18n' => 
    array (
      'zh_cn' => 
      array (
        'id' => '115',
        'name' => '关于我们',
        'parent_id' => NULL,
        'left' => '87',
        'right' => '88',
        'slug' => 'about-us',
        'visible' => '1',
        'description' => '',
        'seotitle' => '',
        'seokeywords' => '',
        'seodescription' => '',
        'link' => '',
        'updated' => '2014-04-13 11:06:00',
        'created' => '2014-04-13 11:06:00',
        'content' => '',
        'locale' => 'zh_cn',
        'model' => 'Category',
        'template' => 'regions/_singlecontent',
        'submenu' => '',
        'coverimg' => '',
        'domain' => '',
        'theme' => '',
        'view_template' => '',
        'view_layout' => NULL,
        'prefix_icon' => NULL,
        'model_conditions' => NULL,
        'is_index' => '0',
      ),
    ),
    'Tag' => 
    array (
    ),
    'children' => 
    array (
    ),
  ),
  3 => 
  array (
    'Category' => 
    array (
      'id' => '116',
      'name' => '联系我们',
      'parent_id' => NULL,
      'left' => '89',
      'right' => '90',
      'slug' => 'contact-us',
      'visible' => '1',
      'description' => '',
      'seotitle' => '',
      'seokeywords' => '',
      'seodescription' => '',
      'link' => '',
      'updated' => '2014-04-13 11:07:52',
      'created' => '2014-04-13 11:07:52',
      'content' => '',
      'locale' => NULL,
      'model' => 'Category',
      'template' => 'regions/_singlecontent',
      'submenu' => '',
      'coverimg' => '',
      'domain' => '',
      'theme' => '',
      'view_template' => '',
      'view_layout' => NULL,
      'prefix_icon' => NULL,
      'model_conditions' => NULL,
      'is_index' => '0',
    ),
    'Uploadfile' => 
    array (
    ),
    'CategoryI18n' => 
    array (
    ),
    'Tag' => 
    array (
    ),
    'children' => 
    array (
    ),
  ),
);


	saveTreeItems($datas,'Category');
