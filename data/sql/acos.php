<?php
$datas = array (
  0 => 
  array (
    'Aco' => 
    array (
      'id' => '1',
      'parent_id' => NULL,
      'model' => NULL,
      'foreign_key' => NULL,
      'alias' => 'controllers',
      'lft' => '9',
      'rght' => '82',
      'name' => '模块操作',
    ),
    'children' => 
    array (
      0 => 
      array (
        'Aco' => 
        array (
          'id' => '71',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Categories',
          'lft' => '10',
          'rght' => '11',
          'name' => '栏目管理',
        ),
        'children' => 
        array (
        ),
      ),
      1 => 
      array (
        'Aco' => 
        array (
          'id' => '111',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Crawls',
          'lft' => '14',
          'rght' => '15',
          'name' => 'Crawls',
        ),
        'children' => 
        array (
        ),
      ),
      2 => 
      array (
        'Aco' => 
        array (
          'id' => '124',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'I18nfields',
          'lft' => '16',
          'rght' => '17',
          'name' => 'I18nfields',
        ),
        'children' => 
        array (
        ),
      ),
      3 => 
      array (
        'Aco' => 
        array (
          'id' => '138',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Keywords',
          'lft' => '18',
          'rght' => '19',
          'name' => 'Keywords',
        ),
        'children' => 
        array (
        ),
      ),
      4 => 
      array (
        'Aco' => 
        array (
          'id' => '149',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Languages',
          'lft' => '20',
          'rght' => '21',
          'name' => 'Languages',
        ),
        'children' => 
        array (
        ),
      ),
      5 => 
      array (
        'Aco' => 
        array (
          'id' => '162',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Menus',
          'lft' => '22',
          'rght' => '23',
          'name' => 'Menus',
        ),
        'children' => 
        array (
        ),
      ),
      6 => 
      array (
        'Aco' => 
        array (
          'id' => '174',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Products',
          'lft' => '24',
          'rght' => '25',
          'name' => 'Products',
        ),
        'children' => 
        array (
        ),
      ),
      7 => 
      array (
        'Aco' => 
        array (
          'id' => '185',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Regions',
          'lft' => '26',
          'rght' => '27',
          'name' => 'Regions',
        ),
        'children' => 
        array (
        ),
      ),
      8 => 
      array (
        'Aco' => 
        array (
          'id' => '199',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Roles',
          'lft' => '28',
          'rght' => '29',
          'name' => 'Roles',
        ),
        'children' => 
        array (
        ),
      ),
      9 => 
      array (
        'Aco' => 
        array (
          'id' => '210',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Settings',
          'lft' => '30',
          'rght' => '31',
          'name' => 'Settings',
        ),
        'children' => 
        array (
        ),
      ),
      10 => 
      array (
        'Aco' => 
        array (
          'id' => '225',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Staffs',
          'lft' => '32',
          'rght' => '33',
          'name' => 'Staffs',
        ),
        'children' => 
        array (
        ),
      ),
      11 => 
      array (
        'Aco' => 
        array (
          'id' => '241',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Stylevars',
          'lft' => '34',
          'rght' => '35',
          'name' => 'Stylevars',
        ),
        'children' => 
        array (
        ),
      ),
      12 => 
      array (
        'Aco' => 
        array (
          'id' => '255',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Tools',
          'lft' => '36',
          'rght' => '37',
          'name' => 'Tools',
        ),
        'children' => 
        array (
        ),
      ),
      13 => 
      array (
        'Aco' => 
        array (
          'id' => '278',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Uploadfiles',
          'lft' => '38',
          'rght' => '39',
          'name' => 'Uploadfiles',
        ),
        'children' => 
        array (
        ),
      ),
      14 => 
      array (
        'Aco' => 
        array (
          'id' => '296',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Filemanager',
          'lft' => '40',
          'rght' => '41',
          'name' => 'Filemanager',
        ),
        'children' => 
        array (
        ),
      ),
      15 => 
      array (
        'Aco' => 
        array (
          'id' => '297',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'AclActions',
          'lft' => '42',
          'rght' => '43',
          'name' => 'AclActions',
        ),
        'children' => 
        array (
        ),
      ),
      16 => 
      array (
        'Aco' => 
        array (
          'id' => '310',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'AclAros',
          'lft' => '44',
          'rght' => '45',
          'name' => 'AclAros',
        ),
        'children' => 
        array (
        ),
      ),
      17 => 
      array (
        'Aco' => 
        array (
          'id' => '311',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'AclPermissions',
          'lft' => '46',
          'rght' => '47',
          'name' => 'AclPermissions',
        ),
        'children' => 
        array (
        ),
      ),
      18 => 
      array (
        'Aco' => 
        array (
          'id' => '312',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Flowsteps',
          'lft' => '48',
          'rght' => '49',
          'name' => 'Flowsteps',
        ),
        'children' => 
        array (
        ),
      ),
      19 => 
      array (
        'Aco' => 
        array (
          'id' => '354',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Articles',
          'lft' => '12',
          'rght' => '13',
          'name' => '文章管理',
        ),
        'children' => 
        array (
        ),
      ),
      20 => 
      array (
        'Aco' => 
        array (
          'id' => '370',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Projects',
          'lft' => '50',
          'rght' => '51',
          'name' => NULL,
        ),
        'children' => 
        array (
        ),
      ),
      21 => 
      array (
        'Aco' => 
        array (
          'id' => '371',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Contacts',
          'lft' => '52',
          'rght' => '53',
          'name' => NULL,
        ),
        'children' => 
        array (
        ),
      ),
      22 => 
      array (
        'Aco' => 
        array (
          'id' => '372',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Customers',
          'lft' => '54',
          'rght' => '55',
          'name' => NULL,
        ),
        'children' => 
        array (
        ),
      ),
      23 => 
      array (
        'Aco' => 
        array (
          'id' => '374',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'JobExperiences',
          'lft' => '56',
          'rght' => '57',
          'name' => NULL,
        ),
        'children' => 
        array (
        ),
      ),
      24 => 
      array (
        'Aco' => 
        array (
          'id' => '375',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'TaskExecutes',
          'lft' => '58',
          'rght' => '59',
          'name' => NULL,
        ),
        'children' => 
        array (
        ),
      ),
      25 => 
      array (
        'Aco' => 
        array (
          'id' => '376',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Tasks',
          'lft' => '60',
          'rght' => '61',
          'name' => NULL,
        ),
        'children' => 
        array (
        ),
      ),
      26 => 
      array (
        'Aco' => 
        array (
          'id' => '393',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Wxes',
          'lft' => '62',
          'rght' => '63',
          'name' => NULL,
        ),
        'children' => 
        array (
        ),
      ),
      27 => 
      array (
        'Aco' => 
        array (
          'id' => '394',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'WxVariables',
          'lft' => '64',
          'rght' => '65',
          'name' => NULL,
        ),
        'children' => 
        array (
        ),
      ),
      28 => 
      array (
        'Aco' => 
        array (
          'id' => '395',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'WxMsgs',
          'lft' => '66',
          'rght' => '67',
          'name' => NULL,
        ),
        'children' => 
        array (
        ),
      ),
      29 => 
      array (
        'Aco' => 
        array (
          'id' => '396',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Uploadfiles',
          'lft' => '68',
          'rght' => '69',
          'name' => NULL,
        ),
        'children' => 
        array (
        ),
      ),
      30 => 
      array (
        'Aco' => 
        array (
          'id' => '397',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Partners',
          'lft' => '70',
          'rght' => '71',
          'name' => NULL,
        ),
        'children' => 
        array (
        ),
      ),
      31 => 
      array (
        'Aco' => 
        array (
          'id' => '398',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'WxMenus',
          'lft' => '72',
          'rght' => '73',
          'name' => NULL,
        ),
        'children' => 
        array (
        ),
      ),
      32 => 
      array (
        'Aco' => 
        array (
          'id' => '399',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Companies',
          'lft' => '74',
          'rght' => '75',
          'name' => NULL,
        ),
        'children' => 
        array (
        ),
      ),
      33 => 
      array (
        'Aco' => 
        array (
          'id' => '400',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Shortmessages',
          'lft' => '76',
          'rght' => '77',
          'name' => NULL,
        ),
        'children' => 
        array (
        ),
      ),
      34 => 
      array (
        'Aco' => 
        array (
          'id' => '401',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'Qrcodes',
          'lft' => '78',
          'rght' => '79',
          'name' => NULL,
        ),
        'children' => 
        array (
        ),
      ),
      35 => 
      array (
        'Aco' => 
        array (
          'id' => '402',
          'parent_id' => '1',
          'model' => NULL,
          'foreign_key' => '0',
          'alias' => 'WxAutoReplies',
          'lft' => '80',
          'rght' => '81',
          'name' => NULL,
        ),
        'children' => 
        array (
        ),
      ),
    ),
  ),
  1 => 
  array (
    'Aco' => 
    array (
      'id' => '377',
      'parent_id' => NULL,
      'model' => NULL,
      'foreign_key' => '0',
      'alias' => 'Flowsteps',
      'lft' => '83',
      'rght' => '84',
      'name' => NULL,
    ),
    'children' => 
    array (
    ),
  ),
  2 => 
  array (
    'Aco' => 
    array (
      'id' => '379',
      'parent_id' => NULL,
      'model' => 'Flowstep',
      'foreign_key' => '9',
      'alias' => NULL,
      'lft' => '5',
      'rght' => '6',
      'name' => NULL,
    ),
    'children' => 
    array (
    ),
  ),
  3 => 
  array (
    'Aco' => 
    array (
      'id' => '380',
      'parent_id' => NULL,
      'model' => 'Flowstep',
      'foreign_key' => '10',
      'alias' => NULL,
      'lft' => '7',
      'rght' => '8',
      'name' => NULL,
    ),
    'children' => 
    array (
    ),
  ),
  4 => 
  array (
    'Aco' => 
    array (
      'id' => '382',
      'parent_id' => NULL,
      'model' => 'Flowstep',
      'foreign_key' => '12',
      'alias' => NULL,
      'lft' => '1',
      'rght' => '2',
      'name' => NULL,
    ),
    'children' => 
    array (
    ),
  ),
  5 => 
  array (
    'Aco' => 
    array (
      'id' => '383',
      'parent_id' => NULL,
      'model' => 'Flowstep',
      'foreign_key' => '13',
      'alias' => NULL,
      'lft' => '3',
      'rght' => '4',
      'name' => NULL,
    ),
    'children' => 
    array (
    ),
  ),
  6 => 
  array (
    'Aco' => 
    array (
      'id' => '385',
      'parent_id' => NULL,
      'model' => 'Flowstep',
      'foreign_key' => '15',
      'alias' => NULL,
      'lft' => '85',
      'rght' => '86',
      'name' => NULL,
    ),
    'children' => 
    array (
    ),
  ),
  7 => 
  array (
    'Aco' => 
    array (
      'id' => '386',
      'parent_id' => NULL,
      'model' => 'Flowstep',
      'foreign_key' => '16',
      'alias' => NULL,
      'lft' => '87',
      'rght' => '88',
      'name' => NULL,
    ),
    'children' => 
    array (
    ),
  ),
  8 => 
  array (
    'Aco' => 
    array (
      'id' => '387',
      'parent_id' => NULL,
      'model' => 'Flowstep',
      'foreign_key' => '17',
      'alias' => NULL,
      'lft' => '89',
      'rght' => '90',
      'name' => NULL,
    ),
    'children' => 
    array (
    ),
  ),
  9 => 
  array (
    'Aco' => 
    array (
      'id' => '388',
      'parent_id' => NULL,
      'model' => 'Flowstep',
      'foreign_key' => '18',
      'alias' => NULL,
      'lft' => '91',
      'rght' => '92',
      'name' => NULL,
    ),
    'children' => 
    array (
    ),
  ),
  10 => 
  array (
    'Aco' => 
    array (
      'id' => '389',
      'parent_id' => NULL,
      'model' => 'Flowstep',
      'foreign_key' => '19',
      'alias' => NULL,
      'lft' => '93',
      'rght' => '94',
      'name' => NULL,
    ),
    'children' => 
    array (
    ),
  ),
  11 => 
  array (
    'Aco' => 
    array (
      'id' => '390',
      'parent_id' => NULL,
      'model' => 'Flowstep',
      'foreign_key' => '20',
      'alias' => NULL,
      'lft' => '95',
      'rght' => '96',
      'name' => NULL,
    ),
    'children' => 
    array (
    ),
  ),
  12 => 
  array (
    'Aco' => 
    array (
      'id' => '391',
      'parent_id' => NULL,
      'model' => 'Flowstep',
      'foreign_key' => '21',
      'alias' => NULL,
      'lft' => '97',
      'rght' => '98',
      'name' => NULL,
    ),
    'children' => 
    array (
    ),
  ),
  13 => 
  array (
    'Aco' => 
    array (
      'id' => '392',
      'parent_id' => NULL,
      'model' => 'Flowstep',
      'foreign_key' => '22',
      'alias' => NULL,
      'lft' => '99',
      'rght' => '100',
      'name' => NULL,
    ),
    'children' => 
    array (
    ),
  ),
);


	saveTreeItems($datas,'Aco');
