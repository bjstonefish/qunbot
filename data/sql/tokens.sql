set names utf8;
CREATE TABLE IF NOT EXISTS `miao_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(32) DEFAULT '',
  `ecrypt_key` varchar(44) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `remark` varchar(200) DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`),
  KEY `creator` (`creator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'remark', '1', '备注', 'string', 'Token', 'zh_cn', '200', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2016-03-29 13:38:39', '2016-03-29 13:38:39', NULL),
(NULL, 'ecrypt_key', '1', '加密key', 'string', 'Token', NULL, '44', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2016-03-29 11:23:10', '2016-03-29 13:27:21', NULL),
(NULL, 'cate_id', '1', '分类', 'integer', 'Token', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-03-29 11:23:10', '2016-03-29 11:23:10', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'Token', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-03-29 11:23:10', '2016-03-29 11:23:10', NULL),
(NULL, 'status', '1', '是否启用', 'integer', 'Token', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '0=>停用\r\n1=>启用', '0', '', '', 'equal', 'select', '0', '1', '', NULL, '', '', '', 0, '2016-03-29 11:23:10', '2016-03-29 13:39:36', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Token', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-03-29 11:23:10', '2016-03-29 11:23:10', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Token', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-03-29 11:23:10', '2016-03-29 11:23:10', NULL),
(NULL, 'id', '1', '编号', 'integer', 'Token', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-03-29 11:23:10', '2016-03-29 11:23:10', NULL),
(NULL, 'token', '1', 'token', 'string', 'Token', NULL, '32', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2016-03-29 11:23:10', '2016-03-29 13:23:50', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Token', 'Token', '', 'default', '', 27, '2016-03-29 11:23:10', '2016-03-29 11:23:10', 'miao_tokens', '', '', '', '0', NULL, 0, '0', '0', '');
