set names utf8;
CREATE TABLE IF NOT EXISTS `miao_blogs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(300) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `published` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `content` text,
  `summary` varchar(2000) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Blog', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-25 18:36:49', '2016-11-25 18:36:49', NULL),
(NULL, 'name', '1', '名称', 'string', 'Blog', NULL, '200', 5, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-25 18:36:49', '2016-11-25 18:36:49', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'Blog', NULL, '300', 5, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-25 18:36:49', '2016-11-25 18:36:49', NULL),
(NULL, 'cate_id', '1', '分类', 'integer', 'Blog', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-25 18:36:49', '2016-11-25 18:36:49', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'Blog', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-25 18:36:49', '2016-11-25 18:36:49', NULL),
(NULL, 'published', '1', '是否发布', 'integer', 'Blog', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '0=>否\r\n1=>是', '0', '', '', 'equal', 'select', '1', '1', '', NULL, '', '', '', 0, '2016-11-25 18:36:49', '2016-11-25 18:39:14', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Blog', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-25 18:36:49', '2016-11-25 18:36:49', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Blog', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-25 18:36:49', '2016-11-25 18:36:49', NULL),
(NULL, 'content', '1', '内容', 'content', 'Blog', 'zh_cn', '65535', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'ckeditor', '', '1', '', NULL, '', '', '', 0, '2016-11-25 18:38:42', '2016-11-25 18:38:42', NULL),
(NULL, 'summary', '1', '摘要', 'string', 'Blog', 'zh_cn', '2000', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2016-11-25 18:45:17', '2016-11-25 18:45:17', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Blog', '记录', '', 'default', '', 0, '2016-11-25 18:36:49', '2016-11-25 18:36:49', 'miao_blogs', '', '', '', '0', 1, NULL, '0', '0', '');
