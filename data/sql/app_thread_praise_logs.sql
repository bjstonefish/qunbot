set names utf8;
CREATE TABLE IF NOT EXISTS `miao_app_thread_praise_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT '0',
  `app_id` int(11) DEFAULT '0',
  `thread_id` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_user_id` (`app_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'thread_id', '1', '帖子编号', 'integer', 'AppThreadPraiseLog', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-04 10:48:41', '2017-02-07 21:26:44', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'AppThreadPraiseLog', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-04 10:48:41', '2015-06-04 10:48:41', NULL),
(NULL, 'app_id', '1', '小程序编号', 'integer', 'AppThreadPraiseLog', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-04 10:48:41', '2017-02-07 21:26:25', NULL),
(NULL, 'id', '1', '编号', 'integer', 'AppThreadPraiseLog', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-04 10:48:41', '2015-06-04 10:48:41', NULL),
(NULL, 'user_id', '1', '用户编号', 'integer', 'AppThreadPraiseLog', NULL, '11', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-04 10:48:41', '2017-02-07 21:27:13', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'AppThreadPraiseLog', '帖子赞记录', '', 'default', '', 0, '2017-02-07 21:25:24', '2017-02-07 21:25:24', 'miao_app_thread_praise_logs', '', '', '', '0', NULL, NULL, '0', '0', '');
