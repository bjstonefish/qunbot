set names utf8;
CREATE TABLE IF NOT EXISTS `miao_barters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` float(10,2) DEFAULT '0.00',
  `barters` varchar(200) DEFAULT NULL,
  `trade_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Barter', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-06-14 13:46:17', '2014-06-14 13:46:17', NULL),
(NULL, 'price', '1', '出价', 'float', 'Barter', NULL, '10,2', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-06-14 13:46:17', '2014-06-14 13:46:17', ''),
(NULL, 'barters', '1', '交换物品', 'string', 'Barter', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-06-14 13:46:17', '2014-06-14 13:46:17', ''),
(NULL, 'trade_id', '1', '交易物编号', 'integer', 'Barter', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-06-14 13:46:17', '2014-06-14 13:46:17', ''),
(NULL, 'creator', '1', '编创建者', 'integer', 'Barter', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-06-14 13:46:17', '2014-06-14 13:46:17', NULL),
(NULL, 'status', '1', '状态', 'integer', 'Barter', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-06-14 13:46:17', '2014-06-14 13:46:17', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Barter', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-06-14 13:46:17', '2014-06-14 13:46:17', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Barter', '易物交换', '', 'default', '', 27, '2014-06-14 13:46:17', '2014-06-14 13:46:17', 'miao_barters', '', '', '', '0', 2, NULL, '0', '0', '');
