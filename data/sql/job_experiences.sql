set names utf8;
CREATE TABLE IF NOT EXISTS `miao_job_experiences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `creator` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `company_name` varchar(64) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `location` varchar(64) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'JobExperience', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-10-04 17:15:52', '2014-10-04 17:15:52', NULL),
(NULL, 'name', '1', '名称', 'string', 'JobExperience', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-10-04 17:15:52', '2014-10-04 17:15:52', NULL),
(NULL, 'creator', '1', '编创建者', 'integer', 'JobExperience', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-10-04 17:15:52', '2014-10-04 17:15:52', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'JobExperience', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-10-04 17:15:52', '2014-10-04 17:15:52', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'JobExperience', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-10-04 17:15:52', '2014-10-04 17:15:52', NULL),
(NULL, 'location', '1', '工作地点', 'string', 'JobExperience', 'zh_cn', '64', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-10-04 19:47:33', '2016-05-04 07:00:54', ''),
(NULL, 'company_name', '1', '公司名称', 'string', 'JobExperience', 'zh_cn', '64', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-10-04 18:02:40', '2014-10-04 18:02:40', ''),
(NULL, 'start_date', '1', '开始时间', 'date', 'JobExperience', NULL, '10', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'date', '', '1', '', NULL, '', '', '', 0, '2014-10-04 18:04:31', '2014-10-04 19:52:26', ''),
(NULL, 'end_date', '1', '结束时间', 'date', 'JobExperience', NULL, '10', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'datetime', '', '1', '', NULL, '', '', '', 0, '2014-10-04 18:04:31', '2014-10-04 19:52:11', ''),
(NULL, 'description', '1', '职位描述', 'string', 'JobExperience', NULL, '1000', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'textarea', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-10-04 18:04:32', '2014-10-04 18:04:32', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'JobExperience', '工作经验', '', 'default', '', 27, '2014-10-04 17:15:52', '2014-10-04 17:15:52', 'miao_job_experiences', '', '', '', '0', NULL, 0, '0', '0', '');
