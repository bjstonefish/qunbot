set names utf8;
CREATE TABLE IF NOT EXISTS `miao_mobile_captchas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mobile` char(12) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `code` char(4) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mobile` (`mobile`),
  KEY `mobile_2` (`mobile`),
  KEY `mobile_3` (`mobile`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'MobileCaptcha', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-10-19 05:49:28', '2017-10-19 05:49:28', NULL),
(NULL, 'mobile', '1', '名称', 'char', 'MobileCaptcha', NULL, '12', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2017-10-19 05:49:28', '2017-10-19 06:14:03', NULL),
(NULL, 'code', '1', '验证码', 'char', 'MobileCaptcha', NULL, '4', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2017-10-19 05:49:28', '2017-10-19 06:14:29', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'MobileCaptcha', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-10-19 05:49:28', '2017-10-19 05:49:28', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'MobileCaptcha', '手机短信验证码', '', 'default', '', 0, '2017-10-19 05:49:27', '2017-10-19 05:49:27', 'miao_mobile_captchas', '', '', '', '0', NULL, NULL, '0', '0', '');
