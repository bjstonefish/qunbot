set names utf8;
CREATE TABLE IF NOT EXISTS `miao_barter_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `barter_id` int(11) DEFAULT '0',
  `trade_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `target_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'trade_id', '1', '分类', 'integer', 'BarterLog', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-06-14 13:59:55', '2014-06-14 13:59:55', ''),
(NULL, 'barter_id', '1', '交换编号', 'integer', 'BarterLog', NULL, '11', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-06-14 13:59:55', '2014-06-14 13:59:55', ''),
(NULL, 'id', '1', '编号', 'integer', 'BarterLog', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-06-14 13:59:55', '2014-06-14 13:59:55', NULL),
(NULL, 'creator', '1', '编创建者', 'integer', 'BarterLog', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-06-14 13:59:55', '2014-06-14 13:59:55', NULL),
(NULL, 'status', '1', '状态', 'integer', 'BarterLog', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-06-14 13:59:55', '2014-06-14 13:59:55', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'BarterLog', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-06-14 13:59:55', '2014-06-14 13:59:55', NULL),
(NULL, 'target_id', '1', '交易物目标id', 'integer', 'BarterLog', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-06-14 14:06:00', '2014-06-14 14:06:00', '');
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'BarterLog', '易物交换记录', '', 'default', '', 27, '2014-06-14 13:59:54', '2014-06-14 13:59:54', 'miao_barter_logs', '', '', '', '0', NULL, NULL, '0', '0', '');
