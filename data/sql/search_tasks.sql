set names utf8;
CREATE TABLE IF NOT EXISTS `miao_search_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `searchurl` varchar(300) DEFAULT NULL,
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `score` smallint(3) DEFAULT '0',
  `summary` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'SearchTask', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-02 13:37:26', '2015-05-02 13:37:26', NULL),
(NULL, 'name', '1', '搜索词', 'string', 'SearchTask', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-05-02 13:37:26', '2015-05-02 13:41:14', NULL),
(NULL, 'searchurl', '1', '搜索网址', 'string', 'SearchTask', NULL, '300', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-05-02 13:37:26', '2015-05-02 13:43:21', NULL),
(NULL, 'cate_id', '1', '分类', 'integer', 'SearchTask', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-02 13:37:26', '2015-05-02 13:37:26', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'SearchTask', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-02 13:37:26', '2015-05-02 13:37:26', NULL),
(NULL, 'status', '1', '是否生效', 'integer', 'SearchTask', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '0=>否\r\n1=>是', '0', '', '', '', 'select', '1', '1', '', NULL, '', '', '', 0, '2015-05-02 13:37:26', '2015-05-02 13:44:05', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'SearchTask', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-02 13:37:26', '2015-05-02 13:37:26', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'SearchTask', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-02 13:37:26', '2015-05-02 13:37:26', NULL),
(NULL, 'score', '1', '任务积分', 'integer', 'SearchTask', 'en_us', '3', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-05-02 13:45:51', '2015-05-02 13:45:51', NULL),
(NULL, 'summary', '1', '任务描述', 'string', 'SearchTask', 'en_us', '1000', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2015-05-02 14:27:24', '2015-05-02 14:27:24', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'SearchTask', '搜索任务', '', 'default', '', 27, '2015-05-02 13:37:26', '2015-05-02 13:37:26', 'miao_search_tasks', '', '', '', '0', 1, NULL, '0', '0', '');
