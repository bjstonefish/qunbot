set names utf8;
CREATE TABLE IF NOT EXISTS `miao_event_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` char(10) DEFAULT NULL,
  `user_id` int(11) DEFAULT '0',
  `data_id` int(11) DEFAULT '0',
  `model` varchar(40) DEFAULT NULL,
  `created` int(11) DEFAULT '0',
  `before_data` text,
  `after_data` text,
  `url` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'EventLog', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-11-15 12:04:26', '2014-11-15 12:04:26', NULL),
(NULL, 'event', '1', '事件名称', 'char', 'EventLog', NULL, '10', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-11-15 12:04:26', '2014-11-15 12:06:55', NULL),
(NULL, 'user_id', '1', '编创建者', 'integer', 'EventLog', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', '', '', '1', '', NULL, '', '', '', 0, '2014-11-15 12:04:26', '2014-11-15 12:05:18', NULL),
(NULL, 'data_id', '1', '数据编号', 'integer', 'EventLog', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', '', '0', '1', '', NULL, '', '', '', 0, '2014-11-15 12:04:26', '2014-11-15 12:07:49', NULL),
(NULL, 'model', '1', '数据模块', 'string', 'EventLog', NULL, '40', 2, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-11-15 12:04:26', '2014-11-15 12:08:36', NULL),
(NULL, 'created', '1', '修改时间', 'integer', 'EventLog', NULL, '11', 1, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'datetime', '', '1', '', NULL, '', '', '', 0, '2014-11-15 12:04:26', '2014-11-15 12:10:12', NULL),
(NULL, 'before_data', '1', '修改前数据', 'content', 'EventLog', NULL, '65535', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-11-15 13:55:16', '2014-11-15 13:55:16', NULL),
(NULL, 'after_data', '1', '修改后数据', 'content', 'EventLog', NULL, '65535', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-11-15 13:55:16', '2014-11-15 13:55:16', NULL),
(NULL, 'url', '1', '操作网址', 'string', 'EventLog', 'en_us', '32', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-04-27 19:24:20', '2015-04-27 19:24:20', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'EventLog', '事件变更记录', '', 'default', '', 27, '2014-11-15 12:04:26', '2014-11-15 13:44:28', 'miao_event_logs', '', '', '', '0', NULL, 0, '0', '0', '');
