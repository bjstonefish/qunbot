set names utf8;
CREATE TABLE IF NOT EXISTS `miao_praise_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT '0',
  `model` char(24) DEFAULT '',
  `data_id` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `score` smallint(3) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `app_data_id` (`model`,`data_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'data_id', '1', '数据编号', 'integer', 'PraiseLog', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-04 10:48:41', '2017-02-18 23:25:31', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'PraiseLog', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-04 10:48:41', '2015-06-04 10:48:41', NULL),
(NULL, 'model', '1', '数据模块', 'char', 'PraiseLog', NULL, '24', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-04 10:48:41', '2017-02-18 23:26:02', NULL),
(NULL, 'id', '1', '编号', 'integer', 'PraiseLog', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-04 10:48:41', '2015-06-04 10:48:41', NULL),
(NULL, 'user_id', '1', '用户编号', 'integer', 'PraiseLog', NULL, '11', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-04 10:48:41', '2017-02-07 21:27:13', NULL),
(NULL, 'score', '1', '评分', 'integer', 'PraiseLog', 'zh_cn', '3', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '评分数，1~5，或者1~10', 0, '2017-03-02 14:51:45', '2017-03-02 14:51:45', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'PraiseLog', '点赞记录', '', 'default', '', 0, '2017-02-18 23:24:51', '2017-02-18 23:24:51', 'miao_praise_logs', '', '', '', '0', NULL, NULL, '0', '0', '');
