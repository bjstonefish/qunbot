set names utf8;
CREATE TABLE IF NOT EXISTS `miao_course_msgs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fileurl` varchar(300) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `course_id` int(11) DEFAULT '0',
  `msg_text` varchar(1000) DEFAULT '',
  `is_question` smallint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'CourseMsg', NULL, '11', 9, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-08-11 06:59:21', '2016-08-11 06:59:21', NULL),
(NULL, 'is_question', '1', '是否提问', 'integer', 'CourseMsg', 'zh_cn', '3', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-08-11 07:32:49', '2016-08-11 07:32:49', NULL),
(NULL, 'fileurl', '1', '消息文件', 'string', 'CourseMsg', NULL, '300', 4, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2016-08-11 06:59:21', '2016-08-11 07:02:49', NULL),
(NULL, 'cate_id', '1', '消息类型', 'integer', 'CourseMsg', NULL, '11', 7, '1', '1', '', '', '', 0, '1', '1=>语音\r\n2=>文字\r\n3=>图片', '0', '', '', 'equal', 'select', '', '1', '', NULL, '', '', '', 0, '2016-08-11 06:59:21', '2016-08-11 07:04:09', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'CourseMsg', NULL, '11', 5, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-08-11 06:59:21', '2016-08-11 06:59:21', NULL),
(NULL, 'status', '1', '状态', 'integer', 'CourseMsg', NULL, '11', 3, '1', '1', '', NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-08-11 06:59:21', '2016-08-11 06:59:21', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'CourseMsg', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-08-11 06:59:21', '2016-08-11 06:59:21', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'CourseMsg', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-08-11 06:59:21', '2016-08-11 06:59:21', NULL),
(NULL, 'course_id', '1', '课程编号', 'integer', 'CourseMsg', 'zh_cn', '11', 8, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-08-11 07:00:04', '2016-08-11 07:00:04', NULL),
(NULL, 'msg_text', '1', '消息文本', 'string', 'CourseMsg', 'zh_cn', '1000', 6, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2016-08-11 07:06:10', '2016-08-11 07:06:10', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'CourseMsg', '课程消息', '', 'default', '', 0, '2016-08-11 06:59:21', '2016-08-11 06:59:21', 'miao_course_msgs', '', '', '', '0', NULL, NULL, '0', '0', '');
