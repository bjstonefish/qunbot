set names utf8;
CREATE TABLE IF NOT EXISTS `miao_signatures` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(300) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `prefix` text,
  `suffix` text,
  PRIMARY KEY (`id`),
  KEY `creator` (`creator`),
  KEY `creator_2` (`creator`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Signature', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-30 20:23:00', '2016-04-30 20:23:00', NULL),
(NULL, 'name', '1', '名称', 'string', 'Signature', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-30 20:23:00', '2016-04-30 20:23:00', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'Signature', NULL, '300', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 1, '2016-04-30 20:23:00', '2016-04-30 20:23:00', NULL),
(NULL, 'cate_id', '1', '分类', 'integer', 'Signature', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-30 20:23:00', '2016-04-30 20:23:00', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'Signature', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-30 20:23:00', '2016-04-30 20:23:00', NULL),
(NULL, 'status', '1', '状态', 'integer', 'Signature', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-30 20:23:00', '2016-04-30 20:23:00', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Signature', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-30 20:23:00', '2016-04-30 20:23:00', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Signature', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-30 20:23:00', '2016-04-30 20:23:00', NULL),
(NULL, 'prefix', '1', '前缀', 'content', 'Signature', NULL, '65535', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'textarea', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-30 20:26:52', '2016-04-30 20:26:52', NULL),
(NULL, 'suffix', '1', '后缀', 'content', 'Signature', NULL, '65535', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'textarea', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-30 20:26:52', '2016-04-30 20:26:52', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Signature', '签名', '', 'default', '', 27, '2016-04-30 20:23:00', '2016-04-30 20:23:00', 'miao_signatures', '', 'self', '', '0', NULL, NULL, '0', '0', '');
