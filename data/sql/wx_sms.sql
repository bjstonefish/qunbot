set names utf8;
CREATE TABLE IF NOT EXISTS `miao_wx_sms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `event` varchar(32) DEFAULT '',
  `wx_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `user_openid` varchar(44) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `wx_id` (`wx_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
