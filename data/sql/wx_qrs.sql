set names utf8;
CREATE TABLE IF NOT EXISTS `miao_wx_qrs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `coverimg` varchar(200) DEFAULT NULL,
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `slug` varchar(44) DEFAULT NULL,
  `wx_id` int(11) DEFAULT '0',
  `scan_nums` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'WxQr', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-22 16:38:49', '2015-05-22 16:38:49', NULL),
(NULL, 'name', '1', '名称', 'string', 'WxQr', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '微信二维码时，名称将作为自动回复的关键字，查找规则，进行自动回复消息', 0, '2015-05-22 16:38:49', '2015-05-22 16:48:53', NULL),
(NULL, 'coverimg', '1', '二维码图片', 'string', 'WxQr', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', '', '', '1', '', NULL, '', '', '', 0, '2015-05-22 16:38:49', '2015-05-22 16:43:13', NULL),
(NULL, 'cate_id', '1', '分类', 'integer', 'WxQr', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '1=>微信\r\n2=>网址\r\n3=>文字\r\n4=>名片', '0', '', '', '', 'select', '', '1', '', NULL, '', '', '', 0, '2015-05-22 16:38:49', '2015-05-22 16:42:47', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'WxQr', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-22 16:38:49', '2015-05-22 16:38:49', NULL),
(NULL, 'status', '1', '状态', 'integer', 'WxQr', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-22 16:38:49', '2015-05-22 16:38:49', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'WxQr', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-22 16:38:49', '2015-05-22 16:38:49', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'WxQr', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-22 16:38:49', '2015-05-22 16:38:49', NULL),
(NULL, 'slug', '1', '唯一参数', 'string', 'WxQr', 'en_us', '44', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-05-22 16:41:45', '2015-05-22 16:41:45', NULL),
(NULL, 'wx_id', '1', '所属微信', 'integer', 'WxQr', 'en_us', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-05-22 16:44:08', '2015-05-22 16:44:08', NULL),
(NULL, 'scan_nums', '1', '扫码打开次数', 'integer', 'WxQr', 'en_us', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '', 0, '2015-05-22 16:44:55', '2015-05-22 16:47:20', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`) VALUES (NULL, 'WxQr', '二维码', '', 'default', '', 27, '2015-05-22 16:38:49', '2015-05-22 16:38:49', 'miao_wx_qrs', '', '', '', '0', 2, 0);
