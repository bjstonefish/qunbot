set names utf8;
CREATE TABLE IF NOT EXISTS `miao_icons` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(300) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `description` varchar(200) DEFAULT '',
  `source` varchar(20) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'updated', '1', '修改时间', 'datetime', 'icon', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-05-20 21:42:56', '2016-05-20 21:42:56', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'icon', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-05-20 21:42:56', '2016-05-20 21:42:56', NULL),
(NULL, 'status', '1', '状态', 'integer', 'icon', NULL, '11', 4, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-05-20 21:42:56', '2016-05-20 21:42:56', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'icon', NULL, '11', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-05-20 21:42:56', '2016-05-20 21:42:56', NULL),
(NULL, 'cate_id', '1', '分类', 'integer', 'icon', NULL, '11', 9, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-05-20 21:42:56', '2016-05-20 21:42:56', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'icon', NULL, '300', 7, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'textarea', '', '1', '', NULL, '', 'explode', '新增操作一次添加多个图标时，一行一个图标地址。（具有相同的标题、内容描述、标签等）', 0, '2016-05-20 21:42:56', '2016-05-21 09:45:02', NULL),
(NULL, 'name', '1', '名称', 'string', 'icon', NULL, '200', 8, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-05-20 21:42:56', '2016-05-20 21:42:56', NULL),
(NULL, 'id', '1', '编号', 'integer', 'icon', NULL, '11', 10, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-05-20 21:42:56', '2016-05-20 21:42:56', NULL),
(NULL, 'description', '1', '图片描述', 'string', 'Icon', 'zh_cn', '200', 3, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2016-05-20 21:47:16', '2016-05-21 09:39:07', NULL),
(NULL, 'source', '1', '来源', 'string', 'Icon', 'zh_cn', '20', 6, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-05-21 09:45:50', '2016-05-21 09:45:50', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`) VALUES (NULL, 'icon', '图标', '', 'default', '', 27, '2016-05-20 21:42:56', '2016-05-20 21:42:56', 'miao_icons', '', '', '', '0', 1, NULL);
