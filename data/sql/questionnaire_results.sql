set names utf8;
CREATE TABLE IF NOT EXISTS `miao_questionnaire_results` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `content` text,
  `q_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `remark` varchar(100) DEFAULT '',
  `star` smallint(3) DEFAULT '0',
  `readed` smallint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'QuestionnaireResult', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-04-19 07:56:49', '2017-04-19 07:56:49', NULL),
(NULL, 'content', '1', '提交结果', 'content', 'QuestionnaireResult', NULL, '65535', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'textarea', '', '1', '', NULL, '', '', '整个表单的内容汇总成json格式的数据', 0, '2017-04-19 07:56:49', '2017-04-19 08:01:48', NULL),
(NULL, 'q_id', '1', '问卷表单编号', 'integer', 'QuestionnaireResult', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2017-04-19 07:56:49', '2017-04-19 08:01:12', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'QuestionnaireResult', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-04-19 07:56:49', '2017-04-19 07:56:49', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'QuestionnaireResult', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-04-19 07:56:49', '2017-04-19 07:56:49', NULL),
(NULL, 'remark', '1', '备注', 'string', 'QuestionnaireResult', 'zh_cn', '100', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2017-04-19 08:07:34', '2017-04-19 08:07:34', NULL),
(NULL, 'status', '1', '处理状态', 'integer', 'QuestionnaireResult', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-04-19 08:07:00', '2017-04-19 08:07:00', NULL),
(NULL, 'star', '1', ' 是否星标', 'integer', 'QuestionnaireResult', 'zh_cn', '3', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '', 0, '2017-04-20 05:28:11', '2017-04-20 05:28:11', NULL),
(NULL, 'readed', '1', ' 是否已读', 'integer', 'QuestionnaireResult', 'zh_cn', '3', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '', 0, '2017-04-20 05:28:48', '2017-04-20 05:28:48', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'QuestionnaireResult', '问卷结果', '', 'default', '', 0, '2017-04-19 07:56:49', '2017-04-19 07:56:49', 'miao_questionnaire_results', '', '', '', '0', NULL, NULL, '0', '0', '');
