set names utf8;
CREATE TABLE IF NOT EXISTS `miao_wx_similars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `words` varchar(400) DEFAULT '',
  `wx_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wx_id` (`wx_id`,`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'WxSimilar', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-07-31 17:13:48', '2015-07-31 17:13:48', NULL),
(NULL, 'name', '1', '名称', 'string', 'WxSimilar', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-07-31 17:13:48', '2015-07-31 17:13:48', NULL),
(NULL, 'words', '1', '近义词', 'string', 'WxSimilar', NULL, '400', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2015-07-31 17:13:48', '2015-07-31 17:16:26', NULL),
(NULL, 'wx_id', '1', '微信编号', 'integer', 'WxSimilar', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-07-31 17:13:48', '2015-07-31 17:14:58', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'WxSimilar', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-07-31 17:13:48', '2015-07-31 17:13:48', NULL),
(NULL, 'status', '1', '状态', 'integer', 'WxSimilar', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-07-31 17:13:48', '2015-07-31 17:13:48', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'WxSimilar', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-07-31 17:13:48', '2015-07-31 17:13:48', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'WxSimilar', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-07-31 17:13:48', '2015-07-31 17:13:48', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'WxSimilar', '自动回复近义词', '', 'default', '', 27, '2015-07-31 17:13:48', '2015-07-31 17:13:48', 'miao_wx_similars', '', '', '', '0', NULL, NULL, '0', '0', '');
