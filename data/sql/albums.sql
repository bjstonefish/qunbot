set names utf8;
CREATE TABLE IF NOT EXISTS `miao_albums` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(300) DEFAULT '',
  `aid` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `published` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `price` float(10,2) DEFAULT '0.00',
  `is_virtual` smallint(3) DEFAULT '0',
  `storage` int(11) DEFAULT '0',
  `saled` int(11) DEFAULT '0',
  `content` text,
  `summary` varchar(500) DEFAULT '',
  `view_nums` int(11) DEFAULT '0',
  `fav_nums` int(11) DEFAULT '0',
  `comment_nums` int(11) DEFAULT '0',
  `play_nums` int(11) DEFAULT '0',
  `type` smallint(3) DEFAULT '0',
  `author` varchar(32) DEFAULT '',
  `qrimg` varchar(300) DEFAULT '',
  `avatar` varchar(300) DEFAULT '',
  `thirdurl` varchar(300) DEFAULT '',
  `author_des` varchar(300) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'published', '1', '是否发布', 'integer', 'Album', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '0=>否\r\n1=>是', '0', '', '', 'equal', 'select', '1', '1', '', NULL, '', '', '', 0, '2017-06-06 17:21:02', '2017-06-12 10:56:23', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'Album', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-06 17:21:02', '2017-06-06 17:21:02', NULL),
(NULL, 'aid', '1', '活动编号', 'integer', 'Album', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 1, '2017-06-06 17:21:02', '2017-06-07 09:00:10', NULL),
(NULL, 'coverimg', '0', '封面图片', 'string', 'Album', NULL, '300', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'file', '', '1', '', NULL, '', '', '', 0, '2017-06-06 17:21:02', '2017-06-13 14:29:28', NULL),
(NULL, 'name', '1', '专辑名称', 'string', 'Album', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2017-06-06 17:21:02', '2017-06-12 10:40:44', NULL),
(NULL, 'id', '1', '编号', 'integer', 'Album', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-06 17:21:02', '2017-06-06 17:21:02', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Album', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-06 17:21:02', '2017-06-06 17:21:02', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Album', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-06 17:21:02', '2017-06-06 17:21:02', NULL),
(NULL, 'price', '1', '价格', 'float', 'Album', NULL, '10,2', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '0.00', '1', '', NULL, '', '', '', 0, '2017-06-06 17:50:15', '2017-06-06 17:50:35', NULL),
(NULL, 'is_virtual', '1', '是否虚拟产品', 'integer', 'Album', NULL, '3', 0, '1', '1', '', '', '', 0, '1', '1=>是\r\n0=>否', '0', '', '', '', 'select', '1', '1', '', NULL, '', '', '', 0, '2017-06-06 17:50:15', '2017-06-06 17:51:01', NULL),
(NULL, 'storage', '1', '库存量', 'integer', 'Album', NULL, '11', 0, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-06 17:50:15', '2017-06-06 17:50:15', NULL),
(NULL, 'saled', '1', '销售量', 'integer', 'Album', NULL, '11', 0, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-06 17:50:15', '2017-06-06 17:50:15', NULL),
(NULL, 'content', '1', '专辑内容介绍', 'content', 'Album', 'zh_cn', '65535', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'ckeditor', '', '1', '', NULL, '', '', '', 0, '2017-06-12 10:42:57', '2017-06-12 10:42:57', NULL),
(NULL, 'summary', '1', '专辑摘要', 'string', 'Album', 'zh_cn', '500', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2017-06-12 10:43:31', '2017-06-12 10:43:31', NULL),
(NULL, 'view_nums', '1', '阅读数', 'integer', 'Album', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-06-12 10:45:04', '2017-06-12 10:45:04', NULL),
(NULL, 'fav_nums', '1', '收藏数', 'integer', 'Album', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-06-12 10:45:25', '2017-06-12 10:45:25', NULL),
(NULL, 'comment_nums', '1', '评论数', 'integer', 'Album', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-06-12 10:45:44', '2017-06-12 10:45:44', NULL),
(NULL, 'play_nums', '1', '播放数', 'integer', 'Album', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-06-12 10:46:08', '2017-06-12 10:46:08', NULL),
(NULL, 'type', '1', '专辑类型', 'integer', 'Album', 'zh_cn', '3', NULL, '1', '1', '', '', '', NULL, '1', '1=>视频点播\r\n2=>音频点播', '0', '', '', '', 'select', '1', '1', '', NULL, '', '', '', 0, '2017-06-12 10:53:50', '2017-06-13 12:22:26', NULL),
(NULL, 'author', '1', '作者', 'string', 'Album', 'zh_cn', '32', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-06-22 10:46:11', '2017-06-22 10:46:11', NULL),
(NULL, 'qrimg', '1', '二维码', 'string', 'Album', 'zh_cn', '300', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'file', '', '1', '', NULL, '', '', '', 0, '2017-06-22 10:46:28', '2017-06-22 10:47:46', NULL),
(NULL, 'avatar', '1', '头像或logo', 'string', 'Album', 'zh_cn', '300', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'file', '', '1', '', NULL, '', '', '', 0, '2017-06-22 10:46:42', '2017-06-22 10:47:31', NULL),
(NULL, 'thirdurl', '1', '第三方地址', 'string', 'Album', 'zh_cn', '300', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-06-22 10:47:00', '2017-06-22 10:49:06', NULL),
(NULL, 'author_des', '1', '作者描述', 'string', 'Album', 'zh_cn', '300', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2017-06-22 11:23:50', '2017-06-22 11:23:50', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Album', '专辑', '', 'default', '', 0, '2017-06-12 10:39:53', '2017-06-13 13:16:03', 'miao_albums', '', '', '', '0', 1, NULL, '0', '0', '');
