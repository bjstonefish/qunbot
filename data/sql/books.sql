set names utf8;
CREATE TABLE IF NOT EXISTS `miao_books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(200) DEFAULT NULL,
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `lastupdator` int(11) DEFAULT '0',
  `remoteurl` varchar(200) DEFAULT '',
  `status` tinyint(4) DEFAULT '0',
  `locale` char(5) DEFAULT '',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `view_nums` int(11) DEFAULT '0',
  `summary` varchar(500) DEFAULT NULL,
  `chapter_nums` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Book', NULL, '11', 6, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2012-12-01 21:13:21', '2012-12-01 21:13:21', NULL),
(NULL, 'name', '1', '名称', 'string', 'Book', NULL, '200', 5, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2012-12-01 21:13:21', '2012-12-01 21:13:21', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'Book', NULL, '200', 5, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'file', '', '1', '', NULL, '', '', '', 0, '2012-12-01 21:13:21', '2012-12-01 21:13:21', ''),
(NULL, 'cate_id', '1', '分类', 'integer', 'Book', NULL, '11', 6, '1', '1', 'Category', 'id', 'name', NULL, '1', '', '0', '', '', 'treenode', 'select', '', '1', '', NULL, '', '', '', 0, '2012-12-01 21:13:21', '2012-12-01 21:13:21', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<options>\r\n    <conditions>        <Category.model>Book</Category.model>\r\n    </conditions>  \r\n</options>'),
(NULL, 'creator', '1', '编创建者', 'integer', 'Book', NULL, '11', 6, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2012-12-01 21:13:21', '2012-12-01 21:13:21', NULL),
(NULL, 'lastupdator', '1', '最后修改人', 'integer', 'Book', NULL, '11', 6, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2012-12-01 21:13:21', '2012-12-01 21:13:21', NULL),
(NULL, 'remoteurl', '1', '引用地址', 'string', 'Book', NULL, '200', 5, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2012-12-01 21:13:21', '2012-12-01 21:13:21', NULL),
(NULL, 'status', '1', '状态', 'integer', 'Book', NULL, '11', 3, '1', '1', NULL, NULL, NULL, NULL, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2012-12-01 21:13:21', '2012-12-01 21:13:21', NULL),
(NULL, 'locale', '1', '语言版本', 'char', 'Book', NULL, '5', 3, '1', '1', NULL, NULL, NULL, NULL, '1', 'zh_cn', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2012-12-01 21:13:21', '2012-12-01 21:13:21', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Book', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2012-12-01 21:13:21', '2012-12-01 21:13:21', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Book', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2012-12-01 21:13:21', '2012-12-01 21:13:21', NULL),
(NULL, 'view_nums', '1', '查看次数', 'integer', 'Book', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '', 0, '2014-07-04 22:30:28', '2014-07-04 22:30:28', ''),
(NULL, 'summary', '1', '书籍摘要', 'string', 'Book', 'zh_cn', '500', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2014-12-27 08:41:13', '2014-12-27 08:41:14', NULL),
(NULL, 'chapter_nums', '1', '章节数目', 'integer', 'Book', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-12-27 15:15:09', '2016-12-27 15:15:09', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Book', '知识书籍', '', 'default', '', 27, '2012-12-01 21:13:21', '2012-12-01 21:13:21', 'miao_books', '', '', '', '0', 1, NULL, '0', '0', '');
