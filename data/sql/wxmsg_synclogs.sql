set names utf8;
CREATE TABLE IF NOT EXISTS `miao_wxmsg_synclogs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `source` varchar(20) DEFAULT '',
  `thumb_media_id` varchar(300) DEFAULT '',
  `oauth_openid` varchar(44) DEFAULT '',
  `creator` int(11) DEFAULT '0',
  `object_id` varchar(44) DEFAULT '',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `preview_url` varchar(255) DEFAULT '',
  `editor_url` varchar(255) DEFAULT '',
  `mid` varchar(44) DEFAULT '',
  `data_id` int(11) DEFAULT '0',
  `idx` smallint(3) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_id` (`data_id`,`oauth_openid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'WxmsgSynclog', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-09-14 07:28:09', '2016-09-14 07:28:09', NULL),
(NULL, 'source', '1', '类型', 'string', 'WxmsgSynclog', NULL, '20', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', '', '', '1', '', NULL, '', '', '', 0, '2016-09-14 07:28:09', '2016-09-14 07:31:13', NULL),
(NULL, 'thumb_media_id', '1', '封面媒体文件编号', 'string', 'WxmsgSynclog', NULL, '300', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', '', '', '1', '', NULL, '', '', '', 0, '2016-09-14 07:28:09', '2016-09-14 07:29:31', NULL),
(NULL, 'oauth_openid', '1', '第三方编号', 'string', 'WxmsgSynclog', NULL, '44', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', '', '', '1', '', NULL, '', '', '', 0, '2016-09-14 07:28:09', '2016-09-14 07:30:18', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'WxmsgSynclog', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-09-14 07:28:09', '2016-09-14 07:28:09', NULL),
(NULL, 'object_id', '1', '第三方添加的数据编号', 'string', 'WxmsgSynclog', NULL, '44', 3, '1', '1', '', '', '', 0, '1', '0=>否\r\n1=>是', '0', '', '', 'equal', 'input', '0', '1', '', NULL, '', '', '', 0, '2016-09-14 07:28:09', '2016-09-14 07:32:57', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'WxmsgSynclog', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-09-14 07:28:09', '2016-09-14 07:28:09', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'WxmsgSynclog', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-09-14 07:28:09', '2016-09-14 07:28:09', NULL),
(NULL, 'preview_url', '1', '预览地址', 'string', 'WxmsgSynclog', 'zh_cn', '255', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-09-14 07:34:36', '2016-09-14 07:34:36', NULL),
(NULL, 'editor_url', '1', '第三方编辑地址', 'string', 'WxmsgSynclog', 'zh_cn', '255', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-09-14 07:34:59', '2016-09-14 07:34:59', NULL),
(NULL, 'mid', '1', '其它相关编号（如微博编号）', 'string', 'WxmsgSynclog', 'zh_cn', '44', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-09-14 07:37:07', '2016-09-14 07:37:07', NULL),
(NULL, 'data_id', '1', '本地数据编号', 'integer', 'WxmsgSynclog', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-09-14 07:53:51', '2016-09-14 07:53:51', NULL),
(NULL, 'idx', '1', '多图文顺序', 'integer', 'WxmsgSynclog', 'zh_cn', '3', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-09-14 08:44:50', '2016-09-14 08:44:50', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'WxmsgSynclog', '文章同步记录', '', 'default', '', 0, '2016-09-14 07:28:09', '2016-09-14 07:28:09', 'miao_wxmsg_synclogs', '', 'self', '', '0', 2, NULL, '0', '0', '');
