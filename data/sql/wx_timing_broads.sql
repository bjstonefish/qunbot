set names utf8;
CREATE TABLE IF NOT EXISTS `miao_wx_timing_broads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` varchar(60) DEFAULT '',
  `msg_type` varchar(32) DEFAULT '',
  `wx_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `sendtime` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` smallint(3) DEFAULT '0',
  `failedmsg` varchar(32) DEFAULT '',
  `send_ignore_reprint` smallint(3) DEFAULT '0',
  `msg_id` varchar(32) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `wx_id` (`wx_id`),
  KEY `wx_id_2` (`wx_id`),
  KEY `sendtime` (`sendtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'failedmsg', '1', '失败描述', 'string', 'WxTimingBroad', 'en_us', '32', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '群发失败错误类型', 0, '2015-06-07 08:42:19', '2015-06-07 08:42:19', NULL),
(NULL, 'status', '1', '群发状态', 'integer', 'WxTimingBroad', 'en_us', '3', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '0为未群发，1为已群发，-1为群发失败，', 0, '2015-06-07 08:41:18', '2015-06-07 08:41:18', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'WxTimingBroad', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-06 18:18:09', '2015-06-06 18:18:09', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'WxTimingBroad', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-06 18:18:09', '2015-06-06 18:18:09', NULL),
(NULL, 'sendtime', '1', '发送时间', 'datetime', 'WxTimingBroad', NULL, '19', 3, '1', '1', '', '', '', 0, '1', '0=>否\r\n1=>是', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-06 18:18:09', '2015-06-06 18:50:10', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'WxTimingBroad', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-06 18:18:09', '2015-06-06 18:18:09', NULL),
(NULL, 'wx_id', '1', '分类', 'integer', 'WxTimingBroad', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-06 18:18:09', '2015-06-06 18:21:36', NULL),
(NULL, 'media_id', '1', '微信素材编号', 'string', 'WxTimingBroad', NULL, '60', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-06 18:18:09', '2015-06-06 18:26:04', NULL),
(NULL, 'msg_type', '1', '消息内容', 'string', 'WxTimingBroad', NULL, '32', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-06 18:18:09', '2015-06-06 18:27:05', NULL),
(NULL, 'id', '1', '编号', 'integer', 'WxTimingBroad', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-06 18:18:09', '2015-06-06 18:18:09', NULL),
(NULL, 'send_ignore_reprint', '1', '转载选项', 'integer', 'WxTimingBroad', 'zh_cn', '3', NULL, '1', '1', '', '', '', NULL, '1', '1=>文章被判定为转载时，继续进行群发操作\r\n0=>文章被判定为转载时，停止群发操作', '0', '', '', '', 'select', '1', '1', '', NULL, '', '', '', 0, '2017-01-05 09:46:50', '2017-01-05 10:52:14', NULL),
(NULL, 'msg_id', '1', '消息id', 'string', 'WxTimingBroad', 'zh_cn', '32', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '群发消息后返回的消息id', 0, '2017-01-05 10:53:23', '2017-01-05 10:53:23', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'WxTimingBroad', '定时群发', '', 'default', '', 27, '2015-06-06 18:18:09', '2015-06-06 18:20:08', 'miao_wx_timing_broads', '', '', '', '0', NULL, NULL, '0', '0', '');
