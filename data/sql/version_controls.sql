set names utf8;
CREATE TABLE IF NOT EXISTS `miao_version_controls` (
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `model` varchar(30) NOT NULL DEFAULT '',
  `data_id` int(11) NOT NULL DEFAULT '0',
  `content` mediumtext,
  `user_id` int(11) DEFAULT '0',
  `url` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`version_id`),
  KEY `model` (`model`,`data_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'content', '1', '内容', 'mediumtext', 'VersionControl', 'zh_cn', '65535', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', 'none', '', 0, '2014-08-17 12:40:58', '2014-08-17 12:40:58', ''),
(NULL, 'data_id', '1', '数据编号', 'integer', 'VersionControl', NULL, '11', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-17 12:24:03', '2014-08-17 12:24:03', NULL),
(NULL, 'version_id', '1', '版本号', 'integer', 'VersionControl', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-08-17 11:23:27', '2014-08-17 12:47:53', ''),
(NULL, 'created', '1', '创建时间', 'datetime', 'VersionControl', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-17 11:23:27', '2014-08-17 11:23:27', NULL),
(NULL, 'model', '1', '模块', 'string', 'VersionControl', NULL, '30', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-17 12:24:03', '2014-08-17 12:24:03', NULL),
(NULL, 'user_id', '1', '用户编号', 'integer', 'VersionControl', 'en_us', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '', 0, '2015-04-27 19:20:18', '2015-04-27 19:20:18', NULL),
(NULL, 'url', '1', '操作网址', 'string', 'VersionControl', 'en_us', '32', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-04-27 19:20:52', '2015-04-27 19:20:52', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'VersionControl', '历史版本', '', 'default', '', 27, '2014-08-17 11:23:27', '2014-08-17 11:23:27', 'miao_version_controls', '', '', '', '0', 100, 0, '0', '0', '');
