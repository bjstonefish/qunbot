set names utf8;
CREATE TABLE IF NOT EXISTS `miao_complaints` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(300) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `model` varchar(20) DEFAULT '',
  `data_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Complaint', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-26 14:04:59', '2016-11-26 14:04:59', NULL),
(NULL, 'name', '1', '投诉描述', 'string', 'Complaint', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', '', '', '1', '', NULL, '', '', '', 0, '2016-11-26 14:04:59', '2016-11-26 14:10:12', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'Complaint', NULL, '300', 5, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 1, '2016-11-26 14:04:59', '2016-11-26 14:04:59', NULL),
(NULL, 'cate_id', '1', '举报类型', 'integer', 'Complaint', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '1=>欺诈\r\n2=>色情\r\n3=>政治谣言\r\n4=>常识谣言\r\n5=>恶意营销\r\n6=>隐私收集\r\n7=>抄袭侵权\r\n8=>违法违纪', '0', '', '', 'equal', 'select', '', '1', '', NULL, '', '', '', 0, '2016-11-26 14:04:59', '2016-11-26 14:09:23', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'Complaint', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-26 14:04:59', '2016-11-26 14:04:59', NULL),
(NULL, 'status', '1', '是否处理', 'integer', 'Complaint', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '0=>否\r\n1=>是', '0', '', '', 'equal', 'select', '0', '1', '', NULL, '', '', '', 0, '2016-11-26 14:04:59', '2016-11-26 14:10:37', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Complaint', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-26 14:04:59', '2016-11-26 14:04:59', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Complaint', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-26 14:04:59', '2016-11-26 14:04:59', NULL),
(NULL, 'model', '1', '投诉数据类型', 'string', 'Complaint', 'zh_cn', '20', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-11-26 14:11:17', '2016-11-26 14:11:18', NULL),
(NULL, 'data_id', '1', '投诉数据编号', 'integer', 'Complaint', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-11-26 14:12:02', '2016-11-26 14:12:02', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Complaint', '投诉举报', '', 'default', '', 0, '2016-11-26 14:04:58', '2016-11-26 14:04:59', 'miao_complaints', '', '', '', '0', 1, NULL, '0', '0', '');
