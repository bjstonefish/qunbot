set names utf8;
CREATE TABLE IF NOT EXISTS `miao_app_uploadfiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_id` int(11) DEFAULT '0',
  `modelclass` varchar(30) DEFAULT NULL,
  `fieldname` varchar(30) DEFAULT NULL,
  `sortorder` int(11) DEFAULT '0',
  `created` datetime DEFAULT '0000-00-00 00:00:00',
  `name` varchar(100) DEFAULT NULL,
  `fspath` varchar(255) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `size` int(11) DEFAULT '0',
  `app_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `data_id` (`app_id`,`data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'AppUploadfile', 'zh_cn', '11', 15, '0', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'data_id', '1', 'data_id', 'integer', 'AppUploadfile', 'zh_cn', '11', 13, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'fieldname', '1', 'fieldname', 'string', 'AppUploadfile', 'zh_cn', '30', 11, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'sortorder', '1', 'sortorder', 'integer', 'AppUploadfile', 'zh_cn', '11', 10, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'AppUploadfile', 'zh_cn', NULL, 2, '0', '0', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', '0000-00-00 00:00:00', '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'name', '1', 'name', 'string', 'AppUploadfile', 'zh_cn', '100', 14, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', '', '', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '2014-11-20 06:43:32', NULL),
(NULL, 'fspath', '1', 'fspath', 'string', 'AppUploadfile', 'zh_cn', '255', 7, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'type', '1', '文件类型', 'string', 'AppUploadfile', 'zh_cn', '20', 6, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', '', '', '1', '', '', '', '', '如：image/jpeg，上传文件时，系统自动识别生成。', 0, '0000-00-00 00:00:00', '2014-11-20 06:44:33', ''),
(NULL, 'size', '1', 'size', 'integer', 'AppUploadfile', 'zh_cn', '11', 5, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', '0', '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'modelclass', '1', 'modelclass', 'string', 'AppUploadfile', 'zh_cn', '30', 12, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', '', 'Article', '1', '', NULL, '', '', '', 0, '0000-00-00 00:00:00', '2014-11-20 06:43:52', NULL),
(NULL, 'app_id', '1', '小程序编号', 'integer', 'AppUploadfile', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2012-08-27 12:46:07', '2017-01-13 14:15:52', ''),
(NULL, 'user_id', '1', '用户编号', 'integer', 'AppUploadfile', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-10-22 18:26:37', '2014-10-22 18:26:38', '');
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'AppUploadfile', 'App上传文件', 'onetomany', 'default', '', 0, '2017-01-13 13:38:39', '2017-01-13 13:38:39', 'miao_app_uploadfiles', '', '', '', '0', NULL, 0, '0', '0', '');
