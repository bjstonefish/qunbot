set names utf8;
CREATE TABLE IF NOT EXISTS `miao_tc_tracks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(2000) DEFAULT NULL,
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `customer_id` int(11) DEFAULT '0',
  `contact_id` int(11) DEFAULT '0',
  `contact_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'TcTrack', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-09-14 11:20:53', '2014-09-14 11:20:53', NULL),
(NULL, 'name', '1', '联系内容', 'string', 'TcTrack', NULL, '2000', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2014-09-14 11:20:53', '2015-03-02 09:14:32', NULL),
(NULL, 'cate_id', '1', '分类', 'integer', 'TcTrack', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-09-14 11:20:53', '2014-09-14 11:20:53', NULL),
(NULL, 'creator', '1', '编创建者', 'integer', 'TcTrack', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-09-14 11:20:53', '2014-09-14 11:20:53', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'TcTrack', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-09-14 11:20:53', '2014-09-14 11:20:53', NULL),
(NULL, 'contact_id', '1', '客户联系人', 'integer', 'TcTrack', 'zh_cn', '11', NULL, '1', '1', 'TcContact', 'id', 'name', NULL, '1', '', '1', 'customer_id', 'CustomerId', '', 'select', '', '1', '', NULL, '', '', '', 0, '2014-09-14 11:36:27', '2015-03-02 09:44:18', ''),
(NULL, 'customer_id', '1', '客户编号', 'integer', 'TcTrack', 'zh_cn', '11', NULL, '1', '1', 'Customer', 'id', 'name', NULL, '1', '', '0', '', '', '', 'select', '', '1', '', NULL, '', '', '', 0, '2014-09-14 11:32:50', '2014-09-14 11:32:50', ''),
(NULL, 'contact_time', '1', '联系时间', 'datetime', 'TcTrack', 'en_us', '19', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'datetime', '', '1', '', NULL, '', '', '', 0, '2015-03-02 09:13:17', '2015-03-02 09:37:45', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`) VALUES (NULL, 'TcTrack', '客户跟踪', '', 'default', '', 27, '2015-02-24 20:12:55', '2015-02-24 20:12:55', 'miao_tc_tracks', '', 'branch', '', '0', 100, 0);
