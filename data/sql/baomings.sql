set names utf8;
CREATE TABLE IF NOT EXISTS `miao_baomings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(300) DEFAULT '',
  `data_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `published` tinyint(3) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `price` float(10,2) DEFAULT '0.00',
  `is_virtual` smallint(3) DEFAULT '0',
  `storage` int(11) DEFAULT '0',
  `saled` int(11) DEFAULT '0',
  `model` varchar(32) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Baoming', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-06 17:21:02', '2017-06-06 17:21:02', NULL),
(NULL, 'name', '1', '报名项名称', 'string', 'Baoming', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2017-06-06 17:21:02', '2017-06-07 09:03:03', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'Baoming', NULL, '300', 5, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-06 17:21:02', '2017-06-06 17:21:02', NULL),
(NULL, 'data_id', '1', '活动编号', 'integer', 'Baoming', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2017-06-06 17:21:02', '2017-06-16 07:27:37', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'Baoming', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-06 17:21:02', '2017-06-06 17:21:02', NULL),
(NULL, 'published', '1', '是否发布', 'integer', 'Baoming', NULL, '11', 3, '1', '1', '', NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-06 17:21:02', '2017-06-06 17:21:02', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Baoming', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-06 17:21:02', '2017-06-06 17:21:02', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Baoming', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-06 17:21:02', '2017-06-06 17:21:02', NULL),
(NULL, 'price', '1', '价格', 'float', 'Baoming', NULL, '10,2', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '0.00', '1', '', NULL, '', '', '', 0, '2017-06-06 17:50:15', '2017-06-06 17:50:35', NULL),
(NULL, 'is_virtual', '1', '是否虚拟产品', 'integer', 'Baoming', NULL, '3', 0, '1', '1', '', '', '', 0, '1', '1=>是\r\n0=>否', '0', '', '', '', 'select', '1', '1', '', NULL, '', '', '', 0, '2017-06-06 17:50:15', '2017-06-06 17:51:01', NULL),
(NULL, 'storage', '1', '库存量', 'integer', 'Baoming', NULL, '11', 0, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-06 17:50:15', '2017-06-06 17:50:15', NULL),
(NULL, 'saled', '1', '销售量', 'integer', 'Baoming', NULL, '11', 0, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-06 17:50:15', '2017-06-06 17:50:15', NULL),
(NULL, 'model', '1', '活动模块', 'string', 'Baoming', 'zh_cn', '32', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-06-16 07:28:12', '2017-06-16 07:28:12', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Baoming', '报名表', '', 'default', '', 0, '2017-06-06 17:21:02', '2017-06-11 21:00:45', 'miao_baomings', '', '', '', '0', NULL, NULL, '0', '0', '');
