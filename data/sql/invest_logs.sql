set names utf8;
CREATE TABLE IF NOT EXISTS `miao_invest_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(200) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `investor_id` int(11) DEFAULT '0',
  `project_id` int(11) DEFAULT '0',
  `amount` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'InvestLog', NULL, '11', 11, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-14 09:14:34', '2014-08-14 09:14:34', NULL),
(NULL, 'cate_id', '1', '项目投资阶段', 'integer', 'InvestLog', NULL, '11', 10, '1', '1', '', '', '', 0, '1', '1=>种子\r\n2=>天使\r\n3=>A轮\r\n4=>B轮\r\n5=>C轮\r\n6=>D轮', '0', '', '', '', 'select', '', '1', '', NULL, '', '', '', 0, '2014-08-14 09:14:34', '2014-08-14 22:48:14', ''),
(NULL, 'creator', '1', '编创建者', 'integer', 'InvestLog', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-14 09:14:34', '2014-08-14 09:14:34', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'InvestLog', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-14 09:14:34', '2014-08-14 09:14:34', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'InvestLog', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-14 09:14:34', '2014-08-14 09:14:34', NULL),
(NULL, 'investor_id', '1', '投资方', 'integer', 'InvestLog', NULL, '11', 9, '1', '1', 'Investor', 'id', 'name', 0, '1', '', '0', '', '', '', 'select', '', '1', '', NULL, '', '', '', 0, '2014-08-14 09:40:30', '2014-08-14 09:41:17', ''),
(NULL, 'project_id', '1', '投资项目', 'integer', 'InvestLog', NULL, '11', 8, '1', '1', 'Project', 'id', 'name', 0, '1', '', '0', '', '', '', 'select', '', '1', '', NULL, '', '', '', 0, '2014-08-14 09:40:30', '2014-08-14 09:41:52', ''),
(NULL, 'amount', '1', '投资金额', 'string', 'InvestLog', NULL, '20', 7, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-14 09:40:30', '2014-08-14 09:40:30', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'InvestLog', '投资记录', '', 'default', '', 27, '2014-08-14 09:14:34', '2014-08-14 09:14:34', 'miao_invest_logs', '', '', '', '0', NULL, 0, '0', '0', '');
