set names utf8;
CREATE TABLE IF NOT EXISTS `miao_smtps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) DEFAULT '',
  `smtp_password` varchar(64) DEFAULT '',
  `web_password` varchar(64) DEFAULT '',
  `creator` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `smtp_host` varchar(100) DEFAULT '',
  `smtp_port` int(11) DEFAULT '0',
  `smtp_ssl` smallint(3) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'username', '1', '邮箱地址', 'string', 'Smtp', NULL, '64', 10, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2016-01-24 11:00:56', '2016-01-24 11:04:43', NULL),
(NULL, 'id', '1', '编号', 'integer', 'Smtp', NULL, '11', 11, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-01-24 11:00:56', '2016-01-24 11:00:56', NULL),
(NULL, 'smtp_password', '1', '密码', 'string', 'Smtp', NULL, '64', 9, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', 'smtp发送邮件的密码', 0, '2016-01-24 11:00:56', '2016-01-24 11:07:27', NULL),
(NULL, 'web_password', '1', '网页登录密码', 'string', 'Smtp', NULL, '64', 8, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '网易163邮箱的网页登录密码与smtp发送邮件的密码不一致', 0, '2016-01-24 11:00:56', '2016-01-24 11:08:16', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'Smtp', NULL, '11', 4, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-01-24 11:00:56', '2016-01-24 11:00:56', NULL),
(NULL, 'status', '1', '状态', 'integer', 'Smtp', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '0=>无效\r\n1=>有效', '0', '', '', 'equal', 'select', '1', '1', '', NULL, '', '', '', 0, '2016-01-24 11:00:56', '2016-01-24 11:09:09', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Smtp', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-01-24 11:00:56', '2016-01-24 11:00:56', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Smtp', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-01-24 11:00:56', '2016-01-24 11:00:56', NULL),
(NULL, 'smtp_host', '1', 'host地址', 'string', 'Smtp', 'zh_cn', '100', 7, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-01-24 11:21:44', '2016-01-24 11:21:44', NULL),
(NULL, 'smtp_port', '1', 'smtp端口', 'integer', 'Smtp', 'zh_cn', '11', 6, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-01-24 11:22:10', '2016-01-24 11:26:31', NULL),
(NULL, 'smtp_ssl', '1', 'ssl加密', 'integer', 'Smtp', 'zh_cn', '3', 5, '1', '1', '', '', '', NULL, '1', '0=>否\r\n1=>是', '0', '', '', '', 'select', '0', '1', '', NULL, '', '', '', 0, '2016-01-24 11:23:22', '2016-01-24 11:23:22', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Smtp', 'smtp邮箱', '', 'default', '', 27, '2016-01-24 11:00:56', '2017-10-20 10:26:18', 'miao_smtps', '', '', '', '0', NULL, 0, '0', '0', '系统从smtp邮箱列表中随机选取一个有效状态的邮箱发送找回密码、邮箱激活等邮件。QQ邮箱接收有频次限制，可配置多个邮箱发送。建议使用阿里云邮件推送服务。');
