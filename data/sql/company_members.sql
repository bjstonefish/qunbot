set names utf8;
CREATE TABLE IF NOT EXISTS `miao_company_members` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `role_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `company_id` (`company_id`,`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'CompanyMember', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-17 14:29:33', '2016-11-17 14:29:33', NULL),
(NULL, 'company_id', '1', '公司编号', 'integer', 'CompanyMember', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2016-11-17 14:29:33', '2016-11-17 14:30:43', NULL),
(NULL, 'user_id', '1', '用户编号', 'integer', 'CompanyMember', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2016-11-17 14:29:33', '2016-11-17 14:31:03', NULL),
(NULL, 'status', '1', '状态', 'integer', 'CompanyMember', NULL, '11', 3, '1', '1', '', NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-17 14:29:33', '2016-11-17 14:29:33', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'CompanyMember', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-17 14:29:33', '2016-11-17 14:29:33', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'CompanyMember', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-17 14:29:33', '2016-11-17 14:29:33', NULL),
(NULL, 'role_id', '1', '公司担任角色', 'integer', 'CompanyMember', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-11-24 13:17:34', '2016-11-24 13:17:34', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'CompanyMember', '公司成员', '', 'default', '', 0, '2016-11-17 14:29:33', '2016-11-17 14:29:33', 'miao_company_members', '', '', '', '0', NULL, NULL, '0', '0', '');
