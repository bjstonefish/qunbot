set names utf8;
CREATE TABLE IF NOT EXISTS `miao_taskings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '0',
  `deleted` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT '0',
  `organize_id` varchar(100) DEFAULT NULL,
  `price` varchar(32) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_id` (`task_id`,`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Tasking', 'zh_cn', '11', 8, '0', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'status', '1', '任务分配状态', 'integer', 'Tasking', 'zh_cn', '11', 4, '1', '1', '', '', '', NULL, '1', '1=> 确认接受任务\r\n2=> 任务已按时完成\r\n3=> 任务延时完成\r\n4=> 任务失败\r\n\r\n9=> 任务已过期（未接受分配任务）\r\n10=>分配任务', '0', '', '', 'treenode', 'select', '0', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '2014-08-12 18:49:41', ''),
(NULL, 'deleted', '1', '是否删除', 'integer', 'Tasking', 'zh_cn', '11', 3, '0', '1', NULL, NULL, NULL, NULL, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Tasking', 'zh_cn', NULL, 2, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Tasking', 'zh_cn', NULL, 1, '0', '0', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'task_id', '1', '任务id', 'integer', 'Tasking', 'zh_cn', NULL, 7, '1', '1', 'Task', 'id', 'name', NULL, '1', '', '0', '', '', 'equal', 'select', '', '0', '', '', '', 'none', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'user_id', '1', '用户编号', 'integer', 'Tasking', 'zh_cn', '11', 5, '1', '1', '', 'staff_id', 'staff_id', NULL, '1', '', '1', 'organize_id', 'organize_id', '', 'input', '', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '2014-10-04 20:34:16', ''),
(NULL, 'organize_id', '1', '职员部门', 'string', 'Tasking', 'zh_cn', '100', 6, '1', '1', 'Organization', 'id', 'name', NULL, '1', '', '0', '', '', 'equal', 'select', '', '0', '', NULL, '', 'none', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'price', '1', '报价', 'string', 'Tasking', NULL, '32', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-10-10 17:00:18', '2014-10-10 17:00:18', NULL),
(NULL, 'start_date', '1', '开始日期', 'date', 'Tasking', NULL, '10', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-10-10 17:00:18', '2014-10-10 17:00:18', NULL),
(NULL, 'end_date', '1', '结束日期', 'date', 'Tasking', NULL, '10', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-10-10 17:00:18', '2014-10-10 17:00:18', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Tasking', '任务分配', 'onetomany', 'default', '<id>', 1, '2010-08-06 15:51:35', '2010-08-06 15:51:35', 'miao_taskings', NULL, NULL, NULL, '0', 0, 0, '0', '0', '');
