set names utf8;
CREATE TABLE IF NOT EXISTS `miao_wx_auto_qas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `coverimg` varchar(200) DEFAULT NULL,
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `msg_type` varchar(20) DEFAULT NULL,
  `media_id` varchar(200) DEFAULT NULL,
  `content` text,
  `answer` text,
  `view_nums` int(11) DEFAULT '0',
  `answer_times` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'WxAutoQa', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-22 15:31:21', '2015-05-22 15:31:21', NULL),
(NULL, 'name', '1', '问题标题', 'string', 'WxAutoQa', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', '', '', '1', '', NULL, '', '', '', 0, '2015-05-22 15:31:21', '2015-05-22 15:58:51', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'WxAutoQa', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'file', '', '1', '', NULL, '', '', '', 0, '2015-05-22 15:31:21', '2015-05-22 18:02:45', NULL),
(NULL, 'cate_id', '1', '分类', 'integer', 'WxAutoQa', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-22 15:31:21', '2015-05-22 15:31:21', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'WxAutoQa', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-22 15:31:21', '2015-05-22 15:31:21', NULL),
(NULL, 'status', '1', '状态', 'integer', 'WxAutoQa', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-22 15:31:21', '2015-05-22 15:31:21', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'WxAutoQa', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-22 15:31:21', '2015-05-22 15:31:21', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'WxAutoQa', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-22 15:31:21', '2015-05-22 15:31:21', NULL),
(NULL, 'msg_type', '1', '回复类型', 'string', 'WxAutoQa', NULL, '20', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'select', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-22 16:05:11', '2015-05-22 16:05:11', NULL),
(NULL, 'media_id', '1', '多媒体编号', 'string', 'WxAutoQa', NULL, '200', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-22 16:05:11', '2015-05-22 16:05:11', NULL),
(NULL, 'content', '1', '问题内容', 'content', 'WxAutoQa', NULL, '65575', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'ckeditor', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-22 16:05:11', '2015-05-22 16:05:11', NULL),
(NULL, 'answer', '1', '问题答案', 'content', 'WxAutoQa', NULL, '65575', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2015-05-22 16:05:11', '2015-06-12 16:04:50', NULL),
(NULL, 'view_nums', '1', '查看次数', 'integer', 'WxAutoQa', NULL, '11', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-12 15:53:25', '2015-06-12 15:53:25', NULL),
(NULL, 'answer_times', '1', '回答次数', 'integer', 'WxAutoQa', NULL, '11', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-12 15:53:25', '2015-06-12 15:53:25', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`) VALUES (NULL, 'WxAutoQa', '自动问答', '', 'default', '', 27, '2015-05-22 15:31:21', '2015-05-22 15:31:21', 'miao_wx_auto_qas', '', '', '', '0', 2, 0);
