set names utf8;
CREATE TABLE IF NOT EXISTS `miao_wx_short_msgs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `event` varchar(32) DEFAULT '',
  `wx_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `user_openid` varchar(44) DEFAULT '',
  `wall_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `wx_id` (`wx_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'WxShortMsg', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-19 12:03:07', '2015-05-19 12:03:07', NULL),
(NULL, 'name', '1', '名称', 'string', 'WxShortMsg', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-19 12:03:07', '2015-05-19 12:03:07', NULL),
(NULL, 'event', '1', '事件', 'string', 'WxShortMsg', NULL, '32', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-05-19 12:03:07', '2015-06-03 23:34:20', NULL),
(NULL, 'wx_id', '1', '微信编号', 'integer', 'WxShortMsg', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-05-19 12:03:07', '2015-06-03 23:35:07', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'WxShortMsg', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 1, '2015-05-19 12:03:07', '2015-05-19 12:03:07', NULL),
(NULL, 'status', '1', '状态', 'integer', 'WxShortMsg', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-19 12:03:07', '2015-05-19 12:03:07', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'WxShortMsg', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-05-19 12:03:07', '2015-05-19 12:03:07', NULL),
(NULL, 'user_openid', '1', '微信用户编号', 'string', 'WxShortMsg', 'en_us', '44', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-03 23:36:01', '2015-06-03 23:36:01', NULL),
(NULL, 'wall_id', '1', '所属微信墙', 'integer', 'WxShortMsg', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-28 14:39:26', '2015-06-28 14:39:26', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`) VALUES (NULL, 'WxShortMsg', '微信短消息', '', 'default', '', 27, '2015-05-19 12:03:05', '2015-06-06 06:45:00', 'miao_wx_short_msgs', '', '', '', '0', 2, 0);
