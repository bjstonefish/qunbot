set names utf8;
CREATE TABLE IF NOT EXISTS `miao_app_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `open_id` varchar(32) DEFAULT '',
  `username` varchar(60) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `email` varchar(32) DEFAULT '',
  `avatar` varchar(300) DEFAULT '',
  `created` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `sex` smallint(3) DEFAULT '0',
  `union_id` varchar(44) DEFAULT '',
  `location` varchar(200) DEFAULT '',
  `city` varchar(60) DEFAULT NULL,
  `province` varchar(60) DEFAULT NULL,
  `mobile` varchar(18) DEFAULT NULL,
  `wechat` varchar(18) DEFAULT NULL,
  `score` int(11) DEFAULT '0',
  `area` varchar(24) DEFAULT '',
  `app_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_id` (`app_id`,`open_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'AppUser', 'zh_cn', '11', 18, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'input', '', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '2017-01-13 13:57:36', NULL),
(NULL, 'open_id', '1', '开放编号', 'string', 'AppUser', 'zh_cn', '32', 17, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', '', '', 'implode', '', 0, '0000-00-00 00:00:00', '2017-01-13 13:53:55', ''),
(NULL, 'username', '1', '用户名', 'string', 'AppUser', 'zh_cn', '60', 16, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', '', '', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(NULL, 'password', '1', '密码', 'string', 'AppUser', 'zh_cn', '100', 15, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', '', '', '1', '', '', '', '', '当不修改密码时，请保留为空', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(NULL, 'email', '1', '邮箱', 'string', 'AppUser', 'zh_cn', '32', 11, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', '', '', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '2017-01-13 13:52:09', ''),
(NULL, 'avatar', '1', '头像', 'string', 'AppUser', 'zh_cn', '300', 8, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '2017-01-13 13:56:36', ''),
(NULL, 'created', '1', '创建时间', 'datetime', 'AppUser', 'zh_cn', NULL, 2, '0', '0', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'last_login', '1', '上次登录时间', 'datetime', 'AppUser', 'zh_cn', NULL, 5, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'sex', '1', '性别', 'integer', 'AppUser', 'zh_cn', '3', 13, '1', '1', '', '', '', NULL, '1', '0=>未知\r\n1=>男\r\n2=>女', '0', '', '', 'equal', 'select', '1', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '2016-12-05 18:01:16', NULL),
(NULL, 'union_id', '1', '联合编号', 'string', 'AppUser', 'zh_cn', '44', 12, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'input', '', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '2017-01-13 13:55:13', NULL),
(NULL, 'province', '1', '省份', 'string', 'AppUser', 'zh_cn', '60', NULL, '1', '1', '', NULL, NULL, NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2010-10-17 12:44:17', '2010-10-17 12:44:17', NULL),
(NULL, 'city', '1', '城市', 'string', 'AppUser', 'zh_cn', '60', NULL, '1', '1', '', NULL, NULL, NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2010-10-17 12:43:53', '2010-10-17 12:43:53', NULL),
(NULL, 'location', '1', '详细地址', 'string', 'AppUser', 'zh_cn', '200', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2010-10-17 12:41:14', '2017-01-13 14:01:07', NULL),
(NULL, 'wechat', '1', '个人微信号', 'string', 'AppUser', NULL, '18', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-06 17:12:01', '2015-01-06 17:12:01', NULL),
(NULL, 'mobile', '1', '手机号码', 'string', 'AppUser', NULL, '18', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-06 17:12:01', '2015-01-06 17:12:01', NULL),
(NULL, 'app_id', '1', '小程序编号', 'integer', 'AppUser', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-01-13 14:11:15', '2017-01-13 14:11:15', NULL),
(NULL, 'score', '1', '积分', 'integer', 'AppUser', 'en_us', '11', NULL, '0', '0', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '', 0, '2015-03-08 06:53:19', '2015-03-08 06:53:19', NULL),
(NULL, 'area', '1', '所在区', 'string', 'AppUser', 'zh_cn', '24', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-01-13 14:00:49', '2017-01-13 14:00:49', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'AppUser', 'App用户', 'onetomany', 'default', '', 0, '2017-01-13 13:30:24', '2017-01-13 13:30:24', 'miao_app_users', '', '', '', '0', NULL, 0, '0', '0', '');
