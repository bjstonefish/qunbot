set names utf8;
CREATE TABLE IF NOT EXISTS `miao_item_marks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `seconds` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `author` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', 'id', 'integer', 'ItemMark', NULL, '11', 0, '0', '0', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-07-03 16:25:59', '2017-07-03 16:25:59', NULL),
(NULL, 'name', '1', 'name', 'string', 'ItemMark', NULL, '200', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-07-03 16:26:00', '2017-07-03 17:12:10', NULL),
(NULL, 'cate_id', '1', 'cate_id', 'integer', 'ItemMark', NULL, '11', 0, '0', '0', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, '', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-07-03 16:26:00', '2017-07-03 16:26:00', NULL),
(NULL, 'creator', '1', 'creator', 'integer', 'ItemMark', NULL, '11', 0, '0', '0', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, '', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-07-03 16:26:00', '2017-07-03 16:26:00', NULL),
(NULL, 'seconds', '1', 'seconds', 'integer', 'ItemMark', NULL, '11', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '', 0, '2017-07-03 16:26:00', '2017-07-03 17:12:23', NULL),
(NULL, 'created', '1', 'created', 'datetime', 'ItemMark', NULL, '19', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'datetime', '', '1', '', NULL, '', '', '', 0, '2017-07-03 16:26:00', '2017-07-03 17:12:38', NULL),
(NULL, 'author', '1', '评分对象', 'string', 'ItemMark', 'zh_cn', '32', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-07-04 09:57:26', '2017-07-04 09:57:26', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'ItemMark', '项目评分', '', 'default', '', 0, '2017-06-26 15:15:46', '2017-06-26 16:08:50', 'miao_item_marks', '', '', '', '0', 1, NULL, '0', '0', '');
