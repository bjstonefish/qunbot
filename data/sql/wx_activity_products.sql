set names utf8;
CREATE TABLE IF NOT EXISTS `miao_wx_activity_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `aid` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `pid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `apid` (`aid`,`pid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'WxActivityProduct', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-17 14:29:33', '2016-11-17 14:29:33', NULL),
(NULL, 'aid', '1', '活动编号', 'integer', 'WxActivityProduct', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2016-11-17 14:29:33', '2017-10-15 08:20:50', NULL),
(NULL, 'creator', '1', '用户编号', 'integer', 'WxActivityProduct', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2016-11-17 14:29:33', '2017-10-15 08:19:35', NULL),
(NULL, 'status', '1', '状态', 'integer', 'WxActivityProduct', NULL, '11', 3, '1', '1', '', NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-17 14:29:33', '2016-11-17 14:29:33', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'WxActivityProduct', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-17 14:29:33', '2016-11-17 14:29:33', NULL),
(NULL, 'pid', '1', '产品编号', 'integer', 'WxActivityProduct', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-11-24 13:17:34', '2017-10-15 08:21:10', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'WxActivityProduct', '活动分销商品', '', 'default', '', 0, '2017-10-15 08:18:08', '2017-10-15 08:18:08', 'miao_wx_activity_products', '', '', '', '0', NULL, NULL, '0', '0', '');
