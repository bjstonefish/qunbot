set names utf8;
CREATE TABLE IF NOT EXISTS `miao_customer_tracks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(2000) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `customer_id` int(11) DEFAULT '0',
  `contact_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'CustomerTrack', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-09-14 11:20:53', '2014-09-14 11:20:53', NULL),
(NULL, 'name', '1', '名称', 'string', 'CustomerTrack', NULL, '2000', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'textarea', '', '1', '', NULL, '', '', '', 0, '2014-09-14 11:20:53', '2016-05-05 20:56:23', NULL),
(NULL, 'cate_id', '1', '分类', 'integer', 'CustomerTrack', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-09-14 11:20:53', '2014-09-14 11:20:53', NULL),
(NULL, 'creator', '1', '编创建者', 'integer', 'CustomerTrack', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-09-14 11:20:53', '2014-09-14 11:20:53', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'CustomerTrack', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-09-14 11:20:53', '2014-09-14 11:20:53', NULL),
(NULL, 'contact_id', '1', '客户联系人编号', 'integer', 'CustomerTrack', 'zh_cn', '11', NULL, '1', '1', '', 'id', 'name', NULL, '1', '', '1', 'customer_id', 'CustomerId', '', 'select', '', '1', '', NULL, '', '', '', 0, '2014-09-14 11:36:27', '2016-05-05 21:01:04', ''),
(NULL, 'customer_id', '1', '客户编号', 'integer', 'CustomerTrack', 'zh_cn', '11', NULL, '1', '1', 'Customer', 'id', 'name', NULL, '1', '', '0', '', '', '', 'select', '', '1', '', NULL, '', '', '', 0, '2014-09-14 11:32:50', '2014-09-14 11:32:50', '');
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'CustomerTrack', '客户跟踪', '', 'default', '', 27, '2014-09-14 11:20:53', '2014-09-14 11:20:53', 'miao_customer_tracks', '', 'branch', '', '0', 100, 0, '0', '0', '');
