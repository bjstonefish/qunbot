set names utf8;
CREATE TABLE IF NOT EXISTS `miao_app_forum_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(5000) DEFAULT '',
  `coverimg` varchar(300) DEFAULT '',
  `forum_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `published` tinyint(3) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `thread_id` int(11) DEFAULT '0',
  `app_id` int(11) DEFAULT '0',
  `cate_id` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `praise_nums` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `app_id` (`app_id`,`forum_id`,`thread_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'status', '1', '状态', 'integer', 'AppForumComment', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '如是否审核，是否采纳等', 0, '2017-01-17 06:56:01', '2017-01-17 06:56:01', NULL),
(NULL, 'praise_nums', '1', '赞数目', 'integer', 'AppForumComment', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-01-17 07:18:46', '2017-01-17 07:18:46', NULL),
(NULL, 'id', '1', '编号', 'integer', 'AppForumComment', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-01-13 13:28:17', '2017-01-13 13:28:17', NULL),
(NULL, 'content', '1', '评论内容', 'string', 'AppForumComment', NULL, '5000', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'textarea', '', '1', '', NULL, '', '', '', 0, '2017-01-13 13:28:17', '2017-01-16 21:24:19', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'AppForumComment', NULL, '300', 5, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 1, '2017-01-13 13:28:17', '2017-01-13 13:28:17', NULL),
(NULL, 'forum_id', '1', '论坛编号', 'integer', 'AppForumComment', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2017-01-13 13:28:17', '2017-01-13 14:21:07', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'AppForumComment', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-01-13 13:28:17', '2017-01-13 13:28:17', NULL),
(NULL, 'published', '1', '是否发布', 'integer', 'AppForumComment', NULL, '11', 3, '1', '1', '', NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-01-13 13:28:17', '2017-01-13 13:28:17', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'AppForumComment', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-01-13 13:28:17', '2017-01-13 13:28:17', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'AppForumComment', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-01-13 13:28:17', '2017-01-13 13:28:17', NULL),
(NULL, 'thread_id', '1', '帖子编号', 'integer', 'AppForumComment', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-01-13 14:20:54', '2017-01-13 14:20:54', NULL),
(NULL, 'cate_id', '1', '评论分类', 'integer', 'AppForumComment', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-01-16 21:25:58', '2017-01-16 21:25:58', NULL),
(NULL, 'app_id', '1', 'App编号', 'integer', 'AppForumComment', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-01-13 14:21:27', '2017-01-13 14:21:27', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'AppForumComment', '论坛评论', '', 'default', '', 0, '2017-01-13 13:28:17', '2017-01-13 13:28:17', 'miao_app_forum_comments', '', '', '', '0', 2, NULL, '0', '0', '');
