set names utf8;
CREATE TABLE IF NOT EXISTS `miao_wx_variables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `value` varchar(1000) DEFAULT NULL,
  `wx_id` int(11) NOT NULL DEFAULT '0',
  `creator` int(11) NOT NULL DEFAULT '0',
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`wx_id`,`creator`,`id`),
  KEY `creator` (`creator`),
  KEY `creator_2` (`creator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'WxVariable', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-05-16 18:29:19', '2014-05-16 18:29:19', NULL),
(NULL, 'name', '1', '名称', 'string', 'WxVariable', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-05-16 18:29:19', '2014-05-16 18:29:19', NULL),
(NULL, 'value', '1', '参数值', 'string', 'WxVariable', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-05-16 18:29:19', '2014-05-16 18:29:19', ''),
(NULL, 'wx_id', '1', '公众号编号', 'integer', 'WxVariable', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-05-16 18:29:19', '2014-05-16 18:29:19', ''),
(NULL, 'creator', '1', '编创建者', 'integer', 'WxVariable', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-05-16 18:29:19', '2014-05-16 18:29:19', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'WxVariable', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-05-16 18:29:19', '2014-05-16 18:29:19', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'WxVariable', '微信参数设置', '', 'default', '', 27, '2014-05-16 18:29:19', '2014-05-16 18:29:19', 'miao_wx_variables', '', 'self', '', '0', 1, 0, '0', '0', '');
