set names utf8;
CREATE TABLE IF NOT EXISTS `miao_category_i18ns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `locale` varchar(10) DEFAULT NULL,
  `foreign_key` int(11) DEFAULT '0',
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '0', 'id', 'integer', 'CategoryI18n', NULL, '11', 0, '0', '0', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2013-06-22 23:49:38', '2013-06-22 23:49:38', NULL),
(NULL, 'name', '0', 'name', 'string', 'CategoryI18n', NULL, '200', 0, '0', '0', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, NULL, '', '1', NULL, NULL, NULL, NULL, NULL, 0, '2013-06-22 23:49:38', '2013-06-22 23:49:38', NULL),
(NULL, 'content', '1', '栏目内容', 'content', 'CategoryI18n', 'zh_cn', '65535', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'ckeditor', '', '1', '', NULL, '', 'none', '', 0, '2014-11-09 12:40:38', '2014-11-09 12:40:38', NULL),
(NULL, 'deleted', '0', 'deleted', 'boolean', 'CategoryI18n', NULL, '1', 0, '0', '0', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, NULL, '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2013-06-22 23:49:38', '2013-06-22 23:49:38', NULL),
(NULL, 'locale', '0', 'locale', 'string', 'CategoryI18n', NULL, '10', 0, '0', '0', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2013-06-22 23:49:38', '2013-06-22 23:49:38', NULL),
(NULL, 'foreign_key', '0', 'foreign_key', 'integer', 'CategoryI18n', NULL, '11', 0, '0', '0', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, NULL, '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2013-06-22 23:49:38', '2013-06-22 23:49:38', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'CategoryI18n', 'CategoryI18n', 'onetomany', 'default', NULL, 1, '2013-10-16 22:09:16', '2013-10-16 22:09:16', 'miao_category_i18ns', NULL, NULL, NULL, '0', 0, 0, '0', '0', '');
