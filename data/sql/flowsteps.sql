set names utf8;
CREATE TABLE IF NOT EXISTS `miao_flowsteps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '0',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `name` varchar(60) DEFAULT NULL,
  `flowmodel` varchar(60) DEFAULT NULL,
  `allowactions` varchar(60) DEFAULT NULL,
  `operate_fields` text,
  `conditions` text,
  `flow_id` int(11) DEFAULT '0',
  `slug` varchar(80) DEFAULT NULL,
  `content` text,
  `template` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Flowstep', 'zh_cn', '11', 16, '0', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'status', '1', '是否启用', 'integer', 'Flowstep', 'zh_cn', '11', 4, '1', '1', '', 'id', 'name', 25, '1', '0=>否\r\n1=>是', '0', '', '', 'treenode', 'select', '1', '1', '', NULL, '', '', '', 0, '0000-00-00 00:00:00', '2014-11-30 10:37:25', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Flowstep', 'zh_cn', NULL, 2, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Flowstep', 'zh_cn', NULL, 1, '0', '0', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'name', '1', '步骤名称', 'string', 'Flowstep', 'zh_cn', '60', 15, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', 'none', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'flowmodel', '1', '操作模块', 'string', 'Flowstep', 'zh_cn', '60', 12, '1', '1', 'Modelextend', 'name', 'cname', NULL, '1', '', '0', '', '', 'equal', 'select', '', '1', '', NULL, '', 'none', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'allowactions', '1', '允许的操作', 'string', 'Flowstep', 'zh_cn', '60', 11, '1', '1', '', '', '', NULL, '1', 'add=>添加\r\nedit=>修改\r\nview=>查看\r\ntrash=>回收站\r\nrestore=>恢复\r\ndelete=>删除', '0', '', '', '', 'checkbox', '', '1', '', NULL, '', 'implode', '选择相关的操作时，能自动进入列表页，看到有权限操作的数据以及字段信息。\r\n点开对应的权限操作菜单，先显示数据列表，选择单条数据后可进行相应操作。', 0, '0000-00-00 00:00:00', '2014-11-30 18:31:24', ''),
(NULL, 'operate_fields', '1', '允许操作的字段', 'content', 'Flowstep', 'zh_cn', '65535', 10, '1', '1', 'I18nfield', 'name', 'translate', NULL, '1', '', '1', 'flowmodel', 'model', '', 'textarea', '', '1', '', NULL, '', 'none', '', 0, '0000-00-00 00:00:00', '2014-11-26 10:57:27', NULL),
(NULL, 'conditions', '1', '可操作数据筛选条件', 'content', 'Flowstep', 'zh_cn', '', 6, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', 'none', 'JSON格式，设置conditions,joins,contain,recursive,limit,order等所有find查询参数的条件', 0, '0000-00-00 00:00:00', '2014-11-26 11:10:02', NULL),
(NULL, 'flow_id', '1', '所属流程', 'integer', 'Flowstep', 'zh_cn', NULL, 13, '1', '1', 'Flow', 'id', 'name', NULL, '1', '', '0', '', '', 'equal', 'select', '', '1', '', NULL, '', 'none', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'slug', '1', '链接文字', 'string', 'Flowstep', 'zh_cn', '80', 14, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '链接别名请使用英文、数字、下划线，不要出现中文与空格等非法字符', 0, '2013-02-02 10:23:36', '2013-02-02 10:23:36', ''),
(NULL, 'content', '1', '流程说明', 'content', 'Flowstep', 'zh_cn', '65535', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'ckeditor', '', '1', '', NULL, '', '', '', 0, '2013-02-02 11:17:46', '2013-02-02 11:17:46', ''),
(NULL, 'template', '1', '自定义操作模板内容', 'content', 'Flowstep', 'en_us', '65535', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '自定义了模板的安装自定义模板显示操作界面，否则安装默认模板显示操作界面', 0, '2014-11-26 11:03:13', '2014-11-26 11:03:37', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Flowstep', '流程步骤', 'onetomany', 'default', '<id>', 27, '2010-08-16 17:36:21', '2010-08-16 17:36:21', 'miao_flowsteps', NULL, NULL, NULL, '0', 0, 0, '0', '0', '');
