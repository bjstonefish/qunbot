set names utf8;
CREATE TABLE IF NOT EXISTS `miao_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `alias` varchar(100) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `group` smallint(3) DEFAULT '0',
  `content` text,
  `price` float(10,2) DEFAULT '0.00',
  `max_upload_size` int(11) DEFAULT '0',
  `total_upload_size` int(11) DEFAULT '0',
  `is_virtual` smallint(3) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Role', 'zh_cn', '11', 5, '0', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'name', '1', '角色名称', 'string', 'Role', 'zh_cn', '100', 4, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(NULL, 'alias', '1', '别名', 'string', 'Role', 'zh_cn', '100', 3, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', '', '', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(NULL, 'created', '1', '创建时间', 'datetime', 'Role', 'zh_cn', NULL, 2, '1', '1', '', NULL, NULL, NULL, '1', '', '0', '', '', 'equal', 'datetime', '', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Role', 'zh_cn', NULL, 1, '0', '0', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'deleted', '1', '是否删除', 'integer', 'Role', 'zh_cn', '11', 0, '0', '1', NULL, NULL, NULL, NULL, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'is_virtual', '1', '是否虚拟商品', 'integer', 'Role', 'en_us', '3', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '1', '1', '', NULL, '', '', '1 for Yes, 0 for No', 0, '2015-03-18 19:09:08', '2015-03-18 19:09:08', NULL),
(NULL, 'group', '1', '角色组', 'integer', 'Role', 'en_us', '3', NULL, '1', '1', '', '', '', NULL, '1', '0=>后台用户组\r\n1=>前台用户组', '0', '', '', '', 'select', '1', '0', '', NULL, '', '', '', 0, '2015-03-15 09:43:44', '2015-03-15 09:45:56', NULL),
(NULL, 'content', '1', '用户角色描述', 'content', 'Role', 'en_us', '65535', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'ckeditor', '', '1', '', NULL, '', '', '', 0, '2015-03-17 23:13:00', '2015-03-17 23:13:00', NULL),
(NULL, 'price', '1', '单价/月', 'float', 'Role', 'en_us', '10,2', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '每月单价，多次购买时，权限时间累加。仅可购买前台操作权限', 0, '2015-03-17 23:15:56', '2015-03-17 23:15:56', NULL),
(NULL, 'max_upload_size', '1', '最大上传文件大小', 'integer', 'Role', NULL, '11', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-03-17 23:29:40', '2015-03-17 23:29:40', NULL),
(NULL, 'total_upload_size', '1', '每月可上传文件总量', 'integer', 'Role', NULL, '11', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-03-17 23:29:40', '2015-03-17 23:29:40', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Role', '角色', 'onetomany', 'default', '', 26, '2010-06-30 23:06:27', '2010-06-30 23:06:27', 'miao_roles', NULL, NULL, NULL, '0', 0, 0, '0', '0', '');



REPLACE INTO `miao_roles` (`id`, `name`, `alias`, `deleted`, `created`, `updated`, `group`, `content`, `price`, `max_upload_size`, `total_upload_size`, `is_virtual`) VALUES (1, '系统管理组', 'admin', 0, '2009-04-05 00:10:34', '2016-07-18 09:20:13', 0, NULL, 100000000.00, 0, 0, 1),
(2, '注册用户', 'registered', 0, '2009-04-05 00:10:50', '2015-03-19 13:14:35', 1, '<p><br></p>', '0.00', 0, 0, 1),
(4, '内容管理组', 'content', 0, '2010-12-05 08:38:24', '2012-08-07 22:03:59', 0, NULL, '0.00', 0, 0, 0),
(101, '文件上传', 'upload', 0, '2015-03-18 11:11:32', '2016-05-31 22:25:08', 1, '<p>用户上传文件，每月15元</p>', 14.99, 5, 300, 1),
(3, '数据管理员组', 'DataManage', 0, '2016-07-18 09:19:45', '2016-07-18 09:19:45', 0, '<p><span style=\"color: #000000; font-family: 微软雅黑, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25.6px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: #FFFFFF;\">数据管理员能够管理所在模块的所有数据，不限是自己添加的内容</span></p>', 100000000.00, NULL, NULL, 1),
(8, '普通会员', 'putong', 0, '2016-05-16 22:04:10', '2017-05-19 23:10:37', 1, '<p><br/></p>', 11.00, NULL, NULL, 1),
(9, '高级会员', 'advance', 0, '2016-09-14 19:59:48', '2017-09-16 16:01:06', 1, '', 11.00, NULL, NULL, 1),
(102, '查看订单', 'view_order', 0, '2017-06-08 15:49:28', '2017-06-08 15:49:28', 0, '', NULL, NULL, NULL, 1);
