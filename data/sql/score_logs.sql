set names utf8;
CREATE TABLE IF NOT EXISTS `miao_score_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `score` int(11) DEFAULT '0',
  `data_id` int(11) DEFAULT '0',
  `last_score` int(11) DEFAULT '0',
  `score_date` date DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`rule_id`,`score_date`),
  KEY `rule_id` (`rule_id`,`score_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'ScoreLog', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-03-08 06:57:59', '2015-03-08 06:57:59', NULL),
(NULL, 'rule_id', '1', '积分规则', 'integer', 'ScoreLog', NULL, '11', 6, '1', '1', 'ScoreRule', 'id', 'name', 0, '1', '', '0', '', '', '', 'select', '', '1', '', NULL, '', '', '', 0, '2015-03-08 06:57:59', '2016-07-02 09:08:12', NULL),
(NULL, 'user_id', '1', '用户', 'integer', 'ScoreLog', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-03-08 06:57:59', '2015-03-08 06:59:16', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'ScoreLog', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-03-08 06:57:59', '2015-03-08 06:57:59', NULL),
(NULL, 'score', '1', '积分', 'integer', 'ScoreLog', NULL, '11', 1, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-03-08 06:57:59', '2015-03-08 07:00:44', NULL),
(NULL, 'data_id', '1', '相关数据', 'integer', 'ScoreLog', 'en_us', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-03-08 07:11:07', '2015-03-08 07:11:07', NULL),
(NULL, 'last_score', '1', '上次积分', 'integer', 'ScoreLog', NULL, '11', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-03-08 14:21:53', '2015-03-08 14:21:53', NULL),
(NULL, 'score_date', '1', '积分日期', 'date', 'ScoreLog', 'en_us', '10', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'date', '', '1', '', NULL, '', '', '', 0, '2015-03-09 09:47:21', '2015-03-09 09:47:21', NULL),
(NULL, 'name', '1', '相关数据标题', 'string', 'ScoreLog', 'en_us', '200', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-05-02 18:49:54', '2015-05-02 18:49:54', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'ScoreLog', '积分记录', '', 'default', '', 27, '2015-03-08 06:57:59', '2015-03-08 06:57:59', 'miao_score_logs', '', '', '', '0', NULL, 0, '0', '0', '');
