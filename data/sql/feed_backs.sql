set names utf8;
CREATE TABLE IF NOT EXISTS `miao_feed_backs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(2000) DEFAULT NULL,
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `qq` char(12) DEFAULT NULL,
  `phone` varchar(32) NOT NULL,
  `files` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', 'id', 'integer', 'FeedBack', NULL, '11', 0, '0', '0', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-29 17:30:27', '2016-04-29 17:30:27', NULL),
(NULL, 'name', '1', 'name', 'string', 'FeedBack', NULL, '2000', 0, '0', '0', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-29 17:30:27', '2016-04-29 17:30:27', NULL),
(NULL, 'cate_id', '1', 'cate_id', 'integer', 'FeedBack', NULL, '11', 0, '0', '0', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, NULL, '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-29 17:30:27', '2016-04-29 17:30:27', NULL),
(NULL, 'creator', '1', 'creator', 'integer', 'FeedBack', NULL, '11', 0, '0', '0', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, NULL, '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-29 17:30:27', '2016-04-29 17:30:27', NULL),
(NULL, 'created', '1', 'created', 'datetime', 'FeedBack', NULL, NULL, 0, '0', '0', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-29 17:30:27', '2016-04-29 17:30:27', NULL),
(NULL, 'email', '1', 'email', 'string', 'FeedBack', NULL, '30', 0, '0', '0', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-29 17:30:27', '2016-04-29 17:30:27', NULL),
(NULL, 'qq', '1', 'qq', 'string', 'FeedBack', NULL, '12', 0, '0', '0', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-29 17:30:27', '2016-04-29 17:30:27', NULL),
(NULL, 'phone', '1', 'phone', 'string', 'FeedBack', NULL, '32', 0, '0', '0', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-29 17:30:27', '2016-04-29 17:30:27', NULL),
(NULL, 'files', '1', '反馈附件', 'string', 'FeedBack', 'zh_cn', '255', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'file', '', '1', '', NULL, '', '', '', 0, '2017-10-19 09:59:32', '2017-10-19 09:59:32', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'FeedBack', 'FeedBack', 'onetomany', 'default', '', 1, '2014-07-16 23:45:21', '2017-10-19 10:25:19', 'miao_feed_backs', '', '', '', '0', NULL, 0, '0', '1', '');
