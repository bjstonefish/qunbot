set names utf8;
CREATE TABLE IF NOT EXISTS `miao_chapter_learn_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `chapter_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `minutes` int(11) DEFAULT '0',
  `content_type` int(11) DEFAULT '0',
  `cate_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `user_id_2` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'ChapterLearnLog', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-03 13:38:30', '2016-11-03 13:38:30', NULL),
(NULL, 'content_type', '1', '内容类型', 'integer', 'ChapterLearnLog', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '用于区分语音、内容时长', 0, '2016-11-03 13:45:12', '2016-11-04 08:41:34', NULL),
(NULL, 'chapter_id', '1', '章节编号', 'integer', 'ChapterLearnLog', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', '', '', '1', '', NULL, '', '', '', 0, '2016-11-03 13:38:30', '2016-11-03 13:39:26', NULL),
(NULL, 'user_id', '1', '学习人编号', 'integer', 'ChapterLearnLog', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2016-11-03 13:38:30', '2016-11-03 13:39:55', NULL),
(NULL, 'status', '1', '状态', 'integer', 'ChapterLearnLog', NULL, '11', 3, '1', '1', '', NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-03 13:38:30', '2016-11-03 13:38:30', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'ChapterLearnLog', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-03 13:38:30', '2016-11-03 13:38:30', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'ChapterLearnLog', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-03 13:38:30', '2016-11-03 13:38:30', NULL),
(NULL, 'minutes', '1', '学习时长', 'integer', 'ChapterLearnLog', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '学习时长，单位分钟', 0, '2016-11-03 13:41:54', '2016-11-03 13:41:54', NULL),
(NULL, 'cate_id', '1', '章节分类', 'integer', 'ChapterLearnLog', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '', 0, '2016-11-04 08:41:13', '2016-11-04 08:41:13', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'ChapterLearnLog', '章节学习记录', '', 'default', '', 0, '2016-11-03 13:38:30', '2016-11-03 13:38:30', 'miao_chapter_learn_logs', '', '', '', '0', NULL, NULL, '0', '0', '');
