set names utf8;
CREATE TABLE IF NOT EXISTS `miao_oauth_scopes` (
  `scope` text,
  `is_default` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
