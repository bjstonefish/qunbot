set names utf8;
CREATE TABLE IF NOT EXISTS `miao_aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(11) DEFAULT '0',
  `aco_id` int(11) DEFAULT '0',
  `_create` smallint(3) DEFAULT '0',
  `_read` smallint(3) DEFAULT '0',
  `_update` smallint(3) DEFAULT '0',
  `_delete` smallint(3) DEFAULT '0',
  `_alldata` tinyint(2) DEFAULT '0',
  `_list` smallint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'ArosAco', 'zh_cn', '10', 7, '0', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'aro_id', '1', '角色编号', 'integer', 'ArosAco', 'zh_cn', '11', 6, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'input', '', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '2017-06-08 10:29:35', NULL),
(NULL, 'aco_id', '1', '操作编号', 'integer', 'ArosAco', 'zh_cn', '11', 5, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'input', '', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '2017-06-08 10:29:18', NULL),
(NULL, '_create', '1', '创建', 'integer', 'ArosAco', 'zh_cn', '3', 4, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'input', '0', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '2017-06-08 10:28:50', NULL),
(NULL, '_read', '1', '查看', 'integer', 'ArosAco', 'zh_cn', '3', 3, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'input', '0', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '2017-06-08 10:28:34', NULL),
(NULL, '_update', '1', '修改', 'integer', 'ArosAco', 'zh_cn', '3', 2, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'input', '0', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '2017-06-08 10:28:18', NULL),
(NULL, '_delete', '1', '删除', 'integer', 'ArosAco', 'zh_cn', '3', 1, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'input', '0', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '2017-06-08 10:27:56', NULL),
(NULL, '_alldata', '1', '全量数据', 'integer', 'ArosAco', 'zh_cn', '2', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '', 0, '2017-03-24 21:04:57', '2017-03-24 21:04:57', NULL),
(NULL, '_list', '1', '列表', 'integer', 'ArosAco', 'zh_cn', '3', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '1', '1', '', NULL, '', '', '', 0, '2017-06-08 10:24:30', '2017-06-08 10:24:30', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'ArosAco', '权限关系', 'onetomany', 'default', '', 27, '2010-06-30 23:06:27', '2010-06-30 23:06:27', 'miao_aros_acos', '', '', '', '0', NULL, 0, '0', '0', '');



REPLACE INTO `miao_aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`, `_alldata`, `_list`) VALUES (1, 4, 71, 1, 1, 1, 1, 0, 1),
(2, 4, 354, 1, 1, 1, 1, 0, 1),
(5, 4, 174, 1, 1, 1, 1, 0, 0),
(18, 116, 278, 1, 1, 1, 1, 0, 0),
(19, 2, 393, 1, 1, 1, 1, 0, 1),
(20, 2, 354, 0, 0, 0, 0, 0, 0),
(21, 2, 174, 1, 1, 1, 1, 0, 1),
(22, 2, 199, 1, 1, 1, 1, 0, 1),
(23, 2, 71, 0, 0, 0, 0, 0, 0),
(24, 2, 394, 1, 1, 1, 1, 0, 1),
(25, 116, 395, 1, 1, 1, 1, 0, 0),
(26, 116, 394, 1, 1, 1, 1, 0, 0),
(27, 116, 393, 1, 1, 1, 1, 0, 0),
(28, 116, 377, 1, 1, 1, 1, 0, 0),
(30, 2, 398, 1, 1, 1, 1, 0, 1),
(31, 2, 395, 1, 1, 1, 1, 0, 1),
(32, 116, 399, 1, 1, 1, 1, 0, 0),
(33, 4, 400, 1, 1, 1, 1, 0, 0),
(34, 4, 377, 1, 1, 1, 1, 0, 0),
(35, 4, 278, 1, 1, 1, 1, 0, 0),
(36, 2, 278, 1, 1, 1, 1, 0, 1),
(37, 117, 278, 1, 1, 1, 1, 0, 0),
(38, 2, 401, 1, 1, 1, 1, 0, 1),
(39, 2, 402, 1, 1, 1, 1, 0, 1),
(40, 116, 403, 1, 1, 1, 1, 0, 0),
(41, 2, 403, 1, 1, 1, 1, 0, 1),
(42, 4, 403, 1, 1, 1, 1, 0, 0),
(43, 4, 402, 1, 1, 1, 1, 0, 0),
(44, 117, 403, 1, 1, 1, 1, 0, 0),
(45, 117, 402, 1, 1, 1, 1, 0, 0),
(46, 117, 401, 1, 1, 1, 1, 0, 0),
(47, 117, 398, 1, 1, 1, 1, 0, 0),
(48, 117, 394, 1, 1, 1, 1, 0, 0),
(49, 117, 395, 1, 1, 1, 1, 0, 0),
(50, 117, 393, 1, 1, 1, 1, 0, 0),
(51, 117, 404, 1, 1, 1, 1, 0, 0),
(52, 4, 404, 1, 1, 1, 1, 0, 0),
(53, 117, 374, 1, 1, 1, 1, 0, 0),
(54, 117, 375, 1, 1, 1, 1, 0, 0),
(55, 117, 376, 1, 1, 1, 1, 0, 0),
(56, 117, 372, 1, 1, 1, 1, 0, 0),
(57, 117, 371, 1, 1, 1, 1, 0, 0),
(58, 117, 174, 1, 1, 1, 1, 0, 0),
(59, 118, 278, 1, 1, 1, 1, 0, 0),
(60, 119, 278, 1, 1, 1, 1, 0, 0),
(61, 2, 396, 1, 1, 1, 1, 0, 1),
(62, 4, 396, 1, 1, 1, 1, 0, 0),
(63, 116, 396, 1, 1, 1, 1, 0, 0),
(64, 118, 396, 1, 1, 1, 1, 0, 0),
(65, 117, 396, 1, 1, 1, 1, 0, 0),
(66, 119, 396, 1, 1, 1, 1, 0, 0),
(67, 4, 399, 1, 1, 1, 1, 0, 0),
(68, 0, 71, 0, 0, 0, 0, 0, 1),
(69, 4, 124, 0, 0, 0, 0, 0, 0),
(70, 102, 405, 0, 1, 0, 0, 0, 0),
(71, 120, 405, 0, 1, 0, 0, 0, 0),
(72, 2, 404, 1, 1, 1, 1, 0, 1),
(73, 119, 407, 1, 1, 1, 1, 1, 1);
