set names utf8;
CREATE TABLE IF NOT EXISTS `miao_wx_city_cates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coverimg` varchar(200) DEFAULT NULL,
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `city_id` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `model` varchar(40) DEFAULT NULL,
  `wx_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'WxCityCate', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-30 20:45:31', '2015-01-30 20:45:31', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'WxCityCate', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'file', '', '1', '', NULL, '', '', '', 0, '2015-01-30 20:45:31', '2015-01-30 22:31:16', NULL),
(NULL, 'cate_id', '1', '分类', 'integer', 'WxCityCate', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-30 20:45:31', '2015-01-30 20:45:31', NULL),
(NULL, 'creator', '1', '编创建者', 'integer', 'WxCityCate', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-30 20:45:31', '2015-01-30 20:45:31', NULL),
(NULL, 'city_id', '1', '城市编号', 'integer', 'WxCityCate', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '', 0, '2015-01-30 20:45:31', '2015-01-30 20:47:32', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'WxCityCate', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-30 20:45:31', '2015-01-30 20:45:31', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'WxCityCate', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-30 20:45:31', '2015-01-30 20:45:31', NULL),
(NULL, 'model', '1', '所属模块', 'string', 'WxCityCate', 'en_us', '40', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-01-30 21:36:30', '2015-01-30 21:36:30', NULL),
(NULL, 'wx_id', '1', '微信编号', 'integer', 'WxCityCate', 'en_us', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-01-30 21:59:11', '2015-01-30 21:59:11', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'WxCityCate', '城市分类信息', '', 'default', '', 27, '2015-01-30 20:45:31', '2015-01-31 08:34:19', 'miao_wx_city_cates', '', '', '', '0', 2, 0, '0', '0', '');
