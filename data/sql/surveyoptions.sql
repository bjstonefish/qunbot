set names utf8;
CREATE TABLE IF NOT EXISTS `miao_surveyoptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) DEFAULT '',
  `correct` smallint(3) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `qid` int(11) DEFAULT '0',
  `withinput` tinyint(1) DEFAULT NULL,
  `optiontype` varchar(10) DEFAULT '',
  `extra` varchar(32) DEFAULT '',
  `input_val` varchar(200) DEFAULT '',
  `score` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Surveyoption', 'zh_cn', '11', 7, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2010-09-11 17:28:01', '2010-09-11 17:28:01', NULL),
(NULL, 'name', '1', '选项内容', 'string', 'Surveyoption', 'zh_cn', '300', 6, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2010-09-11 17:28:01', '2016-08-12 22:04:42', NULL),
(NULL, 'correct', '1', '是否正确答案', 'integer', 'Surveyoption', 'zh_cn', '3', 4, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'input', '0', '1', '', NULL, '', '', '1表示正确答案', 0, '2010-09-11 17:28:01', '2016-08-12 22:06:13', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Surveyoption', 'zh_cn', NULL, 2, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2010-09-11 17:28:01', '2010-09-11 17:28:01', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Surveyoption', 'zh_cn', NULL, 1, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2010-09-11 17:28:01', '2010-09-11 17:28:01', NULL),
(NULL, 'qid', '1', '问题编号', 'integer', 'Surveyoption', 'zh_cn', '11', 5, '1', '1', 'Survey', 'id', 'name', NULL, '1', '', '0', '', '', '', 'select', '', '1', '', NULL, '', '', '', 0, '2010-09-11 17:29:32', '2016-12-22 17:39:43', NULL),
(NULL, 'withinput', '1', '带文本框', 'integer', 'Surveyoption', 'zh_cn', '1', NULL, '1', '1', '', NULL, NULL, NULL, '1', '', '0', '', '', '', '', '', '1', '', NULL, '', '', '', 0, '2010-09-11 17:49:28', '2010-09-11 17:49:28', NULL),
(NULL, 'optiontype', '1', '选项类型', 'string', 'Surveyoption', 'zh_cn', '10', NULL, '1', '1', '', '', '', NULL, '1', 'x=>x\r\ny=>y\r\nz=>z', '0', '', '', '', 'select', '', '1', '', NULL, '', '', '选项类型，用于多维如表格类型的调查', 0, '2010-09-11 17:50:39', '2016-08-12 22:07:36', NULL),
(NULL, 'extra', '1', '额外值', 'string', 'Surveyoption', 'zh_cn', '32', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '额外值，比如心理测试等每个选项有不同的打分值', 0, '2017-04-18 16:46:46', '2017-04-18 16:46:46', NULL),
(NULL, 'input_val', '1', '输入框内容', 'string', 'Surveyoption', 'zh_cn', '200', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-04-20 06:06:47', '2017-04-20 09:48:33', NULL),
(NULL, 'score', '1', '分数', 'integer', 'Surveyoption', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-04-20 06:11:24', '2017-04-20 09:47:52', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Surveyoption', '调查选项', '', 'default', '', 27, '2010-09-11 17:28:00', '2016-08-12 21:40:33', 'miao_surveyoptions', '', '', '', '0', NULL, 0, '0', '0', '');
