set names utf8;
CREATE TABLE IF NOT EXISTS `miao_poineerings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(200) DEFAULT NULL,
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `financing` varchar(30) DEFAULT NULL,
  `area` varchar(20) DEFAULT NULL,
  `industry` int(11) DEFAULT '0',
  `website` varchar(200) DEFAULT NULL,
  `summary` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'cate_id', '1', '分类', 'integer', 'Poineering', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-13 21:47:12', '2014-08-13 21:47:12', NULL),
(NULL, 'creator', '1', '编创建者', 'integer', 'Poineering', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-13 21:47:12', '2014-08-13 21:47:12', NULL),
(NULL, 'id', '1', '编号', 'integer', 'Poineering', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-13 21:47:12', '2014-08-13 21:47:12', NULL),
(NULL, 'name', '1', '名称', 'string', 'Poineering', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-13 21:47:12', '2014-08-13 21:47:12', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'Poineering', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'file', '', '1', '', NULL, '', '', '', 0, '2014-08-13 21:47:12', '2014-08-13 22:26:57', ''),
(NULL, 'created', '1', '创建时间', 'datetime', 'Poineering', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-13 21:47:12', '2014-08-13 21:47:12', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Poineering', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-13 21:47:12', '2014-08-13 21:47:12', NULL),
(NULL, 'state', '1', '项目阶段', 'string', 'Poineering', NULL, '20', 0, '1', '1', '', '', '', 0, '1', '初创\r\n种子\r\n天使\r\nA轮\r\nB轮\r\nC轮\r\nD轮\r\nE轮\r\n上市', '0', '', '', '', 'select', '', '1', '', NULL, '', '', '', 0, '2014-08-13 22:23:03', '2014-08-13 22:25:08', ''),
(NULL, 'financing', '1', '期望融资信息', 'string', 'Poineering', NULL, '30', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '融资金额，投资方占股比例', 0, '2014-08-13 22:23:03', '2014-08-13 22:24:11', ''),
(NULL, 'area', '1', '所在地区', 'string', 'Poineering', NULL, '20', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-08-13 22:23:03', '2014-08-13 22:33:33', ''),
(NULL, 'industry', '1', '行业', 'integer', 'Poineering', NULL, '11', 0, '1', '1', 'Misccate', 'id', 'name', 1, '1', '', '0', '', '', 'equal\r\n', 'select', '', '1', '', NULL, '', '', '', 0, '2014-08-13 22:23:03', '2014-08-14 22:55:23', ''),
(NULL, 'website', '1', '项目网址', 'string', 'Poineering', NULL, '200', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-08-13 22:23:03', '2014-08-13 22:32:15', ''),
(NULL, 'summary', '1', '项目摘要', 'string', 'Poineering', NULL, '500', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2014-08-13 22:23:03', '2014-08-13 22:36:27', '');
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Poineering', '创业项目', '', 'default', '', 27, '2014-08-13 21:47:12', '2014-09-24 15:43:13', 'miao_poineerings', '{\"hasOne\":{\"PoineeringContent\":{\"foreignKey\":\"id\"}}}', '', '', '0', NULL, 0, '0', '0', '');
