set names utf8;
CREATE TABLE IF NOT EXISTS `miao_exam_results` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `exam` varchar(32) DEFAULT '',
  `creator` int(11) DEFAULT '0',
  `updated` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `total_num` int(11) DEFAULT '0',
  `correct_num` int(11) DEFAULT '0',
  `error_num` int(11) DEFAULT '0',
  `questionnaire_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `creator` (`creator`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'questionnaire_id', '1', '测验编号', 'integer', 'ExamResult', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-12-24 08:30:38', '2016-12-24 08:30:38', NULL),
(NULL, 'created', '1', '答题时间', 'datetime', 'ExamResult', 'zh_cn', '19', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'datetime', '', '1', '', NULL, '', '', '', 0, '2016-12-24 08:26:41', '2016-12-24 08:26:41', NULL),
(NULL, 'exam', '1', '测验批次', 'string', 'ExamResult', NULL, '32', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '为随机字符串，区分同一人不同次的答题', 0, '2016-12-24 08:23:02', '2016-12-24 08:24:55', NULL),
(NULL, 'id', '1', '编号', 'integer', 'ExamResult', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-12-24 08:23:02', '2016-12-24 08:23:02', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'ExamResult', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-12-24 08:23:02', '2016-12-24 08:23:02', NULL),
(NULL, 'total_num', '1', '总题数', 'integer', 'ExamResult', NULL, '11', 0, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-12-24 08:29:21', '2016-12-24 08:29:21', NULL),
(NULL, 'correct_num', '1', '答对数', 'integer', 'ExamResult', NULL, '11', 0, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-12-24 08:29:21', '2016-12-24 08:29:21', NULL),
(NULL, 'error_num', '1', '答错数', 'integer', 'ExamResult', NULL, '11', 0, '1', '1', '', NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-12-24 08:29:21', '2016-12-24 08:29:21', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'ExamResult', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-12-24 08:23:02', '2016-12-24 08:23:02', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'ExamResult', '测验成绩', '', 'default', '', 0, '2016-12-24 08:23:02', '2016-12-24 08:23:02', 'miao_exam_results', '', '', '', '0', 2, NULL, '0', '0', '');
