set names utf8;
CREATE TABLE IF NOT EXISTS `miao_tp_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `content` text,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `tp_id` int(11) DEFAULT '0',
  `team_id` int(11) DEFAULT '0',
  `view_nums` int(11) DEFAULT '0',
  `cate_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'status', '1', '任务状态', 'integer', 'TpTask', 'zh_cn', '11', 14, '1', '1', '', 'id', 'name', 25, '1', '-1=>failed	任务失败\r\n0=>任务已添加\r\n1=>started任务开始\r\n2=>任务完成\r\n3=>任务确认完成', '0', '', '', 'treenode', 'select', '0', '1', '', '', '', 'none', '', 0, '0000-00-00 00:00:00', '2014-12-04 10:08:27', ''),
(NULL, 'id', '1', '编号', 'integer', 'TpTask', 'zh_cn', '11', 20, '0', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'TpTask', 'zh_cn', NULL, 2, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'datetime', '', '1', '', '', '', 'none', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'TpTask', 'zh_cn', NULL, 1, '0', '0', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'datetime', '', '1', '', '', '', 'none', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'name', '1', '任务名称', 'string', 'TpTask', 'zh_cn', '200', 18, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'input', '', '1', '', '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'content', '1', '内容', 'content', 'TpTask', 'zh_cn', NULL, 5, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'textarea', '', '1', '', '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'start_time', '1', '任务开始时间', 'datetime', 'TpTask', 'zh_cn', '', 16, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'datetime', '', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '2014-10-04 20:27:27', ''),
(NULL, 'end_time', '1', '结束时间', 'datetime', 'TpTask', 'zh_cn', '', 15, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'datetime', '', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '2014-10-04 20:27:11', ''),
(NULL, 'tp_id', '1', '所属团队项目', 'integer', 'TpTask', 'zh_cn', '11', 10, '1', '1', 'TeamProject', 'id', 'name', NULL, '1', '', '0', '', '', '', 'select', '', '1', '', NULL, '', 'none', '', 0, '2014-10-10 18:06:34', '2014-12-23 23:19:19', ''),
(NULL, 'team_id', '1', '所属团队编号', 'integer', 'TpTask', 'zh_cn', '11', 9, '1', '1', 'Team', 'id', 'name', NULL, '1', '', '1', 'tp_id', 'team_id', 'equal\r\n', 'select', '', '1', '', NULL, '', 'none', '', 0, '2014-10-14 13:59:55', '2014-12-24 10:21:57', ''),
(NULL, 'task_files', '0', '任务文件', 'string', 'TpTask', 'zh_cn', '100', 8, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'file', '', '1', '', NULL, '', '', '', 0, '2014-10-16 11:11:09', '2014-10-16 11:11:09', ''),
(NULL, 'view_nums', '1', '访问次数', 'integer', 'TpTask', 'zh_cn', '11', 6, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '', 0, '2014-10-24 18:56:09', '2014-10-24 18:56:09', ''),
(NULL, 'cate_id', '1', '任务分类', 'integer', 'TpTask', 'zh_cn', '11', 19, '1', '1', 'Category', 'id', 'name', NULL, '1', '', '0', '', '', '', 'select', '', '1', '', NULL, '', '', '', 0, '2014-10-30 09:18:21', '2014-10-30 09:36:23', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<options><conditions><Category.name>Task</Category.name></conditions><order>created desc</order></options>\n');
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'TpTask', '团队项目任务', 'onetomany', 'default', '<id>', 27, '2014-12-04 10:02:09', '2014-12-04 10:02:09', 'miao_tp_tasks', '', '', '', '0', 10, 0, '0', '0', '');
