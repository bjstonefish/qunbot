set names utf8;
CREATE TABLE IF NOT EXISTS `miao_score_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `model` varchar(32) DEFAULT NULL,
  `creator` int(11) DEFAULT '0',
  `action` varchar(32) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `score` smallint(3) DEFAULT '0',
  `status` smallint(3) DEFAULT '1',
  `daytimes` smallint(3) DEFAULT '0',
  `daylimit` smallint(3) DEFAULT '0',
  `data_id_limit` smallint(3) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `model` (`model`,`action`,`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'name', '1', '规则名称', 'string', 'ScoreRule', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-03-08 07:03:20', '2015-03-08 07:04:13', NULL),
(NULL, 'id', '1', '编号', 'integer', 'ScoreRule', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-03-08 07:03:20', '2015-03-08 07:03:20', NULL),
(NULL, 'model', '1', '操作模块', 'string', 'ScoreRule', NULL, '32', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-03-08 07:03:20', '2015-03-08 07:04:40', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'ScoreRule', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-03-08 07:03:20', '2015-03-08 07:03:20', NULL),
(NULL, 'action', '1', '操作类型', 'string', 'ScoreRule', NULL, '32', 3, '1', '1', '', '', '', 0, '1', '0=>否\r\n1=>是', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-03-08 07:03:20', '2015-03-08 07:05:12', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'ScoreRule', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-03-08 07:03:20', '2015-03-08 07:03:20', NULL),
(NULL, 'score', '1', '增加积分', 'integer', 'ScoreRule', NULL, '3', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-03-08 07:06:52', '2015-03-08 07:06:52', NULL),
(NULL, 'status', '1', '是否生效', 'integer', 'ScoreRule', NULL, '3', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '1', '1', '', NULL, '', '', '1开启生效，0关闭失效', 0, '2015-03-08 07:06:52', '2015-03-08 13:48:24', NULL),
(NULL, 'daytimes', '1', '每日生效次数', 'integer', 'ScoreRule', 'en_us', '3', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '1', '1', '', NULL, '', '', '', 0, '2015-03-08 07:12:10', '2015-03-08 07:12:10', NULL),
(NULL, 'daylimit', '1', '是否按天限制', 'integer', 'ScoreRule', 'en_us', '3', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '1', '1', '', NULL, '', '', '', 0, '2015-05-02 15:24:02', '2015-05-02 15:24:02', NULL),
(NULL, 'data_id_limit', '1', '是否按数据限制', 'integer', 'ScoreRule', 'en_us', '3', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '', 0, '2015-05-02 15:24:35', '2015-05-02 15:24:35', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'ScoreRule', '积分规则', '', 'default', '', 27, '2015-03-08 07:03:20', '2015-03-08 07:03:20', 'miao_score_rules', '', '', '', '0', NULL, 0, '0', '0', '');



REPLACE INTO `miao_score_rules` (`id`, `name`, `model`, `creator`, `action`, `created`, `score`, `status`, `daytimes`, `daylimit`, `data_id_limit`) VALUES (1, '注册成功', 'User', 1, 'register', '2015-03-08 13:45:44', 10, 1, 1, 0, 0),
(2, '登录成功', 'User', 1, 'login', '2015-03-08 13:46:06', 2, 1, 1, 0, 0),
(3, '收藏扣积分', 'EditorStyle', 1, 'addfavor', '2015-03-09 10:47:58', -1, 1, 10000, 0, 0),
(4, '领取会员', 'Role', 1, 'pay', '2016-05-16 20:48:05', 10, 1, 1, 0, 1),
(5, '系统赠送积分', 'System', 1, 'givescore', '2016-07-02 09:51:34', 100, 1, 10000, 0, 0),
(6, '领取会员', 'Role', 1, 'auth', '2016-05-16 20:48:05', 10, 1, 1, 0, 1);
