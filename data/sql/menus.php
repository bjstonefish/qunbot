<?php
$datas = array (
  0 => 
  array (
    'Menu' => 
    array (
      'id' => '1',
      'parent_id' => NULL,
      'name' => '站点管理',
      'slug' => 'site',
      'visible' => '1',
      'rel' => '',
      'target' => '',
      'link' => '#',
      'left' => '1',
      'right' => '168',
      'created' => '2010-05-14 22:52:07',
      'updated' => '2014-05-30 18:13:07',
      'deleted' => '0',
      'locale' => 'zh_cn',
      'prefix_icon' => NULL,
    ),
    'Uploadfile' => 
    array (
    ),
    'MenuI18n' => 
    array (
      'en_us' => 
      array (
        'id' => '48',
        'name' => 'Site CMS',
        'published' => '0',
        'deleted' => '0',
        'created' => '2014-05-30 17:19:35',
        'updated' => '2014-05-30 17:19:35',
        'foreign_key' => '1',
        'locale' => 'en_us',
      ),
      'zh_cn' => 
      array (
        'id' => '1',
        'parent_id' => NULL,
        'name' => '站点管理',
        'slug' => 'site',
        'visible' => '1',
        'rel' => '',
        'target' => '',
        'link' => '#',
        'left' => '1',
        'right' => '168',
        'created' => '2010-05-14 22:52:07',
        'updated' => '2014-05-30 18:13:07',
        'deleted' => '0',
        'locale' => 'zh_cn',
        'prefix_icon' => NULL,
      ),
    ),
    'Tag' => 
    array (
    ),
    'children' => 
    array (
      0 => 
      array (
        'Menu' => 
        array (
          'id' => '10',
          'parent_id' => '1',
          'name' => '用户',
          'slug' => 'user',
          'visible' => '1',
          'rel' => '',
          'target' => 'leftmenu',
          'link' => '/admin/menus/menu/10',
          'left' => '44',
          'right' => '61',
          'created' => '2010-05-15 17:14:30',
          'updated' => '2014-11-09 11:06:51',
          'deleted' => '0',
          'locale' => 'zh_cn',
          'prefix_icon' => 'glyphicon glyphicon-user',
        ),
        'Uploadfile' => 
        array (
        ),
        'MenuI18n' => 
        array (
          'en_us' => 
          array (
            'id' => '9',
            'name' => 'Users',
            'published' => '0',
            'deleted' => '0',
            'created' => '2011-03-13 23:16:06',
            'updated' => '2012-08-16 23:23:12',
            'foreign_key' => '10',
            'locale' => 'en_us',
          ),
          'zh_cn' => 
          array (
            'id' => '10',
            'parent_id' => '1',
            'name' => '用户',
            'slug' => 'user',
            'visible' => '1',
            'rel' => '',
            'target' => 'leftmenu',
            'link' => '/admin/menus/menu/10',
            'left' => '44',
            'right' => '61',
            'created' => '2010-05-15 17:14:30',
            'updated' => '2014-11-09 11:06:51',
            'deleted' => '0',
            'locale' => 'zh_cn',
            'prefix_icon' => 'glyphicon glyphicon-user',
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
          0 => 
          array (
            'Menu' => 
            array (
              'id' => '27',
              'parent_id' => '10',
              'name' => '用户管理',
              'slug' => '',
              'visible' => '1',
              'rel' => '',
              'target' => '',
              'link' => '',
              'left' => '45',
              'right' => '60',
              'created' => '2010-05-17 22:38:10',
              'updated' => '2014-11-18 22:13:02',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => '',
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'en_us' => 
              array (
                'id' => '60',
                'name' => 'User Management',
                'published' => '0',
                'deleted' => '0',
                'created' => '2014-05-30 17:25:14',
                'updated' => '2014-05-30 17:25:14',
                'foreign_key' => '27',
                'locale' => 'en_us',
              ),
              'zh_cn' => 
              array (
                'id' => '27',
                'parent_id' => '10',
                'name' => '用户管理',
                'slug' => '',
                'visible' => '1',
                'rel' => '',
                'target' => '',
                'link' => '',
                'left' => '45',
                'right' => '60',
                'created' => '2010-05-17 22:38:10',
                'updated' => '2014-11-18 22:13:02',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => '',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '11',
                  'parent_id' => '27',
                  'name' => '前台用户',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '/admin/users/list',
                  'left' => '46',
                  'right' => '47',
                  'created' => '2010-05-15 17:15:22',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '61',
                    'name' => 'Website User',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:25:29',
                    'updated' => '2014-05-30 17:25:29',
                    'foreign_key' => '11',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '11',
                    'parent_id' => '27',
                    'name' => '前台用户',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '/admin/users/list',
                    'left' => '46',
                    'right' => '47',
                    'created' => '2010-05-15 17:15:22',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '12',
                  'parent_id' => '27',
                  'name' => '角色管理',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '/admin/roles/list',
                  'left' => '50',
                  'right' => '51',
                  'created' => '2010-05-15 17:15:55',
                  'updated' => '2014-11-18 22:13:29',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '35',
                    'name' => 'Role Management',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2012-08-16 23:21:14',
                    'updated' => '2014-05-30 17:26:41',
                    'foreign_key' => '12',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '12',
                    'parent_id' => '27',
                    'name' => '角色管理',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '/admin/roles/list',
                    'left' => '50',
                    'right' => '51',
                    'created' => '2010-05-15 17:15:55',
                    'updated' => '2014-11-18 22:13:29',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              2 => 
              array (
                'Menu' => 
                array (
                  'id' => '13',
                  'parent_id' => '27',
                  'name' => '权限管理',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '/admin/acl/acl_permissions',
                  'left' => '48',
                  'right' => '49',
                  'created' => '2010-05-15 17:16:43',
                  'updated' => '2014-11-18 22:13:17',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '62',
                    'name' => 'Permission Management',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:26:27',
                    'updated' => '2014-05-30 17:26:27',
                    'foreign_key' => '13',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '13',
                    'parent_id' => '27',
                    'name' => '权限管理',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '/admin/acl/acl_permissions',
                    'left' => '48',
                    'right' => '49',
                    'created' => '2010-05-15 17:16:43',
                    'updated' => '2014-11-18 22:13:17',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              3 => 
              array (
                'Menu' => 
                array (
                  'id' => '65',
                  'parent_id' => '27',
                  'name' => '后台用户',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/staffs/list',
                  'left' => '52',
                  'right' => '53',
                  'created' => '2010-06-26 18:43:33',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '36',
                    'name' => 'Admin User',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2012-08-16 23:21:47',
                    'updated' => '2014-05-30 17:26:53',
                    'foreign_key' => '65',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '65',
                    'parent_id' => '27',
                    'name' => '后台用户',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/staffs/list',
                    'left' => '52',
                    'right' => '53',
                    'created' => '2010-06-26 18:43:33',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              4 => 
              array (
                'Menu' => 
                array (
                  'id' => '162',
                  'parent_id' => '27',
                  'name' => '批量发放会员',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/user_roles/batchadd',
                  'left' => '54',
                  'right' => '55',
                  'created' => '2016-07-02 15:12:38',
                  'updated' => '2016-07-02 15:12:38',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '162',
                    'parent_id' => '27',
                    'name' => '批量发放会员',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/user_roles/batchadd',
                    'left' => '54',
                    'right' => '55',
                    'created' => '2016-07-02 15:12:38',
                    'updated' => '2016-07-02 15:12:38',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              5 => 
              array (
                'Menu' => 
                array (
                  'id' => '163',
                  'parent_id' => '27',
                  'name' => '积分规则',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/score_rules/list',
                  'left' => '56',
                  'right' => '57',
                  'created' => '2016-07-02 15:13:37',
                  'updated' => '2016-07-02 15:13:37',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '163',
                    'parent_id' => '27',
                    'name' => '积分规则',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/score_rules/list',
                    'left' => '56',
                    'right' => '57',
                    'created' => '2016-07-02 15:13:37',
                    'updated' => '2016-07-02 15:13:37',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              6 => 
              array (
                'Menu' => 
                array (
                  'id' => '164',
                  'parent_id' => '27',
                  'name' => '积分增加记录',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/score_logs/list',
                  'left' => '58',
                  'right' => '59',
                  'created' => '2016-07-02 15:14:08',
                  'updated' => '2016-07-02 15:14:08',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '164',
                    'parent_id' => '27',
                    'name' => '积分增加记录',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/score_logs/list',
                    'left' => '58',
                    'right' => '59',
                    'created' => '2016-07-02 15:14:08',
                    'updated' => '2016-07-02 15:14:08',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
        ),
      ),
      1 => 
      array (
        'Menu' => 
        array (
          'id' => '26',
          'parent_id' => '1',
          'name' => '设置',
          'slug' => 'setting',
          'visible' => '1',
          'rel' => '',
          'target' => '',
          'link' => '#',
          'left' => '16',
          'right' => '43',
          'created' => '2010-05-15 18:38:44',
          'updated' => '2014-11-09 11:25:43',
          'deleted' => '0',
          'locale' => 'zh_cn',
          'prefix_icon' => 'glyphicon glyphicon-cog',
        ),
        'Uploadfile' => 
        array (
        ),
        'MenuI18n' => 
        array (
          'en_us' => 
          array (
            'id' => '49',
            'name' => 'Setting',
            'published' => '0',
            'deleted' => '0',
            'created' => '2014-05-30 17:19:55',
            'updated' => '2014-05-30 17:19:55',
            'foreign_key' => '26',
            'locale' => 'en_us',
          ),
          'zh_cn' => 
          array (
            'id' => '26',
            'parent_id' => '1',
            'name' => '设置',
            'slug' => 'setting',
            'visible' => '1',
            'rel' => '',
            'target' => '',
            'link' => '#',
            'left' => '16',
            'right' => '43',
            'created' => '2010-05-15 18:38:44',
            'updated' => '2014-11-09 11:25:43',
            'deleted' => '0',
            'locale' => 'zh_cn',
            'prefix_icon' => 'glyphicon glyphicon-cog',
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
          0 => 
          array (
            'Menu' => 
            array (
              'id' => '138',
              'parent_id' => '26',
              'name' => '站点设置',
              'slug' => '',
              'visible' => '1',
              'rel' => '',
              'target' => '#',
              'link' => '#',
              'left' => '17',
              'right' => '30',
              'created' => '2014-04-17 12:57:33',
              'updated' => '2014-05-30 18:13:07',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => NULL,
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'en_us' => 
              array (
                'id' => '54',
                'name' => 'Site Setting',
                'published' => '0',
                'deleted' => '0',
                'created' => '2014-05-30 17:23:21',
                'updated' => '2014-05-30 17:23:21',
                'foreign_key' => '138',
                'locale' => 'en_us',
              ),
              'zh_cn' => 
              array (
                'id' => '138',
                'parent_id' => '26',
                'name' => '站点设置',
                'slug' => '',
                'visible' => '1',
                'rel' => '',
                'target' => '#',
                'link' => '#',
                'left' => '17',
                'right' => '30',
                'created' => '2014-04-17 12:57:33',
                'updated' => '2014-05-30 18:13:07',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => NULL,
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '17',
                  'parent_id' => '138',
                  'name' => '站点设置',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '/admin/settings/prefix/Site.html',
                  'left' => '18',
                  'right' => '19',
                  'created' => '2010-05-15 18:06:54',
                  'updated' => '2014-11-08 11:25:22',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '92',
                    'name' => 'Site Setting',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-11-08 10:36:36',
                    'updated' => '2014-11-08 11:18:06',
                    'foreign_key' => '17',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '17',
                    'parent_id' => '138',
                    'name' => '站点设置',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '/admin/settings/prefix/Site.html',
                    'left' => '18',
                    'right' => '19',
                    'created' => '2010-05-15 18:06:54',
                    'updated' => '2014-11-08 11:25:22',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '18',
                  'parent_id' => '138',
                  'name' => '评论设置',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '/admin/settings/prefix/Comment.html',
                  'left' => '24',
                  'right' => '25',
                  'created' => '2010-05-15 18:07:41',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '58',
                    'name' => 'Comment Setting',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:24:21',
                    'updated' => '2014-05-30 17:24:21',
                    'foreign_key' => '18',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '18',
                    'parent_id' => '138',
                    'name' => '评论设置',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '/admin/settings/prefix/Comment.html',
                    'left' => '24',
                    'right' => '25',
                    'created' => '2010-05-15 18:07:41',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              2 => 
              array (
                'Menu' => 
                array (
                  'id' => '19',
                  'parent_id' => '138',
                  'name' => '语言设置',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '/admin/languages/list.html',
                  'left' => '20',
                  'right' => '21',
                  'created' => '2010-05-15 18:08:16',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '56',
                    'name' => 'Language Setting',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:23:52',
                    'updated' => '2014-05-30 17:23:52',
                    'foreign_key' => '19',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '19',
                    'parent_id' => '138',
                    'name' => '语言设置',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '/admin/languages/list.html',
                    'left' => '20',
                    'right' => '21',
                    'created' => '2010-05-15 18:08:16',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              3 => 
              array (
                'Menu' => 
                array (
                  'id' => '70',
                  'parent_id' => '138',
                  'name' => '用户设置',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/settings/prefix/User.html',
                  'left' => '22',
                  'right' => '23',
                  'created' => '2010-07-18 00:45:38',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '57',
                    'name' => 'User Setting',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:24:08',
                    'updated' => '2014-05-30 17:24:08',
                    'foreign_key' => '70',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '70',
                    'parent_id' => '138',
                    'name' => '用户设置',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/settings/prefix/User.html',
                    'left' => '22',
                    'right' => '23',
                    'created' => '2010-07-18 00:45:38',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              4 => 
              array (
                'Menu' => 
                array (
                  'id' => '118',
                  'parent_id' => '138',
                  'name' => '页面变量设置',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/settings/prefix/Page',
                  'left' => '26',
                  'right' => '27',
                  'created' => '2013-01-08 23:24:42',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '59',
                    'name' => 'FrontPage Setting',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:24:43',
                    'updated' => '2014-05-30 17:24:43',
                    'foreign_key' => '118',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '118',
                    'parent_id' => '138',
                    'name' => '页面变量设置',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/settings/prefix/Page',
                    'left' => '26',
                    'right' => '27',
                    'created' => '2013-01-08 23:24:42',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              5 => 
              array (
                'Menu' => 
                array (
                  'id' => '143',
                  'parent_id' => '138',
                  'name' => '邮件设置',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '/admin/settings/prefix/Email',
                  'left' => '28',
                  'right' => '29',
                  'created' => '2014-07-13 22:04:32',
                  'updated' => '2014-11-18 21:25:19',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => 'glyphicon glyphicon-envelope',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '93',
                    'name' => 'Email Setting',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-11-18 21:25:19',
                    'updated' => '2014-11-18 21:25:19',
                    'foreign_key' => '143',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '143',
                    'parent_id' => '138',
                    'name' => '邮件设置',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '/admin/settings/prefix/Email',
                    'left' => '28',
                    'right' => '29',
                    'created' => '2014-07-13 22:04:32',
                    'updated' => '2014-11-18 21:25:19',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => 'glyphicon glyphicon-envelope',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
          1 => 
          array (
            'Menu' => 
            array (
              'id' => '139',
              'parent_id' => '26',
              'name' => '开放设置',
              'slug' => '',
              'visible' => '1',
              'rel' => '',
              'target' => '',
              'link' => '#',
              'left' => '31',
              'right' => '36',
              'created' => '2014-04-17 12:59:39',
              'updated' => '2014-05-30 18:13:07',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => NULL,
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'en_us' => 
              array (
                'id' => '52',
                'name' => 'Open Api Setting',
                'published' => '0',
                'deleted' => '0',
                'created' => '2014-05-30 17:22:36',
                'updated' => '2014-05-30 17:22:36',
                'foreign_key' => '139',
                'locale' => 'en_us',
              ),
              'zh_cn' => 
              array (
                'id' => '139',
                'parent_id' => '26',
                'name' => '开放设置',
                'slug' => '',
                'visible' => '1',
                'rel' => '',
                'target' => '',
                'link' => '#',
                'left' => '31',
                'right' => '36',
                'created' => '2014-04-17 12:59:39',
                'updated' => '2014-05-30 18:13:07',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => NULL,
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '137',
                  'parent_id' => '139',
                  'name' => '微信设置',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/settings/prefix/Weixin',
                  'left' => '32',
                  'right' => '33',
                  'created' => '2014-04-17 12:54:14',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '53',
                    'name' => 'WeChat Setting',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:23:02',
                    'updated' => '2014-05-30 17:23:02',
                    'foreign_key' => '137',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '137',
                    'parent_id' => '139',
                    'name' => '微信设置',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/settings/prefix/Weixin',
                    'left' => '32',
                    'right' => '33',
                    'created' => '2014-04-17 12:54:14',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '144',
                  'parent_id' => '139',
                  'name' => '存储设置',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '/admin/settings/prefix/Storage',
                  'left' => '34',
                  'right' => '35',
                  'created' => '2014-07-13 22:04:32',
                  'updated' => '2014-11-26 18:35:29',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '55',
                    'name' => 'Storage Setting',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:23:35',
                    'updated' => '2014-07-16 23:25:58',
                    'foreign_key' => '144',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '144',
                    'parent_id' => '139',
                    'name' => '存储设置',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '/admin/settings/prefix/Storage',
                    'left' => '34',
                    'right' => '35',
                    'created' => '2014-07-13 22:04:32',
                    'updated' => '2014-11-26 18:35:29',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
          2 => 
          array (
            'Menu' => 
            array (
              'id' => '145',
              'parent_id' => '26',
              'name' => 'Payment Setting',
              'slug' => 'Payment-Setting',
              'visible' => '1',
              'rel' => '',
              'target' => '',
              'link' => '#',
              'left' => '37',
              'right' => '42',
              'created' => '2014-11-26 16:41:37',
              'updated' => '2014-11-26 16:41:37',
              'deleted' => '0',
              'locale' => 'en_us',
              'prefix_icon' => '',
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'en_us' => 
              array (
                'id' => '145',
                'parent_id' => '26',
                'name' => 'Payment Setting',
                'slug' => 'Payment-Setting',
                'visible' => '1',
                'rel' => '',
                'target' => '',
                'link' => '#',
                'left' => '37',
                'right' => '42',
                'created' => '2014-11-26 16:41:37',
                'updated' => '2014-11-26 16:41:37',
                'deleted' => '0',
                'locale' => 'en_us',
                'prefix_icon' => '',
              ),
              'zh_cn' => 
              array (
                'id' => '99',
                'name' => '',
                'published' => '0',
                'deleted' => '0',
                'created' => '2014-11-26 16:41:37',
                'updated' => '2014-11-26 16:41:37',
                'foreign_key' => '145',
                'locale' => 'zh_cn',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '146',
                  'parent_id' => '145',
                  'name' => 'SecurePay',
                  'slug' => 'SecurePay',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/settings/prefix/SecurePay',
                  'left' => '38',
                  'right' => '39',
                  'created' => '2014-11-26 16:42:50',
                  'updated' => '2014-11-26 16:42:50',
                  'deleted' => '0',
                  'locale' => 'en_us',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '146',
                    'parent_id' => '145',
                    'name' => 'SecurePay',
                    'slug' => 'SecurePay',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/settings/prefix/SecurePay',
                    'left' => '38',
                    'right' => '39',
                    'created' => '2014-11-26 16:42:50',
                    'updated' => '2014-11-26 16:42:50',
                    'deleted' => '0',
                    'locale' => 'en_us',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '160',
                  'parent_id' => '145',
                  'name' => '微信支付',
                  'slug' => 'WechatPay',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/settings/prefix/WechatPay',
                  'left' => '40',
                  'right' => '41',
                  'created' => '2015-03-19 15:41:57',
                  'updated' => '2015-03-19 15:41:57',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '160',
                    'parent_id' => '145',
                    'name' => '微信支付',
                    'slug' => 'WechatPay',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/settings/prefix/WechatPay',
                    'left' => '40',
                    'right' => '41',
                    'created' => '2015-03-19 15:41:57',
                    'updated' => '2015-03-19 15:41:57',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
        ),
      ),
      2 => 
      array (
        'Menu' => 
        array (
          'id' => '36',
          'parent_id' => '1',
          'name' => '运营',
          'slug' => '',
          'visible' => '1',
          'rel' => '',
          'target' => 'main',
          'link' => '/admin/menus/menu/36',
          'left' => '72',
          'right' => '109',
          'created' => '2010-05-23 00:15:58',
          'updated' => '2014-11-09 11:36:28',
          'deleted' => '0',
          'locale' => 'zh_cn',
          'prefix_icon' => 'glyphicon glyphicon-bullhorn',
        ),
        'Uploadfile' => 
        array (
        ),
        'MenuI18n' => 
        array (
          'en_us' => 
          array (
            'id' => '11',
            'name' => 'Marketing',
            'published' => '0',
            'deleted' => '0',
            'created' => '2011-03-13 23:16:50',
            'updated' => '2011-03-13 23:16:50',
            'foreign_key' => '36',
            'locale' => 'en_us',
          ),
          'zh_cn' => 
          array (
            'id' => '36',
            'parent_id' => '1',
            'name' => '运营',
            'slug' => '',
            'visible' => '1',
            'rel' => '',
            'target' => 'main',
            'link' => '/admin/menus/menu/36',
            'left' => '72',
            'right' => '109',
            'created' => '2010-05-23 00:15:58',
            'updated' => '2014-11-09 11:36:28',
            'deleted' => '0',
            'locale' => 'zh_cn',
            'prefix_icon' => 'glyphicon glyphicon-bullhorn',
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
          0 => 
          array (
            'Menu' => 
            array (
              'id' => '45',
              'parent_id' => '36',
              'name' => '搜索引擎营销',
              'slug' => '',
              'visible' => '1',
              'rel' => '',
              'target' => '',
              'link' => '#',
              'left' => '79',
              'right' => '88',
              'created' => '2010-05-23 11:39:00',
              'updated' => '2014-05-30 18:13:07',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => NULL,
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '45',
                'parent_id' => '36',
                'name' => '搜索引擎营销',
                'slug' => '',
                'visible' => '1',
                'rel' => '',
                'target' => '',
                'link' => '#',
                'left' => '79',
                'right' => '88',
                'created' => '2010-05-23 11:39:00',
                'updated' => '2014-05-30 18:13:07',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => NULL,
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '37',
                  'parent_id' => '45',
                  'name' => 'SEO关键字优化',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '/admin/tools/startseo',
                  'left' => '82',
                  'right' => '83',
                  'created' => '2010-05-23 00:19:16',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '91',
                    'name' => 'Content SEO',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 18:11:29',
                    'updated' => '2014-05-30 18:11:29',
                    'foreign_key' => '37',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '37',
                    'parent_id' => '45',
                    'name' => 'SEO关键字优化',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '/admin/tools/startseo',
                    'left' => '82',
                    'right' => '83',
                    'created' => '2010-05-23 00:19:16',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '39',
                  'parent_id' => '45',
                  'name' => '网站收录页面数',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '#',
                  'left' => '86',
                  'right' => '87',
                  'created' => '2010-05-23 00:27:47',
                  'updated' => '2014-11-18 21:27:44',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '95',
                    'name' => 'Search Enginge Indexed Page',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-11-18 21:27:44',
                    'updated' => '2014-11-18 21:27:44',
                    'foreign_key' => '39',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '39',
                    'parent_id' => '45',
                    'name' => '网站收录页面数',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '#',
                    'left' => '86',
                    'right' => '87',
                    'created' => '2010-05-23 00:27:47',
                    'updated' => '2014-11-18 21:27:44',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              2 => 
              array (
                'Menu' => 
                array (
                  'id' => '40',
                  'parent_id' => '45',
                  'name' => '关键字排行',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '#',
                  'left' => '84',
                  'right' => '85',
                  'created' => '2010-05-23 00:29:01',
                  'updated' => '2014-11-18 21:26:41',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '94',
                    'name' => 'Keyword Search Rank',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-11-18 21:26:41',
                    'updated' => '2014-11-18 21:26:41',
                    'foreign_key' => '40',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '40',
                    'parent_id' => '45',
                    'name' => '关键字排行',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '#',
                    'left' => '84',
                    'right' => '85',
                    'created' => '2010-05-23 00:29:01',
                    'updated' => '2014-11-18 21:26:41',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              3 => 
              array (
                'Menu' => 
                array (
                  'id' => '72',
                  'parent_id' => '45',
                  'name' => '关键字管理',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/keywords/list',
                  'left' => '80',
                  'right' => '81',
                  'created' => '2010-07-24 21:25:47',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '72',
                    'parent_id' => '45',
                    'name' => '关键字管理',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/keywords/list',
                    'left' => '80',
                    'right' => '81',
                    'created' => '2010-07-24 21:25:47',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
          1 => 
          array (
            'Menu' => 
            array (
              'id' => '126',
              'parent_id' => '36',
              'name' => '广告',
              'slug' => '',
              'visible' => '1',
              'rel' => '',
              'target' => '',
              'link' => '#',
              'left' => '89',
              'right' => '96',
              'created' => '2013-01-13 16:51:04',
              'updated' => '2014-05-30 18:13:07',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => NULL,
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'en_us' => 
              array (
                'id' => '81',
                'name' => 'Advertisement',
                'published' => '0',
                'deleted' => '0',
                'created' => '2014-05-30 17:35:54',
                'updated' => '2014-05-30 17:35:54',
                'foreign_key' => '126',
                'locale' => 'en_us',
              ),
              'zh_cn' => 
              array (
                'id' => '126',
                'parent_id' => '36',
                'name' => '广告',
                'slug' => '',
                'visible' => '1',
                'rel' => '',
                'target' => '',
                'link' => '#',
                'left' => '89',
                'right' => '96',
                'created' => '2013-01-13 16:51:04',
                'updated' => '2014-05-30 18:13:07',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => NULL,
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '130',
                  'parent_id' => '126',
                  'name' => '广告管理',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/advertises/list',
                  'left' => '94',
                  'right' => '95',
                  'created' => '2013-01-13 23:04:28',
                  'updated' => '2014-11-18 22:15:24',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '84',
                    'name' => 'Advertising Management',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:36:56',
                    'updated' => '2014-05-30 17:36:56',
                    'foreign_key' => '130',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '130',
                    'parent_id' => '126',
                    'name' => '广告管理',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/advertises/list',
                    'left' => '94',
                    'right' => '95',
                    'created' => '2013-01-13 23:04:28',
                    'updated' => '2014-11-18 22:15:24',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '119',
                  'parent_id' => '126',
                  'name' => '广告设置',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/settings/prefix/Advertise',
                  'left' => '90',
                  'right' => '91',
                  'created' => '2013-01-08 23:25:18',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '82',
                    'name' => 'Advertisement Setting',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:36:08',
                    'updated' => '2014-05-30 17:36:08',
                    'foreign_key' => '119',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '119',
                    'parent_id' => '126',
                    'name' => '广告设置',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/settings/prefix/Advertise',
                    'left' => '90',
                    'right' => '91',
                    'created' => '2013-01-08 23:25:18',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              2 => 
              array (
                'Menu' => 
                array (
                  'id' => '129',
                  'parent_id' => '126',
                  'name' => '链接管理',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/links/list',
                  'left' => '92',
                  'right' => '93',
                  'created' => '2013-01-13 20:33:17',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '83',
                    'name' => 'Links',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:36:34',
                    'updated' => '2014-05-30 17:36:34',
                    'foreign_key' => '129',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '129',
                    'parent_id' => '126',
                    'name' => '链接管理',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/links/list',
                    'left' => '92',
                    'right' => '93',
                    'created' => '2013-01-13 20:33:17',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
          2 => 
          array (
            'Menu' => 
            array (
              'id' => '132',
              'parent_id' => '36',
              'name' => '订单管理',
              'slug' => '',
              'visible' => '1',
              'rel' => '',
              'target' => '',
              'link' => '#',
              'left' => '73',
              'right' => '78',
              'created' => '2014-01-07 22:02:20',
              'updated' => '2014-11-18 22:14:15',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => '',
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'en_us' => 
              array (
                'id' => '78',
                'name' => 'Order Management',
                'published' => '0',
                'deleted' => '0',
                'created' => '2014-05-30 17:34:37',
                'updated' => '2014-05-30 17:34:37',
                'foreign_key' => '132',
                'locale' => 'en_us',
              ),
              'zh_cn' => 
              array (
                'id' => '132',
                'parent_id' => '36',
                'name' => '订单管理',
                'slug' => '',
                'visible' => '1',
                'rel' => '',
                'target' => '',
                'link' => '#',
                'left' => '73',
                'right' => '78',
                'created' => '2014-01-07 22:02:20',
                'updated' => '2014-11-18 22:14:15',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => '',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '133',
                  'parent_id' => '132',
                  'name' => '订单管理',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/orders/list',
                  'left' => '74',
                  'right' => '75',
                  'created' => '2014-01-07 22:03:15',
                  'updated' => '2014-11-18 22:14:27',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '79',
                    'name' => 'Order Management',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:34:55',
                    'updated' => '2014-05-30 17:34:55',
                    'foreign_key' => '133',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '133',
                    'parent_id' => '132',
                    'name' => '订单管理',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/orders/list',
                    'left' => '74',
                    'right' => '75',
                    'created' => '2014-01-07 22:03:15',
                    'updated' => '2014-11-18 22:14:27',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '134',
                  'parent_id' => '132',
                  'name' => '订单商品',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/carts/list',
                  'left' => '76',
                  'right' => '77',
                  'created' => '2014-01-07 22:03:40',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '80',
                    'name' => 'Order Products',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:35:09',
                    'updated' => '2014-05-30 17:35:09',
                    'foreign_key' => '134',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '134',
                    'parent_id' => '132',
                    'name' => '订单商品',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/carts/list',
                    'left' => '76',
                    'right' => '77',
                    'created' => '2014-01-07 22:03:40',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
          3 => 
          array (
            'Menu' => 
            array (
              'id' => '148',
              'parent_id' => '36',
              'name' => '邮件营销',
              'slug' => '',
              'visible' => '1',
              'rel' => '',
              'target' => '',
              'link' => '#',
              'left' => '97',
              'right' => '106',
              'created' => '2014-12-02 07:29:50',
              'updated' => '2014-12-02 07:29:50',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => '',
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'en_us' => 
              array (
                'id' => '89',
                'name' => 'Email Marketing',
                'published' => '0',
                'deleted' => '0',
                'created' => '2014-05-30 18:07:24',
                'updated' => '2014-12-02 07:29:50',
                'foreign_key' => '148',
                'locale' => 'en_us',
              ),
              'zh_cn' => 
              array (
                'id' => '148',
                'parent_id' => '36',
                'name' => '邮件营销',
                'slug' => '',
                'visible' => '1',
                'rel' => '',
                'target' => '',
                'link' => '#',
                'left' => '97',
                'right' => '106',
                'created' => '2014-12-02 07:29:50',
                'updated' => '2014-12-02 07:29:50',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => '',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '150',
                  'parent_id' => '148',
                  'name' => '邮件模板',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/templates/list?model=email',
                  'left' => '98',
                  'right' => '99',
                  'created' => '2014-12-02 07:48:08',
                  'updated' => '2014-12-02 07:48:08',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '90',
                    'name' => 'Keywords',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 18:10:55',
                    'updated' => '2014-12-02 07:48:08',
                    'foreign_key' => '150',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '150',
                    'parent_id' => '148',
                    'name' => '邮件模板',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/templates/list?model=email',
                    'left' => '98',
                    'right' => '99',
                    'created' => '2014-12-02 07:48:08',
                    'updated' => '2014-12-02 07:48:08',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '151',
                  'parent_id' => '148',
                  'name' => '发送邮件',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/tools/email',
                  'left' => '100',
                  'right' => '101',
                  'created' => '2014-12-02 07:48:08',
                  'updated' => '2014-12-02 07:49:52',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '151',
                    'parent_id' => '148',
                    'name' => '发送邮件',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/tools/email',
                    'left' => '100',
                    'right' => '101',
                    'created' => '2014-12-02 07:48:08',
                    'updated' => '2014-12-02 07:49:52',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              2 => 
              array (
                'Menu' => 
                array (
                  'id' => '152',
                  'parent_id' => '148',
                  'name' => '邮件设置',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/settings/prefix/Email',
                  'left' => '102',
                  'right' => '103',
                  'created' => '2014-12-02 09:29:01',
                  'updated' => '2014-12-02 09:29:01',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '101',
                    'name' => '',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-12-02 09:29:01',
                    'updated' => '2014-12-02 09:29:01',
                    'foreign_key' => '152',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '152',
                    'parent_id' => '148',
                    'name' => '邮件设置',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/settings/prefix/Email',
                    'left' => '102',
                    'right' => '103',
                    'created' => '2014-12-02 09:29:01',
                    'updated' => '2014-12-02 09:29:01',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              3 => 
              array (
                'Menu' => 
                array (
                  'id' => '161',
                  'parent_id' => '148',
                  'name' => 'SMTP邮箱管理',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/smtps/list',
                  'left' => '104',
                  'right' => '105',
                  'created' => '2016-05-20 08:56:35',
                  'updated' => '2016-05-20 08:56:35',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '161',
                    'parent_id' => '148',
                    'name' => 'SMTP邮箱管理',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/smtps/list',
                    'left' => '104',
                    'right' => '105',
                    'created' => '2016-05-20 08:56:35',
                    'updated' => '2016-05-20 08:56:35',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
          4 => 
          array (
            'Menu' => 
            array (
              'id' => '149',
              'parent_id' => '36',
              'name' => '邮件营销',
              'slug' => '',
              'visible' => '1',
              'rel' => '',
              'target' => '',
              'link' => '#',
              'left' => '107',
              'right' => '108',
              'created' => '2014-12-02 07:29:50',
              'updated' => '2014-12-02 07:29:50',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => '',
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '149',
                'parent_id' => '36',
                'name' => '邮件营销',
                'slug' => '',
                'visible' => '1',
                'rel' => '',
                'target' => '',
                'link' => '#',
                'left' => '107',
                'right' => '108',
                'created' => '2014-12-02 07:29:50',
                'updated' => '2014-12-02 07:29:50',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => '',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
        ),
      ),
      3 => 
      array (
        'Menu' => 
        array (
          'id' => '57',
          'parent_id' => '1',
          'name' => '系统',
          'slug' => 'system',
          'visible' => '1',
          'rel' => '',
          'target' => '',
          'link' => '/admin/menus/menu/57',
          'left' => '110',
          'right' => '143',
          'created' => '2010-06-26 12:14:21',
          'updated' => '2014-11-09 11:36:02',
          'deleted' => '0',
          'locale' => 'zh_cn',
          'prefix_icon' => 'glyphicon glyphicon-cloud',
        ),
        'Uploadfile' => 
        array (
        ),
        'MenuI18n' => 
        array (
          'en_us' => 
          array (
            'id' => '33',
            'name' => 'Maintain',
            'published' => '0',
            'deleted' => '0',
            'created' => '2012-08-16 23:18:28',
            'updated' => '2012-08-19 22:26:23',
            'foreign_key' => '57',
            'locale' => 'en_us',
          ),
          'zh_cn' => 
          array (
            'id' => '57',
            'parent_id' => '1',
            'name' => '系统',
            'slug' => 'system',
            'visible' => '1',
            'rel' => '',
            'target' => '',
            'link' => '/admin/menus/menu/57',
            'left' => '110',
            'right' => '143',
            'created' => '2010-06-26 12:14:21',
            'updated' => '2014-11-09 11:36:02',
            'deleted' => '0',
            'locale' => 'zh_cn',
            'prefix_icon' => 'glyphicon glyphicon-cloud',
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
          0 => 
          array (
            'Menu' => 
            array (
              'id' => '58',
              'parent_id' => '57',
              'name' => '分类及选项',
              'slug' => '',
              'visible' => '1',
              'rel' => '',
              'target' => '',
              'link' => '#',
              'left' => '131',
              'right' => '136',
              'created' => '2010-06-26 12:14:47',
              'updated' => '2014-05-30 18:13:07',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => NULL,
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'en_us' => 
              array (
                'id' => '67',
                'name' => 'Category And Options',
                'published' => '0',
                'deleted' => '0',
                'created' => '2014-05-30 17:29:35',
                'updated' => '2014-05-30 17:29:35',
                'foreign_key' => '58',
                'locale' => 'en_us',
              ),
              'zh_cn' => 
              array (
                'id' => '58',
                'parent_id' => '57',
                'name' => '分类及选项',
                'slug' => '',
                'visible' => '1',
                'rel' => '',
                'target' => '',
                'link' => '#',
                'left' => '131',
                'right' => '136',
                'created' => '2010-06-26 12:14:47',
                'updated' => '2014-05-30 18:13:07',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => NULL,
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '25',
                  'parent_id' => '58',
                  'name' => '选项分类管理',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '/admin/misccates/list',
                  'left' => '132',
                  'right' => '133',
                  'created' => '2010-05-15 18:31:43',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '69',
                    'name' => 'Misc Categories',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:30:21',
                    'updated' => '2014-05-30 17:30:21',
                    'foreign_key' => '25',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '25',
                    'parent_id' => '58',
                    'name' => '选项分类管理',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '/admin/misccates/list',
                    'left' => '132',
                    'right' => '133',
                    'created' => '2010-05-15 18:31:43',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '142',
                  'parent_id' => '58',
                  'name' => '城市地区管理',
                  'slug' => 'cityarea',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '#',
                  'link' => '/admin/city_areas/list',
                  'left' => '134',
                  'right' => '135',
                  'created' => '2014-05-24 22:47:43',
                  'updated' => '2014-11-18 22:18:00',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '68',
                    'name' => 'City Area Management',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:30:00',
                    'updated' => '2014-05-30 18:12:43',
                    'foreign_key' => '142',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '142',
                    'parent_id' => '58',
                    'name' => '城市地区管理',
                    'slug' => 'cityarea',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '#',
                    'link' => '/admin/city_areas/list',
                    'left' => '134',
                    'right' => '135',
                    'created' => '2014-05-24 22:47:43',
                    'updated' => '2014-11-18 22:18:00',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
          1 => 
          array (
            'Menu' => 
            array (
              'id' => '59',
              'parent_id' => '57',
              'name' => '系统工具',
              'slug' => '',
              'visible' => '1',
              'rel' => '',
              'target' => '',
              'link' => '#',
              'left' => '111',
              'right' => '130',
              'created' => '2010-06-26 12:38:23',
              'updated' => '2014-05-30 18:13:07',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => NULL,
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'en_us' => 
              array (
                'id' => '63',
                'name' => 'Tools',
                'published' => '0',
                'deleted' => '0',
                'created' => '2014-05-30 17:27:26',
                'updated' => '2014-05-30 17:27:26',
                'foreign_key' => '59',
                'locale' => 'en_us',
              ),
              'zh_cn' => 
              array (
                'id' => '59',
                'parent_id' => '57',
                'name' => '系统工具',
                'slug' => '',
                'visible' => '1',
                'rel' => '',
                'target' => '',
                'link' => '#',
                'left' => '111',
                'right' => '130',
                'created' => '2010-06-26 12:38:23',
                'updated' => '2014-05-30 18:13:07',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => NULL,
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '60',
                  'parent_id' => '59',
                  'name' => '清除全部缓存',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => 'ajaxAction',
                  'target' => 'main',
                  'link' => '/admin/tools/clearcache',
                  'left' => '128',
                  'right' => '129',
                  'created' => '2010-06-26 12:39:26',
                  'updated' => '2014-12-14 14:35:29',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '102',
                    'name' => 'Clear All Cache',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-12-14 14:35:29',
                    'updated' => '2014-12-14 14:35:29',
                    'foreign_key' => '60',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '60',
                    'parent_id' => '59',
                    'name' => '清除全部缓存',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => 'ajaxAction',
                    'target' => 'main',
                    'link' => '/admin/tools/clearcache',
                    'left' => '128',
                    'right' => '129',
                    'created' => '2010-06-26 12:39:26',
                    'updated' => '2014-12-14 14:35:29',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '89',
                  'parent_id' => '59',
                  'name' => '系统备份',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/tools/dbexport',
                  'left' => '112',
                  'right' => '113',
                  'created' => '2010-09-11 07:52:32',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '89',
                    'parent_id' => '59',
                    'name' => '系统备份',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/tools/dbexport',
                    'left' => '112',
                    'right' => '113',
                    'created' => '2010-09-11 07:52:32',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              2 => 
              array (
                'Menu' => 
                array (
                  'id' => '90',
                  'parent_id' => '59',
                  'name' => '备份恢复',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/tools/dbimport',
                  'left' => '114',
                  'right' => '115',
                  'created' => '2010-09-11 07:52:45',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '65',
                    'name' => 'Data Restore',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:28:08',
                    'updated' => '2014-05-30 17:28:08',
                    'foreign_key' => '90',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '90',
                    'parent_id' => '59',
                    'name' => '备份恢复',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/tools/dbimport',
                    'left' => '114',
                    'right' => '115',
                    'created' => '2010-09-11 07:52:45',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              3 => 
              array (
                'Menu' => 
                array (
                  'id' => '99',
                  'parent_id' => '59',
                  'name' => '自定义语言包',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/defined_language/defined_languages/index',
                  'left' => '118',
                  'right' => '119',
                  'created' => '2011-08-09 22:26:27',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '14',
                    'name' => 'Custom Languages',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2011-08-09 22:26:27',
                    'updated' => '2014-05-30 17:28:33',
                    'foreign_key' => '99',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '99',
                    'parent_id' => '59',
                    'name' => '自定义语言包',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/defined_language/defined_languages/index',
                    'left' => '118',
                    'right' => '119',
                    'created' => '2011-08-09 22:26:27',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              4 => 
              array (
                'Menu' => 
                array (
                  'id' => '147',
                  'parent_id' => '59',
                  'name' => '数据库升级',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/tools/runsql',
                  'left' => '116',
                  'right' => '117',
                  'created' => '2014-11-26 16:55:47',
                  'updated' => '2014-11-26 16:56:30',
                  'deleted' => '0',
                  'locale' => 'en_us',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '100',
                    'name' => '数据库升级',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-11-26 16:56:30',
                    'updated' => '2014-11-26 16:56:30',
                    'foreign_key' => '147',
                    'locale' => 'zh_cn',
                  ),
                  'en_us' => 
                  array (
                    'id' => '147',
                    'parent_id' => '59',
                    'name' => 'Update by Run SQL ',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/tools/runsql',
                    'left' => '116',
                    'right' => '117',
                    'created' => '2014-11-26 16:55:47',
                    'updated' => '2014-11-26 16:56:30',
                    'deleted' => '0',
                    'locale' => 'en_us',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              5 => 
              array (
                'Menu' => 
                array (
                  'id' => '156',
                  'parent_id' => '59',
                  'name' => '清除表结构缓存',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => 'ajaxAction',
                  'target' => 'main',
                  'link' => '/admin/tools/clearModelCache',
                  'left' => '120',
                  'right' => '121',
                  'created' => '2014-12-14 14:32:34',
                  'updated' => '2014-12-14 14:32:34',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '156',
                    'parent_id' => '59',
                    'name' => '清除表结构缓存',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => 'ajaxAction',
                    'target' => 'main',
                    'link' => '/admin/tools/clearModelCache',
                    'left' => '120',
                    'right' => '121',
                    'created' => '2014-12-14 14:32:34',
                    'updated' => '2014-12-14 14:32:34',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              6 => 
              array (
                'Menu' => 
                array (
                  'id' => '157',
                  'parent_id' => '59',
                  'name' => '清除核心缓存(语言、路径)',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => 'ajaxAction',
                  'target' => 'main',
                  'link' => '/admin/tools/clearCoreCache',
                  'left' => '122',
                  'right' => '123',
                  'created' => '2014-12-14 14:34:01',
                  'updated' => '2014-12-14 14:34:01',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '157',
                    'parent_id' => '59',
                    'name' => '清除核心缓存(语言、路径)',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => 'ajaxAction',
                    'target' => 'main',
                    'link' => '/admin/tools/clearCoreCache',
                    'left' => '122',
                    'right' => '123',
                    'created' => '2014-12-14 14:34:01',
                    'updated' => '2014-12-14 14:34:01',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              7 => 
              array (
                'Menu' => 
                array (
                  'id' => '158',
                  'parent_id' => '59',
                  'name' => '清除模板缓存',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => 'ajaxAction',
                  'target' => 'main',
                  'link' => '/admin/tools/clearViewCache',
                  'left' => '124',
                  'right' => '125',
                  'created' => '2014-12-14 14:34:34',
                  'updated' => '2014-12-14 14:34:34',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '158',
                    'parent_id' => '59',
                    'name' => '清除模板缓存',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => 'ajaxAction',
                    'target' => 'main',
                    'link' => '/admin/tools/clearViewCache',
                    'left' => '124',
                    'right' => '125',
                    'created' => '2014-12-14 14:34:34',
                    'updated' => '2014-12-14 14:34:34',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              8 => 
              array (
                'Menu' => 
                array (
                  'id' => '159',
                  'parent_id' => '59',
                  'name' => '清除运行缓存',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => 'ajaxAction',
                  'target' => 'main',
                  'link' => '/admin/tools/clearRunCache',
                  'left' => '126',
                  'right' => '127',
                  'created' => '2014-12-14 14:35:00',
                  'updated' => '2014-12-14 14:35:00',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '66',
                    'name' => 'Clear Run Cache',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:28:47',
                    'updated' => '2014-12-14 14:35:00',
                    'foreign_key' => '159',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '159',
                    'parent_id' => '59',
                    'name' => '清除运行缓存',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => 'ajaxAction',
                    'target' => 'main',
                    'link' => '/admin/tools/clearRunCache',
                    'left' => '126',
                    'right' => '127',
                    'created' => '2014-12-14 14:35:00',
                    'updated' => '2014-12-14 14:35:00',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
          2 => 
          array (
            'Menu' => 
            array (
              'id' => '153',
              'parent_id' => '57',
              'name' => '工作流',
              'slug' => 'workflow',
              'visible' => '1',
              'rel' => '',
              'target' => '',
              'link' => '#',
              'left' => '137',
              'right' => '142',
              'created' => '2014-12-05 06:13:20',
              'updated' => '2014-12-05 06:13:20',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => '',
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '153',
                'parent_id' => '57',
                'name' => '工作流',
                'slug' => 'workflow',
                'visible' => '1',
                'rel' => '',
                'target' => '',
                'link' => '#',
                'left' => '137',
                'right' => '142',
                'created' => '2014-12-05 06:13:20',
                'updated' => '2014-12-05 06:13:20',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => '',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '154',
                  'parent_id' => '153',
                  'name' => '流程分类',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/flows/list',
                  'left' => '138',
                  'right' => '139',
                  'created' => '2014-12-05 06:14:58',
                  'updated' => '2014-12-05 06:14:58',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '154',
                    'parent_id' => '153',
                    'name' => '流程分类',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/flows/list',
                    'left' => '138',
                    'right' => '139',
                    'created' => '2014-12-05 06:14:58',
                    'updated' => '2014-12-05 06:14:58',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '155',
                  'parent_id' => '153',
                  'name' => '流程操作',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/flowsteps/list',
                  'left' => '140',
                  'right' => '141',
                  'created' => '2014-12-05 06:16:48',
                  'updated' => '2014-12-05 06:16:48',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '64',
                    'name' => 'Work Flow Operations',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:27:51',
                    'updated' => '2014-12-05 06:16:48',
                    'foreign_key' => '155',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '155',
                    'parent_id' => '153',
                    'name' => '流程操作',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/flowsteps/list',
                    'left' => '140',
                    'right' => '141',
                    'created' => '2014-12-05 06:16:48',
                    'updated' => '2014-12-05 06:16:48',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
        ),
      ),
      4 => 
      array (
        'Menu' => 
        array (
          'id' => '120',
          'parent_id' => '1',
          'name' => '扩展',
          'slug' => 'extend',
          'visible' => '1',
          'rel' => '',
          'target' => '',
          'link' => '',
          'left' => '144',
          'right' => '167',
          'created' => '2013-01-12 23:30:56',
          'updated' => '2014-11-09 11:37:35',
          'deleted' => '0',
          'locale' => 'zh_cn',
          'prefix_icon' => 'glyphicon glyphicon-wrench',
        ),
        'Uploadfile' => 
        array (
        ),
        'MenuI18n' => 
        array (
          'en_us' => 
          array (
            'id' => '51',
            'name' => 'Extend',
            'published' => '0',
            'deleted' => '0',
            'created' => '2014-05-30 17:20:42',
            'updated' => '2014-05-30 17:20:42',
            'foreign_key' => '120',
            'locale' => 'en_us',
          ),
          'zh_cn' => 
          array (
            'id' => '120',
            'parent_id' => '1',
            'name' => '扩展',
            'slug' => 'extend',
            'visible' => '1',
            'rel' => '',
            'target' => '',
            'link' => '',
            'left' => '144',
            'right' => '167',
            'created' => '2013-01-12 23:30:56',
            'updated' => '2014-11-09 11:37:35',
            'deleted' => '0',
            'locale' => 'zh_cn',
            'prefix_icon' => 'glyphicon glyphicon-wrench',
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
          0 => 
          array (
            'Menu' => 
            array (
              'id' => '32',
              'parent_id' => '120',
              'name' => '模块管理',
              'slug' => 'models',
              'visible' => '1',
              'rel' => '',
              'target' => 'main',
              'link' => '/admin/menus/menu/32',
              'left' => '145',
              'right' => '150',
              'created' => '2010-05-22 11:49:38',
              'updated' => '2014-11-18 22:15:56',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => '',
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'en_us' => 
              array (
                'id' => '70',
                'name' => 'Model Management',
                'published' => '0',
                'deleted' => '0',
                'created' => '2014-05-30 17:30:39',
                'updated' => '2014-05-30 17:30:39',
                'foreign_key' => '32',
                'locale' => 'en_us',
              ),
              'zh_cn' => 
              array (
                'id' => '32',
                'parent_id' => '120',
                'name' => '模块管理',
                'slug' => 'models',
                'visible' => '1',
                'rel' => '',
                'target' => 'main',
                'link' => '/admin/menus/menu/32',
                'left' => '145',
                'right' => '150',
                'created' => '2010-05-22 11:49:38',
                'updated' => '2014-11-18 22:15:56',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => '',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '33',
                  'parent_id' => '32',
                  'name' => '字段管理',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '/admin/i18nfields/index',
                  'left' => '146',
                  'right' => '147',
                  'created' => '2010-05-22 11:51:02',
                  'updated' => '2014-11-18 22:16:05',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '71',
                    'name' => 'Field Management',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:30:56',
                    'updated' => '2014-05-30 17:30:56',
                    'foreign_key' => '33',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '33',
                    'parent_id' => '32',
                    'name' => '字段管理',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '/admin/i18nfields/index',
                    'left' => '146',
                    'right' => '147',
                    'created' => '2010-05-22 11:51:02',
                    'updated' => '2014-11-18 22:16:05',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '34',
                  'parent_id' => '32',
                  'name' => '模块扩展',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '/admin/modelextends/list',
                  'left' => '148',
                  'right' => '149',
                  'created' => '2010-05-22 18:03:54',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '72',
                    'name' => 'Model Extends',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:31:18',
                    'updated' => '2014-05-30 17:31:18',
                    'foreign_key' => '34',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '34',
                    'parent_id' => '32',
                    'name' => '模块扩展',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '/admin/modelextends/list',
                    'left' => '148',
                    'right' => '149',
                    'created' => '2010-05-22 18:03:54',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
          1 => 
          array (
            'Menu' => 
            array (
              'id' => '121',
              'parent_id' => '120',
              'name' => '开发工具',
              'slug' => 'develop_tools',
              'visible' => '1',
              'rel' => '',
              'target' => '',
              'link' => '',
              'left' => '151',
              'right' => '166',
              'created' => '2013-01-12 23:32:14',
              'updated' => '2014-05-30 18:13:07',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => NULL,
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'en_us' => 
              array (
                'id' => '73',
                'name' => 'Dev Tools',
                'published' => '0',
                'deleted' => '0',
                'created' => '2014-05-30 17:31:34',
                'updated' => '2014-05-30 17:31:34',
                'foreign_key' => '121',
                'locale' => 'en_us',
              ),
              'zh_cn' => 
              array (
                'id' => '121',
                'parent_id' => '120',
                'name' => '开发工具',
                'slug' => 'develop_tools',
                'visible' => '1',
                'rel' => '',
                'target' => '',
                'link' => '',
                'left' => '151',
                'right' => '166',
                'created' => '2013-01-12 23:32:14',
                'updated' => '2014-05-30 18:13:07',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => NULL,
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '28',
                  'parent_id' => '121',
                  'name' => '后台菜单管理',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '/admin/menus/list',
                  'left' => '152',
                  'right' => '153',
                  'created' => '2010-05-18 23:11:56',
                  'updated' => '2014-11-18 22:16:21',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '16',
                    'name' => 'Admin Menu Management',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2011-08-27 09:42:07',
                    'updated' => '2014-05-30 17:31:54',
                    'foreign_key' => '28',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '28',
                    'parent_id' => '121',
                    'name' => '后台菜单管理',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '/admin/menus/list',
                    'left' => '152',
                    'right' => '153',
                    'created' => '2010-05-18 23:11:56',
                    'updated' => '2014-11-18 22:16:21',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '96',
                  'parent_id' => '121',
                  'name' => '插件管理',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/extensions/extensions/index',
                  'left' => '156',
                  'right' => '157',
                  'created' => '2011-03-06 01:24:00',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '75',
                    'name' => 'Plugins',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:32:28',
                    'updated' => '2014-05-30 17:32:28',
                    'foreign_key' => '96',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '96',
                    'parent_id' => '121',
                    'name' => '插件管理',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/extensions/extensions/index',
                    'left' => '156',
                    'right' => '157',
                    'created' => '2011-03-06 01:24:00',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              2 => 
              array (
                'Menu' => 
                array (
                  'id' => '98',
                  'parent_id' => '121',
                  'name' => '更新语言包缓存',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/tools/updateLanCache',
                  'left' => '164',
                  'right' => '165',
                  'created' => '2011-08-04 21:16:50',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '13',
                    'name' => 'Update Language Cache',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2011-08-04 21:16:51',
                    'updated' => '2011-08-04 21:16:51',
                    'foreign_key' => '98',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '98',
                    'parent_id' => '121',
                    'name' => '更新语言包缓存',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/tools/updateLanCache',
                    'left' => '164',
                    'right' => '165',
                    'created' => '2011-08-04 21:16:50',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              3 => 
              array (
                'Menu' => 
                array (
                  'id' => '122',
                  'parent_id' => '121',
                  'name' => '设置管理',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/settings/list',
                  'left' => '154',
                  'right' => '155',
                  'created' => '2013-01-12 23:38:44',
                  'updated' => '2014-11-18 22:16:31',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '74',
                    'name' => 'Setting Management',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:32:13',
                    'updated' => '2014-05-30 17:32:13',
                    'foreign_key' => '122',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '122',
                    'parent_id' => '121',
                    'name' => '设置管理',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/settings/list',
                    'left' => '154',
                    'right' => '155',
                    'created' => '2013-01-12 23:38:44',
                    'updated' => '2014-11-18 22:16:31',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              4 => 
              array (
                'Menu' => 
                array (
                  'id' => '106',
                  'parent_id' => '121',
                  'name' => '数据库升级sql生成',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/tools/dbsync',
                  'left' => '158',
                  'right' => '159',
                  'created' => '2011-09-17 21:44:26',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '22',
                    'name' => 'Generate DB Update SQL',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2011-09-17 21:44:26',
                    'updated' => '2014-05-30 17:34:06',
                    'foreign_key' => '106',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '106',
                    'parent_id' => '121',
                    'name' => '数据库升级sql生成',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/tools/dbsync',
                    'left' => '158',
                    'right' => '159',
                    'created' => '2011-09-17 21:44:26',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              5 => 
              array (
                'Menu' => 
                array (
                  'id' => '127',
                  'parent_id' => '121',
                  'name' => '导出安装sql',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/devtools/exportModelSql.html',
                  'left' => '160',
                  'right' => '161',
                  'created' => '2013-01-13 18:00:06',
                  'updated' => '2015-02-12 15:33:53',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '76',
                    'name' => 'Export Install SQL',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:32:51',
                    'updated' => '2015-02-12 15:33:53',
                    'foreign_key' => '127',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '127',
                    'parent_id' => '121',
                    'name' => '导出安装sql',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/devtools/exportModelSql.html',
                    'left' => '160',
                    'right' => '161',
                    'created' => '2013-01-13 18:00:06',
                    'updated' => '2015-02-12 15:33:53',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              6 => 
              array (
                'Menu' => 
                array (
                  'id' => '128',
                  'parent_id' => '121',
                  'name' => '初始化ACL',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/devtools/build_acl.html',
                  'left' => '162',
                  'right' => '163',
                  'created' => '2013-01-13 18:01:04',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '77',
                    'name' => 'Init ACL',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:33:04',
                    'updated' => '2014-05-30 17:33:04',
                    'foreign_key' => '128',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '128',
                    'parent_id' => '121',
                    'name' => '初始化ACL',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/devtools/build_acl.html',
                    'left' => '162',
                    'right' => '163',
                    'created' => '2013-01-13 18:01:04',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
        ),
      ),
      5 => 
      array (
        'Menu' => 
        array (
          'id' => '117',
          'parent_id' => '1',
          'name' => '内容',
          'slug' => 'content',
          'visible' => '1',
          'rel' => '',
          'target' => '',
          'link' => '/admin/menus/contentmenu',
          'left' => '2',
          'right' => '15',
          'created' => '2012-08-17 22:29:05',
          'updated' => '2014-11-09 11:05:34',
          'deleted' => '0',
          'locale' => 'zh_cn',
          'prefix_icon' => 'glyphicon glyphicon-th-list',
        ),
        'Uploadfile' => 
        array (
        ),
        'MenuI18n' => 
        array (
          'en_us' => 
          array (
            'id' => '38',
            'name' => 'Content',
            'published' => '0',
            'deleted' => '0',
            'created' => '2012-08-17 22:29:05',
            'updated' => '2014-05-30 10:22:52',
            'foreign_key' => '117',
            'locale' => 'en_us',
          ),
          'zh_cn' => 
          array (
            'id' => '117',
            'parent_id' => '1',
            'name' => '内容',
            'slug' => 'content',
            'visible' => '1',
            'rel' => '',
            'target' => '',
            'link' => '/admin/menus/contentmenu',
            'left' => '2',
            'right' => '15',
            'created' => '2012-08-17 22:29:05',
            'updated' => '2014-11-09 11:05:34',
            'deleted' => '0',
            'locale' => 'zh_cn',
            'prefix_icon' => 'glyphicon glyphicon-th-list',
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
          0 => 
          array (
            'Menu' => 
            array (
              'id' => '108',
              'parent_id' => '117',
              'name' => '采集管理',
              'slug' => '',
              'visible' => '1',
              'rel' => '',
              'target' => '',
              'link' => '#',
              'left' => '3',
              'right' => '14',
              'created' => '2011-12-24 13:54:54',
              'updated' => '2014-11-18 22:12:43',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => '',
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'en_us' => 
              array (
                'id' => '24',
                'name' => 'Spider Management',
                'published' => '0',
                'deleted' => '0',
                'created' => '2011-12-24 13:54:54',
                'updated' => '2014-05-30 17:21:50',
                'foreign_key' => '108',
                'locale' => 'en_us',
              ),
              'zh_cn' => 
              array (
                'id' => '108',
                'parent_id' => '117',
                'name' => '采集管理',
                'slug' => '',
                'visible' => '1',
                'rel' => '',
                'target' => '',
                'link' => '#',
                'left' => '3',
                'right' => '14',
                'created' => '2011-12-24 13:54:54',
                'updated' => '2014-11-18 22:12:43',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => '',
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '16',
                  'parent_id' => '108',
                  'name' => '采集规则设置',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '/admin/crawls/list',
                  'left' => '6',
                  'right' => '7',
                  'created' => '2010-05-15 17:41:16',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '4',
                    'name' => 'Crawl Rules',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2011-03-13 23:09:58',
                    'updated' => '2014-05-30 17:38:44',
                    'foreign_key' => '16',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '16',
                    'parent_id' => '108',
                    'name' => '采集规则设置',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '/admin/crawls/list',
                    'left' => '6',
                    'right' => '7',
                    'created' => '2010-05-15 17:41:16',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '101',
                  'parent_id' => '108',
                  'name' => '采集数据发布',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/crawl_title_lists/publishlist',
                  'left' => '12',
                  'right' => '13',
                  'created' => '2011-08-27 09:46:09',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '17',
                    'name' => 'Publish Data',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2011-08-27 09:46:09',
                    'updated' => '2014-05-30 17:40:06',
                    'foreign_key' => '101',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '101',
                    'parent_id' => '108',
                    'name' => '采集数据发布',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/crawl_title_lists/publishlist',
                    'left' => '12',
                    'right' => '13',
                    'created' => '2011-08-27 09:46:09',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              2 => 
              array (
                'Menu' => 
                array (
                  'id' => '109',
                  'parent_id' => '108',
                  'name' => '采集分类管理',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/modelcates/list/model:Crawl',
                  'left' => '4',
                  'right' => '5',
                  'created' => '2011-12-24 21:02:22',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '25',
                    'name' => 'Crawl Categories',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2011-12-24 21:02:22',
                    'updated' => '2014-05-30 17:40:27',
                    'foreign_key' => '109',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '109',
                    'parent_id' => '108',
                    'name' => '采集分类管理',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/modelcates/list/model:Crawl',
                    'left' => '4',
                    'right' => '5',
                    'created' => '2011-12-24 21:02:22',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              3 => 
              array (
                'Menu' => 
                array (
                  'id' => '110',
                  'parent_id' => '108',
                  'name' => '采集发布规则',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/crawl_releases/list/',
                  'left' => '10',
                  'right' => '11',
                  'created' => '2011-12-25 16:15:10',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '26',
                    'name' => 'Publish Rules',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2011-12-25 16:15:10',
                    'updated' => '2014-05-30 17:39:43',
                    'foreign_key' => '110',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '110',
                    'parent_id' => '108',
                    'name' => '采集发布规则',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/crawl_releases/list/',
                    'left' => '10',
                    'right' => '11',
                    'created' => '2011-12-25 16:15:10',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              4 => 
              array (
                'Menu' => 
                array (
                  'id' => '111',
                  'parent_id' => '108',
                  'name' => '采集发布站点',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/crawl_release_sites/list/',
                  'left' => '8',
                  'right' => '9',
                  'created' => '2011-12-26 22:01:08',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '27',
                    'name' => 'Data Publish Sites',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2011-12-26 22:01:08',
                    'updated' => '2014-05-30 17:39:19',
                    'foreign_key' => '111',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '111',
                    'parent_id' => '108',
                    'name' => '采集发布站点',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/crawl_release_sites/list/',
                    'left' => '8',
                    'right' => '9',
                    'created' => '2011-12-26 22:01:08',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
        ),
      ),
      6 => 
      array (
        'Menu' => 
        array (
          'id' => '123',
          'parent_id' => '1',
          'name' => '界面',
          'slug' => 'ui',
          'visible' => '1',
          'rel' => '',
          'target' => '',
          'link' => '#',
          'left' => '62',
          'right' => '71',
          'created' => '2013-01-13 13:50:40',
          'updated' => '2014-11-09 11:09:51',
          'deleted' => '0',
          'locale' => 'zh_cn',
          'prefix_icon' => 'fa fa-desktop',
        ),
        'Uploadfile' => 
        array (
        ),
        'MenuI18n' => 
        array (
          'en_us' => 
          array (
            'id' => '50',
            'name' => 'UI',
            'published' => '0',
            'deleted' => '0',
            'created' => '2014-05-30 17:20:26',
            'updated' => '2014-05-30 17:20:26',
            'foreign_key' => '123',
            'locale' => 'en_us',
          ),
          'zh_cn' => 
          array (
            'id' => '123',
            'parent_id' => '1',
            'name' => '界面',
            'slug' => 'ui',
            'visible' => '1',
            'rel' => '',
            'target' => '',
            'link' => '#',
            'left' => '62',
            'right' => '71',
            'created' => '2013-01-13 13:50:40',
            'updated' => '2014-11-09 11:09:51',
            'deleted' => '0',
            'locale' => 'zh_cn',
            'prefix_icon' => 'fa fa-desktop',
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
          0 => 
          array (
            'Menu' => 
            array (
              'id' => '135',
              'parent_id' => '123',
              'name' => '界面设置',
              'slug' => 'ui-setting',
              'visible' => '1',
              'rel' => '',
              'target' => '',
              'link' => '#',
              'left' => '63',
              'right' => '70',
              'created' => '2014-04-13 11:30:47',
              'updated' => '2014-05-30 18:13:07',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => NULL,
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'en_us' => 
              array (
                'id' => '85',
                'name' => 'UI Setting',
                'published' => '0',
                'deleted' => '0',
                'created' => '2014-05-30 17:37:18',
                'updated' => '2014-05-30 17:37:18',
                'foreign_key' => '135',
                'locale' => 'en_us',
              ),
              'zh_cn' => 
              array (
                'id' => '135',
                'parent_id' => '123',
                'name' => '界面设置',
                'slug' => 'ui-setting',
                'visible' => '1',
                'rel' => '',
                'target' => '',
                'link' => '#',
                'left' => '63',
                'right' => '70',
                'created' => '2014-04-13 11:30:47',
                'updated' => '2014-05-30 18:13:07',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => NULL,
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '124',
                  'parent_id' => '135',
                  'name' => '风格管理',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/styles/list',
                  'left' => '64',
                  'right' => '65',
                  'created' => '2013-01-13 13:51:37',
                  'updated' => '2014-11-18 22:13:47',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '86',
                    'name' => 'Style Management',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:37:31',
                    'updated' => '2014-05-30 17:37:31',
                    'foreign_key' => '124',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '124',
                    'parent_id' => '135',
                    'name' => '风格管理',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/styles/list',
                    'left' => '64',
                    'right' => '65',
                    'created' => '2013-01-13 13:51:37',
                    'updated' => '2014-11-18 22:13:47',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '125',
                  'parent_id' => '135',
                  'name' => '模板套系',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/tools/themes',
                  'left' => '66',
                  'right' => '67',
                  'created' => '2013-01-13 13:52:08',
                  'updated' => '2014-11-18 22:13:58',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '87',
                    'name' => 'Theme Management',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:37:44',
                    'updated' => '2014-05-30 17:37:44',
                    'foreign_key' => '125',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '125',
                    'parent_id' => '135',
                    'name' => '模板套系',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/tools/themes',
                    'left' => '66',
                    'right' => '67',
                    'created' => '2013-01-13 13:52:08',
                    'updated' => '2014-11-18 22:13:58',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              2 => 
              array (
                'Menu' => 
                array (
                  'id' => '136',
                  'parent_id' => '135',
                  'name' => '区域列表管理',
                  'slug' => 'section-list',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/regions/list',
                  'left' => '68',
                  'right' => '69',
                  'created' => '2014-04-13 11:33:41',
                  'updated' => '2014-11-18 22:14:03',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => '',
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'en_us' => 
                  array (
                    'id' => '88',
                    'name' => 'Section Data List Management',
                    'published' => '0',
                    'deleted' => '0',
                    'created' => '2014-05-30 17:38:08',
                    'updated' => '2014-05-30 17:38:08',
                    'foreign_key' => '136',
                    'locale' => 'en_us',
                  ),
                  'zh_cn' => 
                  array (
                    'id' => '136',
                    'parent_id' => '135',
                    'name' => '区域列表管理',
                    'slug' => 'section-list',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/regions/list',
                    'left' => '68',
                    'right' => '69',
                    'created' => '2014-04-13 11:33:41',
                    'updated' => '2014-11-18 22:14:03',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => '',
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  ),
  1 => 
  array (
    'Menu' => 
    array (
      'id' => '30',
      'parent_id' => NULL,
      'name' => '办公系统',
      'slug' => 'oa',
      'visible' => '1',
      'rel' => '',
      'target' => '',
      'link' => '#',
      'left' => '169',
      'right' => '210',
      'created' => '2010-05-21 23:55:18',
      'updated' => '2014-05-30 18:13:07',
      'deleted' => '0',
      'locale' => 'zh_cn',
      'prefix_icon' => NULL,
    ),
    'Uploadfile' => 
    array (
    ),
    'MenuI18n' => 
    array (
      'en_us' => 
      array (
        'id' => '42',
        'name' => 'System',
        'published' => '0',
        'deleted' => '0',
        'created' => '2012-08-22 15:29:52',
        'updated' => '2012-08-22 15:29:52',
        'foreign_key' => '30',
        'locale' => 'en_us',
      ),
      'zh_cn' => 
      array (
        'id' => '30',
        'parent_id' => NULL,
        'name' => '办公系统',
        'slug' => 'oa',
        'visible' => '1',
        'rel' => '',
        'target' => '',
        'link' => '#',
        'left' => '169',
        'right' => '210',
        'created' => '2010-05-21 23:55:18',
        'updated' => '2014-05-30 18:13:07',
        'deleted' => '0',
        'locale' => 'zh_cn',
        'prefix_icon' => NULL,
      ),
    ),
    'Tag' => 
    array (
    ),
    'children' => 
    array (
      0 => 
      array (
        'Menu' => 
        array (
          'id' => '50',
          'parent_id' => '30',
          'name' => '调查与评价',
          'slug' => '',
          'visible' => '1',
          'rel' => '',
          'target' => '0',
          'link' => '/admin/menus/menu/50',
          'left' => '194',
          'right' => '201',
          'created' => '2010-06-26 09:05:48',
          'updated' => '2014-05-30 18:13:07',
          'deleted' => '0',
          'locale' => 'zh_cn',
          'prefix_icon' => NULL,
        ),
        'Uploadfile' => 
        array (
        ),
        'MenuI18n' => 
        array (
          'zh_cn' => 
          array (
            'id' => '50',
            'parent_id' => '30',
            'name' => '调查与评价',
            'slug' => '',
            'visible' => '1',
            'rel' => '',
            'target' => '0',
            'link' => '/admin/menus/menu/50',
            'left' => '194',
            'right' => '201',
            'created' => '2010-06-26 09:05:48',
            'updated' => '2014-05-30 18:13:07',
            'deleted' => '0',
            'locale' => 'zh_cn',
            'prefix_icon' => NULL,
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
          0 => 
          array (
            'Menu' => 
            array (
              'id' => '91',
              'parent_id' => '50',
              'name' => '调查管理',
              'slug' => NULL,
              'visible' => '1',
              'rel' => NULL,
              'target' => '',
              'link' => '',
              'left' => '195',
              'right' => '200',
              'created' => '2010-09-11 08:06:04',
              'updated' => '2014-05-30 18:13:07',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => NULL,
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '91',
                'parent_id' => '50',
                'name' => '调查管理',
                'slug' => NULL,
                'visible' => '1',
                'rel' => NULL,
                'target' => '',
                'link' => '',
                'left' => '195',
                'right' => '200',
                'created' => '2010-09-11 08:06:04',
                'updated' => '2014-05-30 18:13:07',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => NULL,
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '92',
                  'parent_id' => '91',
                  'name' => '调查管理',
                  'slug' => NULL,
                  'visible' => '1',
                  'rel' => NULL,
                  'target' => '',
                  'link' => '',
                  'left' => '196',
                  'right' => '197',
                  'created' => '2010-09-11 08:08:17',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '92',
                    'parent_id' => '91',
                    'name' => '调查管理',
                    'slug' => NULL,
                    'visible' => '1',
                    'rel' => NULL,
                    'target' => '',
                    'link' => '',
                    'left' => '196',
                    'right' => '197',
                    'created' => '2010-09-11 08:08:17',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '93',
                  'parent_id' => '91',
                  'name' => '评价管理',
                  'slug' => NULL,
                  'visible' => '1',
                  'rel' => NULL,
                  'target' => '',
                  'link' => '/admin/appraises/list',
                  'left' => '198',
                  'right' => '199',
                  'created' => '2010-09-11 08:08:31',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '93',
                    'parent_id' => '91',
                    'name' => '评价管理',
                    'slug' => NULL,
                    'visible' => '1',
                    'rel' => NULL,
                    'target' => '',
                    'link' => '/admin/appraises/list',
                    'left' => '198',
                    'right' => '199',
                    'created' => '2010-09-11 08:08:31',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
        ),
      ),
      1 => 
      array (
        'Menu' => 
        array (
          'id' => '51',
          'parent_id' => '30',
          'name' => '人力资源管理',
          'slug' => '',
          'visible' => '1',
          'rel' => '',
          'target' => '',
          'link' => '/admin/menus/menu/51',
          'left' => '182',
          'right' => '193',
          'created' => '2010-06-26 09:07:03',
          'updated' => '2014-05-30 18:13:07',
          'deleted' => '0',
          'locale' => 'zh_cn',
          'prefix_icon' => NULL,
        ),
        'Uploadfile' => 
        array (
        ),
        'MenuI18n' => 
        array (
          'zh_cn' => 
          array (
            'id' => '51',
            'parent_id' => '30',
            'name' => '人力资源管理',
            'slug' => '',
            'visible' => '1',
            'rel' => '',
            'target' => '',
            'link' => '/admin/menus/menu/51',
            'left' => '182',
            'right' => '193',
            'created' => '2010-06-26 09:07:03',
            'updated' => '2014-05-30 18:13:07',
            'deleted' => '0',
            'locale' => 'zh_cn',
            'prefix_icon' => NULL,
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
          0 => 
          array (
            'Menu' => 
            array (
              'id' => '61',
              'parent_id' => '51',
              'name' => '部门与人员',
              'slug' => '',
              'visible' => '1',
              'rel' => '',
              'target' => '',
              'link' => '#',
              'left' => '183',
              'right' => '190',
              'created' => '2010-06-26 15:27:36',
              'updated' => '2014-05-30 18:13:07',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => NULL,
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '61',
                'parent_id' => '51',
                'name' => '部门与人员',
                'slug' => '',
                'visible' => '1',
                'rel' => '',
                'target' => '',
                'link' => '#',
                'left' => '183',
                'right' => '190',
                'created' => '2010-06-26 15:27:36',
                'updated' => '2014-05-30 18:13:07',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => NULL,
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '62',
                  'parent_id' => '61',
                  'name' => '部门管理',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '/admin/organizations/list',
                  'left' => '184',
                  'right' => '185',
                  'created' => '2010-06-26 15:30:37',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '62',
                    'parent_id' => '61',
                    'name' => '部门管理',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '/admin/organizations/list',
                    'left' => '184',
                    'right' => '185',
                    'created' => '2010-06-26 15:30:37',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '63',
                  'parent_id' => '61',
                  'name' => '职位管理',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '/admin/positions/list',
                  'left' => '186',
                  'right' => '187',
                  'created' => '2010-06-26 18:39:47',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '63',
                    'parent_id' => '61',
                    'name' => '职位管理',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '/admin/positions/list',
                    'left' => '186',
                    'right' => '187',
                    'created' => '2010-06-26 18:39:47',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              2 => 
              array (
                'Menu' => 
                array (
                  'id' => '64',
                  'parent_id' => '61',
                  'name' => '任职管理',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/tenures/list',
                  'left' => '188',
                  'right' => '189',
                  'created' => '2010-06-26 18:43:33',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '64',
                    'parent_id' => '61',
                    'name' => '任职管理',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/tenures/list',
                    'left' => '188',
                    'right' => '189',
                    'created' => '2010-06-26 18:43:33',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
          1 => 
          array (
            'Menu' => 
            array (
              'id' => '82',
              'parent_id' => '51',
              'name' => '人员角色',
              'slug' => NULL,
              'visible' => '1',
              'rel' => NULL,
              'target' => '',
              'link' => '#',
              'left' => '191',
              'right' => '192',
              'created' => '2010-08-21 09:49:26',
              'updated' => '2014-05-30 18:13:07',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => NULL,
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '82',
                'parent_id' => '51',
                'name' => '人员角色',
                'slug' => NULL,
                'visible' => '1',
                'rel' => NULL,
                'target' => '',
                'link' => '#',
                'left' => '191',
                'right' => '192',
                'created' => '2010-08-21 09:49:26',
                'updated' => '2014-05-30 18:13:07',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => NULL,
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
            ),
          ),
        ),
      ),
      2 => 
      array (
        'Menu' => 
        array (
          'id' => '52',
          'parent_id' => '30',
          'name' => '任务管理',
          'slug' => '',
          'visible' => '1',
          'rel' => '',
          'target' => '',
          'link' => '/admin/menus/menu/52',
          'left' => '170',
          'right' => '181',
          'created' => '2010-06-26 09:07:36',
          'updated' => '2014-05-30 18:13:07',
          'deleted' => '0',
          'locale' => 'zh_cn',
          'prefix_icon' => NULL,
        ),
        'Uploadfile' => 
        array (
        ),
        'MenuI18n' => 
        array (
          'zh_cn' => 
          array (
            'id' => '52',
            'parent_id' => '30',
            'name' => '任务管理',
            'slug' => '',
            'visible' => '1',
            'rel' => '',
            'target' => '',
            'link' => '/admin/menus/menu/52',
            'left' => '170',
            'right' => '181',
            'created' => '2010-06-26 09:07:36',
            'updated' => '2014-05-30 18:13:07',
            'deleted' => '0',
            'locale' => 'zh_cn',
            'prefix_icon' => NULL,
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
          0 => 
          array (
            'Menu' => 
            array (
              'id' => '73',
              'parent_id' => '52',
              'name' => '任务管理',
              'slug' => '',
              'visible' => '1',
              'rel' => '',
              'target' => '',
              'link' => '#',
              'left' => '171',
              'right' => '180',
              'created' => '2010-08-05 21:16:59',
              'updated' => '2014-05-30 18:13:07',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => NULL,
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '73',
                'parent_id' => '52',
                'name' => '任务管理',
                'slug' => '',
                'visible' => '1',
                'rel' => '',
                'target' => '',
                'link' => '#',
                'left' => '171',
                'right' => '180',
                'created' => '2010-08-05 21:16:59',
                'updated' => '2014-05-30 18:13:07',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => NULL,
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '74',
                  'parent_id' => '73',
                  'name' => '任务管理',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '/admin/tasks/list.html',
                  'left' => '172',
                  'right' => '173',
                  'created' => '2010-08-05 21:17:52',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '74',
                    'parent_id' => '73',
                    'name' => '任务管理',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '/admin/tasks/list.html',
                    'left' => '172',
                    'right' => '173',
                    'created' => '2010-08-05 21:17:52',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '78',
                  'parent_id' => '73',
                  'name' => '任务分配与参与',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '/admin/taskings/list',
                  'left' => '174',
                  'right' => '175',
                  'created' => '2010-08-05 21:47:43',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '78',
                    'parent_id' => '73',
                    'name' => '任务分配与参与',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '/admin/taskings/list',
                    'left' => '174',
                    'right' => '175',
                    'created' => '2010-08-05 21:47:43',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              2 => 
              array (
                'Menu' => 
                array (
                  'id' => '79',
                  'parent_id' => '73',
                  'name' => '任务实施记录',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => 'main',
                  'link' => '/admin/taskexecutes/list.html',
                  'left' => '176',
                  'right' => '177',
                  'created' => '2010-08-05 21:48:55',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '79',
                    'parent_id' => '73',
                    'name' => '任务实施记录',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => 'main',
                    'link' => '/admin/taskexecutes/list.html',
                    'left' => '176',
                    'right' => '177',
                    'created' => '2010-08-05 21:48:55',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              3 => 
              array (
                'Menu' => 
                array (
                  'id' => '80',
                  'parent_id' => '73',
                  'name' => '个人任务查询',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '',
                  'left' => '178',
                  'right' => '179',
                  'created' => '2010-08-05 21:49:29',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '80',
                    'parent_id' => '73',
                    'name' => '个人任务查询',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '',
                    'left' => '178',
                    'right' => '179',
                    'created' => '2010-08-05 21:49:29',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
        ),
      ),
      3 => 
      array (
        'Menu' => 
        array (
          'id' => '75',
          'parent_id' => '30',
          'name' => '客户关系管理',
          'slug' => '',
          'visible' => '1',
          'rel' => '',
          'target' => '',
          'link' => '/admin/menus/menu/75',
          'left' => '202',
          'right' => '209',
          'created' => '2010-08-05 21:21:17',
          'updated' => '2014-05-30 18:13:07',
          'deleted' => '0',
          'locale' => 'zh_cn',
          'prefix_icon' => NULL,
        ),
        'Uploadfile' => 
        array (
        ),
        'MenuI18n' => 
        array (
          'zh_cn' => 
          array (
            'id' => '75',
            'parent_id' => '30',
            'name' => '客户关系管理',
            'slug' => '',
            'visible' => '1',
            'rel' => '',
            'target' => '',
            'link' => '/admin/menus/menu/75',
            'left' => '202',
            'right' => '209',
            'created' => '2010-08-05 21:21:17',
            'updated' => '2014-05-30 18:13:07',
            'deleted' => '0',
            'locale' => 'zh_cn',
            'prefix_icon' => NULL,
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
          0 => 
          array (
            'Menu' => 
            array (
              'id' => '76',
              'parent_id' => '75',
              'name' => '客户关系',
              'slug' => '',
              'visible' => '1',
              'rel' => '',
              'target' => '',
              'link' => '#',
              'left' => '203',
              'right' => '208',
              'created' => '2010-08-05 21:22:19',
              'updated' => '2014-05-30 18:13:07',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => NULL,
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '76',
                'parent_id' => '75',
                'name' => '客户关系',
                'slug' => '',
                'visible' => '1',
                'rel' => '',
                'target' => '',
                'link' => '#',
                'left' => '203',
                'right' => '208',
                'created' => '2010-08-05 21:22:19',
                'updated' => '2014-05-30 18:13:07',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => NULL,
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '77',
                  'parent_id' => '76',
                  'name' => '客户管理',
                  'slug' => '',
                  'visible' => '1',
                  'rel' => '',
                  'target' => '',
                  'link' => '/admin/customers/list',
                  'left' => '204',
                  'right' => '205',
                  'created' => '2010-08-05 21:22:56',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '77',
                    'parent_id' => '76',
                    'name' => '客户管理',
                    'slug' => '',
                    'visible' => '1',
                    'rel' => '',
                    'target' => '',
                    'link' => '/admin/customers/list',
                    'left' => '204',
                    'right' => '205',
                    'created' => '2010-08-05 21:22:56',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '83',
                  'parent_id' => '76',
                  'name' => '联系人管理',
                  'slug' => NULL,
                  'visible' => '1',
                  'rel' => NULL,
                  'target' => 'main',
                  'link' => '/admin/contacts/list',
                  'left' => '206',
                  'right' => '207',
                  'created' => '2010-08-30 21:10:40',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '83',
                    'parent_id' => '76',
                    'name' => '联系人管理',
                    'slug' => NULL,
                    'visible' => '1',
                    'rel' => NULL,
                    'target' => 'main',
                    'link' => '/admin/contacts/list',
                    'left' => '206',
                    'right' => '207',
                    'created' => '2010-08-30 21:10:40',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  ),
  2 => 
  array (
    'Menu' => 
    array (
      'id' => '53',
      'parent_id' => NULL,
      'name' => '个人中心',
      'slug' => '',
      'visible' => '1',
      'rel' => '',
      'target' => '',
      'link' => '',
      'left' => '211',
      'right' => '228',
      'created' => '2010-06-26 10:11:37',
      'updated' => '2014-05-30 18:13:07',
      'deleted' => '0',
      'locale' => 'zh_cn',
      'prefix_icon' => NULL,
    ),
    'Uploadfile' => 
    array (
    ),
    'MenuI18n' => 
    array (
      'en_us' => 
      array (
        'id' => '43',
        'name' => 'Dashboard',
        'published' => '0',
        'deleted' => '0',
        'created' => '2012-08-22 15:30:41',
        'updated' => '2012-08-22 15:30:41',
        'foreign_key' => '53',
        'locale' => 'en_us',
      ),
      'zh_cn' => 
      array (
        'id' => '53',
        'parent_id' => NULL,
        'name' => '个人中心',
        'slug' => '',
        'visible' => '1',
        'rel' => '',
        'target' => '',
        'link' => '',
        'left' => '211',
        'right' => '228',
        'created' => '2010-06-26 10:11:37',
        'updated' => '2014-05-30 18:13:07',
        'deleted' => '0',
        'locale' => 'zh_cn',
        'prefix_icon' => NULL,
      ),
    ),
    'Tag' => 
    array (
    ),
    'children' => 
    array (
      0 => 
      array (
        'Menu' => 
        array (
          'id' => '54',
          'parent_id' => '53',
          'name' => '工作流程',
          'slug' => '',
          'visible' => '1',
          'rel' => '',
          'target' => '',
          'link' => '/admin/flowsteps/menu',
          'left' => '214',
          'right' => '215',
          'created' => '2010-06-26 10:14:20',
          'updated' => '2014-05-30 18:13:07',
          'deleted' => '0',
          'locale' => 'zh_cn',
          'prefix_icon' => NULL,
        ),
        'Uploadfile' => 
        array (
        ),
        'MenuI18n' => 
        array (
          'zh_cn' => 
          array (
            'id' => '54',
            'parent_id' => '53',
            'name' => '工作流程',
            'slug' => '',
            'visible' => '1',
            'rel' => '',
            'target' => '',
            'link' => '/admin/flowsteps/menu',
            'left' => '214',
            'right' => '215',
            'created' => '2010-06-26 10:14:20',
            'updated' => '2014-05-30 18:13:07',
            'deleted' => '0',
            'locale' => 'zh_cn',
            'prefix_icon' => NULL,
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
        ),
      ),
      1 => 
      array (
        'Menu' => 
        array (
          'id' => '55',
          'parent_id' => '53',
          'name' => '个人资料',
          'slug' => '',
          'visible' => '1',
          'rel' => '',
          'target' => '',
          'link' => '/admin/menus/menu/55',
          'left' => '216',
          'right' => '227',
          'created' => '2010-06-26 10:14:37',
          'updated' => '2014-05-30 18:13:07',
          'deleted' => '0',
          'locale' => 'zh_cn',
          'prefix_icon' => NULL,
        ),
        'Uploadfile' => 
        array (
        ),
        'MenuI18n' => 
        array (
          'zh_cn' => 
          array (
            'id' => '55',
            'parent_id' => '53',
            'name' => '个人资料',
            'slug' => '',
            'visible' => '1',
            'rel' => '',
            'target' => '',
            'link' => '/admin/menus/menu/55',
            'left' => '216',
            'right' => '227',
            'created' => '2010-06-26 10:14:37',
            'updated' => '2014-05-30 18:13:07',
            'deleted' => '0',
            'locale' => 'zh_cn',
            'prefix_icon' => NULL,
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
          0 => 
          array (
            'Menu' => 
            array (
              'id' => '84',
              'parent_id' => '55',
              'name' => '修改密码',
              'slug' => '',
              'visible' => '1',
              'rel' => '',
              'target' => '',
              'link' => '/admin/staffs/editpassword',
              'left' => '217',
              'right' => '220',
              'created' => '2010-08-31 21:33:38',
              'updated' => '2014-05-30 18:13:07',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => NULL,
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '84',
                'parent_id' => '55',
                'name' => '修改密码',
                'slug' => '',
                'visible' => '1',
                'rel' => '',
                'target' => '',
                'link' => '/admin/staffs/editpassword',
                'left' => '217',
                'right' => '220',
                'created' => '2010-08-31 21:33:38',
                'updated' => '2014-05-30 18:13:07',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => NULL,
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '88',
                  'parent_id' => '84',
                  'name' => '修改密码',
                  'slug' => NULL,
                  'visible' => '1',
                  'rel' => NULL,
                  'target' => '',
                  'link' => '/admin/staffs/editpassword',
                  'left' => '218',
                  'right' => '219',
                  'created' => '2010-08-31 21:58:23',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '88',
                    'parent_id' => '84',
                    'name' => '修改密码',
                    'slug' => NULL,
                    'visible' => '1',
                    'rel' => NULL,
                    'target' => '',
                    'link' => '/admin/staffs/editpassword',
                    'left' => '218',
                    'right' => '219',
                    'created' => '2010-08-31 21:58:23',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
          1 => 
          array (
            'Menu' => 
            array (
              'id' => '85',
              'parent_id' => '55',
              'name' => '站内短信',
              'slug' => '',
              'visible' => '1',
              'rel' => '',
              'target' => '',
              'link' => '/admin/shortmessages/list',
              'left' => '221',
              'right' => '226',
              'created' => '2010-08-31 21:36:37',
              'updated' => '2014-05-30 18:13:07',
              'deleted' => '0',
              'locale' => 'zh_cn',
              'prefix_icon' => NULL,
            ),
            'Uploadfile' => 
            array (
            ),
            'MenuI18n' => 
            array (
              'zh_cn' => 
              array (
                'id' => '85',
                'parent_id' => '55',
                'name' => '站内短信',
                'slug' => '',
                'visible' => '1',
                'rel' => '',
                'target' => '',
                'link' => '/admin/shortmessages/list',
                'left' => '221',
                'right' => '226',
                'created' => '2010-08-31 21:36:37',
                'updated' => '2014-05-30 18:13:07',
                'deleted' => '0',
                'locale' => 'zh_cn',
                'prefix_icon' => NULL,
              ),
            ),
            'Tag' => 
            array (
            ),
            'children' => 
            array (
              0 => 
              array (
                'Menu' => 
                array (
                  'id' => '86',
                  'parent_id' => '85',
                  'name' => '收件箱',
                  'slug' => NULL,
                  'visible' => '1',
                  'rel' => NULL,
                  'target' => '',
                  'link' => '/admin/shortmessages/list/folder:inbox/',
                  'left' => '222',
                  'right' => '223',
                  'created' => '2010-08-31 21:39:55',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '86',
                    'parent_id' => '85',
                    'name' => '收件箱',
                    'slug' => NULL,
                    'visible' => '1',
                    'rel' => NULL,
                    'target' => '',
                    'link' => '/admin/shortmessages/list/folder:inbox/',
                    'left' => '222',
                    'right' => '223',
                    'created' => '2010-08-31 21:39:55',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
              1 => 
              array (
                'Menu' => 
                array (
                  'id' => '87',
                  'parent_id' => '85',
                  'name' => '发件箱',
                  'slug' => NULL,
                  'visible' => '1',
                  'rel' => NULL,
                  'target' => 'main',
                  'link' => '/admin/shortmessages/list/folder:outbox/',
                  'left' => '224',
                  'right' => '225',
                  'created' => '2010-08-31 21:40:32',
                  'updated' => '2014-05-30 18:13:07',
                  'deleted' => '0',
                  'locale' => 'zh_cn',
                  'prefix_icon' => NULL,
                ),
                'Uploadfile' => 
                array (
                ),
                'MenuI18n' => 
                array (
                  'zh_cn' => 
                  array (
                    'id' => '87',
                    'parent_id' => '85',
                    'name' => '发件箱',
                    'slug' => NULL,
                    'visible' => '1',
                    'rel' => NULL,
                    'target' => 'main',
                    'link' => '/admin/shortmessages/list/folder:outbox/',
                    'left' => '224',
                    'right' => '225',
                    'created' => '2010-08-31 21:40:32',
                    'updated' => '2014-05-30 18:13:07',
                    'deleted' => '0',
                    'locale' => 'zh_cn',
                    'prefix_icon' => NULL,
                  ),
                ),
                'Tag' => 
                array (
                ),
                'children' => 
                array (
                ),
              ),
            ),
          ),
        ),
      ),
      2 => 
      array (
        'Menu' => 
        array (
          'id' => '56',
          'parent_id' => '53',
          'name' => '个人内容管理',
          'slug' => 'pcontent',
          'visible' => '1',
          'rel' => '',
          'target' => '',
          'link' => '#',
          'left' => '212',
          'right' => '213',
          'created' => '2010-06-26 10:16:42',
          'updated' => '2016-05-20 22:29:29',
          'deleted' => '0',
          'locale' => 'zh_cn',
          'prefix_icon' => '',
        ),
        'Uploadfile' => 
        array (
        ),
        'MenuI18n' => 
        array (
          'zh_cn' => 
          array (
            'id' => '56',
            'parent_id' => '53',
            'name' => '个人内容管理',
            'slug' => 'pcontent',
            'visible' => '1',
            'rel' => '',
            'target' => '',
            'link' => '#',
            'left' => '212',
            'right' => '213',
            'created' => '2010-06-26 10:16:42',
            'updated' => '2016-05-20 22:29:29',
            'deleted' => '0',
            'locale' => 'zh_cn',
            'prefix_icon' => '',
          ),
        ),
        'Tag' => 
        array (
        ),
        'children' => 
        array (
        ),
      ),
    ),
  ),
);


	saveTreeItems($datas,'Menu');
