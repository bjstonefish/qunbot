set names utf8;
CREATE TABLE IF NOT EXISTS `miao_course_members` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(300) DEFAULT '',
  `course_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT '0',
  `identity` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `role_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'CourseMember', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-04 21:10:49', '2016-11-04 21:10:49', NULL),
(NULL, 'name', '1', '姓名', 'string', 'CourseMember', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', '', '', '1', '', NULL, '', '', '', 0, '2016-11-04 21:10:49', '2016-11-04 21:18:14', NULL),
(NULL, 'coverimg', '1', '头像', 'string', 'CourseMember', NULL, '300', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2016-11-04 21:10:49', '2016-11-04 21:18:37', NULL),
(NULL, 'course_id', '1', '课程编号', 'integer', 'CourseMember', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2016-11-04 21:10:49', '2016-11-04 21:16:06', NULL),
(NULL, 'user_id', '1', '用户编号', 'integer', 'CourseMember', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', '', '', '1', '', NULL, '', '', '', 0, '2016-11-04 21:10:49', '2016-11-04 21:12:31', NULL),
(NULL, 'identity', '1', '身份', 'integer', 'CourseMember', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '0=>普通听众\r\n1=>老师\r\n2=>主持人', '0', '', '', 'equal', 'select', '0', '1', '', NULL, '', '', '', 0, '2016-11-04 21:10:49', '2016-11-04 21:15:31', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'CourseMember', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-04 21:10:49', '2016-11-04 21:10:49', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'CourseMember', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-04 21:10:49', '2016-11-04 21:10:49', NULL),
(NULL, 'role_id', '1', '角色编号', 'integer', 'CourseMember', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-12-16 13:01:44', '2016-12-16 13:01:44', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'CourseMember', '课程成员', '', 'default', '', 0, '2016-11-04 21:10:48', '2016-11-04 21:10:48', 'miao_course_members', '', '', '', '0', NULL, NULL, '0', '0', '');
