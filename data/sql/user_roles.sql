set names utf8;
CREATE TABLE IF NOT EXISTS `miao_user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `started` datetime DEFAULT NULL,
  `ended` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'UserRole', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-03-14 22:08:20', '2015-03-14 22:08:20', NULL),
(NULL, 'role_id', '1', '角色', 'integer', 'UserRole', NULL, '11', 6, '1', '1', 'Role', 'id', 'name', NULL, '1', '', '0', '', '', '', 'select', '', '1', '', NULL, '', '', '', 0, '2015-03-14 22:08:20', '2015-03-25 13:09:15', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<options><conditions><Role.group>1</Role.group></conditions></options>\n'),
(NULL, 'user_id', '1', '用户编号', 'integer', 'UserRole', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-03-14 22:08:20', '2015-03-25 13:13:25', NULL),
(NULL, 'status', '1', '状态', 'integer', 'UserRole', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-03-14 22:08:20', '2015-03-14 22:08:20', NULL),
(NULL, 'started', '1', '开始时间', 'datetime', 'UserRole', NULL, '19', 2, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'datetime', '', '1', '', NULL, '', '', '', 0, '2015-03-14 22:08:20', '2015-03-14 22:11:55', NULL),
(NULL, 'ended', '1', '结束时间', 'datetime', 'UserRole', NULL, '19', 1, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'datetime', '', '1', '', NULL, '', '', '', 0, '2015-03-14 22:08:20', '2015-03-14 22:12:19', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'UserRole', '用户角色', '', 'default', '', 27, '2015-03-14 22:08:20', '2015-03-14 22:08:20', 'miao_user_roles', '', '', '', '0', NULL, NULL, '0', '0', '');
