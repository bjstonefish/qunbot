set names utf8;
CREATE TABLE IF NOT EXISTS `miao_ipview_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` char(15) DEFAULT NULL,
  `model` char(20) DEFAULT NULL,
  `data_id` int(11) DEFAULT '0',
  `created` date DEFAULT NULL,
  `view_nums` int(11) DEFAULT '0',
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip_day_unique` (`ip`,`model`,`data_id`,`created`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'ip', '1', '名称', 'char', 'IpviewLog', NULL, '15', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-02-13 22:14:34', '2015-02-14 16:01:44', NULL),
(NULL, 'id', '1', '编号', 'integer', 'IpviewLog', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-02-13 22:14:34', '2015-02-13 22:14:34', NULL),
(NULL, 'model', '1', '所属模块', 'char', 'IpviewLog', NULL, '20', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-02-13 22:14:34', '2015-02-14 09:30:52', NULL),
(NULL, 'data_id', '1', '数据编号', 'integer', 'IpviewLog', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '', 0, '2015-02-13 22:14:34', '2015-02-14 09:15:49', NULL),
(NULL, 'created', '1', '创建时间', 'date', 'IpviewLog', NULL, '10', 2, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'date', '', '1', '', NULL, '', '', '', 0, '2015-02-13 22:14:34', '2015-02-14 09:18:53', NULL),
(NULL, 'updated', '1', '最后更新时间', 'datetime', 'IpviewLog', 'en_us', '19', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'datetime', '', '1', '', NULL, '', '', '', 0, '2015-02-23 20:57:55', '2015-02-23 20:57:55', NULL),
(NULL, 'view_nums', '1', '查看次数', 'integer', 'IpviewLog', 'en_us', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-02-14 09:16:39', '2015-02-14 09:16:39', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'IpviewLog', 'ip访问记录', '', 'default', '', 27, '2015-02-14 09:20:17', '2015-02-14 09:20:17', 'miao_ipview_logs', '', '', '', '0', NULL, NULL, '0', '0', '');
