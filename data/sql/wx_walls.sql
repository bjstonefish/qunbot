set names utf8;
CREATE TABLE IF NOT EXISTS `miao_wx_walls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(200) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `wx_id` int(11) DEFAULT '0',
  `started` datetime DEFAULT NULL,
  `ended` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'WxWall', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-29 14:48:32', '2015-06-29 14:48:32', NULL),
(NULL, 'name', '1', '名称', 'string', 'WxWall', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-29 14:48:32', '2015-06-29 14:48:32', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'WxWall', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-29 14:48:32', '2015-06-29 14:48:32', NULL),
(NULL, 'cate_id', '1', '分类', 'integer', 'WxWall', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-29 14:48:32', '2015-06-29 14:48:32', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'WxWall', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-29 14:48:32', '2015-06-29 14:48:32', NULL),
(NULL, 'status', '1', '状态', 'integer', 'WxWall', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-29 14:48:32', '2015-06-29 14:48:32', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'WxWall', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-29 14:48:32', '2015-06-29 14:48:32', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'WxWall', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-06-29 14:48:32', '2015-06-29 14:48:32', NULL),
(NULL, 'wx_id', '1', '微信编号', 'integer', 'WxWall', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-06-29 14:49:42', '2015-06-29 14:49:42', NULL),
(NULL, 'started', '1', '开始时间', 'datetime', 'WxWall', 'zh_cn', '19', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'datetime', '', '1', '', NULL, '', '', '', 0, '2015-06-29 14:50:09', '2015-06-29 14:50:09', NULL),
(NULL, 'ended', '1', '开始时间', 'datetime', 'WxWall', 'zh_cn', '19', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'datetime', '', '1', '', NULL, '', '', '', 0, '2015-06-29 14:50:28', '2015-06-29 14:50:28', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'WxWall', '微信墙', '', 'default', '', 27, '2015-06-29 14:48:32', '2015-06-29 14:48:32', 'miao_wx_walls', '', '', '', '0', NULL, NULL, '0', '0', '');
