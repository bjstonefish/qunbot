set names utf8;
CREATE TABLE IF NOT EXISTS `miao_team_invites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `invite_code` varchar(44) DEFAULT NULL,
  `creator` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'TeamInvite', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-12-04 09:52:53', '2014-12-04 09:52:53', NULL),
(NULL, 'team_id', '1', '所属团队', 'integer', 'TeamInvite', NULL, '11', 6, '1', '1', 'Team', 'id', 'name', 0, '1', '', '0', '', '', '', 'select', '', '1', '', NULL, '', '', '', 0, '2014-12-04 09:52:53', '2014-12-04 09:54:45', NULL),
(NULL, 'user_id', '1', '接受邀请的用户', 'integer', 'TeamInvite', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-12-04 09:52:53', '2016-05-06 20:33:29', NULL),
(NULL, 'status', '1', '状态', 'integer', 'TeamInvite', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '-1=>拒绝\r\n0=>未处理\r\n1=>接受', '0', '', '', '', 'select', '0', '1', '', NULL, '', '', '', 0, '2014-12-04 09:52:53', '2014-12-24 14:36:55', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'TeamInvite', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-12-04 09:52:53', '2014-12-04 09:52:53', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'TeamInvite', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-12-04 09:52:53', '2014-12-04 09:52:53', NULL),
(NULL, 'email', '1', '邀请邮箱', 'string', 'TeamInvite', 'zh_cn', '64', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', 'none', '', 0, '2014-12-24 14:35:13', '2015-03-16 11:56:24', NULL),
(NULL, 'invite_code', '1', '邀请码', 'string', 'TeamInvite', 'zh_cn', '44', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-12-24 14:35:53', '2014-12-24 14:35:53', NULL),
(NULL, 'creator', '1', '发起人', 'integer', 'TeamInvite', 'en_us', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-05-06 20:34:12', '2016-05-06 20:34:12', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'TeamInvite', '团队邀请成员', '', 'default', '', 27, '2014-12-24 14:32:53', '2016-05-06 20:36:08', 'miao_team_invites', '', '', '', '0', 10, NULL, '0', '0', '');
