set names utf8;
CREATE TABLE IF NOT EXISTS `miao_event_handle_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `handle_name` varchar(200) DEFAULT NULL,
  `coverimg` varchar(200) DEFAULT '',
  `handle_id` int(11) DEFAULT '0',
  `event_id` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `times` smallint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'handle_name', '1', '事件处理器名称', 'string', 'EventHandleLog', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-11-16 22:50:06', '2014-11-17 09:56:51', NULL),
(NULL, 'id', '1', '编号', 'integer', 'EventHandleLog', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-11-16 22:50:06', '2014-11-16 22:50:06', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'EventHandleLog', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 1, '2014-11-16 22:50:06', '2014-11-16 22:50:06', NULL),
(NULL, 'handle_id', '1', '事件处理器编号', 'integer', 'EventHandleLog', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', '', '', '1', '', NULL, '', '', '', 0, '2014-11-16 22:50:06', '2014-11-17 09:59:17', NULL),
(NULL, 'event_id', '1', '事件编号', 'integer', 'EventHandleLog', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-11-16 22:50:06', '2014-11-17 09:58:38', NULL),
(NULL, 'status', '1', '状态', 'integer', 'EventHandleLog', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '0=>执行失败\r\n1=>执行成功', '0', '', '', '', 'select', '0', '1', '', NULL, '', '', '', 0, '2014-11-16 22:50:06', '2014-11-17 10:02:09', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'EventHandleLog', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-11-16 22:50:06', '2014-11-16 22:50:06', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'EventHandleLog', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-11-16 22:50:06', '2014-11-16 22:50:06', NULL),
(NULL, 'times', '1', '重试次数', 'integer', 'EventHandleLog', 'zh_cn', '3', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '', 0, '2014-11-17 10:01:28', '2014-11-17 10:01:28', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'EventHandleLog', '时间处理记录', '', 'default', '', 27, '2014-11-16 22:50:06', '2014-11-16 22:50:06', 'miao_event_handle_logs', '', '', '', '0', NULL, 0, '0', '0', '');
