set names utf8;
CREATE TABLE IF NOT EXISTS `miao_user_settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `settings` text,
  `status` tinyint(4) DEFAULT '0',
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'UserSetting', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-30 17:42:28', '2016-04-30 17:42:28', NULL),
(NULL, 'settings', '1', '参数设置', 'content', 'UserSetting', NULL, '65535', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'textarea', '', '1', '', NULL, '', '', 'json格式的数据，多维对象数组', 0, '2016-04-30 17:42:28', '2016-04-30 17:44:47', NULL),
(NULL, 'status', '1', '状态', 'integer', 'UserSetting', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-30 17:42:28', '2016-04-30 17:42:28', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'UserSetting', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-04-30 17:42:28', '2016-04-30 17:42:28', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'UserSetting', '用户设置', '', 'default', '', 27, '2016-04-30 17:42:27', '2016-04-30 17:42:27', 'miao_user_settings', '', 'self', '', '0', 2, NULL, '0', '0', '');
