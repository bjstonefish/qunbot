set names utf8;
CREATE TABLE IF NOT EXISTS `miao_wx_storefront_i18ns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(200) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `sub_title` varchar(60) DEFAULT NULL,
  `address` varchar(60) DEFAULT NULL,
  `business_hours` varchar(1000) DEFAULT NULL,
  `content` text,
  `foreign_key` int(11) DEFAULT '0',
  `locale` char(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'WxStorefrontI18n', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-25 14:11:37', '2015-01-25 14:11:37', NULL),
(NULL, 'name', '1', '名称', 'string', 'WxStorefrontI18n', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-25 14:11:37', '2015-01-25 14:11:37', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'WxStorefrontI18n', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 1, '2015-01-25 14:11:37', '2015-01-25 14:11:37', NULL),
(NULL, 'cate_id', '1', '分类', 'integer', 'WxStorefrontI18n', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 1, '2015-01-25 14:11:37', '2015-01-25 14:11:37', NULL),
(NULL, 'creator', '1', '编创建者', 'integer', 'WxStorefrontI18n', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-25 14:11:37', '2015-01-25 14:11:37', NULL),
(NULL, 'status', '1', '状态', 'integer', 'WxStorefrontI18n', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 1, '2015-01-25 14:11:37', '2015-01-25 14:11:37', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'WxStorefrontI18n', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-25 14:11:37', '2015-01-25 14:11:37', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'WxStorefrontI18n', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-25 14:11:37', '2015-01-25 14:11:37', NULL),
(NULL, 'sub_title', '1', '副标题', 'string', 'WxStorefrontI18n', NULL, '60', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-25 16:16:19', '2015-01-25 16:16:19', NULL),
(NULL, 'address', '1', '地址', 'string', 'WxStorefrontI18n', NULL, '60', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-25 16:16:19', '2015-01-25 16:16:19', NULL),
(NULL, 'business_hours', '1', '营业时间', 'string', 'WxStorefrontI18n', NULL, '1000', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2015-01-25 16:16:19', '2015-02-11 06:24:03', NULL),
(NULL, 'content', '1', '详细描述', 'content', 'WxStorefrontI18n', NULL, '65535', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'ckeditor', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-25 16:16:19', '2015-01-25 16:16:19', NULL),
(NULL, 'foreign_key', '1', '外键', 'integer', 'WxStorefrontI18n', NULL, '11', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-25 16:16:19', '2015-01-25 16:16:19', NULL),
(NULL, 'locale', '1', '语言版本', 'char', 'WxStorefrontI18n', 'en_us', '5', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-02-10 22:10:03', '2015-02-10 22:10:03', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'WxStorefrontI18n', '商铺店面', '', 'default', '', 27, '2015-01-25 14:11:37', '2015-01-25 14:11:37', 'miao_wx_storefront_i18ns', '', '', '', '0', 2, NULL, '0', '0', '');
