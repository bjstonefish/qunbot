set names utf8;
CREATE TABLE IF NOT EXISTS `miao_vods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(300) DEFAULT '',
  `album_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `published` tinyint(3) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `video_id` varchar(44) DEFAULT '',
  `summary` varchar(200) DEFAULT '',
  `duration` int(11) DEFAULT '0',
  `size` int(11) DEFAULT '0',
  `is_free` smallint(3) DEFAULT '0',
  `priority` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'vod', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-12 11:06:29', '2017-06-12 11:06:29', NULL),
(NULL, 'name', '1', '名称', 'string', 'vod', NULL, '200', 5, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-12 11:06:29', '2017-06-12 11:06:29', NULL),
(NULL, 'coverimg', '0', '封面图片', 'string', 'vod', NULL, '300', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'file', '', '1', '', NULL, '', '', '', 0, '2017-06-12 11:06:29', '2017-06-12 11:13:00', NULL),
(NULL, 'album_id', '1', '所属专辑', 'integer', 'vod', NULL, '11', 6, '1', '1', 'Album', 'id', 'name', 0, '1', '', '0', '', '', 'equal', 'select', '', '1', '', NULL, '', '', '', 0, '2017-06-12 11:06:29', '2017-06-14 18:38:58', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'vod', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-12 11:06:29', '2017-06-12 11:06:29', NULL),
(NULL, 'published', '1', '是否发布', 'integer', 'vod', NULL, '11', 3, '1', '1', '', NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-12 11:06:29', '2017-06-12 11:06:29', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'vod', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-12 11:06:29', '2017-06-12 11:06:29', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'vod', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-06-12 11:06:29', '2017-06-12 11:06:29', NULL),
(NULL, 'video_id', '1', '视频编号', 'string', 'Vod', 'zh_cn', '44', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-06-12 11:07:57', '2017-06-12 11:07:57', NULL),
(NULL, 'summary', '1', '摘要', 'string', 'Vod', 'zh_cn', '200', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2017-06-12 11:08:25', '2017-06-12 11:08:25', NULL),
(NULL, 'duration', '1', '持续时间', 'integer', 'Vod', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '单位秒', 0, '2017-06-12 11:09:33', '2017-06-12 11:09:33', NULL),
(NULL, 'size', '1', '视频大小', 'integer', 'Vod', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '单位KB', 0, '2017-06-12 11:10:28', '2017-06-12 11:10:28', NULL),
(NULL, 'is_free', '1', '免费试听课', 'integer', 'Vod', 'zh_cn', '3', NULL, '1', '1', '', '', '', NULL, '1', '0=>否\r\n1=>是', '0', '', '', '', 'select', '0', '1', '', NULL, '', '', '', 0, '2017-06-14 19:14:01', '2017-06-14 19:14:01', NULL),
(NULL, 'priority', '1', '集数', 'integer', 'Vod', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-09-02 10:53:28', '2017-09-02 10:53:28', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'vod', '视频点播', '', 'default', '', 0, '2017-06-12 11:06:29', '2017-06-12 11:06:29', 'miao_vods', '', '', '', '0', 1, NULL, '0', '0', '');
