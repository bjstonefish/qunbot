set names utf8;
CREATE TABLE IF NOT EXISTS `miao_wx_user_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uin` varchar(200) DEFAULT NULL,
  `key` varchar(300) DEFAULT NULL,
  `pass_ticket` varchar(200) NOT NULL DEFAULT '',
  `agent` varchar(500) NOT NULL DEFAULT '',
  `cookie` varchar(5000) NOT NULL DEFAULT '',
  `failed_nums` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '0', 'id', 'integer', 'WxUserSession', NULL, '11', 0, '0', '0', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-04-27 18:04:17', '2014-04-27 18:04:17', NULL),
(NULL, 'uin', '0', 'uin', 'string', 'WxUserSession', NULL, '200', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', '', '', '1', '', NULL, '', '', '', 0, '2014-04-27 18:04:17', '2014-04-27 18:04:17', ''),
(NULL, 'key', '0', 'key', 'string', 'WxUserSession', NULL, '200', 0, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', '', '', '1', '', NULL, '', '', '', 0, '2014-04-27 18:04:17', '2014-04-27 18:04:17', '');
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'WxUserSession', '微信用户Session', '', 'default', '', 27, '2014-04-27 17:52:33', '2014-04-27 17:52:33', 'miao_wx_user_sessions', '', '', '', '0', NULL, NULL, '0', '0', '');
