set names utf8;
CREATE TABLE IF NOT EXISTS `miao_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cate_id` int(11) DEFAULT '0',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `priority` int(11) DEFAULT '0',
  `enabled` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `color` varchar(30) DEFAULT '',
  `strong` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `value` (`name`),
  KEY `cate_id` (`cate_id`),
  KEY `cate_id_2` (`cate_id`),
  KEY `cate_id_3` (`cate_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Tag', 'zh_cn', '11', 6, '0', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'cate_id', '1', 'cateid', 'integer', 'Tag', 'zh_cn', '11', 5, '1', '1', 'Misccate', 'id', 'name', NULL, '1', '', '0', '', '', '', 'select', '', '1', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<options>\r\n    <conditions>\r\n        <Misccate.model>Tag</Misccate.model>\r\n    </conditions>\r\n    <order>created desc</order>\r\n</options>'),
(NULL, 'name', '1', '名称', 'string', 'Tag', 'zh_cn', '200', 4, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '0', '', '', '', 'explode', '支持一次录入多个标签，一行一个', 0, '0000-00-00 00:00:00', '2014-08-14 23:30:58', ''),
(NULL, 'priority', '1', 'priority', 'integer', 'Tag', 'zh_cn', '11', 3, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', '0', '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'enabled', '1', 'enabled', 'boolean', 'Tag', 'zh_cn', '1', 2, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', '1', '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'deleted', '0', '1', 'integer', 'Tag', NULL, '11', 0, '0', '0', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-05-12 13:08:56', '2014-05-12 13:08:56', NULL),
(NULL, 'color', '1', '显示颜色', 'string', 'Tag', 'zh_cn', '30', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'color', '', '1', '', NULL, '', '', '标签显示颜色，最长30字符', 0, '2016-05-23 03:50:22', '2016-05-23 03:59:43', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Tag', 'zh_cn', '19', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'datetime', '', '1', '', NULL, '', '', '', 0, '2016-05-23 03:54:01', '2016-05-23 03:54:01', NULL),
(NULL, 'strong', '1', '加粗显示', 'integer', 'Tag', 'zh_cn', '1', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', '', '0', '1', '', NULL, '', '', '', 0, '2016-05-23 07:36:13', '2016-05-23 07:36:13', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Tag', '标签', 'onetomany', 'default', '', 26, '2013-01-13 20:48:23', '2013-01-13 20:48:23', 'miao_tags', '', '', '', '0', 1, 0, '0', '0', '');
