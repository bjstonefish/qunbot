set names utf8;
CREATE TABLE IF NOT EXISTS `miao_ipviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(20) DEFAULT NULL,
  `data_id` int(11) DEFAULT '0',
  `created` date DEFAULT NULL,
  `view_nums` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `model` (`model`(3),`data_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Ipview', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-02-13 22:14:34', '2015-02-13 22:14:34', NULL),
(NULL, 'model', '1', '所属模块', 'string', 'Ipview', NULL, '20', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-02-13 22:14:34', '2015-02-14 09:15:16', NULL),
(NULL, 'data_id', '1', '数据编号', 'integer', 'Ipview', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '', 0, '2015-02-13 22:14:34', '2015-02-14 09:15:49', NULL),
(NULL, 'created', '1', '创建时间', 'date', 'Ipview', NULL, '10', 2, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'date', '', '1', '', NULL, '', '', '', 0, '2015-02-13 22:14:34', '2015-02-14 09:18:53', NULL),
(NULL, 'view_nums', '1', '查看次数', 'integer', 'Ipview', 'en_us', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-02-14 09:16:39', '2015-02-14 09:16:39', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Ipview', 'ip访问统计', '', 'default', '', 27, '2015-02-13 22:14:34', '2015-02-13 22:14:34', 'miao_ipviews', '', '', '', '0', NULL, NULL, '0', '0', '');
