set names utf8;
CREATE TABLE IF NOT EXISTS `miao_province_i18ns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `locale` char(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'ProvinceI18n', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-23 11:32:59', '2015-01-23 11:32:59', NULL),
(NULL, 'name', '1', '名称', 'string', 'ProvinceI18n', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-23 11:32:59', '2015-01-23 11:32:59', NULL),
(NULL, 'locale', '1', '语言版本', 'char', 'ProvinceI18n', NULL, '5', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-01-23 11:32:59', '2015-01-23 11:34:17', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'ProvinceI18n', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-23 11:32:59', '2015-01-23 11:32:59', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'ProvinceI18n', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-01-23 11:32:59', '2015-01-23 11:32:59', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'ProvinceI18n', '城市多语言', '', 'default', '', 27, '2015-01-23 12:05:08', '2015-01-23 12:05:08', 'miao_province_i18ns', '', '', '', '0', 2, NULL, '0', '0', '');
