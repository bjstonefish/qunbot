set names utf8;
CREATE TABLE IF NOT EXISTS `miao_customer_illness_records` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `diagnosis_date` date DEFAULT NULL,
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `published` tinyint(3) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `summary` varchar(500) DEFAULT '',
  `customer_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'CustomerIllnessRecord', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 16:46:00', '2017-02-09 16:46:00', NULL),
(NULL, 'diagnosis_date', '1', '诊断日期', 'date', 'CustomerIllnessRecord', NULL, '10', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'date', '', '1', '', NULL, '', '', '', 0, '2017-02-09 16:46:00', '2017-02-09 17:02:48', NULL),
(NULL, 'name', '1', '患病名称', 'string', 'CustomerIllnessRecord', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2017-02-09 16:46:00', '2017-02-09 17:01:25', NULL),
(NULL, 'cate_id', '1', '疾病编号', 'integer', 'CustomerIllnessRecord', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', '', '', '1', '', NULL, '', '', '', 0, '2017-02-09 16:46:00', '2017-02-09 17:33:59', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'CustomerIllnessRecord', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', '', '', '1', '', NULL, '', '', '', 0, '2017-02-09 16:46:00', '2017-02-09 17:53:41', NULL),
(NULL, 'published', '1', '是否发布', 'integer', 'CustomerIllnessRecord', NULL, '11', 3, '1', '1', '', NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 16:46:00', '2017-02-09 16:46:00', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'CustomerIllnessRecord', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 16:46:00', '2017-02-09 16:46:00', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'CustomerIllnessRecord', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 16:46:00', '2017-02-09 16:46:00', NULL),
(NULL, 'summary', '1', '诊断描述', 'string', 'CustomerIllnessRecord', 'zh_cn', '500', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2017-02-09 17:03:49', '2017-02-09 17:03:49', NULL),
(NULL, 'customer_id', '1', '客户编号', 'integer', 'CustomerIllnessRecord', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-02-09 17:39:42', '2017-02-09 17:39:43', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'CustomerIllnessRecord', '客户疾病记录', '', 'default', '', 0, '2017-02-09 16:46:00', '2017-02-10 10:14:55', 'miao_customer_illness_records', '', 'branch', '', '0', NULL, NULL, '0', '0', '');
