set names utf8;
CREATE TABLE IF NOT EXISTS `miao_city_areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `published` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `left` int(11) DEFAULT '0',
  `right` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'CityArea', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-05-21 17:48:21', '2014-05-21 17:48:21', NULL),
(NULL, 'name', '1', '地区名称', 'string', 'CityArea', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', '', '', '1', '', NULL, '', '', '', 0, '2014-05-21 17:48:21', '2014-05-21 17:48:21', ''),
(NULL, 'parent_id', '1', '上级分类', 'integer', 'CityArea', NULL, '11', 6, '1', '1', 'CityArea', 'id', 'name', 0, '1', '', '0', '', '', '', 'select', '', '1', '', NULL, '', '', '', 0, '2014-05-21 17:48:21', '2014-05-21 17:48:21', ''),
(NULL, 'creator', '1', '编创建者', 'integer', 'CityArea', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-05-21 17:48:21', '2014-05-21 17:48:21', NULL),
(NULL, 'published', '1', '是否发布', 'integer', 'CityArea', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '0=>否\r\n1=>是', '0', '', '', '', 'select', '1', '1', '', NULL, '', '', '', 0, '2014-05-21 17:48:21', '2014-05-21 17:48:21', ''),
(NULL, 'created', '1', '创建时间', 'datetime', 'CityArea', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-05-21 17:48:21', '2014-05-21 17:48:21', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'CityArea', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-05-21 17:48:21', '2014-05-21 17:48:21', NULL),
(NULL, 'left', '0', 'left节点', 'integer', 'CityArea', NULL, '11', 0, '0', '0', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-05-21 17:51:11', '2014-05-21 17:51:11', NULL),
(NULL, 'right', '0', 'right节点', 'integer', 'CityArea', NULL, '11', 0, '0', '0', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-05-21 17:51:11', '2014-05-21 17:51:11', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'CityArea', '城市地区', '', 'tree', '', 27, '2014-05-21 17:48:21', '2014-05-21 17:48:21', 'miao_city_areas', '', '', '', '0', 100, NULL, '0', '0', '');
