set names utf8;
CREATE TABLE IF NOT EXISTS `miao_terms` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(300) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `published` tinyint(3) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'cate_id', '1', '分类', 'integer', 'Term', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 16:44:46', '2017-02-09 16:44:46', NULL),
(NULL, 'name', '1', '名称', 'string', 'Term', NULL, '200', 5, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 16:44:46', '2017-02-09 16:44:46', NULL),
(NULL, 'published', '1', '是否发布', 'integer', 'Term', NULL, '11', 3, '1', '1', '', NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 16:44:46', '2017-02-09 16:44:46', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Term', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 16:44:46', '2017-02-09 16:44:46', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Term', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 16:44:46', '2017-02-09 16:44:46', NULL),
(NULL, 'id', '1', '编号', 'integer', 'Term', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 16:44:46', '2017-02-09 16:44:46', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'Term', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 16:44:46', '2017-02-09 16:44:46', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'Term', NULL, '300', 5, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2017-02-09 16:44:46', '2017-02-09 16:44:46', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Term', '术语解释', '', 'default', '', 0, '2017-02-09 16:44:46', '2017-02-10 10:15:31', 'miao_terms', '', 'open', '', '0', 1, NULL, '0', '0', '');
