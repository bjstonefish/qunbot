set names utf8;
CREATE TABLE IF NOT EXISTS `miao_task_executes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '0',
  `deleted` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT '0',
  `achieve_num` varchar(20) DEFAULT NULL,
  `task_id` int(11) DEFAULT '0',
  `customer_id` int(11) DEFAULT NULL,
  `content` text,
  `name` varchar(200) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'TaskExecute', 'zh_cn', '11', 12, '0', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'status', '1', '发布状态', 'integer', 'TaskExecute', 'zh_cn', '11', 4, '1', '1', 'Misccate', 'id', 'name', 25, '1', NULL, '0', NULL, NULL, 'treenode', 'select', '0', '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'deleted', '1', '是否删除', 'integer', 'TaskExecute', 'zh_cn', '11', 3, '0', '1', NULL, NULL, NULL, NULL, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'TaskExecute', 'zh_cn', NULL, 2, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'TaskExecute', 'zh_cn', NULL, 1, '0', '0', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'user_id', '1', '执行人编号', 'integer', 'TaskExecute', 'zh_cn', '', 11, '1', '1', '', 'id', 'name', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', '', 'var txt = $(\"#TaskexecuteExecuterId\").find(\"option:selected\").text(); \r\ntxt = txt.replace(/_/g,\'\');\r\nif($(\"#TaskexecuteExecuterId\").val()!=\"\")\r\n{\r\n    $(\"#TaskexecuteExecuterName\").val(txt);\r\n}\r\nelse\r\n{\r\n    $(\"#TaskexecuteExecuterName\").val(\"\");\r\n}', 'none', '', 0, '0000-00-00 00:00:00', '2014-10-05 16:48:26', ''),
(NULL, 'achieve_num', '1', '完成数', 'string', 'TaskExecute', 'zh_cn', '20', 8, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'input', '', '1', '', '', '', 'none', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'task_id', '1', '具体参与任务', 'integer', 'TaskExecute', 'zh_cn', '', 9, '1', '1', '', 'task_id', 'task_id', NULL, '1', '', '1', '', '', '', 'select', '', '1', '', '', '', 'none', '', 0, '0000-00-00 00:00:00', '2014-11-05 20:09:31', NULL),
(NULL, 'customer_id', '1', '相关客户id', 'integer', 'TaskExecute', 'zh_cn', NULL, 7, '1', '1', 'Customer', 'id', 'name', NULL, '1', '', '0', '', '', 'equal', 'select', '', '1', '', '', 'var txt = $(\"#TaskexecuteCustomerId\").find(\"option:selected\").text(); \ntxt = txt.replace(/_/g,\'\');\nif($(\"#TaskexecuteCustomerId\").val()!=\"\")\n{\n    $(\"#TaskexecuteCustomerName\").val(txt);\n}\nelse\n{\n    $(\"#TaskexecuteCustomerName\").val(\"\");\n}', 'none', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'start_date', '1', '开始日期', 'date', 'TaskExecute', NULL, '10', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-10-06 09:42:43', '2014-10-06 09:42:43', NULL),
(NULL, 'content', '1', '内容', 'content', 'TaskExecute', 'zh_cn', NULL, 5, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'textarea', '', '1', '', '', '', 'none', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'end_date', '1', '结束日期', 'date', 'TaskExecute', NULL, '10', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-10-06 09:42:43', '2014-10-06 09:42:43', NULL),
(NULL, 'name', '1', '名称', 'string', 'TaskExecute', NULL, '200', 0, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-10-06 09:42:43', '2014-10-06 09:42:43', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'TaskExecute', '子任务(任务执行记录)', 'onetomany', 'default', '<id>', 27, '2010-08-08 19:22:40', '2014-10-04 20:51:43', 'miao_task_executes', '', '', '', '0', NULL, 0, '0', '0', '');
