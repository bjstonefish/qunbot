set names utf8;
CREATE TABLE IF NOT EXISTS `miao_aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(10) DEFAULT '0',
  `rght` int(10) DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Aro', 'zh_cn', '10', 7, '0', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'parent_id', '1', 'parent_id', 'integer', 'Aro', 'zh_cn', '10', 6, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'model', '1', 'model', 'string', 'Aro', 'zh_cn', '255', 5, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', '', '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'foreign_key', '1', 'foreign_key', 'integer', 'Aro', 'zh_cn', '10', 4, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'alias', '1', 'alias', 'string', 'Aro', 'zh_cn', '255', 3, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', '', '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'lft', '1', '树左节点', 'integer', 'Aro', 'zh_cn', '10', 2, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'rght', '1', '树右节点', 'integer', 'Aro', 'zh_cn', '10', 1, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, '', NULL, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(NULL, 'name', '1', '角色名称', 'string', 'Aro', 'zh_cn', '100', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2013-05-18 09:24:54', '2013-05-18 09:24:54', '');
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Aro', '权限角色', 'onetomany', 'default', '', 27, '2010-06-30 23:06:27', '2010-06-30 23:06:27', 'miao_aros', '', '', '', '0', NULL, 0, '0', '0', '');



REPLACE INTO `miao_aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`, `name`) VALUES (1, NULL, 'Role', 1, '', 3, 6, NULL),
(2, NULL, 'Role', 2, '', 7, 12, NULL),
(4, NULL, 'Role', 4, '', 1, 2, NULL),
(65, 1, 'Staff', 1, '', 4, 5, NULL),
(72, 2, 'Staff', 73, '', 10, 11, NULL),
(73, 2, 'Staff', 74, '', 8, 9, NULL),
(116, NULL, 'Role', 101, '', 13, 14, NULL),
(117, NULL, 'Role', 8, '', 15, 16, NULL),
(118, NULL, 'Role', 3, '', 17, 18, NULL),
(119, NULL, 'Role', 9, '', 19, 20, NULL),
(120, NULL, 'Role', 102, '', 21, 22, NULL);
