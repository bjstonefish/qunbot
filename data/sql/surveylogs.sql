set names utf8;
CREATE TABLE IF NOT EXISTS `miao_surveylogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `exam` varchar(32) DEFAULT '',
  `creator` int(13) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `model` varchar(60) DEFAULT NULL,
  `data_id` int(11) DEFAULT NULL,
  `q_id` int(11) DEFAULT NULL,
  `q_optid` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `creator` (`model`,`data_id`,`creator`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Surveylog', 'zh_cn', '11', 15, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2010-10-05 17:21:15', '2010-10-05 17:21:15', NULL),
(NULL, 'name', '1', '名称', 'string', 'Surveylog', 'zh_cn', '200', 7, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2010-10-05 17:21:15', '2010-10-05 17:21:15', NULL),
(NULL, 'exam', '1', '所属考试', 'string', 'Surveylog', 'zh_cn', '32', 10, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '一套题可以多次回答，每次回答时算做一次考试。不同次的答题区分互相不受影响', 0, '2010-10-05 17:21:15', '2016-12-24 07:37:20', NULL),
(NULL, 'creator', '1', '编创建者', 'integer', 'Surveylog', 'zh_cn', '11', 9, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2010-10-05 17:21:15', '2010-10-05 17:21:15', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Surveylog', 'zh_cn', NULL, 2, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2010-10-05 17:21:15', '2010-10-05 17:21:15', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Surveylog', 'zh_cn', NULL, 1, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2010-10-05 17:21:15', '2010-10-05 17:21:15', NULL),
(NULL, 'model', '1', '模块', 'string', 'Surveylog', 'zh_cn', '60', 14, '1', '1', '', NULL, NULL, NULL, '1', '', '0', '', '', '', 'input', 'Article', '1', '', NULL, '', '', '', 0, '2010-10-05 17:23:29', '2010-10-05 17:23:29', NULL),
(NULL, 'data_id', '1', '数据编号', 'integer', 'Surveylog', 'zh_cn', '11', 13, '1', '1', '', NULL, NULL, NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2010-10-05 17:23:59', '2010-10-05 17:23:59', NULL),
(NULL, 'q_id', '1', '投票问题', 'integer', 'Surveylog', 'zh_cn', '11', 12, '1', '1', '', NULL, NULL, NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2010-10-05 17:25:05', '2010-10-05 17:25:05', NULL),
(NULL, 'q_optid', '1', '投票选项', 'integer', 'Surveylog', 'zh_cn', '11', 11, '1', '1', '', NULL, NULL, NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2010-10-05 17:25:41', '2010-10-05 17:25:41', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Surveylog', '调查记录', '', 'default', '', 27, '2010-10-05 17:21:14', '2016-08-12 21:35:31', 'miao_surveylogs', '', '', '', '0', NULL, 0, '0', '0', '');
