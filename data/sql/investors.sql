set names utf8;
CREATE TABLE IF NOT EXISTS `miao_investors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(200) DEFAULT NULL,
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `view_nums` int(11) DEFAULT '0',
  `website` varchar(255) DEFAULT NULL,
  `summary` varchar(1000) DEFAULT NULL,
  `content` text,
  `scope` varchar(100) DEFAULT NULL,
  `contact` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Investor', NULL, '11', 14, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-13 21:47:59', '2014-08-13 21:47:59', NULL),
(NULL, 'name', '1', '名称', 'string', 'Investor', NULL, '200', 11, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-13 21:47:59', '2014-08-13 21:47:59', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'Investor', NULL, '200', 10, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'file', '', '1', '', NULL, '', '', '', 0, '2014-08-13 21:47:59', '2014-08-13 21:57:55', ''),
(NULL, 'cate_id', '1', '分类', 'integer', 'Investor', NULL, '11', 13, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-13 21:47:59', '2014-08-13 21:47:59', NULL),
(NULL, 'creator', '1', '编创建者', 'integer', 'Investor', NULL, '11', 12, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-13 21:47:59', '2014-08-13 21:47:59', NULL),
(NULL, 'status', '1', '状态', 'integer', 'Investor', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-13 21:47:59', '2014-08-13 21:47:59', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Investor', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-13 21:47:59', '2014-08-13 21:47:59', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Investor', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-13 21:47:59', '2014-08-13 21:47:59', NULL),
(NULL, 'view_nums', '1', '查看次数', 'integer', 'Investor', 'zh_cn', '11', 4, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-08-13 21:51:07', '2014-08-13 21:51:07', ''),
(NULL, 'website', '1', '网址', 'string', 'Investor', NULL, '255', 9, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'input', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-13 21:54:13', '2014-08-13 21:54:13', NULL),
(NULL, 'summary', '1', '摘要', 'string', 'Investor', NULL, '1000', 6, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'textarea', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-13 21:54:13', '2014-08-13 21:54:13', NULL),
(NULL, 'content', '1', '内容', 'content', 'Investor', NULL, '65535', 5, '1', '1', NULL, NULL, NULL, 0, '0', NULL, '0', NULL, NULL, NULL, 'ckeditor', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-08-13 21:54:13', '2014-08-13 21:54:13', NULL),
(NULL, 'scope', '1', '投资领域', 'string', 'Investor', 'zh_cn', '100', 7, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '投资机构的主投行业，但一般不限于此，还是要看项目说话', 0, '2014-08-14 07:08:28', '2014-08-14 07:08:28', ''),
(NULL, 'contact', '1', '联系方式', 'string', 'Investor', 'zh_cn', '200', 8, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2014-08-14 07:10:04', '2014-08-14 07:10:04', '');
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Investor', '投资机构', '', 'default', '', 27, '2014-08-13 21:47:59', '2014-08-13 21:47:59', 'miao_investors', '', '', '', '0', NULL, 0, '0', '0', '');
