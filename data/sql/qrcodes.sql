set names utf8;
CREATE TABLE IF NOT EXISTS `miao_qrcodes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(300) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `event` varchar(128) DEFAULT '',
  `url` varchar(255) DEFAULT '',
  `click_nums` int(11) DEFAULT '0',
  `wx_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Qrcode', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-07-13 22:13:51', '2016-07-13 22:13:51', NULL),
(NULL, 'name', '1', '二维码用途描述', 'string', 'Qrcode', NULL, '200', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2016-07-13 22:13:51', '2016-07-13 22:37:17', NULL),
(NULL, 'coverimg', '1', '二维码图片', 'string', 'Qrcode', NULL, '300', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', 'equal', 'file', '', '1', '', NULL, '', '', '', 0, '2016-07-13 22:13:51', '2016-07-13 22:35:49', NULL),
(NULL, 'cate_id', '1', '分类', 'integer', 'Qrcode', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-07-13 22:13:51', '2016-07-13 22:13:51', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'Qrcode', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-07-13 22:13:51', '2016-07-13 22:13:51', NULL),
(NULL, 'status', '1', '状态', 'integer', 'Qrcode', NULL, '11', 3, '1', '1', '', NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-07-13 22:13:51', '2016-07-13 22:13:51', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Qrcode', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-07-13 22:13:51', '2016-07-13 22:13:51', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Qrcode', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-07-13 22:13:51', '2016-07-13 22:13:51', NULL),
(NULL, 'event', '1', '事件代码', 'string', 'Qrcode', 'zh_cn', '128', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-07-13 22:32:53', '2016-07-13 22:32:53', NULL),
(NULL, 'url', '1', '网址', 'string', 'Qrcode', 'zh_cn', '255', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-07-13 22:33:26', '2016-07-13 22:33:26', NULL),
(NULL, 'click_nums', '1', '点击扫码次数', 'integer', 'Qrcode', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-07-13 22:34:11', '2016-07-13 22:34:11', NULL),
(NULL, 'wx_id', '1', '微信编号', 'integer', 'Qrcode', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2016-07-13 22:35:13', '2016-07-13 22:35:13', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Qrcode', '二维码', '', 'default', '', 27, '2016-07-13 22:13:51', '2016-07-13 22:13:51', 'miao_qrcodes', '', '', '', '0', 2, NULL, '0', '0', '');
