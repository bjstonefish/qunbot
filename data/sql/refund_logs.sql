set names utf8;
CREATE TABLE IF NOT EXISTS `miao_refund_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_method` varchar(20) DEFAULT NULL,
  `order_id` int(11) DEFAULT '0',
  `staff_id` int(11) DEFAULT '0',
  `fee_type` char(3) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `transaction_id` varchar(44) DEFAULT '',
  `refund_fee` float(10,2) DEFAULT '0.00',
  `refund_no` varchar(44) DEFAULT '',
  `user_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'RefundLog', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-03-24 19:01:50', '2015-03-24 19:01:50', NULL),
(NULL, 'pay_method', '1', '名称', 'string', 'RefundLog', NULL, '20', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', '', '', '1', '', NULL, '', '', '', 0, '2015-03-24 19:01:50', '2015-03-24 19:03:11', NULL),
(NULL, 'order_id', '1', '分类', 'integer', 'RefundLog', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', '', '', '1', '', NULL, '', '', '', 0, '2015-03-24 19:01:50', '2015-03-24 19:02:39', NULL),
(NULL, 'staff_id', '1', '退款人', 'integer', 'RefundLog', NULL, '11', 6, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-03-24 19:01:50', '2017-10-21 07:22:54', NULL),
(NULL, 'fee_type', '1', '币种', 'char', 'RefundLog', NULL, '3', 3, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '0', '1', '', NULL, '', '', '', 0, '2015-03-24 19:01:50', '2015-03-24 19:04:24', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'RefundLog', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-03-24 19:01:50', '2015-03-24 19:01:50', NULL),
(NULL, 'transaction_id', '1', '支付交易号', 'string', 'RefundLog', NULL, '44', 1, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-03-24 19:01:50', '2017-10-21 07:27:59', NULL),
(NULL, 'refund_fee', '1', '退款金额', 'float', 'RefundLog', 'en_us', '10,2', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2015-03-24 19:07:42', '2017-10-21 07:21:46', NULL),
(NULL, 'user_id', '1', '用户编号', 'integer', 'RefundLog', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-10-21 10:27:21', '2017-10-21 10:27:21', NULL),
(NULL, 'refund_no', '1', '退款号', 'string', 'RefundLog', 'zh_cn', '44', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-10-21 07:23:50', '2017-10-21 07:23:50', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'RefundLog', '退款记录', '', 'default', '', 0, '2017-10-21 07:13:40', '2017-10-21 07:13:40', 'miao_refund_logs', '', '', '', '0', NULL, 0, '0', '0', '');
