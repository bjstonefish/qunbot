set names utf8;
CREATE TABLE IF NOT EXISTS `miao_brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `creator` bigint(11) DEFAULT '0',
  `contact` varchar(200) DEFAULT '',
  `summary` varchar(300) DEFAULT '',
  `status` int(11) DEFAULT '0',
  `published` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `mch_id` varchar(32) DEFAULT '',
  `mobile` varchar(18) DEFAULT '',
  `remark` varchar(500) DEFAULT '',
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Brand', 'zh_cn', '11', 11, '1', '1', '', NULL, NULL, NULL, '1', '', '0', '', '', 'equal', '', '', '1', '', NULL, '', '', '', 0, '2011-03-25 15:15:46', '2011-03-25 15:15:46', NULL),
(NULL, 'name', '1', '供应商名称', 'string', 'Brand', 'zh_cn', '200', 10, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'input', '', '1', '', NULL, '', '', '', 0, '2011-03-25 15:15:46', '2017-09-22 09:28:19', NULL),
(NULL, 'creator', '1', 'creator', 'integer', 'Brand', 'zh_cn', '11', 8, '1', '1', '', NULL, NULL, NULL, '1', '', '0', '', '', 'equal', '', '0', '1', '', NULL, '', '', '', 0, '2011-03-25 15:15:46', '2011-03-25 15:15:46', ''),
(NULL, 'contact', '1', '联系人', 'string', 'Brand', 'zh_cn', '200', 7, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'input', '0', '1', '', NULL, '', '', '', 0, '2011-03-25 15:15:46', '2017-09-22 07:46:03', NULL),
(NULL, 'summary', '1', '摘要描述', 'string', 'Brand', 'zh_cn', '300', 6, '1', '1', '', '', '', NULL, '1', '', '0', '', '', 'equal', 'textarea', '', '1', '', NULL, '', '', '', 0, '2011-03-25 15:15:46', '2017-09-22 09:32:53', NULL),
(NULL, 'status', '1', '状态', 'integer', 'Brand', 'zh_cn', '11', 5, '1', '1', '', '', '', NULL, '1', '0=>下线\r\n1=>上线', '0', '', '', 'equal', 'select', '0', '1', '', NULL, '', '', '', 0, '2011-03-25 15:15:46', '2017-09-22 07:47:05', NULL),
(NULL, 'published', '1', '是否发布', 'boolean', 'Brand', 'zh_cn', '1', 4, '1', '1', NULL, NULL, NULL, NULL, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 1, '2011-03-25 15:15:46', '2011-03-25 15:15:46', NULL),
(NULL, 'deleted', '1', '是否删除', 'boolean', 'Brand', 'zh_cn', '1', 3, '1', '1', NULL, NULL, NULL, NULL, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2011-03-25 15:15:46', '2011-03-25 15:15:46', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Brand', 'zh_cn', NULL, 2, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2011-03-25 15:15:47', '2011-03-25 15:15:47', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Brand', 'zh_cn', NULL, 1, '1', '1', NULL, NULL, NULL, NULL, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2011-03-25 15:15:47', '2011-03-25 15:15:47', NULL),
(NULL, 'mch_id', '1', '微信支付子商户编号', 'string', 'Brand', 'zh_cn', '32', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-09-22 09:28:00', '2017-09-22 09:28:00', NULL),
(NULL, 'mobile', '1', '联系电话', 'string', 'Brand', 'zh_cn', '18', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-09-22 09:29:29', '2017-09-22 09:29:29', NULL),
(NULL, 'remark', '1', '备注', 'string', 'Brand', 'zh_cn', '500', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2017-09-22 09:30:07', '2017-09-22 09:30:07', NULL),
(NULL, 'content', '1', '详细描述', 'content', 'Brand', 'zh_cn', '65535', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'ckeditor', '', '1', '', NULL, '', '', '', 0, '2017-09-22 09:30:41', '2017-09-22 09:30:41', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Brand', '产品品牌', '', 'default', '', 27, '2011-03-25 15:07:49', '2011-03-25 15:07:49', 'miao_brands', '', '', '', '0', 0, 0, '0', '0', '');
