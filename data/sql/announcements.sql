set names utf8;
CREATE TABLE IF NOT EXISTS `miao_announcements` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(300) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `published` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `content` text,
  `linkurl` varchar(300) DEFAULT '',
  `view_nums` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'Announcement', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-25 18:37:17', '2016-11-25 18:37:17', NULL),
(NULL, 'name', '1', '名称', 'string', 'Announcement', NULL, '200', 5, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-25 18:37:17', '2016-11-25 18:37:17', NULL),
(NULL, 'coverimg', '1', '封面图片', 'string', 'Announcement', NULL, '300', 5, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-25 18:37:17', '2016-11-25 18:37:17', NULL),
(NULL, 'cate_id', '1', '分类', 'integer', 'Announcement', NULL, '11', 6, '1', '1', 'Modelcate', 'id', 'name', NULL, '1', '', '0', '', '', 'treenode', 'select', '', '1', '', NULL, '', '', '', 0, '2016-11-25 18:37:17', '2017-06-20 10:35:09', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<options><conditions><Modelcate.model>Announcement</Modelcate.model></conditions></options>\n'),
(NULL, 'creator', '1', '创建者', 'integer', 'Announcement', NULL, '11', 6, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-25 18:37:17', '2016-11-25 18:37:17', NULL),
(NULL, 'published', '1', '是否发布', 'integer', 'Announcement', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '0=>否\r\n1=>是', '0', '', '', 'equal', 'select', '1', '1', '', NULL, '', '', '', 0, '2016-11-25 18:37:17', '2016-11-25 18:45:56', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Announcement', NULL, NULL, 2, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-25 18:37:17', '2016-11-25 18:37:17', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Announcement', NULL, NULL, 1, '1', '1', '', NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-11-25 18:37:17', '2016-11-25 18:37:17', NULL),
(NULL, 'content', '1', '内容', 'content', 'Announcement', 'zh_cn', '65535', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'ckeditor', '', '1', '', NULL, '', '', '', 0, '2016-11-25 18:47:15', '2016-11-25 18:47:15', NULL),
(NULL, 'linkurl', '1', '链接地址', 'string', 'Announcement', 'zh_cn', '300', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-06-23 11:31:47', '2017-06-23 11:31:47', NULL),
(NULL, 'view_nums', '1', '阅读数', 'integer', 'Announcement', 'zh_cn', '11', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'input', '', '1', '', NULL, '', '', '', 0, '2017-06-23 22:23:57', '2017-06-23 22:23:57', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Announcement', '公告', '', 'default', '', 0, '2016-11-25 18:37:17', '2016-11-25 18:37:17', 'miao_announcements', '', '', '', '0', 1, NULL, '0', '0', '');
