set names utf8;
CREATE TABLE IF NOT EXISTS `miao_customer_opportunities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `content` text,
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `customer_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'customer_id', '1', '所属客户', 'integer', 'CustomerOpportunity', 'en_us', '11', NULL, '1', '1', 'TeamCustomer', 'id', 'name', NULL, '1', '', '0', '', '', '', 'select', '', '1', '', NULL, '', '', '', 0, '2015-02-25 22:29:49', '2015-02-25 22:29:49', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<options><conditions><TeamCustomer.team_id>{team_id}</TeamCustomer.team_id></conditions></options>\n'),
(NULL, 'id', '1', '编号', 'integer', 'CustomerOpportunity', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-02-25 09:06:21', '2015-02-25 09:06:21', NULL),
(NULL, 'name', '1', '名称', 'string', 'CustomerOpportunity', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-02-25 09:06:21', '2015-02-25 09:06:21', NULL),
(NULL, 'content', '1', '内容', 'content', 'CustomerOpportunity', NULL, '65535', 5, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', 'ckeditor', '', '1', '', NULL, '', '', '', 0, '2015-02-25 09:06:21', '2015-02-25 22:28:07', NULL),
(NULL, 'cate_id', '1', '分类', 'integer', 'CustomerOpportunity', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-02-25 09:06:21', '2015-02-25 09:06:21', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'CustomerOpportunity', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-02-25 09:06:21', '2015-02-25 09:06:21', NULL),
(NULL, 'status', '1', '状态', 'integer', 'CustomerOpportunity', NULL, '11', 3, '1', '1', '', '', '', 0, '1', '-1=>Closed\r\n0=>Active\r\n1=>Success', '0', '', '', '', 'select', '0', '1', '', NULL, '', '', '', 0, '2015-02-25 09:06:21', '2015-03-17 12:30:15', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'CustomerOpportunity', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-02-25 09:06:21', '2015-02-25 09:06:21', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'CustomerOpportunity', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2015-02-25 09:06:21', '2015-02-25 09:06:21', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'CustomerOpportunity', '客户机会', '', 'default', '', 27, '2015-02-25 09:06:21', '2016-05-04 06:39:08', 'miao_customer_opportunities', '', '', '', '0', NULL, 0, '0', '0', '');
