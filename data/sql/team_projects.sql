set names utf8;
CREATE TABLE IF NOT EXISTS `miao_team_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `content` text,
  `team_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'id', '1', '编号', 'integer', 'TeamProject', NULL, '11', 15, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-09-24 17:24:21', '2014-09-24 17:24:21', NULL),
(NULL, 'name', '1', '名称', 'string', 'TeamProject', NULL, '200', 12, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-09-24 17:24:21', '2014-09-24 17:24:21', NULL),
(NULL, 'cate_id', '1', '分类', 'integer', 'TeamProject', NULL, '11', 14, '1', '1', '', '', '', 0, '1', '', '0', '', '', '', '', '', '1', '', NULL, '', '', '', 0, '2014-09-24 17:24:21', '2014-11-05 12:57:49', NULL),
(NULL, 'creator', '1', '编创建者', 'integer', 'TeamProject', NULL, '11', 13, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-09-24 17:24:21', '2014-09-24 17:24:21', NULL),
(NULL, 'status', '1', '状态', 'integer', 'TeamProject', NULL, '11', 4, '1', '1', '', '', '', 0, '1', '0=>计划项目需求\r\n1=>项目进行中\r\n2=>项目已完成\r\n3=>项目已取消', '0', '', '', '', 'select', '0', '1', '', NULL, '', '', '', 0, '2014-09-24 17:24:21', '2014-11-22 16:00:38', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'TeamProject', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-09-24 17:24:21', '2014-09-24 17:24:21', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'TeamProject', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2014-09-24 17:24:21', '2014-09-24 17:24:21', NULL),
(NULL, 'start_date', '1', '开始日期', 'date', 'TeamProject', 'zh_cn', '10', 10, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'date', '', '1', '', NULL, '', 'none', '', 0, '2014-09-24 17:27:52', '2014-09-24 17:27:52', ''),
(NULL, 'end_date', '1', '结束日期', 'date', 'TeamProject', 'zh_cn', '10', 9, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'date', '', '1', '', NULL, '', 'none', '', 0, '2014-09-24 17:28:20', '2014-09-24 17:28:20', ''),
(NULL, 'content', '1', '项目描述', 'content', 'TeamProject', 'zh_cn', '65535', 3, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', 'none', '', 0, '2014-09-24 17:47:28', '2014-11-05 12:28:52', ''),
(NULL, 'team_id', '1', '所属团队', 'integer', 'TeamProject', 'zh_cn', '11', 7, '1', '1', 'Team', 'id', 'name', NULL, '1', '', '0', '', '', '', 'select', '', '1', '', NULL, '', 'none', '', 0, '2014-09-24 18:09:17', '2014-12-23 22:57:57', ''),
(NULL, 'project_files', '0', '项目文档', 'string', 'TeamProject', 'zh_cn', '1000', 5, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'file', '', '1', '', NULL, '', '', '', 0, '2014-09-25 09:48:05', '2014-09-25 09:48:05', '');
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`) VALUES (NULL, 'TeamProject', '团队项目', '', 'default', '', 27, '2014-12-04 09:42:51', '2014-12-04 09:42:51', 'miao_team_projects', '', 'self', '', '0', 10, 0);
