set names utf8;
CREATE TABLE IF NOT EXISTS `miao_dailies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT '',
  `coverimg` varchar(300) DEFAULT '',
  `cate_id` int(11) DEFAULT '0',
  `creator` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `dailydate` date DEFAULT NULL,
  `todayplan` varchar(2000) DEFAULT '',
  `content` varchar(2000) DEFAULT '',
  `review` varchar(2000) DEFAULT '',
  `suggestion` varchar(2000) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `creator` (`creator`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
REPLACE INTO `miao_i18nfields` (`id`, `name`, `savetodb`, `translate`, `type`, `model`, `locale`, `length`, `sort`, `allowadd`, `allowedit`, `selectmodel`, `selectvaluefield`, `selecttxtfield`, `selectparentid`, `selectautoload`, `selectvalues`, `associateflag`, `associateelement`, `associatefield`, `associatetype`, `formtype`, `default`, `allownull`, `validationregular`, `description`, `onchange`, `explodeimplode`, `explain`, `deleted`, `created`, `updated`, `conditions`) VALUES (NULL, 'coverimg', '1', '封面图片', 'string', 'Daily', NULL, '300', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-05-04 06:03:19', '2016-05-04 06:03:19', NULL),
(NULL, 'creator', '1', '创建者', 'integer', 'Daily', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-05-04 06:03:19', '2016-05-04 06:03:19', NULL),
(NULL, 'id', '1', '编号', 'integer', 'Daily', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-05-04 06:03:19', '2016-05-04 06:03:19', NULL),
(NULL, 'name', '1', '名称', 'string', 'Daily', NULL, '200', 5, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-05-04 06:03:19', '2016-05-04 06:03:19', NULL),
(NULL, 'status', '1', '状态', 'integer', 'Daily', NULL, '11', 3, '1', '1', NULL, NULL, NULL, 0, '1', '0=>否\n1=>是', '0', NULL, NULL, 'equal', 'select', '0', '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-05-04 06:03:19', '2016-05-04 06:03:19', NULL),
(NULL, 'cate_id', '1', '分类', 'integer', 'Daily', NULL, '11', 6, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-05-04 06:03:19', '2016-05-04 06:03:19', NULL),
(NULL, 'created', '1', '创建时间', 'datetime', 'Daily', NULL, NULL, 2, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-05-04 06:03:19', '2016-05-04 06:03:19', NULL),
(NULL, 'updated', '1', '修改时间', 'datetime', 'Daily', NULL, NULL, 1, '1', '1', NULL, NULL, NULL, 0, '1', NULL, '0', NULL, NULL, 'equal', 'datetime', NULL, '1', NULL, NULL, NULL, NULL, NULL, 0, '2016-05-04 06:03:19', '2016-05-04 06:03:19', NULL),
(NULL, 'dailydate', '1', '日报日期', 'date', 'Daily', 'en_us', '10', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'date', '', '1', '', NULL, '', '', '', 0, '2016-05-04 06:05:19', '2016-05-04 06:05:19', NULL),
(NULL, 'todayplan', '1', '今日计划', 'string', 'Daily', 'en_us', '2000', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2016-05-04 06:06:02', '2016-05-04 06:06:02', NULL),
(NULL, 'content', '1', '实际工作', 'string', 'Daily', 'en_us', '2000', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2016-05-04 06:06:19', '2016-05-04 06:06:19', NULL),
(NULL, 'review', '1', '自我评价', 'string', 'Daily', 'en_us', '2000', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2016-05-04 06:06:35', '2016-05-04 06:06:35', NULL),
(NULL, 'suggestion', '1', '意见建议', 'string', 'Daily', 'en_us', '2000', NULL, '1', '1', '', '', '', NULL, '1', '', '0', '', '', '', 'textarea', '', '1', '', NULL, '', '', '', 0, '2016-05-04 06:07:03', '2016-05-04 06:07:03', NULL);
REPLACE INTO `miao_modelextends` (`id`, `name`, `cname`, `belongtype`, `modeltype`, `idtype`, `status`, `created`, `updated`, `tablename`, `related_model`, `security`, `operatorfields`, `deleted`, `cate_id`, `localetype`, `bind_tags`, `bind_uploads`, `summary`) VALUES (NULL, 'Daily', '日报', '', 'default', '', 27, '2016-05-04 06:03:19', '2016-05-04 06:03:19', 'miao_dailies', '', '', '', '0', NULL, NULL, '0', '0', '');
