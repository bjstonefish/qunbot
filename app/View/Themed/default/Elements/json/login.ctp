<?php

if(empty($_REQUEST['state']) && $_SERVER['HTTP_HOST']=='www.135plat.com') $_REQUEST['state']='135plat';

$login_txt = __('Login');
$head_txt = __('Sign In To Your Account');
$reg_txt = __('Register');
$pwd_txt = __('Password');
$remember_txt = __('Remember me');
$forget_txt = __('Forget Your Password?');
$noreg_txt = __('Not registered?');

$html = <<<EOT
<div class="ui-portlet clearfix ">
    <div class="reg-header">            
        <h2>{$head_txt}</h2>
    </div>

<div  id="login-form">
<style>
.reg-page { background: none repeat scroll 0 0 #fefefe;border: 1px solid #eee;  box-shadow: 0 0 3px #eee;   color: #555; padding: 20px 30px;}
.reg-page .input-group-addon {background: none repeat scroll 0 0 rgba(0, 0, 0, 0);   color: #bbb;min-width: 120px;text-align: left;}
.reg-header {border-bottom: 1px solid #eee;color: #555;margin-bottom: 20px;text-align: center;}
</style>
	<h4>第三方账号登录</h4>
      <p>
      <a onclick="window.open('http://www.135editor.com/oauth/qq/login?state={$_REQUEST['state']}','qqlogin','height=500,width=800,top=120,left='+(window.screen.availWidth/2-400)+',help=no,resizable=no,status=no,scroll=no');" href="javascript:;" ><img style="height: 45px;" src="/img/oauth/icon-qq.png" title="使用QQ账号登陆" class="showtooltip" ></a>
      &nbsp;
      <a onclick="window.open('http://www.135editor.com/oauth/weixin/qrlogin?state={$_REQUEST['state']}','weixin_qrlogin','height=500,width=800,top=120,left='+(window.screen.availWidth/2-400)+',help=no,resizable=no,status=no,scroll=no');" href="javascript:;" ><img style="height: 45px;" src="/img/oauth/icon-wechat.png" title="使用微信账号登陆" class="showtooltip" ></a> &nbsp;
      <a onclick="window.open('http://www.135editor.com/oauth/sina/login?state={$_REQUEST['state']}','sinalogin','height=500,width=800,top=120,left='+(window.screen.availWidth/2-400)+',help=no,resizable=no,status=no,scroll=no');" href="javascript:;" ><img style="height: 45px;" src="/img/oauth/icon-weibo.png" title="使用新浪微博账号登陆" class="showtooltip" ></a> &nbsp;
     <a onclick="window.open('http://www.135editor.com/oauth/toutiao/login?state={$_REQUEST['state']}','sinalogin','height=500,width=800,top=120,left='+(window.screen.availWidth/2-400)+',help=no,resizable=no,status=no,scroll=no');" href="javascript:;" ><img style="height: 45px;" src="/img/oauth/icon-toutiao.png" title="使用今日头条账号登陆" class="showtooltip" ></a>
      </p>
      <hr style="margin: 15px 0;"/>
      <h4>网站用户登录</h4>
<form class="reg-page"  action="/users/login?{$q_args}" id="loginform" accept-charset="utf-8"  method="post" role="form" onsubmit="return ajaxSubmitForm(this,'loginSuccess');">
                <div class="form-group help-block" id="login_errorinfo"></div>    

EOT;
if ($_REQUEST['type']=='mocapcha') {

$html .= <<<EOT
		
<script>
function change_captcha(){
	$('#register-captcha').attr('src','/users/captcha?_='+randomString(8));
}
function send_msg(dom){
	var mobile = $('#UserMobile').val();
	var captcha = $('#sms-captcha').val();
	if(mobile == '') {
		showErrorMessage("请先输入手机号");$('#register-mobile').focus();return false;
	}
	else if(captcha == '') {
		showErrorMessage("请输入图片验证码后再点击发送短信验证码");$('#sms-captcha').focus();return false;
	}
	ajaxAction('/users/send_msg/0', {mobile:mobile,captcha:captcha,skip_check:1},null,function(request){
		if(request.success){
			var time = 90;
			var send_msg_ti = setInterval(function(){				
				if(time > 0) {
					$('#btn-send-msg').attr('disabled','distabled');
					$('#btn-send-msg').val('再次发送('+time+'秒后)');
				}
				else{
					$('#btn-send-msg').removeAttr('disabled');
					$('#btn-send-msg').val('发送短信验证码');
					clearInterval(send_msg_ti);
				}
				time--;
			},1000);
		}
		else{
			change_captcha();
		}
	});
}
</script>
				<div class="form-group">
			 		<div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-user"></i>
                        	 手机号
                        </span>

                        <input type="hidden" name="data[User][referer]" value="{$this->data['User']['referer']}">
                        <input type="text" required="required" name="data[User][mobile]" id="UserMobile" class="form-control" placeholder="请输入您的手机号">
                    </div>
                </div>
                <div class="form-group">
                	<div style="margin-top:5px;">
                          	<input style="display:inline-block;width:80px;" type="text" placeholder="验证码" class="form-control" minlength="4" maxlength="4" id="sms-captcha">
                        	<a href="javascript:void(0)" title="看不清，点击换一张" onclick="change_captcha();"><img style="height:38px;" id="register-captcha" src="/users/captcha"/></a>
                        	<input type="button" class="btn btn-default" id="btn-send-msg" value="发送短信验证码" onclick="send_msg(this);">
                    </div>
                </div>          
                <div class="form-group">                  
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-lock"></i>
                            短信验证码
                        </span>
                        <input type="text"  required="required" maxlength="15"  name="data[User][capcha]" class="form-control" placeholder="输入手机接收的短信验证码">
                    </div>
                </div>
EOT;

}
else{

$html .= <<<EOT
			<div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-user"></i>
                         账户
                        </span>

                        <input type="hidden" name="data[User][referer]" value="{$this->data['User']['referer']}">

                        <input type="text" required="required" name="data[User][email]" id="UserEmail" class="form-control" placeholder="请输入您的注册邮箱或手机号">
                    </div>
             </div>
                <div class="form-group">                  
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-lock"></i>
                            {$pwd_txt}
                        </span>
                        <input type="password"  required="required" maxlength="15"  name="data[User][password]" class="form-control" placeholder="{$pwd_txt}">
                    </div>
                </div>
EOT;
}
$html .= <<<EOT
                <div class="form-group" style="margin-bottom: 0;">                  
                    <div class="checkbox" >
                            <label>
                              <input type="checkbox" checked="checked" name="data[User][remember_me]"  value="2592000"> {$remember_txt}
                            </label>        
                        </div>
                </div>
                <div class="form-group">
                    <div class="clearfix">
                            <button type="submit" class="btn-primary btn btn-block">{$login_txt}</button>  
                    </div>
                </div>
                
                    <hr>
                    
                    <p>
                    	<a target="_blank" class="pull-right" href="/users/forgot" class="color-green">{$forget_txt}</a>
                    	{$noreg_txt} <a href="/users/register" target="_blank" >{$reg_txt}</a>
                    </p>
                    <p>如果无法登录或退出登录，点击这里<a href="javascript:;" onclick="return clearUserCookie();">清除Cookie</a></p>
                   
</form>  
  </div>   
  </div>                       
</div>

EOT;

//echo str_replace(array("\n","\r"),'',$html);
echo json_encode(array(
    'ret'=> -10910,
    'msg' => __("You need to login"),
    'tasks' => array(array(
        'dotype' => 'msg_dialog',
        'id' => 'login_dialog',
        'title' => __("You need to login"),
        'content' => str_replace(array("\n","\r"),'',$html),
    )),
));
?>