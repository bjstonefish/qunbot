<?php
if(isset($_SERVER['SERVER_ADDR']) && ($_SERVER['SERVER_ADDR'] == '127.0.0.1' || $_SERVER['SERVER_ADDR'] == '::1')) {
	Configure::write('debug', 0);
	Configure::write('Cache.disable', false);
	define('IS_LOCALHOST',true);
}
else{
	Configure::write('debug', 0);
	Configure::write('Cache.disable', false);
}

Configure::write('Error', array(
            'handler' => 'ErrorHandler::handleError',
            'level' => E_ERROR | E_WARNING | E_PARSE,
            'trace' => true
        ));
Configure::write('Exception', array(
            'handler' => 'ErrorHandler::handleException',
            'renderer' => 'ExceptionRenderer',
            'log' => true
        ));
Configure::write('App.encoding', 'UTF-8');

Configure::write('default_currency','CNY');

// 为默认语言时，链接不会带locale参数
// Configure::write('Site.language', 'zh-cn');

// Configure::write('App.baseUrl', env('SCRIPT_NAME'));
define('LOG_ERROR', 2);
/* 使用数据库记录session信息，限制一个用户只允许一个人同时登录 */
/*
 * 数据库记录session使数据库操作太频繁，影响数据库性能。
 * 仅使用数据库记录已登录用户的session，比较与php的session与数据库已登录用户的session是否一致。来限制唯一性
Configure::write('Session', array(
    'defaults' => 'database',
    'timeout' => 1500,
    'name' => 'Miao',
    'handler' => array(
       'engine' => 'CustomDatabaseSession',
	   'model' => 'Session'
    )
));
*/

Configure::write('Session', array(
   'defaults' => 'php',
   'timeout' => 1500,
   'name' => 'Miao'
));



Configure::write('Session.cookie', 'cake');
Configure::write('Security.level', 'medium');

// Configure::write('Asset.timestamp', true);
// Configure::write('Asset.compress', true);
// Configure::write('Asset.filter.css', 'asset_filter.php');
// Configure::write('Asset.filter.js', 'asset_filter.php');


Configure::write('Acl.classname', 'DbAcl');
Configure::write('Acl.database', 'default');

if (function_exists('date_default_timezone_set')) {
    date_default_timezone_set('Etc/GMT-8');
}

if (defined('SAE_MYSQL_DB')) {
    $engine = 'Saemc';
}
elseif(isset($_SERVER['HTTP_HOST']) && preg_match('/\.aliapp\.com$/',$_SERVER['HTTP_HOST'])){
	$engine = 'Acemc';
}
else {
    $engine = 'File';
    if (extension_loaded('apc') && (php_sapi_name() !== 'cli' || ini_get('apc.enable_cli'))) {
        $engine = 'Apc';
    }
}



if(defined('SAE_MYSQL_DB')){
	// 区分各版本的缓存，不互相冲突
	$cache_prefix = $_SERVER['HTTP_APPVERSION'].'_';
}
else{
	$cache_prefix= '';
}
$cache_prefix .= APP_DIR.'_';
/*
 *  前后台使用不同的缓存文件 _cake_core_,_cake_model_
 *  缓存的配置，前台的前缀包含后台的前缀（利用后台的prefix比较时能涵盖前台的文件）。后台删除缓存时，前后台就都能删除了.
 *  如后台的前缀为 miaocms_, 则前台的前缀可使用miaocms_app_
 *  
 *  前台后使用相同的配置文件 default
 *  后台操作修改数据更新缓存时，同时能更新前台的缓存（使用同一份缓存文件）
 *  如后台修改Setting设置项
 */
Cache::config('_cake_core_', array(
            'engine' => 'File',
            'prefix' => 'core_'.$cache_prefix,
            'path' => CACHE . 'persistent' . DS,
            'serialize' => true,
            'duration' => 7200,
            'probability'=> 100,
        ));
/* 使用统一的前缀model,不加prefix区分，不同数据库同名表格要有同样的结构与用途 */
Cache::config('_cake_model_', array(
            'engine' => 'File',
            'prefix' => 'model_',
            'path' => CACHE . 'models' . DS,
            'serialize' => true,
            'duration' => 7200,
            'probability'=> 100,
        ));

Cache::config('long', array(
	'engine' => $engine,
	'duration' => 432000, // 5 days
	'probability' => 100,
	'compress' => false,
	'prefix' => 'miaocms_'.$cache_prefix,
	'lock' => false,
	'serialize' => ($engine === 'File'),
));
// In development mode, caches should expire quickly.
// 缓存的配置，后台的前缀与前台的前缀保持一致。后台删除缓存时，前后台缓存就都能删除了
$duration = 900;
if (Configure::read('debug') > 1) {
    $duration = 5;
}

Cache::config('default', array(
            'engine' => $engine,
            'duration' => $duration,
            'probability' => 100,
            'prefix' => 'miaocms_'.$cache_prefix,
            'lock' => false,
            'serialize' => ($engine === 'File'),
        ));


// UCenter config
/*
if(defined('SAE_MYSQL_DB')){ // in sae
	define('UC_CONNECT', 'mysql');
	define('UC_DBHOST', 'w.rdc.sae.sina.com.cn:3307');
	define('UC_DBUSER', 'w1yyo2lx31');
	define('UC_DBPW', 'j0i2ljiz4w3z4h42m115k0h21i4hw5lwwhkjz4m0');
	define('UC_DBNAME', 'app_discuzx');
	define('UC_DBCHARSET', 'utf8');
	define('UC_DBTABLEPRE', '`app_discuzx`.sae_ucenter_');
	define('UC_DBCONNECT', '0');
	define('UC_KEY', '9d5dj/1W72UJixnEUkqtR9t2KSY7QxAswfaH0UQ');
	define('UC_API', 'http://discuzx.sinaapp.com/uc_server');
	define('UC_CHARSET', 'utf-8');
	define('UC_IP', '');
	define('UC_APPID', '4');
	define('UC_PPP', '20');

}
elseif($_SERVER['SERVER_ADDR'] == '127.0.0.1'){ // localhost
	define('UC_CONNECT', 'mysql');
	define('UC_DBHOST', 'localhost');
	define('UC_DBUSER', 'root');
	define('UC_DBPW', 'xsdfuh232sdw!3S#sd');
	define('UC_DBNAME', 'ultrax');
	define('UC_DBCHARSET', 'utf8');
	define('UC_DBTABLEPRE', '`ultrax`.pre_ucenter_');
	define('UC_DBCONNECT', '0');
	define('UC_KEY', 'fcbe360P7xsz45nhpq4awu1BjeMqZzoaNvctV+o');
	define('UC_API', 'http://www.a.com/uc_server');
	define('UC_CHARSET', 'utf-8');
	define('UC_IP', '');
	define('UC_APPID', '2');
	define('UC_PPP', '20');
}
*/

define('UC_REMOTEUSER',false); 
// remote user 为true时，使用远程验证ucenter的用户和密码。
// 为false时，验证本地的用户和密码，同时同步登陆ucenter的其它应用
// 
// 