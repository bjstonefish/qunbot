<?php
class Questionnaire extends AppModel { 
    var $name = 'Questionnaire';
    var $hasAndBelongsToMany = array(
    	"Survey"=> array(
			'joinTable' => 'questionnaire_surveys',
			'with' => 'QuestionnaireSurvey',
			'foreignKey' => 'questionnaire_id',
			'associationForeignKey' => 'survey_id',
			'order' => 'QuestionnaireSurvey.sort desc',
    	),
    );
}