<?php

class WxImgLog extends AppModel {

    var $name = 'WxImgLog';
    
    public function beforeSave($options = array())
    {
    	parent::beforeSave($options);
    	
    	if($this->data['WxImgLog']['content']) {
    	    $this->data['WxImgLog']['content'] = json_encode(array_values($this->data['WxImgLog']['content']));
    	}
    	return true;
    }
    
    public function afterFind($results, $primary = false) {    	
    	foreach($results as &$msg) {
    		$msg['WxImgLog']['content'] = json_decode($msg['WxImgLog']['content'],true);
    	}
    	return $results;
    }
}
?>