<?php
class Article extends AppModel { 
       var $name = 'Article';
       
       var $useDbConfig = 'article';
       
       var $defaultDbConfig = 'article';
       var $masterDbConfig = 'article';

       var $article_content  = '';
       
       var $content_to_oss = true; //将内容保存到OSS
       var $delete_id = 0;
       
       /*
        * 内容直接保存到oss中，不需要ArticleContent表
        */
        /*var $hasOne = array( 
          'ArticleContent'=>array(
            'className'     => 'ArticleContent',
           'foreignKey'    => 'id',
           'dependent'=> true
          ),
        );*/
       
       /*var $hasMany = array(
	       'Uploadfile' => array(
		       'className'     => 'Uploadfile',
		       'foreignKey'    => 'data_id',
		       'conditions'    => array('Uploadfile.modelclass' => 'Article','Uploadfile.trash' => '0'), 
		       'order'    => 'Uploadfile.sortorder asc,Uploadfile.created ASC',
		       'limit'        => '',
		       'dependent'=> true
	       )
       );*/
       /*var $belongsTo=array(
        'Category' => array(
            'className' => 'Category',
            'foreignKey' => 'cate_id',
//             'conditions' => "Category.model='Article'",
        ),
    );*/
    var $validate = array(
    	'name' => array(
            'rule' => 'notEmpty',
            'message' => 'This field cannot be left blank.',
        ),        
    );
    

    public function beforeSave($options = array())
    {
    	parent::beforeSave($options);
    
    	$this->article_content = $this->data['Article']['content'] ?  $this->data['Article']['content'] : $this->data['ArticleContent']['content'];
    	unset($this->data['Article']['content']);
    	unset($this->data['ArticleContent']);
    	return true;
    }
    
    public function afterSave($created = false) {
    	parent::afterSave($created);
    
    	$id =  $this->id;
    	if($this->content_to_oss  && Configure::read('Article.content_to_oss') && $id && $this->article_content &&  $this->id){
    		App::uses('CrawlUtility', 'Utility');
    		CrawlUtility::saveToAliOss($this->article_content, '/article_content/'.$id,false,true);
    	}
    
    	return true;
    }
    
    public function beforeDelete($cascade = true) {
        parent::beforeDelete($cascade);
        $this->delete_id = $this->id;
    }
    
    public function afterDelete(){
        if( $this->content_to_oss  && Configure::read('Article.content_to_oss') && $this->delete_id ){
            App::uses('CrawlUtility', 'Utility');
            $object_name = '/article_content/'. $this->delete_id;
            if(! CrawlUtility::delFromAliOss( $object_name )) {
                // 删除数据后，删除OSS中存储的内容
                CakeLog::info("delete from oss $object_name  failed");
            }
        }
        parent::afterDelete();
    }
    
    public function afterFind($results, $primary = false) {
    
    	if( $this->content_to_oss && Configure::read('Article.content_to_oss') ) {
    		App::uses('CrawlUtility', 'Utility');
    		try{
    		    $total = count($results);
    		    if($total == 1 || $this->recursive ==1) { // 只有一条，或者要拉取所有相关记录
    		        foreach($results as &$item) {
    		            $content = CrawlUtility::getFromAliOss('/article_content/'.$item['Article']['id']);
    		            if($content) {
    		                $item['ArticleContent']['content'] = $content; // 有内容时，才使用值，否则使用数据库中的值。兼容数据库中已有的数据
    		            }
    		            else{
    		                $content = CrawlUtility::getFromAliOss('/article_content/'.$item['Article']['id'],$header,array('bucket_name'=>'wx135'));
    		                if($content) {
    		                    $item['ArticleContent']['content'] = $content; // 有内容时，才使用值，否则使用数据库中的值。兼容数据库中已有的数据
    		                }
    		            }
    		        }
    		    }    			
    		}
    		catch(Exception $e){
    			return $results;
    		}
    	}
    	return $results;
    }
    
} 