<?php

class ScoreRule extends AppModel {

    var $name = 'ScoreRule';
    
    public function getRule($model,$action) {
    	$cache_key = 'score_rule_'.$model.'_'.$action;
    	$rule = Cache::read($cache_key);
    	if($rule === false) {
    		$rule = $this->find('first',array(
    				'conditions' => array(
    						'model' => $model,
    						'action' => $action,
    						'status' => 1,
    				)
    		));
    		Cache::write($cache_key, $rule);
    	}
    	return $rule;
    }
}
?>