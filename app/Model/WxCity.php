<?php

class WxCity extends AppModel {

    var $name = 'WxCity';
    
    var $belongsTo=array(
    		'City' => array(
    				'className' => 'City',
    				'foreignKey' => 'city_id',
    		),
    );
    
}
?>