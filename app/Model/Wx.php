<?php

App::uses('WeixinUtility', 'Utility');
App::uses('WeixinCompUtility', 'Utility');
App::uses('CrawlUtility', 'Utility');

class Wx extends AppModel {

    var $name = 'Wx';
    
    var $validate = array(
    		'name' => array(
    				'rule' => 'notEmpty',
    				'message' => 'This field cannot be left blank.',
    		),
    );
    
    public function getWxAppInfo($wx_id) {
        
        $wx = $this->find('first',array(
            'conditions'=> 	array('Wx.id' => $wx_id),
            'joins' => array(array(
                'table' => 'oauthbinds',
                'alias' => 'Oauthbind',
                'type' => 'left',
                'conditions' => array(
                    'Wx.creator=Oauthbind.user_id',
                    'Wx.oauth_appid=Oauthbind.oauth_openid',
                    'Oauthbind.source'=>'wechatComp',
                    //'Oauthbind.updated >' => time() - 7200,
                )
            )),
            'order' => 'Oauthbind.updated desc',
            'fields' => 'Wx.*,Oauthbind.*',
        ));
         
        if($wx['Oauthbind'] && $wx['Oauthbind']['updated'] < time() - 7100) {
             
            $refresh_result = WeixinCompUtility::refreshToken($wx['Oauthbind']['oauth_openid'],$wx['Oauthbind']['refresh_token']);
            if(empty($refresh_result)) { // 网络原因未取得结果，跳过不处理。下次继续请求
                $refresh_result = WeixinCompUtility::refreshToken($wx['Oauthbind']['oauth_openid'],$wx['Oauthbind']['refresh_token']);
                if(empty($refresh_result)) {
                    // 				                unset($this->user_wxes[$k]); //过期刷新失败
                    return false;
                }
            }
            if( empty($refresh_result['errcode']) && !empty($refresh_result['authorizer_access_token']) ){
                $oauthbind = loadModelObject('Oauthbind');
                $oauthbind->updateAll(array(
                    'oauth_token' => $oauthbind->escape_string($refresh_result['authorizer_access_token']),
                    'expires' => $refresh_result['expires_in'],
                    'refresh_token' => $oauthbind->escape_string($refresh_result['authorizer_refresh_token']),
                    'updated' => time(),
                ),array('id'=>$wx['Oauthbind']['id']));
                
                $wx['Oauthbind']['oauth_token'] = $refresh_result['authorizer_access_token'];
            }
        }
        return $wx;
        
    }
    
    public function getByAppId($appid){
    	$cache_key = 'wxap_'.$appid;
    	$item = Cache::read($cache_key);
    	if($item === false){
    		$item =  $this->find('first',array(
    				'conditions' => array('Wx.oauth_appid' => $appid ),
    				'recursive' => -1,
    		));
    		Cache::write($cache_key, $item);
    	}
    	return $item;
    }
    
    public function getById($wx_id){    	
    	$cache_key = 'wx_'.$wx_id;
    	$item = Cache::read($cache_key);
    	if($item === false){
	    	$item =  $this->find('first',array(
	    			'conditions' => array( 'Wx.id' => $wx_id ),
	    			'recursive' => -1,
	    	));
	    	Cache::write($cache_key, $item);
    	}
    	return $item;
    }
    
}
?>