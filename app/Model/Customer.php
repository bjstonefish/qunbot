<?php

class Customer extends AppModel {

    var $name = 'Customer';

    public function refresh($customer_id){
        $data = array(
            'id'=>$customer_id,
            'lastconnect'=>date('Y-m-d H:i:s')
        );
        return $this->save($data);
    }

}
?>