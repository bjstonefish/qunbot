<?php
class AppForum extends AppModel { 

    var $name = 'AppForum'; 
    
    var $useDbConfig = 'liteapp';
    var $defaultDbConfig = 'liteapp_slave';
    var $masterDbConfig = 'liteapp';
    
    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }
    
}
?>