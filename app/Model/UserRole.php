<?php

class UserRole extends AppModel {

    var $name = 'UserRole';
    
    var $actsAs = array('EventLog');
    
    /**
     * return false to stop delete
     * @see Model::beforeDelete()
     */
    public function beforeDelete($cascade = ture) {
    	Cakelog::debug("skip UserRole delete.\n url:".Router::url()."\n request:".var_export($_REQUEST,true)." session:".var_export(CakeSession::read('Auth.User'),true)."\n user_roles id:".$this->id);
    	return false;
    }
    
    
}
?>