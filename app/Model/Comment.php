<?php

class Comment extends AppModel {

    var $name = 'Comment';

    var $validate = array(
        'body' => array(
            'minLength' => array(
                'rule' => array('minLength', 6),
                'message' => '评论内容不能少于6个字',
            ),
        ),
    );
}
?>