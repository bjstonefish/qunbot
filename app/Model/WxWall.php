<?php

class WxWall extends AppModel {

    var $name = 'WxWall';
    
    public function  getActiveWall($wx_id) {
        $cache_active =  'active_wall_'.$wx_id;
        $item = Cache::read($cache_active);
        if($item === false) {
            $time = date('Y-m-d H:i:s');
            $item = $this->find('first',array('conditions'=> array(
                'wx_id' => $wx_id,'status' => 0,
                'started <' => $time, 'ended >'=> $time,
            )));
            Cache::write($cache_active, $item);
        }
        
        return $item;
        
    }
    
    public function afterSave($created = false) {
        if($this->data['WxWall']['wx_id']) {
            $cache_active =  'active_wall_'.$this->data['WxWall']['wx_id'];
            Cache::delete($cache_active);
        }
        parent::afterSave($created);        
    }
}
?>