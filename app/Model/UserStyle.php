<?php
class UserStyle extends AppModel {
    
       var $name = 'UserStyle';
       
       var $msg_content  = '';
       
       var $content_to_oss = true; //将内容保存到OSS
       var $delete_id = 0;
       
       public function beforeSave($options = array())  {
           parent::beforeSave($options);
           if( $this->content_to_oss && Configure::read('Storage.aliyun_oss') ){
               $this->msg_content = $this->data['UserStyle']['content'];
               $this->data['UserStyle']['content'] = '';
           }
           return true;
       }
       
       public function afterSave($created = false) {
       
           if($this->msg_content) {
               $this->data['UserStyle']['content'] = $this->msg_content; // 表中不保存content字段值，保存后重设content，一起保存历史版本到VersionControl中
       
           }
       
           parent::afterSave($created);
            
           $id =  $this->id;
           
           $options = array();
           if($id < 123733) {
               $options['bucket_name'] = 'wx135';
           }
           
           if( $this->content_to_oss && $id && $this->msg_content && Configure::read('Storage.aliyun_oss') ){
               App::uses('CrawlUtility', 'Utility');
               if(! CrawlUtility::saveToAliOss($this->msg_content, '/UserStyle/'.$id,false,true,false,$options) ) {
                   $this->updateAll(array('content'=> $this->escape_string($this->msg_content)),array('id' => $id) );
                   // 保存失败时，将内容回写到数据库。
               }
           }
           return true;
       }
       
       public function beforeDelete($cascade = true) {
           parent::beforeDelete($cascade);
           $this->delete_id = $this->id;
       }
       
       public function afterDelete(){
           if( $this->content_to_oss && $this->delete_id ){
               App::uses('CrawlUtility', 'Utility');
               $object_name = '/UserStyle/'. $this->delete_id;
               
               $ret = true;
               if($this->delete_id < 123733) {
                   $ret = CrawlUtility::delFromAliOss( $object_name,'wx135');
               }
               else{
                   $ret = CrawlUtility::delFromAliOss( $object_name );
               }
               if(! $ret ) {
                   // 删除数据后，删除OSS中存储的内容
                   CakeLog::info("delete from oss $object_name  failed");
               }
           }
           parent::afterDelete();
       }
       
       public function afterFind($results, $primary = false) {
            
           if( $this->content_to_oss ) {
               App::uses('CrawlUtility', 'Utility');
               try{
                   foreach($results as &$msg) {
                       if($msg['UserStyle']['id'] < 123733) {
                           $options = array('bucket_name'=>'wx135');
                           $header = array(); //引用传参，函数中设置值
                           $content = CrawlUtility::getFromAliOss('/UserStyle/'.$msg['UserStyle']['id'],$header,$options);
                       }
                       else{
                           $content = CrawlUtility::getFromAliOss('/UserStyle/'.$msg['UserStyle']['id']);
                       }
                       if($content) {
                           $msg['UserStyle']['content'] = $content; // 有内容时，才使用值，否则使用数据库中的值。兼容数据库中已有的数据
                       }
                   }
               }
               catch(Exception $e){
                   return $results;
               }
           }
           return $results;
       }
} 