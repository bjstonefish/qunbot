<?php
class Uploadfile extends AppModel {

    var $name = 'Uploadfile'; 
    
    // 仅为微信文章模块时，才分表处理。
    var $actsAs = array('SplitModel'=> array( 'field'=> 'user_id' ));

    /* save 方法时，会判断数据是否存在 */
    public function exists($id = null) {
        if ($id === null) {
            $id = $this->getID();
        }
        if ($id === false) {
            return false;
        }
        return (bool)$this->find('count', array(
            'conditions' => array(
                $this->alias . '.' . $this->primaryKey => $id
            ),
            'recursive' => -1,
            'callbacks' => 'before' // false. before用于检测分表的数据，定位到不同的分表
        ));
    }
    
} 
?>