<?php
class AppForumComment extends AppModel { 

    var $name = 'AppForumComment'; 
    
    var $useDbConfig = 'liteapp';
    var $defaultDbConfig = 'liteapp_slave';
    var $masterDbConfig = 'liteapp';
    
    // 仅为微信文章模块时，才分表处理。
    var $actsAs = array('SplitModel'=> array( 'field'=> 'app_id','split'=> 512 ));
    
    public function __construct($id = false, $table = null, $ds = null) {
        
//         $slaves = array('rr-bp150q78ezl3e0u4n.mysql.rds.aliyuncs.com','rr-bp1o3yapd61f96094.mysql.rds.aliyuncs.com');
//         $rdx = rand(0,1);
//         $this->default['host'] = $slaves[$rdx];
        
        parent::__construct($id, $table, $ds);
    }
    
} 
?>