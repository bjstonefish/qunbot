<?php

class SignToken extends AppModel {

    var $name = 'SignToken';

    public function getToken($token) {
        $cache_key = 'st_'.$token;
        $signToken = Cache::read($cache_key);
        if($signToken ===false) {
            $signToken = $this->findByName($token);
            if(!empty($signToken)) {
                Cache::write($cache_key,$signToken);
            }
        }
        return $signToken;
    }

}
?>