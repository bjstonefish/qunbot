<?php

class MobileCaptcha extends AppModel {

    var $name = 'MobileCaptcha';

    /**
     * 获取手机号半小时的有效短信码
     * @param $mobile
     * @return array
     */
    public function getCodes($mobile){

        $result = $this->find('all',array(
            'conditions' => array(
                'mobile' => $mobile,
                'created >' => date('Y-m-d H:i:s',strtotime('-30 minutes')),
            ),
        ));
        $codes = array();
        foreach($result as $item){
            $codes[] = strtolower($item['MobileCaptcha']['code']);
        }
        return $codes;
    }

    public function remove($mobile,$code){
        $this->deleteAll(array('mobile'=>$mobile,'code'=>$code));
    }
}
?>