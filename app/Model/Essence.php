<?php
class Essence extends AppModel { 
       var $name = 'Essence';

       
       var $hasAndBelongsToMany = array(
	       'Tag' => array(
           'className'              => 'Tag',
              'joinTable'              => 'tag_relateds',
              'foreignKey'             => 'relatedid', // 对应本模块的id
              'associationForeignKey'  => 'tag_id', // 对应tag的id
              'conditions'             => array('TagRelated.relatedmodel' => 'Essence'),
              'unique'                 => true,//'keepExisting'
              // 'limit'        => 100,
              'dependent'            => true,
              'exclusive'            => true,
	       )
       );    
} 