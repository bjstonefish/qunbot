<?php
class EditorStyle extends AppModel { 
       var $name = 'EditorStyle';

       
       var $hasAndBelongsToMany = array(
	       'Tag' => array(
           'className'              => 'Tag',
              'joinTable'              => 'tag_relateds',
              'foreignKey'             => 'relatedid', // 对应本模块的id
              'associationForeignKey'  => 'tag_id', // 对应tag的id
              'conditions'             => array('TagRelated.relatedmodel' => 'EditorStyle'),
              'unique'                 => true,//'keepExisting'
              // 'limit'        => 100,
              'dependent'            => true,
              'exclusive'            => true,
	       )
       );    
} 