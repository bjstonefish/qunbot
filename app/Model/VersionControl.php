<?php

class VersionControl extends AppModel {
	
    var $name = 'VersionControl';    
    
    var $primaryKey = 'version_id';
    
    var $actsAs = array('SplitModel'=> array( 'field'=> 'data_id' ));
    
}
?>