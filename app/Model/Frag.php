<?php
class Frag extends AppModel {
       var $name = 'Frag';

    /**
     * 获取随机的碎片内容，缓存为10秒
     * @param int $limit
     * @return array|mixed
     */
       public function getRand($limit = 1){
           $cache_key = 'rand-frag-'.$limit;
            $datas = Cache::read($cache_key);
            if($datas === false) {
                $datas = $this->find('all',array(
                    'conditions'=> array('deleted'=>0),
                    'recursive' => -1,
                    'limit' => $limit,
                    'order' => 'RAND()',
                ));
                Cache::write($cache_key,$datas,'default',10);
            }
            return $datas;
       }
}