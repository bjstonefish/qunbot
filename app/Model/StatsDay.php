<?php
class StatsDay extends AppModel { 
       var $name = 'StatsDay';
       
       
       function addlog($model,$data_id,$stats_type='view') {

           if(empty($data_id)) {
               return false;
           }
			if(!in_array($stats_type,array('view','favor','comment'))) {
				throw new ForbiddenException("stats_type should in ('view','favor','comment')");
			}
           $year = date('Y');
           $month = date('m');
           $day = date('d');
           $referer = $_SERVER['HTTP_REFERER'];
           if(!empty($referer)) {
               $parse = parse_url($referer);
               $this->data['StatsDay']['related'] = $parse['path']; //记录路径
           }

			$this->data['StatsDay']['model'] = $model;
			$this->data['StatsDay']['data_id'] = $data_id;
			$this->data['StatsDay']['date'] = date('Y-m-d');
			$this->data['StatsDay']['year'] = $year;
			$this->data['StatsDay']['month'] = $month;
			$this->data['StatsDay']['day'] = $day;
			
			$num_fields = $stats_type.'_nums';
			$hasdata = $this->find('first',array(
				'conditions'=>$this->data['StatsDay'],
				));
			
			if(empty($hasdata))	{
				$this->data['StatsDay'][$num_fields] = 1;
				return $this->save($this->data);
			}
			else {
				return $this->updateAll( array( $num_fields=>"$num_fields+1" ), array('id'=>$hasdata['StatsDay']['id'])	);
			}
       }
}