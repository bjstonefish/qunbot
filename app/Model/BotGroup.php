<?php
class BotGroup extends AppModel {
    
    var $name = 'BotGroup';
    
    var $belongsTo=array(
        'Bot' => array(
            'className' => 'Bot',
            'foreignKey' => 'bot_id',
        ),
    );

    var $hasMany = array(
        'BotGroupMember' => array(
            'className'     => 'BotGroupMember',
            'foreignKey'    => 'group_id',
            'dependent'=> true,
        )
    );
    
       /*
       
       var $hasOne = array(
          'Bot'=>array(
            'className'     => 'Bot',
            'foreignKey'    => 'bot_id',
            'dependent'=> true
          ),
        );
         */
} 