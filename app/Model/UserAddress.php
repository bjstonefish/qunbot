<?php
class UserAddress extends AppModel {
    var $name = 'UserAddress';
    
    public function getUserAddressInfo($uid,$address_id){
    	if(empty($uid) || empty($address_id)){
    		return false;
    	}
    	else{
    		$address_info = $this->find('first',array(
				'conditions'=>array(
					'creator' => $uid,
					'id' => $address_id,
				)
			));
			return $address_info;
    	}
    }

    public function getUserAddressList($uid){
    	$address_list = $this->find('all',array(
			'conditions'=>array(
				'creator' => $uid,
			),
			'limit' => 10,
		));
		return $address_list;
    }
}
?>