<?php
class AppThreadPraiseLog extends AppModel { 

    var $name = 'AppThreadPraiseLog'; 
    
    var $useDbConfig = 'liteapp';
    var $defaultDbConfig = 'liteapp_slave';
    var $masterDbConfig = 'liteapp';
    
    // 仅为微信文章模块时，才分表处理。
    var $actsAs = array('SplitModel'=> array( 'field'=> 'app_id','split'=> 256 ));
    
    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }
    
} 
?>