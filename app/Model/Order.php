<?php

class Order extends AppModel {

    var $name = 'Order';
    
    var $actsAs = array('EventLog');

    var $hasMany = array(
        'Cart' => array(
            'className'     => 'Cart',
            'foreignKey'    => 'order_id',
            'dependent'=> true
        )
    );
    
}
?>