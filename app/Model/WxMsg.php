<?php

class WxMsg extends AppModel {

    var $name = 'WxMsg';
    
    var $msg_content  = '';
    

    var $actsAs = array('SplitModel'=> array( 'field'=> 'creator' ));    
    
    var $hasAndBelongsToMany = array(
					'Tag' => array(
							'className'              => 'Tag',
							'joinTable'              => 'tag_relateds',
							'foreignKey'             => 'relatedid', // 对应本模块的id
							'associationForeignKey'  => 'tag_id', // 对应tag的id
							'conditions'             => array('TagRelated.relatedmodel' => 'WxMsg' ),
							'unique'                 => true,//'keepExisting'
							// 'limit'        => 100,
							'dependent'            => true,
							'exclusive'            => true,
					)
			);
    
    var $content_to_oss = true; //将内容保存到OSS
    var $delete_id = 0;
    var $creator = 0;
    
    public function beforeSave($options = array())
    {
    	parent::beforeSave($options);
    	if( $this->content_to_oss && Configure::read('Storage.aliyun_oss') ){
        	$this->msg_content = $this->data['WxMsg']['content'];
        	$this->data['WxMsg']['content'] = '';
    	}
    	return true;
    }
    
    public function afterSave($created = false) {

        if($this->msg_content) {
            $this->data['WxMsg']['content'] = $this->msg_content; // 表中不保存content字段值，保存后重设content，一起保存历史版本到VersionControl中
                
        }
        
    	parent::afterSave($created);
    	
    	$id =  $this->id;
    	$options = array();
    	if( $this->content_to_oss && $id && $this->msg_content && Configure::read('Storage.aliyun_oss') ){
	    	App::uses('CrawlUtility', 'Utility');
	    	if($id < 2827868) {
	    		$options['bucket_name'] = 'wx135';
	    	}
	    	else{
	    	    $options['bucket_name'] = '135editor';
	    	}
	    	
	    	if( $id > 3599999 ) {
	    		$object_name = '/wxmsg/'.$this->data['WxMsg']['creator'].'/'.$id; // 分表后，在不同的表中，$id数据会重复。不能单独用id存储内容。
	    	}
	    	else{
	    		$object_name = '/wxmsg/'. $id; 
	    	}
	    	
	    	if(! CrawlUtility::saveToAliOss($this->msg_content, $object_name,false,true,false,$options)) {
	    		$this->updateAll(array('content'=> $this->escape_string($this->msg_content)),array('id' => $id) );
	    		// 保存失败时，将内容回写到数据库。
	    	}
    	}
    	return true;
    }
    
    public function saveContent($id,$content) {
        App::uses('CrawlUtility', 'Utility');
        return CrawlUtility::saveToAliOss($content, '/wxmsg/'.$id,false,true,false,array('bucket_name'=>'135editor'));
    }
    
    public function getContent($id) {
        App::uses('CrawlUtility', 'Utility');
        return CrawlUtility::getFromAliOss('/wxmsg/'.$id);
    }
    
    public function beforeDelete($cascade = true) {
    	parent::beforeDelete($cascade);
    	$this->delete_id = $this->id;
//     	$item = $this->findById($this->id);
//     	$this->creator = $item['WxMsg']['creator'];
    }
    
    public function afterDelete(){
    	if( $this->content_to_oss && $this->delete_id ){
    		App::uses('CrawlUtility', 'Utility');
    		if( $this->delete_id > 3599999 ) {
    			$object_name = '/wxmsg/'.$this->creator.'/'. $this->delete_id;
    		}
    		else{
    			$object_name = '/wxmsg/'. $this->delete_id;
    		}
		    $ret = true;		    
		    if($this->delete_id < 2827868) {
		        $ret = CrawlUtility::delFromAliOss( $object_name,'wx135');
		    }
		    else{
		        $ret = CrawlUtility::delFromAliOss( $object_name );
		    }
		    if(! $ret ) {
    		    // 删除数据后，删除OSS中存储的内容
    		    CakeLog::info("delete from oss $object_name  failed");
    		}
    	}
    	parent::afterDelete();
    }
    
    public function afterFind($results, $primary = false) {
    	
    	if( $this->content_to_oss ) {
	    	App::uses('CrawlUtility', 'Utility');
	    	try{
	    	    $header = array(); //引用传参，函数中设置值
		    	foreach($results as &$msg) {
		    		if($msg['WxMsg']['id'] > 3599999) {
		    		    $options = array('bucket_name'=>'135editor');
		    			$content = CrawlUtility::getFromAliOss('/wxmsg/'.$msg['WxMsg']['creator'].'/'.$msg['WxMsg']['id'],$header,$options);
		    		}
		    		elseif($msg['WxMsg']['id'] < 2827868) {
		    		    $options = array('bucket_name'=>'wx135');
		    		    $content = CrawlUtility::getFromAliOss('/wxmsg/'.$msg['WxMsg']['id'],$header,$options);
		    		}
		    		else{
		    		    $options = array('bucket_name'=>'135editor');
		    		    $content = CrawlUtility::getFromAliOss('/wxmsg/'.$msg['WxMsg']['id'],$header,$options);
		    		}
		    		if($content) {
		    			$msg['WxMsg']['content'] = $content; // 有内容时，才使用值，否则使用数据库中的值。兼容数据库中已有的数据
		    		}
		    	}
	    	}
	    	catch(Exception $e){
	    		return $results;
	    	}
    	}
    	return $results;
    }
    
}
?>