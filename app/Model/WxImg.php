<?php

class WxImg extends AppModel {

    var $name = 'WxImg';
    
    public function beforeSave($options = array())
    {
        if( is_array($this->data['WxImg']['user_inputs']) && !empty($this->data['WxImg']['user_inputs']) )  {
            foreach($this->data['WxImg']['user_inputs'] as $k => $input) {//去除空的输入规则
                if(empty($input['name'])) {
                    unset($this->data['WxImg']['user_inputs'][$k]);
                }
            }
            $this->data['WxImg']['user_inputs'] = json_encode(array_values($this->data['WxImg']['user_inputs']));
        }
        if( is_array($this->data['WxImg']['random_options']) && !empty($this->data['WxImg']['random_options']) )  {
            $this->data['WxImg']['random_options'] = json_encode(array_values($this->data['WxImg']['random_options']));
        }
        
    	parent::beforeSave($options);
    	
    	return true;
    }
    
    public function afterFind($results, $primary = false) {
    	
    	foreach($results as &$msg) {
    			$msg['WxImg']['user_inputs'] = json_decode($msg['WxImg']['user_inputs'],true);
    			$msg['WxImg']['random_options'] = json_decode($msg['WxImg']['random_options'],true);
    			//$msg['WxImg']['rule_details'] = json_decode($msg['WxImg']['rule_details'],true);
    	}
    	return $results;
    }
}
?>