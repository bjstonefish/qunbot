<?php
APP::uses('WxAppController', 'Controller');
class UserSettingsController extends WxAppController{
	
	var $uses = array('UserSetting','Wx');
	
	public $layout = 'user_default';

	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allowedActions = array_merge($this->Auth->allowedActions,array('setting'));
	}
	
	public function setting(){
	    
		if( empty($this->currentUser['id'])) {
			$this->__message(__('You need to login'), '/users/login');
		}
		
		$this->pageTitle = __('参数设置');
		
		$current_wxinfo = $this->current_wxinfo;		
		$wx_id = $this->current_wxinfo['Wx']['id'];		
		if( !$wx_id ){
		    $wx_id = 0;
		}
		$settings = $this->UserSetting->get($this->currentUser['id']);
		
		$this->set('user_wxes',$this->Session->read('User.wxes'));
		$this->set('current_wxinfo',$current_wxinfo);
		
		$this->set('wx_id',$wx_id);
		
		if ( empty($this->data) ) {
		    if($_REQUEST['type'] == 'preview') {
		        //设置上级分类的选项
		        WeixinUtility::$appId = $this->current_wxinfo['Wx']['oauth_appid'];
		        if($this->current_wxinfo['Wx']['oauth_appid']) {
		            $cache_key = 'wx_qrcode_preview_ticket_'.$this->current_wxinfo['Wx']['oauth_appid'];
		            $ticket = Cache::read($cache_key);
		            if($ticket === false) {
		                $qrcode = WeixinUtility::qrcode_create(WX_PREVIEW_SCENE_ID); 
		                if($qrcode['errcode'] == 48001) {
		                    // 账号没有认证，不能调用二维码及预览接口
		                     $errmsg = "您的微信公众号没有认证，不能设置预览消息接收微信。"; //使用扫码
		                     $this->set('errmsg',$errmsg);
		                }
		                else{
    		                $ticket = $qrcode['ticket'];
    		                Cache::write($cache_key, $ticket,'default',604800); //缓存时间最大支持7天
		                }
		            }
		        }
		        $this->set('ticket',$ticket);
		        
		        //$this->Session->delete('Wx.Varibale'); // 删除session记录的缓存，扫描后无法识别更新
		    }
		   
			$variables = array('wxAppId'=>'AppId','wxAppSecret'=>'AppSecret', 'token'=>'微信Token','wxAESKey'=> 'EncodingAESKey' );
			
			$this->data = array('UserSetting'=>$settings);
// 			$this->set('ids', $ids);
// 			$this->set('values', $values);
// 			$this->set('settings', $settings);
		}
		else{
			//数组相加已前面的为准
			$settings = $this->data['UserSetting'] + $settings ; //合并数组，已提交的内容为准，补充其他参数设置。
			$this->UserSetting->write($this->currentUser['id'],$settings);
			
			$this->Session->write('Auth.User.UserSetting',$settings);
			
			$successinfo = array('ret'=>0,'msg' => __('Setting success'));
			$this->Session->setFlash(__('Setting success'));
			$this->set('successinfo', $successinfo);
			$this->set('_serialize', 'successinfo');

            if ($this->RequestHandler->accepts('json') || $this->request->is('ajax') || isset($_REQUEST['inajax'])) {
                $this->ajaxOutput($successinfo);
            }

            $redirect = $this->data[$this->modelClass]['referer'] ? $this->data[$this->modelClass]['referer'] : ($_GET['referer'] ? $_GET['referer'] : $this->Session->read('Auth.redirect'));
			if(empty($redirect)){
				if($_SERVER['HTTP_REFERER']) {
					$urls = parse_url($_SERVER['HTTP_REFERER']);
					if( !empty($urls['query']) ) {
						$redirect = $urls['path'].'?'.$urls['query'] ;
					}
					else {
						$redirect = $urls['path'];
					}
				}
				else{
					$redirect = '/';
				}
			}
			$this->redirect($redirect);
			//$this->redirect(array('action'=>'mine'));
		}
	}
	
}
