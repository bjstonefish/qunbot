<?php

class ArticlesController extends AppController{
	public $name = 'Articles';	
	

	public function view($slug) {
	    parent::view($slug);
	}
	
	public function lists(){
	    parent::lists();
	}
	
	/**
	 * used in wx135.
	 * @param unknown $wx_id
	 * @param unknown $id
	 */
	public function dew_playlist($wx_id,$id){
		$fields = $this->Article->schema();
		$field_keys = array_keys($fields);
		if(in_array('wx_id',$field_keys) && in_array('voice_file', $field_keys)){
			
			$item = $this->Article->findById($id);
			$articles = $this->Article->find('all',array(
				'conditions' => array('wx_id' => $wx_id,'created <' => $item['Article']['created']),
				'order' => 'created desc',
				'fields' => 'id,name,voice_file',
				'limit' => 15,
			));
			
			$tracks = '<track>
		          <location>'.Router::url($item['Article']['voice_file']).'</location>
		          <creator></creator>
		          <album></album>
		          <title>'.$item['Article']['name'].'</title>
		          <annotation></annotation>
		          <duration></duration>
		          <image></image>
		          <info></info>
		          <link></link>
		        </track>';
			
			foreach($articles as $voice){
				$tracks .= '<track>
		          <location>'.Router::url($voice['Article']['voice_file']).'</location>
		          <creator></creator>
		          <album></album>
		          <title>'.$voice['Article']['name'].'</title>
		          <annotation></annotation>
		          <duration></duration>
		          <image></image>
		          <info></info>
		          <link></link>
		        </track>';
			}
			
		}
		$xml = '<?xml version="1.0" encoding="UTF-8"?>
<playlist version="1" xmlns="http://xspf.org/ns/0/">
    <title></title>
    <creator></creator>
    <link></link>
    <info></info>
    <image></image>
    <trackList>'.$tracks.'</trackList>
</playlist>';
		echo $xml;

		exit;
	}
}
