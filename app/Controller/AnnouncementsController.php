<?php

class AnnouncementsController extends AppController{
	
	var $name = 'Announcements';

	var $modelClass = 'Announcements';
	
	public function view($slug) {
	    parent::view($slug);

	    $this->loadModel('ViewLog');
	    $hasRead = $this->ViewLog->find('first',array(
	        'conditions' => array(
	            'model' => 'Announcement',
                'data_id' => $this->item['Announcement']['id'],
                'user_id' => $this->currentUser['id'],
            ),
        ));
	    if(empty($hasRead)) {
            $this->ViewLog->save(array(
                'model' => 'Announcement',
                'data_id' => $this->item['Announcement']['id'],
                'user_id' => $this->currentUser['id'],
                'times' => 1,
            ));
        }
        else{
            $this->ViewLog->updateAll( array('times'=>'times+1') , array('id'=>$hasRead['ViewLog']['id']) );
        }

        if($this->item['Announcement']['linkurl']){
	        $this->redirect($this->item['Announcement']['linkurl']);
        }

	}
	
	public function lists(){
	    parent::lists();
	}
	
}