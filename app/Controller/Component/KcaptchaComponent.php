<?php 

App::import('Vendor', 'kcaptcha', array('file' => 'kcaptcha'.DS.'kcaptcha.php'));
 
class KcaptchaComponent extends Component
{
    function startup(Controller $controller)
    {
        $this->controller = $controller;
    }

    function render_captcha()
    {
//        vendor('kcaptcha/kcaptcha');
        $kcaptcha = new KCAPTCHA();
        $this->controller->Session->write('captcha', $kcaptcha->getKeyString());
    }
}
?>