<?php
/**
 * 多站点模式钩子
 * @author arlon
 *
 */
class MultiSiteHookComponent extends Component {
	
    function startup(Controller $controller) {
    	
    	// 开启了多站点模式
    	if( Configure::read('Site.MultiSiteMode') ){
    		$cSiteCateKey = 'site_cate'.$_SERVER['HTTP_HOST'];
    		$site_category = Cache::read($cSiteCateKey);
    		if( $site_category === false) {
    			if( !$controller->Category ){
    				$controller->loadModel('Category');
    			}
    			$http_host = $_SERVER['HTTP_HOST'];
    			if('' == preg_replace('/[\d.]/','',$http_host)){ //为ip时，访问默认配置的站点域名
                    $info = parse_url( Configure::read('Site.url') );
                    $http_host = $info['host'];
				}

    			// 根据多站点的域名来查询当前域名站点对应的导航
    			$hosts = explode('.',$http_host);
    			$hosts[0]= '*';
    			$starhost = implode('.',$hosts);
    			
    			$site_category = $controller->Category->find('first',array(
    			    'conditions'=>array(
        			    'OR'=> array(
        			        array('domain like'=>'%'.$_SERVER['HTTP_HOST'].'%'),
        			        array('domain like'=>'%'.$starhost.'%'),
        			    ),
        			),
    			    'fields' => array('id','name','visible','left','right','coverimg','view_layout','theme','domain','extra_script'),
    			    
    			));
    			if($site_category){
    				$GLOBALS['site_cate_id'] = $site_category['Category']['id'];
    				Cache::write($cSiteCateKey,$site_category);
    			}
    		}
    		else{
    			$GLOBALS['site_cate_id'] = $site_category['Category']['id'];
    		}
    		
    		if(!empty($site_category)){
    			
    			Configure::write('Site.title',$site_category['Category']['name']);
    			
    			$cIndexKey = 'site_index_id'.$_SERVER['HTTP_HOST'];
    			$index_page = Cache::read($cIndexKey);
    			if( $index_page === false && $GLOBALS['site_cate_id'] ) {
    				
    				if( !$controller->Category ){
    					$controller->loadModel('Category');
    				}
    				
    				$index_category = $controller->Category->find('first',array(
    				    'conditions'=>array(
    						'left >' => $site_category['Category']['left'],
    						'right <' => $site_category['Category']['right'],
    						'is_index' => 1,
    				    ),
    				    'fields' => array('id'),
    				    
    				));
    				$index_page = $index_category['Category']['id'];
    				Cache::write($cIndexKey,$index_page);
    			}
    			Configure::write('Site.index_page',$index_page);
    			if($site_category['Category']['coverimg']){
    			    Configure::write('Site.logo',$site_category['Category']['coverimg']);
    			}
    			if($site_category['Category']['extra_script']){
    			    Configure::write('Site.extra_script',$site_category['Category']['extra_script']);
    			}
    			
    			if($site_category['Category']['view_layout']){
    			    if( $controller->layout === 'default') { //指定了特定的layout的时候，不修改整个的layout
    			        $controller->layout = $site_category['Category']['view_layout'];
    			    }
    			}
    			if($site_category['Category']['theme']){
    				$controller->theme = $site_category['Category']['theme'];
    			}
    		}    		
    			
    	}
    	if(!$GLOBALS['site_cate_id']){
    		$default_id = Configure::read('Site.default_site_cate_id');
    		if($default_id){
    			$GLOBALS['site_cate_id'] = $default_id;
    		}
    	}
    }
    
    function navigation(Controller $controller,$navigations){
    	if( Configure::read('Site.MultiSiteMode') && empty($_REQUEST['nodirect']) ){
    	    $hosts = explode('.',$_SERVER['HTTP_HOST']);
    	    $hosts[0]= '*';
    	    $starhost = implode('.',$hosts);
    	    
	    	foreach($navigations as $nav){
	    		if($nav['Category']['domain'] && strpos($nav['Category']['domain'],$_SERVER['HTTP_HOST']) === false
	    		    && strpos($nav['Category']['domain'],$starhost) === false ) {
					$domains = explode(',',$nav['Category']['domain']);
					$domain = current($domains);
					$url = 'http://'.$domain.$_SERVER['REQUEST_URI'];
		    			// 访问当前站点的数据，且不包含站点的域名时，自动跳转到该站点
		    			//print_r($nav);
					@header('Location:'.$url);
	    		}
	    	}
    	}
    }

    
}
?>
