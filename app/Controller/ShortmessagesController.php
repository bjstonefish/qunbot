<?php


class ShortmessagesController extends AppController {

    var $name = 'Shortmessages';
    public $layout = 'user_default';
//     public $layout = 'dashboard';
    
    public function unread(){
    
        $ret = array('ret'=>0,'total'=>0);
    
        $this->loadModel('Announcement');
    
        $announces = $this->Announcement->find('all',array(
            'conditions'=> array('published'=>1),
            'recursive' => -1,
            'limit' => 5,
            'fields' => array('id','name','created','linkurl','view_nums','cate_id'),
            'order' => 'id desc',
        ));
        $unread_announce = 0;
        if( !empty($announces) ) {
            $aids = array();
            foreach($announces as $a){
                $aids[] = $a['Announcement']['id'];
            }
            $viewed = array();
            if($this->currentUser['id']){
                $this->loadModel('ViewLog');
                $viewed = $this->ViewLog->find('list',array(
                    'fields'=> array('data_id','data_id'),
                    'conditions' => array(
                        'model' => 'Announcement',
                        'data_id' => $aids,
                        'user_id' => $this->currentUser['id'],
                    ),
                ));
            }

            $ret['Announcement'] = array();
            foreach($announces as $a){
                if( !in_array($a['Announcement']['id'],$viewed) ) {
                    $unread_announce++;
                    $a['Announcement']['haveread'] = 0;
                }
                else{
                    $a['Announcement']['haveread'] = 1;
                }
                $ret['Announcement'][] = $a['Announcement'];

            }
        }
    
    
        if($this->currentUser['id']){
            $ret['total'] = $this->Shortmessage->find('count', array(
                'conditions' => array('receiverid' => $this->currentUser['id'],'haveread'=> 0),
                'recursive' => -1,
            ));
    
            if( $ret['total'] > 0 ) {
                $messages = $this->Shortmessage->find('all', array(
                    'conditions' => array('receiverid' => $this->currentUser['id'],'haveread'=> 0),
                    'recursive' => -1,
                    'order' => 'id desc',
                    'limit' => 5,
                ));
    
                if( !empty($messages) ) {
                    $ret['Shortmessage'] = array();
                    foreach($messages as $m){
                        $ret['Shortmessage'][] = $m['Shortmessage'];
                    }
                }
            }
            $ret['total'] += $unread_announce;
            echo json_encode($ret);
        }
        else{
            echo json_encode($ret);
        }
        exit;
    }
    
    public function unreadnum(){
        
    	if($this->currentUser['id']){
	    	echo $this->Shortmessage->find('count', array(
	    			'conditions' => array('receiverid' => $this->currentUser['id'],'haveread'=> 0),
	    			'recursive' => -1,
	    	));
    	}
    	else{
    		echo 0;
    	}
    	exit;
    }
    
    public function read($id){
    	if($id && $this->currentUser['id']){
    		$this->Shortmessage->updateAll(array('haveread' => 1),array('receiverid' => $this->currentUser['id'],'id'=>$id));
    	}
    	echo 1;
    	exit;
    }
    

    public function mine(){
    	
    	$this->pageTitle = __('My').__d('modelextend','Shortmessage');
    	
    	$pagesize = intval(Configure::read('Shortmessage.pagesize'));
    	if(!$pagesize){
    		$pagesize = 30;
    	}
    	$page = $_REQUEST['page'] ? $_REQUEST['page']:1;
    
    	$total = $this->Shortmessage->find('count', array(
    			'conditions' => array('receiverid' => $this->currentUser['id']),
    			'recursive' => -1,    		
    	));
    
    	$datalist = $this->Shortmessage->find('all', array(
    			'conditions' => array('Shortmessage.receiverid' => $this->currentUser['id']),
    			'order'=>'Shortmessage.id desc',
    			'recursive' => -1,
    			'fields' => array('*'),
    	));
    
    	$page_navi = getPageLinks($total, $pagesize, '/shortmessages/mine', $page);
    	$this->set('datalist',$datalist);
    	$this->set('total',$total);
    	$this->set('page_navi', $page_navi);
    }
    
	function add()
	{
	    $this->pageTitle = __('Send A Short Message');
		if (!empty($this->data) ) {
			if($this->currentUser['id'] && $this->data['Shortmessage']['receiverid'])
			{
				$this->loadModel('User');
				$this->User->recursive = -1;
				$receiver = $this->User->findById($this->data['Shortmessage']['receiverid']);
				if($receiver)
				{
					$this->data['Shortmessage']['receiverid'] = $receiver['User']['id'];
					$this->data['Shortmessage']['msgfromid'] = $this->currentUser['id'];
					if( $this->Shortmessage->save($this->data) ){
					    echo json_encode(array('ret'=>0,'msg'=> '发送成功'));
					}
					else{
					    echo json_encode(array('ret'=>1,'msg'=> 'receiver not exists'));
					}
				}
				else
				{
					echo json_encode(array('ret'=>1,'msg'=> 'receiver not exists'));
				}
			}
			else
			{
				echo json_encode(array('ret'=>1,'msg'=> 'params error.'));
			}
			exit;
		}
	}

}
?>