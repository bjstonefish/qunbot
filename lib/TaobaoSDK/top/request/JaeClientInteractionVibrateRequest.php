<?php
/**
 * TOP API: taobao.jae.client.interaction.vibrate request
 * 
 * @author auto create
 * @since 1.0, 2014-08-01 12:30:47
 */
class JaeClientInteractionVibrateRequest
{
	
	private $apiParas = array();
	
	public function getApiMethodName()
	{
		return "taobao.jae.client.interaction.vibrate";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
