<?php
/**
 * TOP API: ecs.aliyuncs.com.ReleasePublicIpAddress.2014-05-26 request
 * 
 * @author auto create
 * @since 1.0, 2014-08-01 12:30:47
 */
class Ecs20140526ReleasePublicIpAddressRequest
{
	/** 
	 * 公网IP地址，格式如：121.1.1.0
	 **/
	private $publicIpAddress;
	
	private $apiParas = array();
	
	public function setPublicIpAddress($publicIpAddress)
	{
		$this->publicIpAddress = $publicIpAddress;
		$this->apiParas["PublicIpAddress"] = $publicIpAddress;
	}

	public function getPublicIpAddress()
	{
		return $this->publicIpAddress;
	}

	public function getApiMethodName()
	{
		return "ecs.aliyuncs.com.ReleasePublicIpAddress.2014-05-26";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->publicIpAddress,"publicIpAddress");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
