<?php
/**
 * TOP API: ecs.aliyuncs.com.ModifyIntranetBandwidthKb.2014-05-26 request
 * 
 * @author auto create
 * @since 1.0, 2014-08-01 12:30:47
 */
class Ecs20140526ModifyIntranetBandwidthKbRequest
{
	/** 
	 * 指定的需要实例规格的实例ID
	 **/
	private $instanceId;
	
	/** 
	 * 内网入带宽最大值，单位为Kbps。可取值范围为[1, 1000000]，默认1G不做限制。<br /> 支持最大值为：1000000<br /> 支持最小值为：1
	 **/
	private $intranetMaxBandwidthIn;
	
	/** 
	 * 内网出带宽最大值，单位为Kbps。可取值范围为[1, 1000000]，默认1G不做限制<br /> 支持最大值为：1000000<br /> 支持最小值为：1
	 **/
	private $intranetMaxBandwidthOut;
	
	private $apiParas = array();
	
	public function setInstanceId($instanceId)
	{
		$this->instanceId = $instanceId;
		$this->apiParas["InstanceId"] = $instanceId;
	}

	public function getInstanceId()
	{
		return $this->instanceId;
	}

	public function setIntranetMaxBandwidthIn($intranetMaxBandwidthIn)
	{
		$this->intranetMaxBandwidthIn = $intranetMaxBandwidthIn;
		$this->apiParas["IntranetMaxBandwidthIn"] = $intranetMaxBandwidthIn;
	}

	public function getIntranetMaxBandwidthIn()
	{
		return $this->intranetMaxBandwidthIn;
	}

	public function setIntranetMaxBandwidthOut($intranetMaxBandwidthOut)
	{
		$this->intranetMaxBandwidthOut = $intranetMaxBandwidthOut;
		$this->apiParas["IntranetMaxBandwidthOut"] = $intranetMaxBandwidthOut;
	}

	public function getIntranetMaxBandwidthOut()
	{
		return $this->intranetMaxBandwidthOut;
	}

	public function getApiMethodName()
	{
		return "ecs.aliyuncs.com.ModifyIntranetBandwidthKb.2014-05-26";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->instanceId,"instanceId");
		RequestCheckUtil::checkMaxValue($this->intranetMaxBandwidthIn,1000000,"intranetMaxBandwidthIn");
		RequestCheckUtil::checkMinValue($this->intranetMaxBandwidthIn,1,"intranetMaxBandwidthIn");
		RequestCheckUtil::checkMaxValue($this->intranetMaxBandwidthOut,1000000,"intranetMaxBandwidthOut");
		RequestCheckUtil::checkMinValue($this->intranetMaxBandwidthOut,1,"intranetMaxBandwidthOut");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
