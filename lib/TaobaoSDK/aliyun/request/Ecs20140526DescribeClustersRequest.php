<?php
/**
 * TOP API: ecs.aliyuncs.com.DescribeClusters.2014-05-26 request
 * 
 * @author auto create
 * @since 1.0, 2014-08-01 12:30:47
 */
class Ecs20140526DescribeClustersRequest
{
	/** 
	 * 区域ID
	 **/
	private $regionId;
	
	private $apiParas = array();
	
	public function setRegionId($regionId)
	{
		$this->regionId = $regionId;
		$this->apiParas["RegionId"] = $regionId;
	}

	public function getRegionId()
	{
		return $this->regionId;
	}

	public function getApiMethodName()
	{
		return "ecs.aliyuncs.com.DescribeClusters.2014-05-26";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->regionId,"regionId");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
