<?php

//ACCESS_ID
define('OSS_ACCESS_ID', Configure::read('Storage.alioss_access_id'));
//ACCESS_KEY
define('OSS_ACCESS_KEY', Configure::read('Storage.alioss_access_key'));

// oss文件存储的bucket名称
if( !defined('ALI_BUCKET_NAME') ) {
    define('ALI_BUCKET_NAME', Configure::read('Storage.alioss_bucket'));
}

if( !defined('OSS_HOST_SERVER') ) {
    define('OSS_HOST_SERVER', Configure::read('Storage.alioss_host_server'));
}

if( !defined('OSS_DOMAIN_URL') ) {
    define('OSS_DOMAIN_URL', Configure::read('Storage.alioss_domain_url'));
}

//是否记录日志
define('ALI_LOG', FALSE);
//自定义日志路径，如果没有设置，则使用系统默认路径，在./logs/
//define('ALI_LOG_PATH','');
//是否显示LOG输出
define('ALI_DISPLAY_LOG', FALSE);
//语言版本设置
define('ALI_LANG', 'zh');



$GLOBALS['model_behaviors'] = array();
/* 内容一一对应的多语言模块 */

// I18nfield 在这里会有问题，不设置。 在模型的初始化时，会调用I18nfield模型查询模型的字段。出现嵌套造成错误。
/*
$GLOBALS['model_behaviors']['I18nfield']['MultiTranslate'] = array('translate',);
$GLOBALS['model_behaviors']['Menu']['MultiTranslate'] = array('name',);
*/
/* 内容互相对立的多语言模块 */
/*
$GLOBALS['model_behaviors']['Article']['IndLang'] = array();
$GLOBALS['model_behaviors']['Category']['IndLang'] = array();

*/

$GLOBALS['currency'] = array(
		'AUD'=>'$',
		'CNY'=>'￥',
);