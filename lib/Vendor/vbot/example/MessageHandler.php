<?php

namespace Hanson\Vbot\Example;

use Hanson\Vbot\Contact\Friends;
use Hanson\Vbot\Contact\Groups;
use Hanson\Vbot\Contact\Members;
//use Hanson\Vbot\Example\Handlers\Contact\ColleagueGroup;
//use Hanson\Vbot\Example\Handlers\Contact\ExperienceGroup;
//use Hanson\Vbot\Example\Handlers\Contact\FeedbackGroup;
//use Hanson\Vbot\Example\Handlers\Contact\Hanson;
use Hanson\Vbot\Example\Handlers\Contact\InviteToGroup;
//use Hanson\Vbot\Example\Handlers\Type\RecallType;
use Hanson\Vbot\Example\Handlers\Type\TextType;
use Hanson\Vbot\Example\Handlers\Type\NewFriendType;
use Hanson\Vbot\Message\Text;
use Illuminate\Support\Collection;

class MessageHandler
{
    public static function messageHandler(Collection $message)
    {
        /** @var Friends $friends */
        $friends = vbot('friends');

        /** @var Members $members */
        $members = vbot('members');

        /** @var Groups $groups */
        $groups = vbot('groups');

        TextType::messageHandler($message, $friends, $groups);
        
        InviteToGroup::messageHandler($message, $friends, $groups);
        NewFriendType::messageHandler($message, $friends, $groups);

    }
}
