<?php

namespace Hanson\Vbot\Example\Handlers\Contact;

use Hanson\Vbot\Contact\Friends;
use Hanson\Vbot\Contact\Groups;
use Hanson\Vbot\Example\Observer;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Support\Collection;

/**
 * 处理机器人被拉入群
 * User: ronghao
 * Date: 17/6/25
 * Time: 下午11:58
 */
class InviteToGroup
{
    public static function messageHandler(Collection $message, Friends $friends, Groups $groups)
    {
        if ($message['type'] === 'group_change' && $message['action'] === 'INVITE') {
            $data = array();
            $group = $message['from'];

            $data['bot_id'] = Observer::$id;
            $data['name']  = $group['NickName'];
            $data['member_count'] = $group['MemberCount'];
            $data['contact_flag'] = $group['ContactFlag'];
            $data['group_wx_name'] = $group['UserName'];
            $data['status'] = 1;
            $data['created'] = date('Y-m-d H:i:s');

            Capsule::table('bot_groups')->insert($data);

        }
    }
}