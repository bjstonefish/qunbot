<?php

namespace Hanson\Vbot\Example\Handlers\Type;

use Hanson\Vbot\Contact\Friends;
use Hanson\Vbot\Contact\Groups;
use Hanson\Vbot\Message\Text;
use Hanson\Vbot\Message\Image;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Support\Collection;

class TextType
{
    private static $addUrl = 'http://www.135plat.com/wx_activities/add?model=Baoming&theme=3g';
    private static $addShortUrl = 'http://t.cn/RoTWsKn';

    private static $listUrl = 'http://www.135plat.com/wx_activities/mine';
    private static $listShortUrl = 'http://t.cn/RoTWsps';

    private static $viewUrl = 'http://www.135plat.com/wx_activities/view/';

    private static $orderUrl = 'http://www.135plat.com/orders/mine';
    private static $orderShortUrl = 'http://t.cn/RoTjeEp';

    private static $groupUrl = 'http://t.cn/RKkX2Fy';
    private static $trainUrl = 'http://t.cn/R9X1sjv';

    private static $splitStr =  "\n"; //'-----------------------------' .
    public static $SINA_APP_KEY='203379639';

    public static function messageHandler(Collection $message, Friends $friends, Groups $groups)
    {
        $botInfo = vbot()::$botInfo;
        //echo var_export($message,true);
        if ($message['type'] === 'text') {
            $fromUserName = $message['from']['UserName'];
            $content = $message['content'];

            if ($message['fromType'] === 'Friend') {
                if ($content === '入群') {
                    if($botInfo && $botInfo->invite_group) {
                        $groupName = $groups->getUsernameByNickname( $botInfo->invite_group );
                        $groups->addMember($groupName, $fromUserName);
                        return ;
                    }
                }

                $replyContent = static::reply($content, $fromUserName);
                if(!empty($replyContent)) {
                    Text::send($fromUserName, $replyContent);
                }
            }

            if ($message['fromType'] === 'Group' && $message['isAt']) {
                $myself = $groups->getVbot()->myself;
                static::replyByGroupAt($message, $myself);
            }
        }
    }


    private static function replyByGroupAt($message, $myself)
    {
        $fromGroupNickName = $message['from']['NickName'];
        $fromGroupUserName = $message['from']['UserName'];
        $content = $message['content'];
        //去掉@机器人
        $pureContent = str_replace("@" . $myself->nickname, "", $content);
        //微信里@nickname 后有一个3长度的字符，展示为空白
        $pureContent = substr($pureContent, 3);

        if (empty($pureContent)) { // 群消息没有消息内容时，设置 @at
            $pureContent = '@at';
        }
        $bot_id = vbot()::$id;
        $group_id = 0;
        if(!empty($message['from']['NickName'])) {
            $group = Capsule::table('bot_groups')->select(['id', 'creator','group_wx_name'])->where('bot_id',$bot_id)->where('name', $fromGroupNickName)->first();
            if( empty($group) ) {
                $arguments = vbot()::$runargs;
                $data = array();
                $data['bot_id'] = $bot_id;
                //creator 给到运营人员来手工填入
                $data['creator'] = $arguments['creator'];
                $data['name'] = $message['from']['NickName'];

                $data['member_count'] = $message['from']['MemberCount'];
                $data['contact_flag'] = $message['from']['ContactFlag'];
                if(isset($message['from']['IsOwner'])) {
                    $data['is_owner'] = $message['from']['IsOwner'];
                }
                if(isset($message['from']['RemarkName'])) {
                    $data['remark_name'] = $message['from']['RemarkName'];
                }
                $data['group_wx_name'] = $message['from']['UserName'];
                if(isset($message['from']['ChatRoomId'])) {
                    $data['slug'] = $message['from']['ChatRoomId'];
                }
                $data['status'] = 1;
                if(isset($message['sender']['NickName'])) {
                    $data['username'] = $message['sender']['NickName'];
                }
                $data['created'] = date('Y-m-d H:i:s');
                echo "auto add group:";print_r($data);
                try {
                    $group_id = Capsule::table('bot_groups')->insertGetId($data);
                }catch (Exception $e){
                    return;
                }
            }
            elseif ( !empty($group) && !empty($group->creator) ) {
                $group_id = $group->id;
                if( $group->group_wx_name != $message['from']['UserName'] ){ //已失效的更新username
                    Capsule::table('bot_groups')->where( 'id', $group_id )->update(array('group_wx_name'=>$message['from']['UserName']));
                }
            }
        }

        $replyContent = static::reply($pureContent, $fromGroupUserName,true,$group_id);
        if( !empty($replyContent) ) {
            Text::send($fromGroupUserName, $replyContent);
        }
    }

    private static function reply($content, $fromGroupUserName , $tuling = false,$group_id = 0)
    {
        $botInfo = vbot()::$botInfo;
        $bot_id = $bot_id = vbot()::$id;
        if($botInfo && $botInfo->autoreply_wxids){
            echo $url = 'http://www.135plat.com/weixin/getReply?ids='.$botInfo->autoreply_wxids.'&bot_id='.$bot_id.'&group_id='.$group_id.'&content='.urlencode($content);
            $result = static::curlQuery($url);
            $json = json_decode($result,true);
            print_r($json);
            if($json && $json['ret'] == 0) {
                if(!empty($json['msg']['text'])){
                    Text::send($fromGroupUserName, implode("\n\n",$json['msg']['text']));
                }
                if(!empty($json['msg']['link'])){
                    Text::send($fromGroupUserName, implode("\n\n",$json['msg']['link']));
                }
                if(!empty($json['msg']['image'])){
                    foreach($json['msg']['image'] as $img) {
                        $imgcontent = vbot('http')->get($img);
                        $imgfile = __DIR__.'/../../../tmp/img_'.md5($img);
                        file_put_contents($imgfile,$imgcontent);
                        Image::send($fromGroupUserName, $imgfile);
                    }
                }
            }
        }

        /*if($tuling) {
            try {
                $result = vbot('http')->post('http://www.tuling123.com/openapi/api', [
                    'key' => '2ff66644006543649c5a01355d836b7d',
                    'info' => $content,
                    'userid' => $fromGroupUserName,
                ], true);
                return isset($result['url']) ? $result['text'] . $result['url'] : $result['text'];
            } catch (\Exception $e) {
                return null;
            }
        }*/
        return null;
    }

    //根据长网址获取短网址
    static function sinaShortenUrl($long_url)
    {
        //拼接请求地址，此地址你可以在官方的文档中查看到
        $url = 'http://api.t.sina.com.cn/short_url/shorten.json?source=' . self::$SINA_APP_KEY . '&url_long=' . $long_url;
        //获取请求结果
        $result = static::curlQuery($url);
        //下面这行注释用于调试，
        //print_r($result);exit();
        //解析json
        $json = json_decode($result);
        //异常情况返回false
        if (isset($json->error) || !isset($json[0]->url_short) || $json[0]->url_short == '')
            return $long_url;
        else
            return $json[0]->url_short;
    }

    static function curlQuery($url)
    {
        //设置附加HTTP头
        $addHead = array(
            "Content-type: application/json"
        );
        //初始化curl，当然，你也可以用fsockopen代替
        $curl_obj = curl_init();
        //设置网址
        curl_setopt($curl_obj, CURLOPT_URL, $url);
        //附加Head内容
        curl_setopt($curl_obj, CURLOPT_HTTPHEADER, $addHead);
        //是否输出返回头信息
        curl_setopt($curl_obj, CURLOPT_HEADER, 0);
        //将curl_exec的结果返回
        curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, 1);
        //设置超时时间
        curl_setopt($curl_obj, CURLOPT_TIMEOUT, 15);
        //执行
        $result = curl_exec($curl_obj);
        //关闭curl回话
        curl_close($curl_obj);
        return $result;
    }
}
