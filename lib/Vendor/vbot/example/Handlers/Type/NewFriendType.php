<?php
/**
 * Created by PhpStorm.
 * User: ronghao
 * Date: 17/6/26
 * Time: 下午3:50
 */

namespace Hanson\Vbot\Example\Handlers\Type;

use Hanson\Vbot\Contact\Friends;
use Hanson\Vbot\Contact\Groups;
use Hanson\Vbot\Message\Text;
use Illuminate\Support\Collection;
use Illuminate\Database\Capsule\Manager as Capsule;
use Hanson\Vbot\Example\Observer;

class NewFriendType
{
    public static function messageHandler(Collection $message, Friends $friends, Groups $groups)
    {
        //默认同意好友请求
        if ($message['type'] === 'request_friend') {
            // 同意添加好友
            $friends->approve($message);
        }
        //加为好友后，发送欢迎信息，并拉到官方群
        //TODO 执行了两次：一次对方加机器人，一次机器人同意对方为好友ß
        if ($message['type'] === 'new_friend') {
            $data = array();
            $friend = $message['from'];

            $data['bot_id'] = Observer::$id;
            $data['nickname']  = $friend['NickName'];
            $data['username'] = $friend['UserName'];
            $data['created'] = date('Y-m-d H:i:s');

            Capsule::table('bot_friends')->insert($data);

            $botInfo = vbot()::$botInfo;
            if($botInfo && $botInfo->new_friend_msg){
                Text::send($friend['UserName'],$botInfo->new_friend_msg);
            }
            //Text::send($friend['UserName'],'亲，终于等到您。微分享助手：国内最好的微信群接龙工具 ！回复：入群。马上入群领养我哦。');
        }

        if ($message['from']['NickName'] === '微分享助手官方群') {
            $rule = "群规：\n".
                "首先感谢大家对微分享助手的信任，微分享助手：国内最好的微信群接龙工具 ！\n".
                "这里可以询问微分享助手的事情，包括如何使用和使用中存在的问题以及对未来的改进建议。\n".
                "本群禁止一切广告，一旦发现立刻踢出，谢谢配合。";


            if ($message['content'] === '群规') {
                Text::send($message['from']['UserName'], $rule);
            }

            if ($message['type'] === 'group_change') {
                if ($message['action'] === 'ADD') {
                    Text::send($message['from']['UserName'], '欢迎新人 '.$message['invited']);
                    Text::send($message['from']['UserName'], $rule);
                }
            }

        }
    }
}