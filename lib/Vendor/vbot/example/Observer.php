<?php

namespace Hanson\Vbot\Example;

//use Hanson\Vbot\Support\File;
use Illuminate\Database\Capsule\Manager as Capsule;


### 根据runcode定为一次执行的sql条件
### php example\run.php --session=bef551 --serverip=127.0.0.1 --port=8866 --creator=1 --runcode=xyzsadfsfd

class Observer
{
    public static $id;

    public static function setQrCodeObserver($qrCodeUrl)
    {
        $uuid = str_replace('https://login.weixin.qq.com/l/','',$qrCodeUrl);
        vbot('console')->log('qrCodeUrl:'.$qrCodeUrl, 'Observer');

        $arguments = getopt(null,['session::','serverip::','port::','runcode::','creator::']);
        $data = array();
        $data['serverip'] = $arguments['serverip'];
        $data['port'] = $arguments['port'];
        $data['pid'] = getmypid();
        $data['runcode'] = $arguments['runcode'];
        $data['creator'] = $arguments['creator'];
        $data['uuid'] = $uuid;
        $data['online_status'] = 0;
        $data['qr_time'] = date('Y-m-d H:i:s');

        vbot()::$runargs = $arguments;

        $bot = Capsule::table('bots')->where('runcode',$data['runcode'])->where('creator',$data['creator'])->first();
        if( !empty($bot) ) {
            vbot()::$id = static::$id = $bot->id;
            Capsule::table('bots')->where( 'id', $bot->id )->update($data);
        }
        else{
            $data['created'] = date('Y-m-d H:i:s');
            vbot()::$id = static::$id = Capsule::table('bots')->insertGetId($data);
        }

        echo "BotId:".static::$id;
    }

    public static function setLoginSuccessObserver()
    {
        $arguments = getopt(null,['session::','serverip::','port::','runcode::','creator::']);

        // vbot()->config
        $data = array();
        $data['name'] = vbot('myself')->nickname;

        $data['serverip'] = $arguments['serverip'];
        $data['port'] = $arguments['port'];
        $data['pid'] = getmypid();
        $data['runcode'] = $arguments['runcode'];
        $data['creator'] = $arguments['creator'];

        $data['online_status'] = 1; //成功登录
        $data['login_time'] = date('Y-m-d H:i:s'); 

        //$config = vbot()->config;
        //vbot('console')->log( 'config:'.var_dump($config,true) , 'Config' );

        $data['session'] = vbot()->config['session'];
        $data['uin'] = vbot()->config['server.uin'];

        $uinbot = Capsule::table('bots')->where('uin',$data['uin'])->first();
        if( empty($uinbot) || $uinbot->runcode == $data['runcode'] ) { //bot 没有登录过，或以原有runcode登录
            Capsule::table('bots')
            ->where('runcode',$data['runcode'])->where('creator',$data['creator'])
            ->update($data);

        }
        else{ // bot登录过，但又错误的使用了新建账号登录,有了新的runcode.
            vbot()::$id = static::$id = $uinbot->id;
            vbot()::$botInfo = $uinbot;

            //删除新产生的空记录，关联到旧记录，并关闭旧记录的进程。
            Capsule::table('bots')
            ->where('runcode',$data['runcode'])->where('creator',$data['creator'])->delete();

            if( $uinbot->pid ) {
                system('kill '.$uinbot->pid);
            }

            Capsule::table('bots')->where( 'id', $uinbot->id )->update($data);
        }
        
        //$data['uin'] = $config->server['uin'];
        vbot('console')->log( 'data:'.var_export($data,true) , 'data' );
        echo "BotId:".static::$id;
        
    }

    public static function setReLoginSuccessObserver()
    {
        $arguments = getopt(null,['session::','serverip::','port::','runcode::','creator::']);

        vbot()::$runargs = $arguments;

        $data = array();
        $data['serverip'] = $arguments['serverip'];
        $data['port'] = $arguments['port'];
        $data['session'] = $arguments['session'];
        $data['pid'] = getmypid();

        $data['online_status'] = 1; //成功登录
        $data['login_time'] = date('Y-m-d H:i:s');

        $bot = Capsule::table('bots')
            ->where('session',$arguments['session'])->where('creator',$arguments['creator'])->first();

        vbot()::$botInfo = $bot;
        vbot()::$id = static::$id = $bot->id;
        echo "BotId:".static::$id;

        Capsule::table('bots')
            ->where('session',$arguments['session'])->where('creator',$arguments['creator'])->update($data);

        vbot('console')->log('skip qrcode relogin success', 'Observer');        
    }

    public static function setExitObserver()
    {
        $arguments = getopt(null,['session::','serverip::','runcode::','creator::']);
       
        $data = array();
        $data['online_status'] = 0; //退出登录
        $data['login_time'] = date('Y-m-d H:i:s');

        Capsule::table('bots')
            ->where('runcode',$arguments['runcode'])->where('creator',$arguments['creator'])
            ->update($data);

        vbot('console')->log('exit', 'Observer');
    }

    public static function setFetchContactObserver(array $contacts)
    {
        $arguments = getopt(null,['session::','serverip::','runcode::','creator::']);        
        vbot('console')->log('fetch friends', 'Observer');
        //print_r(array_keys($contacts));
        //File::saveTo(__DIR__.'/../tmp/group_'.$arguments['runcode'].'.json', $contacts['groups']);
        //clear friends table
        Capsule::table('bot_friends')->where('bot_id',static::$id)->delete();

        foreach($contacts['groups'] as $key  => $group) {
            unset($group['MemberList']);
            if(empty($group['NickName'])  ){
                continue; //没有设置群名字的跳过. && empty($group['RemarkName'])
            }
            $botgroup = Capsule::table('bot_groups')->where('bot_id',static::$id)->where('name',$group['NickName'])->first();
            if( empty($botgroup) ) {
                $data = array();
                $data['bot_id'] = static::$id;
                //creator 给到运营人员来手工填入
//                $data['creator'] = $arguments['creator'];
                $data['name'] = $group['NickName'];
                $data['member_count'] = $group['MemberCount'];
                $data['contact_flag'] = $group['ContactFlag'];
                $data['is_owner'] = $group['IsOwner'];
                $data['remark_name'] = $group['RemarkName'];
                $data['group_wx_name'] = $group['UserName'];
                $data['slug'] = $group['ChatRoomId'];
                $data['status'] = 1;
                $data['created'] = date('Y-m-d H:i:s');
                print_r($data);
               try {
                   Capsule::table('bot_groups')->insert($data);
               }catch (Exception $e){
                    continue;
               }
            }else{
                Capsule::table('bot_groups')->where('bot_id',static::$id)->where('name',$group['NickName'])
                    ->update(['group_wx_name'=>$group['UserName']]);
            }
        }
//         $content = var_export($contacts,true);
//         File::saveTo(__DIR__.'../tmp/contacts_'.$arguments['runcode'].'.json',$content );
    }

    public static function setBeforeMessageObserver()
    {
        vbot('console')->log('I am ready', 'Observer');
    }

    public static function setNeedActivateObserver()
    {
        vbot('console')->log('need activate.', 'Observer');
    }
}
