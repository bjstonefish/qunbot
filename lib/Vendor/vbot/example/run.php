<?php

namespace Hanson\Vbot\Example;

require __DIR__.'/../vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;
$capsule = new Capsule;

$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => '127.0.0.1',
    'database'  => 'miaocms',
    'username'  => 'root',
    'password'  => 'xsdfuh2&32sdw!3S#sd',
    'charset'   => 'utf8mb4',
    'collation' => 'utf8mb4_unicode_ci',
    'prefix'    => 'miao_',
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$vbot = new Example();

$vbot->run();
