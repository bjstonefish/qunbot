<?php

namespace Hanson\Vbot\Core;

use Hanson\Vbot\Foundation\Vbot;
use Swoole\Process;
use Swoole\Server as SwooleServer;

class Swoole
{
    /**
     * @var Vbot
     */
    private $vbot;
    private $serverPid;

    public function __construct(Vbot $vbot)
    {
        $this->vbot = $vbot;
    }

    public function run()
    {

        $arguments = getopt(null,['serverip::','port::']);
        $serverip = $arguments['serverip'];
        $port = $arguments['port'];

        $server = new SwooleServer($serverip, $port);
        $server->set(array(
            'daemonize' => $this->vbot->config->get('swoole.daemonize', false),
            'log_file' => $this->vbot->config->get('swoole.log_file', '/tmp/swoole.log'),
            'log_level' => $this->vbot->config->get('swoole.log_level', 0),
        ));

        $server->on('receive', function (SwooleServer $server, $fd, $from_id, $data) {
            $response = $this->vbot->api->handle($data);

            $response = $this->makeResponse($response);

            $server->send($fd, $response);
        });

        $handleProcess = new Process(function ($worker) use(&$server) {
            $server->start();
        });

        $this->serverPid = $handleProcess->start();

        $this->vbot->messageHandler->listen($server);

        exit;
    }

    private function makeResponse($data)
    {
        $data = json_encode($data);

        $headers = [
            'Server'         => 'Swoole',
            'Content-Type'   => 'application/json',
            'Content-Length' => strlen($data),
        ];

        $response[] = 'HTTP/1.1 200';

        foreach ($headers as $key => $val) {
            $response[] = $key.':'.$val;
        }

        $response[] = '';
        $response[] = $data;

        return implode("\r\n", $response);
    }
}
