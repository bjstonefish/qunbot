<?php

namespace Hanson\Vbot\Core;

use Carbon\Carbon;
use Hanson\Vbot\Exceptions\ArgumentException;
use Hanson\Vbot\Foundation\Vbot;
use Hanson\Vbot\Message\Text;
use Illuminate\Support\Collection;
use Illuminate\Database\Capsule\Manager as Capsule;

class MessageHandler
{
    /**
     * @var Vbot
     */
    protected $vbot;

    protected $handler;

    public function __construct(Vbot $vbot)
    {
        $this->vbot = $vbot;
    }

    public function listen($server = null)
    {
        $this->vbot->beforeMessageObserver->trigger();

        $time = 0;

        while (true) {

            if ( !($checkSync = $this->checkSync()) ) {
                $time = $this->heartbeat($time); //获取消息没有返回时，才进行心跳
                continue;
            }

            if ( time() - $time > 300 ) {
                if( vbot()::$id ) {
                    //每次心跳时，更新机器人的配置信息。
                    vbot()::$botInfo = Capsule::table('bots')->where('id',vbot()::$id)->first();
                }
                $time = time();
            }

            if ( !$this->handleCheckSync($checkSync[0], $checkSync[1]) ) {
                if ($server) {
                    $server->shutdown();
                } else {
                    break;
                }
            }
        }
    }

    /**
     * make a heartbeat every 5 minutes.
     *
     * @param $time
     *
     * @return int
     */
    private function heartbeat($time)
    {
        if ( time() - $time > 300 ) {

            $friends = vbot('friends');
            $username = $friends->getUsernameByNickname( '135助手' );
            Text::send($username, '你好，在吗？');
            if( vbot()::$id ) {
                //每次心跳时，更新机器人的配置信息。
                vbot()::$botInfo = Capsule::table('bots')->where('id',vbot()::$id)->first();
            }
            return time();
        }
        return $time;
    }

    private function checkSync()
    {
        return $this->vbot->sync->checkSync();
    }

    /**
     * handle a sync from wechat.
     *
     * @param $retCode
     * @param $selector
     * @param bool $test
     *
     * @return bool
     */
    public function handleCheckSync($retCode, $selector, $test = false)
    {
        if (in_array($retCode, [1100, 1101, 1102, 1205])) { // 微信客户端上登出或者其他设备登录

            $this->vbot->console->log('vbot exit normally.');

            return false;
        } elseif ($retCode != 0) {
            $this->vbot->needActivateObserver->trigger();
            return true;
        } else {
            if (!$test) {
                $this->handleMessage($selector);
            }
            return true;
        }
    }

    /**
     * 处理消息.
     *
     * @param $selector
     */
    private function handleMessage($selector)
    {
        if ($selector == 0) {
            return;
        }

        $message = $this->vbot->sync->sync();

        $this->log($message);

        $this->storeContactsFromMessage($message);

        if ($message['AddMsgList']) {
            foreach ($message['AddMsgList'] as $msg) {
                $collection = $this->vbot->messageFactory->make($msg);
                if ($collection) {
                    $this->console($collection);
                    //$this->cache($msg, $collection);
                    if ($this->handler) {
                        call_user_func_array($this->handler, [$collection]);
                    }
                }
            }
        }
    }

    /**
     * log the message.
     *
     * @param $message
     */
    private function log($message)
    {
        if ($this->vbot->messageLog && ($message['ModContactList'] || $message['AddMsgList'])) {
            $this->vbot->messageLog->info(json_encode($message));
        }
    }

    private function console(Collection $collection)
    {
        $logtxt = 'fromType:'.$collection['fromType'];
        $logtxt .= "\r\nfrom NickName:".$collection['from']['NickName'];
        $logtxt .= "\r\nfrom UserName:".$collection['from']['UserName'];
        if(isset($collection['from']['PYQuanPin'])){
            $logtxt .= "\r\nfrom PYQuanPin:".$collection['from']['PYQuanPin'];
        }
        if(isset($collection['from']['ChatRoomId'])) {
            $logtxt .= "\r\nChatRoomId:".$collection['from']['ChatRoomId'];
        }
        if(isset($collection['from']['EncryChatRoomId'])) {
            $logtxt .= "\r\nEncryChatRoomId:" . $collection['from']['EncryChatRoomId'];
        }
        if(isset($collection['from']['ContactFlag'])) {
            $logtxt .= "\r\nfrom ContactFlag:" . count($collection['from']['ContactFlag']);
        }
        if(isset($collection['from']['MemberCount'])) {
            $logtxt .= "\r\nMemberCount:" . $collection['from']['MemberCount'];
        }

        if( !empty($collection['sender']) ) {
            $logtxt .= "\r\nSender NickName:".$collection['sender']['NickName'];
            $logtxt .= "\r\nSender UserName:".$collection['sender']['UserName'];
            if(isset($collection['sender']['PYQuanPin'])) {
                $logtxt .= "\r\nSender PYQuanPin:" . $collection['sender']['PYQuanPin'];
            }
        }
        $this->vbot->console->log($logtxt."\r\nContent:".var_export($collection['content']."\r\n",true));

//        $this->vbot->console->log(var_export($collection,true));
    }

    private function storeContactsFromMessage($message)
    {
        if (count($message['ModContactList']) > 0) {
            $this->vbot->contactFactory->store($message['ModContactList']);
        }
    }

    private function cache($msg, Collection $collection)
    {
        $this->vbot->cache->put('msg-'.$msg['MsgId'], $collection->toArray(), 2);
    }

    /**
     * set a message handler.
     *
     * @param $callback
     *
     * @throws ArgumentException
     */
    public function setHandler($callback)
    {
        if (!is_callable($callback)) {
            throw new ArgumentException('Argument must be callable in '.get_class());
        }

        $this->handler = $callback;
    }
}
