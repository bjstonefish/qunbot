<?php

namespace Hanson\Vbot\Api;

use Hanson\Vbot\Message\Traits\SendAble;
use Illuminate\Database\Capsule\Manager as Capsule;
use Hanson\Vbot\Example\Observer;

class Send extends BaseApi
{

    public function needParams(): array
    {
        return ['contact-type', 'nickname', 'message-type', 'content'];
    }

    /**
     * @param $params
     * @return array
     */
    public function handle($params): array
    {
        /** @var SendAble $messageClass */
        $contactClass = '\\Hanson\\Vbot\\Contact\\' . ucfirst($params['contact-type']);
        $messageClass = '\\Hanson\\Vbot\\Message\\' . ucfirst($params['message-type']);

        if (!class_exists($contactClass)) {
            return $this->response('Class: ' . $contactClass . ' not exist.', 500);
        }

        if (!class_exists($messageClass)) {
            return $this->response('Class: ' . $messageClass . ' not exist.', 500);
        }

        $contactType = strtolower($params['contact-type']);
        $contacts = $this->vbot->$contactType;
        $sendUserName = call_user_func_array([$contacts, 'getUsernameByNickname'], [$params['nickname'], false]);

        if (empty($sendUserName)) {
            $botId = Observer::$id;

            if ($contactType == 'groups') {
                $botGroup = Capsule::table('bot_groups')->where('bot_id', $botId)
                    ->where('name', $params['nickname'])->first();
                if (!empty($botGroup))
                    $sendUserName = $botGroup->group_wx_name;
            }elseif ($contactType == 'friends'){
                $botFriend = Capsule::table('bot_friends')->where('bot_id', $botId)
                    ->where('nickname', $params['nickname'])->first();
                if (!empty($botFriend)){
                    $sendUserName = $botFriend->username;
                }
            }
        }

        if (!method_exists(new $messageClass(), 'send')) {
            return $this->response('Class: ' . $messageClass . ' doesn\'t support send.', 500);
        }

        if( $params['message-type'] == 'text' ) {
            $sendParams = array($sendUserName,$params['content']);
        }
        else{
            $sendParams = array_merge([$sendUserName], explode(',', $params['content']));
        }
        return $this->response($messageClass::send(...$sendParams), 200);
    }
}
