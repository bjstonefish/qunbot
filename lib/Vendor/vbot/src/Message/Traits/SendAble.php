<?php

namespace Hanson\Vbot\Message\Traits;

use Hanson\Vbot\Core\ApiExceptionHandler;
use Hanson\Vbot\Message\Text;

if(!function_exists('safe_json_encode')) {

    function safe_json_encode($value, $options = 0, $depth = 512) {
        $encoded = json_encode($value, $options, $depth);
        if ($encoded === false && $value && json_last_error() == JSON_ERROR_UTF8) {
            $encoded = json_encode(utf8ize($value), $options, $depth);
        }
        return $encoded;
    }

    function utf8ize($mixed) {
        if (is_array($mixed)) {
            foreach ($mixed as $key => $value) {
                $mixed[$key] = utf8ize($value);
            }
        } elseif (is_string($mixed)) {
            return mb_convert_encoding($mixed, "UTF-8", "UTF-8");
        }
        return $mixed;
    }
}


/**
 * Trait SendAble.
 */
trait SendAble
{
    protected static function sendMsg($msg)
    {
        $data = [
            'BaseRequest' => vbot('config')['server.baseRequest'],
            'Msg'         => $msg,
            'Scene'       => 0,
        ];

        //json_encode
        $post_data = safe_json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        $url = static::getUrl();
        $result = vbot('http')->post($url,$post_data, true);

        //vbot('console')->log('url:'.$url."\r\npost data:".$post_data, 'Http');
        vbot('messageLog')->info('url:'.$url."\r\npost data:".$post_data."\r\n".var_export($result,true));

        static::stopSync();

        sleep(1);

        return ApiExceptionHandler::handle($result);
    }

    private static function getUrl()
    {
        return vbot('config')['server.uri.base'].DIRECTORY_SEPARATOR.static::API.'pass_ticket='.vbot('config')['server.passTicket'];
    }

    private static function stopSync()
    {
        if (get_class(new static()) != Text::class) {
            Text::send('filehelper', 'stop sync');
        }
    }

    abstract public static function send(...$args);
}
