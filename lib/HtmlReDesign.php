<?php
class HtmlReDesign{

	/**
	 * 去除圆角
	 * @param $html
	 */
	public static function strip_radius($html){
		return preg_replace('/border(-.*)?-radius\s*:.*?[;|"|\']/is', '', $html);
	}
	
	public static function strap_shadow($html){
		$html =  preg_replace('/box-shadow\s*:.*?[;|"|\']/is', '', $html);
		return preg_replace('/text-shadow\s*:.*?[;|"|\']/is', '', $html);
	}
	public static function strip_border($content){
		$html = str_get_html($content);
		//TODO. 是否保留script，保留时，相对地址的调用改成绝对地址调用。
		//TODO. clear方法有问题，删除节点时，结尾标记删除失败,造成页面错乱。如</div>  </script>
		foreach($html->childNodes() as $item){
			$style = $item->getAttribute('style');
			if(preg_match('/[\"|\'|;]\s*border/is',$style)){
				$has_background = true;
				foreach($item->childNodes() as $inner){
					$inner_style = $inner->getAttribute('style');
					if(!strpos(strtolower($inner_style),'background')){
						$has_background = false;
					}
				}
				if($has_background){ //都有背景色时替换掉边框
					$style = preg_replace('/border(-\S+)?\s*:.+?[\"|\'|;]/is','',$style);
					$item->setAttribute('style',$style);
				}
			}
		}
		
		return $content = $html->outertext;
	}
	
	
}