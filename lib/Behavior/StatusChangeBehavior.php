<?php

/**
 * Records changes made to an object during save operations.
 */
class StatusChangeBehavior extends ModelBehavior {
  /**
   * A copy of the object as it existed prior to the save. We're going
   * to store this off so we can calculate the deltas after save.
   *
   * @var   Object
   */
  private $_original = array();

  public $settings = array(
  		'Tasking'=>array(
  			'10_11' => array('title'=> 'Bid {$name} failed','message'=>'bid <a href="/tasks/view/{id}" target="_blank">{$name}</a> failed.'),
  			'10_13' => array('title'=> 'Bid {$name} success','message'=>'bid <a href="/tasks/view/{id}" target="_blank">{$name}</a> success.'),
  			'16' => array('title'=> 'Task {$name} confirm finished.','message'=>'Task <a href="/tasks/view/{id}" target="_blank">{$name}</a> confirm finished.'),
  		),
  		'Project'=>array(
  			
  		),
  );
  public function setup( Model $Model, $settings = array() ) {
    if( !isset( $this->settings[$Model->alias] ) ) {
    	return true;
    }
    if( !is_array( $settings ) ) {
      $settings = array();
    }
    $this->settings[$Model->alias] = array_merge_recursive( $this->settings[$Model->alias], $settings );

   
  }

  /**
   * Executed before a save() operation.
   *
   * @return  boolean
   */
  public function beforeSave( Model $Model ) {
  	if( !isset( $this->settings[$Model->alias] ) ) {
  		return true;
  	}
    if( !empty( $Model->id )) {
      $this->_original[$Model->alias] = $Model->find('first', array(
  			'conditions' => array( $Model->alias . '.' . $Model->primaryKey => $Model->id ),
      		'recursive'=> -1,
  		));
    }
    else{
    	$this->_original[$Model->alias] = null;
    }
    return true;
  }

  public function afterSave( Model $Model, $created ) {
  	if( !isset( $this->settings[$Model->alias] ) ) {
  		return true;
  	}
  	$data = $Model->find('first', array(
  			'conditions' => array( $Model->alias . '.' . $Model->primaryKey => $Model->id ),
  			'recursive'=> -1,
  	));
  	if(empty($this->_original[$Model->alias]['status'])){
  		$this->_original[$Model->alias]['status'] = 0;
  	}
  	if(empty($data[$Model->alias]['status'])){
  		$data[$Model->alias]['status'] = 0;
  	}
  	
  	$status_index = $this->_original[$Model->alias]['status'].'_'.$data[$Model->alias]['status'];
  	$msg = false;
  	if(isset($this->settings[$Model->alias][$status_index] )){
  		//状态发生了变化。
  		$msg = $this->settings[$Model->alias][$status_index];
  	}
  	elseif(isset($this->settings[$Model->alias][ $data[$Model->alias]['status'] ] )) {
  		$msg = $this->settings[$Model->alias][ $data[$Model->alias]['status'] ];
  	}
  	if(empty($msg)){
  		return true;
  	}
  	
  	$receiver_id = 0;
  	if(isset($msg['receiver'])){
  		$receiver_id = $data[$Model->alias][$msg['receiver']];
  	}
  	elseif($data[$Model->alias]['user_id']){
  		$receiver_id = $data[$Model->alias]['user_id'];
  	}
  	elseif($data[$Model->alias]['creator']){
  		$receiver_id = $data[$Model->alias]['creator'];
  	}
  	
  	if($receiver_id){
  		foreach($data[$Model->alias] as $key => $value){
  			if(strpos($msg['title'],'{'.$key.'}')!==false){
  				$msg['title'] = str_replace($msg['title'],'{'.$key.'}',$value);
  			}
  			if(strpos($msg['message'],'{'.$key.'}')!==false){
  				$msg['message'] = str_replace($msg['message'],'{'.$key.'}',$value);
  			}
  		}
  		$ShortMessage = ClassRegistry::init( 'Shortmessage' );
  		$ShortMessage->create();
  		$ShortMessage->save(array(
  				'msgfromid' => 0,
  				'receiverid' => $receiver_id,
  				'title' => $msg['title'],
  				'message' => $msg['message'],
  		));
  	}
    
  	if( isset($this->_original) ) {
      unset( $this->_original[$Model->alias] );
    }
    
    return true;    
	
  }
}
