<?php


class VersionControlBehavior extends ModelBehavior {
  
  public function beforeSave( Model $Model ) {
    # If we're editing an existing object, save off a copy of
    # the object as it exists before any changes.
    if( !empty( $Model->id ) ) {
      $this->_original[$Model->alias] = $this->_getModelData( $Model );
    }
    
    return true;
  }
  
  /**
   * function afterSave
   * Executed after a save operation completes.
   *
   * @param   $created  Boolean. True if the save operation was an
   *                    insertion. False otherwise.
   * @return  void
   */
  public function afterSave( Model $Model, $created ) {
  
  	if($Model->id){
  		$vesion_c = loadModelObject('VersionControl');
  		$vesion_c->primaryKey = 'version_id';
  		$vesion_c->create();
  		$vd = array(
  				'data_id' => $Model->id,
  				'model' => $Model->name,
  				'content' => serialize($Model->data),
  		);
  		$vesion_c->save($vd);
  	}
    return true;    
  }
  
}
