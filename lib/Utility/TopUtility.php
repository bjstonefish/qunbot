<?php

/**
 * 第三方登录操作
 * @author Arlon
 *
 */
class TopUtility {
    public $c;
    public $sessionKey;

    public function TopUtility($appkey,$secret,$sessionKey){
        $this->c = new TopClient;
        $this->c->appkey = $appkey;
        $this->c->secretKey = $secret;
        $this->sessionKey = $sessionKey;
	echo "========================";
    }

    /***
     * taobao.crm.group.add 卖家创建一个分组
     */
    public function addGroup($name){        
        $req = new CrmGroupAddRequest;
        $req->setGroupName($name);
        $resp = $this->c->execute($req, $this->sessionKey);
        print_r($resp);
    }

    /***
     * taobao.crm.groups.get 查询卖家的分组,分页显示
     */
    public function getGroupList($page=1,$page_size=100){
        $req = new CrmGroupsGetRequest;
        $req->setPageSize($page_size);
        $req->setCurrentPage($page);
        $resp = $this->c->execute($req, $this->sessionKey);
        print_r($resp);
    }

    /***
     * taobao.crm.member.group.get 获取买家身上的标签
     */
    public function getMemberGroup($nick){
        $req = new CrmMemberGroupGetRequest;
        $req->setBuyerNick($nick);
        $resp = $this->c->execute($req, $this->sessionKey);
        print_r($resp);
    }
    
    /***
     * taobao.crm.grademkt.member.add 会员等级营销-会员吸纳
     */
    public function addMember($nick){
        $req = new CrmGrademktMemberAddRequest;
        $req->setParameter('{"module":{"buyer_nick":"'.$nick.'"}}');
        $req->setFeather('{"value":"1"}');
        $req->setBuyerNick($nick);
        $resp = $this->c->execute($req, $this->sessionKey);
        print_r($resp);
    }

}
