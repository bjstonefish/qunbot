<?php

App::uses('RequestFacade', 'Network');

/**
 * 微信相关操作
 * 扩展的Utility类名后面都加上Utility，防止类名与Model等其它类重名
 * @author Arlon
 *
 */
class WeiboUtility {
	
	public static $appId = '';
	public static $secretKey = '';

	public static $SINA_APP_KEY = '203379639';
	
	public static function curlQuery($url) {
        //设置附加HTTP头
        $addHead = array(
            "Content-type: application/json"
        );
        $curl_obj = curl_init();
        //设置网址
        curl_setopt($curl_obj, CURLOPT_URL, $url);
        //附加Head内容
        curl_setopt($curl_obj, CURLOPT_HTTPHEADER, $addHead);
        //是否输出返回头信息
        curl_setopt($curl_obj, CURLOPT_HEADER, 0);
        //将curl_exec的结果返回
        curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, 1);
        //设置超时时间
        curl_setopt($curl_obj, CURLOPT_TIMEOUT, 15);
        //执行
        $result = curl_exec($curl_obj);
        //关闭curl回话
        curl_close($curl_obj);
        return $result;
    }

    //根据长网址获取短网址
    public static function sinaShortenUrl($long_url)
    {
        //拼接请求地址，此地址你可以在官方的文档中查看到
        $url = 'http://api.t.sina.com.cn/short_url/shorten.json?source=' . self::$SINA_APP_KEY . '&url_long=' . urlencode($long_url);
        //获取请求结果
        $result = self::curlQuery($url);
        //下面这行注释用于调试，
        //CakeLog::debug($result);
        //解析json
        $json = json_decode($result,true);
        
        //异常情况返回false
        if ( !empty($json) && isset($json[0]['url_short']) && !empty($json[0]['url_short']) )
            return $json[0]['url_short'];
        else
            return $long_url;
    }
	
	/**
	 * 增加永久图文
	 * @param unknown $articles
	 */
	public static function add_draft($article,$oauth) {
	
	    $access_token = $oauth['oauth_token']; //self::getAccessToken();
	
	    $i = 0;
	    do{
	        $article['content'] = rawurlencode($article['content']);
	        $article['access_token'] = $access_token;
	        $add_url = 'https://api.weibo.com/proxy/article/draft.json';
	        $response =  RequestFacade::post($add_url,$article);
	        $result = json_decode($response->body,true);
	        $i ++;
	    } while( empty($result) && $i < 3 );
	
	    return $result;
	}
	
	
	/**
	 * 增加永久图文
	 * @param unknown $articles
	 */
	public static function add_news($article,$oauth) {
		
		$access_token = $oauth['oauth_token']; //self::getAccessToken();
		
		$i = 0;
		do{
		    $article['content'] = rawurlencode($article['content']);
		    $article['access_token'] = $access_token;
    		$add_url = 'https://api.weibo.com/proxy/article/publish.json';
    		$response =  RequestFacade::post($add_url,$article);
    		$result = json_decode($response->body,true);
    		$i ++;
		} while( empty($result) && $i < 3 );
		
		return $result;
	}
	
	/**
	 * 编辑永久图文
	 * @param unknown $articles
	 */
	public static function update_news($media_id,$index,$article){
	    if(empty($access_token)){
	        $access_token = self::getAccessToken();
	    }
	    if(empty($index)) $index = 0;
	    $update_url = 'https://api.weixin.qq.com/cgi-bin/material/update_news?access_token='.$access_token;
	    $response =  RequestFacade::post($update_url,json_encode(array('media_id' =>$media_id, 'index' => $index,'articles'=>$article),JSON_UNESCAPED_UNICODE));
	    return json_decode($response->body,true);
	}

	
    /**
     * 获取微信的access_token
     * @param string $force 是否不使用缓存，强制获取
     * @return string 
     */
	public static function getAccessToken($force=false,$dev=false){
		
	    return false;
			// 直接通过公众号授权的公众号
			$obj = loadModelObject('Oauthbind');
			$user = CakeSession::read('Auth.User');
			if($user['id']) {
			    return false;
			}
			
			$oauthbind = $obj->find('first',array(
					'conditions' => array('source'=>'sina','user_id' => $user['id'] ),
			));
			
			if( !empty($oauthbind)){
			    
			   
			}
		
		return false;
	}
	

}
