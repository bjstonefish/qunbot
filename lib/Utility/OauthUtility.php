<?php

/**
 * 第三方登录操作
 * @author Arlon
 *
 */
class OauthUtility {

    public static function new_oauth($auth_info = array())
    {
        $oauth_user_nick = $auth_info['oauth_user_nick'];
        $oauth_user_id = $auth_info['oauth_user_id'];
        $access_token = $auth_info['access_token'];
        $expires = $auth_info['oauth_expires'];
        $refresh_token = $auth_info['refresh_token'];
        $auth_type = $auth_info['type'];
        
        $m_user = loadModelObject('User');
        $dbconfig = new DATABASE_CONFIG();
        $userinfo = $m_user->find('first',array(
            'conditions'=>array(),
            'recursive' => -1,
            'joins'=> array(                    
                        array(
                            'table' => Inflector::tableize('Oauthbind'),
                            'alias' => 'Oauthbind',  
                            'type' => 'inner',
                            'conditions' => array(
                                "Oauthbind.user_id = User.id",
                                "source" => $auth_type,
                                "Oauthbind.oauth_openid" => $oauth_user_id,
                            ),
                        ),
            ),
            'fields' => array('User.*','Oauthbind.*'),
        ));
        
                
        $current_time = $auth_info['oauth_timestamp'] ? date('Y-m-d H:i:s',$auth_info['oauth_timestamp']) : date('Y-m-d H:i:s');
        
        
        if(empty($userinfo)){
            $userinfo = array(
                'role_id'=>2,
                'username' => $oauth_user_nick,
                'password' => Security::hash(random_str(12), null, true),
                'nickname' => $oauth_user_nick,
                'screen_name' => $oauth_user_nick,
                'image' => $auth_info['oauth_user_avatar'],             
                'sex' => $gender,
                'location' => $auth_info['oauth_user_location'],                
                'last_login' => $current_time,
                'created' => $current_time,             
                'activation_key' => md5(uniqid()),
                'status' => 1,
            );
            $m_user->save($userinfo);           
            $userinfo['id'] = $user_id = $m_user->getLastInsertID();            
            // $this->Session->write('Auth.User',$userinfo);
            // $this->Cookie->write('Auth.User',$userinfo,true,0);
        }
        else{
            $updateinfo = array(
                'nickname'=> $oauth_user_nick,
                'screen_name'=> $oauth_user_nick,
                'image'=> $auth_info['oauth_user_avatar'],
                'sex'=> $gender,
                'location'=> $auth_info['oauth_user_location'],
                'last_login' => $current_time,
            );
            $userinfo['User'] = array_merge($userinfo['User'],$updateinfo);
            $user_id = $userinfo['User']['id'];
            $m_user->save($userinfo['User']);
            // $this->Session->write('Auth.User',$userinfo['User']);
            // $this->Cookie->write('Auth.User',$userinfo['User'],true,0);
        }

        $m_oauthbind = loadModelObject('Oauthbind');
        $oauth_bind = $m_oauthbind->find('first',array(
            'conditions'=>array(
                'oauth_openid'=> $oauth_user_id,
                'user_id' => $user_id,
                'source' => $auth_type,
            ),
        ));
        
        //echo $topUser['user']['user_id'];exit;
        
        if(empty($oauth_bind)){
            $oauth_bind = array(
                'user_id' => $user_id,
                'oauth_openid' => $oauth_user_id,
                'oauth_token' =>  $access_token,
                'expires' => $expires,
                //'oauth_token_secret' => $login_key['oauth_token_secret'],
                'source' => $auth_type,
            );
            $m_oauthbind->save($oauth_bind);
        }
        else{
            $oauth_bind['Oauthbind']['updated'] = date('Y-m-d H:i:s');
            $oauth_bind['Oauthbind']['oauth_token'] = $access_token;
            $oauth_bind['Oauthbind']['expires'] = $expires;
            //$oauth_bind['Oauthbind']['oauth_token_secret'] = $login_key['oauth_token_secret'];
            $m_oauthbind->save($oauth_bind);
        }
        
        $user_oauths = $m_oauthbind->find('all',array(
            'conditions'=>array(
                'user_id' => $user_id,
            ),
        ));
        // $this->Session->write('Auth.Oauthbind',$user_oauths);
        return array('User'=>$userinfo['User'],'Oauthbinds'=>$user_oauths);
    }


}
