<?php

App::uses('RequestFacade', 'Network');
App::uses('ImageResize','Lib');

App::import('Vendor', 'ALIOSS', array('file' => 'alioss'.DS.'sdk.class.php'));

require_once COMMON_PATH.'Vendor/Qiniu/autoload.php'; // 包含七牛的库文件
require_once COMMON_PATH.'Vendor'.DS.'alioss'.DS.'v2'.DS.'aliyun.php'; // 包含阿里云OSS的库文件

use Aliyun\OSS\OSSClient;

/**
 * 抓取相关操作封装类
 * 扩展的Utility类名后面都加上Utility，放在类名与Model等其它类重名
 * @author Arlon
 *
 */
class CrawlUtility {

    public static $recrawl_img = false;     //是否重复抓取图片
	
    /**
     * 保存内容中的图片到本站服务器，图片会缩放
     * @param $content
     * @param $url
     */
    public static function saveImagesInContent($content, $url) {
    	
        preg_match_all("/src=[\s|\"|']*((http:\/\/)?([^>]*)\.(gif|jpg|jpeg|bmp|png))/isU", $content, $imagearray);
        $imagearray = array_unique($imagearray[1]);
        
        // <img src="http://mmsns.qpic.cn/mmsns/agEQQ7NdJSNdDGUW5se1J1nB7UYjBGJ7Ew9ELOEBtIWA60qQqvETAA/0" style="border: 0px; height: auto !important;"  />
        preg_match_all("/<img[^>]*src=[\"|'](.*)[\"|']/isU", $content, $other_img);
        $imagearray = array_merge($imagearray,$other_img[1]);
        //<img data-src="http://mmbiz.qpic.cn/mmbiz/5DZAKbdPGFcPKibg0ggoHibbDSlpNDLpbGicYEHZhy3TqjHjYVS0x5JFomSxQMGZJkz5JOtIaYNM2ZZKup2ic9GHAg/0"  />
        // <img data-src="http://mmbiz.qpic.cn/mmbiz/q03j6z8KXgujx3sDxJGyOXGZAicD5vYBl1ict47w0EhboUeWLh9LibM5CSbm09B1ic4EDTicUgHdNFokRsBLB0iaicuxA/0"  />
        $imagearray = array_unique($imagearray);

        if(count($imagearray) > 5){ //5个图片的以上的不保存图片
            //$imagearray = array_slice($imagearray,0,5);
            return array('content' => $content, 'coverimg' => array());
        }

        $referer_url = $url;
        $coverimg = array();
        foreach ($imagearray as $key => $value) {
            $value = str_replace('"', '', $value);
            $value = str_replace("'", '', $value);
            $originimgurl = $value = trim($value);
            $imageurl = self::getPagelinkUrl($value, $url);

            if( strpos($imageurl,'sinaimg.cn')!==false ||
			strpos($imageurl,'gtimg.com') !== false ||
			 strpos($imageurl,'http://mmbiz.qpic.cn') !== false  || strpos($imageurl,'http://mmsns.qpic.cn') !== false) {
                continue; // 对于mmbiz.qpic.cn mmsns.qpic.cn微信中上传的图片跳过不抓取。节省图片云空间存储
            }


            $i = 0;
            do{
                if ($local_imageurl = self::saveImagesByUrl($imageurl, $referer_url)) {
                    list($width_orig, $height_orig) = getimagesize($local_imageurl);
                    if ($height_orig > 90 &&  $width_orig > 90){ // 判断宽高度要大于90px
                        $coverimg[] = $local_imageurl;
                    }
                    $local_imageurl = getStaticFileUrl($local_imageurl,true);                
                    $content = str_replace($originimgurl, $local_imageurl, $content);
                }
                else{
                    echo "=$imageurl get error.====<br/>\n";
                }
                $i++;
            }while(!$local_imageurl && $i < 3 ); //try 2 times
        }
        return array('content' => $content, 'coverimg' => $coverimg);
    }
    
    public static function saveMusic($url,$music_name,$referer='',$agent='Mozilla/5.0 (Linux; U; Android 4.1.2; zh-cn; Lenovo A820 Build/JZO54K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30 MicroMessenger/5.3.0.50_r694338.420') {
    	
    	$year_month = date('Y-m');
    	$image_save_path = UPLOAD_FILE_PATH . 'files/remote/' . $year_month . '/';
    	$file_path = $image_save_path.$music_name;
    	
    	if (!file_exists($file_path)) {
	    	$filecontent = self::getRomoteUrlContent($url, array('header' => array('Referer' => $referer,'User-Agent'=>$agent)));
	    	if ($filecontent && strlen($filecontent) > 30) {
	    		$img_file = new File($file_path, true);
	    		$img_file->write($filecontent);
	    		$img_file->close();    	
	    	} else {
	    		return false;
	    	}
    	}
    	return '/files/remote/' . $year_month . '/' . $music_name;
    }
    
    
    public static function getFileMime($filename){
    	$img_file = new File($filename);
    	$mime = $img_file->mime();
    	$img_file->close();
    	
    	if($mime==false && function_exists('exif_imagetype')){
    		$exif_type = exif_imagetype($filename);
    		if($exif_type==IMAGETYPE_GIF){
    			$mime = 'image/gif';
    		}
    		elseif($exif_type==IMAGETYPE_JPEG){
    			$mime = 'image/jpg';
    		}
    		elseif($exif_type==IMAGETYPE_PNG){
    			$mime = 'image/png';
    		}
    		elseif($exif_type==IMAGETYPE_BMP){
    			$mime = 'image/bmp';
    		}
    	}
    	/*elseif($mime==false){
    		$filebi = trim(exec('file -bi '.escapeshellarg($filename))); // image/gif; charset=binary
    		$arr = explode(';',$filebi);
    		$mime = $arr[0];
    	}*/
    	
    	if($mime==false){
    		$mime = get_mime_type($filename);
    	}
    	return $mime;
    }

    public static function aliOssExists($object_name,$bucket_name="") {
        
        if( defined('IS_LOCALHOST') ) {
            return true;
        }
        
        $oss_client = self::getAliOssClient();
        
        $bucket_name = $bucket_name ? $bucket_name : ALI_BUCKET_NAME;
        
        $response = $oss_client->get_object_meta($bucket_name, $object_name);
        // oss已经存在的文件，直接跳过
        if($response->status == 200 && $response->header['content-length'] > 4096) {
            return true;
        }
        return false;
        
    }
    /**
     * 保存远程图片到服务器，图片会缩放到600*800以内
     * @param $url
     */
    public static function saveImagesByUrl($imageurl, $referer='', 	$options = array()) {
    	
    	$options = array_merge(array(
    			'resize' => true,
    			'oss' => true,
    			'agent' => 'Mozilla/5.0 (Linux; U; Android 4.1.2; zh-cn; Lenovo A820 Build/JZO54K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30 MicroMessenger/5.3.0.50_r694338.420',
    			'max_width' => 600,
    	       'bucket_name' => '',
    	),$options);
    	
    	while(strpos($imageurl,'http://remote.wx135.com/oss/view') !== false) {
    		$urlinfo = parse_url($imageurl);
    		parse_str($urlinfo['query '],$query);
    		if($query['d']) {
    			$imageurl = $query['d'];
    		}
    	}
    	
    	if(empty($referer)) {
    	    $referer = $imageurl;
    	}
    	// 修复了新浪云存储app的网址限制了referer的问题
    	if( strpos($referer,'stor.sinaapp.com') !== false ){    	    
    	    $referer = preg_replace('/-.+\.stor\./','.',$referer); //如 hhh9-wordpress.stor.sinaapp.com  => hhh9.sinaapp.com
    	}
    	
    	//$imageurl = str_replace('http://mmsns.qpic.cn','https://mmbiz.qlogo.cn',$imageurl);
    	$imageurl = str_replace('http://mmbiz.qpic.cn','https://mmbiz.qlogo.cn',$imageurl);
    	
    	$bucket_name = $options['bucket_name'] ? $options['bucket_name'] : ALI_BUCKET_NAME;
    	$domain_url = $options['domain_url'] ? $options['domain_url'] : OSS_DOMAIN_URL;
    	
        $md5_sum = md5($imageurl); //使用md5码，忽略冲突
        $folder = substr($md5_sum,0,2).'/'.substr($md5_sum,2,3);
        $image_save_path = UPLOAD_FILE_PATH . 'files/' . $folder . '/';
        mkdir_p($image_save_path);
        $imgname = $md5_sum.'.jpg'; //random_str(8)
        $filename = $image_save_path . $imgname;
        
        $object_name = $options['object_name'] ? $options['object_name'] : 'files/' . $folder . '/'.$imgname;

		$storage_file = false;
		if($options['oss'] && Configure::read('Storage.aliyun_oss')){
			$oss_client = self::getAliOssClient();
			$response = $oss_client->get_object_meta($bucket_name,$object_name);
			// oss已经存在的文件，直接跳过
			if($response->status == 200 && $response->header['content-length'] > 4096)
			{
				//echo "$imageurl is exists in ali oss.<br>\n";            
	            $url = $domain_url.$object_name;
				return $url;
			}
			else{
				//echo "$imageurl is not exists in ali oss.<br>\n";
			}
		}
        
        // 设置了重复抓取图片或者图片文件不存在时，重复抓取
        if (self::$recrawl_img || !file_exists($filename)) {
            // 微信文章的图片 http://mmsns.qpic.cn 可能会被屏蔽，换成mmbiz.qpic.cn尝试继续抓取
            $img_filecontent = self::getRomoteUrlContent($imageurl, array('header' => array('Referer' => $referer,'User-Agent'=> $options['agent'])));
            if ( $img_filecontent ) {            	
                $img_file = new File($filename, true,0777);
                $ret = $img_file->write($img_filecontent,'w',true);
                $img_file->close();
                
                // 根据内容写的文件重新判断图片的类型，不为jpg时更换文件后缀
                // 当ext后缀不对时，ImageResize生成的新文件会有问题
				if(!file_exists($filename)) {  // write file error. not know why. safe mode?
		        	if($options['oss'] && Configure::read('Storage.aliyun_oss')){
		        		return self::saveToAliOss($img_filecontent, $object_name ,true,true,false,array('bucket_name'=>$bucket_name,'domain_url'=>$domain_url));
		        	}
					return false;
				}
				$ext = false;
				$mime = self::getFileMime($filename);
        		//image/gif
        		if(!$ext){
        			$mime = preg_split("/;/",$mime); 
        			$mime = $mime[0];
        			if($mime == 'image/gif')	$ext = 'gif';
        			if($mime == 'image/png')	$ext = 'png';
        			if($mime == 'image/jpg' || $mime == 'image/jpeg')	$ext = 'jpg';
        			if($mime == 'image/bmp')	$ext = 'bmp';
        		}	
        		if($ext && $ext!='jpg'){
                	$imgname = $md5_sum.'.'.$ext;
                	$new_filename = $image_save_path . $imgname;
                	rename($filename,$new_filename);
        			if(file_exists($new_filename)) {
        			    $object_name = $options['object_name'] ? $options['object_name'] : 'files/' . $folder . '/'.$imgname;
        			    $filename = $new_filename;
        			}
        		}
                if($options['resize'] ){
	                //缩放处理图片,替换原图
	                $image = new ImageResize();
	                $image->resizefile($filename,$filename,$options['max_width'],99999); //仅限制最大宽度，高度不限制，防止一些长图片宽度过小无法看清。
                }
            } else {
                CakeLog::error("Curl get Remote image content error.The url is ".$imageurl);
                return false;
            }
        }
    	else{
    		//echo "$filename is exists in local.<br>\n";
    	}
        if($options['oss'] && Configure::read('Storage.aliyun_oss')){
           return self::saveToAliOss($filename,$object_name,true,false,false,array('bucket_name'=>$bucket_name,'domain_url'=>$domain_url));
        }
        return '/'.$object_name;
    }
    public static $oss_client = null;
    public static function getAliOssClient(){ // V1
        /***
        青岛节点外网地址： oss-cn-qingdao.aliyuncs.com 
        青岛节点内网地址： oss-cn-qingdao-internal.aliyuncs.com

         北京节点外网地址：oss-cn-beijing.aliyuncs.com
        北京节点内网地址：oss-cn-beijing-internal.aliyuncs.com

        杭州节点外网地址： oss-cn-hangzhou.aliyuncs.com
        杭州节点内网地址： oss-cn-hangzhou-internal.aliyuncs.com 
        */
        if(self::$oss_client == null){
        	
        	if(strpos(OSS_HOST_SERVER,'http://') === false){
        		$endpoint = OSS_HOST_SERVER;
        	}
        	else{
        		$endpoint = str_replace('http://','',OSS_HOST_SERVER);
        	}
        	
            self::$oss_client = new ALIOSS(OSS_ACCESS_ID,OSS_ACCESS_KEY,OSS_HOST_SERVER);
            self::$oss_client->set_debug_mode(false);
		self::$oss_client->set_enable_domain_style(TRUE);
        }
        return self::$oss_client;
    }
	public static function getFromAliOss($object_name,&$header = null,$options=array()) {
	    
	    if( defined('IS_LOCALHOST') && file_exists(DATA_PATH.'files'.DS.'content'.DS.$object_name) ) {
            $filename = DATA_PATH.'files'.DS.'content'.DS.$object_name;
            return file_get_contents($filename);
	    }
	    
		if( empty($object_name) || !Configure::read('Storage.aliyun_oss') ){
			return false;
		}
		if(preg_match('|http://img\d*\.wx135\.com|i',$object_name)) {
		    $info = parse_url($object_name);
		    $object_name = $info['path']; // 去除域名与get参数		    
		}
		
		$object_name = str_replace(OSS_DOMAIN_URL,'',$object_name);
		while(substr($object_name,0,1)=='/'){ //上传的object字符串前面不能带斜线。
			$object_name = substr($object_name,1);
		}
        $bucket_name = $options['bucket_name'] ? $options['bucket_name']: ALI_BUCKET_NAME;
        $numargs = func_num_args();

        try{
            if(strpos(OSS_HOST_SERVER,'http://') === false){
                $endpoint = 'http://'.OSS_HOST_SERVER;
            }
            else{
                $endpoint = OSS_HOST_SERVER;
            }
            // V2 Endpoint必须以"http://"开头。V1 不带http://
            $oss_client_v2 = OSSClient::factory(array(
                'AccessKeyId' => OSS_ACCESS_ID,
                'AccessKeySecret' => OSS_ACCESS_KEY,
                'Endpoint' => $endpoint,
            ));
            $options = array(
                'Bucket' => $bucket_name,
                'Key' => $object_name,
            );
            $response = $oss_client_v2->getObject($options);
            if( $numargs >= 2 ) {
                $header = $response->header;
            }
            if($response->status==200 || $response->status==204) {
                return $response->body;
            }
            else{
                $oss_client = self::getAliOssClient();
                $response = $oss_client->get_object($bucket_name,$object_name);
                if( $numargs >= 2 ) {
                    $header = $response->header;
                }
                if($response->status==200 || $response->status==204) {
                    return $response->body;
                }
            }
        }
        catch(Exception $e){
            $errMsg = "Curl get alioss object {$bucket_name}:{$object_name} Error:".$e->getMessage();
            CakeLog::error($errMsg);
        }
		return null;
	}
    public static function delFromAliOss($object_name,$bucket_name="") {
        while(substr($object_name,0,1)=='/'){ //上传的object字符串前面不能带斜线。
            $object_name = substr($object_name,1);
        }
        if(empty($bucket_name)) {
            $bucket_name = ALI_BUCKET_NAME;
        }
        
    	$oss_client = self::getAliOssClient();
    	$response = $oss_client->delete_object($bucket_name,$object_name);
        if($response->status==200 || $response->status==204){
			return true;
		}
		
    	if(strpos(OSS_HOST_SERVER,'http://') === false){
    		$endpoint = 'http://'.OSS_HOST_SERVER;
    	}
    	else{
    		$endpoint = OSS_HOST_SERVER;
    	}
    	// V2 Endpoint必须以"http://"开头。V1 不带http://
    	$oss_client_v2 = OSSClient::factory(array(
    			'AccessKeyId' => OSS_ACCESS_ID,
    			'AccessKeySecret' => OSS_ACCESS_KEY,
    			'Endpoint' => $endpoint,
    	));
    	$options = array(
    			'Bucket' => $bucket_name,
    			'Key' => $object_name,
    	);
        $response = $oss_client_v2->deleteObject($options);
        if($response->status==200 || $response->status==204){
            return true;
        }
    }
    
    public static function delFromQiniu($object_name,$bucket = 'editor135'){
    	$accessKey = 'zcVrFfcm03fpBfnyy-kH1y3ShMGUFUJ2QaEzC7_o';
    	$secretKey = 'jycPHoZFQbk1M8XY3mrWYA9HNr5qFQyVDtdRnJXM';
    	while(substr($object_name,0,1)=='/'){ //上传的object字符串前面不以斜线开头。
    		$object_name = substr($object_name,1);
    	}
    	$auth = new Qiniu\Auth($accessKey, $secretKey);
    	$bucketMgr = new Qiniu\Storage\BucketManager($auth);;
    	//删除$bucket 中的文件 $object_name
    	$err = $bucketMgr->delete($bucket, $object_name);
    	if ($err !== null) {
    		CakeLog::error("qiniu delete error:".$err->message());
    		return false;
    	} else {
    		return true;
    	}
    }

    public static function qiniuFops($key,$fops){
        $accessKey = 'zcVrFfcm03fpBfnyy-kH1y3ShMGUFUJ2QaEzC7_o';
        $secretKey = 'jycPHoZFQbk1M8XY3mrWYA9HNr5qFQyVDtdRnJXM';
        $auth = new Qiniu\Auth($accessKey, $secretKey);
        // 要转码的文件所在的空间
        $bucket = 'editor135';
        // 转码时使用的队列名称
        $pipeline = 'wartermark';
        // 初始化
        $pfop = new Qiniu\Processing\PersistentFop($auth, $bucket, $pipeline,null,true);

        echo $key."<br>";
        echo $fops."<br>";
        list($id, $err) = $pfop->execute($key, $fops);
        if ($err != null) {
            CakeLog::error('qiniuFops error :'.$err);
            return false;
        } else {
            return $id;
        }
    }

    public static function qiniuToken($bucket = '',$keyToOverwrite = null,$callback = true){

        $accessKey = Configure::read('Qiniu.accessKey');
        $secretKey = Configure::read('Qiniu.secretKey');
        if(empty($bucket)) {
            $bucket = Configure::read('Qiniu.bucket');
        }
        // 构建鉴权对象
        $auth = new Qiniu\Auth($accessKey, $secretKey);

        $expires = 7200;
        $returnBody = '{"key":"$(key)","model":"$(x:model)","field":"$(x:field)","uid":"$(x:uid)"}';
        $callbackBody = '{"key":"$(key)","hash":"$(etag)","fname":"$(fname)","model":"$(x:model)","bucket":"$(bucket)","field":"$(x:field)","fsize":$(fsize),"uid":"$(x:uid)"}';

        if(!$callback) { //后台不使用回调。
            $policy = array(
                'returnBody' => '{"ret":"0","state":"SUCCESS","url":"'.Configure::read('Qiniu.url').'/nc/$(x:uid)/$(year)/$(etag)$(ext)"}',
                'saveKey' => 'nc/$(x:uid)/$(year)/$(etag)$(ext)', //$(ext)带有点号
            );
        }
        else{
            $policy = array(
                'returnBody' => $returnBody,
                'saveKey' => '$(x:uid)/$(year)/$(etag)$(ext)', //$(ext)带有点号
                'callbackUrl' => Configure::read('Qiniu.callbackUrl'),//'http://www.135editor.com/uploadfiles/qn_callback',
                'callbackBody' => $callbackBody,
                'callbackBodyType' => 'application/json',
            );
        }
        /*if($wartermarks['mark']) {
            $mark = $wartermarks['mark'];
            $gravity = $wartermarks['gravity'];

            if(strpos($mark,'http')!=='false') {
                //网址
                $policy['persistentOps'] = 'watermark/1/image/'.base64_encode($mark).'/gravity/'.$gravity.'/dissolve/60/ws/0.15';
            }
            else{ //文字
                $policy['persistentOps'] = 'watermark/2/text/'.base64_encode($mark).'/fill/#FFFFFF';
            }
            $policy['persistentPipeline'] = 'wartermark';
        }*/
        $upToken = $auth->uploadToken($bucket, $keyToOverwrite, $expires, $policy, true);
        return $upToken;
    }
    
    public static function saveToQiniu($file_path,$object_name,$bucket = ''){
    	// http://developer.qiniu.com/code/v7/sdk/php.html#upload

        $accessKey = Configure::read('Qiniu.accessKey');
        $secretKey = Configure::read('Qiniu.secretKey');
        if(empty($bucket)){
            $bucket = Configure::read('Qiniu.bucket');
        }

    	// 构建鉴权对象
    	$auth = new Qiniu\Auth($accessKey, $secretKey);
    	// 生成上传 Token
    	$token = $auth->uploadToken($bucket);
    	// 初始化 UploadManager 对象并进行文件的上传
    	$uploadMgr = new Qiniu\Storage\UploadManager();
    	while(substr($object_name,0,1)=='/'){ //上传的object字符串前面不以斜线开头。
    		$object_name = substr($object_name,1);
    	}
    	// 调用 UploadManager 的 putFile 方法进行文件的上传
    	list($ret, $err) = $uploadMgr->putFile($token, $object_name, $file_path);
    	if ($err !== null) {
    		CakeLog::error("upload qiniu error:".var_export($ret,true));
    		return false;
    	} else {
    		//var_dump($ret); array('hash'=>'xxx','key'=>$object_name)
    		return Configure::read('Qiniu.url').'/'.$object_name;
    	}
    }
    // $file_path只允许本地文件路径，双斜线会被替换成单斜线
    /**
     * 
     * @param unknown $file_path 文件的地址，或者内容。
     * @param unknown $object_name 保存的对象的名字
     * @param string $delete true，上传后默认删除本地的文件。
     * @param string $is_content  是否为直接保存内容
     * @param string $mime	文件的媒体类型
     * @return boolean|string
     */
    public static function saveToAliOss($file_path,$object_name,$delete = true,$is_content = false,$mime = false,$options=array()){
    	
        if( defined('IS_LOCALHOST') ) {
            if($is_content) {
                $filename = DATA_PATH.'files'.DS.'content'.DS.$object_name;
                mkdir_p(dirname($filename));
                file_put_contents($filename, $file_path);
                return $filename;
            }
            else{
                return $file_path;
            }
        }
        
    	if( !Configure::read('Storage.aliyun_oss') ){
    		return false;
    	}
    	
        while(substr($object_name,0,1)=='/'){ //上传的object字符串前面不能带斜线。
            $object_name = substr($object_name,1);
        }
        
        $bucket_name = ALI_BUCKET_NAME;
        if( $options['bucket_name'] ) {
            $bucket_name = $options['bucket_name'];
        }
        $domain_url = OSS_DOMAIN_URL;
        if( $options['domain_url'] ) {
            $domain_url = $options['domain_url'];
        }
        
        
		if(!$is_content) {
			$file_path = str_replace('//','/',$file_path);
			if(!file_exists($file_path))	return false;
			$file_content = file_get_contents($file_path);
			if($mime == false){
				$mime = self::getFileMime($file_path);
			}
		}
		else{
			$file_content = $file_path;
		}
		
    	$i=0;
    	do{
            // v2版本的client处理失败后，v1再试一次
            try{
            	if(strpos(OSS_HOST_SERVER,'http://') === false){
            		$endpoint = 'http://'.OSS_HOST_SERVER;
            	}
            	else{
            		$endpoint = OSS_HOST_SERVER;
            	}
            	
            	// V2 Endpoint必须以"http://"开头。V1 不带http://
                $oss_client_v2 = OSSClient::factory(array(
                    'AccessKeyId' => OSS_ACCESS_ID,
                    'AccessKeySecret' => OSS_ACCESS_KEY,
                	'Endpoint' => $endpoint,
                ));
                $options = array(
                    'Bucket' => $bucket_name,
                    'Key' => $object_name,
                    'Content' => $file_content,
                );
                if($mime){
                	$options['ContentType'] = $mime;
                }
                
                $result = $oss_client_v2->putObject($options);
                $url = $domain_url.$object_name;
                if( $delete && !$is_content ) {
                    @unlink($file_path);
                }
//                 print_r($result);
                return $url;
            }
            catch(Exception $e){
                $errMsg =  "Error V2:save to aliyun oss error.object=$object_name file is $file_path<br>\n";
                $errMsg .= $e->getMessage();
                CakeLog::error($errMsg);
                try{
                    $oss_client = self::getAliOssClient();
                    $response = $oss_client->upload_file_by_file($bucket_name,$object_name,$file_path);
                    if($response->status==200){
                        // $url = $response->header['_info']['url'];
                        // $url = str_replace(array(
                        //     'wx135.oss.aliyuncs.com',
                        //     'wx135.oss-cn-hangzhou-internal.aliyuncs.com',
                        //     'wx135.img-cn-hangzhou.aliyuncs.com'),'img.wx135.com',$url);
                        $url = $domain_url.$object_name;
                        if($delete){
                            @unlink($file_path);
                        }
                        return $url;
                    }
                    else{
                        CakeLog::error(var_export($response,true));
                        return false;                 
                    }
                }
                catch(Exception $ve){
                    CakeLog::error("Error V1:save to aliyun oss error.object=$object_name file is $file_path<br>\n" );
//                     print_r( $ve->getMessage());
//                     echo "<br/>\n";
                }
            }
            $i++;
       }while($i<3); //重试1次
        return false;
    }

    /**
     * 获取页面链接的绝对url,将相对的url转换成全部url地址
     * @param $url_in_page  在页面上出现的链接，（可能为相对路径）
     * @param $url 页面链接url，为可以直接访问的网址.带前缀 http://或者https://
     */
    public static function getPagelinkUrl($url_in_page, $url) {
        if ('http' != substr($url_in_page, 0, 4)) {
            // url前带 '/'，仅需加上域名即可
            if (substr($url_in_page, 0, 1) == '/') {
                $hostarray = explode('/', $url);
                $url_in_page = $hostarray[0] . '//' . $hostarray[2] . $url_in_page;
            } else {
                // url前不带 '/',使用相对路径
                $pageurls = pathinfo($url);
                $pageurls = $pageurls['dirname'];
                $url_in_page = $pageurls . '/' . $url_in_page;
            }
        }
        // 将空格替换成%20,rfc1738,rawurlencode
        $url_in_page = str_replace(' ', '%20', $url_in_page);
        return $url_in_page;
    }

    /**
     * 抓取url内容
     * @param $url 页面url地址
     * @param $options  抓取时其他参数
     * @return object Response
     */
    public static function getRomoteUrlContent($url, $options=array(),&$header = null) {
    	
    	if(strpos($url,OSS_DOMAIN_URL) !== false  || preg_match('/img\d*\.wx135\.com/i',$url) ) {
    		return  self::getFromAliOss($url);
    	}
    	
        $request = array(
                    'header' => array(
                        'Referer' => $url,
                        'User-Agent' => 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:29.0) Gecko/20100101 Firefox/29.0',
                    ),
                );
        if(is_array($options['header'])) {
            $request['header'] = @array_merge($request['header'],$options['header']);
        }
        /* 注意区分这个response对象与aliyun的OSS客户端的response对象不一致 */
        $response =  RequestFacade::get($url, array(), $request);
        
//         print_r(array_keys(object_to_array($response)));
//         print_r($response->headers);
//         print_r($response);
        
        $numargs = func_num_args();
		if( $numargs == 3 ) {
			$header = $response->headers;
		}
		/**
		 * [0] => body
    [1] => headers
    [2] => cookies
    [3] => httpVersion
    [4] => code
    [5] => reasonPhrase
    [6] => raw
    [7] => context
		 */
		if( $response->code==200 ) {
		    return $response->body;
		}
        return null;
    }

}
