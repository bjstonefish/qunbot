<?php

class OcsEngine extends CacheEngine {

	var $_Memcached = null;

	var $settings = array();
	
/**
 * Initialize the Cache Engine
 *
 * Called automatically by the cache frontend
 * To reinitialize the settings call Cache::engine('EngineName', [optional] settings = array());
 *
 * @param array $setting array of setting for the engine
 * @return boolean True if the engine has been successfully initialized, false if not
 * @access public
 */
	function init($settings = array()) {
		$this->settings =array_merge(array(
			'engine'=> 'Ocs', 'prefix' => Inflector::slug(APP_DIR) . '_', 'compress'=> false
			),$settings);
		
		$this->_Memcached =  new Memcached;
		$this->_Memcached->setOption(Memcached::OPT_COMPRESSION, false);
		
		$this->_Memcached->setOption(Memcached::OPT_BINARY_PROTOCOL, true); //使用binary二进制协议
		$this->_Memcached->addServer('a53fa2b297fd4772.m.cnhzaliqshpub001.ocs.aliyuncs.com', 11211); //添加OCS实例地址及端口号
		// 加入白名单，免密码进行操作。
		//$this->_Memcached->setSaslAuthData('a53fa2b297fd4772', 'HNsDLcweBPPrE7AN'); //设置OCS帐号密码进行鉴权
				
		if($this->_Memcached == false){
			// memcache初始化失败时，不使用缓存。
			Configure::write('Cache.disable', true);
			return false;
		}
		else{
			return $this->_Memcached;
		}
	}
/**
 * Write data for key into cache
 *
 * @param string $key Identifier for the data
 * @param mixed $value Data to be cached
 * @param integer $duration How long to cache the data, in seconds
 * @return boolean True if the data was succesfully cached, false on failure
 * @access public
 */
	function write($key, $value, $duration) {
		return $this->_Memcached->set($key,$value,$duration);
	}
/**
 * Read a key from the cache
 *
 * @param string $key Identifier for the data
 * @return mixed The cached data, or false if the data doesn't exist, has expired, or if there was an error fetching it
 * @access public
 */
	function read($key) {
		return $this->_Memcached->get($key);
	}
/**
 * Delete a key from the cache
 *
 * @param string $key Identifier for the data
 * @return boolean True if the value was succesfully deleted, false if it didn't exist or couldn't be removed
 * @access public
 */
	function delete($key) {
		return $this->_Memcached->delete($key);
	}
/**
 * Delete all keys from the cache
 *
 * @return boolean True if the cache was succesfully cleared, false otherwise
 * @access public
 */
	function clear($check) {
		return $this->_Memcached->flush();
	}
/**
 * Connects to a server in connection pool
 *
 * @param string $host host ip address or name
 * @param integer $port Server port
 * @return boolean True if memcache server was connected
 * @access public
 */
	function connect($host, $port = 11211) {
	    if ($this->_Memcache->getServerStatus($host, $port) === 0) {
	        if ($this->_Memcache->connect($host, $port)) {
	            return true;
	        }
	        return false;
	    }
	    return true;
	}
	
	public function increment($key, $offset = 1) {
		return $this->_Memcached->increment($key, $offset);
	}
	
	/**
	 * Decrements the value of an integer cached key
	 *
	 * @param string $key Identifier for the data
	 * @param integer $offset How much to subtract
	 * @return New decremented value, false otherwise
	 * @throws CacheException when you try to decrement with compress = true
	 */
	public function decrement($key, $offset = 1) {
		return $this->_Memcached->decrement($key, $offset);
	}
}
?>