<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>系统罢工中...</title>

<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link href="/css/bootstrap.css" rel="stylesheet" type="text/css" >
<link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="/css/ui-customer.css" />
</head>
<body>
<!-- 站点维护中，所有页面均不可访问 -->

<div class="container" style="padding:150px 0">
      <div class="row">
        <div class="col-xs-12">
          <!-- Main Content -->
          <h1 class="text-center">Coming Soon...</h1>
          <p class="lead text-center">
            访问人数太多，数据库服务器已累瘫，紧急维护中，请休息一会再访问吧。<br />
          </p>
          <section class="text-center">
          	<?php
          	
          	if( Configure::read('Site.wechat_qrcode') ){
          		echo '<p class="text-center">扫描二维码关注最新动态。</p>';
          		echo $this->Html->image($this->Html->url(Configure::read('Site.wechat_qrcode')), array('style'=>'height:200px;','title' => '微信二维码',));
          	}
          	?>
          </section>          
        </div>
      </div>
</div>
</body>
</html>
