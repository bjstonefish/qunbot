<?php

class HtmlPurifierComponent extends Component
{

    private $_config;
    private $purifier;

    public function __construct(ComponentCollection $collection, array $settings = array())
    {
        parent::__construct($collection, $settings);

        App::import('Vendor', 'HtmlPurifier', array('file' => 'htmlpurifier' . DS . 'HTMLPurifier.auto.php'));
        $this->_config = HTMLPurifier_Config::createDefault();
        $this->_config->set('HTML.DefinitionID', '1');
        $this->_config->set('HTML.DefinitionRev', 1);
        $this->_config->set('HTML.Doctype', 'XHTML 1.0 Transitional');


        $this->_config->set('Attr.EnableID', true);
        $this->_config->set('Code.Encoding', 'UTF-8');

        // support all valid css.
        $this->_config->set('CSS.AllowDuplicates', true); //允许重复，如margin:0 auto;margin-top-30px;
        $this->_config->set('CSS.AllowImportant', true); // !important
        $this->_config->set('CSS.Trusted', true); // position,top,bottom,left,right,z-index
        $this->_config->set('CSS.AllowTricky', true); // display,visibility,overflow
        $this->_config->set('CSS.Proprietary', true); // opacity,filter,scrollbar

        /*$this->_config->set('CSS.ForbiddenProperties', array(
            'line-height'=>true,'text-align'=>true
        ));*/

        $this->_config->set('HTML.AllowedElements', array(
            'section' => true, 'a'=>true,'div' => true, 'p' => true, 'span' => true,
            'strong' => true, 'b' => true, 'i' => true, 'del' => true, 'img' => true,
            'table' => true, 'tr' => true, 'td' => true, 'th' => true, 'br' => true, 'hr' => true,
            'iframe' => true, 'center' => true,
            'svg' => true, 'text' => true, 'input' => true,
            'tspan' => true, 'animateMotion' => true, 'animate' => true,
            'path' => true,'animateTransform'=>true,'rect'=>true,'g'=>true,
        ));

        /**
         * AllowedAttributes 修改AttrValidator中的allowed_attr.
         */

        /*$this->_config->set('HTML.AllowedAttributes', array(
            'src' => true, 'style' => true, '*.data-id' => true,
            'width' => true, 'height' => true, 'href' => true, 'target' => true,
            'data-src' => true, 'class' => true, 'alt' => true,
            // --svg attributes--
            'attributename' => true, 'x' => true, 'y' => true, 'from' => true, 'to' => true, 'begin' => true,
            'dur' => true, 'repeatcount' => true, 'fill' => true,
            'font-family' => true, 'font-weight' => true, 'font-size' => true, 'text-anchor' => true,
            'xmlns' => true, 'version' => true, 'data-width' => true, 'values' => true,
            'transform' => true, 'path' => true, 'additive' => true, 'attributeType' => true,
            'stroke-dasharray'=>true,'stroke-miterlimit'=>true,'fill-rule'=>true,'clip-rule'=>true,
            //'stroke-dasharray'=>true,''=>true,''=>true,''=>true,''=>true,''=>true,''=>true,
        ));*/

        $this->_config->set('Core.RemoveInvalidImg', false);

        $this->_config->set('HTML.SafeIframe', true); //允许iframe，视频代码为iframe形式
        $this->_config->set('URI.SafeIframeRegexp', '%^http://(v.qq.com/iframe/|player.youku.com/|www.tudou.com/|player.video.qiyi.com/)%');
        //     http://v.qq.com/
        // %^http://(www.youtube.com/embed/|player.vimeo.com/video/)% - Allow both
        //$this->_config->set('Attr.AllowedClasses', array());
        //$this->_config->set('Attr.ForbiddenClasses', array('background-color'));
    }

    public function config($key, $val)
    {
        $this->purifier->config->set($key, $val);
    }

    /**
     * @param $func getHTMLDefinition获取的def的方法名
     * @param $arguments 方法的参数，为数组形式
     */
    public function def($func, $arguments)
    {
        $def = $this->_config->getHTMLDefinition(true);
        call_user_func_array(array($def, $func), $arguments);
    }


    /***
     * css属性的允许，修改HTMLPurifier/AttrDef/CSS.php
     */
    public function initPurifier()
    {


        $def = $this->_config->getHTMLDefinition(true);
        $def->addAttribute('img', 'data-src', 'Text');
        $def->addElement('section', 'Block', 'Flow', 'Common');
        $def->addElement('center', 'Block', 'Flow', 'Common');
        $def->addAttribute('section', 'data-id', 'Text');


        // 属性同时添加到 HTML.AllowedAttributes
        $svgAttri = array(
            'xmlns' => 'Text',
            'version' => 'Text', 'width' => 'Text', 'height' => 'Text',
            'attributename' => 'Text', 'path' => 'Text','rotate'=>'Text','d'=>'Text',
            'from' => 'Text',
            'to' => 'Text', 'transform' => 'Text',
            'begin' => 'Text',
            'dur' => 'Text',
            'values' => 'Text',
            'repeatcount' => 'Text',
            'x' => 'Text',
            'y' => 'Text',
            'stroke' => 'Text','stroke-width'=>'Text',
            'fill' => 'Text','additive'=>'Text',
            'attributeType' => 'Text','stroke-dasharray'=>'Text','stroke-miterlimit'=>'Text',
            'fill-rule'=>'Text','clip-rule'=>'Text',
            'font-family' => 'Text', 'font-weight' => 'Text', 'font-size' => 'Text',
            'text-anchor' => 'Text',
        );
        $def->addElement('g', 'Block', 'Flow', 'Common');
        $def->addElement('svg', 'Block', 'Flow', 'Common', $svgAttri);
        $def->addElement('animate', 'Inline', 'Inline', 'Common', $svgAttri);
        $def->addElement('tspan', 'Inline', 'Inline', 'Common', $svgAttri);
        $def->addElement('text', 'Inline', 'Inline', 'Common', $svgAttri);
        $def->addElement('path', 'Inline', 'Inline', 'Common', $svgAttri);
        $def->addElement('rect', 'Inline', 'Inline', 'Common', $svgAttri);
        $def->addElement('animateMotion', 'Inline', 'Inline', 'Common', $svgAttri);
//        foreach(array('svg','animate','tspan','text') as $el){
//            foreach ($svgAttri as $atr => $type){
//                $def->addAttribute($el, $atr, $type);
//            }
//        };

        $this->purifier = new HTMLPurifier($this->_config);
    }

    /**
     * 过滤html，去除非法的，不对称的标签。
     * @param string $html
     */
    public function filter($html)
    {
        return $this->purifier->purify($html);
    }
}