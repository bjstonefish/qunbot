<?php
/**
 * 
 * 数据的访问权限控制，可编辑的字段。
 * 
 */
App::uses('BaseAuthorize', 'Controller/Component/Auth');
App::uses('Router', 'Routing');

class DataItemAuthorize extends BaseAuthorize {

	public function __construct(ComponentCollection $collection, $settings = array()) {
		parent::__construct($collection, $settings);
		$this->_setPrefixMappings();
	}

	public function authorize($user, CakeRequest $request) {
		if (!isset($this->settings['actionMap'][$request->params['action']])) {
			trigger_error(__d('cake_dev',
				'CrudAuthorize::authorize() - Attempted access of un-mapped action "%1$s" in controller "%2$s"',
				$request->action,
				$request->controller
				),
				E_USER_WARNING
			);
			return false;
		}
		$user = array($this->settings['userModel'] => $user);
		$Acl = $this->_Collection->load('Acl');
		return $Acl->check(
			$user,
			$this->action($request, ':controller'),
			$this->settings['actionMap'][$request->params['action']]
		);
	}

}
