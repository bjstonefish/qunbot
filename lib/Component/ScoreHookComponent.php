<?php

class ScoreHookComponent extends Component {
	
	
	private function updateScore($user_id,$score){
		$user = loadModelObject('User');
		
		if( $score > 0 ) {
			$user->updateAll(array('score'=>'score+'.$score),array('User.id'=>$user_id));
		}
		else{
			$user->updateAll(array('score'=>'score-'.abs($score)),array('User.id'=>$user_id));
		}
	}
	
	public function checkscore($controller,$model,$action,$score = 0) {
		$controller->loadModel('ScoreRule');
		$rules = $controller->ScoreRule->getRule($model,$action);
		
		if(!empty($rules) &&  $rules['ScoreRule']['score'] < 0 ) {
			$last_score = $controller->Session->read('Auth.User.score');
			
			$score = $score ? $score: $rules['ScoreRule']['score'];
			
			if( $last_score < abs($score) ) {
				return false;
			}
		}
		return true;		
	}
    
    public function addScore($controller,$user_id,$data_id = '',$model = '',$action = '',$update_score = 0)
    {
    	if(empty($model)) {
    		$model = $controller->modelClass;
    	}
    	if(empty($action)) {
    		$action = $controller->action;
    	}
    	
    	if( $user_id ){
    		$controller->loadModel('ScoreRule');
    		$rules = $controller->ScoreRule->getRule($model,$action);
    		if(!empty($rules)) {
    			
    			$date = date('Y-m-d');
    			$controller->loadModel('ScoreLog');
    			
    			if( $rules['ScoreRule']['daytimes'] > 0 ){ // 限定了每天执行的次数
    				$conditions = array( 'rule_id'=> $rules['ScoreRule']['id'],'user_id' => $user_id, );
    				// 按日期，
    				if($rules['ScoreRule']['daylimit']) {
    					$conditions['score_date'] = $date;
    				}
    				// 按数据编号，
    				if($rules['ScoreRule']['data_id_limit'] && $data_id) {
    					$conditions['data_id'] = $data_id;
    				}
    				
    				$total = $controller->ScoreLog->find('count',array(
    					'conditions'=> $conditions,
    				));
    				
    				if( $rules['ScoreRule']['data_id_limit'] && $data_id ) {
    				    if( $total >= $rules['ScoreRule']['data_id_limit'] ) {
    				        return false; //次数达到时，不再增加积分
    				    }
    				}
    				elseif($total >= $rules['ScoreRule']['daytimes']) {
    					return false; //次数达到时，不再增加积分
    				}
    			}
    			
    			if($user_id == $controller->Session->read('Auth.User.id')) {
    			    // 个人操作引起自己的积分变化
    			    $last_score = $controller->Session->read('Auth.User.score');
    			}
    			else{
    			    // 个人操作引起他人的积分变化
    			    $last_score = $controller->User->getUserScore($user_id);
    			}
    			
// 				$controller->loadModel('User');    			
//     			$last_score = $controller->User->getUserScore($user_id);
    			$update_score = (($update_score != 0) ? $update_score : $rules['ScoreRule']['score']);
    			$this->updateScore($user_id,$update_score);
    			
    			if($user_id == $controller->Session->read('Auth.User.id')) {
    			    // 个人操作引起自己的积分变化
    			     $controller->Session->write( 'Auth.User.score', $last_score + $update_score );
    			}
    			
    			$name = '';
    			if( is_array( $controller->item) && $controller->item[$model] ) {
    				$name = $controller->item[$model]['name'];
    			}
    			elseif(!in_array($model,array('User')) && $data_id){
    				if( ModelExists($model) ){
	    				$controller->loadModel($model);
	    				$item = $controller->{$model}->find('first',array('conditions'=>array('id'=>$data_id)));
	    				$name = $item[$model]['name'];
    				}
    			}
    			$controller->ScoreLog->create(); //必须create。否则一次请求产生多次积分记录时，前面保存的都会被替换掉了
    			$controller->ScoreLog->save(array(
    				'rule_id' => $rules['ScoreRule']['id'],
    				'last_score' => $last_score,
    				'score_date' => $date,
    				'score' => $update_score,
    				'user_id' => $user_id,
    				'data_id' => $data_id,
    				'name' => $name,
    			));
    			
    			return $update_score;
    		}
    	}
    	return 0;
    }
    
    public function favorSuccess($controller,$data_id,$model,$update_score ){
		$user_id = $controller->currentUser['id'];
    	return $this->addScore($controller,$user_id,$data_id,$model,'addfavor',$update_score);
    }
    
    public function registerSuccess($controller,$user_id) {
    	return $this->addScore($controller, $user_id);
    }
    
    public function loginSuccess($controller,$user) {
    	if( is_array($user) && $user['id'] ){
    		$score = $this->addScore($controller, $user['id']);
			return $score;
    	}
    }
}
?>