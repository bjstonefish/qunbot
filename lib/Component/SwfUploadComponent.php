<?php 

if (!class_exists('Folder')) {
	App::uses ('Folder','Utility');
}

class SwfUploadComponent extends Component {	
	public $params = array();
	public $errorCode = null;
	public $errorMessage = null;
	
	/**
	 * 文件保存的路径（绝对路径）
	 * @var string
	 */
	public $uploadpath;
	/**
	 * 文件访问的路径（相对路径）。在保存至sae时使用
	 * @var string
	 */
	public $relativePath;
	public $overwrite = true; // 覆盖同名文件,指经过加密后的文件名相同，可能性微乎其微；$this->savename = md5($filebase.time()).'_'.date('Ymd').$fileext;
	public $filename; // 上传的文件名
	
	public $savename ; // 保存的文件名
	public $file_type = '';
	
	public $is_image = false;

	public $upload_type = 'image';

	public $allow_type = array(
	    'image' => array("image/png", "image/gif", "image/jpeg", "image/bmp", "image/jpg"),
        'audio' => array('audio/mp3','audio/mpeg','audio/wav','audio/ogg','audio/amr'),
        'video' => array(),
    );
	
	public $remoteType = 'oss'; // oss, qiniu, wechatUploadimg, wechat
	
	public $watermark_pos = 0;
	public $watermark_img = '';
	public $watermark_text = '';
	/**
	 * 图片是否生成缩略图
	 * @var boolean
	 */
	public $gen_thumb = true;//是否生成缩略图
	
	public $file_post_name='photo';
	
	public $current_dir;
	
	/**
	 * Contructor function
	 * @param Object &$controller pointer to calling controller
	 */
	public function startup(Controller $controller) {
		// uploadpath 上传文件files所在的目录
		$this->current_dir = 		date('Ym');
		
		$this->uploadpath = UPLOAD_FILE_PATH.UPLOAD_RELATIVE_PATH.$this->current_dir . DS;
		$this->relativePath = UPLOAD_RELATIVE_PATH.$this->current_dir . DS;		
		$this->relativeUrl = str_replace(DS,'/',$this->relativePath);        
		$this->params = $controller->params;		
	}
	/**
	 * 设置上传文件的保存路径
	 * @param string $path
	 */
	public function setSavePath($path=''){
		if($path){
			$this->uploadpath = UPLOAD_FILE_PATH.UPLOAD_RELATIVE_PATH.$path.'/';
			
			mkdir_p($this->uploadpath,0755);
			
			$this->relativePath = UPLOAD_RELATIVE_PATH.$path.'/';
			$this->relativeUrl = str_replace(DS,'/',$this->relativePath);
		}
	}
	
	public function setSaveName($name) {
	    $this->savename = $name;
	}
	
	public function setRemoteType($type){
	    $this->remoteType = $type;
	}
	
	public function deletefile($relativepath){
		
		$file_path = UPLOAD_FILE_PATH.$relativepath;
		$pathinfo = pathinfo($file_path);
		if(in_array(strtolower($pathinfo['extension']),array('gif','jpg','bmp'))){
			@unlink($pathinfo['dirname'].DS.'thumb_s'.DS.$pathinfo['basename']);
			@unlink($pathinfo['dirname'].DS.'thumb_m'.DS.$pathinfo['basename']);
		}
		@unlink($file_path);		
	}
	
	/**
	 * Uploads a file to location
	 * @return boolean true if upload was successful, false otherwise.
	 */
	public function upload() {
		$ok = false;
		if ($this->validate()) {
			$this->filename = $this->params['form'][$this->file_post_name]['name'];
			
		    $filesize =  $this->params ['form'] [$this->file_post_name] ['size'];
			
			$fileparts = explode('.', $this->filename);
			$fileext = strtolower(array_pop($fileparts));
			$filebase = urlencode(implode('.', $fileparts));
			if(in_array($fileext,array('php','html','htm','shtml','sh','js','shtm','asp','exe','cgi'))){
				$fileext = '_'.$fileext;
			}

			// 使用md5_file生成hash，可避免同一文件在同一月份里重复上传存在多份数据，浪费空间。
			// 又因为上传文件按月份存在不同的目录，MD5file的hash值冲突的情况的完全忽略。
			// $hash = md5_file($this->params['form'][$this->file_post_name]['tmp_name']);
// 			$hash = md5($this->params['form'][$this->file_post_name]['tmp_name']);
			
			// 图片会编辑保存，故每次上传时，都重新生成新的图片不覆盖原图片
			if(empty($this->savename)) {
			    $this->savename = random_str(8).'_'.random_str(4).'.'.$fileext;
			}
			
			$ok = $this->write();
			if(!$ok){
                $this->setError(-3, 'File write error.');
                return false;
			}
			
			$file_type = get_mime_type($this->uploadpath . $this->savename); // mime_content_type函数在php 5.3版本里取消了

            if( !in_array($file_type,$this->allow_type[$this->upload_type]) ) {
                $this->setError(-3, 'File Content-Type '.$file_type.' not allowed.');
                return false;
            }


			$uploainfo = array(
				'fspath' => $this->relativeUrl . $this->savename,
				'file_type' => $file_type,
				'filename' => $this->filename
			);
			// 取消图片的压缩
			if( in_array($file_type,array ("image/png", "image/gif", "image/jpeg", "image/bmp", "image/jpg"))){
			    $this->is_image = true;
				App::uses('CrawlUtility', 'Utility');
				App::uses('ImageResize','Lib');
				
				if( $this->gen_thumb) {
					//生成小图
					$image = new ImageResize($this->savename);
					$image->PicDir = $this->uploadpath.'thumb_s';
					mkdir_p($image->PicDir, 0755);				
					$image->newWidth = $image->newHeight = Configure::read('Admin.min_image_size');
					$image->TmpName = $this->uploadpath . $this->savename;
					if($image->resize()){
						if(Configure::read('Storage.aliyun_oss')){
							$uploainfo['thumb'] = CrawlUtility::saveToAliOss(
								$this->uploadpath . 'thumb_s/' . $this->savename,
								$this->relativeUrl . 'thumb_s/' . $this->savename);
						}
						else{
							$uploainfo['thumb']=$this->relativeUrl . 'thumb_s/' . $this->savename;
						}
					}
					else{
						$uploainfo['thumb']=$this->relativeUrl . $this->savename;
					}
				}
				
				$image = new ImageResize($this->savename);
				if( !in_array($this->remoteType,array('wechat','wechatUploadimg')) ){
				    // 生成大图，大图会覆盖原图片。 防止图片过大造成页面加载缓慢。
				    $image->PicDir = $this->uploadpath;
				    mkdir_p($image->PicDir, 0755);
				    $image->newWidth  = Configure::read('Admin.max_image_width');
				    $image->newHeight = Configure::read('Admin.max_image_height');
				    $image->TmpName = $this->uploadpath . $this->savename;
				    if($filesize > 102400) { // 当文件大于100K时，才进行压缩与清晰度调整
				        $image->resize();
				    }
				}

				if($this->watermark_img) {
					$image->watermark($this->uploadpath . $this->savename, $this->watermark_img, $this->watermark_pos);
					//CakeLog::info("upload watermark over. ". (time() - $start) );
				}
				elseif($this->watermark_text){
					$image->watermark_text($this->uploadpath . $this->savename, $this->watermark_text, $this->watermark_pos);
					//CakeLog::info("upload watermark over. ". (time() - $start) );
				}
				
				if(Configure::read('Storage.aliyun_oss')){
				    $ok = $this->toRemote($this->remoteType,$uploainfo);
				}
				
			}
			else{
				App::uses('CrawlUtility', 'Utility');
				if(Configure::read('Storage.aliyun_oss')){
				    $ok = $this->toRemote($this->remoteType,$uploainfo);
				}
			}
			
			if ($ok) {
				return $uploainfo;
			}
		}
		
		return $ok;
	}
	
	public function toRemote($type = 'oss', &$uploainfo){
		
		if($type == 'qiniu') { // 七牛存储
	    	$uploainfo['fspath'] =  CrawlUtility::saveToQiniu( $this->uploadpath . $this->savename, $this->relativeUrl . $this->savename);
	    	if($uploainfo['fspath']) {
	    		return true;
	    	}
	    }
	    elseif($type == 'oss') {
	        if(Configure::read('Storage.aliyun_oss')){
	            $uploainfo['fspath'] =  CrawlUtility::saveToAliOss( $this->uploadpath . $this->savename, $this->relativeUrl . $this->savename,true,false,false,array('bucket_name'=>'wx135','domain_url' => 'http://image.135editor.com/'));
	            if($uploainfo['fspath']) {
	               return true;
	            }
	        }
	    }
	    elseif($type == 'wechat') {
	        App::uses('WeixinUtility', 'Utility');
	        if($this->is_image) {
	           $ret = WeixinUtility::add_material( $this->uploadpath . $this->savename, 'image');
	           if($ret['media_id']) {
	               $uploainfo['media_id'] = $ret['media_id'];
	               $uploainfo['fspath'] = $ret['url'];
	               
	               $object_name = 'cache/remote/'.base64_encode( $ret['url'] );
	               $uploainfo['fspath'] = 'http://image2.135editor.com/'.$object_name; // 展示时才自动下载图片
// 	               $uploainfo['fspath'] =  CrawlUtility::saveToAliOss( $this->uploadpath . $this->savename, $object_name,true,false,false,array('bucket_name'=>'wx135','domain_url' => 'http://image.135editor.com/'));
// 	               if( empty($uploainfo['fspath']) ) {
// 	                   @unlink($this->uploadpath . $this->savename);
// 	                   return false;
// 	               }
	               
	               /*$image_url = WeixinUtility::getImageMediaUrl($ret['media_id']);
	               if( $image_url ) {
	                   $uploainfo['fspath'] = $image_url;
	                   @unlink($this->uploadpath . $this->savename); //上传微信成功后，删除本地图片
	               }*/
	               return true;
	           }
	        }
	    }
	    elseif($type == 'wechatUploadimg') { // 上传图文消息内的图片获取URL【订阅号与服务号认证后均可用】
	        App::uses('WeixinUtility', 'Utility');
	        if($this->is_image) {
	            $ret = WeixinUtility::uploadImg( $this->uploadpath . $this->savename);
	            if(!empty($ret['url'])) {
	                $object_name = 'cache/remote/'.base64_encode( $ret['url'] );
	                $uploainfo['fspath'] = 'http://image7.135editor.com/'.$object_name; // 展示时才自动下载图片
	                /*$uploainfo['fspath'] =  CrawlUtility::saveToAliOss( $this->uploadpath . $this->savename, $object_name,true,false,false,array(
	                    'bucket_name'=>'wx135',
	                    'domain_url' => 'http://image.135editor.com/',
	                ));
	                if( empty($uploainfo['fspath']) ) {
	                    return false;
	                }*/
	                return true;
	            }
	            else{
	            	/*$uploainfo['fspath'] =  CrawlUtility::saveToQiniu( $this->uploadpath . $this->savename, $this->relativeUrl . $this->savename);
	            	if($uploainfo['fspath']) {
	            		return true;
	            	}*/
	            	
	                if(Configure::read('Storage.aliyun_oss')){//上传微信失败后，上传到阿里oss
	                    $image = new ImageResize($this->savename);
                        // 生成大图，大图会覆盖原图片。 防止图片过大造成页面加载缓慢。
                        $image->PicDir = $this->uploadpath;
                        mkdir_p($image->PicDir, 0755);
                        $image->newWidth  = Configure::read('Admin.max_image_width');
                        $image->newHeight = Configure::read('Admin.max_image_height');
                        $image->TmpName = $this->uploadpath . $this->savename;
                        $image->resize();
	                    
	                    $uploainfo['fspath'] =  CrawlUtility::saveToAliOss( $this->uploadpath . $this->savename, $this->relativeUrl . $this->savename,true,false,false,array('bucket_name'=>'wx135','domain_url'=>'http://image.135editor.com/'));
	                    if($uploainfo['fspath']) {
	                        return true;
	                    }
	                }
	            }
	        }
	    }
	    return false;
	}

	/**
	 * moves the file to the desired location from the temp directory
	 * @return boolean true if the file was successfully moved from the temporary directory to the desired destination on the filesystem
	 */
	public function write() {
		// Include libraries
		$moved = false;
		
		if(defined('SAE_MYSQL_DB')){
			$stor = new SaeStorage();
			$moved = $stor->upload(SAE_STORAGE_UPLOAD_DOMAIN_NAME , $this->relativePath.$this->savename , $this->params['form'][$this->file_post_name]['tmp_name']);
		}
		else{
			$folder = new Folder($this->uploadpath, true, 0755);
			$errors = $folder->errors(true);
			if (!empty($errors)) {
				$this->setError(1500, 'File system save failed.', 'Could not create requested directory: ' . $this->uploadpath);
			} else {
				if(file_exists($this->uploadpath . $this->savename)) {
					@unlink($this->uploadpath . $this->savename);
				}
				if (!($moved = move_uploaded_file($this->params['form'][$this->file_post_name]['tmp_name'], $this->uploadpath . $this->savename))) {
					$this->setError(1000, 'File system save failed.');
				}
			}
		}
		return $moved;
	}
	
	/**
	 * validates the post data and checks receipt of the upload
	 * @return boolean true if post data is valid and file has been properly uploaded, false if not
	 */
	public function validate() {
		$post_ok = isset($this->params['form'][$this->file_post_name]);
		$upload_error = $this->params['form'][$this->file_post_name]['error'];
		$got_data = (is_uploaded_file($this->params['form'][$this->file_post_name]['tmp_name']));
		
		if (!$post_ok){
			$this->setError(2000, 'Validation failed.', 'Expected file upload field to be named "Filedata."');
		}
		if ($upload_error){
			$this->setError(2500, 'Validation failed.', $this->getUploadErrorMessage($upload_error));
		}
		return !$upload_error && $post_ok && $got_data;
	}
	
	/**
	 * parses file upload error code into human-readable phrase.
	 * @param int $err PHP file upload error constant.
	 * @return string human-readable phrase to explain issue.
	 */
	public function getUploadErrorMessage($err) {
		$msg = null;
		switch ($err) {
			case UPLOAD_ERR_OK:
				break;
			case UPLOAD_ERR_INI_SIZE:
				$msg = ('The uploaded file exceeds the upload_max_filesize directive ('.ini_get('upload_max_filesize').') in php.ini.');
				break;
			case UPLOAD_ERR_FORM_SIZE:
				$msg = ('The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.');
				break;
			case UPLOAD_ERR_PARTIAL:
				$msg = ('The uploaded file was only partially uploaded.');
				break;
			case UPLOAD_ERR_NO_FILE:
				$msg = ('No file was uploaded.');
				break;
			case UPLOAD_ERR_NO_TMP_DIR:
				$msg = ('The remote server has no temporary folder for file uploads.');
				break;
			case UPLOAD_ERR_CANT_WRITE:
				$msg = ('Failed to write file to disk.');
				break;
			default:
				$msg = ('Unknown File Error. Check php.ini settings.');
		}
		
		return $msg;
	}
	
	/**
	 * sets an error code which can be referenced if failure is detected by controller.
	 * note: the amount of info stored in message depends on debug level.
	 * @param int $code a unique error number for the message and debug info
	 * @param string $message a simple message that you would show the user
	 * @param string $debug any specific debug info to include when debug mode > 1
	 * @return bool true unless an error occurs
	 */
	private function setError($code = 1, $message = 'An unknown error occured.', $debug = '') {
		$this->errorCode = $code;
		$this->errorMessage = $message;
		if (DEBUG) {
			$this->errorMessage .= $debug;
		}
		return true;
	}
	
	/*private $mime_array =  array( 
         "ez" => "application/andrew-inset", 
         "hqx" => "application/mac-binhex40", 
         "cpt" => "application/mac-compactpro", 
         "doc" => "application/msword", 
         "bin" => "application/octet-stream", 
         "dms" => "application/octet-stream", 
         "lha" => "application/octet-stream", 
         "lzh" => "application/octet-stream", 
         "exe" => "application/octet-stream", 
         "class" => "application/octet-stream", 
         "so" => "application/octet-stream", 
         "dll" => "application/octet-stream", 
         "oda" => "application/oda", 
         "pdf" => "application/pdf", 
         "ai" => "application/postscript", 
         "eps" => "application/postscript", 
         "ps" => "application/postscript", 
         "smi" => "application/smil", 
         "smil" => "application/smil", 
         "wbxml" => "application/vnd.wap.wbxml", 
         "wmlc" => "application/vnd.wap.wmlc", 
         "wmlsc" => "application/vnd.wap.wmlscriptc", 
         "bcpio" => "application/x-bcpio", 
         "vcd" => "application/x-cdlink", 
         "pgn" => "application/x-chess-pgn", 
         "cpio" => "application/x-cpio", 
         "csh" => "application/x-csh", 
         "dcr" => "application/x-director", 
         "dir" => "application/x-director", 
         "dxr" => "application/x-director", 
         "dvi" => "application/x-dvi", 
         "spl" => "application/x-futuresplash", 
         "gtar" => "application/x-gtar", 
         "hdf" => "application/x-hdf", 
         "js" => "application/x-javascript", 
         "skp" => "application/x-koan", 
         "skd" => "application/x-koan", 
         "skt" => "application/x-koan", 
         "skm" => "application/x-koan", 
         "latex" => "application/x-latex", 
         "nc" => "application/x-netcdf", 
         "cdf" => "application/x-netcdf", 
         "sh" => "application/x-sh", 
         "shar" => "application/x-shar", 
         "swf" => "application/x-shockwave-flash", 
         "sit" => "application/x-stuffit", 
         "sv4cpio" => "application/x-sv4cpio", 
         "sv4crc" => "application/x-sv4crc", 
         "tar" => "application/x-tar", 
         "tcl" => "application/x-tcl", 
         "tex" => "application/x-tex", 
         "texinfo" => "application/x-texinfo", 
         "texi" => "application/x-texinfo", 
         "t" => "application/x-troff", 
         "tr" => "application/x-troff", 
         "roff" => "application/x-troff", 
         "man" => "application/x-troff-man", 
         "me" => "application/x-troff-me", 
         "ms" => "application/x-troff-ms", 
         "ustar" => "application/x-ustar", 
         "src" => "application/x-wais-source", 
         "xhtml" => "application/xhtml+xml", 
         "xht" => "application/xhtml+xml", 
         "zip" => "application/zip", 
         "au" => "audio/basic", 
         "snd" => "audio/basic", 
         "mid" => "audio/midi", 
         "midi" => "audio/midi", 
         "kar" => "audio/midi", 
         "mpga" => "audio/mpeg", 
         "mp2" => "audio/mpeg", 
         "mp3" => "audio/mpeg", 
         "aif" => "audio/x-aiff", 
         "aiff" => "audio/x-aiff", 
         "aifc" => "audio/x-aiff", 
         "m3u" => "audio/x-mpegurl", 
         "ram" => "audio/x-pn-realaudio", 
         "rm" => "audio/x-pn-realaudio", 
         "rpm" => "audio/x-pn-realaudio-plugin", 
         "ra" => "audio/x-realaudio", 
         "wav" => "audio/x-wav", 
         "pdb" => "chemical/x-pdb", 
         "xyz" => "chemical/x-xyz", 
         "bmp" => "image/bmp", 
         "gif" => "image/gif", 
         "ief" => "image/ief", 
         "jpeg" => "image/jpeg", 
         "jpg" => "image/jpeg", 
         "jpe" => "image/jpeg", 
         "png" => "image/png", 
         "tiff" => "image/tiff", 
         "tif" => "image/tif", 
         "djvu" => "image/vnd.djvu", 
         "djv" => "image/vnd.djvu", 
         "wbmp" => "image/vnd.wap.wbmp", 
         "ras" => "image/x-cmu-raster", 
         "pnm" => "image/x-portable-anymap", 
         "pbm" => "image/x-portable-bitmap", 
         "pgm" => "image/x-portable-graymap", 
         "ppm" => "image/x-portable-pixmap", 
         "rgb" => "image/x-rgb", 
         "xbm" => "image/x-xbitmap", 
         "xpm" => "image/x-xpixmap", 
         "xwd" => "image/x-windowdump", 
         "igs" => "model/iges", 
         "iges" => "model/iges", 
         "msh" => "model/mesh", 
         "mesh" => "model/mesh", 
         "silo" => "model/mesh", 
         "wrl" => "model/vrml", 
         "vrml" => "model/vrml", 
         "css" => "text/css", 
         "html" => "text/html", 
         "htm" => "text/html", 
         "asc" => "text/plain", 
         "txt" => "text/plain", 
         "rtx" => "text/richtext", 
         "rtf" => "text/rtf", 
         "sgml" => "text/sgml", 
         "sgm" => "text/sgml", 
         "tsv" => "text/tab-seperated-values", 
         "wml" => "text/vnd.wap.wml", 
         "wmls" => "text/vnd.wap.wmlscript", 
         "etx" => "text/x-setext", 
         "xml" => "text/xml", 
         "xsl" => "text/xml", 
         "mpeg" => "video/mpeg", 
         "mpg" => "video/mpeg", 
         "mpe" => "video/mpeg", 
         "qt" => "video/quicktime", 
         "mov" => "video/quicktime", 
         "mxu" => "video/vnd.mpegurl", 
         "avi" => "video/x-msvideo", 
         "movie" => "video/x-sgi-movie", 
         "ice" => "x-conference-xcooltalk" 
      ); */
}
?>