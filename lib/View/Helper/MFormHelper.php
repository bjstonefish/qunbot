<?php
App::uses('FormHelper', 'View/Helper');
class MFormHelper extends FormHelper {
	
	var $helpers = array('Html','Js','Swfupload','Layout');
	var $_extschema = array();
	
	public function create($model = null, $options = array()) {
		
		$model_obj = loadModelObject ( $model );
		$this->_extschema[$model] = $model_obj->getExtSchema();
		
		$defaultOptions = array(
				'inputDefaults' => array(
						'div' => array(
								'class' => 'form-group'
						),
						'label' => array(
								'class' => 'col-sm-2 control-label'
						),						
						'class' => 'form-control',
				),
				'class' => 'form-horizontal',
				'role' => 'form',
		);
		
		if(!empty($options['inputDefaults'])) {
			$options = array_merge($defaultOptions['inputDefaults'], $options['inputDefaults']);
		} else {
			$options = array_merge($defaultOptions, $options);
		}
		return parent::create($model, $options);
	}
	
	public function input($fieldName, $options = array()) {
// 		print_r($options);
		$this->setEntity($fieldName);
		$modelKey = $this->model();
		$fieldKey = $this->field();  // 获取模块和字段，可修改到FormHelper中去
// 		print_r($options);
		if(!isset($options['div'])){
			$options['div'] = 'form-group';
		}
		elseif(is_array($options['div'])){
			$options['div']['class'] .= ' form-group';
		}
		elseif(!empty($options['div'])){
			$options['div'] .= ' form-group';
		}
		if(empty($options['class'])){
			$options['class'] = 'form-control';
		}

        if( $options['required'] ){
            $required = '<span class="required-star">*</span>';
        }
        else{
            $required = '';
        }

		if(!empty($this->_extschema[$modelKey][$fieldKey])){
			$fieldinfo = $this->_extschema[$modelKey][$fieldKey];
						
			if(!empty($fieldinfo['allownull']) && empty($options['required'])){
				$options['required'] = false;
			}
			if($fieldinfo['formtype']=='checkbox' || empty($fieldinfo['formtype']) && $fieldinfo['type']=='integer' && $fieldinfo['length']==1){
					//unset($options['class']);
					$options['class'] = '';
			}
			
			if(empty($options['type']) && !empty($fieldinfo['formtype']) && $fieldinfo['formtype']!='input' && !$options['is_editor']){
				// input时，为默认的text，不能指定type值为input
				// is_editor为true时,为编辑器ckeditor函数中调用，不能再将type设为ckeditor，防止重复调用，陷入死循环
				$options['type'] = $fieldinfo['formtype'];
			}
			
			if($options['type']=='checkbox'){
				$options ['multiple'] = 'checkbox';
			}


			
			if(!isset($options['after'])){
				$options['after'] = nl2br($fieldinfo['explain']);
			}
			if(!isset($options['label'])){
				$options['label'] = array('text'=>$fieldinfo['translate'].__(':').$required,'class'=>'col-sm-2 control-label');
			}
			if ($fieldinfo ['formtype'] == 'select' && $fieldinfo ['selectmodel'] && $fieldinfo ['selecttxtfield'] && $fieldinfo ['selecttxtfield']) {
				//$fieldid = Inflector::camelize ( $modelClass . '_' . $key . '_associate' );
				//$options ['after'] = '&nbsp;<span for="' . $fieldid . '">' . __ ( 'Filter', true ) . '</span>&nbsp;<input class="associate-text" style="width: 80px;" type="text" value="" id="' . $fieldid . '" />';
				if ($modelKey != $fieldinfo ['selectmodel']) {
					$addurl = $this->url ( '/admin/' . Inflector::tableize ( $fieldinfo ['selectmodel'] ) . '/add?model=' . $modelKey . '&select_parent_id=' . $fieldinfo ['selectparentid'] );
					$listurl = $this->url ( '/admin/' . Inflector::tableize ( $fieldinfo ['selectmodel'] ) . '/list?model=' . $modelKey . '&select_parent_id=' . $fieldinfo ['selectparentid'] );
						
					// $relateurl = $this->Html->url($relateurl);
					$options ['after'] .= '<div style="margin-top:5px;">';
					$options ['after'] .= '<a class="btn btn-default btn-sm" href="' . $addurl . '" onclick="open_dialog({title:\'' . __ ( 'New ' ) .__d('modelextend', $fieldinfo ['selectmodel'] ) . '\'},this.href);return false;">' . __ ( 'New ' ) . __d('modelextend',  $fieldinfo ['selectmodel'] ) . '</a>';
					$options ['after'] .= '&nbsp;&nbsp;&nbsp;<a class="btn btn-default btn-sm" href="' . $listurl . '" onclick="open_dialog({title:\'' . __d('modelextend', $fieldinfo ['selectmodel'] ). __( ' Management' ) . '\'},this.href);return false;">' . __d('modelextend',  $fieldinfo ['selectmodel'] ). __( ' Management' ) . '</a>';
					$options ['after'] .= '</div>';
				}
				$options['class'] = 'form-control';
				$options['div'] = array('class'=>'form-group form-inline');
			}
			if($fieldinfo['formtype']=='color') {
				$options['class'] = 'colorPicker form-control';
				$options['type']='text';
				$options['div'] = array('class'=>'form-group form-inline');//,'style'=>'position: relative;'
			}			
		}
		elseif( !isset($options['label']) ){
			// 此值在生成的语言缓存中，ToolsController::admin_updateLanCache
			$options['label'] = __d('i18nfield', $modelKey.'_'.$fieldKey).__(':').$required;
		}
		elseif( is_array($options['label']) && isset($options['label']['text']) ){
            $options['label']['text'] = $options['label']['text'].$required;
        }
		if( !is_array($options['label']) && $options['label']!=false ){
			$options['label'] = array('text'=> $options['label'].$required,'class'=>'col-sm-2 control-label');
		}

		if($options['type'] == 'textarea' || isset($options['rows'])){
			if($options['use_editor']===true){
				$txt = __('Use WYSIWYG Editor.');
				if($options['is_editor']){
					$txt = __('Destory WYSIWYG Editor.');
				}
				//before
				$options['after'].='<div class="clearfix"></div><div class="btn btn-default use_editor clearfix">'.$txt.'</div>';
			}
			if(!isset($options['cols'])){
				$options['cols']=60;
			}
			if(!isset($options['rows'])){
				$options['rows']=2;
			}
		}
		$options = $this->_parseOptions($options);
		if ($options['label'] !== false && $options['type']!='radio') {
			$options['between'] .='<div class="col-sm-10 controls '.$options['type'].'">';
			$options['after'] .='</div>';
		}
		if($options['type']=='checkbox'){ // 重设checkbox的格式
			$options ['format'] = array('before', 'label', 'between','input',  'after', 'error');
		}
		if($options['type']=='radio'){ // 重设checkbox的格式
			$options['before'] .='<div class="col-sm-10 controls '.$options['type'].'">';
			$options['after'] .='</div>';
		}
		return parent::input($fieldName,$options);
	}
	
	public function form($options = array(), $type = 'post') {
		
		$suffix = '_' . intval ( mt_rand () ); // 随机数作为表单各id的后缀，防止出现两个同模块的表单时，id相同。
		$htmlDefaults = array (
				'id' => 'form' . $suffix,
				'type' => $type,
				'class' => 'form-horizontal form'
		);	
		$options = array_merge ( $htmlDefaults, $options );
		if (empty ( $options ['form_type'] )){
			$options ['form_type'] = 'add';
		}
				
		$form_start = $this->create ( $options ['model'], $options );
		$formhtml = $advancedhtml = $seohtml = '';
		if (Configure::read ( $options ['model'] . '.advancedfield' )) {
			$advancedfields = explode ( ',', Configure::read ( $options ['model'] . '.advancedfield' ) );
		} else {
			$advancedfields = explode ( ',', Configure::read ( 'Modelextend.advancedfield' ) );
		}
		
		$seofields = explode ( ',', Configure::read ( 'Modelextend.seofield' ) );
		
// 		print_r($options);
		$modelClass = Inflector::classify ( $options ['model'] );
		
		$ui_tab_nav = $ui_tab_content = '';
		if (! empty ( $options ['auto_form'] )) {
			$formhtml = $advancedhtml = $seohtml =  '';
			// print_r($this);exit;
			// print_r($this->params['_flowstep_edit_fields']);
			$model_obj = loadModelObject ( $modelClass );
			$_extschema = $this->_extschema[$modelClass];
			if(empty($_extschema)){				
				$this->_extschema[$modelClass] = $_extschema = $model_obj->getExtSchema();
			}
			
			foreach ($_extschema as $key => $value ) {
				$field_html = $this->autoFormElement ( $modelClass, $key, $value, $options);
				if( $key=='name' && !empty( $model_obj->hasAndBelongsToMany ) ){
// 					$field_html .= $this->selectAssoc('Tag');
// 					print_r($model_obj->hasAndBelongsToMany);
					foreach($model_obj->hasAndBelongsToMany as $am => $value) {
					    $field_html .= $this->selectAssoc($am);
					}
				}
				if (in_array ( $key, $advancedfields ))
					$advancedhtml .= $field_html."\n";
				elseif (in_array ( $key, $seofields ))
					$seohtml .= $field_html."\n";
				else
					$formhtml .= $field_html."\n";
			}
			
			/*
			if ($model_obj->hasAndBelongsToMany) {
				foreach ( $model_obj->hasAndBelongsToMany as $key => $val ) {
					$formhtml .= $this->selectAssoc($val ['className']);
				}
			}*/
			if (isset ( $this->params ['_flowstep_edit_fields'] )) {
				$ui_tab_nav .= '<li class="active"><a href="#' . $modelClass . 'basic-info"><span>' . __ ( 'Basic', true ) . '</span></a></li>'."\n";
				// 流程中的增加与修改表单
				$ui_tab_content = '<div id="' . $modelClass . 'basic-info" class="tab-pane active"><fieldset>' . $formhtml . $advancedhtml . $seohtml . '</fieldset></div>';
			
			} else {
				$ui_tab_nav .= '<li class="active"><a href="#' . $modelClass . '-basic-info" data-toggle="tab" ><span>' . __ ( 'Basic Info' ) . '</span></a></li>'."\n";
				$ui_tab_nav .= '<li><a href="#' . $modelClass . '-advanced-info" data-toggle="tab" ><span>' . __ ( 'Advanced Options' ) . '</span></a></li>'."\n";
				
				$formhtml = "\n".'<div id="' . $modelClass . '-basic-info" class="tab-pane active"><fieldset>' . $formhtml . '</fieldset></div>'."\n";
				$advancedhtml = "\n".'<div id="' . $modelClass . '-advanced-info" class="tab-pane"><fieldset>' . $advancedhtml . '</fieldset></div>'."\n";
				
				if($seohtml){
					$ui_tab_nav .= '<li><a href="#' . $modelClass . '-seo-info" data-toggle="tab" ><span>' . __ ( 'SEO' ) . '</span></a></li>'."\n";
					$seohtml = "\n".'<div id="' . $modelClass . '-seo-info" class="tab-pane"><fieldset>' . $seohtml . '</fieldset></div>'."\n";
				}
				
				
				$ui_tab_content = $formhtml.$advancedhtml . $seohtml;
				$ui_tab_nav .= $this->Layout->getLanguageTabHead ( $modelClass );
				$ui_tab_content .= $this->Layout->getLanguageTabContent ( $modelClass );
			}

			/*** 模块的相关模块，生成相关模块的表单 ***/
			if ($model_obj->hasOne) {
				$has_one_html = '';
				foreach ( $model_obj->hasOne as $key => $val ) {
					$assoc_model = loadModelObject($val ['className']);
					$assoc_fields = $assoc_model->getExtSchema();
					foreach($assoc_fields as $afk => $afv){
						if($afk==$val ['foreignKey']){
							continue;
						}
						$has_one_html .= $this->autoFormElement($val ['className'], $afk, $afv, $options);
					}

					$ui_tab_nav .= '<li><a href="#has-one-'.$val ['className'].'" data-toggle="tab" ><span>' . __d('modelextend','Model_'.$val ['className']). '</span></a></li>'."\n";
					$ui_tab_content .= '<div id="has-one-'.$val ['className'].'" class="tab-pane"><fieldset>'.$has_one_html.'</fieldset></div>';
				}


			}



			if ($ui_tab_nav)
				$ui_tab_nav = '<ul class="nav nav-tabs">' . $ui_tab_nav . '</ul>'."\n";
			
			//$formhtml .= $this->submit ( __ ( 'Submit') );
			// echo '---------------------';print_r($params);
		}
		if (! isset ( $options ['form_type'] ))
			$options ['form_type'] = 'add';
		
// 		$formhtml .= $this->buildAssociate ( $modelClass, $options ['form_type'] ); // 生成级联操作的js代码
		$form_submit = '<div class="form-group">
	    	<label class="col-sm-2 control-label"></label>
	    	<div class="col-sm-10 controls">'.$this->submit(__('Submit'),array('id'=>'btnsubmit')).'</div>
	    </div>';
		return $form_start.'<fieldset>'.$ui_tab_nav.'<div class="tab-content">'.$ui_tab_content.'</div>'.$form_submit.'</fieldset>'.$this->end().$script;
	}
	/**
	 *
	 * @param <type> $modelClass
	 *        	模块的名字
	 * @param <type> $key
	 *        	字段名
	 * @param <type> $value
	 *        	extschema中字段的描述及值
	 * @param <type> $params
	 *        	生成form时传入的参数
	 * @return string
	 */
	function autoFormElement($modelClass, $key, $value, $params) {
		if ($params ['form_type'] == 'add' && ! $value ['allowadd']) {
			return;
		} elseif ($params ['form_type'] == 'edit' && ! $value ['allowedit']) {
			return;
		}
		if ($key != 'id' && isset ( $this->params ['_flowstep_edit_fields'] ) && ! in_array ( $key, $this->params ['_flowstep_edit_fields'] )) {
			return;
		}
		
		$options = array ();
		$options ['label'] = $value ['translate'];
		if (in_array ( $value ['formtype'], array (
				'input',
				'datetime' 
		) )) {
			$options ['type'] = 'text';
		} elseif ($value ['formtype'] == 'checkbox') {
			$options ['type'] = 'select';
			if (! ($params ['form_type'] == 'edit' && $value ['explodeimplode'] == 'explode')) {
				$options ['multiple'] = 'checkbox';
				$options ['div'] = array (
					'id' => Inflector::camelize ( $modelClass . '_' . $key . '_Checkboxs' ) 
				);
			}
		} elseif ($value ['formtype']) {
			$options ['type'] = $value ['formtype'];
		}
		
		if ($value ['formtype']=='select' && !empty($value ['selectmodel'])) {
// 			print_r($value);
			$props = array();			
			if(!empty($value['conditions'])){
				$query = xml_to_array($value['conditions']);
				$props = $query['options'];
			}
			if(!empty($value['selectparentid'])){
				if ($value['associatetype'] == 'treenode') {
					$selectmodel_name = Inflector::classify($value['selectmodel']);
					if (!$this->{$selectmodel_name} || !($this->{$selectmodel_name} instanceof AppModel)) {
						$this->{$selectmodel_name} = loadModelObject($selectmodel_name);
					}
					$rootcate = $this->{$selectmodel_name}->findById($value['selectparentid']);
					$props['conditions']['left >'] = $left = $rootcate[$selectmodel_name]['left'];
					$props['conditions']['right <'] = $right = $rootcate[$selectmodel_name]['right'];
				}
				else{
					$props['conditions']['parent_id'] = $value['selectparentid'];
				}
			}
			
			$conditions = http_build_query($props);
			
			$options ['class'] = 'chzn-select select';
			$options ['data-rel'] = 'chosen';
			$options ['data-url'] = $this->url ( '/admin/ajaxes/associate' ).
				'?valuefield=' . $value ['selectvaluefield'].
				'&selectmodel=' . $value ['selectmodel'] . 
				'&txtfield='. $value ['selecttxtfield'] . 
				'&associatefield=' . $value ['associatefield'] . 
				'&associatetype=' . $value ['associatetype'].
				'&'.$conditions;
		}
		
		$options ['id'] = Inflector::camelize ( $modelClass . '_' . $key );
		$options ['after'] = '';
		if (isset ( $value ['default'] )) {
			$options ['default'] = $value ['default'];
		}
		
		// append field explain text
		if (! empty ( $value ['explain'] )) {
			$options ['after'] .= '<div>' . nl2br($value ['explain']) . '</div>';
		}
		
		if($value['formtype']=='coverimg'){			
			$img = $options['value']?$options['value']:('nophoto.gif');			
			$options['after'] = $this->Html->image($img,array('width'=>120,'id'=>$options['id'].'Preview'));
			//$options['type'] = 'hidden';
			$options['class'] = 'form-control hidden';
			return $field_html = $this->input($key, $options);;
		}
		elseif ($value ['formtype'] == 'datetime') {
			// 生成时间类型的字段的表单内容
			$fieldid = Inflector::camelize ( $modelClass . '_' . $key );
			
			if ('{$now}' == $value ['default'] || in_array ( $key, array (
					'created',
					'updated' 
			) )) {
				$ymd = date ( 'Y-m-d' );
			} else {
				$ymd = '';
			}
			
			if (isset ( $this->data [$modelClass] [$key] ) ) {
				if ( ! is_array ( $this->data [$modelClass] [$key] )) {
					$time_array = explode ( ' ', $this->data [$modelClass] [$key] );
					$ymd = $time_array [0];
					$his = $time_array [1];
				} else{
					$ymd = $this->data [$modelClass] [$key] ['ymd'];
					$his = $this->data [$modelClass] [$key] ['his'];
				}
			}
			else{
				$his = $params['his'] ? $params['his'] : date ( 'H:i:s' );
			}
			
			$on_select = '';
			if ($key == 'starttime') {
				$on_select .= '$("[id$=\'EndtimeYmd\']").datepicker("option", "minDate", date);';
			} elseif ($key == 'endtime') {
				$on_select .= '$("[id$=\'StarttimeYmd\']").datepicker("option", "maxDate", date);';
			} elseif ($key == 'created') {
				$on_select .= '$("[id$=\'UpdatedYmd\']").datepicker("option", "minDate", date);';
			} elseif ($key == 'updated') {
				$on_select .= '$("[id$=\'CreatedYmd\']").datepicker("option", "maxDate", date);';
			}
			$datetimehtml = '<div class="form-group">
					<label class="col-sm-2 control-label">' . $value ['translate'] . '</label>
					<div class="col-sm-10 controls text">
					<input class="datepicker" type="text" id="' . $fieldid . 'Ymd" name="data[' . $modelClass . '][' . $key . '][ymd]" value="' . $ymd . '">
					<input type="text" id="' . $fieldid . 'His" name="data[' . $modelClass . '][' . $key . '][his]" value="' . $his . '">
					</div>
					</div>';
			$script = '$(document).ready(function(){ 
						$("#' . $fieldid . 'Ymd").datepicker({
							showButtonPanel: true,dateFormat:\'yy-mm-dd\',
							changeMonth: true,
							changeYear: true,
							onSelect: function(selectedDate) {
								var instance = $(this).data("datepicker");
								var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
								' . $on_select . '
							}
								
						});
						$("#ui-datepicker-div").css("zIndex",100000);
					});';
			//$this->_View->append('bottomscript',$script);
			return $datetimehtml."<script>$script</script>";
			
		} elseif ($value ['formtype'] == 'ckeditor') {
		    
		    // ckeditor
			$field_html = $this->editor135( $modelClass . '.' . $key, array (
// 					'div' => false,
// 					'label' => false, //不显示描述文字时，如果有多个ckeditor的编辑字段，则分不清哪个是哪个。
					'is_admin' => $this->request->params['admin'],
			) );
			return $field_html;
		} else {
			$field_html = '';
			if ($value ['formtype'] == 'file') {
				$field_html = $this->swfupload($key, $modelClass);
				//$field_html = $this->Swfupload->load ( $key, $modelClass ); // 文件上传类型
			} else {
				if ($key == 'id') {
					$varName = Inflector::tableize ( $modelClass );
					$varOptions = $this->_View->getVar ( $varName );
					if (is_array ( $varOptions )) {
						if ($options ['type'] !== 'radio') {
							$options ['type'] = 'select';
						}
						$options ['options'] = $varOptions;
					}
					$options ['selected'] = $this->_View->getVar ( 'selected_' . $varName );
				}
				if($value['type'] == 'integer' && $value['length']==1 && !$value['formtype']){
					//tinyint长度为1的，未设置类型的设置checkbox
					$options['type'] = 'checkbox';
					$options['class'] = '';
				}
				
				$field_html = $this->input ( $modelClass . '.' . $key, $options );
				
			}
			return $field_html;
		}
	}
	/**
	 * 创建级联下拉的选项加载js代码，
	 * 关联其它模块的数据搜索
	 */
	function buildAssociate($modelClass, $form_type = 'add') {
		if(empty($modelClass)){
			return false;
		}
		// echo '<pre>'; print_r($this->data);
		// $modelClass = Inflector::classify($this->params['controller']);
		$js_str = '';
		$onload_trigger_changefields = array (); // 页面加载完成后，自动触发onchange事件的字段
		$_extschema = $this->_extschema[$modelClass];
		if(empty($_extschema)){
			$model_obj = loadModelObject ( $modelClass );
			$this->_extschema[$modelClass] = $_extschema = $model_obj->getExtSchema(); // $this->params['_extschema']
		}
		
		foreach ( $_extschema as $key => $value ) {
			// echo "associateelement";
			if ($value ['onchange']) {
				$fieldid = Inflector::camelize ( $modelClass . '_' . $value ['name'] ); // .$suffix;
				$js_str .= '
					$("#' . $fieldid . '").change(function(){
						' . $value ['onchange'] . '
					});
					';
				if ($form_type == 'edit') {
					$js_str .= '$("#' . $fieldid . '").trigger("change");'; // 编辑页面加载时，触发onchange事件
				}
			}
			if (in_array ( $value ['formtype'], array (
					'select',
					'checkbox' 
			) )) {
				if (! $value ['selectmodel'] || ! $value ['selecttxtfield'] || ! $value ['selecttxtfield']) {
					continue;
				}
				
				if ($value ['associateflag'] && $value ['associatefield']) {
					// 选项的级联效果
					$fieldid = Inflector::camelize ( $modelClass . '_' . $value ['associateelement'] ); // .$suffix;
					$target_id = Inflector::camelize ( $modelClass . '_' . $value ['name'] ); // .$suffix;
					$successjs = '';
					// if(!($params['form_type'] == 'edit' &&
					// $value['explodeimplode']=='explode'))
					// 表单类型为checkbox，explodeimplode值为explode，表示批量添加内容。不支持批量修改。修改时作为select来处理
					if ($value ['formtype'] == 'checkbox' && ! ($form_type == 'edit' && $value ['explodeimplode'] == 'explode')) {
						$div_id = Inflector::camelize ( $modelClass . '_' . $key . '_Checkboxs' ); // .$suffix;
						$successjs = '
							$("#' . $div_id . ' .checkbox").remove();
							$(data).each(function(i){
								if(data[i].value=="")
								{
									data[i].text = "' . __ ( 'Select All', true ) . '"
								}
								$(\'<div class="checkbox"><input type="checkbox" id="' . $modelClass . $key . '_\'+i+\'" value="\'+data[i].value+\'" name="data[' . $modelClass . '][' . $key . '][]"><label for="' . $modelClass . $key . '_\'+i+\'">\'+data[i].text+\'</label></div>\').appendTo("#' . $div_id . '");
							});
						';
						if ($form_type == 'edit') {
							
							if (isset ( $this->data [$modelClass] [$key] ) && is_array ( $this->data [$modelClass] [$key] ) && ! empty ( $this->data [$modelClass] [$key] )) {
								$selectvalue_jsarray = '[';
								foreach ( $this->data [$modelClass] [$key] as $val ) {
									$selectvalue_jsarray .= '"' . str_replace ( '"', '\"', $val ) . '",';
								}
								$selectvalue_jsarray = substr ( $selectvalue_jsarray, 0, - 1 ) . ']';
							} else {
								$selectvalue_jsarray = '[]';
							}
							
							// echo $selectvalue_jsarray;
							// print_r($modelClass);echo
							// "======";print_r($value['associateelement']);
							$successjs .= '
								if($("#' . $fieldid . '").val()=="' . $this->data [$modelClass] [$value ['associateelement']] . '")
								{
									var select_values = ' . $selectvalue_jsarray . ';
									//alert($("#' . $div_id . ' input[type=checkbox]").size());
									$("#' . $div_id . ' input[type=checkbox]").each(function(i){
										if($.inArray(this.value,select_values)!=-1)
										{
											$(this).attr("checked",true);
										}
										return ;
									});
								}
							';
						}
					} else {
						$successjs = '
								$("#' . $target_id . '").html("");
								$(data).each(function(i){	
									var opt=document.createElement("OPTION"); 
									$("#' . $target_id . '").get(0).options.add(opt); 
									opt.value = data[i].value; 
									opt.text = data[i].text; 
									
								});
								
								';
						if ($form_type == 'edit') {
							if (is_array ( $this->data [$modelClass] [$key] )) {
								// 为数组时，表示字段允许批量添加的，值被转换成了数组。
								// 修改单条时，数组只有一个元素。不提供批量修改的checkbox，取数组的第一个即可。
								$this->data [$modelClass] [$key] = array_shift ( $this->data [$modelClass] [$key] );
							}
							$successjs .= '
								if($("#' . $fieldid . '").val()=="' . $this->data [$modelClass] [$value ['associateelement']] . '")
								{
									$("#' . $target_id . '").val("' . $this->data [$modelClass] [$key] . '");
								}
							';
						}
						$successjs .= '
							$("#' . $target_id . '").trigger("change");
							';
					}
					
					$onload_trigger_changefields [] = $fieldid;
					// 当为编辑时的表单时，自动触发级联上级的onchange事件，使触发下级的动作
					
					$js_str .= '
					$("#' . $fieldid . '").change(function(){
						if(this.value==""){
							$("#' . $target_id . '").html("");
							$("#' . $target_id . '").trigger("change");
							return false;
						}
						$.ajax({
							type:"post", 
							dataType: "json",
							data:{"valuefield":"' . $value ['selectvaluefield'] . '","ass_val":this.value,"selectmodel":"' . $value ['selectmodel'] . '","txtfield":"' . $value ['selecttxtfield'] . '","associatefield":"' . $value ['associatefield'] . '","associatetype":"' . $value ['associatetype'] . '"},
							url:"' . $this->url ( '/admin/ajaxes/associate' ) . '",
							success: function(data){								
				                	' . $successjs . '
							}
						});
					});
					';
				
				}
				
				if ($value ['associateflag'] && $value ['associatefield']) {
					$data_str = '{"txt_filter":this.value,"valuefield":"' . $value ['selectvaluefield'] . '","ass_val":$("#' . $fieldid . '").val(),"selectmodel":"' . $value ['selectmodel'] . '","txtfield":"' . $value ['selecttxtfield'] . '","associatefield":"' . $value ['associatefield'] . '","associatetype":"' . $value ['associatetype'] . '"}';
					// 级联上级的条件，和筛选条件一起传入
				} else {
					// $data_str =
					// '{"valuefield":"'.$value['selectvaluefield'].'","ass_val":this.value,"selectmodel":"'.$value['selectmodel'].'","txtfield":"'.$value['selecttxtfield'].'","associatefield":"'.$value['selecttxtfield'].'","associatetype":"like"}';
					$data_str = '{"txt_filter":this.value,"valuefield":"' . $value ['selectvaluefield'] . '","ass_val":this.value,"selectmodel":"' . $value ['selectmodel'] . '","txtfield":"' . $value ['selecttxtfield'] . '"}';
				}
				// 为选项加一个文本的筛选框，输入文本内容即可搜索筛选。筛选框js事件keyup
				$fieldid = Inflector::camelize ( $modelClass . '_' . $value ['name'] . '_associate' ); // .$suffix;
				$target_id = Inflector::camelize ( $modelClass . '_' . $value ['name'] ); // .$suffix;
				                                                                  
				// 追加字段其他的查询条件
				$xam_array = xml_to_array ( $value ['conditions'] );
				$condition_url = '';
				if (! empty ( $xam_array ['options'] ['conditions'] )) {
					foreach ( $xam_array ['options'] ['conditions'] as $ck => $cv ) {
						$condition_url .= 'conditions[' . rawurlencode ( $ck ) . ']=' . rawurlencode ( $cv ) . '&';
					}
					$condition_url = substr ( $condition_url, 0, - 1 );
				}
				
				$js_str .= '
					$("#' . $fieldid . '").keyup(function(){
						$.ajax({
							type:"post", 
							dataType: "json",
							data:' . $data_str . ',
							url:"' . $this->url ( '/admin/ajaxes/associate' ) . '?' . $condition_url . '",
							success: function(data){
								$("#' . $target_id . '").html("");
								$(data).each(function(i){
				                	//alert(data[i].text);
				                	var opt=document.createElement("OPTION"); 
									$("#' . $target_id . '").get(0).options.add(opt); 
									opt.value = data[i].value; 
									opt.text = data[i].text; 
								});
								$("#' . $target_id . '").trigger("change");
							}
						});
					});
				';
			}
		}
		if (! empty ( $onload_trigger_changefields )) {
			$onload_trigger_changefields = array_unique ( $onload_trigger_changefields );
			foreach ( $onload_trigger_changefields as $fieldid ) {
				// 当为编辑时的表单时，自动触发级联上级的onchange事件，使触发下级的动作
				if ($form_type == 'edit') {
					$js_str .= '
					$("#' . $fieldid . '").trigger("change");';
				}
			}
		}
		$js_str = '
				$(document).ready(function(){
					' . $js_str . '
				});
		';
		return $this->Html->scriptBlock($js_str);
	}

	/**
	 * 新增的标题图片表单项
	 * @param string $fieldName 字段名
	 * @return string form表单项
	 */
	public function titleImage($fieldName){
		
		$this->setEntity($fieldName);
		$options = array();
		$options = $this->_initInputField($fieldName, $options);
		$id = $options['id'].'Preview';		
		$modelKey = $this->model();
		if(empty($options['value'])){
			$options['value']='nophoto.gif';/*默认图片*/
		}
		
		$img = $options['value']?$options['value']:('nophoto.gif');
		$options['after'] = $this->Html->image($img,array('width'=>120,'id'=>$options['id'].'Preview'));
		$options['class'] = 'form-control hidden';
		$html = $this->input($fieldName, $options);;
		return $html;
	}
	
	public static $ck_js_loaded = false;
	
	function ckeditor($fieldName,$options=array()){
	    
	    return $this->editor135($fieldName,$options);
	    
		$options = $this->_initInputField($fieldName, $options);
		$id = $options['id'];		
		$model = $this->defaultModel;
		
		$js_files = '';
		if(!self::$ck_js_loaded){
			$js_files = $this->Html->script(array(
					'ckeditor/ckeditor.js',
			));
			self::$ck_js_loaded = true;
		}
		if(!isset($options['is_admin'])){
			$options['is_admin'] = $this->request->params['admin'];
			$options['affix'] = 1;			
		}
		
		
		
		$script = $js_files."<script type=\"text/javascript\">
		$(function(){
			if(typeof CKEDITOR.instances['$id']!='undefined'){
				//CKEDITOR.instances['$id'].destroy(); // 取消编辑器效果，替换为之前的textarea
				CKEDITOR.remove(CKEDITOR.instances['$id']); // 销毁删除对象
			}
			
			".($options['is_admin'] ? "ckeditors['$id'] = CKEDITOR.replace('$id');" : "
					CKEDITOR.editorConfig = function( config ){
						config.toolbar = 'FRONT';
					}
					ckeditors['$id'] = CKEDITOR.replace('$id',
				    {
				        customConfig : '".$this->Html->url("/js/ckeditor/front-config.js")."'
				    });
			")."
			
			ckeditors['$id'].on('focus',function(e){
				current_ckeditor_instance = e.editor;
			});
			
			/**空格与空行导致有些样式错乱，需要紧挨着的中间出现了空白间隔**/
			
			ckeditors['$id'].on( 'setData', function( evt ){ //删除标签中的空格与换行 
				var str = evt.data.dataValue;
				str = str.replace(/(\\n)/g, \"\");  
				str = str.replace(/(\\t)/g, \"\");  
				str = str.replace(/(\\r)/g, \"\"); 
				var obj = $('<div>'+str+'</div>');
				obj.contents().filter(function() {
					return this.nodeType === 3 && $.trim($(this).text()) == \"\";
				}).remove();
				
				obj.find('p').each(function(){
					if($(this).html()=='&nbsp;') {
						$(this).html('<br/>');
					}
				})
				
				evt.data.dataValue = obj.html();
			} );
			ckeditors['$id'].on( 'getData', function( evt ){ //删除标签中的空格与换行 
				var str = evt.data.dataValue;
				str = str.replace(/(\\n)/g, \"\");  
				str = str.replace(/(\\t)/g, \"\");  
				str = str.replace(/(\\r)/g, \"\"); 
				var obj = $('<div>'+str+'</div>');
				obj.contents().filter(function() {
					return this.nodeType === 3 && $.trim($(this).text()) == \"\";
				}).remove();
				obj.find('p').each(function(){
					if($(this).html()=='&nbsp;') {
						$(this).html('<br/>');
					}
				})
				evt.data.dataValue = obj.html();
			} );
			CKEDITOR.on( 'instanceReady', function( e ){
				//e.editor.document.appendStyleSheet( '".Router::url('/../stylevars/getcss.css')."' );
				//e.editor.document.appendStyleSheet( '".$this->webroot(CSS_URL.'/ui-customer.css')."' );
				//e.editor.setMode( 'source' );
						
				e.editor.on( 'mode', function( e ){
					if(e.editor.mode == 'wysiwyg'){
						//e.editor.document.appendStyleSheet( '".Router::url('/../stylevars/getcss.css')."' );
						//e.editor.document.appendStyleSheet( '".$this->webroot(CSS_URL.'/ui-customer.css')."' );
					}
				});
								".($options['affix']?"
				//$('.cke_top').width($('.cke_top').width());
				if($('.cke_top').parent('ui-dialog-content').size()<1){
					$('.cke_top').addClass('clearfix').affix(); 
				}						
										":'')."
				
			});
			
		});
		</script>
		";
		$this->_View->append('bottomscript',$script);
		$options['type'] = 'textarea';
		$options['is_editor'] = true;
		$html = $this->input($fieldName,$options);
		return $html;
	}
	
	public static $e135_js_loaded = false;
	
	public function editor135($fieldName,$options=array()){
		$options = $this->_initInputField($fieldName, $options);
		$id = $options['id'];		
		$model = $this->defaultModel;
		
		$js_files = '';
		if(!self::$e135_js_loaded){
		    if($options['is_admin']) {
		        $js_files = $this->Html->script(array(
		            '/js/ueditor/ueditor.admin.config.js',
		            '/js/ueditor/ueditor.all.min.js',
		            '/open/a92d301d77.js',
		        ));
		    }
		    else{
		        $js_files = $this->Html->script(array(
		            '/js/ueditor/ueditor.config.js',
		            '/js/ueditor/ueditor.all.min.js',
		            '//static.135editor.com/open/a92d301d77.js',
		        ));
		    }
			
			$js_files .= $this->Html->css('//static.135editor.com/open/v1/96619a5672.css');
			self::$e135_js_loaded = true;
		}
		
		$script = $js_files."<script type=\"text/javascript\">
		$(function(){			
			current_editor = UE.getEditor('$id',{
            	initialFrameHeight:400,
            	open_editor: true,
            	pageLoad:true,
            	appkey:'566d34e0-4bac-443b-8a80-37f89fde689a',
            	zIndex : 1000,
            	page_url : '//www.135editor.com/editor_styles/open_styles?inajax=1&appkey=566d34e0-4bac-443b-8a80-37f89fde689a',
            	style_url : '//www.135editor.com/editor_styles/open?inajax=1&v=page&appkey=566d34e0-4bac-443b-8a80-37f89fde689a',
            	focus:true,
	            focusInEnd:true
            });";
            
		
		if($options['is_admin']) {
		    $script .= "
setTimeout(function(){
		        
    current_editor.ready(function(){
		var editor_document = current_editor.document;        
		        var link = editor_document.createElement('link');
                link.rel = 'stylesheet';
                link.type = 'text/css';
                link.href = '".Router::url('/cache/bootstrap_'.Configure::read('Site.style').'.css')."';
                editor_document.getElementsByTagName('head')[0].appendChild(link);
                    
		        var link1 = editor_document.createElement('link');
                link1.rel = 'stylesheet';
                link1.type = 'text/css';
                link1.href = '".$this->webroot(CSS_URL.'/ui-customer.css')."' ;
                editor_document.getElementsByTagName('head')[0].appendChild(link1);
                    
                var link1 = editor_document.createElement('link');
                link1.rel = 'stylesheet';
                link1.type = 'text/css';
                link1.href = '".$this->webroot(CSS_URL.'/font-awesome.min.css')."' ;
                editor_document.getElementsByTagName('head')[0].appendChild(link1);
                
    });
},500);
		    ";
		}
		$script .= "});</script>";
		
		$this->_View->append('bottomscript',$script);
		$options['type'] = 'textarea';
		$options['is_editor'] = true;
		
		if(!empty($this->_extschema[$model][$fieldName])){
		    $fieldinfo = $this->_extschema[$model][$fieldName];
    		if(!isset($options['label'])){
    		    $options['label'] = $fieldinfo['translate'];
    		}
		}
		
		//print_r($options); print_r($this->data);
		$html = '<div class="form-group">
		    <label class="col-sm-2 control-label">'.$options['label'].'</label>
		    <div class="col-sm-12 controls number">
		    <textarea id="'.$id.'" name="'.$options['name'].'" type="text/plain">'.htmlspecialchars($options['value']).'</textarea>
		    </div>
		</div>';
		//$html = $this->input($fieldName,$options);
		return $html;
	}
	
	public static $js_loaded = false;
	
	function swfupload($file_post_name = 'Filedata',$param = array()) {
	
		$this->setEntity($file_post_name);
		$size = 0;
		if( isset($param['size']) && intval($param['size']) ) {
		    $size = intval($param['size']);
		}
		else {
		    $size = Configure::read('Download.upload_limit');
		}
		if(empty($size)) {
		    $size = 10;
		}
		if(!is_array($param)){
			$param = array('modelClass'=>$param);
		}
		if(empty($param['modelClass'])){
			$param['modelClass']=$this->model();
		}
		if(strpos($file_post_name,'.')===false){
			$file_post_name = $param['modelClass'].'.'.$file_post_name;
		}
	
		$fieldid = Inflector::camelize (str_replace('.','_',$file_post_name));
	
		$fieldname = $this->field(); //$file_post_name可能含“.”,经过field函数处理后，不含‘.’，得到name值
		$hide_options = array('id'=>$fieldid);
		$hide_options['after']=$param['after'];
		if($param['value']){
			$hide_options['value']= $param['value'];
		}
		
	
		if(!empty($param['modelClass'])){
			$ext_schemas = $this->_extschema[$param['modelClass']];
			if(empty($ext_schemas)){
				$obj = loadModelObject($param['modelClass']);
				$this->_extschema[$param['modelClass']] = $ext_schemas = $obj->getExtSchema();
			}
			if($ext_schemas[$fieldname] && !empty($ext_schemas[$fieldname]['savetodb'])){
				// 字段选择的是savetodb时，字段值只保存到本模块的数据
				$param['no_db'] = 1;
				$param['upload_limit'] = 1;
				$param['upload_success_handler'] = 'uploadSuccess_'.$fieldid;
			}
		}

		$hide_options['type'] = 'text'; 
		$hide_options['label'] = $hide_options['div'] = false;
		$hide_options['placeholder'] = __('Upload a file or input a file url.');
		// 仅上传一个，或者上传多个但数据库字段存在时，显示地址框
		if($param['upload_limit'] == 1 || isset($ext_schemas[$fieldname]) )
		{
			$hidden = $this->input($file_post_name,$hide_options);
		}
		else{

			$hidden = $this->input($file_post_name,$hide_options); //hidden
		}
		
		
		$param = array_merge(array(
				'modelClass'=> 'Article',
				'isadmin' => true,
				'label' => __d('i18nfield', $param['modelClass'].'_'.$fieldname),
				'after' => '',//描述
				'upload_limit'=> 0, // 最多允许上传的文件数，0为不限制
				'file_types'=> "*.*",
				'file_types_description'=> 'All Files',
				'button_image_url'=> '/img/uploadbutton.png', // 上传按钮描述
				'button_width'=> 110, // 上传按钮宽度
				'button_height'=> 35, // 上传按钮高度
				'no_db' =>0, // 是否保存到数据库
				'no_thumb'=>1,//图片不生成缩略图
				'save_folder' => '', // 保存地址
				'fieldid' => '',
				'with_list_div' => true,
				'upload_success_handler'=>'uploadSuccess', //回调函数
				'return_type'=> 'json',// html or json
				'withprogress'=>true // 是否显示上传进度
		),$param);
		//extract($param); // 变量混淆方法不支持extract，直接用数组来使用变量
	
		if($param['no_db'] && $param['upload_success_handler']=='uploadSuccess'){
			$param['upload_success_handler'] = 'uploadSuccess_'.$fieldid;
		}
		$listfile = '';
		if(!empty($this->data[$param['modelClass']][$fieldname]) || !empty($param['value'])){
			// 上传文件的地址保存到本模块对应的字段中，而非保存在Uploadfile里
			// UPLOAD_FILE_URL 可能含有“http://”
			if($param['value']){
				if(substr($param['value'],0,7) != 'http://'){
					$file_url = UPLOAD_FILE_URL.($param['value']);
				}
				else{
					$file_url = $param['value'];
				}
				//$file_url = str_replace('//','/',UPLOAD_FILE_URL.$param['value']);
			}
			else{
				if(substr($this->data[$param['modelClass']][$fieldname],0,7) != 'http://'){
					$file_url = UPLOAD_FILE_URL.($this->data[$param['modelClass']][$fieldname]);
				}
				else{
					$file_url = $this->data[$param['modelClass']][$fieldname];
				}
			}
			if(is_image($file_url)){
				$listfile = '<div class="col-sm-12"><a href="'.$file_url.'" title="'.__( 'Preview').'" target="_blank"><img src="'.$file_url.'" style="max-height:120px"/></a></div>';
			}
			else{
				$listfile = '<div class="col-sm-12"><a href="'.$file_url.'" target="_blank">'.__( 'Preview').'</a></div>';
			}
		}
		// print_r($this->data['Uploadfile']);
		if(isset($this->data['Uploadfile']) && is_array($this->data['Uploadfile']) && is_array($this->data['Uploadfile'][$fieldname])){
			foreach($this->data['Uploadfile'][$fieldname] as $uploadfile){
				if($uploadfile['fieldname']==$fieldname){
					$listfile.='<li class="upload-fileitem clearfix" id="upload-file-'.$uploadfile['id'].'">';
					if(substr($uploadfile['fspath'],0,7) != 'http://'){
						$file_url = UPLOAD_FILE_URL.($uploadfile['fspath']);
					}
					else{
						$file_url = $uploadfile['fspath'];
					}
					if('image' == substr($uploadfile['type'],0,5)){
						$listfile.='<img style="float:left;" src="'.$file_url.'" max-height="100px" max-width="100px"/>';
					}
					if('image' == substr($uploadfile['type'],0,5)){
						$listfile.='<div style="margin-left:100px" class="form-inline">';
					}
					$listfile.='<input type="hidden" name="data[Uploadfile]['.$uploadfile['id'].'][id]" value="'.$uploadfile['id'].'">
	        		<p><label>'.__ ( 'Uploadfile Name').'</label>: <input type="text" class="form-control" name="data[Uploadfile]['.$uploadfile['id'].'][name]" value="'.urldecode($uploadfile['name']).'"/> &nbsp;
	        			<label>'.__ ( 'Sort Order').'</label>: <input style="width:30px;" class="form-control" type="text" name="data[Uploadfile]['.$uploadfile['id'].'][sortorder]" value="'.$uploadfile['sortorder'].'"/> &nbsp;
	        			<label>'.__ ( 'File Version').'</label>: <input style="width:60px;" class="form-control" type="text" name="data[Uploadfile]['.$uploadfile['id'].'][version]" value="'.$uploadfile['version'].'"/>
	        		</p>
	        		<p><label title="'.__('Used as image click url').'">'.__ ( 'Link url').':</label> 
	        			<input type="text" class="form-control" name="data[Uploadfile]['.$uploadfile['id'].'][link]" value="'.$uploadfile['link'].'"/></p>
	        		<p><label>'.__ ( 'Uploadfile description').'</label>:<textarea class="form-control" name="data[Uploadfile]['.$uploadfile['id'].'][description]" rows="1" cols="60">'.$uploadfile['description'].'</textarea></p>
	        		 <p><a href="'.$file_url.'" target="_blank">' . __ ( 'Preview') . '</a>
	        		<a href="javascript:void(0);" onclick="insertHTML(\'&lt;img id=&#34;file_'.$uploadfile['id'].'&#34; src=&#34;'.($file_url).'&#34; >\')">'.__('Insert Editor').'</a>
		
	        		<a class="upload-file-delete" rel="'.$uploadfile['id'].'" href="#" data-url="'.$this->url('/admin/uploadfiles/delete/'.$uploadfile['id'].'.json').'">'.__('Delete').'</a>
	        		<a href="javascript:void(0);" onclick="setCoverImg(\''.$param['modelClass'].'\',\''.$thumb_url.'\');">' . __ ( 'Set as title img') . '</a></p>
	        		';
					if('image' == substr($uploadfile['type'],0,5)){
						$listfile.='</div>';
					}
					$listfile.='</li>';
				}
			}
		}
		if($param['isadmin']){
			$upload_url = $this->Html->url('/admin/uploadfiles/upload');
		}
		else{
			$upload_url = $this->Html->url('/uploadfiles/upload');
		}
		/*仅上传单个文件时，覆盖upload_success_handler函数*/
		$script = '<script>
'.(($param['upload_limit']==1)?
'function '.$param['upload_success_handler'].'(file, serverData) {
				try {
					var progress = progress_list[file.id] ;
					progress.setComplete();
					if (serverData === " ") {
						this.customSettings.upload_successful = false;
					} else {
						var data=eval("(" + serverData + ")");
						console.log(data);
						if(data.ret==0 || data.state=="SUCCESS"){
							this.customSettings.upload_successful = true;
							$("#'.$fieldid.'").val(data.fspath);
							$("#fileuploadinfo_'.$fieldid.'").html(data.message);
						}
					}
				} catch (e) {
					alert(serverData);
				}
			}
':'').
'var swfu_'.$fieldid.';
$(function () {
	swfu_'.$fieldid.' = new SWFUpload({
		upload_url: "'.$upload_url.'",
		file_post_name: "'.$fieldname.'",
		file_size_limit : "'.$size.' MB",
		file_types : "'.$param['file_types'].'",
		file_types_description : "'.$param['file_types_description'].'",
		file_upload_limit : 0,
		file_queue_limit : '.$param['upload_limit'].',
		post_params : {
			"PHP_SESSION_ID" : "'.session_id().'",
			"file_post_name" : "'.$fieldname.'",
			"file_model_name":"'.$param['modelClass'].'",
			"no_db":"'.$param['no_db'].'",
			"save_folder":"'.$param['save_folder'].'",
			"return_type":"'.$param['return_type'].'"
		},
	
		button_image_url : "'.$this->Html->url($param['button_image_url']).'",
		button_placeholder_id : "spanButtonPlaceholder_'.$fieldid.'",
		button_width: '.$param['button_width'].',
		button_height: '.$param['button_height'].',
	
		flash_url : "'.$this->Html->url('/js/swfupload/swfupload.swf').'",
		flash9_url : "'.$this->Html->url('/js/swfupload/swfupload_fp9.swf').'",
	
		file_queued_handler : fileQueued,
		file_queue_error_handler : fileQueueError,
		file_dialog_complete_handler : fileDialogComplete,
	
		swfupload_preload_handler : preLoad,
		swfupload_load_failed_handler : loadFailed,
		swfupload_loaded_handler : loadSuccess,
	
		upload_start_handler : uploadStart,'.($param['withprogress']?
					'upload_progress_handler : uploadProgress,':'').
					'upload_error_handler : uploadError,
		upload_success_handler : '.$param['upload_success_handler'].',
		upload_complete_handler : uploadComplete,
		custom_settings : {
			progress_target : "fsUploadProgress",
			upload_successful : false,
			auto_start : true
		},
		debug: '.($_GET['debug']?'true':'false').'
	});
	swfu_array[swfu_array.length] = swfu_'.$fieldid.';
});
</script>';
		$this->_View->append('bottomscript',$script);
		
		$js_files = '';
		if(!self::$js_loaded){
			$js_files = $this->Html->script(array(
					'swfupload/swfupload.js',
					'swfupload/swfupload.queue.js',
					'swfupload/swfupload.cookies.js',
					'swfupload/fileprogress.js',
					'swfupload/handlers.js',
			));
			self::$js_loaded = true;
		}
		
		
		if($param['label']){
			return $js_files.'<div class="form-group swfupload-control" >'.
					'<label class="col-sm-2 control-label">'.$param['label'].'</label>'.
					'<div class="col-sm-10 controls">
        				'.$hidden.'
        				<table class="table table-noborder upload-operate" style="margin:0 0;">
        				<tr><td width="120px" style="padding:10px 0" class="upload-btn"><span id="spanButtonPlaceholder_'.$fieldid.'"></span></td><td><div id="'.$param['file_upload_progress'].'"></div></td>
						</tr></table>
					'.($param['with_list_div']? '
						<div class="upload-filelist row" id="fileuploadinfo_'.$fieldid.'">
							'.$listfile.'
						</div>' :$listfile).'
				</div>
			</div>';
		}
		else{
			return $js_files.'<div class="swfupload-control" >'.$hidden.
			'<table class="table table-noborder upload-operate" style="margin:0 0;">
        				<tr><td width="120px" style="padding:10px 0" class="upload-btn"><span id="spanButtonPlaceholder_'.$fieldid.'"></span></td><td><div id="'.$param['file_upload_progress'].'"></div></td>
						</tr></table>
					'.($param['with_list_div']? '
						<div class="upload-filelist row" id="fileuploadinfo_'.$fieldname.'">
							'.$listfile.'
						</div>' :$listfile).'
			</div>';
		}
		/*
		return '
		<div class="form-group swfupload-control" >
			<label class="col-sm-2 control-label">'.$param['label'].'</label>'.
			'<div class="col-sm-10 controls">'.$hidden.'<span id="spanButtonPlaceholder_'.$fieldid.'"></span>'.$param['after'].'</div>
			<div class="clearfix"></div>
			<div class="col-sm-2">&nbsp;</div>
			<ul class="col-sm-10 upload-filelist" id="fileuploadinfo_'.$fieldname.'">'.$listfile.'</ul>
			<div class="clearfix"></div>
		</div>';*/
		
	}
	
	/**
	 * 选择相关模块的数据
	 * @param string $modelClass 相关模块
	 * @return string
	 */
	function selectAssoc($modelClass) {
		$targetid = $modelClass.'-'.time();
		$controller_name = Inflector::tableize($modelClass);
		$associd = Inflector::underscore($modelClass).'_id';
		$ids = array();
		$words = '';
		if(is_array($this->data[$modelClass]) && !empty($this->data[$modelClass])){
			foreach($this->data[$modelClass] as $item){
			    $ids[] = $item['id'];
				$words.='<div class="btn-group">
				<input type="hidden" name="data['.$modelClass.']['.$item['id'].']['.$associd.']" value="'.$item['id'].'">
				<input type="hidden" name="data['.$modelClass.']['.$item['id'].'][relatedmodel]" value="'.$this->defaultModel.'">
				<button class="btn btn-primary showtooltip" title="'.$item['name'].'" data-toogle="tooltip">'.$item['name'].'</button>
				<button class="btn btn-primary btn-remove"><i class="glyphicon glyphicon-remove"></i></button></div>';
			}
		}
		$listword = '<div class="form-group required success"><label class="col-sm-2 control-label">'.__('Related').__d('modelextend',$modelClass).__(':').'</label><div class="col-sm-10 controls" id="'.$targetid.'">';
		$listword.='<button onclick="open_dialog({title:\''.__('Select').'\',width:960},\''.Router::url('/admin/'.$controller_name.'/list?type=select&targetid='.$targetid.'&ids='.implode(',',$ids).'&mid='.$this->data[$this->defaultModel]['id'].'&m='.$this->defaultModel).'\')" class="btn btn-warning pull-left" type="button">'.__('Select').'</button>';
		
		$listword.= $words.'</div><div class="clearfix"></div></div>
		<script>
		$("#'.$targetid.'").on("click",".btn-remove",function(){
		$(this).closest(".btn-group").remove();
	});
	</script>
	';
		return $listword;
	}
	
	/**
	 * 选择关键字标签
	 * @return string
	 */
	function keyword() {
		$targetid = 'keywords-'.time();
		$listword = '<div class="form-group clearfix success"><label class="col-sm-2 control-label">选择标签</label><div class="controls" id="'.$targetid.'">';
		if(is_array($this->data['Keyword']) && !empty($this->data['Keyword'])){
			foreach($this->data['Keyword'] as $word){
				$listword.='<div class="btn-group">
				<input type="hidden" name="data[Keyword]['.$word['id'].'][keyword_id]" value="'.$word['id'].'">
				<input type="hidden" name="data[Keyword]['.$word['id'].'][relatedmodel]" value="'.$word['KeywordRelated']['relatedmodel'].'">
				<button class="btn btn-primary">'.$word['value'].'</button>
				<button class="btn btn-primary btn-remove"><i class="icon-remove"></i></button></div>';
			}
		}
		$listword.='<button onclick="open_dialog({title:\'Select\'},\''.Router::url('/admin/keywords/select?targetid='.$targetid.'&m='.$this->defaultModel).'\')" class="btn btn-warning pull-left" style="margin:5px 5px;" type="button">'.__('Select').'</button>';
		$listword.='</div></div>
		<script>
		$("#'.$targetid.'").on("click",".btn-remove",function(){
			$(this).closest(".btn-group").remove();
		});
		</script>
		';
		return $listword;
	}
}

?>