<?php
class CkeditorHelper extends AppHelper {
	
	public static $js_loaded = false;
	
	public $helpers = array('Html');
	
	function load($id, $toolbar = 'office2013') {
		
		$js_files = '';
		if(!self::$js_loaded){
			$js_files = $this->Html->script(array(
					'ckeditor/ckeditor.js',
			));
			self::$js_loaded = true;
		}
		
		
		
        $script = "<script type=\"text/javascript\">        
$(function(){
	CKEditorAddUploadImage = function(thisDialog){
	    var uploadUrl = '".$this->url('/admin/uploadfiles/listfile/'.$this->_View->getVar('current_model'))."'; //处理文件/图片上传的页面URL	
	    var obj = window.showModalDialog(uploadUrl); 
		thisDialog.getContentElement('info', 'txtUrl').setValue(obj.txtUrl);	   
	    thisDialog.getContentElement('info', 'txtAlt').setValue(obj.txtAlt);	   
	}
	if(CKEDITOR.instances['$id']){
		//CKEDITOR.instances['$id'].destroy(); // 取消编辑器效果，替换为之前的textarea
		CKEDITOR.remove(CKEDITOR.instances['$id']); // 销毁删除对象
	}
	
    CKEDITOR.on( 'instanceReady', function( e ){
    	e.editor.document.appendStyleSheet( '".Router::url('/stylevars/getcss.css')."' );
		e.editor.document.appendStyleSheet( '".$this->webroot(CSS_URL.'/style.css')."' );
		e.editor.document.appendStyleSheet( '".$this->webroot(CSS_URL.'/ui-customer.css')."' );
		//e.editor.setMode( 'source' );
		//$('.cke_top').width($('.cke_top').width()); //.css({'position':'inherit'})
		$('.cke_top').addClass('clearfix').affix({offset:{top: 110}});
		
		e.editor.on( 'mode', function( e ){
			if(e.editor.mode == 'wysiwyg'){
				e.editor.document.appendStyleSheet( '".Router::url('/stylevars/getcss.css')."' );
				e.editor.document.appendStyleSheet( '".$this->webroot(CSS_URL.'/style.css')."' );				
				e.editor.document.appendStyleSheet( '".$this->webroot(CSS_URL.'/ui-customer.css')."' );
			}
		});		
	});
	
	ckeditors['$id'].on( 'setData', function( evt ){ //删除标签中的空格与换行 
				var str = evt.data.dataValue;
				str = str.replace(/(\\n)/g, \"\");  
				str = str.replace(/(\\t)/g, \"\");  
				str = str.replace(/(\\r)/g, \"\"); 
				var obj = $('<div>'+str+'</div>');
				obj.contents().filter(function() {
					return this.nodeType === 3 && $.trim($(this).text()) == \"\";
				}).remove();
				evt.data.dataValue = obj.html();
			} );
		ckeditors['$id'].on( 'getData', function( evt ){ //删除标签中的空格与换行 
				var str = evt.data.dataValue;
				str = str.replace(/(\\n)/g, \"\");  
				str = str.replace(/(\\t)/g, \"\");  
				str = str.replace(/(\\r)/g, \"\"); 
				var obj = $('<div>'+str+'</div>');
				obj.contents().filter(function() {
					return this.nodeType === 3 && $.trim($(this).text()) == \"\";
				}).remove();
				evt.data.dataValue = obj.html();
			} );
	
    ckeditors['$id'] = CKEDITOR.replace( '$id');	
	ckeditors['$id'].on('focus',function(e){
		current_ckeditor_instance = e.editor;
	});
//	addUploadButton(ckeditors['$id']);
});
</script>
"; 
        $this->_View->append('bottomscript',$js_files.$script);
        
    }
/*
 *  ckeditors['$id'] = CKEDITOR.replace( '$id',{
    	skin : '$toolbar',
    	extraPlugins : 'autogrow',
		removePlugins : 'resize'
	});	
 * */

}
?>