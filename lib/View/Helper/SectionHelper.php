<?php
class SectionHelper extends AppHelper {

    /**
     * Other helpers used by this helper
     *
     * @var array
     * @access public
     */
    public $helpers = array('Html', 'Form','Hook');

    private $maxdepth = 2;
    /**
     * Current Node
     *
     * @var array
     * @access public
     */
    var $node = null;

    /**
     * 系统菜单
     * @param $modelClass
     * @param $parent_id
     */
    function getSystemsMenu($modelClass='Menu', $parent_id= null) {
        // 判断有模板缓存，不处理;注意不同页面，高亮显示的不一样。guid来对应html缓存内容
        // 判断有变量缓存，直接返回变量缓存
        // 若两个缓存都没有，则从数据库查出内容，返回
        $guid = $modelClass . '_systemmenu_' . $parent_id;
        
            $channels = Cache::read($guid . '_menu');
            if ($channels === false) {
                $model_obj = loadModelObject($modelClass);
                $channels = $model_obj->find('threaded',
                                array('conditions' => array('parent_id' => $parent_id, 'visible' => 1),
                                    'fields' => array('id', 'name', 'slug', 'link', 'parent_id'),
                                    'order' => 'left asc',
                        ));

                Cache::write($guid . '_menu', $channels);
            }
            $channels_html = '';
            foreach ($channels as $item) {
                $channels_html.=' ' . $this->Html->link($item[$modelClass]['name'], '/admin/?menu=' . $item[$modelClass]['id']);
            }
            //$channels_html = $this->nestedLinks($channels, $options);           
            return $channels_html;
    }

    /**
     * 栏目导航菜单
     * @param $modelClass
     * @param $options
     * 	$options['linkAttributes2']  定义2级的link属性。 末尾的数字表示菜单的层级
     *  $options['liAttributes2']  定义2级的li属性，会继承 liAttributes
     *  $options['subliAttributes2'] 带2级菜单中包含有下级的li的样式，会继承subliAttributes
     *  
     *  $options['conditions'] 传入筛选条件，可按model筛选出本模块的栏目。后台数据列表模板中选择栏目调用
     *  
     * @param $parent_id  父类别id
     * @param $with_child 为true是，选取所有的子类。为false时，仅选择当前父类的直属子类
     */
    function getNavMenu($modelClass, $options=array(), $parent_id= null, $with_child = true) {
        if (empty($options['conditions'])) {
            $options['conditions'] = array();
        }
        if($options['maxdepth']){
            $this->maxdepth = $options['maxdepth'];
        }
        $guid = $modelClass . '_' . guid_string($options['conditions']) . '-' . $parent_id . '-' . $with_child;
        
            $channels = Cache::read('menu_'.$guid);
            if ($channels === false) {
            	if(!ModelExists($modelClass)){
            		return null;
            	}
                $model_obj = loadModelObject($modelClass);
                $schema_fields = array_keys($model_obj->schema());
                $selectfields = array('id', 'name', 'slug', 'link', 'parent_id');
                if(in_array('model',$schema_fields)){
                	$selectfields[] = 'model'; // 模块字段含model时，将model也选出来，生成link的时候可能会用到。如link=>/controller:{model}/slug:{slug}
                }
                if(in_array('submenu',$schema_fields)){
                	$selectfields[] = 'submenu'; //自定义下级菜单的内容
                }
                if(in_array('prefix_icon',$schema_fields)){
                	$selectfields[] = 'prefix_icon'; // 菜单的前缀图标
                }
                //$model_obj->contain(); // 需要 var $actsAs = array('Containable');
				$model_obj->recursive = 1;
                if ($with_child) {
                    if ($parent_id) {
                        $parent_info = $model_obj->findById($parent_id);
                        $left = $parent_info[$modelClass]['left'];
                        $right = $parent_info[$modelClass]['right'];
                        $conditions = array($modelClass . '.left >' => $left, 'visible' => 1,$modelClass . '.right <' => $right);
                        $conditions = array_merge($conditions, $options['conditions']);
                        $channels = $model_obj->find('threaded', array(
                            'conditions' => $conditions, 
                            'order' => 'left asc',
                            'fields' => $selectfields,
                        ));
                    } else {
            			if(in_array('visible',$schema_fields)){
                                $conditions = array('visible' => 1);
            			}
                        if(is_array($conditions)){
                            $conditions = array_merge($conditions, $options['conditions']);
                        }
                        else{
                            $conditions = $options['conditions'];
                        }
                        
			             //print_r($conditions);
                        $channels = $model_obj->find('threaded', array(
                                    'conditions' => $conditions,
                                    'fields' => $selectfields,
                                    'order' => 'left asc',
                                ));
                    }
                } else {
                    $conditions = array('visible' => 1, 'parent_id' => $parent_id);
                    $conditions = array_merge($conditions, $options['conditions']);
                    $channels = $model_obj->find('threaded', array(
                                'conditions' => $conditions,
                                'fields' => $selectfields,
                                'order' => 'left asc',
                            ));
                }                
                Cache::write('menu_'.$guid, $channels);
            }
            //print_r($channels);
            $this->Hook->call('NavMemu',array(&$channels,$modelClass,&$options, $parent_id, $with_child));
            
            $_options = array();
            $_options = array(
                'ulAttributes' => array('class' => 'nav navbar-nav'),
            	'liAttributes'=> array('activeclass'=>'active','withSubClass'=> 'dropdown'),
            	'subliAttributes'=> array('apend'=>'<span class="caret"></span>'),
            	'subliAttributes2'=> array('class'=>'dropdown-submenu'),
            	'subliAttributes3'=> array('class'=>'dropdown-submenu'),
            	'linkAttributes'=> array(), //'activeclass'=>'active'
                //'liSelectedClass' => 'active', // ui-state-active
            	//'linkSelectedClass' => 'active', 
                'selectedid' => 0,
                'dropdown' => true,
            	'data-toggle' => 'dropdown',
            	'outtag' => 'ul',// 当 outtag,midtag为空或false时，外围无ul,li.
            	'midtag' => 'li',
            	'target' => '',
                'modelclass' => $modelClass,
            	'separator'=>'<li class="divider-vertical"></li>',
            	'inner_separator'=>'<li class="divider"></li>',
                'dropdownClass' => 'dropdown-menu',
            );
            $options = array_merge($_options,$options);
            
            if($options['fullwidth']) { // fullwidth为true时，表示百分百宽度。所有项宽度和为100%
                $nums = count($channels);
                if($nums) {
                    $options['liAttributes1']['style'] = 'width:'.round(100/$nums,2).'%';
                }
            }
            return $this->nestedLinks($channels, $options);
    }

    /**
     * 模块类别列表参考
     * @param $modelClass
     * @param $options
     */
    function getLeftMenu($modelClass, $options = array()) {
        //$modelClass = 'Category';
        $parent_id = $options['parent_id'];
        // 判断有模板缓存，不处理
        // 判断有变量缓存，直接返回变量缓存
        // 若两个缓存都没有，则从数据库查出内容，返回
        if (empty($options['conditions'])) {
            $options['conditions'] = array();
        }
        if($options['maxdepth']){
        	$this->maxdepth = $options['maxdepth'];
        }
        $guid = $modelClass . '_' . $parent_id.'_'.guid_string($options['conditions']);
        $cache_key = 'menu_left_'.$guid;
            if(isset($options['nocache']) && $options['nocache']){
            	$channels = false;
            }
            else{
            	$channels = Cache::read($cache_key);
            }
            if ($channels === false) {
            	if(!ModelExists($modelClass)){
            		return null;
            	}
                $model_obj = loadModelObject($modelClass);
                
                $schema_fields = array_keys($model_obj->schema());
                //$model_obj->contain(); // 需要 var $actsAs = array('Containable');
                //$model_obj->recursive = -1;
                $model_obj->recursive = 1; //多语言支持
                if ($parent_id) {
                    $parent_info = $model_obj->findById($parent_id);
                    $left = $parent_info[$modelClass]['left'];
                    $right = $parent_info[$modelClass]['right'];
                    $conditions = array($modelClass . '.left >' => $left,$modelClass . '.right <' => $right);
                    $conditions = array_merge($conditions, $options['conditions']);
					if(in_array('visible',$schema_fields)){
						$conditions[$modelClass . '.visible'] = 1;
					}
					
                    $channels = $model_obj->find('threaded', array('conditions' => $conditions, 'order' => 'left asc'));
                } else {
                	if(in_array('visible',$schema_fields)){
                		$conditions = array($modelClass . '.visible' => 1);
                	}
                	else{
                		$conditions = array();
                	}
                    $conditions = array_merge($conditions, $options['conditions']);
                    
                    $channels = $model_obj->find('threaded', array('conditions' => $conditions, 'order' => 'left asc'));
                }
                if( empty($options['nocache']) ){
                	Cache::write($cache_key, $channels);
                }
            }
            $_options = array();
            $_options = array(
            	'ulAttributes' => array('class' => 'nav nav-pills nav-stacked'),
            	'liAttributes'=> array('activeclass' => 'active','withSubClass'=> 'dropdown'),
            	//'subliAttributes'=> array('class'=>'dropdown-submenu','apend'=>'<span class="caret"></span>'),
            	'subliAttributes' => array(),
            	'linkAttributes'=> array(), //'activeclass'=>'active'
                //'liSelectedClass' => 'active_li', // ui-state-active
            	//'linkSelectedClass' => 'active',            		
                'selectedid' => 0,
                'dropdown' => true,
            	'outtag' => 'ul',// 当 outtag,midtag为空或false时，外围无ul,li.
            	'midtag' => 'li',
            	'target' => '',
            	'modelclass' => $modelClass,
            	'data-toggle' => 'dropdown',
            	'separator'=>'<li class="divider"></li>',
                'dropdownClass' => 'dropdown-menu',
            );
            $options = array_merge($_options, $options);

            return  $this->nestedLinks($channels, $options);            
    }

    /**
     * Nested Links
     *
     * @param array $links model datas find in threaded type. 
     * @param array $options (optional)
     * @param integer $depth depth level
     * @return string
     */
    function nestedLinks($links, $options = array(), $depth = 1) {
        $_options = array();
        $options = array_merge($_options, $options);
        $modelName = $options['modelclass'];
        $output = '';
        // 修改链接的格式，默认是不带有span的
        $this->Html->tags['link'] = '<a href="%s.html"%s><span>%s</span></a>';
        $link_length = count($links);
        foreach ($links AS $key => $link) {
        	
        	$liAttr = $options['liAttributes'];
        	if(!empty($options['liAttributes'.$depth])){
        		$liAttr = array_merge($liAttr,$options['liAttributes'.$depth]);
        	}
        	$linkAttr = $options['linkAttributes'];
            if(!empty($options['linkAttributes'.$depth])){
            	$linkAttr = array_merge($linkAttr,$options['linkAttributes'.$depth]);
            }
            $linkAttr['id'] = 'link-' . $link[$modelName]['id'];
            $linkAttr['data-id'] = $link[$modelName]['id'];
            $linkAttr['ref'] = $link[$modelName]['slug']?$link[$modelName]['slug']:$link[$modelName]['id'];
            if ($key == 0) {
                $linkAttr['class'].=' ui-menu-first';
            } elseif ($key == $link_length - 1) {
                $linkAttr['class'].=' ui-menu-last';
            }
            if (!empty($options['selectedid']) && $link[$modelName]['id'] == $options['selectedid']) {
            	if($linkAttr['class']){
            		$linkAttr['class'].= ' '.$linkAttr['activeclass'];
            	}
            	else{
            		$linkAttr['class'] = $linkAttr['activeclass'];
            	}
            }
            unset($linkAttr['activeclass']);
            
            
            foreach ($linkAttr AS $attrKey => $attrValue) {
                if ($attrValue === null) {
                    unset($linkAttr[$attrKey]);
                }
            }
            
            if ( isset($linkAttr['link']) && !empty($linkAttr['link']) ) {
            	$link[$modelName]['link'] = $this->_parseStringVariable($linkAttr['link'], $link[$modelName]);
            	unset($linkAttr['link']);
            }
            elseif(!empty($options['url'])){
                $link[$modelName]['link'] = $this->_parseStringVariable($options['url'], $link[$modelName]);
            }
            elseif (strpos($link[$modelName]['link'], 'controller:')!==false) {
            	// if link is in the format: controller:contacts/action:view
                $link[$modelName]['link'] = $this->linkStringToArray($link[$modelName]['link'], $link[$modelName]);
            }
            elseif (empty($link[$modelName]['link'])) { // 无指定链接时，自动生成链接地址
                if ($link[$modelName]['slug'] == '/') {
                    $link[$modelName]['link'] = '/index.html';
                }
                elseif ($link[$modelName]['slug'] == '#') {
                    $link[$modelName]['link'] = '#';
                }
                elseif ($link[$modelName]['slug']) {
                    $link[$modelName]['link'] = '/' . $link[$modelName]['slug'].'.html';
                } else {
                    $link[$modelName]['link'] = '/' . $link[$modelName]['id'].'.html';
                }
                //若没有指定链接， 则以表名/id 作为链接
                if ($modelName != 'Category') {
                    $link[$modelName]['link'] = '/' . Inflector::tableize($modelName).'/view/' . $link[$modelName]['id'];
                }
                //$link[$modelName]['link'] = Router::url($link[$modelName]['link']);
            }
            elseif(strpos($link[$modelName]['link'],'http://')!==false){
            	$linkAttr['target'] = '_blank'; //外部链接地址，在新窗口打开
            }
            
            if($options['target']){
            	$linkAttr['target'] = $options['target'];
            }
            
            if($link[$modelName]['link'] == '#'){
                $linkAttr['target'] = '';
            }
            
            $linkAttr['escape'] = false;
            
            if($link[$modelName]['prefix_icon']){
            	$link[$modelName]['name'] = '<i class="'.$link[$modelName]['prefix_icon'].'"></i> '.$link[$modelName]['name'];
            }
            
            if($depth + 1 <= $this->maxdepth && (!empty($link[$modelName]['submenu']) || (isset($link['children']) && count($link['children']) > 0))){
            	$linkAttr['class'].='  dropdown-toggle';
            	$linkAttr['data-toggle']= $options['data-toggle'];
            	$liAttr['class'] .= $liAttr['withSubClass'];//dropdown-submenu
            	$liAttr = array_merge($liAttr,$options['subliAttributes']);
            	if(!empty($options['subliAttributes'.$depth])){
            		$liAttr = array_merge($liAttr,$options['subliAttributes'.$depth]);
            	}
            	
            	if($liAttr['prepend']){
            		$link[$modelName]['name'] = $liAttr['prepend'].$link[$modelName]['name'];
            		unset($liAttr['prepend']);
            	}
            	if($liAttr['apend']){
            		$link[$modelName]['name'] = $link[$modelName]['name'].$liAttr['apend'];
            		unset($liAttr['apend']);
            	}
            	
            	if(!empty($link[$modelName]['link'])) {
            		$linkOutput = $this->Html->link($link[$modelName]['name'], $link[$modelName]['link'], $linkAttr); //
            	}
            	else{
            		$linkOutput = $this->Html->tag($linkAttr['tag'], $link[$modelName]['name'],$linkAttr);
            		// '<span>'.$link[$modelName]['name'], $link[$modelName]['link'], $linkAttr); //
            	}
            	
            	if(!empty($link[$modelName]['submenu'])){
            		$linkOutput .=$link[$modelName]['submenu'];
            	}
                elseif($depth + 1 <= $this->maxdepth){
                	$linkOutput .= $this->nestedLinks($link['children'], $options, $depth + 1);
                }
            }
            else{
            	//$linkOutput = $this->Html->tag($options['midtag'], $linkOutput,$liAttr);
            	$linkOutput = $this->Html->link($link[$modelName]['name'], $link[$modelName]['link'], $linkAttr); //
            }
            
            
            if($options['midtag']){
            	if (!empty($options['selectedid']) && $link[$modelName]['id'] == $options['selectedid']) {
            		if($liAttr['class']){
            			$liAttr['class'] .= ' '.$liAttr['activeclass']; // 将选中样式放在a中，为一个独立元素，对其他无影响。放在li中时，会对下级ul中样式产生影响。
            		}
            		else{
            			$liAttr['class'] = $liAttr['activeclass']; // 将选中样式放在a中，为一个独立元素，对其他无影响。放在li中时，会对下级ul中样式产生影响。
            		}
            	}
            	unset($liAttr['activeclass']);unset($liAttr['withSubClass']);
            	$linkOutput = "\r\n" . $this->Html->tag($options['midtag'], $linkOutput,$liAttr);
            }
            if ($key != $link_length - 1){
	            if($depth>1){
	            	$output .= $linkOutput.$options['inner_separator'];
	            }
	            else{
	            	$output .= $linkOutput.$options['separator'];
	            }
            }
            else{
            	$output .= $linkOutput;
            }
        }
        if ($options['outtag'] && $output != null) {
            $tagAttr = array();
            $tagAttr = $options['ulAttributes'];
            if ($options['dropdown'] && $depth > 1) {
                $tagAttr['class'] = $options['dropdownClass'];
            }
            if ($depth == 1) {
                $output = $options['preli'] . $output . $options['sufli'];
            }
            $output = "\r\n" . $this->Html->tag($options['outtag'], $output, $tagAttr) . "\r\n";
        }
        return $output;
    }
    
    private function _parseStringVariable($string,$array){
    	
    	if(is_array($string)){
    		$string = getSearchLinks($this->request,$string,array(),true) ;
    		//$url_string = Router::url($url_string);
    	}
    	elseif(!is_string($string)){
    		return $string; //非字符串非数组时，不处理
    	}
    	
    	$string = str_replace(array('%25','%7B','%7D'),array('%','{','}'),$string);
        if (preg_match_all('/{(\w+)}/', $string, $matches)) {
               foreach($matches[0] as $key => $val){
                   $string = str_replace($val,$array[$matches[1][$key]],$string);
               }
        }
        return $string;
    }
    
    /**
     * Converts strings like controller:abc/action:xyz/ to arrays
     *
     * @param string $link link
     * @return array
     */
    public function linkStringToArray($link, $data = array()) {
        $link = explode('/', $link);
        $linkArr = array();
        foreach ($link AS $linkElement) {
            if ($linkElement != null) {
                $linkElementE = explode(':', $linkElement);
                if (isset($linkElementE['1'])) {
                    if (preg_match('/^{(\w+)}$/', $linkElementE['1'], $matches)) {
                        $linkElementE['1'] = $data[$matches[1]];
                    }
                    if ($linkElementE['0'] == 'controller') {
                        $linkElementE['1'] = Inflector::tableize($linkElementE['1']);
                    }
                    $linkArr[$linkElementE['0']] = $linkElementE['1'];
                } else {
                    if (preg_match('/^{(\w+)}$/', $linkElement, $matches)) {
                        if ($matches[1] == 'slug' && empty($data[$matches[1]])) {
                            $data[$matches[1]] = $data['id'];
                        }
                        $linkElement = $data[$matches[1]];
                    }
                    $linkArr[] = $linkElement;
                }
            }
        }
        return $linkArr;
    }

    /**
     * Filter content
     *
     * Replaces bbcode-like element tags
     *
     * @param string $content content
     * @return string
     */
    function filter($content) {
        $content = $this->filterElements($content);
        return $content;
    }

    /**
     * Filter content for elements
     *
     * Original post by Stefan Zollinger: http://bakery.cakephp.org/articles/view/element-helper
     * [element:element_name] or [e:element_name]
     *
     * @param string $content
     * @return string
     */
    function filterElements($content) {
        preg_match_all('/\[(element|e):([A-Za-z0-9_\-]*)(.*?)\]/i', $content, $tagMatches);
        for ($i = 0; $i < count($tagMatches[1]); $i++) {
            $regex = '/(\S+)=[\'"]?((?:.(?![\'"]?\s+(?:\S+)=|[>\'"]))+.)[\'"]?/i';
            preg_match_all($regex, $tagMatches[3][$i], $attributes);
            $element = $tagMatches[2][$i];
            $options = array();
            for ($j = 0; $j < count($attributes[0]); $j++) {
                $options[$attributes[1][$j]] = $attributes[2][$j];
            }
            $content = str_replace($tagMatches[0][$i], $this->View->element($element, $options), $content);
        }
        return $content;
    }
	
	/**
	 * 获取内容列表
	 * @param int $id region id
	 * @param int $page page list
	 * @throws MissingModelException
	 * @return array. such as array('regioninfo' => array(), 'datalist' => array(), 'page_navi' => '');
	 */
	public function getRegionListById($id, $page=1) {
		if (isset($GLOBALS['regioninfo'][$id])) {
			$regioninfo = $GLOBALS['regioninfo'][$id];
		}
		else{
			$regioninfo = Cache::read('regioninfo_' . $id);
			if ($regioninfo === false) {
				$region = loadModelObject('Region');
				$GLOBALS['regioninfo'][$id] = $regioninfo = $region->read(null, $id);
				if (empty($regioninfo)) {
					//throw new NotFoundException("Region $id not found");
					return '';
				}
				$regioninfo = current($regioninfo);
				Cache::write('regioninfo_' . $id, $regioninfo);
			}
		}
		if (empty($regioninfo)) {
			return array('regioninfo' => array(), 'datalist' => array(), 'page_navi' => '');
		}
		/**
		 * 一个页面仅支持一个region接收参数传入（ajax动态交互不算在内）
		 */
		list($plugin, $modelClass) = pluginSplit($regioninfo['model'], true);
		if(!ModelExists($modelClass,$plugin)){
			return array('regioninfo' => $regioninfo, 'datalist' => array(), 'page_navi' => '');
		}
		
		$page = $this->request->query['page'] ? $this->request->query['page'] : 1;
		
		if ($regioninfo['auto_receive_param']) {
			$cache_key = 'region_list_'.$id.'_'.$page.'_'.guid_string($this->request->query);
		}
		else{
			$cache_key = 'region_list_'.$id.'_'.$page;
		}
		$rdlist = Cache::read($cache_key);
		if ($rdlist === false) {
			$this->request = Router::getRequest();
			if (!empty($regioninfo['conditions'])) {
				if (is_array($GLOBALS['RegionReplaceVar'])) {
					foreach ($GLOBALS['RegionReplaceVar'] as $key => $val) {
						$regioninfo['conditions'] = str_replace('$' . $key, $val, $regioninfo['conditions']);
						//{{named Taobaoke.name}}
						$regioninfo['conditions'] = preg_replace('/{{named\s+(.+?)\s*}}/ies', "\$this->request->params['named']['\\1']", $regioninfo['conditions']);
					}
				}
				$searchoptions = parseXmlToSqlOption($regioninfo['conditions']);
			}
		
			// $searchoptions['conditions'][$modelClass . '.deleted'] = 0;
			// $searchoptions['conditions'][$modelClass . '.published'] = 1;
		
			if ($regioninfo['auto_receive_param']) {
				if (!empty($searchoptions['joins'])) {
					foreach ($searchoptions['joins'] as $jk => $jv) {
						$joinmodel = $jv['alias'];
						$searchoptions['fields'][] = $jv['alias'] . '.*'; // 追加连接模块的字段
						if(!empty($this->request->query[$joinmodel])){
							$joincondition = getSearchOptions($this->request->query[$joinmodel],$joinmodel);
							$searchoptions['conditions'] +=$joincondition;
						}
					}
				}
				
				
				// 排序参数加上_{id}的后缀
				if (!empty($this->request->query['order'])) {
					$searchoptions['order'] = $this->request->query['order'];
				}
				
				$conditions = getSearchOptions($this->request->query,$modelClass);
				
				$searchoptions['conditions'] = array_merge($searchoptions['conditions'],$conditions);
				
// 				if (!empty($this->request->query['conditions'])) {
// 					$searchoptions['conditions'] += $this->request->query['conditions']; //保留了数据库查询语句条件
// 				}
			}
	// 		print_r($this->request->query);
	// 		print_r($searchoptions);
		
			$searchoptions['page'] = $page;
			
			$model = loadModelObject($modelClass,$plugin);
			$datas = $model->find('all', $searchoptions);
	// 		exit;
			$page_navi = '';
			if ($regioninfo['showpages']) {			 
				// 当前页，第一页。
				$total = $model->find('count',
						array(
								'conditions' => $searchoptions['conditions'],
								'joins' => $searchoptions['joins'],
						)
				);
		
				//                 $page_navi = getPageLinks($total, $regioninfo['rows'], '/'.$Category['Category']['slug'], $page);
		
				if ($regioninfo['pagelink_type'] == 'pageurl') {
					$page_navi = getPageLinks($total, $regioninfo['rows'], $this->request, $page);
				} else { // regionurl
					App::uses('Page', 'Lib');
					$pagelinks = new Page($total, $regioninfo['rows'], 'regions/' . $regioninfo['id'] . '/', $page);
					$page_navi = $pagelinks->renderNav(6);
				}
			}
		
			$rdlist = array('regioninfo' => $regioninfo, 'datalist' => $datas, 'page_navi' => $page_navi);
			Cache::write($cache_key, $rdlist);
		}
		return $rdlist;
	
	}
	
	
	/**
	 * 根据模版中设置的串转换后得到的数组来获取数据列表
	 * @param array $params
	 * @return array
	 */
	function getRegionListByArray($params)
	{
		if(empty($params['model'])){
			return array();
		}
		
		$modelname = Inflector::classify($params['model']);
		
		if( $params['showpages'] && empty($params['page']) ) {
			$page_name = $params['page_name'] ? $params['page_name'] : 'page';
			$params['page'] = $this->request->query[$page_name] ? $this->request->query[$page_name] : 1;
		}
		else{
		    $params['page'] = 1;
		}
		$params['limit'] = $params['limit'] ? $params['limit'] : 30;

		// 仅在第一页缓存内容，其余不缓存。避免缓存的内容太多，吞吐量太大 
		if( !in_array($params['cache'],array('no','false')) && $params['page']==1 ) {
		    if($params['cachekey']) {
		        $cache_key = $params['cachekey'];
		    }
		    else{
		        $cache_key = 'region_list_'.guid_string($params);
		    }
    		$rdlist = Cache::read($cache_key);
		}
		else{
		    $rdlist = false;
		}
		if ( in_array($params['cache'],array('no','false')) || $rdlist === false ) {
			$model = loadModelObject($modelname);
			if($params['recursive']){
				$model->recursive = $params['recursive'];
			}
			else{
				$model->recursive = -1;
			}
			
			$fields = array_keys($model->schema());

            if($params['redis_zlist_key'] && Configure::read($modelname.'.open_redis')){
                App::uses('RedisUtility', 'Utility');
                $redis_zlist = RedisUtility::getZlist($params['redis_zlist_key'],array('page'=> $params['page'],'limit'=>$params['limit']));

                $params['conditions'][$modelname.'.id'] = $redis_zlist['list'];
                if( $params['showpages'] ) {
                    $page_navi = getPageLinks($redis_zlist['total'],$params['limit'], $this->request, $params['page'],$page_name);
                }
                unset($params['limit'],$params['page']);
                $datas = $model->find('all', $params);
            }
            elseif($params['xs_search_wd']) {
			if( Configure::read($modelname.'.xs_ini') && Configure::read($modelname.'.xs_db') ) {
				
				try{
					App::uses('XS','Lib');
					$ini_file = ROOT.DS.Configure::read($modelname.'.xs_ini');
					$xs = new XS($ini_file);
					$search = $xs->search;
					$search->setDb(Configure::read($modelname.'.xs_db'));
					$search->setLimit($params['limit']);
					$search->setQuery($params['xs_search_wd']);
					if($params['xs_sort']) {
						$search->setSort($params['xs_sort']);
					}
					$docs = $search->search();
					$datas = array();
					foreach($docs as $doc) {
						$info = $doc->getFields();
						$info['created'] = date('Y-m-d H:i:s',$info['created']);
						$datas[] = array($modelname => $info );
					}
					if( $params['showpages'] ) {
					   $total = $search->getLastCount();
					}
				}
				catch(Exception $e){
					$datas = array();
					$total = 0;
				}
			}
	
	}
            elseif($params['sphinx_search_wd']) {
                App::uses('SphinxClient', 'Lib');
                if( Configure::read($modelname.'.sphinx_host') && Configure::read($modelname.'.sphinx_port') && Configure::read($modelname.'.sphinx_index')) {
                    $sc = new SphinxClient;
                    $wd = $params['sphinx_search_wd'];
                    // sphinx_index 配置项未sphinx创建的索引的名字
                    $sc->SetServer(Configure::read($modelname.'.sphinx_host') , Configure::read($modelname.'.sphinx_port'));
                    $sc->SetLimits(($params['page']-1)*$params['limit'],$params['limit']);
                    $result = $sc->Query($wd,Configure::read($modelname.'.sphinx_index'));
                    $conditions[$modelname.'.id'] = array_keys($result['matches']);
                    $total = $result['total'];
                    $datas = $model->find('all',array(
                        'conditions'=> $conditions,
                        //'order' => $modelname.'.created desc',
                        'recursive' => -1,
                    ));
                    
                    if($params['showpages']){
                        $page_navi = getPageLinks($total,$params['limit'], $this->request, $params['page'],$page_name);
                    }
                }
            }
            else{
                if(!empty($params['data_id']) && in_array('id',$fields)){
                    $params['conditions'][$modelname.'.id'] = $params['data_id'];
                }
                
                $page_navi = '';
                $datas = $model->find('all', $params);
                
                if($params['reverse'] == "true"){
                    $datas = array_reverse($datas);
                }
                 
                if($params['showpages'] && empty($params['data_id'])){
                    // 当前页，第一页。
                    $total = $model->find('count',
                        array(
                            'conditions' => $params['conditions'],
                            'joins' => $params['joins'],
                            'recursive' => -1,
                        )
                    );
                    $page_navi = getPageLinks($total,$params['limit'], $this->request, $params['page'],$page_name);
                }
            }		
	// 		print_r($params);
			$rdlist = array('regioninfo' => array('Region'=>$params), 'datalist' => $datas, 'page_navi' => $page_navi);
			$duration  = intval($params['cache']) > 0 ? intval($params['cache']) : 0;
			if( $cache_key ) {
				Cache::write($cache_key,$rdlist,'default',$duration);
			}
		}
		return $rdlist;
	}
	
}

?>
