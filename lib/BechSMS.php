<?php

class BechSMS {
	
	//&mobile=您的手机号码&content=".urlencode("中文 空格 换行符"
	static public $send_url = "http://sms.bechtech.cn/Api/send/data/json?accesskey=1736&secretkey=acb8938b039a589b4a0423fa1b2200ebcd9e1f51&mobile=%s&content=%s";
	
    
    //添加轨迹
    static public function sendMsg($phone, $content){
    	$url = sprintf(BechSMS::$send_url,$phone,urlencode($content));
    	$content = BechSMS::curl($url);
    	$ret = json_decode($content,true);
    	if($ret['result']=='01'){
    		return true;
    	}
    	else{
    		return false;
    	}
    }
    
    static public function getLeft(){
    	$url = "http://sms.bechtech.cn/Api/getLeft/data/json?accesskey=1736&secretkey=acb8938b039a589b4a0423fa1b2200ebcd9e1f51";
    	$content = BechSMS::curl($url);
    	$ret = json_decode($content,true);
    	return $ret['result'];
    }
    
    static public function curl($url,$post = array()){
    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_URL, $url);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_HEADER, 0);
    	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    	curl_setopt($ch, CURLOPT_MAXREDIRS, 5); // 302 跳转5次
    	curl_setopt($ch, CURLOPT_TIMEOUT, 5); //页面最大执行时间为5s
    	if(!empty($post)){
	    	curl_setopt($ch, CURLOPT_POST, true);
	    	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    	}
    	$content = curl_exec($ch);
    	$http_info = curl_getinfo($ch);
    	curl_close($ch);
    	return $content;
//     	if ($http_info['http_code'] == '200') {
//     		return ;
//     	}
    }
   
}
?>