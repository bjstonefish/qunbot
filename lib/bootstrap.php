<?php


include_once COMMON_PATH.'global_function.php';

error_reporting(E_ERROR | E_WARNING | E_PARSE);

App::build(array(
	'plugins' => array(COMMON_PATH.'Plugin'.DS, APP_PATH.'Plugin'.DS, ),
    'libs' => array(COMMON_PATH),
	'vendors' => array(COMMON_PATH.'Vendor'.DS,),
	'helpers' => array(COMMON_PATH.'View'.DS.'Helper'.DS,),
	'locales' => array(ROOT.DS.'data'.DS.'locale' . DS),
	'components' => array(COMMON_PATH.'Component'. DS),
	'behaviors' => array(COMMON_PATH.'Behavior'. DS),
));


define('STATIC_CDN_URL','//static.135editor.com'); // 域名不带最后的斜线

define('TMP_FILES', ROOT . DS . 'data' . DS . 'files' .DS );

if (defined('SAE_MYSQL_DB')) {
	App::build(array(
		'locales' => array(TMP.'locale' . DS),
	));
	
	// sae上禁止放在二级目录
    define('IN_SAE',true);
    define('SAE_STORAGE_UPLOAD_DOMAIN_NAME','upload');
    define('SAE_STORAGE_UPLOAD_DOMAIN_URL','http://'.$_SERVER['HTTP_APPNAME'].'-'.SAE_STORAGE_UPLOAD_DOMAIN_NAME.'.stor.sinaapp.com/');
    define('UPLOAD_FILE_URL', 'http://'.$_SERVER['HTTP_APPNAME'].'-'.SAE_STORAGE_UPLOAD_DOMAIN_NAME.'.stor.sinaapp.com/');
    // SAE上传地址，S3,Storage等. 带 saestor://，可直接用于fwrite,copy,等函数读写文件
    // 结合UPLOAD_RELATIVE_PATH能获取到文件的地址
    define('UPLOAD_FILE_PATH', 'saestor://'.SAE_STORAGE_UPLOAD_DOMAIN_NAME.'/');
    
    define('WEB_VISIT_CACHE','saemc://cache/');
    define('WEB_VISIT_CACHE_URL','/cache/');
}
else{
	/**
	 * 数据库保存的文件路径从files开始，UPLOAD_FILE_URL不用包含files
	 * 结合UPLOAD_RELATIVE_PATH能获取到文件的地址
	 */
	// define('WWW_ROOT', ROOT . DS . 'webroot' . DS); WWW_ROOT endwith DS.
    define('UPLOAD_FILE_PATH', WWW_ROOT); // 本地上传路径
    define('UPLOAD_FILE_URL',str_replace('\\','/',APP_SUB_DIR)); //APP_SUB_DIR    
    define('WEB_VISIT_CACHE',WWW_ROOT.'cache'.DS);
    //define('WEB_VISIT_CACHE_URL',APP_SUB_DIR.'/cache/');
    if(isset($_SERVER['SERVER_ADDR']) && ($_SERVER['SERVER_ADDR'] == '127.0.0.1' || $_SERVER['SERVER_ADDR'] == '::1')) {
        define('WEB_VISIT_CACHE_URL',APP_SUB_DIR.'/cache/');
    }
    else{
        //http://static.135editor.com
        define('WEB_VISIT_CACHE_URL','/cache/');
    }
}
// 文件的相对地址,结合UPLOAD_FILE_PATH获取到本地的地址
define('UPLOAD_RELATIVE_PATH', '/files/');
// sae 队列接口，提供给外部调用。 类似于分布式任务。本地调用接口添加任务，sae队列中的任务一个一个去执行。
//define('SAE_TASK_QUEUE_URL', 'http://'.$_SERVER['HTTP_APPNAME'].'.sinaapp.com/api/sae/TaskQueue.php');
define('SAE_TASK_QUEUE_URL', 'http://www.miaomiaoxuan.com/api/sae/TaskQueue.php');

if (function_exists('date_default_timezone_set')) {
	date_default_timezone_set('Etc/GMT-8');
}



define('HTTP_REQUEST_METHOD', 'HttpCurl'); // or HttpSocket

define('DEVELOP_MODE', '1'); // 显示隐藏的控制项，显示页面钩子

define('OPEN_INTERNATIONAL',1);	// 站点是否开启多语言

Configure::write('Dispatcher.filters', array(
	'AssetDispatcher', // 处理js和css文件，压缩输出，作用不大。不能支持将内容汇总到一个文件输出
	//'CacheDispatcher', // 检查整个页面的缓存.php文件，有则直接包含输出。与cachehelp配合使用，缓存文件有cachehelp生成。
));

if(!defined('SAE_MYSQL_DB')){
	/**
	 * Configures default file logging options
	 */
	App::uses('CakeLog', 'Log');
	CakeLog::config('debug', array(
		'engine' => 'FileLog',
		'types' => array('notice', 'info', 'debug'),
		'file' => 'debug',
	));
	CakeLog::config('error', array(
		'engine' => 'FileLog',
		'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
		'file' => 'error',
	));
}
$cache_prefix = APP_DIR.'_';
if(defined('SAE_MYSQL_DB')){
	// 区分各版本的缓存，不互相冲突
	$cache_prefix = $_SERVER['HTTP_APPVERSION'];
}



/**
 * 加载扩展配置项中开启的各插件
 */

if( php_sapi_name()==='cli' ){
	define('IN_CLI',true);
	unset($_GET['cron_secret'],$_REQUEST['cron_secret']);
}

if(file_exists(APP . 'Config' . DS . 'database.php')){
	App::uses('PhpReader', 'Configure');
	Configure::config('default', new PhpReader(DATA_PATH));
	try{
		Configure::load('settings');
	}
	catch(ConfigureException $e){
		// skip,setting cache not exists
	}
}

if (function_exists('date_default_timezone_set')) {
	date_default_timezone_set('Etc/GMT-8');
}


if(file_exists(DATA_PATH.'version.php')) {
	include_once DATA_PATH.'version.php';
}


/**
 * 区分前台的插件和对后台的插件
 */
$pluginBootstraps = Configure::read('Hook.bootstraps');
$plugins = array_filter(explode(',', $pluginBootstraps));

if (!empty($plugins)) {
	foreach ($plugins as $plugin) {
		$pluginName = Inflector::camelize($plugin);
		try{
			CakePlugin::load($pluginName, array(
					'bootstrap'=>true,
					'routes' => true,
					'ignoreMissing' => true,
			));
		}
		catch(Exception $e){
			// CakeLog::error('Plugin not found in lib/bootstrap: ' . $pluginName.'. msg:'.$e->getMessage());
			continue;
		}
	}
}

/** 加载额外的参数设置项，可能用到settings表加载到Configure的值  */
include_once COMMON_PATH.'Config'.DS.'extend.php';
