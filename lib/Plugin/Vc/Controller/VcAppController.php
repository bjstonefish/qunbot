<?php

class VcAppController extends AppController {

     public function beforeFilter() {
        parent::beforeFilter();
        $this->loadModel('Vc.Project');
        // 把插件中需要用到的模块都加载进来，否则在其他内容中加载模块时，没有plugin的前缀，加载的是默认的appmodel
    }
}
?>