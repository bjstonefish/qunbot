<?php

// Hook.helpers.xxx一行增加一个钩子文件，文字中实现的钩子数不限
// 不同的钩子设置不同的.xxx,不然会被覆盖
// 变量混淆加密，不支持 global，而使用$GLOBALS
// 变量混淆加密，不支持extract方法，要使用数组方式来使用变量
/***
 *
 *  更改菜单内容后，需要更新缓存才能在页面生效
 */


/*后台顶层菜单*/
$GLOBALS['hookvars']['navmenu'][] = array('Menu'=>Array('name' => 'VC','slug' => 'vc'));
/*后台左侧二级菜单，第二位数组的索引与顶层菜单的slug对应 */
$GLOBALS['hookvars']['submenu']['vc'] = Array(Array(
	'Menu' =>Array('name' => 'VC','link' => '#'),
    'children' => Array(
        Array('Menu' => Array('name' => 'VC机构','link' => '/admin/vc/investors/list')),
		Array('Menu' => Array('name' => '创业项目','link' => '/admin/vc/poineerings/list')),
        Array('Menu' => Array('name' => '投资记录','link' => '/admin/vc/invest_logs/list')),
	),
));

