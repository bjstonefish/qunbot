<?php
/**
 * TranslateHook Component
 *
 * PHP version 5
 *
 * @category Component
 * @package  MIAOCMS
 * @version  1.0
 * @author   Arlon <MIAOCMS@google.com>
 * @link     http://www.MIAOCMS.net
 */


class OauthHookComponent extends Component {


    public $components = array('Session','Cookie');

/**
 * Called after activating the hook in ExtensionsHooksController::admin_toggle()
 *
 * @param object $controller Controller
 * @return void
 */
    function onActivate(&$controller) {
        $controller->MIAOCMS->addAco('Translate');
        $controller->MIAOCMS->addAco('Translate/admin_index');
        $controller->MIAOCMS->addAco('Translate/admin_edit');
        $controller->MIAOCMS->addAco('Translate/admin_delete');
    }
/**
 * Called after deactivating the hook in ExtensionsHooksController::admin_toggle()
 *
 * @param object $controller Controller
 * @return void
 */
    function onDeactivate(&$controller) {
        $controller->MIAOCMS->removeAco('Translate');
    }

    /**
     * 基于TOP协议的登录授权方式介绍 
     * http://open.taobao.com/doc/detail.htm?spm=a219a.7386781.0.0.gFsJwz&id=105
     * 回调URL
     * http://www.miaomiaoxuan.com/?top_appkey=12336677&top_parameters=ZXhwaXJlc19pbj04NjQwMCZpZnJhbWU9MSZyMV9leHBpcmVzX2luPTg2NDAwJnIyX2V4cGlyZXNfaW49ODY0MDAmcmVfZXhwaXJlc19pbj0yNTkyMDAwJnJlZnJlc2hfdG9rZW49NjEwMTAxNmU0NmY2N2IwMTczNmIwOGI0MTJmMTQ3ZTgyZmVmNjVkZjM5N2FmYTIxOTIzMzkwODQmdHM9MTQwNzU5Nzk2OTUyMyZ2aXNpdG9yX2lkPTE5MjMzOTA4NCZ2aXNpdG9yX25pY2s9aWRlYW11c3RlciZ3MV9leHBpcmVzX2luPTg2NDAwJncyX2V4cGlyZXNfaW49ODY0MDA%3D&top_session=6100c161ff32a9c1566f55c716bfad990d1cd4bca550fe5192339084&agreement=true&agreementsign=12336677-23459965-192339084-0D79D1BBC0DFFC29FD5874A859F730ED&top_sign=Mrg2%2FqhiDxSbhMPXAjXpdw%3D%3D
     * top_parameters是插件容器传递给插件的参数集合，具体的参数内容见：
     * http://open.taobao.com/doc/detail.htm?spm=a219a.7386781.0.0.P8lpeu&id=110
     * 基于TOP协议的登录返回参数校验 
     * top_parameters具体的产生方式是：base64(key1=value1&key2=value2……；
     **/
    function startup(Controller $controller) {
        
    }
/**
 * Called after the Controller::beforeRender(), after the view class is loaded, and before the
 * Controller::render()
 *
 * @param object $controller Controller with components to beforeRender
 * @return void
 */
    function beforeRender(Controller $controller) {
       
    }
    
    function loginSuccess(Controller $controller)
    {
    	$user_oauths = $controller->Oauthbind->find('all',array(
			'conditions'=>array(
				'user_id' => $controller->Auth->user('id'),
			),
		));
		$controller->Session->write('Auth.Oauthbind',$user_oauths);	
    }
    
   
}
?>