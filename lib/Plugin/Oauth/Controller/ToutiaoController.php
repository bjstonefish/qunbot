<?php

/**
 * toutiao登录在相同域 。
 * https://open.mp.toutiao.com/
 * 
 * @author arlon
 *
 */
App::uses('RequestFacade', 'Network');

class toutiaoController extends OauthAppController {

    public $name = 'toutiao';
    
    static public $client_key='ae8adce1f60d299c';
    static public $secret_key='3dfbdea223d87c79d934c01014a0d096';
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Hook->loadhook('UCenterHook');
    }

    private function _ucenter_login($user){
        $this->Hook->loadhook('ScoreHook');
        $hook_results = $this->Hook->call('loginSuccess',array($user));
        $login_score = $hook_results['ScoreHook']; unset($hook_results['ScoreHook']); // 积分数不显示在提示消息中
        $user['login_hook_msg'] = implode("\r\n",$hook_results);
        return $user;
    }

    public function login() {
        //$this->Auth->redirect($_SERVER['HTTP_REFERER']);
        $this->Session->write('oauth_referer',$_SERVER['HTTP_REFERER']);
        $state = $_REQUEST['state'] ? $_REQUEST['state'] : md5(uniqid(rand(), TRUE));
        $redirect_uri = Router::url('/oauth/toutiao/loginCallback',true);
        // display 0	有导航条,1	mini导航条,2	无导航条
        /**
         * 接口地址 https://open.mp.toutiao.com/ 
         * @var unknown
         */
        $url = 'https://open.snssdk.com/auth/authorize/?response_type=code&client_key='.self::$client_key.'&redirect_uri='.$redirect_uri.'&state='.$state.'&auth_only=1&display=0';
        header('Location: '.$url);
        exit;
    }

    public function loginCallback() {
        $oauthkeys = $this->Session->read('Auth.toutiaoOauthKeys');
//         $qc = new QC();
        $login_token = $this->Session->read('toutiao_auth_login_token');
        if( $login_token && $login_token['code'] == $_GET['code'] ) {
            $access_token = $login_token['access_token'];
            $open_id = $login_token['open_id'];
            
            $toutiao_user = $this->Session->read('toutiao_auth_user');
        }
        else{
            $url = 'https://open.snssdk.com/auth/token/?client_key='.self::$client_key.'&client_secret='.self::$secret_key.'&code='.$_REQUEST['code'].'&grant_type=authorize_code';
            App::uses('RequestFacade', 'Network');
            $response =  RequestFacade::get($url);
            $ret  = json_decode($response->body,true);
            $toutiao_user = $ret['data'];
            
            $open_id = $toutiao_user['open_id'];
            $access_token = $toutiao_user['access_token'];
        }
        
        /*
         * Array ( [data] => Array ( [screen_name] => 齐龙4 [open_id] => 564178180014 [access_token] => cc841e17efbcfa34bfb6cd2f22db4ba90014 [expires_in] => 1485733318 [uid_type] => 12 [uid] => 6942826404 ) [ret] => 0 ) 
         * */
        
        $this->loadModel('User');$this->loadModel('Oauthbind');
        $userinfo = $this->User->find('first', array(
                    'conditions' => array(),
                    'recursive' => -1,
                    'joins' => array(
                        array(
                            'table' => Inflector::tableize('Oauthbind'),
                            'alias' => 'Oauthbind',
                            'type' => 'inner',
                            'conditions' => array(
                                "Oauthbind.user_id = User.id",
                                "Oauthbind.source" => 'toutiao',
                                "Oauthbind.oauth_openid" => $open_id,
                            ),
                        ),
                    ),
                    'fields' => array('User.*', 'Oauthbind.*'),
                ));
        //print_r($userinfo);exit;
        $current_time = date('Y-m-d H:i:s');
        
        if ($toutiao_user['gender'] == '男') {
            $gender = 1; //男
        } else {
            $gender = 0; //女
        }
        if (empty($userinfo)) { //该toutiao号未绑定过用户。
            
            if($this->currentUser) {
                // 对已登录的用户，直接绑定用户
                $user_id = $this->currentUser['id'];
                /*$hasbind = $this->Oauthbind->find('first',array( 'conditions' => array('user_id' => $user_id,'source'=>'toutiao' )));
                if( !empty($hasbind) ){
                	$this->__message('这个登录账号已经绑定过toutiao号了，一个绑定只能绑定一个toutiao号。');
                }*/
                $oauth_bind = array(
                    'user_id' => $user_id,
                    'oauth_openid' => $open_id,
                    'oauth_name' => $toutiao_user['nickname'],
                    'oauth_token' => $access_token,
                    'source' => 'toutiao',
                );
                $this->Oauthbind->save($oauth_bind);
                
                $this->_success($user_id,$_REQUEST['state']);
                exit;
            }
            else{
                // 第一次使用第三方帐号登录，用户不存在的，要求输入用户名，密码和邮箱。
                /**
                 * Todo. 注册新用户或者绑定以注册用户
                 */
                $this->set('username',$toutiao_user['screen_name']);
                
                $this->Session->write('toutiao_auth_user', $toutiao_user);
                $this->Session->write('toutiao_auth_login_token', array(
                    'access_token'=> $access_token,
                    'open_id' => $open_id,
                    "code" => $_GET['code']
                ));
                return;
            }
            
        } else {	 // toutiao号已绑定的用户。
            // 登录成功 
            $updateinfo = array(
                'nickname' => $toutiao_user['screen_name'],
                'screen_name' => $toutiao_user['screen_name'],
                'last_login' => $current_time,
            );
            
            $userinfo['User'] = array_merge($userinfo['User'], $updateinfo);
            
            $oauth_bind = $userinfo['Oauthbind'];
            
            $user_id = $userinfo['User']['id'];
            $this->User->save($userinfo['User']);
            
            
            $userinfo = $this->User->getUserInfo($userinfo['User']['id']); //重新获取用户信息，保护用户角色等其它内容，避免权限出错。
            
            $this->Session->write('Auth.User', $userinfo['User']);
            $this->Cookie->write('Auth.User', $userinfo['User'], true, 2592000);

            
            $oauth_bind['updated'] = time();
            $oauth_bind['oauth_token'] = $access_token;
            $this->Oauthbind->save($oauth_bind);

            $this->_success($user_id,$_REQUEST['state']);
            exit;
        }
    }

    public function register(){
        $this->autoRender = false;
        $toutiao_user = $this->Session->read('toutiao_auth_user');
        $login_token = $this->Session->read('toutiao_auth_login_token');

        $current_time = date('Y-m-d H:i:s');
        
        $referer =  $this->request->referer();
//     	}
    	if( $this->data['User']['password'] != $this->data['User']['password_confirm']  ) {
            $this->__message(__('密码与确认密码不一致'), $referer);
        }
        if(empty( $this->data['User']['password'] )){
        	$this->data['User']['password'] = random_str(12);
        }
        
        
        $userinfo = array(
//             'email' => $this->data['User']['email'],
            'password' => Security::hash($this->data['User']['password'], null, true),
            'username' => $toutiao_user['screen_name'],
            'nickname' => $toutiao_user['screen_name'],
            'screen_name' => $toutiao_user['screen_name'],
            'role_id' => 2,
            'last_login' => $current_time,
            'created' => $current_time,
            'activation_key' => md5(uniqid()),
            'status' => 1,
        );
        $this->loadModel('User');
        $this->loadModel('Oauthbind');
        if( $this->User->save($userinfo) ) {
            $userinfo['id'] = $user_id = $this->User->getLastInsertID();
            $this->Session->write('Auth.User', $userinfo);
            $this->Cookie->write('Auth.User', $userinfo, true, 2592000);
            
            $oauth_bind = array(
                'user_id' => $user_id,
                'oauth_openid' => $login_token['open_id'],
                'oauth_name' => $toutiao_user['screen_name'],
                'oauth_token' => $login_token['access_token'],
                'expires' => $toutiao_user['expires_in'],
                'source' => 'toutiao',
            );
            $this->Oauthbind->save($oauth_bind);
            
            $this->_success($user_id,$_REQUEST['state']);
        }
        else{
            $errors = $this->User->invalidFields();
            print_r($errors);
        }
        exit;

    }
    
    /**
     * 有本站用户，绑定本站用户
     */
    public function bind(){
        
        if( empty($this->data['User']['email']) ) {
            $this->__message(__('邮箱不能为空'), '');
        }
        
        $this->autoRender =  false;
        $this->loadModel('User');
        
        if(Validation::email($this->data['User']['email'])) {
            $userinfo = $this->User->getUserByEmail($this->data['User']['email']);
        }
        else{
            $userinfo = $this->User->getUserByMobile($this->data['User']['email']);
        }
        
        if( !empty($userinfo) && Security::hash($this->data['User']['password'] , null, true) == $userinfo['User']['password'] ) {
            
            $login_token = $this->Session->read('toutiao_auth_login_token');
            $toutiao_user = $this->Session->read('toutiao_auth_user');
            
            $user_id = $userinfo['User']['id'];
            $this->loadModel('Oauthbind');
            $oauth_bind = $this->Oauthbind->find('first', array(
                'conditions' => array(
                    'oauth_openid' => $login_token['open_id'],
                    'user_id' => $user_id,
                    'source' => 'toutiao',
                ),
            ));
            
            if (empty($oauth_bind)) {
                $oauth_bind = array(
                    'user_id' => $user_id,
                    'oauth_openid' => $login_token['open_id'],
                    'oauth_name' => $toutiao_user['screen_name'],
                    'oauth_token' => $login_token['access_token'],
                    'expires' => $toutiao_user['expires_in'],
                    'source' => 'toutiao',
                );
                $this->Oauthbind->save($oauth_bind);
            } else {
                $oauth_bind['Oauthbind']['updated'] = date('Y-m-d H:i:s');
                $oauth_bind['Oauthbind']['oauth_token'] = $login_token['access_token'];
                $this->Oauthbind->save($oauth_bind);
            }
            $this->Session->write('Auth.User', $userinfo['User']);
            $this->Cookie->write('Auth.User', array(
                'id' => $userinfo['User']['id'],
                'username' => $userinfo['User']['username'],
                'email' => $userinfo['User']['email'],
                'roles' => count($userinfo['User']['role_id']),
            ), true, 2592000);
            
            $this->_success($user_id,$_REQUEST['state']);
        }
        else{
            echo json_encode(array('ret' => -1,'error'=>'Email or password error.'));
        }
        exit;
    }
    
    /**
     * toutiao登录，申请开通的域名为www.135editor.com
     * @param unknown $user_id
     * @param string $state
     */
    private function _success($user_id,$state=''){
    	$content = '';
    	
    	$appkey = '57326a20-e9cc-461a-8b90-199c9fde689a';
    	$secretKey = 'WYFDXgFMZPFiilBdCWGfqyjSLf1SAsSMG9Pxxz2COLcN';
    	 
    	$ts = time();
    	$u = Security::rijndael($user_id, $secretKey, 'encrypt');
    	 
    	//     	echo Security::rijndael($u, $secretKey, 'decrypt');
    	$u = base64_encode($u);
    	$sign = md5($appkey.$secretKey.$u.$ts);
    	
    	if($state == '135plat'){    		
    		// 从www.135plat.com点开，弹窗打开的是135editor域名登录页。135editor登录成功后，跳转到135plat域名
    		$url = 'http://www.135plat.com/users/login?a=appsync&ts='.$ts.'&appkey='.$appkey.'&u='.urlencode($u).'&sign='.$sign;
    		$this->redirect($url);
    	}
    	else{
    	    $redirect = $this->Session->read('Auth.redirect');
    	    $redirect = $redirect ? $redirect : '/users/index';
    	    if($redirect == '/users/login') {
    	        $redirect = '/users/index';
    	    }
    	    
//     		$isvip = false;
//     		if(substr($state,0,3)=='vip') {
//     			$isvip = true;
//     			$state = substr($state,3);
//     			$url = 'http://vip.135editor.com/users/login?a=appsync&ts='.$ts.'&appkey='.$appkey.'&u='.urlencode($u).'&sign='.$sign;
//     			$this->redirect($url);
//     		}
    		// 从www.135editor.com登录后，需要同步登录135plat
    		if($state == 'postmsg'){
    			// 通过ajax重新调用login操作，实现加载用户信息以及提示登录
    			$content = '已成功登录,窗口将自动关闭。<script>document.domain="135editor.com";if(window.opener) {window.opener.postMessage("loginSuccess","*");window.close();}else{location.href="'.$redirect.'";}</script>';
    		}
    		else{
    			$url = 'http://www.135plat.com/users/login?a=appsync&ts='.$ts.'&appkey='.$appkey.'&u='.urlencode($u).'&state='.$state.'&sign='.$sign;
    			$this->Session->setFlash( __('Login Success').'<script type="text/javascript" src="'.$url.'"></script>');
	    		$content = '已成功登录,窗口将自动关闭。<script>document.domain="135editor.com";if(window.opener) {window.opener.location.reload(true);window.close();}else{location.href="'.$redirect.'";}</script>';
	    	}
    	}
    	$this->response->body($content);
    	$this->response->send();
    	exit;
    }

}

?>