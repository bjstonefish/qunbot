<?php

/**
 * 自定义语言管理
 * @author arlonzou
 *
 */
class DefinedLanguagesController extends DefinedLanguageAppController {

	var $name = 'DefinedLanguages';
	
	function admin_delete($key = NULL){

	    if($key) {
            $this->DefinedLanguage->deleteAll(array('key'=>$key));
            $this->Session->setFlash(__('Record deleted.'));
            $this->redirect('edit');
        }
	}

	function admin_index(){
		$lang_alias = Configure::read('Site.language');
		$langs = Configure::read('System.ActiveLanguage');
		$lang_id = $langs[$lang_alias]['id'];
		$defined_languages = $this->DefinedLanguage->find('all', array(
			'conditions' => array(
				'alias' => $lang_id,
			),
			'limit' => 30,
		));
		$this->set('defined_languages', $defined_languages);
	}
	
	function admin_add(){
		$langs = Configure::read('System.ActiveLanguage');
		$this->set('langs',$langs);
		if(!empty($this->data)){
			foreach($this->data['DefinedLanguage']['value'] AS $id => $value){
				$new_definition = array();
				$new_definition['DefinedLanguage']['language_id'] = $id;
				$new_definition['DefinedLanguage']['key'] = $this->data['DefinedLanguage']['key'];
				$new_definition['DefinedLanguage']['value'] = $value;
				$new_definition['DefinedLanguage']['updated'] = date('Y-m-d H:i:s');
				$new_definition['DefinedLanguage']['created'] = date('Y-m-d H:i:s');
				$this->DefinedLanguage->create();
				$this->DefinedLanguage->save($new_definition);
			}
			$resultinfo['success'] .= __('Add data success');
			$this->set('resultinfo', $resultinfo);
			//$this->redirect('index');
			echo json_encode($resultinfo);
			exit;
		}
	}

	function admin_editkey($defined_language_key = ""){
		if(empty($this->data)){
			 $item = $this->DefinedLanguage->find('first', array('conditions' => array('key' => $defined_language_key)));

			 if(empty($item))
			 {
			 	echo lang('the_key_not_exists.');
			 	$this->__message(lang('the_key_not_exists.'));
			 }


			$this->set('defined_key',$defined_language_key);
		}
		else{
			$this->DefinedLanguage->updateAll(
				array('key'=> $this->DefinedLanguage->escape_string($this->data['DefinedLanguage']['key']) ),
				array('key'=> $defined_language_key));
			
			if ($this->RequestHandler->accepts('json') || $this->RequestHandler->isAjax() || isset($_GET['inajax'])) {
				$resultinfo['success'] .= __('Edit data success');
				$this->set('resultinfo', $resultinfo);
        		echo json_encode($resultinfo);
				exit;
        	}
        	else{
        		$this->redirect('index');
        	}
		}
	}
	
	function admin_edit ($defined_language_key = "",$copy=NULL){
		$this->pageTitle = __('Defined Language Details');
		if(empty($this->data)){
			$data = $this->DefinedLanguage->find('all', array('conditions' => array('key like' => '%'.$defined_language_key.'%')));
			$lang_definitions = array();
			foreach($data AS $key => $value){
				$array_key = $value['DefinedLanguage']['language_id'];
				$lan_key = $value['DefinedLanguage']['key'];
				$lang_definitions[$lan_key][$array_key] = $value;
			}
			$this->set('lang_definitions', $lang_definitions);
			$this->set('defined_key',$defined_language_key);
			$this->set('languages', $this->DefinedLanguage->Language->find('all', array('conditions' => array('locale !='=>'zh_tw'), 'order' => array('Language.id ASC'))));
		}
		else{
			// print_r($_POST);exit;
			
			foreach($this->data['DefinedLanguage'] AS $key => $item){
				foreach($item as $id => $value){
					if(empty($value['value']))	continue;
					$new_definition = array();
					if($id){
						$new_definition['DefinedLanguage']['id'] = $id;
					}
					else{
						$this->DefinedLanguage->create();
						$new_definition['DefinedLanguage']['id'] = null;
					}
					$new_definition['DefinedLanguage']['language_id'] = $value['language_id'];
					$new_definition['DefinedLanguage']['key'] = $key;
					$new_definition['DefinedLanguage']['value'] = $value['value'];
					$this->DefinedLanguage->save($new_definition);
				}
			}
			$resultinfo = array('ret'=>0,'msg'=> __('Edit data success'));
			$this->set('resultinfo', $resultinfo);
			echo json_encode($resultinfo);
			exit;
		}
	}
}
?>