<?php
App::uses('TaobaoAppModel','Taobao.Model');
class TaobaoCate extends TaobaoAppModel {	
	var $name = 'TaobaoCate';
	
	var $actsAs = array(
			'Tree'=> array('left'=>'left','right'=>'right'),
	);
	
	var $validate = array(
        'cid' => array(
    		'unique'=>array(
	            'rule' => 'isUnique',
	            'message' => 'The cid has already been taken.',
    		),    		
        ),        
    );

}
?>