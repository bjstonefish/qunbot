<?php

// App::uses('TaobaoAppModel', 'Taobao.Model');

class Taobaoke extends TaobaoAppModel {

    var $name = 'Taobaoke';
    var $validate = array(
        'num_iid' => array(
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'The num_iid has already been taken.',
            ),
        ),
    );
    var $hasMany = array(
        'TaobaoTradeRate' => array(
            'className' => 'TaobaoTradeRate',
            'foreignKey' => 'taobaoke_id',
            'conditions' => array('TaobaoTradeRate.origin' => 'Taobao'),
            'order' => 'TaobaoTradeRate.created DESC',
            'limit' => '10',
            'dependent' => true
        )
    );

}
?>