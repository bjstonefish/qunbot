<?php

/**
 * 评价相关操作，取20条保存。评价cron更新的周期可设大一些。
 * cron: /admin/taobao/taobao_trade_rates/getAllTradeRate
 */
class TaobaoTradeRatesController extends TaobaoAppController {

    public $name = 'TaobaoTradeRates';

    public function beforeFilter() {
        parent::beforeFilter();
    }

    /* 获取商品的评价信息 */

    public function admin_getAllTradeRate() {
        $page = intval($this->request->query['page']) ? intval($this->request->query['page']) : 1;
        $pagesize = 20;
        do {
            $items = $this->Taobaoke->find('all', array(
                        'conditions' => array('published' => 1, 'deleted' => 0),
                        'limit' => $pagesize,
                        'fields' => array('id','num_iid','nick'),
                        'page' => $page,
                        'order' => 'volume desc'
                    ));
            foreach ($items as $item) {
                $num_iid = $item['Taobaoke']['num_iid'];
                $this->admin_getOneTradeRate($num_iid,$item['Taobaoke']['nick'],$item['Taobaoke']['id']);
                sleep(1);
            }
            echo "get page $page \r\n";
            $page++;
        } while (count($items) == $pagesize);

        $this->__message(__('over'), '#', 999999);
    }
    
    public function admin_saecron_getAllTradeRate() {
    	$page = intval($this->request->query['page']) ? intval($this->request->query['page']) : 1;
    	$pagesize = 10;
    	$items = $this->Taobaoke->find('all', array(
                    'conditions' => array('published' => 1, 'deleted' => 0),
                    'limit' => $pagesize,
                    'fields' => array('id','num_iid','nick'),
                    'page' => $page,
                    'order' => 'volume desc'
        ));
        foreach ($items as $item) {
            $num_iid = $item['Taobaoke']['num_iid'];
            $this->admin_getOneTradeRate($num_iid,$item['Taobaoke']['nick'],$item['Taobaoke']['id']);
        }
        echo "get page $page \r\n";
        
    	if (count($items) == $pagesize && empty($_GET['skip_next'])){
			$this->TaskQueue->add('/admin/taobao/taobao_trade_rates/saecron_getAllTradeRate?page:'.($page+1));
        }
        exit;
    }

    /**
     * 获取商品的评价信息
     * @param $num__iid  商品编号 for test,12224681317, 5210490640,,,
     * @param $seller_nick 商家的昵称
     */
    public function admin_getOneTradeRate($num_iid, $seller_nick,$taobaoke_id) {

//        $this->TaobaoTradeRate->deleteAll(array('num_iid' => $num_iid), true, true);
        $this->autoRender = false;
        $c = new TopClient();
        $req_array = array(
            "method" => "taobao.traderates.search",
            'num_iid' => $num_iid,
            'seller_nick' => $seller_nick,
        );
        $resp = $c->execute($req_array);
        if ($resp['total_results']) {
            foreach ($resp['trade_rates']['trade_rate'] as $traderate) {
            	
            	if (empty($traderate['content'])) { //无内容时跳过
            		continue;
            	}
            	
                $exists_items = $this->TaobaoTradeRate->find('first', array(
                            'conditions' => array('taobaoke_id' => $taobaoke_id, 'tid' => $traderate['tid'], 'oid' => $traderate['oid']),
                            'fields' => array('id'),
                        ));
                if (!empty($exists_items)) { //如果已存在，则跳过
                    continue;
                }

                $this->TaobaoTradeRate->create();
                $this->data['TaobaoTradeRate']['nick'] = $traderate['nick'];
                $this->data['TaobaoTradeRate']['taobaoke_id'] = $taobaoke_id;
                $this->data['TaobaoTradeRate']['tid'] = $traderate['tid'];
                $this->data['TaobaoTradeRate']['oid'] = $traderate['oid'];
                $this->data['TaobaoTradeRate']['name'] = $traderate['content'];
                $this->data['TaobaoTradeRate']['reply'] = $traderate['reply'];
                $this->data['TaobaoTradeRate']['role'] = $traderate['role'];
                $this->data['TaobaoTradeRate']['result'] = $traderate['result'];
                $this->data['TaobaoTradeRate']['created'] = $traderate['created'];
                $this->data['TaobaoTradeRate']['origin'] = 'Taobao';
                $this->TaobaoTradeRate->save($this->data);
            }
            echo 'Get ' . $num_iid . " success.\r\n";
            return true;
        }
        echo 'Get ' . $num_iid . " false.\r\n";
        return false;
    }

}
?>