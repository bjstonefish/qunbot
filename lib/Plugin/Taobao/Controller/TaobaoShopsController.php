<?php

class TaobaoShopsController extends TaobaoAppController {

    public $name = 'TaobaoShops';

    public function view($nick){       
        $modelClass = $this->modelClass;
        $nick = trim($nick);
        if (!empty($nick) && $nick != strval(intval($nick))) {
            ${$modelClass} = $this->TaobaoShop->find('first',array('conditions'=>array('nick'=> $nick)));
        }
//        if(empty(${$modelClass})){ 
//            throw new NotFoundException(__('Error url,this url page is not exist.'));
//        }
        $this->set('TaobaoShop',${$modelClass});

        $taobaokes = $this->Taobaoke->find('all',array('conditions'=>array('nick'=> $nick)));

        $this->set('taobaokes',$taobaokes);
    }

    /* 折扣 */

    public function admin_get() {
        $page = 1;
        $pagesize = 20;

        do {
            $items = $this->Taobaoke->find('all', array(
                        'conditions' => array('published' => 1, 'deleted' => 0),
                        'limit' => $pagesize,
                        'fields' => array('nick'),
                        'page' => $page,
                        'group'=>'nick'
                    ));
            foreach ($items as $item) {
                
                $nick = $item['Taobaoke']['nick'];
                $re = $this->admin_getOneShop($nick);
//                if ($re) {
//                    echo 'Get ' . $nick . " success.\r\n";
//                } else {
//                    echo 'Get ' . $nick . " false.\r\n";
//                }
//                sleep(1);
            }
            echo "get page $page \r\n";
            $page++;
        } while (count($items) == $pagesize);

        $this->__message(__('over'), '#', 999999);
    }

    public function admin_shopConvert(){

        $this->autoRender = false;
        $page = intval($this->params['named']['page']) ? intval($this->params['named']['page']) : 1;
        $pagesize = 10; // 每次取10条，top Shops.convert接口支持10条
        do {
            $items = $this->TaobaoShop->find('all', array(
                        'conditions' => array('published' => 1, 'deleted' => 0),
                        'limit' => $pagesize,
                        'fields' => array('sid'),
                        'page' => $page
                    ));
            $sids = array();
            foreach ($items as $item) {
                if ($item['TaobaoShop']['sid']) {   
                    $this->__convertShopItem($item['TaobaoShop']['sid']);
                }
            }
            echo "==$page===<BR/>";
            $page++;
        } while (count($items) == $pagesize);
        echo 'over';

    }

    /**
     * 获取商品的优惠信息
     * @param $num__iid  商品编号 for test,12224681317, 5210490640,,,
     */
    public function admin_getOneShop($nick) {
        $this->autoRender = false;
        $c = new TopClient();
        $req_array = array(
            "method" => "taobao.shop.get",
            'fields' => 'sid,cid,nick,title,desc,bulletin,pic_path,created,modified,shop_score',
            'nick' => $nick,
        );

        $resp = $c->execute($req_array);

        if ($resp['shop']) {
            $shop = $resp['shop'];
               
            $exists_items = $this->TaobaoShop->find('first', array(
                        'conditions' => array('sid' => $shop['sid']),
                        'fields' => array('id'),
                    ));
            unset($this->TaobaoShop->id,$this->data['TaobaoShop']['id']);
            if (!empty($exists_items)) { //如果已存在，则跳过
                $this->data = $exists_items;
            }
            $this->TaobaoShop->create();
            $this->data['TaobaoShop']['sid'] = $shop['sid'];
            $this->data['TaobaoShop']['cid'] = $shop['cid'];
            $this->data['TaobaoShop']['nick'] = $shop['nick'];
            $this->data['TaobaoShop']['pic_path'] = $shop['pic_path'];
            $this->data['TaobaoShop']['name'] = $shop['title'];
            $this->data['TaobaoShop']['content'] = $shop['desc'];
            $this->data['TaobaoShop']['bulletin'] = $shop['bulletin'];
            $this->data['TaobaoShop']['updated'] = $shop['modified'];
            $this->data['TaobaoShop']['created'] = $shop['created'];
            $this->data['TaobaoShop']['delivery_score'] = $shop['shop_score']['delivery_score'];
            $this->data['TaobaoShop']['item_score'] = $shop['shop_score']['item_score'];
            $this->data['TaobaoShop']['service_score'] = $shop['shop_score']['service_score'];
            $this->TaobaoShop->save($this->data);
            echo 'Get ' . $nick . " success.\r\n";
            return true;
        }
        echo 'Get ' . $nick . " false.\r\n";
        return false;
    }


    /**
     * 根据商铺的sid, 淘宝接口：taobao.taobaoke.shops.convert
     * @param $sids 商铺编号，值为字符串,每次传入单个商品
     * @return boolean
     */
    private function __convertShopItem($sids = null) {

        if (!empty($sids)) {
            $c = new TopClient();
            $req_array = array(
                "method" => "taobao.taobaoke.shops.convert",
                'fields' => 'user_id,shop_title,click_url,commission_rate,seller_credit,shop_type,total_auction,auction_count', //返回字段
                'nick' => TOP_NICK,
                "sids" => $sids,
            );

            // 此接口支持返回volume
            $resp = $c->execute($req_array);
            if (!empty($resp['taobaoke_shops'])) {
                $db = $this->TaobaoShop->getDataSource();                
                $taobaokeItem  = $resp['taobaoke_shops']['taobaoke_shop'] ;
                $this->Taobaoke->create();
                $this->data['TaobaoShop'] = array();
                $this->data['TaobaoShop']['name'] = $taobaokeItem['shop_title'];
                $this->data['TaobaoShop']['user_id'] = $taobaokeItem['user_id'];
                $this->data['TaobaoShop']['click_url'] = $taobaokeItem['click_url'];
                $this->data['TaobaoShop']['commission_rate'] = $taobaokeItem['commission_rate'];
                $this->data['TaobaoShop']['seller_credit'] = $taobaokeItem['seller_credit'];
                $this->data['TaobaoShop']['shop_type'] = $taobaokeItem['shop_type'];
                $this->data['TaobaoShop']['total_auction'] = $taobaokeItem['total_auction'];
                $this->data['TaobaoShop']['auction_count'] = $taobaokeItem['auction_count'];
                $this->data['TaobaoShop']['published'] = 1;

                $exists_items = $this->TaobaoShop->find('all', array(
                            'conditions' => array('sid' => $sids,),
                            'fields' => array('sid'),
                        ));
                if (!empty($exists_items)) {
                    // 清空内容中的链接，链接不是推广链接，没有提成的都干掉
                    foreach ($this->data['TaobaoShop'] as &$value) {
                        $value = $db->value($value);
                    }
                    $this->TaobaoShop->updateAll($this->data['TaobaoShop'], array('sid' => $sids));
                }
                return true;
            }
            else{
                return false;
            }
        }
        return false;
    }

}

?>