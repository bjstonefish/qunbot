<?php

class TaobaokeHookComponent extends Component {
		/*******重要： action的li之间要一个紧连着一个 ，不要有换行或空格，否则会由于空格符引起显示错位。*************************/
	function gridDataAction($controller,$model,$data){
		if($model=='Taobaoke'){
			return  '<li class="ui-state-default grid-row-action" data-action_url="'.Router::url('/admin/Taobao/taobaokes/updateTaobaoke/'.$data['num_iid']).'" title="'.__('Get Detail').'"><span class="ui-icon ui-icon-locked"></span></li>'.
            	'<li class="ui-state-default grid-row-action" data-action_url="'.Router::url('/admin/Taobao/taobao_trade_rates/getOneTradeRate/'.$data['num_iid'].'/'.$data['nick'].'/'.$data['id']).'" title="'.__('Get Trade Rate Info').'"><span class="ui-icon ui-icon-locked"></span></li>';
		}
	}
}

?>