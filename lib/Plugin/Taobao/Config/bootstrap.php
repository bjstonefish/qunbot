<?php

// Hook.helpers.xxx一行增加一个钩子文件，文字中实现的钩子数不限
// 不同的钩子设置不同的.xxx,不然会被覆盖
// 变量混淆加密，不支持 global，而使用$GLOBALS
// 变量混淆加密，不支持extract方法，要使用数组方式来使用变量
/*后台顶层菜单*/
$GLOBALS['hookvars']['navmenu'][] = array('Menu'=>Array('name' => '淘宝客','slug' => 'taobao'));
Configure::write('Hook.helpers.Taobaoke','Taobao.TaobaokeHook');
Configure::write('Hook.components.Taobaoke','Taobao.TaobaokeHook');
/*后台左侧二级菜单，第二位数组的索引与顶层菜单的slug对应 */
$GLOBALS['hookvars']['submenu']['taobao'] = Array(Array(
	'Menu' =>Array('name' => '淘宝客','link' => '#'),
    'children' => Array(
        Array('Menu' => Array('name' => '产品分类','link' => '/admin/taobao/taobao_cates/list')),
		Array('Menu' => Array('name' => '淘宝商品','link' => '/admin/taobao/taobaokes/list')),
        Array('Menu' => Array('name' => '淘宝店铺','link' => '/admin/taobao/taobao_shops/list')),
        Array('Menu' => Array('name' => '商品评价','link' => '/admin/taobao/taobao_rates/list')),
		Array('Menu' => Array('name' => '优惠信息','link' => '/admin/taobao/taobao_promotions/list'))
	)
));