<?php
/**
 * ExampleHook Helper
 *
 * An example hook helper for demonstrating hook system.
 *
 * @category Helper
 * @package  Croogo
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class ViewpointsHookHelper extends AppHelper {
    public $helpers = array(
        'Html',
    );
	public function  __construct(View $View, $settings = array()){
		parent::__construct($View,$settings);
	}
	public function affterContent($model,$dataid,$action='view')
	{

		$script = '<script type="text/javascript" src="'.Router::url('/stats_days/numlog/'.$model.'/'.$data_id.'/view').'"></script>
			<script>
				$(function(){
					loadDiggData();
				})
			</script>';
		
		$this->_View->append('bottomscript',$script);

		$html = $this->_View->element('digg',array(),array('plugin'=>'Communicate'));
		if($model == 'Article'){
			$html .= $this->_View->element('viewpoints',array(),array('plugin'=>'Communicate'));
		}
		$html .= $this->_View->element('next_items',array(),array('plugin'=>'Communicate'));
		return $html;
	}

}
?>