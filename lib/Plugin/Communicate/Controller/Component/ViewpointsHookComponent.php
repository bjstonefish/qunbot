<?php
class ViewpointsHookComponent extends Component {

    function onActivate($controller) {
       
    }
/**
 * Called after deactivating the hook in ExtensionsHooksController::admin_toggle()
 *
 * @param object $controller Controller
 * @return void
 */
    function onDeactivate(Controller $controller) {
    }
/**
 * Called after the Controller::beforeFilter() and before the controller action
 *
 * @param object $controller Controller with components to startup
 * @return void
 */
    function startup(Controller $controller) {

    }
    
    function viewItem(Controller $controller,$modelName,$dataid)
    {
		$controller->loadModel('Viewpoint');
		$conditions = array('Viewpoint.model'=> $modelName,
					'Viewpoint.data_id'=> $dataid);
//		echo $this->current_data_id;
		$Viewpoints = $controller->Viewpoint->find('all',array(
				'conditions' => $conditions,
				'recursive'=>-1,
				'fields' =>array('Viewpoint.*'),
				'limit' => 12,
				'order' => 'Viewpoint.support_nums desc'
			));
//		print_r($Viewpoints);
		$controller->set('Viewpoints', $Viewpoints);
		$controller->set('points_model', $modelName);
		$controller->set('points_dataid', $dataid);
   
		//select top 1 * from table_name where id < "&id&" order by id desc
//select top 1 * from table_name where id > "&id&" order by id asc
//		echo $this->current_data_id;
		$next_item = $controller->{$modelName}->find('first',array(
				'conditions'=>array(
					$modelName.'.id >'=> $dataid,
				),
				'order'=> $modelName.'.'.'id asc',
				'recursive'=>-1,
			));
		$last_item = $controller->{$modelName}->find('first',array(
				'conditions'=>array(
					$modelName.'.id <'=> $dataid,
				),
				'recursive'=>-1,
				'order'=>$modelName.'.'.'id desc'
			));
		$controller->set('next_item', $next_item);
		$controller->set('last_item', $last_item);
		$controller->set('next_item_model', $modelName);
		$controller->set('next_item_controller', Inflector::tableize($modelName));
    }
   
}
?>