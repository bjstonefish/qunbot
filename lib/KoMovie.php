<?php

App::uses('RequestFacade', 'Network');

class KoMovie{
	public $server = 'http://test.komovie.cn/api_movie/service';
	public $md5Key = 'mq3CwYZL'; 
	public $desKey = 'dJ0c6mq5';
	public $channelId = 9;
	
	public $format = 'json';
	
// 	public $request = array('header' => array('channel_id' => 9));
	public $request = array('header' => array('ChannelId' => 9));
	
	/**
	 * 获取地区列表
	 * @return json string or array
	 */
	public function getCities(){
		$url = $this->getActionUrl(array('action' => 'city_query'));
		return $this->getApiResult($url);
	}
	/**
	 * 获取影院电影列表
	 * @param unknown $cinemaId
	 * @return json string or array
	 */
	public function getMovies($cinemaId){
		$url = $this->getActionUrl(array('action' => 'movie_Query','cinema_id' => $cinemaId));
		return $this->getApiResult($url);
	}
	/**
	 * 获取城市电影列表
	 * @param unknown $cityId
	 * @param number $coming
	 * @return json string or array
	 */
	public function getMoviesByCity($cityId,$coming = 0){
		$params = array('action' => 'movie_Query','city_id' => $cityId);
		if( $coming ){
			$params['coming'] = 1;
		}
		if($_GET['page']){
			$params['page'] = $_GET['page'];
		}
		
		$url = $this->getActionUrl($params);
		return $this->getApiResult($url);
	}
	/**
	 * 获取电影的场次
	 * @param unknown $cinemaId
	 * @param unknown $movieId
	 * @return json string or array
	 */
	public function getPlans($cinemaId,$movieId){
		$url = $this->getActionUrl(array('action' => 'plan_Query','cinema_id' => $cinemaId,'movie_id' => $movieId));
		return $this->getApiResult($url);
	}
	/**
	 * 获取场次信息
	 * @param unknown $planId
	 * @return json string or array
	 */
	public function getPlan($planId){
		$url = $this->getActionUrl(array('action' => 'plan_Query','plan_id'=> $planId));
		return $this->getApiResult($url);
	}
	/**
	 * 获取场次的座位信息
	 * @param unknown $planId
	 * @return json string or array
	 */
	public function getSeats($planId){
		$url = $this->getActionUrl(array('action' => 'seat_Query','plan_id' => $planId));
		return $this->getApiResult($url);
	}
	
	/**
	 * 新增选座的订单
	 * @param unknown $mobile
	 * @param unknown $activityId
	 * @param unknown $plan_id
	 * @param unknown $seat_no
	 * @param unknown $seat_info
	 * @return json string or array
	 */
	public function addTicketOrder($mobile,$activityId,$plan_id,$seat_no,$seat_info){
		$params = array(
				'action' => 'order_Add',
				'activity_id' => $activityId,
				'seat_no' => $seat_no,
				'plan_id' => $plan_id,
		);
		if ($seat_info) {
			$params['seat_info'] = $seat_info;
		}
		
		$url = $this->getActionUrl($params);
		return $this->getApiResult($url);
	}
	/**
	 * 确认订单
	 * @param int $orderId
	 * @param unknown $payMethod
	 * @param unknown $balance
	 * @param unknown $bank
	 * @param unknown $couponIds
	 * @return json string or array
	 */
	public function confirmOrder($orderId,$payMethod,$balance,$bank,$couponIds){
		$params = array(
				'action' => 'order_Confirm',
				'order_id' => $orderId,
				'pay_method' => $payMethod,
		);
		if ($balance > 0) {
			$params['balance'] = $balance;			
		}
		if ($bank != null) {
			$params["bank"]  = $bank;
		}
		if ($couponIds != null) {
			$params["coupon_ids"] = $couponIds;			
		}
		
		$url = $this->getActionUrl($params);
		return $this->getApiResult($url);
	}
	/**
	 * 查询订单状态
	 * @param int $orderId
	 * @return json string or array
	 */
	public function queryOrder($orderId){
		$url = $this->getActionUrl(array('action' => 'order_Query','order_id' => $orderId));
		return $this->getApiResult($url);
	}
	/**
	 * 取消订单
	 * @param int $orderId
	 * @return json string or array
	 */
	public function cancelOrder($orderId){
		$url = $this->getActionUrl(array('action' => 'order_Delete','order_id' => $orderId));
		return $this->getApiResult($url);
	}
	
	/**
	 * 获取影院列表
	 * @param string $cityId
	 * @param string $platforms
	 * @return json string or array
	 */
	public function getCinemas($cityId = '',$platforms = ''){
		if(empty($platforms)){
			$platforms = array(10001,10002,10004,10005,10007,10008,);//20004，20001
		}
		$params = array();
		if($cityId){
			$params['city_id'] = $cityId;
		}
		elseif(!empty($platforms)){
			if(is_array($platforms)){
				$platforms = implode(',',$platforms);
			}
			$params['platforms'] = $platforms;
		}
		
		$params['action'] = 'cinema_Query' ;
		
		$url = $this->getActionUrl($params);
		return $this->getApiResult($url);
	}
	/**
	 * 获取影院信息
	 * @param unknown $cinemaId
	 * @return json string or array
	 */
	public function getCinemaInfo($cinemaId){
		$url = $this->getActionUrl(array('action' => 'cinema_Query','cinema_id' => $cinemaId));
		return $this->getApiResult($url);
	}
	
	/**
	 * 获取影片、影院的媒体资源
	 * @param array $params
	 */
	public function getMedia($params){
		$params['action'] = 'media_Query';
		$url = $this->getActionUrl($params);
		return $this->getApiResult($url);
	}
	
	public function getApiResult($url) {
		$content =  RequestFacade::get($url,array(),$this->request);
		if($this->format == 'array'){
			return json_decode($content,true);
		}
		else{
			return $content;
		}
	}
	
	private function getActionUrl($params){
		
		$params['time_stamp'] =  time();
		ksort ($params);
		
		$tmpStr = implode( $params );
		$params['enc'] = strtolower(md5(urlencode($tmpStr.$this->md5Key)));
		$url =  $this->server .'?'.http_build_query($params);
		return $url;
	}
}