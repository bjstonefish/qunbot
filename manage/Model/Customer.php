<?php

class Customer extends AppModel {

    var $name = 'Customer';
    
    var $actsAs = array('EventLog');
    
    public function getUserCustomer($user_id){
    	if(empty($user_id)){
    		return null;
    	}
    	$customer = $this->find('first',array(
				'joins' => array(
						array(
								'table' => Inflector::tableize('Contact'),
								'alias' => 'Contact',
								'type' => 'inner',
								'conditions' => array('Customer.id=Contact.customer_id','Contact.user_id' => $user_id,),
						),
				),
				'fields' => array('Customer.*,Contact.id'),
		));
    	if(!empty($customer['Customer'])){
    		$customer['Customer']['contact_id'] = $customer['Contact']['id'];
    		return $customer['Customer'];
    	}
    	else{
    		return null;
    	}
		
    }
}
?>