<?php
/**
 * 后台模型仅使用主库，不连接从库
 */
App::uses('Model', 'Model');

class AppModel extends Model {
	var $useDbConfig = 'default';
	
	public $recursive = 1;
	
	public $actsAs = array('Containable');
	
	private $_extschema = array();
	
	private $_modelinfo = array();
	private $version_flag = true; // 是否记录版本信息，用于在业务逻辑中设置不记录版本信息
	
	public function __construct($id = false, $table = null, $ds = null) {
		/**
		 * 在parent::__construct中，会初始化$this->Behaviors，并调用behavior的setup方法。
		 */
		parent::__construct($id, $table, $ds);		
		//$this->setDataSource('default');
		if($this->name=='Ajax' || $this->name=='Ajaxis'){
			return false;
		}
		else{
			$this->getModelInfo();
            $this->bindAssociate();
			if($this->table && substr($this->name,-4)!='I18n'){
				// 当模型名称的最后4位为I18n时，多语言一对一的记录模块。无需加载模块信息。 加载造成I8循环调用，fatal error。
				$this->getExtSchema();
			}

			if(Configure::read($this->alias.'.open_redis')){
				$this->Behaviors->attach('Redis');
			}
			// 根据字段设置，加载字段值的验证规则。
			foreach($this->_extschema as $k => $v){
				if(!$v['allownull'] || 	$v['validationregular']=='notempty'){
					$this->validate[$k]['notempty']=array('rule' => 'notEmpty','message' => __('This field cannot be left blank.'),);
				}
				// 不能为空的条件可以与其它判断条件组合
				if($v['validationregular']=='email'){
					$this->validate[$k]['email']=array(
							'rule' => 'email',
							'message' => __('Please enter a valid email address.'),
					);
				}
				elseif($v['validationregular']=='unique'){
					$this->validate[$k]['unique'] = array(
							'rule' => 'isUnique',
							'message' => __('This value has already been taken.'),
					);
				}
				elseif($v['validationregular']=='url'){
					$this->validate[$k]['url']=array(
							'rule' => 'url',
							'message' => __('Please enter a valid url.'),
					);
				}
				elseif($v['validationregular']=='numeric'){
					$this->validate[$k]['numeric']=array(
							'rule' => 'numeric',
							'message' => __('Please enter a valid numeric.'),
					);
				}
				elseif($v['validationregular']=='alphaNumeric'){
					$this->validate[$k]['alphaNumeric']=array(
							'rule' => 'alphaNumeric',
							'message' => __('Please enter a valid alphaNumeric string.'),
					);
				}
				elseif($v['validationregular']=='decimal'){
					$this->validate[$k]['decimal']=array(
							'rule' => 'decimal',
							'message' => __('Please enter a valid decimal.'),
					);
				}
			}
			
// 			$this->bindModel(array());// 其中调用 $this->__createLinks();//关联模块
			
			if(!empty($this->_modelinfo['modeltype'])){
				if($this->_modelinfo['modeltype'] == 'tree'){
					if(!isset($this->actsAs['Tree'])){
						$this->actsAs['Tree'] = array('left'=>'left','right'=>'right');
						$this->Behaviors->load('Tree',array('left'=>'left','right'=>'right'));
					}
				}
			}
			
			/**
			 * 在模型表信息中，配置使用的behavior，动态启用行为
			 */
			if(!empty($GLOBALS['model_behaviors']['*'])){
				foreach($GLOBALS['model_behaviors']['*'] as $behavior => $config){
					$this->Behaviors->load($behavior, $config);
				}
			}
			if(!empty($GLOBALS['model_behaviors'][$this->name])){
				foreach($GLOBALS['model_behaviors'][$this->name] as $behavior => $config){
					$this->Behaviors->load($behavior, $config);
				}
			}
			if($this->_modelinfo['localetype']=='1' && $this->table && substr($this->name,-4)!='I18n') { //开启多语言1对1
				//$this->LoadModel
				// 已category模块为例，categories表与category_i18ns表的共有文本字段为多语言可设置的字段
				if($i18n_model = loadModelObject($this->name.'I18n')) {
					$i18n_fields = $i18n_model->schema();
					$fields = array();
					foreach($i18n_fields as $field_name => $field){
						if(in_array($field['type'],array('string','text','content','char','varchar','mediumtext')) && $field_name != 'locale'){
							$fields[] = $field_name;
						}
					}
					$ext_fields = array_keys($this->_extschema);
					$config_fields = array_intersect ($fields,$ext_fields);
					$this->Behaviors->load('MultiTranslate', $config_fields);
		  			$GLOBALS['model_behaviors'][$this->name]['MultiTranslate'] = $config_fields;
				}
		  	}
		}
	}
	
	private function bindAssociate(){
		
		if( substr($this->name,-4) == 'I18n'  || in_array($this->name,array('Tag','Setting','Keyword','TagRelated','Permission','Uploadfile','Staff','I18nfield','Modelextend')) ) {
			return;
		}

		// 绑定相关标签
		if( $this->_modelinfo['bind_uploads'] && !isset($this->hasMany['Uploadfile']) ){
			$this->bindModel(array(					
			'hasMany'=>array(
				'Uploadfile'=> array(
					'className'     => 'Uploadfile',
					'foreignKey'    => 'data_id',
					'conditions'    => array('Uploadfile.modelclass'=>$this->name,'Uploadfile.trash' => '0'),
					'order'    => 'Uploadfile.sortorder asc,Uploadfile.created ASC',
					//'limit'        => 15,
					//'dependent'=> true
			))),false);	
			// 不能带limit参数，带limit参数后，拉取list时，每一条单独的数据都会执行一条sql查询uploadfile表。
			// 无limit参数时，所有数据汇总id综合到一起查询一次，仅一条sql
		}
		// 绑定相关标签
		if( $this->_modelinfo['bind_tags'] &&  !isset($this->hasAndBelongsToMany['Tag']) ){
			$this->bindModel(array(
			'hasAndBelongsToMany' => array(
					'Tag' => array(
							'className'              => 'Tag',
							'joinTable'              => 'tag_relateds',
							'foreignKey'             => 'relatedid', // 对应本模块的id
							'associationForeignKey'  => 'tag_id', // 对应tag的id
							'conditions'             => array('TagRelated.relatedmodel' => $this->name),
							'unique'                 => true,//'keepExisting'
							// 'limit'        => 100,
							'dependent'            => true,
							'exclusive'            => true,
					)
			)),false);
		}
	}
	
	public function isContentModel(){
		$this->getModelInfo();
		if($this->_modelinfo['cate_id']==1){
			return true;
		}
		return false;
	}
	
	/**
	 * 加载模块结构
	 */
	public function getModelInfo()
	{
		$cachekey = 'extend_info_'.$this->name;
		$this->_modelinfo = Cache::read($cachekey,'_cake_model_'); 
		if ($this->_modelinfo === false) {
			$this->_modelinfo=array();
			if($this->name=='Modelextend'){
				$extobj = $this;
			}
			else{
				$extobj =  loadModelObject('Modelextend');
			}
			$extobj->recursive = -1;
			$tempextschema = $extobj->find('first',array('conditions'=>array(
		  			'name'=> $this->name,  // Inflector::classify()
		  			)));
		  	$this->_modelinfo = $tempextschema['Modelextend'];	
		  		  	
		  	
		  	Cache::write($cachekey,$this->_modelinfo,'_cake_model_'); 
		}
		
		if(!empty($this->_modelinfo['related_model'])){
	  		$ralated_models = json_decode($this->_modelinfo['related_model'],true);
	  		if(is_array($ralated_models)){
		  		foreach($ralated_models as $hastype => $assoc){
		  			foreach($assoc as $r_modelname => $variables){
		  				if(ModelExists($r_modelname)){
			  				$this->{$hastype}[$r_modelname] = array_merge( array(
		  						'className'     => $r_modelname,
		  						//'foreignKey'    => $variables['foreignKey'],
		  						'order'    => $r_modelname.'.id ASC',
		  						'limit'        => 100,
		  						'dependent'=> true,
		  				       'unique'=> true,
			  				    'exclusive'=> false,
			  				),$variables);
		  				}
		  			}
// 		  			print_r($this->{$hastype});
		  		}
	  		}
	  	}	  	
	  	// 		print_r($this->_modelinfo);
		return $this->_modelinfo;
	}
	
	/**
	 *  加载数据表结构
	 */
	public function getExtSchema(){
		//echo $local = Configure::read('Site.language');
		if(!$this->table){
			return array();
		}
		if(empty($this->_extschema) && $this->name != 'Permission'){
			$cachekey = 'extschema_'.$this->name;
			$this->_extschema = Cache::read($cachekey,'_cake_model_'); 
			if ($this->_extschema === false) {
				$this->_extschema=array();
				if($this->name=='I18nfield'){
					$I18nObj = $this;
				}
				else{
					$I18nObj =  loadModelObject('I18nfield');
				}
				$I18nObj->recursive = -1;
		    	$tempextschema = $I18nObj->find('all',array(
					'conditions'=>array('model'=>$this->name,'deleted'=>0), //'locale'=> $locale,'savetodb'=>1
			  		'order' => array('sort desc,id asc'),
			  		)
			  	);
		    	
			  	foreach($tempextschema as $value){
			  		$this->_extschema[$value['I18nfield']['name']] = $value['I18nfield'];
			  	}
			  	Cache::write($cachekey,$this->_extschema,'_cake_model_'); 
			}
		}
		return $this->_extschema;
	}
	
	/*function updateAll($fields,$conditions){
		$this->beforeSave();
		$result = parent::updateAll($fields,$conditions);
		$this->afterSave();
		return $result;
	}*/
	
	// update的 breforeSave,afterSave是id可能不是确认的一条数据，不能执行beforeSave,afterSave.
	// 除非根据条件执行查询选出所有的数据，然后分别执行单条的save操作仅保存修改的字段，这样的方式来调用beforeSave和afterSave。 否则不适合调用
	
	/**
	 * 
	 * @param  $fields 数组，索引为字段名，指为要设置的值
	 * @param  $conditions  //数组，同查询的$conditions，update的条件.没有设置条件时，禁止update
	 */
	function updateAll($fields,$conditions = true)
	{
		//$this->beforeSave();
		$db =& ConnectionManager::getDataSource($this->useDbConfig);	
		if(!empty($conditions)){
			$success = (bool)$db->update($this, $fields, null,$conditions);
		}
		else{
			$success = false;
		}
		//$this->afterSave();
		return $success;
	}
	
	/**
	 * 执行数据库的escape_string方法，主要在update, updateAll时执行
	 * @param unknown $value
	 */
	public function escape_string($value){
		return $this->getDataSource()->value($value);
	}
	
	/**
	 * 后台有插入和取数据，可能有超时现象，不启用从库 
	 * (non-PHPdoc)
	 * @see lib/Cake/Model/Model#beforeSave($options)
	 */
//	function beforeSave()
//	{
//	     $this->setDataSource('master');
//	     return true;
//	}
//	 

	public function setVersionFlag($flag = false) {
	    $this->version_flag = $flag ;
	}
	
	function afterSave($created=false)
	{
	    // $this->setDataSource('default'); 
	    if( !in_array($this->name,array('VersionControl','Shortmessage','Recycle','OperateLog','TagRelated','Stylevar') ) && $this->version_flag && strpos($this->name,'Crawl') ===false ) {
	    	if($this->id){
				$vesion_c = loadModelObject('VersionControl');
				$vesion_c->primaryKey = 'version_id';
				$vesion_c->create();
				$vd = array(
					'data_id' => $this->id,
					'model' => $this->name,
					'content' => json_encode($this->data),
					'user_id' => $this->data[$this->name]['user_id'] ? $this->data[$this->name]['user_id'] : $this->data[$this->name]['creator'],
					'url' => Router::url(),
				);
		    	$vesion_c->save($vd);
		    }
	    }
		return true;
	}
	 
//	function beforeDelete()
//	{
//	     $this->setDataSource('master'); 
//		return true;
//	}
//	 
//	function afterDelete()
//	{
//	     $this->setDataSource('default'); 
//		return true;
//	}

	/**
	 *  query方法支持主从
	 */
//	function query() {
//		$params = func_get_args();
//		$this->beforeSave();
//		$db = $this->getDataSource();		
//		$result = call_user_func_array(array(&$db, 'query'), $params);
//		//$result = parent::query($params);
//		$this->afterSave();
//		return $result;
//	}	
//	function deleteAll($conditions,$cascade = true, $callbacks = false){
//		$this->beforeSave();
//        $output = parent::deleteAll($conditions,$cascade,$callbacks);
//        $this->afterSave();
//        return $output;
//	}

//	function saveAll($data = null, $options = array())
//	{
//		$this->beforeSave();
//		$return = parent::saveAll($data, $options);
//		$this->afterSave();
//		return $return;
//	}
//	
	/**
	 * 用于检测多个字段合并的unique索引。在i18nfield,template模块(Model)中有使用
	 * @param $data
	 * @param $fields
	 */
	function isUniqueMulti($data, $fields) {
	    if (!is_array($fields)) {
			$fields = array($fields);
	    }
	    $tmp = array();  
	    foreach ($fields as $key) {
			$tmp[$key] = $this->data[$this->name][$key];
	    }
	    return $this->isUnique($tmp, FALSE);
	}

}
?>