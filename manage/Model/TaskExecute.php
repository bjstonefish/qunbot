<?php

class TaskExecute extends AppModel {

    var $name = 'TaskExecute';
    
    var $actsAs = array('EventLog');
    
    var $hasMany = array(
    		'Comment'=> array(
    				'className'     => 'Comment',
    				'foreignKey'    => 'data_id',
    				'conditions'    => array('Comment.type' => 'TaskExecute'),
    				'limit'        => 5,
    				'order'    => 'Comment.created DESC',
    		),
    );
    
    public $status = array(
    	'added' => 0,
    	'executing' => 1, //已开始
    	'set_finished' => 2,
    	'confirm_finished' => 3,
    );
    
    public function set_start($id,$task_id){
    	
    	if($this->save(array('id'=>$id,'status'=>$this->status['executing']),true,array('status'))) {
    		return true;
    	}
    	return false;
    }
    
    
    public function set_finished($id){
    	if($this->save(array('id'=>$id,'status'=>$this->status['set_finished']),true,array('status'))) {
    		return true;
    	}
    	return false;
    }
    
    public function confirm_finished($id){
    	if($this->save(array('id'=>$id,'status'=>$this->status['confirm_finished']),true,array('status'))) {
    		return true;
    	}
    	return false;
    }
    
}
?>