<?php

class Role extends AppModel {

    var $name = 'Role';

    var $actsAs = array(
        'Acl' => array(
            'type' => 'requester',
        ),
    );

    var $validate = array(
        'name' => array(
            'rule' => array('minLength', 1),
            'message' => 'Name cannot be empty.',
        ),
        'alias' => array(
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'This alias has already been taken.',
            ),
            'minLength' => array(
                'rule' => array('minLength', 1),
                'message' => 'Alias cannot be empty.',
            ),
        ),
    );

    function parentNode() {
        return null;
    }

}
?>