<?php

App::uses('RedisUtility', 'Utility');

class RedisBehavior extends ModelBehavior {


  private $redis = null;

  public function setup( Model $Model, $settings = array() ) {
  }

  public function saveWxArticleRedisList($Model,$id) {  //文章启用微信公共号的标签
      
      $modelClass = $Model->alias;

      if(!$id || $modelClass!='Article')  return false;

      $datalists = $Model->find('all',array(
          'conditions'=>array(
            $modelClass.'.id' => $id,
          ),
          'joins' => array(array(
              'table'=>'tag_relateds',
              'alias'=>'TagRelated',
              'type'=>'left',
              'conditions'=> array(
                'TagRelated.relatedmodel'=> 'Wx',
                'TagRelated.relatedid='.$modelClass.'.wx_id',
              ),
          )),
          'fields' => array($modelClass.'.id',$modelClass.'.created',$modelClass.'.wx_id',$modelClass.'.view_nums','TagRelated.tag_id'),
          'recursive' => -1,
      ));
// print_r($datalists);
      foreach($datalists as $item){        
        //最新列表
        RedisUtility::zAdd($modelClass.'New',strtotime($item[$modelClass]['created']),$Model->id);
        // 总热门列表
        RedisUtility::zAdd($modelClass.'Hot',$item[$modelClass]['view_nums'],$item[$modelClass]['id']);

        // 公众号最新列表
        RedisUtility::zAdd('Wx'.$modelClass.'New_'.$item[$modelClass]['wx_id'],strtotime($item[$modelClass]['created']),$Model->id);
        // 公众号总热门列表
        RedisUtility::zAdd('Wx'.$modelClass.'Hot_'.$item[$modelClass]['wx_id'],$item[$modelClass]['view_nums'],$item[$modelClass]['id']);


        if($item['TagRelated']['tag_id']){
            //标签最新排行
            RedisUtility::zAdd('Tag'.$modelClass.'New_'.$item['TagRelated']['tag_id'],strtotime($item[$modelClass]['created']),$item[$modelClass]['id']);
            //标签热门排行
            if($item[$modelClass]['view_nums'] > 0){
              RedisUtility::zAdd('Tag'.$modelClass.'Hot_'.$item['TagRelated']['tag_id'],$item[$modelClass]['view_nums'],$item[$modelClass]['id']);
            }
        }    
      }
  }

  public function saveRedisList($Model,$id){
    if(!$id)  return false;
      $modelClass = $Model->alias;
      $datalists = $Model->find('all',array(
          'conditions'=>array(
            $modelClass.'.id' => $id,
          ),
          'joins' => array(array(
              'table'=>'tag_relateds',
              'alias'=>'TagRelated',
              'type'=>'left',
              'conditions'=> array(
                'TagRelated.relatedmodel'=>$modelClass,
                'TagRelated.relatedid='.$modelClass.'.id',
              ),
          )),
          'fields' => array($modelClass.'.id',$modelClass.'.created',$modelClass.'.view_nums','TagRelated.tag_id'),
          'recursive' => -1,
      ));
      foreach($datalists as $item){        
        //分类列表
        RedisUtility::zAdd($modelClass.'Cate:'.$item[$modelClass]['cate_id'],strtotime($item[$modelClass]['created']),$item[$modelClass]['id']);
        //最新列表
        RedisUtility::zAdd($modelClass.'New',strtotime($item[$modelClass]['created'],$Model->id));
        // 总热门列表
        RedisUtility::zAdd($modelClass.'Hot',$item[$modelClass]['view_nums'],$item[$modelClass]['id']);
        if($item['TagRelated']['tag_id']){
          //标签最新排行
          RedisUtility::zAdd('Tag'.$modelClass.'New_'.$item['TagRelated']['tag_id'],strtotime($item[$modelClass]['created']),$item[$modelClass]['id']);
          //标签热门排行
          if($item[$modelClass]['view_nums'] > 0){
            RedisUtility::zAdd('Tag'.$modelClass.'Hot_'.$item['TagRelated']['tag_id'],$item[$modelClass]['view_nums'],$item[$modelClass]['id']);
          }
        }        
      }
  }

  public function removeRedisList($Model,$id){
    $modelClass = $Model->alias;
      $datalists = $Model->find('all',array(
          'conditions'=>array(
            $modelClass.'.id' => $id,
          ),
          'joins' => array(array(
              'table'=>'tag_relateds',
              'alias'=>'TagRelated',
              'type'=>'left',
              'conditions'=> array(
                'TagRelated.relatedmodel'=>$modelClass,
                'TagRelated.relatedid='.$modelClass.'.id',
              ),
          )),
          'fields' => array($modelClass.'.id',$modelClass.'.created',$modelClass.'.view_nums','TagRelated.tag_id'),
          'recursive' => -1,
      ));
      foreach($datalists as $item){

       RedisUtility::zRem($modelClass.'Hot',$item[$modelClass]['id']);
       RedisUtility::zRem($modelClass.'New',$item[$modelClass]['id']);
       if($item['TagRelated']['tag_id']){
           //标签最新排行
           RedisUtility::zRem('Tag'.$modelClass.'New_'.$item['TagRelated']['tag_id'],$item[$modelClass]['id']);
            //标签热门排行
           RedisUtility::zRem('Tag'.$modelClass.'Hot_'.$item['TagRelated']['tag_id'],$item[$modelClass]['id']);
       }
       
      }
  }

  public function afterDelete( Model $Model ) {
    $fields = array_keys($Model->schema());
    if($Model->alias == 'Article' && in_array('wx_id',$fields)){
        $this->removeWxTagRedisList($Model,$Model->id);
    }
    else{
      $this->removeRedisList($Model,$Model->id);
    }
  }

  public function afterSave( Model $Model, $created ) {
    if($created){
      $fields = array_keys($Model->schema());
      if($Model->alias == 'Article' && in_array('wx_id',$fields)){
        $this->saveWxArticleRedisList($Model,$Model->id);
      }
      else{
        $this->saveRedisList($Model,$Model->id);
      }
    }
    return true;    
  }
  
//   public function beforeDelete( Model $Model, $cascade = true ) {

//   }



  public function removeWxTagRedisList($Model,$id){
    $modelClass = $Model->alias;
      $datalists = $Model->find('all',array(
          'conditions'=>array(
            $modelClass.'.id' => $id,
          ),
          'joins' => array(array(
              'table'=>'tag_relateds',
              'alias'=>'TagRelated',
              'type'=>'left',
              'conditions'=> array(
                'TagRelated.relatedmodel'=> 'Wx',
                'TagRelated.relatedid='.$modelClass.'.wx_id',
              ),
          )),
          'fields' => array($modelClass.'.id',$modelClass.'.created',$modelClass.'.view_nums','TagRelated.tag_id'),
          'recursive' => -1,
      ));
      foreach($datalists as $item){

       RedisUtility::zRem($modelClass.'Hot',$item[$modelClass]['id']);
       RedisUtility::zRem($modelClass.'New',$item[$modelClass]['id']);
       if($item['TagRelated']['tag_id']){
              //标签最新排行
             RedisUtility::zRem('Tag'.$modelClass.'New_'.$item['TagRelated']['tag_id'],$item[$modelClass]['id']);
              //标签热门排行
             RedisUtility::zRem('Tag'.$modelClass.'Hot_'.$item['TagRelated']['tag_id'],$item[$modelClass]['id']);
        }
      }
  }


}
