<?php
class BotGroup extends AppModel {
    
    var $name = 'BotGroup';
    
    var $belongsTo=array(
        'Bot' => array(
            'className' => 'Bot',
            'foreignKey' => 'bot_id',
        ),
    );
    
       /*
       
       var $hasOne = array(
          'Bot'=>array(
            'className'     => 'Bot',
            'foreignKey'    => 'bot_id',
            'dependent'=> true
          ),
        );
         */
} 