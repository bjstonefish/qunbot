<?php

class Partner extends AppModel {

    var $name = 'Partner';
    
    var $actsAs = array('EventLog');
    
    var $status = array(
    	'added' => 0,
    	'audit' => 1,
    	'audit_failed' => 2,
    	'audit_success' => 3,
    );
    
    var $hasAndBelongsToMany = array(
			'Tag' => array(
					'className'              => 'Tag',
					'joinTable'              => 'tag_relateds',
					'foreignKey'             => 'relatedid', // 对应本模块的id
					'associationForeignKey'  => 'tag_id', // 对应tag的id
					'conditions'             => array('TagRelated.relatedmodel' => 'Partner'),
					'unique'                 => true,//'keepExisting'
					'limit'        => 20,
					'dependent'            => true,
					'exclusive'            => true,
			)
	);
    
    var $hasMany = array(
    		'Uploadfile'=> array(
    				'className'     => 'Uploadfile',
    				'foreignKey'    => 'data_id',
    				'conditions'    => array('Uploadfile.modelclass' => 'Partner','Uploadfile.trash' => '0'),
    				'order'    => 'Uploadfile.sortorder asc,Uploadfile.created ASC',
    		),
    );
}
?>