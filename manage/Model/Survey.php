<?php
class Survey extends AppModel { 
    var $name = 'Survey';
    var $validate = array(
    	'name' => array(
            'rule' => 'notEmpty',
            'message' => 'This field cannot be left blank.',
        ),
    );
    
    var $hasMany = array(
        'Surveyoption' => array(
            'className'     => 'Surveyoption',
            'foreignKey'    => 'qid',
            'dependent'=> true
        )
    );
    
} 