<?php

class Modelextend extends AppModel {

    var $name = 'Modelextend';
    var $validate = array(
        'name' => array(
    		'unique'=>array(
	            'rule' => 'isUnique',
	            'message' => 'The name has already been taken.',
    		),    		
        ),        
    );
    /**
     * 
     * @param string $type
     * @param number $cate_id 1为所有前台内容类型的模块分类
     * @return Ambigous <multitype:, NULL, mixed>
     */
    public function getContentModel($type='list',$cate_id=1){
    	return $this->find($type,array('conditions'=>array('cate_id'=>$cate_id)));
    }
}
?>