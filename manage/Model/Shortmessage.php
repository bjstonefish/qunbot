<?php
class Shortmessage extends AppModel { 
       var $name = 'Shortmessage';


    
       public function send($title,$text_msg,$sender,$receiver){

           if( $receiver && $text_msg ) {
               $this->create();
               $data['Shortmessage'] = array();

               $data['Shortmessage']['title'] = $title;
               $data['Shortmessage']['message'] = $text_msg;
               $data['Shortmessage']['receiverid'] = $receiver;
               $data['Shortmessage']['msgfromid'] = $sender;
               if ($this->save($data)) {
                   CakeLog::info("send user message:".var_export($data,true));
                   return true;
               }
           }
           return false;

       }
}