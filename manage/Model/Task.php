<?php

class Task extends AppModel {

    var $name = 'Task';
    
    var $actsAs = array('EventLog');
    
    var $hasAndBelongsToMany = array(
    		'Tag' => array(
    				'className'              => 'Tag',
    				'joinTable'              => 'tag_relateds',
    				'foreignKey'             => 'relatedid', // 对应本模块的id
    				'associationForeignKey'  => 'tag_id', // 对应tag的id
    				'conditions'             => array('TagRelated.relatedmodel' => 'Task'),
    				'unique'                 => true,//'keepExisting'
    				'limit'        => 20,
    				'dependent'            => true,
    				'exclusive'            => true,
    		)
    );
    
    public $status = array(
    		'added' => 0,
    		'bid_finished' => 1,
    		'prepayed' => 2,
    		'started' => 3,
    		'change_user' => 4,
    		'finished' => 5, //任务完成
    		'task_finished' => 6,//任务确认完成
    		'failed' => 7,
    		'customer_payed' => 8,
    		'partner_payed' => 9,
    );
    
}
?>