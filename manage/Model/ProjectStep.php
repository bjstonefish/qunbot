<?php
class ProjectStep extends AppModel { 
    var $name = 'ProjectStep';
    var $actsAs = array('EventLog'=>array('ignore_event'=>'CREATE'));
    /* 项目步骤，每个步骤可以单独上传附件 */
    var $hasMany = array(
    		'Uploadfile'=> array(
    				'className'     => 'Uploadfile',
    				'foreignKey'    => 'data_id',
    				'conditions'    => array('Uploadfile.modelclass' => 'ProjectStep','Uploadfile.trash' => '0'),
    				'order'    => 'Uploadfile.sortorder asc,Uploadfile.created ASC',
    		),
    );
    
} 