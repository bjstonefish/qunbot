<?php
class Project extends AppModel { 
    var $name = 'Project';
    
    var $actsAs = array('EventLog');
    
    var $validate = array(
    	'name' => array(
            'rule' => 'notEmpty',
            'message' => 'This field cannot be left blank.',
        ),
    );
    
    public $status = array(
    	'added' => 0,
    	'runing' => 1,
    	'complete'=>2,
    	'cancel' => 3,
    );

    var $hasAndBelongsToMany = array(
            'Contact' => array(
                    'className'              => 'Contact',
                    'joinTable'              => 'project_contacts',
                    'foreignKey'             => 'project_id', // 对应本模块的id
                    'associationForeignKey'  => 'contact_id', // 对应tag的id
                    'conditions'    => array('ProjectContact.status' => '1'),
                    'unique'                 => true,//'keepExisting'
                    'dependent'            => true,
                    'exclusive'            => true,
            ),
    );
    
    var $belongsTo = array(
    		'Task'=>array(
    				'className'     => 'Task',
    				'foreignKey'    => 'task_id',
    				'dependent'=> true
    		),
    		'Customer'=>array(
    				'className'     => 'Customer',
    				'foreignKey'    => 'customer_id',
    				'dependent'=> true
    		),
    );
    /* 项目有N个步骤，每个步骤可以单独进行评论,不用对项目单独进行评论 */
    var $hasMany = array(
    		/*'Comment'=> array(
    				'className'     => 'Comment',
    				'foreignKey'    => 'data_id',
    				'conditions'    => array('Comment.type' => 'Project'),
    				'limit'        => 5,
    				'order'    => 'Comment.created DESC',
    		),*/
    		'ProjectStep' => array(
    				'className'     => 'ProjectStep',
    				'foreignKey'    => 'project_id',
    				'limit'        => 20,
    				'order'    => 'ProjectStep.priority aSC',
    		),
    		'Uploadfile'=> array(
    				'className'     => 'Uploadfile',
    				'foreignKey'    => 'data_id',
    				'conditions'    => array('Uploadfile.modelclass' => 'Project','Uploadfile.trash' => '0'),
    				'order'    => 'Uploadfile.sortorder asc,Uploadfile.created ASC',
    		),
    );
    
} 