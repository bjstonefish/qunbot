<?php

class Tasking extends AppModel {

    var $name = 'Tasking';
    
    //public $actsAs = array('StatusChange');

    /**
     *  * 0 参与任务，制定了计划
     * 1 指定了计划，未投标但投标已结束，计划作废
     *
     *
     *
     * 大于9以上的状态对客户与咨询师都可见。 小于等于9的状态仅对咨询师可见
     * 10 参与投标，等待投标审批
     * 11 参与投标不成功
     * 12 任务失败
     	
     * 13 投标通过，成功参与任务
     * 14 任务开始
     * 15 任务完成
     * @var array
     */
    
    public $status = array(
    		/** 小于等于9的状态,任务计划仅对咨询师可见 */
    		'join' => 0,
    		'finished_not_bid' => 1,
    
    		/** 大于9以上的状态,任务计划对客户与咨询师都可见。  */
    		'bid' => 10,
    		'bid_failed' => 11,
    		'task_failed' => 12,
    		'bid_success' => 13,
    		'task_started' => 14,
    		'task_finished' => 15,
    		'confirm_finished'=>16,
    );
    
    
    public $messages = array(
    		'bid_failed' => array(
    				'title'=> 'Bid "{name}" failed',
    				'message'=>'bid <a href="/tasks/view/{id}" target="_blank">"{name}"</a> failed.'
    		),
    		'bid_success' => array(
    				'title'=> 'Bid "{name}" success',
    				'message'=>'bid <a href="/tasks/view/{id}" target="_blank">"{name}"</a> success.'
    		),
    		'confirm_finished' => array(
    				'title'=> 'Task "{name}" confirm finished.',
    				'message'=>'Task <a href="/tasks/view/{id}" target="_blank">"{name}"</a> confirm finished.'
    		),
    		
    );
	
    
    private function send_msg($receiver_id,$data,$message_data) {
    	$title = $message_data['title'];
    	$message = $message_data['message'];
    	
    	print_r($message_data);
    	
    	if($receiver_id){
    		foreach($data as $key => $value){
    			if(strpos($title,'{'.$key.'}')!==false){
    				$title = str_replace('{'.$key.'}',$value,$title);
    			}
    			if(strpos($message,'{'.$key.'}')!==false){
    				$message = str_replace('{'.$key.'}',$value,$message);
    			}
    		}
    		
    		$data = array(
    				'msgfromid' => 0,
    				'receiverid' => $receiver_id,
    				'title' => $title,
    				'message' => $message,
    		);
    		
    		$ShortMessage = ClassRegistry::init( 'Shortmessage' );
    		$ShortMessage->create();
    		$ShortMessage->save($data);
    	}
    }
    
    public function confirm_finished($task_id,$tasking_id){
    	/** 设置任务实施记录的状态 **/
    	if($this->save(array(
    		'id'=> $tasking_id,
    		'status'=> $this->status['confirm_finished'] ),true,array('status'))) {
    		
    		
	    				 
    		/** 设置任务的状态 **/
    		$task = loadModelObject('Task');    		
    		$task->save(array('id'=>$task_id,'status'=> $task->status['task_finished']),true,array('status'));
    		
    		/** 查询数据发送短消息 **/
    		$tasking = $this->find('first',array(
    				'conditions'=>array(
    						'task_id' => $task_id,
    						'id' => $tasking_id,
    				),
    				'recursive' => -1,
    		));
    		$receiver_id = $tasking['Tasking']['user_id'];
    		$taskinfo = $this->findById($task_id);
    		$this->send_msg($receiver_id,$taskinfo['Task'], $this->messages['confirm_finished']);
    		
    		return true;
    	}
    	return false;
    }
    
    /**
     * 客户选择投标
     * @param unknown $task_id
     * @param unknown $plan_id
     */
    public function accect_bid($task_id,$tasking_id){
    	
    	$tasking = $this->find('first',array(
    			'conditions'=>array(
    					'task_id' => $task_id,
    					'id' => $tasking_id,
    					'status' => $this->status['bid'],
    			),
    			'recursive' => -1,
    	));
    	$task = loadModelObject('Task');
    	$taskinfo = $task->find('first',array(
    			'conditions'=>array(
    					'id' => $task_id,
    			),
    			'recursive' => -1,
    	));
    	
    	if(!empty($tasking) && !empty($taskinfo)){
    		/** 设置任务投标记录的状态 **/
    		if($this->save(array(
    				'id'=> $tasking_id,
    				'status'=> $this->status['bid_success'] ),true,array('status'))) {
    			
    			/** 设置任务的状态 **/
    			$task->save(array(
    					'id' => $task_id,
    					'status' => $task->status['bid_finished'],
    					'execute_uid' => $tasking['Tasking']['user_id'],
    				),true,array('status','execute_uid'));
    			
    			/** 设置需求的状态  **/
    			// $project = loadModelObject('Project');
    			// $project->updateAll(array('status'=> $project->status['bid_finished']),array('task_id' => $task_id));
    			
    			/** 发送投标成功通知短消息 **/
    			$receiver_id = $tasking['Tasking']['user_id'];
    			$this->send_msg($receiver_id,$taskinfo['Task'], $this->messages['bid_success']);
    			
    			
    			$failed_users = $this->find('list',array(
    				'conditions'=>array('task_id' => $task_id,'status'=> $this->status['bid']),
    				'recursive' => -1,
    			));
    			if(!empty($failed_users)){
    				/** 发送其余参与用户投标失败短消息 **/
    				foreach($failed_users as $fu){
    					$receiver_id = $fu['Tasking']['user_id'];
    					$this->send_msg($receiver_id,$taskinfo['Task'], $this->messages['bid_failed']);
    				}
    				/** 设置其余参与人的任务状态  **/
    				$this->updateAll(array('status'=> $this->status['bid_failed']), array('task_id' => $task_id,'status'=> $this->status['bid'] ));
    			}
    			
    			return true;
    		}
    	}
    	return false;    	
    }
    
}
?>