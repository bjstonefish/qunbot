<?php
if(isset($_SERVER['SERVER_ADDR']) && ($_SERVER['SERVER_ADDR'] == '127.0.0.1' || $_SERVER['SERVER_ADDR'] == '::1')) {
    define('IS_LOCALHOST',true);
}

Configure::write('debug', 0);
Configure::write('Cache.disable', false);

Configure::write('Error', array(
            'handler' => 'ErrorHandler::handleError',
            'level' => E_ERROR | E_WARNING | E_PARSE,
            'trace' => true
        ));
Configure::write('Exception', array(
            'handler' => 'ErrorHandler::handleException',
            'renderer' => 'ExceptionRenderer',
            'log' => true
        ));

Configure::write('App.encoding', 'UTF-8');

// 为默认语言时，链接不会带locale参数
//define('DEFAULT_LANGUAGE', 'en-us');
// Configure::write('Site.language', 'zh-cn');

// Configure::write('App.baseUrl', env('SCRIPT_NAME'));
Configure::write('Routing.prefixes', array('admin'));


//Configure::write('Cache.check', true);

define('LOG_ERROR', 2);

Configure::write('Session', array(
   'defaults' => 'php',
   'timeout' => 900,
   'name' => 'Miao'
));

// Configure::write('Session', array(
//     'defaults' => 'php',
//     'timeout' => 150,
//     'name' => 'CAKEPHP',
//     'handler' => array(
//         'engine' => 'CustomDatabaseSession',
// 	'model' => 'Session'
//     )
// ));
Configure::write('Session.cookie', 'cake');

Configure::write('Security.level', 'medium');

Configure::write('Acl.classname', 'DbAcl');
Configure::write('Acl.database', 'default');

if (function_exists('date_default_timezone_set')) {
    date_default_timezone_set('Etc/GMT-8');
}

if (defined('SAE_MYSQL_DB')) {
    $engine = 'Saemc';
}
elseif(preg_match('/\.aliapp\.com$/',$_SERVER['HTTP_HOST'])){
    $engine = 'Acemc';
}
else {
    $engine = 'File';
    if (extension_loaded('apc') && (php_sapi_name() !== 'cli' || ini_get('apc.enable_cli'))) {
        $engine = 'Apc';
    }
}

$duration = 7200;
if (Configure::read('debug') > 1) {
    $duration = 300;
}
$cache_prefix = '';
if(defined('SAE_MYSQL_DB')){
	// 区分各版本的缓存，不互相冲突
	$cache_prefix .= $_SERVER['HTTP_APPVERSION'].'_';
}
// 缓存的配置，前台的前缀包含后台的前缀（利用后台的prefix比较时能涵盖前台的文件）。后台删除缓存时，前后台就都能删除了
Cache::config('_cake_core_', array(
            'engine' => $engine,
            'prefix' => 'core_'.$cache_prefix,
            'path' => CACHE . 'persistent' . DS,
            'serialize' => ($engine === 'File'),
            'duration' => $duration,
            'probability' => 100,
        ));

/**
 * Configure the cache for model, and datasource caches.  This cache configuration
 * is used to store schema descriptions, and table listings in connections.
 */
Cache::config('_cake_model_', array(
            'engine' => $engine,
            'prefix' => 'model_',
            'path' => CACHE . 'models' . DS,
            'serialize' => ($engine === 'File'),
            'duration' => $duration,
            'probability' => 100,
        ));

Cache::config('default', array(
        'engine' => $engine, //[required]
        'duration' => $duration, //[optional]
        'probability' => 100, //[optional]
        'prefix' => 'miaocms_'.$cache_prefix, //[optional]  prefix every cache file with this string
        'lock' => false,
        'serialize' => true, // [optional] compress data in Memcache (slower, but uses less memory)
        ));        

