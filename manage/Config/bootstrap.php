<?php
if(!defined('APP_PATH')){
	define('APP_PATH', ROOT . DS . 'manage' . DS);
}
if(!defined('APP_SUB_DIR')){
	define('APP_SUB_DIR', '');
}
if(php_sapi_name()==='cli' && empty($_GET)){
	$_SERVER['SERVER_ADDR']='127.0.0.1';
}
// 变量混淆加密，不支持 global，而使用$GLOBALS
// 变量混淆加密，不支持extract方法，要使用数组方式来使用变量

$GLOBALS['hookvars']['navmenu'] = array();
$GLOBALS['hookvars']['submenu'] = array();
/** 可控制具体开启哪一个方法 ，为空时允许所有方法，否则只允许特定的方法 **/
Configure::write('Hook.'.APP_DIR.'_helpers.MiaoHook',''); // submenu,NavMemu,NavMemu
Configure::write('Hook.'.APP_DIR.'_components.MiaoHook','');

define('COMMON_PATH', ROOT . DS . 'lib' . DS);


if (defined('SAE_MYSQL_DB')) {
	define('DATA_PATH', 'saekv://data/'); //data目录使用kvdb，其余stor的均使用upload_file_path
}
else{
	define('DATA_PATH', ROOT.DS.'data'.DS);
}

include_once COMMON_PATH.'bootstrap.php';

App::build(array(
    'libs' => array(COMMON_PATH,APP_PATH.'Lib'.DS),
));

global $page_style;
global $pages_tpl;
/*  分页样式    */
//style=1 共2991条 200页 当前第1页 [ 1 2 3 4 5 6 7 8 9 10 ... 200 ]
//style=2 共118条 | 首页 | 上一页 | 下一页 | 尾页 | 65条/页 | 共2页  <select>第1页</select>
$page_style = 1;
$pages_tpl = array(
    'total' => __('Total:%d'),
    'pages' => '', //lang('pages:%d')
    'current_page' => '', //当前第%d页
    'first' => __('First page'),
    'last' => __('Last page'),
    'pagesize' => '', //%d条/页
    'pre_page' => __('Previous page'),
    'next_page' => __('Next page'),
    'template' => '{total} {pages} {current_page} [ {list_pages} ]'
);