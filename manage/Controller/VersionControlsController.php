<?php

class VersionControlsController extends AppController{
	public $name = 'VersionControls';	
	
	public function beforeFilter(){
	    parent::beforeFilter();
	    $this->VersionControl->primaryKey = 'version_id';
	}
	
	public function admin_delete($ids= NULL){
		if( !in_array(1,$this->currentUser['role_id']) ) {
			$this->__message('仅超级管理员能删除回收站的数据。');
		}
		parent::admin_delete($ids);
	}
	
	public function admin_lists($model,$data_id) {
	    
	    $this->loadModel($model);
	    $latest_item = $this->{$model}->findById($data_id);
	    if(!in_array(1,$this->currentUser['role_id'])){
	        if(empty($latest_item)  || empty($this->currentUser['id'])  || ($latest_item[$model]['creator'] != $this->currentUser['id'] && $latest_item[$model]['user_id'] != $this->currentUser['id']) ) {
	            throw new ForbiddenException(__('You cannot view this data'));
	        }
	    }
	    
        $datalist = $this->VersionControl->find('all',array(
            'conditions' => array('model'=> $model,'data_id'=> $data_id),
            'limit' => 10,
            'fields'=> array('version_id','created','model','data_id','content','user_id','url'),
            'order' => 'version_id desc',
        ));
        
        $this->Session->delete('Message.flash'); 
        
        $this->set('modelClass',$model);
        $this->set('datalist',$datalist);
        $this->set('latest_item',$latest_item);
	}
	
	public function admin_restore($model='',$data_id='',$version_id='') {
	    $this->loadModel($model);
	    $latest_item = $this->{$model}->findById($data_id);
	    if(!in_array(1,$this->currentUser['role_id'])){
	        if(empty($latest_item)  || empty($this->currentUser['id'])  || ($latest_item[$model]['creator'] != $this->currentUser['id'] && $latest_item[$model]['user_id'] != $this->currentUser['id']) ) {
	            throw new ForbiddenException(__('You cannot view this data'));
	        }
	    }
	     
	    $dataitem = $this->VersionControl->find('first',array(
	        'conditions' => array('model'=> $model,'data_id'=> $data_id,'version_id' => $version_id),
	    ));
	    
	    $this->{$model}->setVersionFlag(false); //恢复版本时，不新记录历史版本
	    
	    $model_data = json_decode($dataitem['VersionControl']['content'],true);
	    
	    $model_data[$model]['view_nums'] = $latest_item[$model]['view_nums']; //恢复数据时，保留原文的阅读数
	    $model_data[$model]['click_nums'] = $latest_item[$model]['click_nums'];
	    $model_data[$model]['favor_nums'] = $latest_item[$model]['favor_nums'];
	    
	    if( $this->{$model}->save($model_data) ){
	        $this->Session->setFlash('版本恢复成功');
	    }
	    else{
	        $this->Session->setFlash('版本恢复失败，请重试');
	    }
	    $this->redirect("/admin/version_controls/view/$model/$data_id/$version_id");
	}
	
	public function admin_view($model,$data_id='',$version_id='') {
	    
	    $this->loadModel($model);
	    $latest_item = $this->{$model}->findById($data_id);
	    if(!in_array(1,$this->currentUser['role_id'])){
	        if(empty($latest_item)  || empty($this->currentUser['id'])  || ($latest_item[$model]['creator'] != $this->currentUser['id'] && $latest_item[$model]['user_id'] != $this->currentUser['id']) ) {
	            throw new ForbiddenException(__('You cannot view this data'));
	        }
	    }
	    
	    $dataitem = $this->VersionControl->find('first',array(
	        'conditions' => array('model'=> $model,'data_id'=> $data_id,'version_id' => $version_id),
	    ));
	    $this->set('modelClass',$model);
	    $this->set('version',$dataitem);
	    $this->set('latest_item',$latest_item);
	    $this->set('item',json_decode($dataitem['VersionControl']['content'],true));
	}
}
