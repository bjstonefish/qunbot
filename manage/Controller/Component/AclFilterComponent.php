<?php
/**
 * AclFilter Component
 *
 * PHP version 5
 *
 * @category Component
 * @package  MIAOCMS
 * @version  1.0
 * @author   Arlon <MIAOCMS@google.com>
 * @link     http://www.MIAOCMS.net
 */
class AclFilterComponent extends Component {
	
	public $components = array ('Session', 'Auth' );
	
	private $controller_name;
	/**
	 * @param object $controller controller
	 * @param array  $settings   settings
	 */
	function initialize(Controller $controller, $settings = array()) {
		$this->controller = $controller;
		$this->controller_name = Inflector::camelize($controller->name);
	}
	
	private function _getStaffRoles() {
		$userinfo = $this->Auth->user ();
		$roleIds = array();
		if(!empty($userinfo['Role'])){
			foreach($userinfo['Role'] as $role){
				$roleIds[] = $role['id'];
			}
		}
		else{
			$roleIds = $this->Auth->user ( 'role_id' );
			if (! is_array ( $roleIds )) {
				$roleIds = explode ( ',', $roleIds );
				$this->Session->write ( 'Auth.Staff.role_id', $roleIds );
			}
		}
		return $roleIds;
	}
	
	function check($controller_name,$action){
		$allowedActions = $this->getControllerAclActions($controller_name);
		$roleIds = $this->_getStaffRoles ();
		if(in_array($action,$allowedActions) || in_array(1,$roleIds) || in_array('*',$allowedActions)){
			return true;
		}
		else{
			return false;
		}
	}
	/** 
	 * 取得用户的aro_id;包括所在用户组(Role)的aro_id 和用户表(Staff)中记录所在的aro_id
	 */
	function getUserAroIds(){
		$user_id = $this->Auth->user ( 'id' );
		if(empty($user_id)){
			return array();
		}
		$aro_ids = Cache::read ( "admin_allow_aroids_".$user_id );
		if($aro_ids===false){
			$aro_ids = array();
			try {
				/*$this->controller->loadModel ( 'Staff' );
				$nodes = $this->controller->Staff->node ( array ('Staff' => $user_id ) );
				if(is_array($nodes)) {
					foreach ( $nodes as $aro ) {
						$aro_ids [] = $aro ['Aro'] ['id'];
					}
				}*/
				
				$this->controller->loadModel ( 'Role' );
				$this->controller->Role->id = $this->_getStaffRoles ();;
				$nodes = $this->controller->Role->node ();
				foreach ( $nodes as $aro ) {
					$aro_ids [] = $aro ['Aro'] ['id'];
				}
			}
			catch(Exception $e)
			{
				echo print_r($e);
				echo "catch exception";
			}
			Cache::write( "admin_allow_aro_ids_".$user_id, $aro_ids);
		}
		return $aro_ids;
	}
	
	function getUserAcoIds(){
		$user_id = $this->Auth->user ( 'id' );
		$aco_ids = Cache::read ( $user_id ."_admin_allow_acoids" );
		if($aco_ids===false)
		{
			$aro_ids = $this->getUserAroIds();
			$aco_ids = array();
			$allowed_aco_ids = ClassRegistry::init ( 'Acl.AclArosAco' )->find ( 'all',
					array (
							'recursive' => '-1',
							'fields' => 'aco_id',
							'conditions' => array(
									'AclArosAco.aro_id' => $aro_ids, 'AclArosAco._read' => 1
							) ) );
			foreach($allowed_aco_ids as $aco){
				$aco_ids[] = $aco['AclArosAco']['aco_id'];
			}
			Cache::write($user_id."_admin_allow_aco_ids",$aco_ids);
		}
		return $aco_ids;
	}
	
	function getControllerAclActions($controller_name){
		$aro_ids = $this->getUserAroIds();
		if(in_array(1,$aro_ids)){
			return array('*');
		}
		$controller_name = Inflector::camelize($controller_name);
		
		$user_id = $this->Auth->user ( 'id' );
		$acoPath = 'controllers/'.$controller_name;
		// 当前控制器在aco中的节点，如：controllers/systems
		{
			$thisControllerNode = $this->controller->Acl->Aco->node ( $acoPath );
			if ($thisControllerNode && $aro_ids) {
				$thisControllerNode = $thisControllerNode ['0'];
				// 获取aco表中，当前控制器中所有的action操作
				$thisControllerActions = $this->controller->Acl->Aco->find ( 'list', array ('conditions' => array ('Aco.parent_id' => $thisControllerNode ['Aco'] ['id'] ), 'fields' => array ('Aco.id', 'Aco.alias' ) ) );
			
				$thisControllerActionsIds = array_keys ( $thisControllerActions );
				$thisControllerActionsIds[] =  $thisControllerNode ['Aco'] ['id']; 
				
				// 'AclArosAco._create' => 1, 'AclArosAco._read' => 1, 'AclArosAco._update' => 1, 'AclArosAco._delete' => 1
				$conditions = array ('AclArosAco.aro_id' => $aro_ids, 'AclArosAco.aco_id' => $thisControllerActionsIds,  );
				$allowedActions = ClassRegistry::init ( 'Acl.AclArosAco' )->find ( 'all', array ('recursive' => '-1', 'conditions' => $conditions ) );
				$allowedActionsIds = Set::extract ( '/AclArosAco/aco_id', $allowedActions );
				
			}
			$allow = array ();
			if(!empty($allowedActions)) {
    			foreach($allowedActions as $action) {
    			    if($action['AclArosAco']['_read']) {
    			        $allow [] = 'admin_view';
    			    }
    			    elseif($action['AclArosAco']['_create']) {
    			        $allow [] = 'admin_add';
    			    }
    			    elseif($action['AclArosAco']['_list']) {
    			        $allow [] = 'admin_list';
    			    }
    			    elseif($action['AclArosAco']['_update']) {
    			        $allow [] = 'admin_edit';
    			        $allow [] = 'admin_setfiled';
    			        $allow [] = 'admin_passAudit';
    			    }
    			    elseif($action['AclArosAco']['_delete']) {
    			        $allow [] = 'admin_delete';
    			    }
    			}
    			if( $action['AclArosAco']['_read'] && $action['AclArosAco']['_update'] && $action['AclArosAco']['_delete'] && $action['AclArosAco']['_create'] && $action['AclArosAco']['_list'] ) { // 全开了时，所有其余操作也开放。
    			    $allow[] = '*';
    			}
			}
			
// 			if (isset ( $allowedActionsIds ) && is_array ( $allowedActionsIds ) && count ( $allowedActionsIds ) > 0) {
// 				foreach ( $allowedActionsIds as $i => $aId ) {
// 					if($aId == $thisControllerNode ['Aco'] ['id']) {
// 						$allow [] = '*';// 对模块授权，可操作模块所有的action
// 					}
// 					else{
// 						$allow [] = $thisControllerActions [$aId];
// 					}
// 				}
// 			}
		}
		return $allow;
	}
	
	
	/**
	 * acl and auth
	 *
	 * @return void
	 */
	function authAdmin() {
		//Configure AuthComponent
		//$this->Auth->authorize = 'actions';
		$this->Auth->loginAction = array ('plugin' => 0, 'controller' => 'staffs', 'action' => 'login' );
		$this->Auth->logoutRedirect = array ('plugin' => 0, 'controller' => 'staffs', 'action' => 'login' );
		$this->Auth->loginRedirect = array ('plugin' => 0, 'controller' => 'staffs', 'action' => 'index' );
		$this->Auth->userScope = array ('Staff.status' => 1 );
		
		if ($this->Auth->user()) {
			$roleId = $this->_getStaffRoles ();
			$user_id = $this->Auth->user ( 'id' );
		}
		else{
			return array('admin_login');
		}
		
        if ($user_id && in_array(1,$roleId)) { // Role: Admin
            $this->Auth->allowedActions = array('*');
            return true;
        }
        else{
			// 若存在缓存，则直接调用缓存
			$cachekey = "admin_allow_act_".$user_id . $this->controller_name;
			$allow_actions = Cache::read ( $cachekey );
			
           if( $allow_actions!==false ) {
                $this->Auth->allowedActions = $allow_actions;
           }
           else {
           		$allow = $this->getControllerAclActions($this->controller_name);
				$allow [] = 'admin_login';
				$allow [] = 'admin_logout';
				$allow [] = 'admin_select';
				$allow [] = 'admin_index';
				$allow [] = 'admin_menu';
				$allow [] = 'admin_getcss';
				$allow [] = 'admin_editpassword';
				
				$this->Auth->allowedActions = $allow;
				Cache::write ( $cachekey , $allow );
			}
		}
	}

}
?>