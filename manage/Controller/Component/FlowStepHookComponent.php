<?php
/**
 * wx135.com 根据用户回复的内容，抓取公众号的推送消息正文。
 * 仅可以在WeixinController中调用
 * @author arlon
 *
 */
class FlowStepHookComponent extends Component {

	public $components = array('Session');

    
    function parseFlowQueryVars($controller,$str){
    	
    	$variables = array();
    	
    	foreach($controller->request->query as $key => $val){
    		$variables['{'.$key.'}'] = $val;
    	}
    	$variables['{user_id}'] = $this->Session->read('Auth.Staff.id');
    	$variables['{team_ids}'] = json_encode($controller->_getTeamIds());
    	
    	$str = str_replace(array_keys($variables), array_values($variables), $str);
    	
    	return $str;
    }
}
?>