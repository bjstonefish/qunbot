<?php

class MenusController extends AppController {

    var $name = 'Menus';
    
    var $helpers = array(
        'Hook'=>array('hooks'=> array('MiaoHook')),
    );

    function admin_index() {
        $this->pageTitle = __('Menus', true);

        $this->Menu->recursive = 0;
        $this->paginate['Menu']['order'] = "Menu.id ASC";
        $this->set('menus', $this->paginate());
    }
	/**
	 * 
	 * @param string/int $parent_id  if int,it's parentid; string slug.
	 */
    function admin_menu($parent_id=null) {
        if(empty($parent_id)){
            $parent_id = $this->Session->read('menuid');
        }    	
    	if(empty($parent_id)) $parent_id='site';
    			
        $this->pageTitle = __('Menus');
        $modelClass = $this->modelClass;
        if ($parent_id) {
        	if(preg_match('/^\d+$/',$parent_id)){
            	$parent_info = $this->Menu->find('first',array(
        				'conditions'=>array('id'=>$parent_id),
        				'recursive'=>-1,
        		));
        	}
        	else{
        		$parent_info = $this->Menu->find('first',array(
        				'conditions'=>array('slug'=>$parent_id),
        				'recursive'=>-1,
        		));
        	}
            $left = $parent_info[$modelClass]['left'];
            $right = $parent_info[$modelClass]['right'];
            $menus = $this->Menu->find('threaded',
                            array(
                                'conditions' => array(
                                    $modelClass . '.left >' => $left,
                                    $modelClass . '.right <' => $right,
                                    $modelClass . '.deleted' => 0
                                ),
                                'order' => $modelClass . '.left',
                            	'recursive'=>1,
                    ));
        }
        
        if( !in_array(1,$this->currentUser['role_id'])  ){ //&& !in_array(100,$this->currentUser['role_id'])
        	$modelextent = loadModelObject('Modelextend');
        	$content_menus = array('Menu' =>Array('name' => __('Content Management'),'link' => '#'),'children'=>array());
        	$contentmodels = $modelextent->getContentModel('all',1);
        	$leftmenu_privilege = '<ul id="leftmenu_privilege" class="leftmenu"><li class="open">
        			<a href="#submenu_pcontent" data-toggle="collapse" data-parent="#leftmenu_pcontent" class="ajax-link"><i class="fa fa-lg fa-fw fa-folder"></i> <span class="menu-item-parent">内容管理</span><b class="collapse-sign"><em class="fa fa-minus-square-o"></em></b></a>
        			<ul id="submenu_pcontent" style="display: block; visibility: visible;">';
        	foreach($contentmodels as $item){
        		$control_name = Inflector::tableize($item['Modelextend']['name']);
        		$actions = $this->AclFilter->getControllerAclActions( Inflector::camelize($control_name));
        		
        		if(!empty($actions)){ //具有模块操作权限
        			if(strpos(Configure::read('Site.language'),'zh')===0){
        				$link_text = $item['Modelextend']['cname'].__(' Management');
        			}
        			else{
        				$link_text = $item['Modelextend']['name'].__(' Management');
        			}
        			// 判断是否具备模块操作的权限
        			
        			$link = Router::url(array('controller'=>$control_name,'action'=>'list','admin'=>true));
        			
        			$leftmenu_privilege .= '<li><a href="'.$link.'" class="ajax-link">'.$link_text.'</a></li>';
        			
        		}
        	}
        	$leftmenu_privilege .= '</ul></li></ul>';
        	$this->set('leftmenu_privilege',$leftmenu_privilege);
        }
        
//         else {
//             $menus = $this->Menu->find('threaded');
//         }
        $this->set('parent_info', $parent_info);
        $this->set('parent_id', $parent_id);
        $this->set('menus', $menus);
        $this->set('_serialize', 'menu');
        if($this->request->params['return']){ // in controller::requestAction;
        	//$this->renderElement('admin_menu');
        	return $this->render('admin_menu',false);
        }
    }
    
    function admin_contentmenu($cate_id=1){
    	$this->loadModel('Modelextend');
    	$contentmodels = $this->Modelextend->getContentModel('all',$cate_id);
    	$this->set('contentmodels', $contentmodels);
    }

}

?>