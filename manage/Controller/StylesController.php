<?php

App::import('Vendor', 'lessc', array('file' => 'lessphp'.DS.'lessc.inc.php'));

class StylesController extends AppController {

    var $name = 'Styles';
    
    public function beforeFilter(){
    	parent::beforeFilter();
    	$this->loadModel('Stylevar');
    }
    
    public function admin_themeroller(){
    	
    }
    
    function admin_delete($ids = null){
    	@set_time_limit(0);
    	if(is_array($_POST['ids'])&& !empty($_POST['ids'])){
    		$ids = $_POST['ids'];
    	}
    	else{
    		if (!$ids) {
    			$this->redirect(array('action' => 'index'));
    		}
    		if(!is_array($ids)){
    			$ids = explode(',', $ids);
    		}
    	}
    	if(parent::admin_delete($ids)){
    		$this->loadModel('Stylevar');
    		$this->Stylevar->deleteAll(array('styleid' => $ids), true, true);
    	}
    }
    
    function admin_list(){
    	$styles = $this->Style->find('all');
    	$site_style = Configure::read('Site.style');
    	$mobile_style = Configure::read('Site.mobile_style');
    	$this->set(array('styles' => $styles, 'site_style' => $site_style, 'mobile_style' => $mobile_style));
    }
    
    function admin_addstyle(){
    	if(!empty($_POST)){
//     		if(empty($_FILES) || $_FILES['error']['Misccate']['variables'] ){
//     			echo '上传文件错误';
//     			exit;
//     		}
    		if($this->Style->save($this->data)){
    			$styleid = $this->Style->getLastInsertID();
    			//$this->data['Misccate']['variables']['tmp_name']
    			$variable_file = $this->data['Style']['variables']['tmp_name'];
    			if(file_exists($variable_file)){
	    			$content = file_get_contents($variable_file);
	    			$vars = explode("\n",$content);
	    			foreach($vars as $line){
	    				$line = trim(preg_replace('/\/\/.*$/','',$line)); // 去掉注释内容
	    				$line = preg_replace('/;.*$/','',$line); // 去掉“;”及其后的内容
	    				 
	    				$var = explode(':',$line);
	    				if(count($var)==2){
	    					if(substr($var[0],0,1)=='@'){
	    						$var[0] = substr($var[0],1);
	    					}
	    					$data = array();
	    					$data['skey']=trim($var[0]);
	    					$data['sval']=trim($var[1]);
	    					$data['styleid']=$styleid;
	    					$hasgot = $this->Stylevar->find('first',array('conditions'=>array('skey'=>$data['skey'],'styleid'=>$styleid)));
	    					if(!empty($hasgot)){
	    						$this->Stylevar->id = $hasgot['Stylevar']['id'];
	    						$hasgot['Stylevar']['sval'] = $data['sval'];
	    						$hasgot = $this->Stylevar->save($hasgot);
	    					}
	    					else{
	    						$this->Stylevar->create();
	    						$hasgot = $this->Stylevar->save($data);
	    					}
	    				}
	    			}
    			}
    			echo 'add success.';
    		}
    		else{
    			print_r($this->Style->invalidFields());
    		}
    		exit;
    	}
    }
    
    /**
     * 传入styleid的参数值 
     * @see AppController::admin_edit()
     */
    public function admin_edit($styleid = '', $copy = NULL){
    	if(!empty($_POST)){
			$this->Stylevar->saveAll($this->data['Stylevar']);    		
    	}
    	$styles = $this->Stylevar->find('all',array('conditions'=>array('styleid'=>$styleid)));
    	$this->set('styles',$styles);
    	$this->set('styleid',$styleid);
    }

}

?>