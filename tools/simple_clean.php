<?php
define('DS', DIRECTORY_SEPARATOR);

$root_path = dirname(dirname(__FILE__));

echo "#######################".$root_path."#######################\r\n";
if(!is_dir($root_path.DS.'app')) {
	echo "has no app.error.";
	exit;
}
elseif(php_sapi_name()!=='cli'){
	echo "not run in cli mode.";
	exit;
}

/*app/Controller目录不在数组中的文件全删除*/
dealfolder( $root_path.DS.'app'.DS.'Controller',array(),array(
	'AppController.php',
	'AnnouncementsController.php',
	'ArticlesController.php',
	'CategoriesController.php',
	'FavoritesController.php',	
	'ShortmessagesController.php',
	'','','',
	'ToolsController.php',
	'UploadfilesController.php',
	'UsersController.php',
	'UserCatesController.php',
	'UserSettingsController.php',	
));

/*app目录不在数组中的文件全删除*/
dealfolder( $root_path.DS.'app'.DS.'Controller'.DS.'Component',array(),array(
	'MultiSiteHookComponent.php',
	'UCenterHookComponent.php',
	'KcaptchaComponent.php',
	'','','',
));

/*删除docs中的所有文件*/
dealfolder( $root_path.DS.'docs',array('a.txt'),array(),true);
dealfolder( $root_path.DS.'client',array('a.txt'),array(),true);
dealfolder( $root_path.DS.'oss',array('a.txt'),array(),true);
dealfolder( $root_path.DS.'webroot'.DS.'open',array('a.txt'),array(),true);

dealfolder( $root_path.DS.'webroot'.DS.'files',array(),array(
	'flashes','images','remote','tmp','avatar','users',
	'videos','videos'.DS.'video.flv'
),true);

dealfolder( $root_path.DS.'data'.DS.'cache',array(),array(
	'models','persistent','views'
),true);

dealfolder( $root_path.DS.'tools',array('simple_clean.php'),array(),true);


/*manage/Controller目录不在数组中的文件全删除*/
dealfolder( $root_path.DS.'manage'.DS.'Controller',array(),array(
	'AppController.php',
	'AjaxesController.php',
	'ArticlesController.php',
	'ModelextendsController.php',
	'MenusController.php',
	'SystemsController.php',
	'EventHandlesController.php',
	'I18nfieldsController.php',
	'UploadfilesController.php',
	'UsersController.php',	
	'ToolsController.php',
	'VersionControlsController.php',
	'RecyclesController.php',
	'SettingsController.php','StaffsController.php',
	'StylesController.php',
	'StylevarsController.php',
	'TemplatesController.php',
));

echo "\r\n\r\nover\r\n";
exit;

//读出文件夹中所有子文件夹
function dealfolder($Path,$excludes,$includes,$recursive = false) {
	$uploadfileurl=array();
	$folderfiles = $foldertree=array();

	if(!is_dir($Path)){
		return;
	}

	$handle  = opendir($Path);
	$i=0;
	//$file =
	while(($file = readdir($handle)) !==false){
		$newpath=$Path . DS . $file;
		if(is_dir($newpath)){
			if($file!=".." && $file!="."){
				$foldertree[] = $newpath;
			}
		}
		else{
			$folderfiles[]=$newpath;
			$i++;
		}
	}
	if(count($folderfiles) > 0) {
		dealfile($folderfiles,$excludes,$includes);
	}
	
	if( $recursive && is_array($foldertree)&& count($foldertree) > 0) {
		foreach($foldertree as $key => $value){
			dealfolder($value,$excludes,$includes,$recursive);
		}
	}

	// #########删除后，重新遍历目录，目录为空则删除目录##############
	$folderfiles = $foldertree=array();
	$handle  = opendir($Path);
	while($file = readdir($handle)){
		$newpath=$Path . DS . $file;
		if(is_dir($newpath)){
			if($file!=".." && $file!="."){
				$foldertree[] = $newpath;
			}
		}
		else{
			$folderfiles[]=$newpath;
			$i++;
		}
	}
	if( empty($foldertree) && empty($folderfiles)  ){
		$path_name = basename($Path);
		if( !in_array($path_name,$includes)) {
			rmdir($Path); //空文件夹删除
		}
		else{
			file_put_contents($Path.DS.'empty','');
		}
	}
}
// $excludes, 在数组内的全部删除
// $includes, 在数组内的全部保留，其余全删除
function dealfile($paths,$excludes,$includes){
	global $buster;	
	foreach($paths as $filename)	{		
		if( !empty($excludes) ){
			$ext_flag = true;
			foreach($excludes as $exf){
				if( strpos($filename,$exf) !== false ){
					$ext_flag = false;
				}
			}
			if( $ext_flag ) {
				unlink($filename);
			}
		}
		elseif( !empty($includes) ){
			$inc_flag = false;
			foreach($includes as $inf){
				if(empty($inf)) {continue;}
				$pos = strpos($filename,$inf);
				// inf路径在最末尾。
				if( $pos !== false && $pos+strlen($inf)==strlen($filename) ){
					$inc_flag = true;					
				}
			}
			if( !$inc_flag ) {
				unlink($filename);
			}
		}
	}
}
