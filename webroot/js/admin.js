
var last_open_dialog = null; // 记录最后一次打开的dialog
var jqgrid_scrollOffset = null; //记录jqgrid的滚动条位置； // 触发更新事件时，自动滚动到先前滚动条所在的位置。

//显示表单提交成功的信息
window.showSuccessMessage = function (text)
{
	//alert(text);
//  top-left, top-right, bottom-left, bottom-right, center
	$.jGrowl(text, { 
//		sticky:true,
		closeTemplate:'',
		themeState:'',
		position:'bottom-right',
		life:6000,
		theme: 'alert alert-success'
	});
	return true;
}
// 显示错误信息

window.showErrorMessage = function (text)
{
	//alert(text);
	$.jGrowl(text, { 
//		sticky:true,
		closeTemplate:'',
		themeState:'',
		position:'center',
		life:8000,
		theme: 'alert alert-danger' // danger
	});
	return true;
}

$(document).ready(function() {
	page_loaded();
	//alert($.jslanguage.selectAll);
	$('.checkbox input:checkbox').each(function(){
		if(this.value==""){
			$(this).next().html($.jslanguage.selectAll); // 设置值为空的多选项后的label的文字为全选
		}
	});
	
	$('.checkbox input:checkbox').on('click',function(){
		//alert($(this).parents('div').eq(1).find('input:checkbox').size());
		// 取得当前父级第二级，并将内部的checkbox全选		
		if(this.value==""){
			if(this.checked){
				$(this).parents('div:first').find('input:checkbox').attr('checked',true);
			}
			else{
				$(this).parents('div:first').find('input:checkbox').removeAttr('checked');
			}
		}
	});
	
	
});
function page_loaded()
{
	if($.fn.colorPicker) {
		$('.colorPicker').colorPicker({
		    customBG: '#FFF',
		    size:2,
		    mode:'hsv-h', //rgb-r
		    init: function(elm, colors) { // colors is a different instance (not connected to colorPicker)
		      elm.style.backgroundColor = elm.value;
		      elm.style.color = colors.rgbaMixCustom.luminance > 0.22 ? '#222' : '#ddd';
		    }
		});
	}

	$('.datepicker').datepicker({
		showButtonPanel: true,dateFormat:'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		zIndex:100000
	});
	
    $('button').addClass('btn btn-default');
    $('input[type=submit]').addClass('btn btn-primary');    
    $('[data-toggle="tooltip"],.showtooltip').tooltip({container: 'body',html:true});
    /**
     * 绑定上传文件的点击删除事件
     */
    $('.upload-filelist').off('click.delete').on('click.delete','a.upload-file-delete',function(){
    	var obj = this;
    	ajaxAction($(obj).data('url'),null,null,function(){
    		$('#upload-file-'+$(obj).attr('rel')).remove();
    	});
    	return false;
    });
    /*参数设置模块，设置项排序。/admin/settings/prefix/site */
    $('.settings-sortable').sortable({
		revert:true,
		cancel:':input,:radio,:image,:button',
		update:function(event,ui){
			//console.log($(this).sortable('toArray'));serialize
			ajaxAction(ADMIN_BASEURL+'/admin/settings/sort.json',$(".settings-sortable").sortable( "serialize" ));
		}
	});

    /**
     * 绑定使用、注销可见即可得编辑器
     */
	$('.use_editor').off( "click").on('click',function(){
		var id = $(this).parent().find("textarea:first").attr('id');
		if(id && CKEDITOR){
			if(CKEDITOR.instances[id]){
				CKEDITOR.instances[id].destroy();
			}
			if($(this).html()==$.jslanguage.use_editor){
				$(this).html($.jslanguage.destory_editor);
				CKEDITOR.replace(id);
			}
			else{
				$(this).html($.jslanguage.use_editor);
				//$(this).addClass('hidden');
			}
		}
	})
}



function open_content_dialog(id){
	$("#"+id).dialog({ width: 650,modal: true});
	return false;
}

function getEvent() {
    if (document.all) {
        return window.event; //如果ie
    }
    func = getEvent.caller;
    while (func != null) {
        var arg0 = func.arguments[0];
        if (arg0) {
            if ((arg0.constructor == Event || arg0.constructor == MouseEvent) || (typeof (arg0) == "object" && arg0.preventDefault && arg0.stopPropagation)) {
//                alert(arg0);
            	return arg0;
            }
        }
        func = func.caller;
    }
    return null;
}

/**
 * dialog方式打开一个url地址,根据url地址生成dialog的id
 * 
 * @param options 为dialog的options参数,为对象形式 ‘{}’
 * @param url 为dialog打开对话框的内容url，通过ajax获取内容
 * @param callback ajax获取完内容后的回调事件
 * @param params 为回调函数的传参，为对象形式 ‘{}’
 * @param event 为触发的事件，可通过它找到对应触发的元素
 * */
var window_oepner_callbacks = {};
function open_dialog(options,url,callback,params,event){
	event = event? event:getEvent();
	var src_obj = event.srcElement ? event.srcElement:event.target;
	if(url.match(/javascript:/)){
		return false;
	}
	
	var dialogid = "dialog_"+url.replace(/\.html/g,'').replace(/http:/g,'').replace(/\/|\.|:|,|\?|=|&| /g,'_').substr(20);
	if(url.search(/\?/)!=-1){
		url+='&callbackid='+dialogid+ '&_=' + randomString(10);
	}
	else{
		url+='?callbackid='+dialogid+ '&_=' + randomString(10);
	}
	var iWidth = 960;                          //弹出窗口的宽度;
    var iHeight = 575;                         //弹出窗口的高度;
    //获得窗口的垂直位置
    var iTop = (window.screen.availHeight-30-iHeight)/2;        
    //获得窗口的水平位置
    var iLeft = (window.screen.availWidth-10-iWidth)/2;  
    var ret;
    if(window.showModalDialog){
    	ret = window.showModalDialog(url,window,'dialogHeight:575px; dialogWidth:1000px;help:No;resizable:No;status:No;scroll:Yes');
    }
    else{
    	ret = window.open(url,'new_window','dialogHeight:575px; dialogWidth:1000px;help:No;resizable:No;status:No;scroll:Yes');
    }
    //var ret = window.showModalDialog(url,window,'dialogheight='+iHeight+'px,dialogwidth='+iWidth+'px,dialogleft=50px,dialogtop=30px,toolbar=no,menubar=no,help=no,resizable=no,location=no, status=no');
	//ret 0时，不做处理。其余回调
    if(ret>0){
    	if(typeof(callback)=='function'){
	    	if(typeof(params)=='undefined'){
				var obj = callback();
			}
			else{
				var obj = callback(params);
			};
    	}
    	if(src_obj.tagName=='A'){
			$(src_obj).trigger('onDialogSubmitSucess');
		}
		else{
			$(src_obj).closest('a').trigger('onDialogSubmitSucess');
		}
    }
    //var dialog = window.open(url,dialogid,'height='+iHeight+',width='+iWidth+',top='+iTop+',left='+iLeft+',alwaysRaised=yes,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no')
	//alert(dialog);
	/*
	var myCallback = function(event) {  
	    if (event.url.indexOf('sina_callback.jsp?uid') > 0) {  
	        alert(event.url);  
	        ref.close();  
	    }  
	}  
	dialog.addEventListener('loadstart', myCallback);
	*/
	return ;
}

/*	
function open_dialog(options,url,callback,params,event){	
	event = event? event:getEvent();
	var src_obj = event.srcElement ? event.srcElement:event.target;
	
	if(typeof(options.title)=='undefined' && typeof($(src_obj).attr('title'))!='undefined'){
		options.title = $(src_obj).attr('title');
	}
	//options.dialogtype = 'iframe';
	
	/*if(url.search(/\?/)!=-1){
		url+='&inajax=1';
	}
	else{
		url+='?inajax=1';
	}
	var dialogid = "dialog_"+url.replace(/\.html/g,'').replace(/http:/g,'').replace(/\/|\.|:|,|\?|=|&| /g,'_');
	
	window.open(url,dialogid,'height=500,width=900,top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no') 
	return ;
	
	
	if(window.open_window){ //desktop
		return window.open_window(url,{title:options.title});
	}
	
	last_open_dialog = dialogid;
	var dialog_tpl = '<div id="'+dialogid+'" class="modal fade">\
  <div class="modal-dialog">\
    <div class="modal-content">\
      <div class="modal-header">\
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
        <h4 class="modal-title"></h4>\
      </div>\
      <div class="modal-body">loading...\
      </div>\
    </div><!-- /.modal-content -->\
  </div><!-- /.modal-dialog -->\
</div><!-- /.modal -->\
';
	
	if($('#'+dialogid).size()<1){
		$(dialog_tpl).appendTo('body');
		//$('<div id="'+dialogid+'">loading...</div>').appendTo('body');
	}
	
	$('#'+dialogid).modal().on('hidden.bs.modal', function () {
		  // do something…
		for(var i in ckeditors)	{
			CKEDITOR.remove(ckeditors[i]);
		}
		$(this).remove();
	});
	


	
	if(options.dialogtype && options.dialogtype=='iframe'){
		url = url.replace(/inajax=1/g,'');
		$('#'+dialogid).find('.modal-body').html('<iframe width="100%" border="0" height="100%" src="'+url+'"></iframe>');
	}
	else{
		$.ajax({
			url:url,
			data:{},
			success:function(data,textStatus){  
				try{
					var obj=eval("("+data+")"); // 返回的数据为json格式，表示操作成功，弹出提示语
					if(obj.success && typeof(obj.success)=='xml'){
						throw "error";
					}
					if(obj.success && obj.success!=""){
						var content = '<div class="ui-widget">'+
						'<div style="margin: 0px;padding: 5px 10px;" class="ui-state-highlight ui-corner-all"> '+
							'<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>'+
							obj.success +'.</p>'+
						'</div></div>';
						$('#'+dialogid).find('.modal-body').html(content);
					}
					else if(obj.error && obj.error!=""){
						var content = '<div class="ui-widget">'+
						'<div style="margin: 0px;padding: 5px 10px;" class="ui-state-error ui-corner-all"> '+
							'<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-alert"></span>'+
							obj.error +'.</p>'+
						'</div></div>';
						$('#'+dialogid).find('.modal-body').html(content);
					}
					else{
						throw "error";
					}
					//$('#'+dialogid).dialog( "option", "width", 350 );
					//$('#'+dialogid).dialog( "option", "buttons", { "Ok": function() { $(this).dialog("close"); } } );
					//$('#'+dialogid).show('pulsate',{times:2},200,function(){
						//$(this).dialog('close');
					//});
					
					//$( "#remote-dialog" ).fadeOut('slow');  
					if(typeof(callback)=='function'){
						if(typeof(params)=='undefined'){
							var obj = callback();
						}
						else{
							var obj = callback(params);
						}
					}
				}
				catch(err){
					$('#'+dialogid).find('.modal-body').html(data);
					
					var form = $('#'+dialogid).find('form').eq(0);
					form.bind("onSubmitSucess", function (e) {
						$('#'+dialogid).modal('hide');
						if(src_obj.tagName=='A'){
							$(src_obj).trigger('onDialogSubmitSucess');
						}
						else{
							$(src_obj).closest('a').trigger('onDialogSubmitSucess');
						}
	                });
			    		    	
			    	if(typeof(callback)=='function'){
			    		// dialog关闭时，刷新列表内容。dialog中表单是否提交都会刷新，选择条件区分无条件时不刷新
			    		$('#'+dialogid).on('hidden.bs.modal', function () {
			    			  // do something…
			    			if(typeof(params)=='undefined'){
								var obj = callback();
							}
							else{
								var obj = callback(params);
							};
			    		});
					}
			    	
			    	
			    	$dialog = $('#'+dialogid);				
					function load_url(){
						page_loaded();
						$dialog.find('a').click(function(){	
							var url = $(this).attr('href');
							var re = /^#/;
							if( typeof($(this).attr('onclick')) != "undefined" || re.test(url) || url.substr(0,10).toLowerCase()=='javascript'){
								return true; // 当为锚点，javascript,或者定义了onclick 时，忽略动作
								// 可以使用onclick="return true;"，来忽略dialog中链接的绑定事件
							}							
							$dialog.find('.modal-body').load(url,function(){ load_url();});							
							return false;
						});						
					}
					load_url();
				}
				$('body').css('overflowY','auto');
				//$('#'+dialogid).dialog( "option", "position", ['center','center']);
				
		    },
		    dataType:"html",
		    cache:false
		});
	}
	return false;
}
*/

// 弹出消息对话框，自动关闭
function show_message(txt,times)
{
	if(!times) times=5;
	if($('#show_message_dialog').size()<1) $('<div id=\"show_message_dialog\" title=\"Result\"></div>').appendTo('body');
	
	$('#show_message_dialog').dialog({
		height:120,
		modal: false
	});
	$('#show_message_dialog').html(txt);
	$( '#show_message_dialog' ).show('pulsate',{times:times},300,function(){
		$( '#show_message_dialog' ).dialog('close');
	});
}
//弹出内容展示，不自动关闭
function show_content(txt)
{
	if($('#show_content_dialog_message').size()<1) $('<div id=\"show_content_dialog_message\" ></div>').appendTo('body');	
	$('#show_content_dialog_message').dialog({
		height:420,
		width:600,
		modal:false
	});
	$('#show_content_dialog_message').html(txt);
	$('#show_content_dialog_message' ).show();
}
/* dialog方式打开一个url地址*/
function close_dialog(){
	$('#remote-dialog').dialog('close');
	return false;
}
// 显示表单提交成功的信息
function showDialogMessage(request)
{
	return showSuccessMessage(request.success);
	
	if(last_open_dialog){
		$('#'+last_open_dialog).dialog('close');
	}
	if($('#ajax_action_result').size()<1) $('<div id="ajax_action_result" title="Result"></div>').appendTo('body');
	$('#ajax_action_result').html(request.success);
	var dbuttons = {};
	if(request.actions){
		for(var i in request.actions){
			if(request.actions[i]=='closedialog'){
				dbuttons[i] = function(){
					$(this).dialog('close');
				};
			}
			else if(request.actions[i]=='resetform'){							
				dbuttons[i] = function(){
					$(document.forms[0]).trigger('reset');
					$('.fileuploadinfo').html('');
					$(this).dialog('close');
				};
			}
		}
	}
	
	$('#ajax_action_result').dialog({
		height:160,
		modal: true,
		buttons: dbuttons
	});  
	$( '#ajax_action_result' ).show('pulsate',{times:5},500,function(){
		$( '#ajax_action_result' ).dialog('close');
	});
}
// 显示表单验证错误的信息
function showValidateErrors(request,model,suffix)
{
	var tempmodel = model;
	var field_name = '';
	var error_message = '';
	var firsterror = true;
	for(var i in request){
//		alert(i);
		tempmodel = model;
		field_name =i;
		var split_str=i.split('.');
		if(split_str.length>1){
			tempmodel = split_str[0];
			field_name = split_str[1];
		}
		// 首字母大写，如将task_id,替换成Task_id
		var field = field_name.replace(/\b\w+\b/g, function(word) {
           return word.substring(0,1).toUpperCase( ) +
                  word.substring(1);
         });
         // 替换下划线 ，并使字符串为驼峰型。如将Task_id,替换成TaskId
         field = field.replace(/\_\w/g, function(word) {
           return word.substring(1,2).toUpperCase( );
         });
         //alert('#error_".$options['model']."'+field);
		if(firsterror)
		{
			window.location.hash = '#'+tempmodel+field+suffix;
			firsterror = false;
		}
		$("#error_"+tempmodel+field+suffix).remove();
		$('#'+tempmodel+field+suffix).parent('div:first').append("<span id='error_"+tempmodel+field+suffix+"' id='error_"+tempmodel+field+suffix+"' class='ui-state-error ui-corner-all' style='position: absolute;'><span class='ui-icon ui-icon-alert'></span>"+request[i]+"</span>");
		var txt = $('label[for="'+tempmodel+field+suffix+'"]').html();
		//alert(txt);
		error_message +=txt+':'+ request[i]+"<br/>";
	}
	if(error_message!=''){
		show_message(error_message,8);
	}
}

function setCoverImg(model,imgsrc){
	$('#'+model+'Coverimg').val(imgsrc);	
	$('#'+model+'CoverimgPreview').attr('src',imgsrc);
}

function addNewCrawlRegular()
{
	var field = $('.model-schema-list').val();
	$('.model-schema-area').before($('<div class="regexp-add"><label for="CrawlRegexp'+field+'">Regexp '+field+'</label><textarea id="CrawlRegexp'+field+'" cols="60" rows="2" name="data[Crawl][regexp_'+field+']"></textarea></div>'));
}

/**
 * ajaxAction设置栏目为首页，成功后的回调函数
 * @param request ajax返回的内容
 * @param obj	  obj为点击的元素 <a class="btn">...</a>
 */
rs_callbacks.set_index_page = function(request,obj){
	$(obj).closest('.ui-dialog-content').find('.btn-success').removeClass('btn-success').html('<i class="icon-home"></i>');
	$(obj).addClass('btn-success').html('<i class="icon-ok icon-white"></i>');
}

/**
 * 生成slug链接的函数及回调
 * @param request
 * @param selector
 */
rs_callbacks.generateSlug = function(request,selector){
	$(selector).val(request.slug); 
}
function generateSlug(obj,selector){
	if(obj.value){
		var url = ADMIN_BASEURL+'/admin/tools/genSlug';
		var postdata = {'word':obj.value};
		if($(obj).data('model')) {
			postdata.model = $(obj).data('model');
		}
		ajaxAction(url,postdata,null,'generateSlug',selector);
	}
}


function upload_multi_file(file, serverData){
	try {
		var progress = progress_list[file.id];
		progress.setComplete();
		if (serverData === " ") {
			this.customSettings.upload_successful = false;
		} else {
			var data=eval("(" + serverData + ")");
			if(data.status==1){
				this.customSettings.upload_successful = true;
				var filesize = '';
				if(data.size/1024/1024 >1){
					
					filesize = Math.round(data.size/1024/1024*100)/100 +'MB';
				}
				else if(data.size/1024 >1){
					filesize = Math.round(data.size/1024*100)/100 +'KB';
				}
				else{
					filesize = data.size + 'B';
				}
				
				$("#fileuploadinfo_"+data.fieldname+"_"+data.data_id).append('<div class="col-md-4  upload-fileitem">'+
		                 ' <a target="_blank" href="'+ BASEURL +'/admin/uploadfiles/download/'+data.id+'" class="btn btn-success "><i class="glyphicon glyphicon-cloud-download"></i><div>'+data.name+'</div><small>File Size: '+ filesize +'</small></a>'+
		                 '<i class="glyphicon glyphicon-remove remove" data-id="'+data.data_id+'" title="Remove"></i>'+
			             '</div>');
			}
		}
	} catch (e) {
		alert(serverData);
	}
}


/*
 * CUSTOM MENU PLUGIN
 */

$.fn.extend({

	//pass the options variable to the function
	jarvismenu : function(options) {

		var defaults = {
			accordion : 'true',
			speed : 200,
			closedSign : '[+]',
			openedSign : '[-]'
		};

		// Extend our default options with those provided.
		var opts = $.extend(defaults, options);
		//Assign current element to variable, in this case is UL element
		var $this = $(this);

		//add a mark [+] to a multilevel menu
		$this.find("li").each(function() {
			if ($(this).find("ul").size() != 0) {
				//add the multilevel sign next to the link
				$(this).find("a:first").append("<b class='collapse-sign'>" + opts.openedSign + "</b>");

				//avoid jumping to the top of the page when the href is an #
				if ($(this).find("a:first").attr('href') == "#") {
					$(this).find("a:first").click(function() {
						return false;
					});
				}
			}
		});

		//open active level
		$this.find("li.active").each(function() {
			$(this).parents("ul").slideDown(opts.speed);
			$(this).parents("ul").parent("li").find("b:first").html(opts.openedSign);
			$(this).parents("ul").parent("li").addClass("open")
		});

		$this.find("li a").click(function() {

			if ($(this).parent().find("ul").size() != 0) {

				if (opts.accordion) {
					//Do nothing when the list is open
					if (!$(this).parent().find("ul").is(':visible')) {
						parents = $(this).parent().parents("ul");
						visible = $this.find("ul:visible");
						visible.each(function(visibleIndex) {
							var close = true;
							parents.each(function(parentIndex) {
								if (parents[parentIndex] == visible[visibleIndex]) {
									close = false;
									return false;
								}
							});
							if (close) {
								if ($(this).parent().find("ul") != visible[visibleIndex]) {
									$(visible[visibleIndex]).slideUp(opts.speed, function() {
										$(this).parent("li").find("b:first").html(opts.closedSign);
										$(this).parent("li").removeClass("open");
									});

								}
							}
						});
					}
				}// end if
				if ($(this).parent().find("ul:first").is(":visible") && !$(this).parent().find("ul:first").hasClass("active")) {
					$(this).parent().find("ul:first").slideUp(opts.speed, function() {
						$(this).parent("li").removeClass("open");
						$(this).parent("li").find("b:first").delay(opts.speed).html(opts.closedSign);
					});

				} else {
					$(this).parent().find("ul:first").slideDown(opts.speed, function() {
						/*$(this).effect("highlight", {color : '#616161'}, 500); - disabled due to CPU clocking on phones*/
						$(this).parent("li").addClass("open");
						$(this).parent("li").find("b:first").delay(opts.speed).html(opts.openedSign);
					});
				} // end else
			} // end if
		});
	} // end function
});

/* ~ END: CUSTOM MENU PLUGIN */