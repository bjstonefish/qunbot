var message_positon = 'bottom-right';

var setAmount={
		min:0,
		max:9999,
		reg:function(x){
			return new RegExp("^[1-9]\\d*$").test(x);
		},
		amount:function(obj,mode){
			var x=$(obj).val();
			if (this.reg(x) || x==0){
				if (mode){
					x++;
				}else{
					x--;
				}
			}else{
				alert("请输入正确的数量！");
				$(obj).val(this.min);
				$(obj).focus();
			}
			return x;
		},
		reduce:function(obj){
			var x=this.amount(obj,false);
			if (x>=this.min){
				$(obj).val(x);
			}else{
				alert("商品数量最少为"+this.min);
				$(obj).val(this.min);
				$(obj).focus();
			}
		},
		add:function(obj){
			var x=this.amount(obj,true);
			if (x<=this.max){
				$(obj).val(x);
			}else{
				alert("商品数量最多为"+this.max);
				$(obj).val(this.max);
				$(obj).focus();
			}
		},
		modify:function(obj){
			var x=$(obj).val();
			if (x<this.min||x>this.max||!this.reg(x)){
				alert("请输入正确的数量！");
				$(obj).val(this.min);
				$(obj).focus();
			}
		}
};

function loadUserCateOptions(selector,model) {
	ajaxAction('/user_cates/getUserCates/'+model,null,null,function(request){
		var options = '';
		if(request['']){
			options = '<option>'+request['']+'</option>';
		}
		for(var i in request){
			if(i!="") {
				options = options + '<option value="'+i+'">'+request[i]+'</option>';
			}
		}
		$(selector).empty().append(options);
		showSuccessMessage('分类数据加载成功');
	});
}

function showHtmlMessage(id,title,content)
{
    publishController.open_msg_dialog(id,title,content);
    return true;
}
// 显示表单提交成功的信息
function showSuccessMessage(text,position,life)
{
	if(position == null || typeof(position) == 'undefined') {
		position = message_positon;
	}
	if(!life){
		life = 4000;
	}
	//  top-left, top-right, bottom-left, bottom-right, center
	if($.fn.jGrowl){
		$('#jGrowl').find('.jGrowl-notification').trigger('jGrowl.close');
		$.jGrowl("<i style='margin-right:10px;' class='fa fa-check'></i> "+text, { 
			theme: 'alert alert-success',
			//sticky:true,
			life:life,
			closer:true,
			closeTemplate:'&times;',
			closerTemplate:'',
			themeState:'',
			position: position
		});
	}
	else{
		alert(text);
	}	
	return true;
}

// 显示错误信息

function showErrorMessage(text,position,life)
{
    if(position == null || typeof(position) == 'undefined') {
        position = message_positon;
    }
    if(!life){
        life = 5000;
    }
	if($.fn.jGrowl){
		$('#jGrowl').find('.jGrowl-notification').trigger('jGrowl.close');
		$.jGrowl("<i class='fa fa-warning'></i> "+text, {
			theme: 'alert alert-danger',// danger,
			position: position,
			//sticky:true,
			closer:true,
			closeTemplate:'&times;',
			closerTemplate:'',
			life:life,
			themeState:''
		});
	}
	else{
		alert(text);
	}
	return true;
}

function page_loaded()
{
	$('.popover-trigger').popover({trigger:"hover"});

	$('.ui-portlet-content').each(function(){
		if(jQuery.trim($(this).html())==''){
			$(this).parent('.ui-portlet').hide();
		}
	});
	
	$('[data-toggle="tooltip"],.showtooltip').tooltip({container: 'body',html:true}); // tooltip为bootstrap样式。页面中使用showtooltip的class
	$('[data-toggle="popover"]').popover({html: true});
	 
    $('.delete-pop').popover(
        {
            trigger:'click', //触发方式
            container:'body',
            placement:'top',
            template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3>'
            	  +'<div class="popover-content">确定删除此内容吗？ </div>'+
			  ' <div class="popover_bar text-center" style="margin-bottom:10px;"><a href="javascript:;" class="btn btn-primary btn-sm pop-comfirm">确定</a>&nbsp;<a href="javascript:;" class="btn btn-default btn-sm pop-cancel">取消</a></div>'
		    +'</div>', 
            html: true, // 为true的话，data-content里就能放html代码了
            // 还有很多选项 ……
        }
    ).unbind('shown.bs.popover').on('shown.bs.popover', function () {
		var $this = $(this);
		$this.trigger('blur');
		var pop = $this.data('bs.popover').tip();
		pop.find('.pop-cancel').unbind('click').click(function(){
			  $this.popover('hide');
		  });
		pop.find('.pop-comfirm').unbind('click').click(function(){
			  $this.popover('hide');
			  ajaxAction($this.data('url'),null,null,function(request){
				  if(request.success || request.ret == 0)
				  {
					  $this.parents('.appmsg:first').remove();
				  }
				  else{
				  	showErrorMessage(request.msg);
				  }
			  });
		})
    });
}

$(document).ready(function() {
	page_loaded();	
});

function detectCapsLock(e, obj) {
	var valueCapsLock = e.keyCode ? e.keyCode : e.which;
	var valueShift = e.shiftKey ? e.shiftKey : (valueCapsLock == 16 ? true : false);
	obj.className = (valueCapsLock >= 65 && valueCapsLock <= 90 && !valueShift || valueCapsLock >= 97 && valueCapsLock <= 122 && valueShift) ? 'form-control clck txt' : 'form-control txt';
	$(obj).blur(function () {
		$(this).className = 'form-control txt';
	});
}


// 显示表单验证错误的信息
function showValidateErrors(request,model,suffix)
{
	var tempmodel = model;
	var field_name = '';
	var error_message = '';
	var firsterror = true;
	for(var i in request)
	{
		//alert(i);
		tempmodel = model;
		field_name =i;
		var split_str=i.split('.');
		if(split_str.length>1)
		{
			tempmodel = split_str[0];
			field_name = split_str[1];
		}
		// 首字母大写，如将task_id,替换成Task_id
		var field = field_name.replace(/\b\w+\b/g, function(word) {
           return word.substring(0,1).toUpperCase( ) +
                  word.substring(1);
         });
         // 替换下划线 ，并使字符串为驼峰型。如将Task_id,替换成TaskId
         field = field.replace(/\_\w/g, function(word) {
           return word.substring(1,2).toUpperCase( );
         });
         //alert('#error_".$options['model']."'+field);
		
		if(firsterror)
		{
			window.location.hash = '#'+tempmodel+field+suffix;
			firsterror = false;
		}
		$("#error_"+tempmodel+field+suffix).remove();
		$('#'+tempmodel+field+suffix).parent('div:first').append("<span id='error_"+tempmodel+field+suffix+"' name='error_"+tempmodel+field+suffix+"' class='ui-state-error ui-corner-all' style='position: absolute;'><span class='ui-icon ui-icon-alert'></span>"+request[i]+"</span>");
		var txt = $('label[for="'+tempmodel+field+suffix+'"]').html();
		//alert(txt);
		error_message +=txt+':'+ request[i]+"<br/>";
	}
	if(error_message!='')
	{
		show_message(error_message,8);
	}
}
function addNewCrawlRegular()
{
	var field = $('.model-schema-list').val();
	$('.model-schema-area').before($('<div class="regexp-add"><label for="CrawlRegexp'+field+'">Regexp '+field+'</label><textarea id="CrawlRegexp'+field+'" cols="60" rows="2" name="data[Crawl][regexp_'+field+']"></textarea></div>'));
}
var AjaxHelper={
	dialog_open:false,
	open_help: function(){
		$('#ajax_doing_help').html('<img src="/img/ajax/circle_ball.gif" /> 正在提交...');
		$('#ajax_doing_help').dialog({width: 650,
			close: function(event, ui) {
				$('#invite-user-html').hide().appendTo('body');
			}
		});
		this.dialog_open=true;
	},	
	has_init_tab:false,
	friends_tab:null		
};


/* 订单ajax操作提示信息  开始*/
//显示提示信息
function showAlert(info,obj,infoSign)
{
   if($('#'+infoSign).size()>0){return;}
   var newd=document.createElement("span");
   newd.id=infoSign;
   newd.className='ui-state-error';
   newd.innerHTML=info;
   $(obj).append($(newd));
}
//删除提示信息
function removeAlert(infoSign)
{
   $(infoSign).remove();
}

function clearSubmitError(obj){
	$(obj).parent().find('.errorInfo').remove();
}
function clearWaitInfo(obj){
	if(obj){
		$(obj).parent().find('.waitInfo').remove();
	}
	else{
		$(".waitInfo").remove();
	}
}

function showWaitInfo(info,obj){
	try{
		if(obj==null)return;
		clearWaitInfo();
		var newd=document.createElement("span");
		newd.className='waitInfo';
		newd.id='waitInfo';
		newd.innerHTML=info;
		obj.parentNode.appendChild(newd);
	}catch(e){}
}

function showWaitInfoOnInner(info,obj){
	try{
		if(obj==null)return;
		clearWaitInfo();
		var newd=document.createElement("span");
		newd.className='waitInfo';
		newd.id='waitInfo';
		newd.innerHTML=info;
		obj.innerHTML='';
		obj.appendChild(newd);
	}catch(e){}
}



function upload_multi_file(file, serverData){
	try {
		var progress = progress_list[file.id];
		progress.setComplete();
		if (serverData === " ") {
			this.customSettings.upload_successful = false;
		} else {
			var data=eval("(" + serverData + ")");
			if(data.status==1){
				this.customSettings.upload_successful = true;
				var filesize = '';
				if(data.size/1024/1024 >1){
					
					filesize = Math.round(data.size/1024/1024*100)/100 +'MB';
				}
				else if(data.size/1024 >1){
					filesize = Math.round(data.size/1024*100)/100 +'KB';
				}
				else{
					filesize = data.size + 'B';
				}
				
				$("#fileuploadinfo_"+data.fieldname+"_"+data.data_id).append('<div class="col-md-4  upload-fileitem">'+
		                 ' <a target="_blank" href="'+ BASEURL +'/uploadfiles/download/'+data.id+'" class="btn btn-success "><i class="glyphicon glyphicon-cloud-download"></i><div>'+data.name+'</div><small>File Size: '+ filesize +'</small></a>'+
		                 '<i class="glyphicon glyphicon-remove remove" data-id="'+data.data_id+'" title="Remove"></i>'+
			             '</div>');
			}
		}
	} catch (e) {
		alert(serverData);
	}
}

/* 订单ajax操作提示信息  结束 */



$(function(){
	
//	jGrowl test
//	$.jGrowl("<i class='glyphicon glyphicon-ok'></i> test", { 
//		sticky:true,
//		closeTemplate:'',
//		theme: 'alert alert-success pull-left',
//		position:'center'
//	});
	//站内查询
//	$("input[type=text]").focusin(function(){$( this ).removeClass( "ui-state-default");
//		$( this ).addClass( "ui-state-focus");
//	});
//	$("input[type=text]").focusout(function(){$( this ).removeClass( "ui-state-focus");
//		$( this ).addClass( "ui-state-default");
//	});
	/* 菜单开始 */
	$('.ui-navi li').hover(function(){
		$(this).children(".ui-drop-menu:first").show();
	},function(){
		$(this).children(".ui-drop-menu:first").hide();
	});
	
	$(document).on('click','[data-toggle="tab"]',function(){
		var target = $(this).attr('href');
		var inner_selector = $(this).data('selector');
		if($(target).html() == "" || $(this).data('refresh') == "always") {	// 没有内容，或者设置了每次都刷新		
			if($(this).data('url')) {
				$(target).html('<img src="http://static.135editor.com/img/ajax/circle_ball.gif"> 正在加载...');
				$(target).load($(this).data('url'),function(){
					tab_loaded();
					function tab_loaded(){
						$(target + " img").lazyload({
							effect: "fadeIn",
							container:target,
							data_attribute:'src',
							threshold : 50,
							failure_limit:50
						});
						
						page_loaded();
						$(target).find('a').click(function(){	
							var url = $(this).attr('href');
							
							if(url.search(/\?/)!=-1){
								url+='&inajax=1';
							}
							else{
								url+='?inajax=1';
							}
							if($(this).attr('id') =='btn-search-image') {
								if($(this).data('input')) {
									url += '&name='+encodeURI($($(this).data('input'),target).val());
								}
								else{
									url += '&name='+encodeURI($('#SearchImageName',target).val());
								}
							}
							
							if(inner_selector) {
								url = url + ' ' + inner_selector;
							}
							
							var re = /^#/;
							if( typeof($(this).attr('onclick')) != "undefined" || $(this).attr('target') =='_blank' || typeof(url) == "undefined" || re.test(url) || url.substr(0,10).toLowerCase()=='javascript'){
								return true; // 当为锚点，javascript,或者定义了onclick 时，忽略动作
								// 可以使用onclick="return true;"或者target="_selft"来忽略dialog中链接的绑定事件
							}
							var inner_target = target;
							if( $(this).data('target') ) {
								inner_target += " "+$(this).data('target');
							}
							console.log(inner_target);
							$(inner_target).load(url,function(){ tab_loaded();});							
							return false;
						});
						
						$(target).find('form').submit(function(){
							var url = $(this).attr('action');
							var re = /^#/;
							if( $(this).attr('method') != "get" || typeof(url) == "undefined" ){
								return true; // 忽略表单提交动作
							}
							
							if(url.search(/\?/)!=-1){
								url += '&inajax=1&'+$(this).serialize();
							}
							else{
								url += '?inajax=1&'+$(this).serialize();
							}
							
							$(target).load(url,function(){ tab_loaded();});							
							return false;
						});
						$(target).scrollTop(0).trigger('scroll');
					}
				});
			}
		}
	});
	
	//二级菜单显示隐藏	
	$(".ui-sidemenu li").hover(
            function () {
                var li_width = $(this).width();
                var li_offset = $(this).offset();
                $(this).children("a").addClass( "ui-state-default");
                var submenu = $(this).children(".ui-secondmenu");
                if(li_offset.left>$(window).width()/2){
                    submenu.css('left',-submenu.width()+2).show();
                }
                else{
                    submenu.css('left',li_width-2).show();
                }
                 var offset = submenu.offset();
                if(li_offset.top-$(window).scrollTop()+submenu.height()>$(window).height()){
                    if(submenu.height() < $(window).height()){
                        // 子菜单的高度小于window的高度时，子菜单从window的底部开始显示
                        submenu.css('top',$(window).height()-2-li_offset.top -submenu.height()+$(window).scrollTop() );
                    }
                    else{
                        // 子菜单的高度大于window的高度时，从window顶部开始显示
                        submenu.css('top',-li_offset.top+$(window).scrollTop()+2);
                    }
                }
            },
            function(){
                $(this).children("a").removeClass( "ui-state-default");
                $(".ui-secondmenu",this).hide();
            });
	/* 菜单结束 */
	
	/*
	$('.pagelink a').live('click',function(){
		$linkobj = this;
		
		var region_obj = $(this).parents('.ui-portlet-content').eq(0);
		// 加载region分页的内容
		var page_url = this.href;
		var portletid = region_obj.parents('.ui-portlet').eq(0).attr('id');
		
		if(portletid){
			region_obj.load(page_url+' #' + portletid,{},function(){			
				var offset = region_obj.offset();
				$('html,body').animate({ scrollTop: offset.top },1000);			
				
				var html = region_obj.find('.ui-portlet-content').html();
				region_obj.html(html);
				
				if(!$.browser.msie){
					region_obj.find('img[original]').lazyload({
			            placeholder : BASEURL+"/img/grey.gif",
			            effect      : "fadeIn" ,threshold : 200                 
			        });
				}
				loadStatsData();
				
				
				var url_info = page_url.split('/');	
				var page = $($linkobj).html();
				var hash = window.location.hash;
				hash = hash.replace('page_'+portletid+'=','page=').replace(/page=\d+/,'');								
				if(hash=='#'||hash=='')	{
					hash='#page_'+portletid+'='+page;
				}
				else{
					hash = hash+'&page_'+portletid+'='+page;
					hash=hash.replace('&&','&');
				}
				page_hash.updateHash(hash, false);
				
			});	
		}
		return false;
	});*/
});


function WxMsgCoverimgUploaded(data) {
	if( $('#WxMsgThumbMediaId').size()> 0 ) {
		if(data.media_id) {
			$('#WxMsgThumbMediaId').val(data.media_id);
		}
		else{
			$('#WxMsgThumbMediaId').val('');
		}
	}
}



rs_callbacks.deleteConsignee = function(request){
	$('#consignee-'+request.id).parents('.consignee-item').remove();
}
//删除地址
function DelAddress(id){
	var evt = window.event || arguments.callee.caller.arguments[0]; 
	evt.stopPropagation();
	if(confirm($.jslanguage.sure_delete)){
		ajaxAction(BASEURL+"/orders/delete_consignee/"+id,null,null,'deleteConsignee');
	}
	return false;
}
rs_callbacks.defaultConsignee = function(request){
	$('.consignee-box').removeClass('bg-success');
	$('#consignee-'+request.id).addClass('bg-success');
}
//设为常用地址
function SetDefaultAddress(id){
	var evt = window.event || arguments.callee.caller.arguments[0]; 
	evt.stopPropagation();
	ajaxAction(BASEURL+"/orders/default_consignee/"+id,null,null,'defaultConsignee');
	return true;
}
rs_callbacks.editConsignee = function(request){
	publishController.close_dialog();

	if($('#part_consignee').parents('.modal-dialog').size() > 0){
		$('body').addClass('modal-open'); // 在弹窗中，修复body的class。
	}
	var dom_id = '#consignee-'+request.data.id;
	if($(dom_id).size() > 0){
		$(dom_id).html($(request.data.content).find(dom_id).html());
	}
	else
	{
		var consignee = $('<div>'+request.data.content+'</div>').find('.consignee-item:first');
		consignee.prependTo('#part_consignee');
		//$('#part_consignee').prepend(consignee.html());
		$(dom_id).trigger('click');
		//新增时选中新增的送货人信息 
	}
}


$(function(){
	
	$('#show-more-address').click(function(){
		if( $(this).find('.fa-chevron-down').size() > 0 ) {
            $('#part_consignee').addClass('show-more');
			$('.consignee-item').removeClass('hidden');
			$(this).html('<i class="fa fa-chevron-up"></i>隐藏</a>');
		}
		else{
            $('#part_consignee').removeClass('show-more');
			$('.consignee-item').each(function(i){
				if(i > 3) {
					$(this).addClass('hidden');
				}
			});
			$(this).html('<i class="fa fa-chevron-down"></i>更多地址</a>');
		}
	});
})

/**********************************************************************/
/*

//----------------------------发票--start-------------------------

rs_callbacks.editInvoice = function(request){
	publishController.close_dialog();
	var dom_id = '#invoice-'+request.data.id;
	if($(dom_id).size() > 0){
		$(dom_id).html($(request.data.content).find(dom_id).html());
	}
	else
	{
		var consignee = $('<div>'+request.data.content+'</div>').find('.invoice-item:first');
		consignee.prependTo('#part_invoice');
		//$('#part_consignee').prepend(consignee.html());
		$(dom_id).trigger('click');
	}
}

rs_callbacks.defaultInvoice = function(request){
	$('.invoice-box').removeClass('bg-success');
	$('#invoice-'+request.id).addClass('bg-success');
}
//设为常用地址
function SetDefaultInvoice(id){
	var evt = window.event || arguments.callee.caller.arguments[0]; 
	evt.stopPropagation();
	ajaxAction(BASEURL+"/orders/default_invoice/"+id,null,null,'defaultInvoice');
	return true;
}

rs_callbacks.deleteInvoice = function(request){
	$('#invoice-'+request.id).parent('.invoice-item').remove();
}
function DelInv(id){
	var evt = window.event || arguments.callee.caller.arguments[0]; 
	evt.stopPropagation();
	if(confirm($.jslanguage.sure_delete)){
		ajaxAction(BASEURL+"/orders/delete_invoice/"+id,null,null,'deleteInvoice');
	}
}


//----------------------------发票--end-------------------------
//显示支付方式和配送方式表单
function showForm_payTypeAndShipType(obj){
   //showWaitInfo('正在读取支付方式及配送方式信息，请等待！',obj);
   var runCode="isPayTypeAndShipTypeOpen=true;";
   runCode+="setPayShipRadioDefault();";
   runCode+="if(isShowUpdateInfo){";
//   runCode+="isShowUpdateInfo=false;";
   runCode+="$('#updateInfo_payType').html(\"<span class='payTypeChangeAlert'>由于您更改了收货人信息，请重新填写支付方式和配送方式！</span>\");";
   runCode+="}";
   runCode+="setPayRemarkShow();ShowShipTimeRemark();";
   $('#part_payTypeAndShipType .o_show').hide();
   $('#part_payTypeAndShipType .o_write').show();
   // ajaxActionHtml(BASEURL+"/orders/edit_consignee","#part_consignee","");
   //setAjax_getResAndRunCode("action=showForm_payTypeAndShipType","part_payTypeAndShipType",runCode);
}

//关闭支配方式
function close_payTypeAndShipType(obj){
	$('#part_payTypeAndShipType .o_show').show();
	$('#part_payTypeAndShipType .o_write').hide();
//  showWaitInfo('正在关闭表单，请等待！',obj);
//  isShowUpdateInfo=false;
//  showLabel_payTypeAndShipType();
}
//选择支付方式
function changePayType(payType){
   $('#payType_IdPaymentType').val(payType);
   showWaitInfoOnInner('正在加载配送方式信息，请等待。。。',g('part_shipType'));
   

   setAjax_getResAndRunCode("action=changePayType&payType="+payType,"part_shipType",runCode);
   
   setPayRemarkShow();
}
*/
/********** 备注信息 ***************/



//检查联系电话
function check_telephone()
{
   removeAlert('#phone_ff');   
   var pNode=$('#consignee_telephone').parent();
   //var myReg=/((\d+)|^((\d+)|(\d+)-(\d+)|(\d+)-(\d+)-(\d+)-(\d+))$)/;
   var myReg=/(\d{11})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$/;
   if($('#consignee_telephone').val()!='' && !myReg.test($('#consignee_telephone').val())){showAlert('固定电话格式不正确',pNode,'phone_ff');return false;}
   if($('#consignee_telephone').val()!='' && $('#consignee_telephone').val().length > 20 ){showAlert('固定电话格式不正确',pNode,'phone_ff');return false;}
   return true;
}
//检查手机号
function check_mobile()
{
   removeAlert('#mobile_ff');
   if($('#consignee_mobilephone').val()!=''){
	   var pNode=$('#consignee_mobilephone').parent();
	   var myReg=/(^\s*)(((\(\d{3}\))|(\d{3}\-))?13\d{9}|1\d{10})(\s*$)/;
	   if(!myReg.test($('#consignee_mobilephone').val())){showAlert('手机号格式不正确',pNode,'mobile_ff');return false;}
   }
   return true;
}
//检查Email
function check_email()
{  
   var iSign='email';
   removeAlert('#'+iSign+'_ff');
   if($('#consignee_'+iSign).val()!=''){
	   var pNode=$('#consignee_'+iSign).parent();
	   var myReg=/(^\s*)\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*(\s*$)/;
	   if(!myReg.test($('#consignee_'+iSign).val())){showAlert('电子邮件格式不正确',pNode,iSign+'_ff');return false;}
   }
   return true;
}
//检查邮政编码
function check_postcode()
{  
   removeAlert('postcode_ff');
   if($('#consignee_postcode').val()!=''){
	   var pNode=$('#consignee_postcode').parent();
	   var myReg=/(^\s*)\d{6}(\s*$)/;
	   if(!myReg.test($('#consignee_postcode').val())){showAlert('邮编格式不正确',pNode,'postcode_ff');return false;}
   }
   return true;
}


/* UItoTop jQuery Plugin 1.2 | Matt Varone | http://www.mattvarone.com/web-design/uitotop-jquery-plugin */
(function($){$.fn.UItoTop=function(options){var defaults={text:'',min:200,inDelay:600,outDelay:400,containerID:'toTop',containerHoverID:'toTopHover',scrollSpeed:1200,easingType:'linear'},settings=$.extend(defaults,options),containerIDhash='#'+settings.containerID,containerHoverIDHash='#'+settings.containerHoverID;$('body').append('<a href="#" id="'+settings.containerID+'">'+settings.text+'</a>');$(containerIDhash).hide().on('click.UItoTop',function(){$('html, body').animate({scrollTop:0},settings.scrollSpeed,settings.easingType);$('#'+settings.containerHoverID,this).stop().animate({'opacity':0},settings.inDelay,settings.easingType);return false;}).prepend('<span id="'+settings.containerHoverID+'"></span>').hover(function(){$(containerHoverIDHash,this).stop().animate({'opacity':1},600,'linear');},function(){$(containerHoverIDHash,this).stop().animate({'opacity':0},700,'linear');});$(window).scroll(function(){var sd=$(window).scrollTop();if(typeof document.body.style.maxHeight==="undefined"){$(containerIDhash).css({'position':'absolute','top':sd+$(window).height()-50});}
if(sd>settings.min)
$(containerIDhash).fadeIn(settings.inDelay);else
$(containerIDhash).fadeOut(settings.Outdelay);});};})(jQuery);


$(function(){
	// jQuery UItoTop
	$().UItoTop({ easingType: 'easeOutQuart' });
});
