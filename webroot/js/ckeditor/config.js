﻿/*
CKEDITOR.dtd.$removeEmpty = {abbr: 1, acronym: 1, b: 1, bdi: 1, bdo: 1, big: 1, cite: 1, 
		dfn: 1,  i: 1, ins: 1, label: 1, kbd: 1, mark: 1, meter: 1, output: 1, q: 1,
		ruby: 1, s: 1, samp: 1, small: 1,  strike: 1, 
		span: 0,strong: 0, sub: 0, sup: 0, em: 0, font: 0, code: 0, del: 0,td:0,p:0,
		section:0,blockquote:0,div:0,
		time: 1, tt: 1, u: 1, 'var': 1}
*/


CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'zh-cn';
	// config.uiColor = '#AADC6E';
	
	config.skin = 'office2013';
	config.toolbarCanCollapse = false;
	config.autoUpdateElement = true;
	config.autoGrow_onStartup = false;
	config.resize_enabled = false;
	config.enterMode = CKEDITOR.ENTER_P;
	config.extraPlugins = 'flvPlayer,portlet,ajax,quicktable,autogrow'; //autogrow,safesave,
	config.templates_replaceContent = false;
	config.qtWidth = '100%';
	//removePlugins : 'resize';
	
	config.fillEmptyBlocks = function( element ) {
	    if ( element.name != 'p' )
	        return false;
	};
	config.ignoreEmptyParagraph = false;
	
	config.filebrowserBrowseUrl = ADMIN_BASEURL+'/admin/uploadfiles/filemanage';  
    config.filebrowserImageBrowseUrl = ADMIN_BASEURL+'/admin/uploadfiles/filemanage?path=images';  
    config.filebrowserFlashBrowseUrl = ADMIN_BASEURL+'/admin/uploadfiles/filemanage?path=flashes';  
    config.filebrowserFlvPlayerBrowseUrl = ADMIN_BASEURL+'/admin/uploadfiles/filemanage?path=videos';  
    
    config.filebrowserUploadUrl = ADMIN_BASEURL+'/admin/uploadfiles/upload?no_db=1&no_thumb=1&return=ckeditor';  
    config.filebrowserImageUploadUrl = ADMIN_BASEURL+'/admin/uploadfiles/upload?type=images&no_db=1&no_thumb=1&return=ckeditor';  
    config.filebrowserFlashUploadUrl = ADMIN_BASEURL+'/admin/uploadfiles/upload?type=flashes&no_db=1&no_thumb=1&return=ckeditor';
    config.filebrowserFlvPlayerUploadUrl = ADMIN_BASEURL+'/admin/uploadfiles/upload?type=videos&no_db=1&no_thumb=1&return=ckeditor';
    
    config.filebrowserWindowWidth = '900';  
    config.filebrowserWindowHeight = '600';  
		
	config.toolbar = 'CMS';
	config.toolbar_Full =
	[
	    ['Source','-','Save','NewPage','Preview','-','Templates'],
	    ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
	    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
	    ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
	    '/',
	    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
	    ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
	    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
	    ['BidiLtr', 'BidiRtl'],
	    ['Link','Unlink','Anchor'],
	    ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe'],
	    '/',
	    ['Styles','Format','Font','FontSize'],
	    ['TextColor','BGColor'],
	    ['Maximize', 'ShowBlocks','-','About']
	];

	config.allowedContent = {
	    $1: {
	        // Use the ability to specify elements as an object.
	        elements: CKEDITOR.dtd,
	        attributes: true,
	        styles: true,
	        classes:true,
	    }
	};

	config.disallowedContent = 'script; *[on*]';
	
	
	config.toolbar_CMS =
	[
	    ['Source','-','Bold','Strike','Font','-','TextColor','BGColor','-','Subscript','Superscript'], //'SafeSave',
	    ['NumberedList','BulletedList','-'],
	    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'], 
	    ['Undo','Redo','-','Find','-','Maximize','SelectAll','RemoveFormat'],
	    [],
	    '/',
	    ['Styles','Templates','Format','FontSize'],
	    ['Cut','Copy','Paste','PasteFromWord'],
	    ['Link','Unlink','Anchor'],
	    ['Image','Flash','Portlet','flvPlayer','Table','HorizontalRule','SpecialChar','PageBreak'],	    
	    ['-', 'ShowBlocks','Preview','About']	    
	];
	
};


