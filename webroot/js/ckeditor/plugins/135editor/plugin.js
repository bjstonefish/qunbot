CKEDITOR.plugins.add( '135editor', {
    icons: 'abbr',
    init: function( editor ) {

        //editor.addCommand( 'abbr', new CKEDITOR.dialogCommand( 'abbrDialog' ) );

        /*editor.ui.addButton( 'Abbr', {
            label: 'Insert Abbreviation',
            command: 'abbr',
            toolbar: 'insert'
        });*/
        
        function createDef( def ) {
			return CKEDITOR.tools.extend( def || {}, {
				contextSensitive: 1			
			} );
		}

		editor.addCommand( '_delete_135_region', createDef( {
			exec: function( editor ) {
					
				var element = editor.getSelection().getStartElement();
				
				var _135Obj = $(element.$).parents('.135editor:first');
				
				if ( _135Obj.size() > 0 ) {
					
					var next = _135Obj.next();
					
					if(next.is('p')){
						if(next.html() == '<br>' || next.html() == ' ' ){
							next.remove();
						}	
					}
					var prev = _135Obj.prev();
					if(prev.is('p')){
						if(prev.html() == '<br>' || prev.html() == ' ' ){
							prev.remove();
						}	
					}
					
										
					var _135Element = new CKEDITOR.dom.element( _135Obj[0] );
					var range = editor.createRange();
					range.moveToPosition( _135Element, CKEDITOR.POSITION_BEFORE_START );
					_135Element.remove();
					range.select();
				
					//_135Obj.remove();
				}				
			}
		} ) );
		
		editor.addCommand( '_select_135_region', createDef( {
			exec: function( editor ) {
					
				var element = editor.getSelection().getStartElement();
				
				var _135Obj = $(element.$).parents('.135editor:first');
				
				if ( _135Obj.size() > 0 ) {
										
					var _135Element = new CKEDITOR.dom.element( _135Obj[0] );
					var range = editor.createRange();
					
					range.selectNodeContents(_135Element);
					//range.moveToPosition( _135Element, CKEDITOR.POSITION_BEFORE_START );
					range.select();
				}				
			}
		} ) );
		
		
        
        if ( editor.contextMenu ) {
            editor.addMenuGroup( 'abbrGroup' );
            editor.addMenuItem( 'deleteItem', {
                label: '删除区域',
                icon: this.path + 'icons/abbr.png',
                command: '_delete_135_region',
                group: 'abbrGroup'
            });
            editor.addMenuItem( 'selectItem', {
                label: '选择区域',
                icon: this.path + 'icons/abbr.png',
                command: '_select_135_region',
                group: 'abbrGroup'
            });

            editor.contextMenu.addListener( function( element ) {
                if ( $(element.$).parents('.135editor').size() > 0 ) {
                    return { deleteItem: CKEDITOR.TRISTATE_OFF,selectItem: CKEDITOR.TRISTATE_OFF };
                }
            });
        }

        //CKEDITOR.dialog.add( 'abbrDialog', this.path + 'dialogs/abbr.js' );
    }
});