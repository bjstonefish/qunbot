﻿
/*
 * CKEDITOR.dtd.$removeEmpty = {abbr: 1, acronym: 1, b: 1, bdi: 1, bdo: 1, big: 1, cite: 1, 
		dfn: 1,  i: 1, ins: 1, label: 1, kbd: 1, mark: 1, meter: 1, output: 1, q: 1,
		ruby: 1, s: 1, samp: 1, small: 1,  strike: 1, 
		span: 0,strong: 0, sub: 0, sup: 0, em: 0, font: 0, code: 0, del: 0,
		section:0,blockquote:0,div:0,
		time: 1, tt: 1, u: 1, 'var': 1}
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'zh-cn';
	// config.uiColor = '#AADC6E';
	
	config.skin = 'office2013';
	config.toolbarCanCollapse = false;
	config.autoUpdateElement = true;
	config.autoGrow_onStartup = false;
	config.resize_enabled = false;
	config.enterMode = CKEDITOR.ENTER_P;
	config.templates_replaceContent = false;

	config.fillEmptyBlocks = function( element ) {
	    if ( element.name != 'p' )
	        return false;
	};
	config.ignoreEmptyParagraph = false;

    config.filebrowserUploadUrl = BASEURL+'/uploadfiles/upload?no_db=1&no_thumb=1&return=ckeditor';  
    config.filebrowserImageUploadUrl = BASEURL+'/uploadfiles/upload?type=images&no_db=1&no_thumb=1&return=ckeditor';  
    config.filebrowserFlashUploadUrl = BASEURL+'/uploadfiles/upload?type=flashes&no_db=1&no_thumb=1&return=ckeditor';
    config.filebrowserFlvPlayerUploadUrl = BASEURL+'/uploadfiles/upload?type=videos&no_db=1&no_thumb=1&return=ckeditor';
    
	config.toolbar = 'FRONT';
	//config.height = 500;
	config.toolbar_Full =
	[
	    ['Source','-','Save','NewPage','Preview','-','Templates'],
	    ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
	    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
	    ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
	    '/',
	    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
	    ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
	    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
	    ['BidiLtr', 'BidiRtl'],
	    ['Link','Unlink','Anchor'],
	    ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe'],
	    '/',
	    ['Styles','Format','Font','FontSize'],
	    ['TextColor','BGColor'],
	    ['Maximize', 'ShowBlocks','-','About']
	];
	
	config.toolbar_FRONT =
	[
	    ['FontSize','-','Bold','Italic','Underline','-','TextColor','BGColor','RemoveFormat','-',],
	    ['NumberedList','BulletedList','Blockquote','JustifyLeft','JustifyCenter','-'],
	    ['HorizontalRule'] ,
	    ['Source']	    
	];
	config.extraPlugins = 'textselection,quicktable,135editor'; //lineheight
	
	config.qtWidth = '100%';

	//richcombo 依赖  button,listblock
	// listblock 依赖 panel
	// floatpanel 依赖 panel
	// lineheight 依赖 richcombo
	config.toolbar_ANONY =
	[
	    ['Undo','Redo','Bold','Underline','Subscript','Superscript','TextColor','BGColor','-','RemoveFormat','Find','-',],
	    ['NumberedList','BulletedList','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-'],
	    ['HorizontalRule'] , '/',['-','Font','FontSize','Styles','Table','Image','Maximize','Preview','Source']
	];

	//config.format_p = { element: 'p', styles: { 'text-indent': '2em' } };
//	config.format_p = { element: 'p', attributes: { 'class': 'normalPara' } };

	config.font_names = '宋体;微软雅黑;黑体;楷体;Arial;Times New Roman;';

	config.font_defaultLabel = '微软雅黑';

	config.fontSize_defaultLabel = '14px';

	config.fontSize_sizes ='12/12px;13/13px;14/14px;15/15px;16/16px;18/18px;20/20px;22/22px;24/24px;26/26px;28/28px;36/36px;48/48px;'


	config.allowedContent = {
		img: {
	        attributes: [ 'src', 'alt', 'width', 'height' ],
	        classes: { tip: true },
	    },
	    audio:{
	    	attributes: [ 'src','controls','autoplay','loop'],
	    	style:true,
	    },
	    
	    $1: {
	        // Use the ability to specify elements as an object.
	        elements: CKEDITOR.dtd,
	        attributes: ['data-ct','data-bcless','data-*','placeholder','style'], //ct:color type。 data-ct=fix固定颜色
	        styles: true,
	        classes:true,
	    }
	};

	config.disallowedContent = 'script;link;style; *[on*]';

	
	/*CKEDITOR.stylesSet.add( 'miao_styles', [
	    // Block-level styles
	    { name: 'Blue Title', element: 'h2', styles: { 'color': 'Blue','background-color':'#fff',
	    	'border-left':'5px solid  #666' } },
	    // { name: 'CSS Style', element: 'span', attributes: { 'class': 'my_style' } },
	    { name: 'Marker: Yellow', element: 'span', styles: { 'background-color': 'Yellow' } }
	] );
	config.stylesSet = 'miao_styles';

	
	{
    name: 'Name displayed in the Styles drop-down list',
    element: 'HTML element name (for example "span")',
    styles: {
        'css-style1': 'desired value',
        'css-style2': 'desired value',
        ...
    }
    attributes: {
        'attribute-name1': 'desired value',
        'attribute-name2': 'desired value',
        ...
    }
}
	*/
	
};


