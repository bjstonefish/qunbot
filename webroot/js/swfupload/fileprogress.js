/*
	A simple class for displaying file information and progress
	Note: This is a demonstration only and not part of SWFUpload.
	Note: Some have had problems adapting this class in IE7. It may not be suitable for your application.
*/

// Constructor
// file is a SWFUpload file object
// targetID is the HTML element id attribute that the FileProgress HTML structure will be added to.
// Instantiating a new FileProgress object with an existing file will reuse/update the existing DOM elements

function FileProgress(file, targetID) {	
	this.fileProgressID = file.id;
	this.opacity = 100;	
	this.fileProgressWrapper = $('#'+this.fileProgressID,top.document);	
	if (!this.fileProgressWrapper.size()>0) {
		var wrapper_html = '<div id='+this.fileProgressID+' class="progressWrapper"><div class="progressContainer">'+
			'<div class="progress upload-progress nomb">'+
			'<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:0%;">'+
			'</div></div>&nbsp;<span class="progressName">&nbsp;'+ file.name + '</span> <i class="glyphicon glyphicon-remove"></i>'+
		'</div></div>';	
		
		this.fileProgressWrapper = $(wrapper_html);
		this.progressBar = this.fileProgressWrapper.find('.progress-bar');
		this.fileProgressElement = this.fileProgressWrapper.find('.progressContainer');
		//document.getElementById(targetID).appendChild(this.fileProgressWrapper);
		$('#'+targetID,top.document).append(this.fileProgressWrapper);		
	} else {
		this.fileProgressElement =  this.fileProgressWrapper.find('.progressContainer');	
		this.fileProgressWrapper.find('.progressName').html(file.name);
	}
	
}
FileProgress.prototype.setProgress = function (percentage) {
	this.fileProgressElement.removeClass();
	this.fileProgressElement.addClass("progressContainer green");
	this.progressBar.width(percentage+'%');
};
FileProgress.prototype.setComplete = function () {
	this.appear();	
	this.fileProgressElement.removeClass();
	this.fileProgressElement.addClass("progressContainer ui-state-highlight");
	var oSelf = this;
	
	this.fileProgressWrapper.find('.glyphicon-remove').removeClass('glyphicon-remove').addClass('glyphicon-ok').unbind('click');
	
	setTimeout(function () {
		oSelf.disappear();
	}, 3000);
};
FileProgress.prototype.setError = function () {
	
	this.fileProgressElement.removeClass();
	this.fileProgressElement.addClass("progressContainer red");
	var oSelf = this;	
	this.appear();	
	setTimeout(function () {
		oSelf.disappear();
	}, 2000);
};
FileProgress.prototype.setCancelled = function () {
	this.fileProgressElement.removeClass();
	this.fileProgressElement.addClass("progressContainer");
	var oSelf = this;	
	this.appear();
	
	setTimeout(function () {
		oSelf.disappear();
	}, 2000);
};

// Show/Hide the cancel button
FileProgress.prototype.toggleCancel = function (show, swfUploadInstance) {
	if(show){
		this.fileProgressWrapper.find('.glyphicon-remove').show();
	}
	else{
		this.fileProgressWrapper.find('.glyphicon-remove').hide();
	}
	var oSelf = this;	
	//this.fileProgressElement.childNodes[0].style.visibility = show ? "visible" : "hidden";
	if (swfUploadInstance) {
		var fileID = this.fileProgressID;
		this.fileProgressWrapper.find('.glyphicon-remove').click(function () {
			swfUploadInstance.cancelUpload(fileID,true);
			setTimeout(function () {
				oSelf.disappear();
			}, 3000);
			return false;
		});
	}
};

// Makes sure the FileProgress box is visible
FileProgress.prototype.appear = function () {
	this.fileProgressWrapper.fadeIn('fast');
};

// Fades out and clips away the FileProgress box.
FileProgress.prototype.disappear = function () {
	this.fileProgressWrapper.fadeOut(2000);
};