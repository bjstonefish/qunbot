(function ($, window) {
	$.fn.extend({
		colorPicker: function(config) {
			var renderCallback = function(colors, mode) {
				
					
					var options = this,
						$input = $(options.input),
						$patch = $(options.patch),
						RGB = colors.RND.rgb,
						HSL = colors.RND.hsl,
						AHEX = options.isIE8 ? (colors.alpha < 0.16 ? '0' : '') +
							(Math.round(colors.alpha * 100)).toString(16).toUpperCase() + colors.HEX : '',
						RGBInnerText = RGB.r + ', ' + RGB.g + ', ' + RGB.b,
						RGBAText = 'rgba(' + RGBInnerText + ', ' + colors.alpha + ')',
						isAlpha = colors.alpha !== 1 && !options.isIE8,
						colorMode = $input.data('colorMode');

					$patch.css({
						'color': (colors.rgbaMixCustom.luminance > 0.22 ? '#222' : '#ddd'), // Black...???
						'background-color': RGBAText,
						'filter' : (options.isIE8 ? 'progid:DXImageTransform.Microsoft.gradient(' + // IE<9
							'startColorstr=#' + AHEX + ',' + 'endColorstr=#' + AHEX + ')' : '')
					});

					$input.val(colorMode === 'HEX' && !isAlpha ? '#' + (options.isIE8 ? AHEX : colors.HEX) :
						colorMode === 'rgb' || (colorMode === 'HEX' && isAlpha) ?
						(!isAlpha ? 'rgb(' + RGBInnerText + ')' : RGBAText) :
						('hsl' + (isAlpha ? 'a(' : '(') + HSL.h + ', ' + HSL.s + '%, ' + HSL.l + '%' +
							(isAlpha ? ', ' + colors.alpha : '') + ')')
					);
					
					// console.log( $input.val() +' ' + mode);
					
					/*if( current_active_135item ) {
						var old_value =  current_active_135item.attr('data-color');
//						console.log(old_value+' : '+ $input.val() + "\n");
						if( strtolower(old_value) != strtolower($input.val()) ){
							current_active_135item.attr('data-color',$input.val()); //浮层弹出菜单中选中颜色
							setBackgroundColor($input.val(),'#FFFFFF', false);
							current_active_135item.attr('data-custom',true);
						}
						
					}
					else */
					
					if (options.displayCallback) {
						options.displayCallback(colors, mode, options);
					}
				},
				actionCallback = function(event, action) {
					
					var options = this,
						$input = $(options.input),
						colorPicker = colorPickers.current;
					
					//alert(typeof colorPicker); //alert(typeof colorPicker.color);
					/*	var str = "",
						opt = event ;
						for(var i in opt ){
							str += i + "="+opt[i]+"\n";
						}
						console.log(action + "  e:"+ str);*/
					
					//console.log( action + " "+ options.color );
					if( $.inArray(action, ["changeXYValue","changeZValue","changeInputValue","shiftColor","setSavedColor","changeOpacityValue"]) != -1 ) {
						// 仅选色变化时才修改设置颜色，renderCallback中调用频率太高，影响性能
						var current_active_135item = typeof( current_editor) != 'undefined'  ?  current_editor.currentActive135Item() : null;
						if( (typeof(replace_full_color) != 'undefined' && replace_full_color) || typeof(current_active_135item) != 'undefined' && current_active_135item) {
							setBackgroundColor($input.val(),'#FFFFFF', false);
						}
						
						if(typeof(current_active_135item) != 'undefined' && ! current_active_135item ) {
							$('.editor-template-list li > .135editor').each(function(){
								parseObject($(this),$input.val(),'#FFFFFF'); //全文替换为对应颜色
							})
						}
					}
					else if(action == 'saveAsBackground') { //点击save按钮，将颜色应用与收藏保存
						
					}
					else if (action === 'toMemery') {
						var memos = colorPicker.nodes.memos,
							$memo,
							backgroundColor = '',
							opacity = 0,
							cookieTXT = [];

						for (var n = 0, m = memos.length; n < m; n++) {
							$memo = $(memos[n]);
							backgroundColor = $memo.css('background-color');
							opacity = Math.round($memo.css('opacity') * 100) / 100;
							cookieTXT.push(backgroundColor.
								replace(/, /g, ',').
								replace('rgb(', 'rgba(').
								replace(')', ',' + opacity + ')')
							);
						}
						cookieTXT = '\'' + cookieTXT.join('\',\'') + '\'';
						$.docCookies('colorPickerMemos' + (options.noAlpha ? 'NoAlpha' : ''), cookieTXT);
					} else if (action === 'resizeApp') {
						$.docCookies('colorPickerSize', colorPicker.color.options.currentSize);
					} else if (action === 'modeChange') {
						var mode = colorPicker.color.options.mode;

						$.docCookies('colorPickerMode', mode.type + '-' + mode.z);
					}
				},
				createInstance = function(elm, config) {
					var initConfig = {
							klass: window.ColorPicker,
							input: elm,
							patch: elm,
							isIE8: !!document.all && !document.addEventListener, // Opera???
							animationSpeed: 200,
							draggable: true,
							margin: {left: -1, top: 2},
							customBG: '#FFFFFF',
							// displayCallback: displayCallback,
							/* --- regular colorPicker options from this point --- */
							color: elm.value,
							initStyle: 'display: none',
							mode: $.docCookies('colorPickerMode') || 'hsv-h',
							// memoryColors: (function(colors, config) {
							// 	return config.noAlpha ?
							// 		colors.replace(/\,\d*\.*\d*\)/g, ',1)') : colors;
							// })($.docCookies('colorPickerMemos'), config || {}),
							memoryColors: $.docCookies('colorPickerMemos' + ((config || {}).noAlpha ? 'NoAlpha' : '')),
							size: $.docCookies('colorPickerSize') || 1,
							renderCallback: renderCallback,
							actionCallback: actionCallback
						};

					for (var n in config) {
						initConfig[n] = config[n]; 
					}
					return new initConfig.klass(initConfig);
				},
				doEventListeners = function(elm, multiple, off) {
					var onOff = off ? 'off' : 'on';
					
					$(elm)[onOff]('focus.colorPicker', function(e) {
						
						var $input = $(this),
							position = $input.position(), 
							index = multiple ? $(that).index(this) : 0,
							colorPicker = colorPickers[index] ||
								(colorPickers[index] = createInstance(this, config)),
							options = colorPicker.color.options,
							$colorPicker = $.ui && options.draggable ?
							$(colorPicker.nodes.colorPicker).draggable(
								{cancel: '.' + options.CSSPrefix + 'app div'}
							) : $(colorPicker.nodes.colorPicker);

						options.color = elm.value; // brings color to default on reset
						//alert(position.left);alert(position.top);
						var left = position.left,top = position.top;
						if($($input).parents('#color-plan').size() > 0) { // 调色板中
							left += $('#color-plan').offset().left - 260;
							top += $('#color-plan').offset().top + 20 + $(window).scrollTop();
						}
						else if($($input).parents('.edui-popup').size() > 0 ){ //浮层工具栏
							var p_popup = $($input).parents('.edui-popup:first');
							left += p_popup.position().left - 165;
							top += p_popup.position().top - 150;
						}
						
						$colorPicker.css({
							'position': 'absolute','z-index':10000,
							'left': left + options.margin.left,
							'top': top + $input.outerHeight(true) + options.margin.top
						});
						if (!multiple) {
							options.input = elm;
							options.patch = elm;
							if(elm.value == "") {
//								colorPicker.setColor("#FFFFFF", undefined, undefined, true);
//								colorPicker.saveAsBackground();
							}
							else {
								colorPicker.setColor(elm.value, undefined, undefined, true);
								colorPicker.saveAsBackground();
							}
						}
						
						colorPickers.current = colorPickers[index];
//						$(options.appenTo || document.body).append($colorPicker);
						if(options.appenTo) {
							$(options.appenTo).append($colorPicker);
						}
						else{
							$($input).after($colorPicker);//当相对偏移时，在body中，相对父级计算的left,top不正确.直接跟在input后面
						}
						setTimeout(function() { // compensating late style on onload in colorPicker
							$colorPicker.show(colorPicker.color.options.animationSpeed);
						}, 0);
					});

					if (!colorPickers.evt || off) {
						colorPickers.evt = true; // prevent new eventListener for window

						$(window)[onOff]('mousedown.colorPicker', function(e) {
							var colorPicker = colorPickers.current,
								$colorPicker = $(colorPicker ? colorPicker.nodes.colorPicker : undefined),
								animationSpeed = colorPicker ? colorPicker.color.options.animationSpeed : 0,
								isColorPicker = $(e.target).closest('.cp-app')[0],
								inputIndex = $(that).index(e.target);

							if (isColorPicker && $(colorPickers).index(isColorPicker)) {
								if (colorPicker && e.target === colorPicker.nodes.exit) {
									$colorPicker.hide(animationSpeed);
									$(':focus').trigger('blur');
								} else {
									// buttons on colorPicker don't work any more
									// $(document.body).append(isColorPicker);
								}
							} else if (inputIndex !== -1) {
								// input fireld
							} else {
								$colorPicker.hide(animationSpeed);
							}
						});
					}
				},
				that = this,
				colorPickers = this.colorPickers || [], // this is a way to prevent data binding on HTMLElements
				testColors = new window.Colors({customBG: config.customBG, allMixDetails: true});

			this.colorPickers = colorPickers;

			$(this).each(function(idx, elm) {
				if (config === 'destroy') {
					// doEventListeners(elm, (config && config.multipleInstances), true);
					$(elm).off('.colorPicker');
					$(window).off('.colorPicker');
					if (colorPickers[idx]) {
						colorPickers[idx].destroyAll();
					}
				} else {
					var color = elm.value; //$(elm).data('color') ? $(elm).data('color'): 
					var value = color.split('(');
					
					$(elm).data('colorMode', value[1] ? value[0].substr(0, 3) : 'HEX');
					doEventListeners(elm, (config && config.multipleInstances), false);
					if (config && config.readOnly) {
						elm.readOnly = true;
					}
					if(color == "") {
//						testColors.setColor("#FFFFFF");
					}
					else{
						testColors.setColor(color);
					}
					if (config && config.init) {
						config.init(elm, testColors.colors);
					}
				}
			});

			return this;
		}
	});

	$.docCookies = function(key, val, options) {
		var encode = encodeURIComponent, decode = decodeURIComponent,
			cookies, n, tmp, cache = {},
			days;

		if (val === undefined) { // all about reading cookies
			cookies = document.cookie.split('; ') || []; // easier for decoding then with RegExp search // .split(/;\s*/)
			for (n = cookies.length; n--; ) {
				tmp = cookies[n].split('=');
				if (tmp[0]) cache[decode(tmp.shift())] = decode(tmp.join('=')); // there might be '='s in the value...
			}

			if (!key) return cache; // return Json for easy access to all cookies
			else return cache[key]; // easy access to cookies from here
		} else { // write/delete cookie
			options = options || {};

			if (val === '' || options.expires < 0) { // prepare deleteing the cookie
				options.expires = -1;
				// options.path = options.domain = options.secure = undefined; // to make shure the cookie gets deleted...
			}

			if (options.expires !== undefined) { // prepare date if any
				days = new Date();
				days.setDate(days.getDate() + options.expires);
			}

			document.cookie = encode(key) + '=' + encode(val) +
				(days            ? '; expires=' + days.toUTCString() : '') +
				(options.path    ? '; path='    + options.path       : '') +
				(options.domain  ? '; domain='  + options.domain     : '') +
				(options.secure  ? '; secure'                        : '');
		}
	};
})(jQuery, this); 