if (!window.jquitr) {
	var jquitr = {};
}

$(function () {
	//add dev tool to page
	jquitr.addThemeRoller = function(url){
		if($('#inline_themeroller').size() > 0){
			$('#inline_themeroller').fadeIn();
		}
		else {
			$('<div id="inline_themeroller" class="panel panel-default" style="display: none; position: fixed; top: 25px; right: 25px;width: 680px;height:580px;z-index: 1000;">'+
				'<div class="panel-heading" style="cursor:move;">'+
				'<span class="closeTR" style="float: right;cursor:pointer;"><i class="fa fa-remove"></i>Close</span>'+
				'<h4>编辑样式------<span>MiaoCMS ThemeRoller</span></h4>'+
				'</div>' + 
				'<div id="themeroller-pannel" class="panel-body" style="height:450px;overflow-y:auto;overflow-x:hidden;padding:8px; border: 0;"></div>'+
				'<div class="modal-footer" style="padding:5px">'+
		        '<button type="button" class="btn btn-default" id="close-themeroller">Close</button>'+
		        '<button type="button" class="btn btn-warning" id="save-themeroller">Save</button>'+
		        //'<button type="button" class="btn btn-warning" id="save-new-themeroller">保存新的样式</button>'+
		        '<button type="button" class="btn btn-primary" id="download-variables">Download variable</button>'+
		        '<button type="button" class="btn btn-primary" id="download-themeroller">Download CSS</button>'+
		      '</div>'+
			'</div>')
				.appendTo('body');
			$('#themeroller-pannel').load(url);
			
				$('#inline_themeroller').draggable({
					handle: ".panel-heading",
					start: function(){
						$('<div id="div_cover" />').appendTo('#inline_themeroller').css({width: $(this).width(), height: $(this).height(), position: 'absolute', top: 0, left:0});
					},
					stop: function(){
						$('#div_cover').remove();
					},
					opacity: 0.6,
					cursor: 'move'
				}).find('.panel-heading .closeTR,#close-themeroller').click(function(){
					jquitr.closeThemeRoller();
				})
				.end().find('#download-variables').click(function(){
					jquitr.downloadVariable();
				})
				.end().find('#download-themeroller').click(function(){
					jquitr.downloadTheme();
				})
				.end().find('#save-themeroller').click(function(){
					jquitr.saveThemeRoller();
				}).end().fadeIn();
		}
	};
	jquitr.downloadVariable = function () {
		var form = $('#themeroller-form');
		form.attr('action', ADMIN_BASEURL+'/admin/stylevars/get_variable');
		//$('#inline_themeroller').find('#themeroller-submit-type').val('download');
		$('#inline_themeroller').find('form:first').get(0).submit();
		$('#inline_themeroller').find('#themeroller-submit-type').val();	
		form.attr('action', ADMIN_BASEURL+'/admin/stylevars/getcss');	
	};
	jquitr.downloadTheme = function () {
		$('#inline_themeroller').find('#themeroller-submit-type').val('download');
		$('#inline_themeroller').find('form:first').get(0).submit();
		$('#inline_themeroller').find('#themeroller-submit-type').val();		
	};
	jquitr.saveThemeRoller = function () {
		var form = $('#inline_themeroller').find('form:first');
		$.ajax({
			type:'post',
			url: ADMIN_BASEURL+'/admin/stylevars/savetheme',
			data: form.serialize(),
			success:function(data){
				alert(data.success);
			},
			dataType:'json'
		});
	};
	//close dev tool
	jquitr.closeThemeRoller = function () {
		$('#inline_themeroller').fadeOut();
	};
	//get current url hash
	jquitr.getHash = function () {
		var currSrc = window.location.hash;
		if (currSrc.indexOf('#') > -1) {
			currSrc = currSrc.split('#')[1];
		}
		return currSrc;
	};
	
	// Actually add the roller
	
});