var random_color_step = 1;
var custom_style_set = false;
var current_edit_img = null;
var allow_upload_img = null;
var current_edit_msg_id = '';

var current_editor;

var replace_full_color = false; // 全文换色标记,默认开启 //默认不开启全文换色
var replace_clear_interval = null;

function strip_stack_span(html){ // 将层叠的上下级span标签合并，同时合并两者的style属性
	var docObj = jQuery('<div>'+html+'</div>');
	
	docObj.find('li,colgroup,a').each(function(){
		if( jQuery.trim( jQuery(this).text() ) == "" && jQuery(this).find('img').size()==0 ) { //无文字内容，且不含图片
			jQuery(this).remove();
		}
	});
	
	var has_secspan = false;
	do
	{
		has_secspan = false;
		
		docObj.find('span:has(span)').each(function(i){
			
    		var innerobj = jQuery(this).find('> span'); //只处理第一层级包含的，包含其余内部层级的不处理(如strong)
    		if(innerobj.size() > 1) { // 为0或者超过1个时，不处理
    			jQuery(this).find('span').each(function(){
    				if( jQuery.trim(jQuery(this).text())=="" ) { // 内部无文字内容的span，替换成内部代码内容（br,img）
    					jQuery(this).replaceWith(jQuery(this).html());
    				}
    			});
    			return ; // 内部多余1个的不处理
    		}
    		else if(innerobj.size() == 0 ){
    			return ; 
    		}
    		
    		if( jQuery.trim(jQuery(this).text()) == jQuery.trim(innerobj.text()) ) {
    			has_secspan = true;
    			var style= jQuery(this).attr('style');
    			var innserstyle = innerobj.attr('style');
    			var newStyle = '';
    			if(style  && style != "") {
    				newStyle += ';'+style;
    			}
    			if(innserstyle && innserstyle != "") {
    				newStyle += ';'+innserstyle;
    			}
    			
    			var new_html = '';
    			jQuery(this).find('> *').each(function(){ //保留可能除span外的其它无text文字的标签
    				if( this.tagName == "SPAN" ) {
    					new_html += jQuery(innerobj).html();
    				}
    				else{
    					new_html += jQuery(this).prop('outerHTML');
    				}    				
    			});
    			jQuery(this).attr('style',newStyle).html(new_html);
    			//jQuery(this).attr('style',newStyle).html(jQuery(innerobj).html());
    		}
    	});
		
	}while(has_secspan); // && docObj.find('span:has(span)').size() > 0
	
	return docObj.html();
}

/*function strip_tags(html){
	var pasteObj = jQuery(html);
	//pasteObj.find('br').removeAttr('style').removeAttr('class');
	if(pasteObj.find('*').size() > 1){
		pasteObj.find('br,div,p').after("===BR===");  //纯文本，仅保留换行
	}
	
	var text = pasteObj.text();
	text = text.replace(/\=\=\=BR\=\=\=/g, "<br>");
	return text;
}*/
function strip_tags(input, allowed) {
	  //  discuss at: http://phpjs.org/functions/strip_tags/
	  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // improved by: Luke Godfrey
	  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  //    input by: Pul
	  //    input by: Alex
	  //    input by: Marc Palau
	  //    input by: Brett Zamir (http://brett-zamir.me)
	  //    input by: Bobby Drake
	  //    input by: Evertjan Garretsen
	  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // bugfixed by: Onno Marsman
	  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // bugfixed by: Eric Nagel
	  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // bugfixed by: Tomasz Wesolowski
	  //  revised by: Rafał Kukawski (http://blog.kukawski.pl/)
	  //   example 1: strip_tags('<p>Kevin</p> <br /><b>van</b> <i>Zonneveld</i>', '<i><b>');
	  //   returns 1: 'Kevin <b>van</b> <i>Zonneveld</i>'
	  //   example 2: strip_tags('<p>Kevin <img src="someimage.png" onmouseover="someFunction()">van <i>Zonneveld</i></p>', '<p>');
	  //   returns 2: '<p>Kevin van Zonneveld</p>'
	  //   example 3: strip_tags("<a href='http://kevin.vanzonneveld.net'>Kevin van Zonneveld</a>", "<a>");
	  //   returns 3: "<a href='http://kevin.vanzonneveld.net'>Kevin van Zonneveld</a>"
	  //   example 4: strip_tags('1 < 5 5 > 1');
	  //   returns 4: '1 < 5 5 > 1'
	  //   example 5: strip_tags('1 <br/> 1');
	  //   returns 5: '1  1'
	  //   example 6: strip_tags('1 <br/> 1', '<br>');
	  //   returns 6: '1 <br/> 1'
	  //   example 7: strip_tags('1 <br/> 1', '<br><br/>');
	  //   returns 7: '1 <br/> 1'

	  allowed = (((allowed || '') + '')
	    .toLowerCase()
	    .match(/<[a-z][a-z0-9]*>/g) || [])
	    .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
	  var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
	    commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
	  return input.replace(commentsAndPhpTags, '')
	    .replace(tags, function($0, $1) {
	      return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
	    });
}

function htmlspecialchars_decode(string, quote_style) {
	  //       discuss at: http://phpjs.org/functions/htmlspecialchars_decode/
	  //      original by: Mirek Slugen
	  //      improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  //      bugfixed by: Mateusz "loonquawl" Zalega
	  //      bugfixed by: Onno Marsman
	  //      bugfixed by: Brett Zamir (http://brett-zamir.me)
	  //      bugfixed by: Brett Zamir (http://brett-zamir.me)
	  //         input by: ReverseSyntax
	  //         input by: Slawomir Kaniecki
	  //         input by: Scott Cariss
	  //         input by: Francois
	  //         input by: Ratheous
	  //         input by: Mailfaker (http://www.weedem.fr/)
	  //       revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // reimplemented by: Brett Zamir (http://brett-zamir.me)
	  //        example 1: htmlspecialchars_decode("<p>this -&gt; &quot;</p>", 'ENT_NOQUOTES');
	  //        returns 1: '<p>this -> &quot;</p>'
	  //        example 2: htmlspecialchars_decode("&amp;quot;");
	  //        returns 2: '&quot;'

	  var optTemp = 0,
	    i = 0,
	    noquotes = false;
	  if (typeof quote_style === 'undefined') {
	    quote_style = 2;
	  }
	  string = string.toString()
	    .replace(/&lt;/g, '<')
	    .replace(/&gt;/g, '>');
	  var OPTS = {
	    'ENT_NOQUOTES'          : 0,
	    'ENT_HTML_QUOTE_SINGLE' : 1,
	    'ENT_HTML_QUOTE_DOUBLE' : 2,
	    'ENT_COMPAT'            : 2,
	    'ENT_QUOTES'            : 3,
	    'ENT_IGNORE'            : 4
	  };
	  if (quote_style === 0) {
	    noquotes = true;
	  }
	  if (typeof quote_style !== 'number') {
	    // Allow for a single string or an array of string flags
	    quote_style = [].concat(quote_style);
	    for (i = 0; i < quote_style.length; i++) {
	      // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
	      if (OPTS[quote_style[i]] === 0) {
	        noquotes = true;
	      } else if (OPTS[quote_style[i]]) {
	        optTemp = optTemp | OPTS[quote_style[i]];
	      }
	    }
	    quote_style = optTemp;
	  }
	  if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
	    string = string.replace(/&#0*39;/g, "'"); // PHP doesn't currently escape if more than one 0, but it should
	    // string = string.replace(/&apos;|&#x0*27;/g, "'"); // This would also be useful here, but not a part of PHP
	  }
	  if (!noquotes) {
	    string = string.replace(/&quot;/g, '"');
	  }
	  // Put this in last place to avoid escape being double-decoded
	  string = string.replace(/&amp;/g, '&');

	  return string;
}
function strip_imgthumb_opr(imgurl){
	var idx = imgurl.indexOf('@');
	if(idx > 0) {
		imgurl = imgurl.substring(0,idx); // 返回从start到end的位置，不包含end的那个字母
	}
	
	var idy = imgurl.indexOf('?');
	if(idy > 0) {
		imgurl = imgurl.substring(0,idy); // 返回从start到end的位置，不包含end的那个字母
	}
	return imgurl;
}

function base64_decode (data) {
	  //  discuss at: http://phpjs.org/functions/base64_decode/
	  // original by: Tyler Akins (http://rumkin.com)
	  // improved by: Thunder.m
	  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  //    input by: Aman Gupta
	  //    input by: Brett Zamir (http://brett-zamir.me)
	  // bugfixed by: Onno Marsman
	  // bugfixed by: Pellentesque Malesuada
	  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  //   example 1: base64_decode('S2V2aW4gdmFuIFpvbm5ldmVsZA==');
	  //   returns 1: 'Kevin van Zonneveld'
	  //   example 2: base64_decode('YQ===');
	  //   returns 2: 'a'
	  //   example 3: base64_decode('4pyTIMOgIGxhIG1vZGU=');
	  //   returns 3: '✓ à la mode'

	  var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
	  var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
	    ac = 0,
	    dec = '',
	    tmp_arr = [];

	  if (!data) {
	    return data;
	  }

	  data += '';

	  do {
	    // unpack four hexets into three octets using index points in b64
	    h1 = b64.indexOf(data.charAt(i++));
	    h2 = b64.indexOf(data.charAt(i++));
	    h3 = b64.indexOf(data.charAt(i++));
	    h4 = b64.indexOf(data.charAt(i++));

	    bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;

	    o1 = bits >> 16 & 0xff;
	    o2 = bits >> 8 & 0xff;
	    o3 = bits & 0xff;

	    if (h3 == 64) {
	      tmp_arr[ac++] = String.fromCharCode(o1);
	    } else if (h4 == 64) {
	      tmp_arr[ac++] = String.fromCharCode(o1, o2);
	    } else {
	      tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
	    }
	  } while (i < data.length);

	  dec = tmp_arr.join('');

	  return decodeURIComponent(escape(dec.replace(/\0+$/, '')));
	}

function base64_encode (data) {
	  //  discuss at: http://phpjs.org/functions/base64_encode/
	  // original by: Tyler Akins (http://rumkin.com)
	  // improved by: Bayron Guevara
	  // improved by: Thunder.m
	  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // improved by: Rafał Kukawski (http://blog.kukawski.pl)
	  // bugfixed by: Pellentesque Malesuada
	  //   example 1: base64_encode('Kevin van Zonneveld');
	  //   returns 1: 'S2V2aW4gdmFuIFpvbm5ldmVsZA=='
	  //   example 2: base64_encode('a');
	  //   returns 2: 'YQ=='
	  //   example 3: base64_encode('✓ à la mode');
	  //   returns 3: '4pyTIMOgIGxhIG1vZGU='

	  var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
	  var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
	    ac = 0,
	    enc = '',
	    tmp_arr = [];

	  if (!data) {
	    return data;
	  }

	  data = unescape(encodeURIComponent(data));

	  do {
	    // pack three octets into four hexets
	    o1 = data.charCodeAt(i++);
	    o2 = data.charCodeAt(i++);
	    o3 = data.charCodeAt(i++);

	    bits = o1 << 16 | o2 << 8 | o3;

	    h1 = bits >> 18 & 0x3f;
	    h2 = bits >> 12 & 0x3f;
	    h3 = bits >> 6 & 0x3f;
	    h4 = bits & 0x3f;

	    // use hexets to index into b64, and append result to encoded string
	    tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
	  } while (i < data.length);

	  enc = tmp_arr.join('');

	  var r = data.length % 3;

	  return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
	}

function str_replace(search, replace, subject, count) {
	  //  discuss at: http://phpjs.org/functions/str_replace/
	  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // improved by: Gabriel Paderni
	  // improved by: Philip Peterson
	  // improved by: Simon Willison (http://simonwillison.net)
	  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // improved by: Onno Marsman
	  // improved by: Brett Zamir (http://brett-zamir.me)
	  //  revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
	  // bugfixed by: Anton Ongson
	  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // bugfixed by: Oleg Eremeev
	  //    input by: Onno Marsman
	  //    input by: Brett Zamir (http://brett-zamir.me)
	  //    input by: Oleg Eremeev
	  //        note: The count parameter must be passed as a string in order
	  //        note: to find a global variable in which the result will be given
	  //   example 1: str_replace(' ', '.', 'Kevin van Zonneveld');
	  //   returns 1: 'Kevin.van.Zonneveld'
	  //   example 2: str_replace(['{name}', 'l'], ['hello', 'm'], '{name}, lars');
	  //   returns 2: 'hemmo, mars'

	  var i = 0,
	    j = 0,
	    temp = '',
	    repl = '',
	    sl = 0,
	    fl = 0,
	    f = [].concat(search),
	    r = [].concat(replace),
	    s = subject,
	    ra = Object.prototype.toString.call(r) === '[object Array]',
	    sa = Object.prototype.toString.call(s) === '[object Array]';
	  s = [].concat(s);
	  if (count) {
	    this.window[count] = 0;
	  }

	  for (i = 0, sl = s.length; i < sl; i++) {
	    if (s[i] === '') {
	      continue;
	    }
	    for (j = 0, fl = f.length; j < fl; j++) {
	      temp = s[i] + '';
	      repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
	      s[i] = (temp)
	        .split(f[j])
	        .join(repl);
	      if (count && s[i] !== temp) {
	        this.window[count] += (temp.length - s[i].length) / f[j].length;
	      }
	    }
	  }
	  return sa ? s : s[0];
	}

function getSelectionHtml(outer){
	var range = current_editor.selection.getRange();
//	当触发浮动菜单中的选中时，range的startContainer与endContainer发生错误，与body相同
   if(range.startContainer.tagName == 'BODY' && range.startContainer === range.endContainer && range.endOffset > 0 &&  range.endOffset === range.startContainer.childNodes.length ) { //全选时返回整个编辑区的内容。
       return getEditorHtml(outer);
   }
   else{
    	range.select();
		var selectionObj = current_editor.selection.getNative();
		var rangeObj = selectionObj.getRangeAt(0);
		var docFragment = rangeObj.cloneContents();
		//　　然后将docFragment渲染出来，获取其innerHTML即可。
		var testDiv = document.createElement("div");
		testDiv.appendChild(docFragment);
		var selectHtml = testDiv.innerHTML;
		if( selectHtml  == "" ) {
			return "";
		}
		else{
			return parse135EditorHtml(selectHtml);
		}
   }
}
function getDealingHtml(){	
	 var html = getSelectionHtml(true);	
	 if( html  == "" ) {
		 if(  current_editor.currentActive135Item() ) {
			 return jQuery( current_editor.currentActive135Item()).html();
		 }
		 else {
			 return getEditorHtml(true);
		 }
	 }
	 else{
		 return html;
	 }
}


function setDealingHtml(newHtml){
	
	newHtml = jQuery.trim(newHtml); //newHtml = str_replace("&#8203;","",newHtml); alert(newHtml);
	
	var html = getSelectionHtml();	 
	if(html != ""){			
		//current_editor.execCommand('insertHtml', newHtml);
		insertHtml(newHtml);
		custom_style_set = true;
		current_editor.undoManger.save();
		return;
	}
	else if( current_editor.currentActive135Item()){		
		  jQuery( current_editor.currentActive135Item()).html(newHtml);
		  current_editor.undoManger.save();
		  return;
	}
	else{
		current_editor.setContent(newHtml);
		current_editor.undoManger.save();
		return ;
	}
}

/** 整篇文章的复制会对style样式做一些兼容处理，避免不问站点各自css文件定义的影响，尽可能让复制到不同网址都能正常效果保持一致 **/

function parse135EditorHtml(html,outer_falg) {
	//html = parseMmbizUrl(html);
	var htmlObj = jQuery('<div>' + html + ' </div>');
	
	htmlObj.find('*').each(function(){
		if(this.style.transform) {
			setElementTransform(this,this.style.transform);
			/* 旋转节点的style其它处理直接跳过，加上后会使复制粘贴到微信里时-webkit-transform丢失 */
			return;
		}
		if(  this.tagName == "SECTION" ) {
			// border-radius:0 100% 100% 0/50%; 复杂的样式直接变了。尽量不要调用style属性（border-radius变化，background变化，transform变化等）
			// jQuery(this).css('box-sizing','border-box'); 不处理box-sizing，修改的对象多，某些style复杂属性会错误。
			var style = jQuery(this).attr('style'); 
			if( style ){
				style = style.toLowerCase();
				if(style.indexOf('box-sizing') >= 0) {
					return;
				}
				else if( style.indexOf('padding') >= 0 || style.indexOf('border') >= 0 ){
					jQuery(this).css('box-sizing','border-box');
				}
			}
		}
		else if(this.tagName == "IMG" || this.tagName == "BR" || this.tagName == "TSPAN" || this.tagName == "TEXT" || this.tagName == "IMAGE" ) {
			return;
		}
		else if(  this.tagName == "STRONG" || this.tagName == "SPAN"
			|| this.tagName == "B" 
			|| this.tagName == "EM"  || this.tagName == "I"  ) {
			// if(!this.style.fontSize){ 	jQuery(this).css({'fontSize','14px'});
			return;
		}
		else if(  this.tagName == "P") {			
			// jQuery(this).css('white-space','normal');
			return;
		}
		else if(this.tagName == "H1" || this.tagName == "H2" || this.tagName == "H3" || this.tagName == "H4" || this.tagName == "H5" || this.tagName == "H6") {
			jQuery(this).css('font-weight','bold');
			if(!this.style.fontSize){
				// this.style.fontSize = '32px';	
				jQuery(this).css({'font-size':'16px'});
			}
			if(!this.style.lineHeight){ //this.style.lineHeight = '36px';	
				jQuery(this).css({'lineHeight':'1.6em'});
			}
			return;
		}
		
		else if(  this.tagName == "OL" || this.tagName == "UL" || this.tagName == "DL" ) {
			jQuery(this).css({'margin':'0px','padding':'0 0 0 30px'});
			return;
		}
		//if( !this.style.fontSize ) { 	this.style.fontSize = '14px';	}
		
		if( (this.tagName == "TD" || this.tagName == "TH") && this.style.padding == "" && this.style.paddingLeft == ""  && this.style.paddingRight == ""  && this.style.paddingTop == ""  && this.style.paddingBottom == "" ) {
				jQuery(this).css({'margin':'5px 10px'});
		}
	});
	
	var html =  jQuery.trim( htmlObj.html() ) ;
	if( html == "" ) {
		return "";
	}
	//html =  htmlObj.prop("innerHTML");
	return html;
}

function setElementTransform(dom,transform){
	if(transform =="none") return;
	var sty = jQuery(dom).attr('style');
	sty = sty.replace(/;\s*transform\s*:[A-Za-z0-9_%,.\-\(\)\s]*;/gim,';');
	sty = sty.replace(/\s*\-[a-z]+\-transform\s*:[A-Za-z0-9_%,.\-\(\)\s]*;/gim,'');
	sty = sty+';transform: '+transform+';-webkit-transform: '+transform+';-moz-transform: '+transform+';-ms-transform: '+transform+';-o-transform: '+transform+';';
	sty = sty.replace(';;',';');
	jQuery(dom).attr('style',sty);
	/*
	jQuery(dom).css({
		'position':'static',
	    "webkitTransform":'rotate('+f+deg+'deg)',
	    "MozTransform":'rotate('+f+deg+'deg)',
	    "msTransform":'rotate('+f+deg+'deg)',
	    "OTransform":'rotate('+f+deg+'deg)',
	    "transform":'rotate('+f+deg+'deg)'
	});
	dom.style.webkitTransform  = 'rotate('+f+deg+'deg)';
	dom.style.msTransform  = 'rotate('+f+deg+'deg)';
	dom.style.MozTransform = 'rotate('+f+deg+'deg)';
	dom.style.OTransform = 'rotate('+f+deg+'deg)';
	dom.style.transform = 'rotate('+f+deg+'deg)';*/
}

function parseMmbizUrl(html) {
	html = html.replace(/https:\/mmbiz./g,'https://mmbiz.'); //fix bug
	//html = html.replace(/http:\/\/mmbiz.qpic.cn/g,'https://mmbiz.qlogo.cn');
	html = html.replace(/http:\/\/mmbiz.qlogo.cn/g,'https://mmbiz.qpic.cn');
	html = html.replace(/http:\/\/mmbiz.qpic.cn/g,'https://mmbiz.qpic.cn');
	return html;
}

function setEditorHtml(newHtml){
	
	newHtml = strip_stack_span(newHtml);
	
	//newHtml = parseMmbizUrl(newHtml);
		
	current_editor.undoManger.save();
	current_editor.setContent(newHtml);
	current_editor.undoManger.save();
}

function insertHtml(html,rules){
	
	//html = parseMmbizUrl(html);
	html = current_editor.parseInsertPasteSetHtml(html);
	
	var select_html = jQuery.trim(getSelectionHtml(true));	
	if(select_html != ""){ /* 秒刷  */		
		
		if(rules) { // 设置了秒刷规则的样式，且选中了内容进行秒刷时，忽略样式的结构代码，按选中的内容根据样式规则进行处理
			var select_obj = jQuery('<div>'+select_html+'</div>');
			select_obj.find('*').each(function(){
				jQuery(this).removeAttr('style'); // remove style attributes from select items
				jQuery(this).removeAttr('class');jQuery(this).removeAttr('placeholder');
			});
			
			for(var i in rules['replace']) {				
				select_obj.find(i).each(function(){
					if(!rules['replace'][i] || rules['replace'][i]=="") {
						jQuery(this).replaceWith( jQuery(this).html() );
					}
					else{
						jQuery(this).replaceWith('<'+rules['replace'][i]+'>'+jQuery(this).html()+'</'+rules['replace'][i]+'>');
					}
				});
			}
			for(var i in rules['attributes']) {
				select_obj.find(i).attr(rules['attributes'][i]);
			}
			for(var i in rules.style) {
				select_obj.find(i).attr('style',rules.style[i]);
			}
			for(var i in rules['class']) {
				select_obj.find(i).attr('class',rules['class'][i]);
			}
			for(var i in rules['css']) {
				select_obj.find(i).css(rules['css'][i]);
			}
			
			html = select_obj.html();
			current_editor.execCommand('insertHtml', html);	
			if( current_editor.getOpt('open_editor') ) {
				current_editor.fireEvent("catchRemoteImage");
			}			
			current_editor.undoManger.save();			
			return true;
		}
		
		select_html = strip_tags(select_html, '<br><p><h1><h2><h3><h4><h5><h6><img>');
		var select_obj = jQuery('<div>'+select_html+'</div>');
		
		select_obj.find('.assistant').remove();
		
		select_obj.find('*').each(function(){
			jQuery(this).removeAttr('style'); // remove style attributes from select items
			jQuery(this).removeAttr('class');jQuery(this).removeAttr('placeholder');
		});
		
		var obj = jQuery('<div>'+html+'</div>');
		
		obj.find('> ._135editor').siblings('p').each(function(i){ //查找与插入样式同级的段落，为空段落时，直接删除
			if(jQuery(this).html() == "" || jQuery(this).html() == "&nbsp;" || jQuery(this).html() == "<br>" || jQuery(this).html() == "<br/>"){
				if(typeof jQuery(this).attr('style') == 'undefined'){
					jQuery(this).remove();//remove the empty paragraph
				}
			}
		});
		
		
		select_obj.find('h1,h2,h3,h4,h5,h6').each(function(i){
			var title = obj.find('.135title').eq(i);
			if(title && title.size() > 0){
				title.html(jQuery.trim(jQuery(this).text()));
				jQuery(this).remove();
			}
			else{
				jQuery(this).replaceWith('<p>'+jQuery(this).text()+'</p>');
			}
		});
		
		// brush background image
		select_obj.find('img').each(function(i){
			var bgimg = obj.find('.135bg').eq(i);
			if(bgimg && bgimg.size() > 0){
				bgimg.css('background-image','url('+jQuery(this).attr('src')+')');
				jQuery(this).remove();
				//obj.find('.135bg').eq(i).remove();
			}
		});
		// brush image.
		var img_i = 0;
		select_obj.find('img').each(function(){
			
			var img = obj.find('img').eq(img_i);
			while( img.hasClass('assistant') ) {
				img_i++;
				img = obj.find('img').eq(img_i);
			}
			
			if(img && img.size() > 0 && jQuery(img).parents('.135brush').size()==0 ){ //图片若包含在135brush中的，则跳过。只处理不包含在135brush中的
				img.attr('src',jQuery(this).attr('src'));
				if(img.parent().attr('data-role')=='circle' || img.parent().attr('data-role')=='square' || img.parent().attr('data-role')=='bgmirror'){ // 圆形图片的opacity:1，设置外围正方形的section的背景图
					img.parent().css( 'backgroundImage','url('+jQuery(this).attr('src')+')' );
				}
				
				img_i++;
				jQuery(this).remove();
			}
		});
		select_obj.find('img').each(function(i){
			var img = obj.find('image').eq(i);
			if(img && img.size() > 0 ){ //svg中的image图片
				img.attr('xlink:href',jQuery(this).attr('src'));
				jQuery(this).remove();
			}
		});
		
		var brushs = obj.find('.135brush');
		var total = brushs.size();
		if( total > 0 ) {
			if( total == 1 ){
				var brush_item = obj.find('.135brush:first');
				if(brush_item.data('brushtype') == 'text') {
					brush_item.html(jQuery.trim(select_obj.text()));
				}
				else {
					// 删除空的段落
					select_obj.contents().each(function(i){
						var $this = this;
						if(this.tagName == "IMG"){
							return;
						};
						if(jQuery.trim(jQuery($this).text()) == "" || this.tagName=='BR' || jQuery(this).html() == "" || jQuery(this).html() == "&nbsp;" || jQuery(this).html() == "<br>" || jQuery(this).html() == "<br/>"){
								jQuery(this).remove();//remove the empty paragraph or br or black
						}
					});
					
					var style = brush_item.data('style');
					if(style){
						select_obj.find('*').each(function(){
							jQuery(this).attr('style',style);
						});
					}
					var html = select_obj.html();
					if(html != "") { // 当内容为空时，保留原样式中的内容
						brush_item.html(html);
					}
				}
			}
			else{
				
				// 空段落不删除了；块级元素中，需要<p>标签，这样回车的时候生成P标签，仍停留在块级元素中。不然会生成一个新的块级元素
				// 若仍要删除空行，可以考虑多个连续的空行一起出现时，合并成一个空行
				// .next('p');
				/*
				 * // 删除空的段落
				 select_obj.contents().each(function(i){
					var $this = this;
					if(this.tagName == "IMG"){
						return;
					};
					if(jQuery.trim(jQuery($this).text()) == "" || this.tagName=='BR' || jQuery(this).html() == "" || jQuery(this).html() == "&nbsp;" || jQuery(this).html() == "<br>" || jQuery(this).html() == "<br/>"){
							jQuery(this).remove();//remove the empty paragraph or br or black
					}
				});*/
				
				select_obj.contents().each(function(i){
					var $this = this;
					
					if($this.nodeType == 3){
						$this = jQuery('<p>'+jQuery(this).text()+'</p>').get(0); //纯文本的节点转换成段落
					}
					
					if(i < total){
						var brush_item = brushs.eq(i);
						if(brush_item.data('brushtype') == 'text') {
							brush_item.html(jQuery.trim(jQuery($this).text()));
						}
						else {
							var style = brush_item.data('style');
							if(style){
								jQuery($this).attr('style',style);
							}
							brush_item.empty().append(jQuery($this));
						}
					}
					else{
						var brush_item = brushs.eq(total-1);
						if(brush_item.data('brushtype') == 'text') {
							brush_item.append(jQuery($this).text());
						}
						else {
							var style = brush_item.data('style');
							if(style){
								jQuery($this).attr('style',style);
							}
							brush_item.append(jQuery($this));
						}							
					}
				});					
			}
			
			
			
			/*obj.find('p').each(function(i){
				if(jQuery(this).html() == "" || jQuery(this).html() == "&nbsp;" || jQuery(this).html() == "<br>" || jQuery(this).html() == "<br/>"){
					if(typeof jQuery(this).attr('style') == 'undefined'){
						jQuery(this).remove();//remove the empty paragraph
					}
				}
			});*/
		}
		
		html = obj.html();
		//current_editor.focus(true);//true
		current_editor.execCommand('insertHtml', html);	
		current_editor.undoManger.save();
		if( current_editor.getOpt('open_editor') ) {
			current_editor.fireEvent("catchRemoteImage");
		}
		return true;
	}
	else{
		//alert('当前为格式刷模式，请先选择要转换格式的内容');
		//return false;
	}
	
	// 空的样式内部时，插入到空行样式之前
	/*var range = current_editor.selection.getRange();
	var startContainer = range.startContainer;
	
	if(startContainer.nodeType == 3 ) {
		startContainer = startContainer.parentNode
	}
	
	if( startContainer && jQuery(startContainer).hasClass('_135editor') && jQuery(startContainer).find('> *').size() == 1 && (startContainer.innerHTML =="<p><br></p>" || startContainer.innerHTML =="<p></p>" )) {
		jQuery(startContainer).before(html);
	}
	else if( startContainer && startContainer.tagName =="P" && (startContainer.innerHTML =="​<br>" || startContainer.innerHTML =="​<br/>" || startContainer.innerHTML =="") && jQuery(startContainer.parentNode).hasClass('_135editor') && jQuery(startContainer.parentNode).find('> *').size() == 1 ){
		jQuery(startContainer.parentNode).before(html);
	}
	else{
		//current_editor.focus(true);//true
		current_editor.execCommand('insertHtml', html);	
	}*/
	current_editor.execCommand('insertHtml', html);	
	if( current_editor.getOpt('open_editor') ) {
		current_editor.fireEvent("catchRemoteImage");
	}
	current_editor.undoManger.save();
	return true;
}

function resetMapUrl(){
	
	jQuery( current_editor.selection.document ).find('img').each(function(){
		var $img = jQuery(this);
		var mapurl = jQuery(this).attr('mapurl');
		if(mapurl) {
			var usemap = jQuery(this).attr('usemap');
			if(usemap) {
				jQuery(usemap).each(function(){jQuery(this).remove();})
			}
			usemap = randomString(10);
			
			$img.attr('usemap',usemap);
			
			$img.after('<map name="'+usemap+'" id="'+usemap+'" style="margin: 0px; padding: 0px; word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important;"><area style="margin: 0px; padding: 0px; word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important;" href="'+mapurl+'" shape="default" target="_blank"/></map>'
+'<section>'
+'<map name="'+usemap+'" id="'+usemap+'" style="margin: 0px; padding: 0px; word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important;"><area style="margin: 0px; padding: 0px; word-wrap: break-word !important; max-width: 100%; box-sizing: border-box !important;" href="'+mapurl+'" shape="default" target="_blank"/></map>'
+'</section>');
		}
	});
	
}


function setBackgroundColor(bgcolor,color,history){
	if(isGreyColor(bgcolor)){
		return false;
	}
	if(history){
		current_editor.undoManger.save();
	}
	var active_item =   current_editor.currentActive135Item();
	if( !replace_full_color &&  active_item ) {
		
		parseObject( active_item, bgcolor , color);
		active_item.attr('data-color',bgcolor);
		active_item.attr('data-custom',bgcolor);
		
		current_editor.undoManger.save();
		return ;
	}
	else{
		if( !replace_full_color ) {
			//showErrorMessage("没有选择要调色的样式，如要全文换色请勾选配色方案位置的“全文换色”的选择框");
			return ;
		}
		// 全文换色
		parseObject( jQuery(current_editor.selection.document) , bgcolor , color);
		
		jQuery(current_editor.selection.document).find('._135editor').each(function(){
			jQuery(this).attr('data-color',bgcolor);
		});
	}
	
	if(history){
		current_editor.undoManger.save();
	}
//	if(changeType == 'select'){
//		alert("浅色颜色不支持全文调色，请选择要调色的部分内容(可手动全选后调色)");
//		return ;
//	}
//	else { //if(!custom_style_set || custom_style_set && confirm("进行过部分区域设色，是否放弃并全部重设")){
//		var html = getEditorHtml();	//getDealingHtml
//		var newHtml = parseHtml(html,bgcolor,color);
//		setEditorHtml(newHtml);
//		html = null; newHtml = null;
//		custom_style_set = false;
//	}
	return;
}

function parseObject(obj,bgcolor,color){
	
	if(isGreyColor(bgcolor)){
		return false;
	}
	
	// 统计颜色点击
	// color_click(bgcolor); // 放在这里点击量太大了，调色板选色时，会出现非常大量的请求。移到编辑首页了，点击推荐颜色与收藏颜色统计
	
	obj.find("*").each(function() {
		if(!this.nodeName || this.nodeName == "HTML" || this.nodeName == "HEAD" || this.nodeName == "STYLE" || this.nodeName == "LINK" || this.nodeName == "BODY" ){
			return;
		}
		
		if(this.nodeName == "HR" || this.nodeName == "hr" ) {
			//this.tyle.borderColor = bgcolor;
			var style = jQuery(this).attr('style');
			jQuery(this).attr('style',style+';border-color:'+bgcolor+';');
			return ; // 水平线自动设置颜色
		}

		if( this.nodeName == "" || jQuery(this).attr('style') == "" ) {
			return ; // 类型为空或者td时，不处理
		}
		
		// if(0) { //已单独设置过颜色的跳过
		// 	if( jQuery(this).hasClass('135editor') && jQuery(this).attr('data-custom') ) {
		// 		// alert(111);
		// 		return ; 
		// 	}
		// 	if(  replace_full_color  ) { //&& !  current_editor.currentActive135Item()
		// 		var p135items = jQuery(this).parents('._135editor');
		// 		for(var i=0;i<p135items.size();i++){			
		// 			if( p135items.eq(i).attr('data-custom')){
		// //				alert('return');
		// 				return;
		// 			}
		// 		}
		// 	}
		// }

		if(jQuery(this).attr('data-ct')=='fix'){
    		return ; // data-ct=fix固定颜色
    	}
		if( jQuery(this).attr('fill') ){
			jQuery(this).attr('fill',bgcolor);
			return;
		}
		
		var oldColor = jQuery(this).parents('._135editor:first').attr('data-color');
		var style = jQuery(this).attr('style');
		
		if(oldColor == bgcolor ) {
			return; //旧颜色值与当前值相等时，跳过不处理
		}
		
		if(oldColor && style && style.indexOf('text-shadow') >= 0) {
			var newstyle = str_replace(rgb2hex(oldColor),bgcolor,style); // style.replace(new RegExp(oldColor, "gm"),bgcolor);			
			newstyle = str_replace(hex2rgb(oldColor),bgcolor,newstyle);
			// 将颜色转换成rgb与#两种形式，同时替换。防止不统一漏了。
			//alert(style +'--'+ newstyle);
			jQuery(this).attr('style',newstyle);
		}

    	var persent = jQuery(this).attr('data-clessp') ? jQuery(this).attr('data-clessp') : '50%';
    	
    	var hasSetBgColor = false;
    	var txt_color;
	    var bgC = jQuery(this).get(0).style.backgroundColor; // 当前页面元素的背景色。新设置前的
	    
	    if (!bgC || bgC==='initial' || bgC === 'transparent' || bgC === "") { //无背景时，设置前景色
	    	
	    	
	    	var fc = jQuery(this).get(0).style.color; //jQuery(this).css('color');
	    	if(fc && fc != "" && fc != 'inherit' && !isGreyColor(fc) ) { // 
	    		
		    	if(jQuery(this).attr('data-txtless')){ // 前景文字色less规则换色
		    		var txtpersent = jQuery(this).attr('data-txtlessp') ? jQuery(this).attr('data-txtlessp') : '30%';
		    		txt_color = getColor(rgb2hex(bgcolor),jQuery(this).attr('data-txtless'),txtpersent);				
			        jQuery(this).css('color',txt_color);
		    	}
		    	else if(isLightenColor(bgcolor)){ // 要设置的文字颜色为浅色时，将颜色加深已区分其它背景色与父级背景
	    			txt_color = getColor(rgb2hex(bgcolor),'darken',persent);
	    			jQuery(this).css('color',txt_color);
	    		}
	    		else{ // 要设置的文字颜色为深色时，
	    			var parentBgColor = getPrarentBgColor(jQuery(this));
	    			if(parentBgColor == bgcolor ) { // 无背景色时，父级背景色与要设置的颜色相同。 将文字颜色变浅（或变白）
	    				txt_color = getColor(rgb2hex(bgcolor),'lighten',persent);
	    				jQuery(this).css('color',txt_color);
	    			}
	    			else{ //颜色与父级的背景色不同时，直接设置颜色
	    				jQuery(this).css('color',bgcolor);
	    			}
	    		}
	    	}
	    } else {
	    	//var bgC_hex = rgb2hex(bgC);
	    	if( typeof(jQuery(this).attr('data-bgless')) != "undefined"){	 // less处理的背景色   			
    			var bgpersent = jQuery(this).attr('data-bglessp') ? jQuery(this).attr('data-bglessp') : '30%';
    			var bg_color;
    			
    			if(jQuery(this).attr('data-bgless') == "true" || jQuery(this).attr('data-bgless') == true ){ //默认变色处理，深色变浅，浅色自动加深
    				if(isLightenColor(bgcolor)){
    					bg_color = getColor(rgb2hex(bgcolor),'darken',bgpersent);
    					bg_color = getColor(rgb2hex(bg_color),'saturate','20%');
    	    		}
    				else{
    					bg_color = getColor(rgb2hex(bgcolor),'lighten',bgpersent);
    				}
    			}
    			else{
    				/**
    				 * data-bgopacity 设置背景透明度
    				 * data-bglessp 为变色百分比
    				 * 
    				 * data-bgless,常用变色处理。
    				 * 
    				 * 	spin  Rotate the hue angle of a color in either direction.
    				 * 		色盘中设置一个角度（-360~0~360度），调色的颜色正向或者反向的角度对应的颜色
    				 *  darken	将颜色加深，参数值为百分比0-100%，
    				 *  lighten	将颜色变浅，参数值为百分比0-100%
    				 *  fade	设置颜色的透明度， 参数值为百分比0-100%.将忽略rgba颜色本身的透明度
    				 *  fadein	增加颜色的透明度值，参数值为百分比0-100%. fadein(rgba(128, 242, 13, 0.5), 10%)。将透明度由0.5变成0.6
    				 *  fadeout 减小颜色的透明度值，参数值为百分比0-100%. fadeout(rgba(128, 242, 13, 0.5), 10%)。将透明度由0.5变成0.4
    				 *  saturate 增加饱和度、颜色浓度。参数值为百分比0-100%. Increase the saturation of a color in the HSL color space by an absolute amount. 
    				 *  desaturate 减少饱和度、颜色浓度。参数值为百分比0-100%. Decrease the saturation of a color in the HSL color space by an absolute amount.
    				 *  
    				 *  
    				 *  greyscale 参数形式不同，不支持。使用desaturate(@color, 100%)调用实现相同的效果。 Remove all saturation from a color in the HSL color space; the same as calling desaturate(@color, 100%).
    				 */
    				bg_color = getColor(rgb2hex(bgcolor),jQuery(this).attr('data-bgless'),bgpersent);
    			}
    			
    			if(jQuery(this).attr('data-bgopacity')) {
    				bg_color = getColor(rgb2hex(bg_color),'fadeout',jQuery(this).attr('data-bgopacity'));
    			}
				
				hasSetBgColor = true;
				jQuery(this).css('backgroundColor',bg_color);
				
				if(isLightenColor(bg_color)){
	    			txt_color = getColor(rgb2hex(bg_color),'darken',persent);
	    			txt_color = getColor(rgb2hex(txt_color),'saturate','20%');
	    		}
	    		else{
	    			txt_color = color;//getColor(rgb2hex(bg_color),'lighten',persent);
	    		}
				
		        jQuery(this).css('color',txt_color);
			}
	    	else if(jQuery(this).attr('data-bgopacity')) { // 颜色透明度
	    		
	    		var bg_color;
	    		bg_color = getColor(rgb2hex(bgcolor),'fadeout',jQuery(this).attr('data-bgopacity'));
	    		
	    		if(isLightenColor(bg_color)){
	    			txt_color = getColor(rgb2hex(bg_color),'darken',persent)
	    		}
	    		else{
	    			
	    			txt_color = color;//getColor(rgb2hex(bg_color),'lighten',persent);
	    		}
	    		
	    		jQuery(this).css('backgroundColor',bg_color);	    		
	    		
		        jQuery(this).css('color',txt_color);
	    	}	    	
	    	else if(!isGreyColor(bgC)) {	 // 旧的背景颜色rgb为3种相同值时，不处理
	    		hasSetBgColor = true;
	    		jQuery(this).css('backgroundColor',bgcolor);
	    		
	    		var fc = jQuery(this).get(0).style.color; //jQuery(this).css('color');
		    	if( fc != 'inherit' ) {
		    		if(isLightenColor(bgcolor)){
		    			txt_color = getColor(rgb2hex(bgcolor),'darken',persent)
		    		}
		    		else{
		    			txt_color = color;//getColor(rgb2hex(bg_color),'lighten',persent);
		    		}
		    		jQuery(this).css('color',txt_color);
		    	}
	    	}
	    	else{ //灰色系背景色时， 不处理背景色仅设置前景色
	    		var fc = jQuery(this).get(0).style.color; //jQuery(this).css('color');
		    	if(fc && fc != "" && fc != 'inherit' && !isGreyColor(fc)) {
		    		
		    		if(jQuery(this).css('backgroundColor') != bgcolor) {
		    			jQuery(this).css('color',bgcolor);
		    		}			        
		    	}
	    	}
	    }
	    
	    

		if( jQuery(this).attr('data-bcless') ){	//	|| hasSetBgColor 		
			var bc_color = bgcolor;			
			if(isLightenColor(bgcolor) ){ //|| jQuery(this).data('bcless')=='darken'
				var persent = jQuery(this).attr('data-bclessp') ? jQuery(this).attr('data-bclessp') : '20%';
				if( jQuery(this).attr('data-bcless')=='darken' ) {
					bc_color = getColor(rgb2hex(bgcolor),'darken',persent);
					//bc_color = getColor(rgb2hex(bc_color),'saturate','30%');
				}
				else{
					bc_color = getColor(rgb2hex(bgcolor),'darken',persent);
					if(  jQuery(this).attr('data-bcless')  && jQuery(this).attr('data-bcless') != 'auto' &&  jQuery(this).attr('data-bcless') != 'true'){
						bc_color = getColor(rgb2hex(bgcolor),jQuery(this).attr('data-bcless'),persent);
					}
					
				}
			}
			else{ //变换的颜色为深色
				var persent = jQuery(this).attr('data-bclessp') ? jQuery(this).attr('data-bclessp') : '20%';	
				if( jQuery(this).attr('data-bcless')=='lighten' ||  jQuery(this).attr('data-bcless')=='auto' ||  jQuery(this).attr('data-bcless')== 'true'  ) {
					bc_color = getColor(rgb2hex(bgcolor),'lighten',persent);
					//bc_color = getColor(rgb2hex(bc_color),'desaturate','20%');
					//bc_color = getColor(rgb2hex(bc_color),'fadein','20%');
				}
				else{
					if(  jQuery(this).attr('data-bcless')  && jQuery(this).attr('data-bcless') != 'auto' &&  jQuery(this).attr('data-bcless') != 'true'){
						bc_color = getColor(rgb2hex(bgcolor),jQuery(this).attr('data-bcless'),persent);
					}
				}
			}
			
			if(jQuery(this).attr('data-bdopacity')) {
				bc_color = getColor(rgb2hex(bc_color),'fadeout',jQuery(this).attr('data-bdopacity'));
			}
			
			//强制换色，不考虑rgb值是否相同。 设置了透明的跳过。可能只设置某一侧的颜色来实现箭头的效果
			if(this.style.borderBottomColor || this.style.borderTopColor || this.style.borderLeftColor || this.style.borderRightColor ) {

				if(this.style.borderBottomColor != 'transparent' && this.style.borderBottomColor.toLowerCase() != 'rgb(255, 255, 255)' && this.style.borderBottomColor.toLowerCase() != '#fff' && this.style.borderBottomColor != 'initial') {
					this.style.borderBottomColor = bc_color;
				}
				if(this.style.borderTopColor != 'transparent' && this.style.borderTopColor.toLowerCase() != 'rgb(255, 255, 255)' && this.style.borderTopColor.toLowerCase() != '#fff' && this.style.borderTopColor != 'initial') {
					this.style.borderTopColor = bc_color;
				}
				if(this.style.borderLeftColor != 'transparent' && this.style.borderLeftColor.toLowerCase() != 'rgb(255, 255, 255)' && this.style.borderLeftColor.toLowerCase() != '#fff' && this.style.borderLeftColor != 'initial') {
					this.style.borderLeftColor = bc_color;
				}
				if(this.style.borderRightColor != 'transparent' && this.style.borderRightColor.toLowerCase() != 'rgb(255, 255, 255)' && this.style.borderRightColor.toLowerCase() != '#fff' && this.style.borderRightColor != 'initial') {
					this.style.borderRightColor = bc_color;
				}
			}
			else{
				if (this.style.borderColor !== 'transparent' && this.style.borderColor !== 'initial') {
					this.style.borderColor = bc_color;
				}
			}
		}
		else{
			var bc_color = bgcolor;	
			if(jQuery(this).attr('data-bdopacity')) {
				bc_color = getColor(rgb2hex(bgcolor),'fadeout',jQuery(this).attr('data-bdopacity'));
			}
			
			if(this.style.borderBottomColor || this.style.borderTopColor || this.style.borderLeftColor || this.style.borderRightColor ) {				
				if(this.style.borderBottomColor != 'transparent' && this.style.borderBottomColor != 'initial') {
					setColor(this,'borderBottomColor',bc_color);
				}
				if(this.style.borderTopColor != 'transparent' && this.style.borderTopColor != 'initial') {
					setColor(this,'borderTopColor',bc_color);
				}
				if(this.style.borderLeftColor != 'transparent' && this.style.borderLeftColor != 'initial') {
					setColor(this,'borderLeftColor',bc_color);
				}
				if(this.style.borderRightColor != 'transparent' && this.style.borderRightColor != 'initial') {
					setColor(this,'borderRightColor',bc_color);
				}
			}
			else{
				var c = this.style.borderColor;
			    if (c !== 'transparent' && c !== 'initial' && !isGreyColor(c) ) {
			        this.style.borderColor = bc_color;
			    }				
			}
		}
		
		var shadow = jQuery(this).css('boxShadow');
    	//-webkit-linear-gradient(left, rgb(250, 235, 255), rgb(255, 255, 250), rgb(250, 235, 255))
		// linear-gradient(-135deg, transparent 1em, rgb(85, 136, 170) 0px)
    	if( shadow && shadow != 'none'){ //阴影色
    		var reg = new RegExp('rgb\\([\\d|,|\\s]+?\\)','ig');
    		var match = shadow.match(reg); //["rgb(0, 5, 1)", "rgb(85, 136, 170)"]
            if(match) {
            	for(var i=0;i<match.length;i++){
	            	if( isGreyColor(match[i]) ){
	            		continue;
	            	}
	            	var less_attr = 'data-shadowless'+i;
	            	var shadow_color = bgcolor;
	            	
	            	if( jQuery(this).attr(less_attr) ){
	        			var shadowpersent = jQuery(this).attr(less_attr+'p') ? jQuery(this).attr(less_attr+'p') : '30%';
	        			shadow_color = getColor(rgb2hex(bgcolor),jQuery(this).attr(less_attr),shadowpersent);
	        			
	        		}
	            	shadow = shadow.replace(match[i],shadow_color).replace(match[i].replace(/ /g,''),shadow_color);	               
	            }
            	jQuery(this).css('boxShadow',shadow);
            }
    	}
		
		var bgimg = jQuery(this).css('backgroundImage');
    	//-webkit-linear-gradient(left, rgb(250, 235, 255), rgb(255, 255, 250), rgb(250, 235, 255))
		// linear-gradient(-135deg, transparent 1em, rgb(85, 136, 170) 0px)
    	if( bgimg.indexOf('linear-gradient(') >= 0 ){ //渐变色
    		
    		var sty = jQuery(this).attr('style');
    		var reg = new RegExp('rgb\\([\\d|,|\\s]+?\\)','ig');
            var match = bgimg.match(reg); //["rgb(0, 5, 1)", "rgb(85, 136, 170)"]
            if(match) {
            	if( jQuery(this).attr('data-bgless') ){
        			var bgpersent = jQuery(this).attr('data-bglessp') ? jQuery(this).attr('data-bglessp') : '30%';
        			main_color = getColor(rgb2hex(bgcolor),jQuery(this).attr('data-bgless'),bgpersent);
        			
        		}
        		else if(isLightenColor(bgcolor)){
        			main_color = getColor(rgb2hex(bgcolor),'saturate','20%');
        		}
        		else{ //深色也转变成浅色，太深的渐变色不好看
        			main_color = getColor(rgb2hex(bgcolor),'lighten','5%');
        			main_color = getColor(rgb2hex(main_color),'desaturate','10%');
        			main_color = getColor(rgb2hex(main_color),'fadein','20%');
        		}
        		if( jQuery(this).attr('data-grless') ){
        			gradient_color =  getColor(rgb2hex(main_color),jQuery(this).attr('data-grless'),jQuery(this).attr('data-grlessp'));
        		}
        		else if( jQuery(this).attr('data-gradient') ){
        			gradient_color =  jQuery(this).attr('data-gradient') ;
        		}
        		else{
        			gradient_color = getColor(rgb2hex(main_color),'lighten','30%');
        		}
        		if(isLightenColor(main_color)){
    				txt_color = getColor(rgb2hex(main_color),'darken','50%');
    				txt_color = getColor(rgb2hex(txt_color),'saturate','30%');
    			}
    			else{
    				txt_color = getColor(rgb2hex(main_color),'lighten','50%');
    			}
        		
        		
	            for(var i=0;i<match.length;i++){
	            	if( isGreyColor(match[i]) ){
	            		continue;
	            	}
	            	if( jQuery(this).attr('data-grorder') == 'reverse' ){
	            		if(i%2==0 || (i>0 && match[i] == match[i-1]) ){
	                		sty = sty.replace(match[i],gradient_color).replace(match[i].replace(/ /g,''),gradient_color);
	                	}
	                	else{
	                		sty = sty.replace(match[i],main_color).replace(match[i].replace(/ /g,''),main_color);
	                	}
	            	}
	            	else{
	            		if(i%2==0 || (i>0 && match[i] == match[i-1]) ){
	                		sty = sty.replace(match[i],main_color).replace(match[i].replace(/ /g,''),main_color);
	                	}
	                	else{
	                		sty = sty.replace(match[i],gradient_color).replace(match[i].replace(/ /g,''),gradient_color);
	                	}
	            	}
	            }
	            jQuery(this).attr('style',sty);
	            jQuery(this).css('color',txt_color);
            }
            
    		/*var colors;
    		var type ;
    		if(bgimg.substring(0,29) == '-webkit-linear-gradient(left,'){
    			type = 'left';
    			colors = bgimg.substring(29 , (bgimg.length-1) );
    		}
    		else if(bgimg.substring(0,28) == '-webkit-linear-gradient(top,'){
    			type = 'top';
    			colors = bgimg.substring(28 , (bgimg.length-1) );
    		}
    		else if(bgimg.substring(0,30) == '-webkit-linear-gradient(right,'){
    			type = 'right';
    			colors = bgimg.substring(30 , (bgimg.length-1) );
    		}
    		else if(bgimg.substring(0,31) == '-webkit-linear-gradient(bottom,'){
    			type = 'bottom';
    			colors = bgimg.substring(31 , (bgimg.length-1) );
    		}
    		if(colors) { // 匹配渐变色成功
        		var color_arr = colors.split('),');
        		
        		var txt_color, gradient_color,main_color;
        		
        		if( jQuery(this).attr('data-bgless') ){
        			var bgpersent = jQuery(this).attr('data-bglessp') ? jQuery(this).attr('data-bglessp') : '30%';
        			main_color = getColor(rgb2hex(bgcolor),jQuery(this).attr('data-bgless'),bgpersent);
        			
        		}
        		else if(isLightenColor(bgcolor)){
        			main_color = getColor(rgb2hex(bgcolor),'saturate','20%');
        		}
        		else{ //深色也转变成浅色，太深的渐变色不好看
        			main_color = getColor(rgb2hex(bgcolor),'lighten','5%');
        			main_color = getColor(rgb2hex(main_color),'desaturate','10%');
        			main_color = getColor(rgb2hex(main_color),'fadein','20%');
        		}
        		
        		if(isLightenColor(main_color)){
    				txt_color = getColor(rgb2hex(main_color),'darken','50%');
    				txt_color = getColor(rgb2hex(txt_color),'saturate','30%');
    			}
    			else{
    				txt_color = getColor(rgb2hex(main_color),'lighten','50%');
    			}
        		if( jQuery(this).attr('data-grless') ){
        			gradient_color =  getColor(rgb2hex(main_color),jQuery(this).attr('data-grless'),jQuery(this).attr('data-grlessp'));
        		}
        		else if( jQuery(this).attr('data-gradient') ){
        			gradient_color =  jQuery(this).attr('data-gradient') ;
        		}
        		else{
        			gradient_color = getColor(rgb2hex(main_color),'lighten','30%');
        		}
        		
        		
        		if(color_arr.length ==3) {
    				jQuery(this).css('backgroundImage','-webkit-linear-gradient('+type+', '+main_color+', '+gradient_color+', '+main_color+')');
    				jQuery(this).css('color',txt_color);
        		}
        		else if(color_arr.length ==2) {
        			if( jQuery(this).attr('data-grorder') == 'reverse' ){
        				jQuery(this).css('backgroundImage','-webkit-linear-gradient('+type+', '+gradient_color+', '+main_color+')');
        			}
        			else{
        				jQuery(this).css('backgroundImage','-webkit-linear-gradient('+type+', '+main_color+', '+gradient_color+')');
        			}
    				jQuery(this).css('color',txt_color);
        		}
    		}*/
    		//return;
    	}
	});
    
	/* 全部样式换色时，耗时较长，减少处理。
	obj.find("*").each(function() {
		//fix inherit color;
		var fc = jQuery(this).css('color');
		jQuery(this).find('*').each(function() {
			var nfc = jQuery(this).css('color');
			if(nfc == fc){ jQuery(this).css('color','inherit') }
		});
	});
	*/
	return obj;
}

function parseHtml(html,bgcolor,color){
	var obj = jQuery('<div id="editor-content">'+html+"</div>");
	
	obj = parseObject(obj,bgcolor,color);
	
	var result = obj.html();
	obj=null;
	return result;
}


function style_click(id){
	var url = BASEURL+'/editor_styles/click_num';
	ajaxAction(url,{id:id});
	return false;
}

function setFavorColor(colors,callback){
	var url = BASEURL+'/editor_styles/setFavorColor';
	ajaxAction(url,{colors:colors},null,callback);
}

function color_click(color){
	color = hex2rgb(color);
	var url = BASEURL+'/colors/click';
	ajaxAction(url,{color:color});
	return false;
}

function setColor(obj,colorType,color){
	var c = jQuery(obj).css(colorType);
    if (c === 'transparent' || c === 'initial') {
    	return;
    } else {
    	if(!isGreyColor(c)){
    		jQuery(obj).css(colorType,color);
    	} 
    }
}
function getPrarentBgColor($obj) {
	
	if(!$obj.parent().get(0)) {
		return "";
	}
	
	if($obj.parent().get(0).tagName && $obj.parent().get(0).tagName == 'BODY') {
		if($obj.parent().get(0).style) {
			var bgC = $obj.parent().get(0).style.backgroundColor; // 当前页面元素的背景色。新设置前的
			return bgC;
		}
		else{
			return "";
		}
		
	}
	if($obj.parent().get(0).style) {
		
		var bgImg = $obj.parent().get(0).style.backgroundImage ;
		if( bgImg && bgImg != "" ){ //有背景图片时，不返回上级的背景色
			return "";
		}
		
		var bgC = $obj.parent().get(0).style.backgroundColor; // 当前上级元素的背景色
	    if (!bgC || bgC==='initial' || bgC === 'transparent' || bgC === "") { //无背景时，继续找上级
	    	return getPrarentBgColor($obj.parent());
	    }
	    else{
	    	return bgC;
	    }
	}
	else{
		return "";
	}
}

function rgb2hex(color){
 rgb = color.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
 return (rgb && rgb.length === 4) ? "#" +
  ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
  ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
  ("0" + parseInt(rgb[3],10).toString(16)).slice(-2) : color;
}

function hex2rgb(color){
	var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
	var sColor = color.toLowerCase();
    if(sColor && reg.test(sColor)){
        if(sColor.length === 4){
            var sColorNew = "#";
            for(var i=1; i<4; i+=1){
                sColorNew += sColor.slice(i,i+1).concat(sColor.slice(i,i+1));        
            }
            sColor = sColorNew;
        }
        //处理六位的颜色值
        var sColorChange = [];
        for(var i=1; i<7; i+=2){
            sColorChange.push(parseInt("0x"+sColor.slice(i,i+2)));        
        }
        return "rgb(" + sColorChange.join(",") + ")";
    }else{
        return sColor;        
    }
};

function isLightenColor(color){
	var c = rgb2hex(color);
	var r = ("" + c.substring(1,3));
	var g = ("" + c.substring(3,5));
	var b = ("" + c.substring(5,7));
	
	// 我们通过把 RGB 模式转换成 YUV 模式，而 Y 是明亮度（灰阶），因此只需要获得 Y 的值而判断他是否足够亮就可以了：
	// $grayLevel = $R * 0.299 + $G * 0.587 + $B * 0.114;
	// if ($grayLevel >= 192) 
	// 80 ~ 128  C0 ~ 192
	if(r > 'C0' && g > 'C0' && b > 'C0' ){
		return true;
	}
	else{
		return false;
	}
}

function isGreyColor(color){
	var c = rgb2hex(color);
	var r = ""+c.substring(1,3);
	var g = ""+c.substring(3,5);
	var b = ""+c.substring(5,7);
	if(r==g && g==b ){ // && r>"dd"    || r>"dd" && g > "dd" && b > "dd"
		return true;
	}
	else{
		return false;
	}
}



//alert(rgb2hex('rgb(249, 110, 87)'));
function getColor(color,type,num){ // change less to tinycolor. https://github.com/bgrins/TinyColor
	var str = '';
	var color_obj = tinycolor(color);
	if( typeof(color_obj[type]) == 'function' ) {
		num = parseInt(num.replace('%','')); // 需要parseInt转换，否则结果不对。
		return hex2rgb(color_obj[type](num).toString());
	}
	else if(type == 'fade' || type == 'fadein' || type == 'fadeout' ) { // setAlpha
		if(num.indexOf('%') > 0) {
			num = num.replace('%','');
			num = parseInt(num) / 100;
		}
		if(type == 'fadein') {
			num += color_obj.getAlpha();
		}
		else if(type == 'fadeout') {
			num = color_obj.getAlpha() - num;
		}
		return hex2rgb(color_obj.setAlpha(num).toString());
	}
	return color;
}

function LightenDarkenColor(col, amt) {	  
    var usePound = false;
  
    if (col[0] == "#") {
        col = col.slice(1);
        usePound = true;
    }
 
    var num = parseInt(col,16);
 
    var r = (num >> 16) + amt;
 
    if (r > 255) r = 255;
    else if  (r < 0) r = 0;
 
    var b = ((num >> 8) & 0x00FF) + amt;
 
    if (b > 255) b = 255;
    else if  (b < 0) b = 0;
 
    var g = (num & 0x0000FF) + amt;
 
    if (g > 255) g = 255;
    else if (g < 0) g = 0;
 
    return (usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16);  
}

function openTplModal(){
	jQuery('#templateModal').modal('show');
	jQuery('#template-modal-body').html('正在加载').load(BASEURL+'/editor_styles/myTemplates #my-templates',function(){
		//jQuery('#my-templates .nav-tabs li:first').trigger('click');
		 jQuery('#my-templates .nav-tabs a:first').tab('show')
	})
}

function set_style(styles){

	ckeditors['WxMsgContent'].execCommand('removeFormat');
	
	var element = ckeditors['WxMsgContent'].getSelection().getStartElement();
	
	if(element.getName() == 'img'){ //图片时直接应用到图片
		element.setStyles(styles);
	}
	else{
		// 应用到编辑器的单项
		var style = new CKEDITOR.style( { element: element.getName(), styles: styles} );
		ckeditors['WxMsgContent'].applyStyle( style );
	}
}

function getPreferences(idx){
	var cookieOpt = current_editor.getPreferences('editor135');
	if( cookieOpt  && cookieOpt[idx] ) {
		return cookieOpt[idx];
	}
	return null;
}
function setPreferences(key,value){
	var cookieOpt = current_editor.getPreferences('editor135');
	var obj = {};
    if (typeof (key) == "string") {
        obj[ key ] = value;
    } else {
        obj = key;
    }
    if(!cookieOpt) cookieOpt = {};
    jQuery.extend(cookieOpt, obj);    
    current_editor.setPreferences('editor135',cookieOpt);
}

function applyParagraph(type) {
	var object ;
	if(type == 'all') {
		var editor_document = current_editor.selection.document;
		object = jQuery(editor_document);
	}
	else{
		if( !  current_editor.currentActive135Item() ) return;
		object =  current_editor.currentActive135Item();
	}
	jQuery(object).find('p').each(function(){
		jQuery(this).css('lineHeight',jQuery('#paragraph-lineHeight').val());
		jQuery(this).css('fontFamily',jQuery('#paragraph-fontFamily').val());
		jQuery(this).css('fontSize',jQuery('#paragraph-fontSize').val());
		jQuery(this).css('textIndent',jQuery('#paragraph-textIndent').val());
		jQuery(this).css('paddingTop',jQuery('#paragraph-paddingTop').val());
		jQuery(this).css('paddingBottom',jQuery('#paragraph-paddingBottom').val());
	})
}
function clean_135helper(){
	var editor_document = current_editor.selection.document;
	jQuery(editor_document).find('._135editor').each(function(){
		jQuery(this).find('.tool-border').remove();
		jQuery(this).removeClass('active');
		jQuery(this).css({'border':'0 none','padding':0});
	});
	current_editor.sync();
}

function showColorPlan( clientY ){ // clientY 鼠标点击的屏幕Y轴位置
	if(current_editor) {
		var $container = jQuery(current_editor.container);
		var offset = $container.offset();
		var width = $container.width();
		var height = $container.height();
		if(offset.left + width + jQuery('#color-plan').width() < jQuery(window).width() ) {
			jQuery('#color-plan').css('left',offset.left + width -5 ); // 右侧位置够用时，显示在右侧
		}
		else{			
			jQuery('#color-plan').css('left',offset.left - jQuery('#color-plan').width() ); // 右侧位置不够用时，显示在左侧
		}
		
		if( clientY  ) {
			if( clientY - jQuery(window).scrollTop() > jQuery('#color-plan').height()/2 ) { //大于选色器的一半，则显示选色器中线在点击位置
				var top = clientY - jQuery(window).scrollTop() - (jQuery('#color-plan').height()/2); 
				if(jQuery(window).height() - top > jQuery('#color-plan').height() ) {
					
					jQuery('#color-plan').css('bottom', 'auto');
					//alert(top); alert(offset.top + $container.find('.edui-editor-toolbarbox').height());
					if(top < offset.top + $container.find('.edui-editor-toolbarbox').height() - jQuery(window).scrollTop() ) { // 高度小于可编辑区域顶部时。不超过编辑区域顶部
						jQuery('#color-plan').css('top', offset.top + $container.find('.edui-editor-toolbarbox').height() - jQuery(window).scrollTop() );
					}
					else if( top + jQuery('#color-plan').height() >  offset.top + $container.height() - jQuery(window).scrollTop() ){ // 高度大于可编辑区域顶部时。不超过编辑区域顶部
						jQuery('#color-plan').css('top', offset.top + $container.height() - jQuery(window).scrollTop() - jQuery('#color-plan').height() +20 );
					}
					else{
						jQuery('#color-plan').css('top', top);
					}
				}
				else{
					jQuery('#color-plan').css('top', 'auto'); //能够完全显示的下。
					jQuery('#color-plan').css('bottom', 0);
				}
			}
			else{
				if( jQuery(window).scrollTop() < offset.top + $container.find('.edui-editor-toolbarbox').height() ) { // 工具栏可见，不超过工具栏
					jQuery('#color-plan').css('top', offset.top + $container.find('.edui-editor-toolbarbox').height() - jQuery(window).scrollTop() );
					jQuery('#color-plan').css('bottom', 'auto');
				}
				else {
					jQuery('#color-plan').css('top', 0 ); //点击高度太小，居顶部显示
					jQuery('#color-plan').css('bottom', 'auto');
				}
				
			}
		}
		else{
			jQuery('#color-plan').css('top', offset.top + height/2 - 120 - jQuery(window).scrollTop() );
		}
		
		
		
		jQuery('#color-plan').show();
	}
	else{
		jQuery('#color-plan').show();
	}	
}

function hideColorPlan(){
	jQuery('#color-plan').hide();
}

function meitu_upload(callback){
	if( sso.check_userlogin() ) {
		
		publishController.open_html_dialog('meitu-full');
		
		//setTimeout(function(){
		//	jQuery('#meitu-full').find('.modal-body .alert').remove();
		//},6000);
		
		
		xiuxiu.setLaunchVars("titleVisible",0);
		xiuxiu.setLaunchVars("maxFinalWidth",600);
		//xiuxiu.setLaunchVars ("nav", "puzzle/puzzlePhoto");
		xiuxiu.setLaunchVars("customMenu", ["decorate","facialMenu","puzzle"]);
		
		xiuxiu.setLaunchVars("uploadBtnLabel", "上传", "meitu_full");
		
		xiuxiu.embedSWF("MeituFullContent",3,940,600,"meitu_full");
		  //修改为您自己的图片上传接口
		  xiuxiu.setUploadURL(BASEURL + '/uploadfiles/upload');
		  xiuxiu.setUploadType(2);
		  xiuxiu.setUploadDataFieldName("upload");		  
		  xiuxiu.onBeforeUpload = function (data, id)
		  {
		        var size = data.size;
		        if(size >  1024 * 1024)
		        { 
		            alert("图片不能超过1M"); 
		            return false; 
		        }
		        xiuxiu.setUploadArgs({'return':"xiuxiu",'remote_type':'wechatUploadimg','no_thumb':1,'MIAOCMS[Auth][User]':jQuery.cookie('MIAOCMS[Auth][User]')}, id);
		        return true; 
		  };
		  
		  xiuxiu.onUploadResponse= function(data,id) {
			  try {
				  var json=eval('(' + data + ')'); 
			  } catch (e) {
				  alert(data);
				  return;
			  }
			  
			  if( json.ret == -1 ) {
				  showErrorMessage(json.error);
			  }
			  else{
				  insertHtml('<img src="'+json.url+'" _src="'+json.url+'">');			  
				  publishController.close_dialog();
			  }
			  
			  if( typeof(callback) == 'function' ) {					
					return callback(json.url);
			  }
		  };
		  xiuxiu.onClose = function(id){
			  publishController.close_dialog();
		  }
	  
		//publishController.open_dialog(BASEURL+'/uploadfiles/crop?inajax=1&src='+url,{'title':'编辑图片',width:960,zIndex:9999999999});
		/*var $dialogid = publishController.dialogid;
		jQuery('#'+$dialogid).on('hidden.bs.modal', function (e) {
				jQuery('#'+$dialogid).remove();
		});*/
	}
	else{
		showErrorMessage('图片美图上传功能需要登录后才能使用');
	}
}

function pingtu_upload(callback){
	if( sso.check_userlogin() ) {
		
		publishController.open_html_dialog('meitu-pingtu');
		
		
		xiuxiu.setLaunchVars("titleVisible",0);
		xiuxiu.setLaunchVars("maxFinalWidth",600);
		
		xiuxiu.setLaunchVars("uploadBtnLabel", "上传", "meitu_pingtu");
		
		xiuxiu.embedSWF("MeituPingTuContent",2,700,500,"meitu_pingtu");
		  //修改为您自己的图片上传接口
		  xiuxiu.setUploadURL(BASEURL + '/uploadfiles/upload');
		  xiuxiu.setUploadType(2);
		  xiuxiu.setUploadDataFieldName("upload");		  
		  xiuxiu.onBeforeUpload = function (data, id)
		  {
		        var size = data.size;
		        if(size > 1024 * 1024)
		        { 
		            alert("图片不能超过1M"); 
		            return false; 
		        }
		        xiuxiu.setUploadArgs({'return':"xiuxiu",'remote_type':'wechatUploadimg','no_thumb':1,'MIAOCMS[Auth][User]':jQuery.cookie('MIAOCMS[Auth][User]')}, id);
		        return true; 
		  };
		  
		  xiuxiu.onUploadResponse= function(data,id) {
			  try {
				  var json=eval('(' + data + ')'); 
			  } catch (e) {
				  alert(data);
				  return;
			  }
			  
			  if( json.ret == -1 ) {
				  showErrorMessage(json.error);
			  }
			  else{
				  insertHtml('<img src="'+json.url+'" _src="'+json.url+'">');			  
				  publishController.close_dialog();
			  }
			  if( typeof(callback) == 'function' ) {					
					return callback(json.url);
			  }
		  };
		  xiuxiu.onClose = function(id){
			  publishController.close_dialog();
		  }
	  
		//publishController.open_dialog(BASEURL+'/uploadfiles/crop?inajax=1&src='+url,{'title':'编辑图片',width:960,zIndex:9999999999});
		/*var $dialogid = publishController.dialogid;
		jQuery('#'+$dialogid).on('hidden.bs.modal', function (e) {
				jQuery('#'+$dialogid).remove();
		});*/
	}
	else{
		showErrorMessage('图片拼图上传功能需要登录后才能使用');
	}
}

function isGif(url){

	var idx = url.indexOf('cache/remote/');
	if( idx > 0) {
		url = base64_decode(url.substring(idx+13) )
	}
	if( url.indexOf('.gif') > 0 || url.indexOf('wx_fmt=gif') > 0 ){
		return true;
	}
	else{
		return false;
	}	
}

function soogif_edit(_src){
			var _html = '<div id="iframeShade">'
							+'<div id="iframeContainer">'
								+'<iframe id="qiframe" src="http://www.soogif.com/html/toolapi/index.html?src='+_src+'&t=2" width="100%" height="540" frameborder="0"></iframe>'
									// +'<iframe id="qiframe" src="http://localhost:8080/html/toolapi/index.html?src='+_src+'&t=2" width="1280" height="540" frameborder="0"></iframe>'
								+'<span class="iframeClose">&times;</span>'
							+'</div></div>';
			$(_html).appendTo($("body"));
			$("#iframeShade").css({
				"display":"block",
				"position":"fixed",
				"left":"0",
				"z-index":"10000",
				"top":"0",
				"width":"100%",
				"height":"100%",
				"background":"rgba(0,0,0,0.3)"
			}).find("#iframeContainer").css({
				"position":"absolute",
				"left":"50%",
				"top":"50%",
				"-webkit-transform":"translate(-50%,-50%)",
				"-moz-transform":"translate(-50%,-50%)",
				"-ms-transform":"translate(-50%,-50%)",
				"transform":"translate(-50%,-50%)",
				"width":"85%",
				"height":"540px",
				// "background":"pink",
				"box-sizing":"border-box"
			}).find(".iframeClose").css({
				"position":"absolute",
				"right":"-45px",
				"top":"0",
				"width":"46px",
				"height":"46px",
				"font":'bold 39px/39px "PingFangSC-Regular","microsoft yahei"',
				"text-align":"center",
				"line-height":"41px",
				"background":"#F2395B",
				"color":"#fff",
				"cursor":"pointer"
			});
			$("body").delegate("#iframeShade #iframeContainer .iframeClose","click",function(){
				$("#iframeShade").remove();
			});
}

function edit_image(url){


	if( sso.check_userlogin() ) {

		if( isGif(url) ) {
			soogif_edit(url);
			return;
		}
		
		publishController.open_html_dialog('meitu-beautify');
		
		xiuxiu.setLaunchVars("titleVisible",0);
		xiuxiu.setLaunchVars("maxFinalWidth",600);
		
		xiuxiu.setLaunchVars("uploadBtnLabel", "上传", "beautify");
		
		xiuxiu.embedSWF("MeituContent",1,700,500,"beautify");
		  //修改为您自己的图片上传接口
		  xiuxiu.setUploadURL(BASEURL + '/uploadfiles/upload');
		  xiuxiu.setUploadType(2);
		  xiuxiu.setUploadDataFieldName("upload");		  
		  xiuxiu.onBeforeUpload = function (data, id)
		  {
		        var size = data.size;
		        if(size > 1024 * 1024)
		        { 
		            alert("图片不能超过1M"); 
		            return false; 
		        }
		        xiuxiu.setUploadArgs({'return':"xiuxiu",'url': url,'remote_type':'wechatUploadimg','no_thumb':1,'MIAOCMS[Auth][User]':jQuery.cookie('MIAOCMS[Auth][User]')}, id);
		        return true; 
		  };
		  xiuxiu.onInit = function ()
		  {
			  if(url) {
				  if( url.indexOf('remote.wx135.com') > 0 || (url.indexOf('.135editor.com') < 0  && url.indexOf('.wx135.com') < 0 ) ){
					  //外部图片
					  url = BASEURL + '/downloads/fetch_url?fu='+ encodeURIComponent(url); //如微信图片，由于crossdomain设置限制，在美图秀秀的flash中无法编辑
				  }
				  xiuxiu.loadPhoto(url);
			  }
			  publishController.focus_close(); // 此操作可修复360浏览器编辑图片时，显示不全的问题。
		  };
		  xiuxiu.onUploadResponse= function(data,id) {
			  try {
				  var json=eval('(' + data + ')'); 
			  } catch (e) {
				  alert(data);
				  return;
			  }
			  
			  
			  if( json.ret == -1 ) {
				  showErrorMessage(json.error);
			  }
			  else{
				  if( url && current_edit_img ) {
					//var new_url = json.fspath + '?_=' + parseInt(Math.random()*100000);
					  var new_url = json.url;
					  current_edit_img.src = new_url;
					  current_edit_img.setAttribute("_src",new_url);			  
					  
				  }
				  else{
					  insertHtml('<img src="'+json.url+'" _src="'+json.url+'">');	
				  }
				  publishController.close_dialog();
			  }
			  //alert("上传响应" + data); //可以开启调试
		  };
		  xiuxiu.onClose = function(id){
			  publishController.close_dialog();
		  }
	  
		//publishController.open_dialog(BASEURL+'/uploadfiles/crop?inajax=1&src='+url,{'title':'编辑图片',width:960,zIndex:9999999999});
		/*var $dialogid = publishController.dialogid;
		jQuery('#'+$dialogid).on('hidden.bs.modal', function (e) {
				jQuery('#'+$dialogid).remove();
		});*/
	}
	else{
		showErrorMessage('编辑图片属于付费功能。更换图片请直接双击图片');
	}
}
