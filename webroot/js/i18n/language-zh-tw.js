jQuery(function($){
	$.jslanguage = {
		selectAll: '全選',
		noSelect: '沒有選中項',
		confirm_trash: '您確認要刪除數據到回收站嗎？',
		confirm_delete: '您確認要徹底刪除數據嗎？',
		confirm_restore: '您確認要從回收站恢復數據嗎？',
		confirm_publish: '您確認要發布這些數據嗎？',
		confirm_batchEdit: '批量修改將會修改您選擇的所有行的值，您確認要進行批量修改嗎？',
		confirm_unpublish: '您確認要使這些發布的數據下線嗎？',
		cancel_fav_confirm:'您確認要刪除這條收藏的數據嗎？',
		needlogin:'需要登錄才能繼續操作',
		
		sure_delete: '您確認要刪除數據嗎？',
		
		dayNames: ['星期日','星期壹','星期二','星期三','星期四','星期五','星期六'],
		dayNamesShort: ['周日','周壹','周二','周三','周四','周五','周六'],
		dayNamesMin: ['日','壹','二','三','四','五','六'],
		weekHeader: '周',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: true,
		yearSuffix: '',
		noSelect: '沒有選中項',
		wysiswyg_mode: '不能在查看源代碼時插入',
		select_editor: '請先在編輯器中選擇要插入的位置',
		insert_portlet:'插入portlet',
		
		use_editor: '使用編輯器',
		destory_editor: '註銷編輯器'
	};
	
	/* jquery tools validator language. */
	if(typeof $.tools !='undefined' && typeof $.tools.validator !='undefined'){
		$.tools.validator.localize("zh", {
			'*'			: '請檢查輸入格式是否正確',
			':email'  	: '請輸入有效的郵箱地址。',
			':number' 	: '請輸入有效的數字。',
			':url' 		: '請輸入有效的網址。',
			'[max]'	 	: '最大值不大於$1',
			'[min]'		: '最小值不小於$1',
			'[required]'	: '此項必填，不允許為空。'
		});
	}
});
