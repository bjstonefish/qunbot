jQuery(function($){
	$.jslanguage = {
		selectAll: 'All',
		noSelect: 'Not Select Any Item',
		confirm_trash: 'Are you sure to delete this data to the trash?',
		confirm_delete: 'Are you sure to delete this data?',
		confirm_restore: 'Are you sure to restore this data?',
		confirm_publish: 'Are you sure to publish these data?',
		confirm_batchEdit: 'Will edit all select items,are you sure to batch edit these data?',
		confirm_unpublish: 'Are you sure to unpublish these data?',
		cancel_fav_confirm:'Are you sure to delete this favorite data?',
		needlogin:'You need to login before this action',
		
		sure_delete: 'Are you sure to delete this data?',
		
		dayNames: ['Sun','Mon','Tue','Wen','Thu','Fri','Sat'],
		dayNamesShort: ['Sun','Mon','Tue','Wen','Thu','Fri','Sat'],
		dayNamesMin: ['Sun','Mon','Tue','Wen','Thu','Fri','Sat'],
		weekHeader: 'Week',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: true,
		yearSuffix: '',
		wysiswyg_mode: 'Can not insert code in source mode.',
		select_editor: 'Please focus in the editor at first.',
		insert_portlet:'Insert Portlet',
		
		use_editor: 'Use Editor',
		destory_editor: 'Destory Editor'
	};
	
	
});
