var PLAT135_URL = "//www.135editor.com";

function strip_imgthumb_opr(imgurl){
	var idx = imgurl.indexOf('@');
	if(idx > 0) {
		return imgurl.substring(0,idx); // 返回从start到end的位置，不包含end的那个字母
	}
	return imgurl;
}

function getEditorHtml( outer ){
	
	jQuery( current_editor.selection.document ).find('p').each(function(){
		if(jQuery.trim(strip_tags(jQuery(this).html()))=="&nbsp;") {
			jQuery(this).html('<br/>');
		}
		else if(jQuery.trim( strip_tags(jQuery(this).html()) )=="") { //由于各种编辑操作可能使内容包含了多余的空段落标签，需要去除(不含图片，不含换行)。如“<p><span style="font-size: 12px; "></span></p>”
			//
			if(jQuery(this).find('img,audio,iframe,mpvoice,video').size() > 0) {
				return;
			}
			if(jQuery(this).find('br').size() > 0) {
				jQuery(this).html('<br/>');
			}
			else{
				if(!this.style.clear ) {
					jQuery(this).remove();
				}
			}
		}
	});
	
	clean_135helper();	
	
	var html = '';
	if( current_editor.getWxContent && !outer ) {
		
		html = current_editor.getWxContent();	
	}
	else{
		html = current_editor.getContent();
	}
	html = parse135EditorHtml( html );	
	//return html;
	// 最外层增加一个节点，粘贴微信时，就不会生成多余的空格。 	
	//return $.trim(html);
	return '<section data-role="outer" label="Powered by 135editor.com" style="font-family:微软雅黑;font-size:16px;">'+ $.trim(html) + '</section>';
}

UE.plugins["open135"] = function () {
    var me = this,editor=this;
    
    me.is_paid_user = true;
    
    var utils = baidu.editor.utils,
        Popup = baidu.editor.ui.Popup,
        Stateful = baidu.editor.ui.Stateful,
        editorui = baidu.editor.ui,
        uiUtils = baidu.editor.ui.uiUtils,
        UIBase = baidu.editor.ui.UIBase;
    var domUtils = baidu.editor.dom.domUtils;
    
    var editor_document = null;
    
    var loading_more = false; //正在加载
    var loading_filter = null;
    var first_load = true; // 第一次加载刷新或者登录
//    domUtils.on(window, ['scroll','resize'], function(){
//    	 var optsAutoFloatEnabled = me.options.autoFloatEnabled !== false,
//         topOffset = me.options.topOffset;
//
//	     //如果不固定toolbar的位置，则直接退出
//	     if(!optsAutoFloatEnabled){
//	         return;
//	     }
//	     
//    });
    
    domUtils.on( window, 'scroll', function (evt,el) {
    	hideColorPlan();
    });
    
    me.addListener( 'autoheightchanged', function( editor ) {
    	var container= me.container;
    	jQuery(container).parent().find('#style-overflow-list').height(jQuery(container).height()-80);
    });
    
    me.addListener("click", function (t, evt) {
    	showColorPlan();
    });
    
    
    me.addListener( 'ready', function( editor ) {
    	refreshStyle( editor );    	
    });
    
    function loadStyles( page ,filter ){
    	jQuery('#load-more-style').before('<div id="loading-style" style="margin:10px;color:red;text-align:center;"><img src="http://static.135editor.com/img/ajax/circle_ball.gif"> '+me.getLang("labelMap.isLoading")+'</div>');
    	
//    	console.log( me.options.page_url +'&inajax=1&page='+page+'&filter='+filter );
    	
    	jQuery.ajax({
    		async:true, 
    		type:'get',
    		url:  me.options.page_url +'&inajax=1&page='+page+'&filter='+filter,
    		success: function (data){
    			
    			jQuery('#style-overflow-list #loading-style').remove();
    			
    			if( first_load ) {
    				jQuery('#style-overflow-list').html(data);
    				jQuery('#style-overflow-list').scrollTop(0);
    				jQuery('#style-categories .active').removeClass('active');
    				jQuery('#style-categories .filter').data('page',null).data('status',null);
    				jQuery('#style-categories').removeAttr('data-status').data('page',2);
    				first_load = false;
    			}
    			else{
    				// jQuery('#load-more-style').remove();
    				var newStyles = jQuery('<div>'+data+'</div>').find('.editor-template-list > li');
    				
    				if(newStyles.size() > 0) {
    					var append_nums = 0;
    					newStyles.each(function() {
    						var $this = jQuery(this);
    						$this.addClass('mix').css({'display':'block'});
    						
    						if( jQuery('#style-overflow-list').find( "#"+$this.attr('id') ).size() == 0) {
    							append_nums ++ ;
    							jQuery('.editor-template-list').append( $this );
    							
    						}
    					});
    					loadedStyles();
    					
    					if(append_nums == 0) { loading_more = false;return true; }; //没有更多了					
    				}
    				else{
    					loading_filter.attr('data-status','done');					
    					loading_more = false;
    					return true; // 没有更多了
    				}
    			}
    			
    			loading_more = false; // 正在加载状态结束，待内容都置入页面之后。
    			loadedStyles();
    		},
    		dataType:"html"
    	});
    }

    function loadedStyles(){
    	var obj_id = '#edui'+ me.ui.id +'-styles';
    	setTimeout(function(){
    		jQuery( obj_id +' img.lazy').lazyload({
				effect: "fadeIn",
				container: obj_id +' #style-overflow-list',
				data_attribute:'src',
				threshold : 50,
				failure_limit:50
			});
    		jQuery( obj_id +'#style-overflow-list' ).trigger('scroll');
		},500);
    }
    function refreshStyle( editor ){
    	if( ! me.options.style_url || ! me.options.open_editor ) {
    		return ;
    	}
    	
    	editor_document = me.selection.document;
    	var uid = me.uid;
    	var container= me.container;
    	var width = jQuery(container).width();
    	
    	var style_width = me.options.style_width || 360;
    	
    	if(jQuery(container).find('.edui-editor-sidebar').size() > 0) {
			jQuery(container).find('.edui-editor-mainbar').css({'width':(width-style_width-2)+'px','border-left':'1px solid #ddd','margin-left': style_width+'px'});
			jQuery(container).find('.edui-editor-sidebar').css({'position':'relative','width':style_width+'px','float':'left','box-sizing':'border-box'});
			
			jQuery(container).find('.edui-editor-sidebar').html('<p style="line-height:32px;margin: 20px;"><img style="float:left;margin-right: 5px;" src="http://static.135editor.com/img/ajax/circle_ball.gif">'+me.getLang("labelMap.isLoading")+'</p>');
		}
		else{
			jQuery(container).parent().css({'position':'relative','padding-left': style_width+'px','box-sizing':'border-box'});
			container.style.width = (width-style_width) + "px";
		}
    	
    	setTimeout(function(){
	    	jQuery('<div></div>').load( me.options.style_url ,function(){
	    		// adapter/editor.js  <div id="##_sidebar" class="%%-sidebar">
	    		var html = jQuery(this).html();
	    		
	    		//var height = me.options.initialFrameHeight + 60;
	    		var obj_id = 'edui'+ me.ui.id +'-styles',
	    			obj = jQuery('<div id="'+obj_id+'" style="position:absolute;width:'+style_width+'px;left:0;top:0;">'+html+'</div>');
	    		
	    		if(jQuery(container).find('.edui-editor-sidebar').size() > 0) {
	    			jQuery(container).find('.edui-editor-sidebar').html('').append(obj);
	    		}
	    		else{
	    			jQuery(container).before(obj);
	    		}
	    		var height = jQuery(container).height() > 0 ? jQuery(container).height() : (me.options.initialFrameHeight + jQuery('.edui-editor-toolbarbox',container).height());
	    		
	    		//jQuery(container).find('.edui-editor-sidebar').css('height',height);
	    		
	    		obj.find('.editor-menus').height( height );
	    		obj.find('#editor-styles-content').css('height',height);
	    		
	    		obj.css('height',height).find('#style-overflow-list').height( height - jQuery('#copyright',obj).height() - jQuery('#style-categories',obj).height() -2 );
	    		
	    		obj.find('#styleSearchResultList').height( height - jQuery('#copyright',obj).height() -45 ); //减去标题栏与版权信息及搜索框的高度
	    		
	    		//jQuery('.editor-template-list > li',obj).addClass('mix');//.css('display','block');
	    		
	    		jQuery('#style-categories >li',obj).hover(function(){
	    			jQuery(this).addClass('open');
	    		},function(){
	    			jQuery(this).removeClass('open');
	    		});
	    		
        		jQuery(document).on('click','#style-categories .filter',function(){
        			
        			jQuery('.editor-template-list > li',obj).hide();//.fadeOut('slow','linear');
        			jQuery(jQuery(this).attr('data-filter'),obj).show();//.fadeIn('slow','linear');
        			
        			jQuery('#style-categories .filter',obj).removeClass('active');
        			jQuery(this).addClass('active');
        			
        			obj.find('#style-overflow-list').trigger('scroll').scrollTop(0);
        		});   
	    		
	        	
        		if( me.options.pageLoad ) {
        			jQuery('#style-overflow-list',obj).on('scroll',function(){

            			if( !loading_more && jQuery('#load-more-style').size() == 1 && jQuery('#load-more-style').position().top < jQuery('#style-overflow-list').height() + 150 ) {
            				
            				
            				if( jQuery('#style-categories a.active:first',obj).size() > 0 ) {
            					loading_filter = jQuery('#style-categories a.active:first');	
            					
            					if(loading_filter && loading_filter.attr('data-status') ) {
            						return true;
            					}
            					var page = loading_filter.data('page');
            					if( !page || typeof(page) == 'undefined' ) { page = 1;}
            					loading_more = true;
            					loadStyles( page , loading_filter.data('filter') );
            					loading_filter.data('page', parseInt(page) + 1);
            				}
            				else{
            					loading_filter = jQuery('#style-categories',obj);
            					if(loading_filter && loading_filter.attr('data-status') == 'done' ) {
            						return true;
            					}
            					var page = loading_filter.data('page');
                                if( !page || typeof(page) == 'undefined' ) { page = 1;}
            					loading_more = true;
            					loadStyles( page );
            					loading_filter.data('page',parseInt(page)+1);
            				}
            			}
            			return true;
            		}).trigger('scroll');
        		}
        		else{
        			loadedStyles();
        		}
	    		
	    		jQuery(obj).on('click','#editor-styles .editor-menus li',function(){
	    			
	    			jQuery(this).parent().find('li').removeClass('active'); //取消之前激活的选项
	    			jQuery(this).parent().next('.tab-content').find('.tab-pane').removeClass('active'); // 取消之前显示的选项内容
	    			
	    			jQuery(jQuery(this).find('a').attr('href'),obj).addClass('active'); // 显示对应的选项内容   			
	    			jQuery(this).addClass('active'); 
	    			
	    			var anchor = jQuery(this).find('a:first');
	    			var target = anchor.attr('href');
	    			if(anchor.data('target')){
	    				target = anchor.data('target');
	    			}
	    			
	    			function tab_loaded(){

	    				jQuery('.open-tpl-brush',target).attr('class','open-tpl-brush');
	    				jQuery('.insert-tpl-content',target).attr('class','insert-tpl-content');
	    				
						jQuery(target).find('a').click(function(){	
							var url = jQuery(this).attr('href');
							if(url.search(/\?/)!=-1){
								url+='&inajax=1';
							}
							else{
								url+='?inajax=1';
							}
							if(jQuery(this).attr('id') =='btn-search-image') {
								url += '&name='+encodeURI(jQuery('#SearchImageName',target).val());
							}
							
							var re = /^#/;
							if( typeof(jQuery(this).attr('onclick')) != "undefined" || jQuery(this).attr('target') =='_blank' || typeof(url) == "undefined" || re.test(url) || url.substr(0,10).toLowerCase()=='javascript'){
								return true; // 当为锚点，javascript,或者定义了onclick 时，忽略动作
								// 可以使用onclick="return true;"或者target="_selft"来忽略dialog中链接的绑定事件
							}
							jQuery(target).parent().scrollTop(0);
							jQuery(target).prepend('<section style="position:absolute;z-index:100;color: red;width:100%;height:100%;background-color:rgba(0,0,0,0.5);padding:10px;"><img src="http://static.135editor.com/img/ajax/circle_ball.gif"> '+me.getLang("labelMap.isLoading")+'</section>');
							jQuery(target).load(url,function(){ tab_loaded();});							
							return false;
						});
					}
	    			
	    			if(jQuery(target).html() == "" || jQuery(anchor).data('refresh') == "always") {	// 没有内容，或者设置了每次都刷新		
	    				if(anchor.data('url')) {
	    					jQuery(target).html('<section style="padding:10px;"><img src="http://static.135editor.com/img/ajax/circle_ball.gif"> '+me.getLang("labelMap.isLoading")+'</section>');
	    					jQuery(target).load(anchor.data('url'),function(){
	    						tab_loaded();
	    					});
	    				}
	    			}
	    			
	    			return false;
	    		}); 
	    		
	    		showColorPlan();
	
	    		jQuery(obj).on('click','#online-styles .col-sm-12,#user-style-list .col-sm-12,#style-overflow-list .ui-portlet-list > li,#styleSearchResultList #style_search_list > ul > li',function(){
	    			//data-135editor=\"true\"
	    			//多插入一个空行，防止有时无法选择到底部了，或者两个元素中间不方便插入内容。
	    			
	    			if(jQuery(this).hasClass('ignore')){
	    				return false;
	    			}
	    			
	    			var ret = false;
	    			var num = parseInt(jQuery(this).find('.autonum:first').text());
	    			
	    			var id = jQuery(this).data('id');
	    			
	    			
	    			jQuery(this).contents().filter(function() {
	    				return this.nodeType === 3 && jQuery.trim(jQuery(this).text()) == "";
	    			}).remove();
	    			jQuery(this).find('p').each(function(){
	    				if(jQuery.trim(jQuery(this).html())=="&nbsp;") {
	    					jQuery(this).html('<br/>');
	    				}
	    			});
	    			jQuery(this).find('*').each(function(){
	    				if(jQuery(this).attr('data-width')) {
	    					return;
	    				}
	    			
	    				if( this.style && this.style.width && this.style.width != "" ) {
	    					jQuery(this).attr('data-width',this.style.width);
	    				}
	    			});
	    			
	    			var style_item = jQuery(this).find('> ._135editor:first'); //第一级的
	    			
	    			if(style_item.size()){
	    				if(style_item.find('> *').size() == 1 && style_item.find('> *').eq(0).hasClass('135editor') ) {
	    					// 只包含一个一级子元素，且为135样式。说明代码本身包含了135editor的class标记，只需要插入即可，无需再增加外围代码。
	    					ret = insertHtml( style_item.html() );
	    				}
	    				else{
	    					var html = style_item.prop('outerHTML');
	    					ret = insertHtml( html ); 
	    				}
	    			}
	    			else{ //最外围包装135editor容器
	    				ret = insertHtml("<section data-id=\""+id+"\" class=\"_135editor\">" + jQuery(this).html() + "</section>"); 
	    			}
	
	    			if(ret){
	    				style_click(id);    				
	    				if(typeof num != "undefined") {
	    					jQuery(this).find('.autonum:first').text(num+1);
	    				}
	    			}
	    		});
	    		
	    		/* 点击模板时加载模板内容 */
	    		jQuery(obj).on('click','#system-template-list .insert-tpl-content',function(){    			
	    			var template_id = jQuery(this).data('id');  
	    			me.undoManger.save();
	    			if( jQuery.trim(me.getPlainTxt()) == "") {
	    				me.setContent( jQuery('#template-'+template_id).html() );
	    				me.undoManger.save();
	    			}
	    			else if(confirm( me.getLang("labelMap.confirmReplace") )) {
	    				me.setContent( jQuery('#template-'+template_id).html() );
	    				me.undoManger.save();
	    			}
	    		});

	    		jQuery(obj).on('click','#system-template-list .template-cover',function(){    			
	    			var template_id = jQuery(this).data('id');    			
	    			ajaxAction(PLAT135_URL+'/editor_styles/view/'+template_id+'.json?nolazy=1',null,null,function(request){
	    				
	    				if(request.data) {
	    					me.undoManger.save();
	    					if( jQuery.trim(me.getPlainTxt()) == "") {
	    						me.setContent(request.data.EditorStyle.content);
	    						me.undoManger.save();
	    					}
	    					else if(confirm( me.getLang("labelMap.confirmReplace") )) {
	    						me.setContent(request.data.EditorStyle.content);
	    						me.undoManger.save();
	    					}
	    					
	    				}
	    			});    			
	    		});

	jQuery(obj).on('click','.changeStyles',function(){
    	var type = jQuery(this).data('type');
    	if( type == 'opennew' ){
    		if( me.options.page_url.indexOf('type=opennew') > 0 ) {
    			return;
    		}
    		else{
                jQuery('#style-categories a.active').removeClass('active');
    			me.options.page_url += '&type=opennew';
    			$('#style-overflow-list .editor-template-list').remove();
    			jQuery('#style-categories',obj).data('page',1);
                first_load = true;//loadStyles( 1 );
                jQuery('#style-overflow-list',obj).trigger('scroll');
    		}
    	}
    	else if( me.options.page_url.indexOf('type=opennew') > 0 ) {
            jQuery('#style-categories a.active').removeClass('active');
            me.options.page_url = me.options.page_url.replace('&type=opennew','');
			$('#style-overflow-list .editor-template-list').remove();
			jQuery('#style-categories',obj).data('page',1);
            first_load = true;//loadStyles( 1 );
            jQuery('#style-overflow-list',obj).trigger('scroll');
    	}
    	//console.log(me.options.page_url);
    })

	    		/* 打开模板刷 */
	jQuery(obj).on('click','#system-template-list .open-tpl-brush',function(){
		
		var tid = jQuery(this).data('id');
		var obj = jQuery('#template-'+tid);
		jQuery('#template-contnet-brush').show().css('top',jQuery('#online-template').scrollTop());
		jQuery('#online-template').css('overflow','hidden');

		jQuery('#template-contnet-brush').html(jQuery('#template-'+tid).html()).prepend('<div id="close-template" style="position: absolute;cursor:pointer;right: 5px;color: #000;font-size:20px;">&times;</div>').find('#close-template').click(function(){
			jQuery('#online-template').css('overflow-y','auto');
			jQuery('#template-contnet-brush').hide();
		});
		/*var offset = jQuery('#style-operate-area').offset();
		jQuery('#tpl-brush-board').css('display','block').css('left',offset.left).css('top',offset.top);
		jQuery('#tpl-brush-board').width(jQuery('#insert-style-list').width());
		jQuery('#tpl-brush-board-content').height(jQuery('#insert-style-list').height()-70);*/

		/*** 绑定模板秒刷的事件 ***/
		jQuery('#template-contnet-brush').find('._135editor').addClass('clearfix').css({
		    'border':'1px solid #ddd','padding':'10px','margin':'5px 0'
		  }).prepend('<div class="tpl-brush-helper"><a href="javascript:void(0)"  class="btn btn-brush btn-xs btn-warning">秒刷此样式</a></div>');
		
		jQuery('#template-contnet-brush').find('.btn-brush').click(function(){
		    var style_item = jQuery(this).parents('._135editor:first').clone();
		    style_item.find('.tpl-brush-helper').remove();
		    style_item.find('._135editor').each(function(){
		    	jQuery(this).remove(); //在内嵌样式的对应位置添加空行，删除嵌套样式
		    });
		    
		    insertHtml("<section data-id=\""+style_item.data('id')+"\" class=\"_135editor\">" + style_item.html() + "</section>"); 
		});

		/*** 绑定模板悬浮效果 ***/
		jQuery('#template-contnet-brush').find('._135editor').hover(function(){
		  jQuery(this).css({
		    'border':'1px dotted red'//,'background-color':'#fafafa'
		  });
		  jQuery(this).find('._135editor').css('opacity',0.9);
		},function(){
			jQuery(this).find('._135editor').css('opacity',1);
		  jQuery(this).css({
		    'border':'1px solid #ddd'//,'background-color':'inherit'
		  })
		  //jQuery(this).find('.tpl-brush-helper').remove();
		})

	});
	    		
	    		/* 一键排版  */
	    		jQuery(obj).on('click','.html-parser-rule',function(){    			
	    			var rule_id = jQuery(this).data('id');
	    			var data = {html: me.getContent()};
	    			jQuery('#html-parsers-options :input',obj).each(function(){
	    				data[this.name] = this.value;
	    			});
	    			var parse_url = PLAT135_URL+'/html_parsers/parse/'+rule_id;
	    			if(me.options.appkey) {
						parse_url += '?appkey=' + me.options.appkey;
					}
	    			ajaxAction(parse_url,data,null,function(request){
	    				if( request.ret == 0 ) {
	    					me.undoManger.save();
	    					me.setContent(request.html);
	    					me.undoManger.save();
	    				}
	    			});    			
	    		});
	    		
	    		/* 刷新按钮  */
	    		jQuery(obj).on('click','#refresh-styles',function(){
	    			first_load = true;
	    			refreshStyle( editor )		
	    		});
	    		
	    		/* 在线图片 */
	    		jQuery(obj).on('click','#system-img-list .appmsg,#images-list .appmsg,.images-list .appmsg',function(){
	    			
	    			var $image = jQuery(this).find('img:first');
	    			var src = strip_imgthumb_opr($image.attr('src'));
	    			
	    			var range = me.selection.getRange();
	    	    	if (!range.collapsed) {
	    		        var img = range.getClosedNode();
	    		        if (img && img.tagName == 'IMG') { //选区为图片则切换图片地址
	    		        	img.src = src;
	    		        	img.setAttribute('_src',src);
	    		        	return;
	    				}
	    	    	}
	    			insertHtml("<img src=\""+src+"\">"); 
//	    			if( $image.data('id') ) {
//	    				style_click($image.data('id'));
//	    			}
	    			return false;
	    		});
	    		
	    		jQuery('#replace-color-all',obj).on('click',function(){
	    			if(this.checked){
	    				window.replace_full_color = true;
	    			}
	    			else{
	    				window.replace_full_color = false;
	    			}
	    		});	
	    		
	
	    		jQuery('#txtStyleSearch',obj).on('keyup blur focus',function(){
	    			var last = jQuery(this).data('last');
	    			var word = jQuery.trim(this.value);
	    			if( word == "" ||  word == " " ) {
	    				jQuery('#styleSearchResult',obj).hide();// 关闭搜索的显示
	    				return false;
	    			}
	    			if(last == word) {
	    				jQuery('#styleSearchResult',obj).show();
	    				return false; // 未变化
	    			}
	    			else {
	    				jQuery(this).data('last',word);
	    				jQuery('#styleSearchResult',obj).show();
	    				
	    				jQuery('#styleSearchResultList',obj).html(me.getLang("labelMap.isLoading"));
	    				jQuery('#styleSearchResultList',obj).load(PLAT135_URL+'/editor_styles/search?inajax=1&name='+word +' #style_search_list',function(data){
	    					;
	    				});	
	    			}
	    		});
	    		
	    		jQuery('.autonum',obj).on('mousewheel', function(event) {
	    			var num = parseInt(jQuery(this).html());
	    			if(event.deltaY < 0) { //向下滚动
	    				if(num <= 1) return ;
	    				jQuery(this).html( num - 1);
	    			}
	    			else{
	    				jQuery(this).html( num + 1);
	    			}
	    		    return false;
	    		});
	    		
	    		jQuery('.colorPicker',obj).colorPicker({
	    		    customBG: '#FFF',
	    		    size:2,
	    		    appenTo:'body',
	    		    mode:'hsv-h', //rgb-r
	    		    init: function(elm, colors) { // colors is a different instance (not connected to colorPicker)
	    		      elm.style.backgroundColor = elm.value;
	    		      elm.style.color = colors.rgbaMixCustom.luminance > 0.22 ? '#222' : '#ddd';
	    		    }
	    		    /*,displayCallback:function(colors, mode, options){
	    		    	setBackgroundColor(jQuery(this).val(),'#FFF', 'all');
	    		    }*/
	    		});
	    		jQuery('.colorPicker',obj).blur(function(){
	    			setBackgroundColor(this.value,'#FFF',true);
	    			this.style.backgroundColor = this.value;
	    			this.style.color = '#FFF';
	    		});
	    		jQuery('.colorPicker',obj).keyup(function(){
	    			if(this.value.search('#') == 0){
	    				if(this.value.length ==7 ) { //|| this.value.length ==4
	    					jQuery(this).trigger('focus.colorPicker');
	    				}
	    			}
	    			else{
	    				//alert(this.value.search('rgb'));alert(this.value.indexOf(")"));
	    				if( this.value.search('rgb') == 0 && this.value.indexOf(")") >0 ) {
	    					jQuery(this).trigger('focus.colorPicker');
	    				}
	    			}
	    		});
	    		
	    	});	
    	},1000);
    }
};

//jQuery(document).on('click',function(event){
//	var object = event.srcElement ? event.srcElement : event.target;
//	if(object && (jQuery(object).parents('#color-plan').size() > 0 || jQuery(object).parents('.cp-app').size() > 0 ) ) { //为调色板或者取色器时，不隐藏
//		// 跳过
//	}
//	else{
//		hideColorPlan();
//	}
//});


jQuery(document).on('click','.color-switch',function(event){
	jQuery('.color-switch').removeClass('active');
	jQuery(this).addClass('active');
	var color = jQuery(this).data('color'); //data-color为前景色，bgcolor为背景色，或者无背景文字的前景色
	var bgcolor = jQuery(this).css('backgroundColor');
	
	jQuery('#custom-color-text').val(bgcolor).css('backgroundColor',bgcolor);
	
	if(!color)  color = '#FFF';
	setBackgroundColor(bgcolor,color, true);	
	event.preventDefault();
	event.stopPropagation();
});



if(typeof(window.showSuccessMessage) == 'undefined') {
	//显示表单提交成功的信息
	window.showSuccessMessage = function (text)
	{
		if(current_editor) {
			current_editor.fireEvent('showmessage', {
		         'id': 'success-msg',
		         'content': text,
		         'type': 'success',
		         'timeout': 4000
		     });
		}
		else{
			alert(text);
		}
		return true;
	}
}

if(typeof(window.color_click) == 'undefined') {
	window.color_click  = function (color){
		color = hex2rgb(color);
		var url = PLAT135_URL+'/colors/click';
		ajaxAction(url,{color:color});
		return false;
	}
}
if(typeof(window.style_click) == 'undefined') {
	window.style_click = function (id){
		var url = PLAT135_URL+'/editor_styles/click_num';
		ajaxAction(url,{id:id});
		return false;
	}
}
if(typeof(window.setFavorColor) == 'undefined') {
	window.setFavorColor = function (colors,callback){
		var url = PLAT135_URL+'/editor_styles/setFavorColor';
		ajaxAction(url,{colors:colors},null,callback);
	}
}

if(typeof(window.showErrorMessage) == 'undefined') {
	
// 显示错误信息
	window.showErrorMessage = function (text)
	{
		if(current_editor) {
			current_editor.fireEvent('showmessage', {
		         'id': 'error-msg',
		         'content': text,
		         'type': 'error',
		         'timeout': 4000
		     });
		}
		else{
			alert(text);
		}
		return true;
	}
}

if(typeof(window.strip_imgthumb_opr) == 'undefined') {
    window.strip_imgthumb_opr = function (imgurl){
        var idx = imgurl.indexOf('@');
        if(idx > 0) {
            return imgurl.substring(0,idx); // 返回从start到end的位置，不包含end的那个字母
        }
        return imgurl;
    }
}

/**
 * url，为ajax提交的url。 要求返回为json数据 postdata 为要提交的数据， form 提交的表单
 * callback_func_name,回调函数名，要在rs_callbacks中定义，回调函数的第一个参数为ajax返回的结果 moreags
 * 传给回调函数的更多参数，参数格式可自定义，字符串、数组、对象等
 */
if(typeof(window.ajaxAction) == 'undefined') {
	window.ajaxAction = function (url,postdata,form,callback_func_name,moreags){
		if(url.search(/\?/)!=-1){
			url+='&inajax=1';
		}
		else{
			url+='?inajax=1';
		}
		if(form){
			jQuery(':submit',form).each(function(){
				var html = jQuery(this).html();
				jQuery(this).data('html',html).html('<img src="http://static.135editor.com/img/ajax/circle_ball.gif"> '+html).attr('disabled','disabled'); // 将按钮置为不可提交
			});
		}
		jQuery.ajax({
			// async:true,
			type:'post',
			url: url,
			data: postdata,
			complete:function (XMLHttpRequest, textStatus) {
				if(form){
					jQuery(':submit',form).each(function(){
						var html = jQuery(this).data('html');
						jQuery(this).html(html).removeAttr('disabled');
					}) // 将按钮置为可提交
				}
			},
			success: function(request){
				
				if(request.success){
					showSuccessMessage(request.success);
				}
				else if(request.error && !request.tasks){
					showErrorMessage(request.error);
					var errorinfo='';
					for(var i in request){
						errorinfo +="<span class='ui-state-error ui-corner-all'><span class='ui-icon ui-icon-alert'></span>"+request[i]+"</span>";
					}				
				}
				
				/* callback中可能重新显示 success时关闭的dialog */
				if(form){
					jQuery(':submit',form).each(function(){
						var html = jQuery(this).data('html');
						jQuery(this).html(html).removeAttr('disabled');
					}) // 将按钮置为可提交
				}
				
				if(typeof(callback_func_name)=='function'){
					callback_func_name(request);
				}
				else if(callback_func_name && rs_callbacks[callback_func_name]){
					var func = rs_callbacks[callback_func_name];
					if(moreags){
						func(request,moreags);
					}
					else{
						func(request);
					}
				}
			// tasks is a json object
			// tasks[i] is a json object that convert from a php array .
			// array('dotype','selector','content');
				
				// callback.apply(callback,callback_args);
				// //回调函数,callback_func_name为回调函数的函数名。如rs_callbacks.addtofavor()
						
				if(request.tasks){
					jQuery(request.tasks).each(function(i){
						var task = request.tasks[i];
						
	                	if(task.dotype=="html"){
	                		if(jQuery(task.selector,form).size()){
	                			jQuery(task.selector,form).html(task.content).show();
	                		}
	                		else{
	                			jQuery(task.selector).html(task.content).show();
	                		}
	                	}
	                	else if(task.dotype=="value"){
	                		if(jQuery(task.selector,form).size()){
	                			jQuery(task.selector,form).val(task.content);
	                		}
	                		else{
	                			jQuery(task.selector).val(task.content);
	                		}
	                	}
	                	else if(task.dotype=="append"){
	                		jQuery(task.content).appendTo(task.selector);
	                	}
	                	else if(task.dotype=="dialog"){
	                		jQuery(task.content).appendTo(task.selector);
	                	}
	                	else if(task.dotype=="location"){
	                		window.location.href = task.url;
	                		//window.location.reload();
	                	}
	                	else if(task.dotype=="reload"){
	                		window.location.reload();
	                	}
	                	else if(task.dotype=="jquery"){
	                		//alert(jQuery(task.selector)[task.function]);
	                		//jQuery(task.selector)[task.function].apply(task.args);
	                		if(task.args) {
	                			jQuery(task.selector)[task.func](task.args);
	                		}
	                		else{
	                			//alert( jQuery(task.selector)[task.function] )
	                			jQuery(task.selector)[task.func]();
	                		}
	                	}
	                	else if(task.dotype=="callback"){
	                		var callback = null,thisArg=null;
	                		eval( "callback= "+task.callback+";");
	                		eval( "thisArg= "+task.thisArg+";");
	                		var args = [];
	                		for(var i in task.callback_args){
	                			args[args.length]=task.callback_args[i];
	                		}
	                		if(callback){
	                			callback.apply(thisArg,args);
	                		}
	                	}
					});
				}
				
			},
			dataType:"json"
		});
		return false;
	}
}
