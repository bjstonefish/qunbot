;

(function(factory){if(typeof define==='function'&&define.amd){define(['jquery'],factory);}else if(typeof exports==='object'){module.exports=factory(require('jquery'));}else{factory(jQuery);}}(function($){var pluses=/\+/g;function encode(s){return config.raw?s:encodeURIComponent(s);}
function decode(s){return config.raw?s:decodeURIComponent(s);}
function stringifyCookieValue(value){return encode(config.json?JSON.stringify(value):String(value));}
function parseCookieValue(s){if(s.indexOf('"')===0){s=s.slice(1,-1).replace(/\\"/g,'"').replace(/\\\\/g,'\\');}
try{s=decodeURIComponent(s.replace(pluses,' '));return config.json?JSON.parse(s):s;}catch(e){}}
function read(s,converter){var value=config.raw?s:parseCookieValue(s);return $.isFunction(converter)?converter(value):value;}
var config=$.cookie=function(key,value,options){if(arguments.length>1&&!$.isFunction(value)){options=$.extend({},config.defaults,options);if(typeof options.expires==='number'){var days=options.expires,t=options.expires=new Date();t.setMilliseconds(t.getMilliseconds()+days*864e+5);}
return(document.cookie=[encode(key),'=',stringifyCookieValue(value),options.expires?'; expires='+options.expires.toUTCString():'',options.path?'; path='+options.path:'',options.domain?'; domain='+options.domain:'',options.secure?'; secure':''].join(''));}
var result=key?undefined:{},cookies=document.cookie?document.cookie.split('; '):[],i=0,l=cookies.length;for(;i<l;i++){var parts=cookies[i].split('='),name=decode(parts.shift()),cookie=parts.join('=');if(key===name){result=read(cookie,value);break;}
if(!key&&(cookie=read(cookie))!==undefined){result[name]=cookie;}}
return result;};config.defaults={};$.removeCookie=function(key,options){$.cookie(key,'',$.extend({},options,{expires:-1}));return!$.cookie(key);};}));;

jQuery(function($){$.jslanguage={selectAll:'全选',noSelect:'没有选中项',confirm_trash:'您确认要删除数据到回收站吗？',confirm_delete:'您确认要彻底删除数据吗？',confirm_restore:'您确认要从回收站恢复数据吗？',confirm_publish:'您确认要发布这些数据吗？',confirm_batchEdit:'批量修改将会修改您选择的所有行的值，您确认要进行批量修改吗？',confirm_unpublish:'您确认要使这些发布的数据下线吗？',cancel_fav_confirm:'您确认要取消收藏这条数据吗？',needlogin:'登录',sure_delete:'您确认要删除数据吗？',dayNames:['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],dayNamesShort:['周日','周一','周二','周三','周四','周五','周六'],dayNamesMin:['日','一','二','三','四','五','六'],weekHeader:'周',dateFormat:'yy-mm-dd',firstDay:1,isRTL:false,showMonthAfterYear:true,yearSuffix:'',noSelect:'没有选中项',wysiswyg_mode:'不能在查看源代码时插入',select_editor:'请先在编辑器中选择要插入的位置',insert_portlet:'插入portlet',use_editor:'使用编辑器',destory_editor:'注销编辑器'};if(typeof $.validator!='undefined'){$.extend($.validator,{messages:{required:"此项必填，不允许为空。",remote:"请检查输入.",email:"请输入有效的邮箱地址",url:"请输入有效的网址",date:"请输入有效的日期",dateISO:"请输入有效的ISO日期",number:"请输入有效的数字",digits:"只允许输入整数",creditcard:"请输入有效的信用卡.",equalTo:"请输入相同的值.",maxlength:$.validator.format("最多输入{0}字符"),minlength:$.validator.format("最少输入{0}字符"),rangelength:$.validator.format("输入内容在{0} ~ {1} 字符"),range:$.validator.format("输入值允许范围为 {0} ~ {1}"),max:$.validator.format("输入值不能大于{0}."),min:$.validator.format("输入值不能小于 {0}.")},});};if(typeof $.tools!='undefined'&&typeof $.tools.validator!='undefined'){$.tools.validator.localize("zh",{'*':'请检查输入格式是否正确',':email':'请输入有效的邮箱地址。',':number':'请输入有效的数字。',':url':'请输入有效的网址。','[max]':'最大值不大于$1','[min]':'最小值不小于$1','[required]':'此项必填，不允许为空。'});}});;

var swfu_array=[];var ckeditors={};var swfu_array=[];var last_open_dialog=null;var jqgrid_scrollOffset=null;var form_submit_flag_for_swfupload=false;var form_submit_obj_for_swfupload=null;function singleSubmitDigg(model,data_id,question_id,option_id,callback)
{var postdata={model:model,data_id:data_id};postdata['options['+question_id+']['+option_id+']']=1;$.ajax({type:'post',url:BASEURL+'/appraiseresults/singlesubmit',data:postdata,success:function(data){if(data.error){alert(data.error);return false;}
if(typeof(callback)=='function'){var obj=callback(data);}},dataType:'json'});return false;}
function setDiggNum(data,total)
{var id='#Digg-'+data.model+'-'+data.data_id+'-'+data.question_id+'-'+data.option_id;$(id).html('('+data.value+')');if(total){var bar='#Diggbar-'+data.model+'-'+data.data_id+'-'+data.question_id+'-'+data.option_id;var p=data.value/total*100;$(bar).attr("width",p);}
else{var bar='#Diggbar-'+data.model+'-'+data.data_id+'-'+data.question_id+'-'+data.option_id;if($(bar).size()>0)
{var total=0;$("[id^='Digg-"+data.model+"-"+data.data_id+"-']").each(function(){var num=$(this).html();num=num.replace(/\(|\)/g,'');total+=parseInt(num);});$("[id^='Diggbar-"+data.model+"-"+data.data_id+"-']").each(function(){var numid=this.id.replace(/Diggbar-/,'Digg-');var num=$("#"+numid).html();num=num.replace(/\(|\)/g,'');var height=parseInt(num);var p=height/total*100;$('span',this).css("width",p);$(this).next().html(p.toFixed(2)+'%');});}}}
function keep_session(){var url=BASEURL+'/users/keep_session';ajaxActionHtml(url,null,function(html){});}
var check_interval=null;function getLoginQr(){$('.refresh_qrcode_mask').hide();ajaxAction(BASEURL+'/users/getLoginQr',null,null,function(request){if(request.ret==0){$('#wechat-qr-image').attr('src','https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket='+request.ticket);if(check_interval){clearInterval(check_interval);}
$('.refresh-logqr-tips').html('扫码关注后，自动登录');var scene_id=request.scene_id;var expriretimeout=setTimeout(function(){$('.refresh_qrcode_mask').show();$('.refresh-logqr-tips').html('二维码过期，点击刷新');clearInterval(check_interval);},60000);check_interval=setInterval(function(){ajaxAction(BASEURL+'/users/checkQrLogin/'+scene_id,null,null,function(request){if(request.ret==0){clearInterval(check_interval);clearTimeout(expriretimeout);}
else if(request.ret<0){clearInterval(check_interval);clearTimeout(expriretimeout);$('.refresh_qrcode_mask').show();$('.refresh-logqr-tips').html(request.msg);}});},3000);}
else{$('#login-Tabs li:eq(1) a').tab('show');showErrorMessage('登录二维码生成错误，请用账号密码登录或者第三方账号登录中的微信登录。<br/>'+request.errmsg);}});};var rs_callbacks={loginSuccess:function(request){if(request.ret==0){publishController.close_dialog();$('#login_errorinfo').hide();if(sso.form){$(sso.form).trigger("submit");sso.form=null;}
if(sso.callback){sso.callback.apply(sso.callback,sso.callback_args);}
$('#login-menus').removeClass('hidden');$('#login-links').addClass('hidden');$('#login-user-name').html(request.userinfo.username);$('#login-user-id').html(request.userinfo.id);keep_session();showSuccessMessage(request.msg);if(window.location.href.indexOf('/users/login')>=0){window.location.reload();}}
else{}},addtoCart:function(request){$('#shopping-cart-num').html(request.total);if(request.msg){if(request.ret==0){showSuccessMessage(request.msg);}
else{showErrorMessage(request.msg);}}
else if(request.success){showSuccessMessage(request.success);}},deleteFromCart:function(request){if(request.msg){if(request.ret==0){showSuccessMessage(request.msg);}
else{showErrorMessage(request.msg);}}
else if(request.success){showSuccessMessage(request.success);}},deleteGridRow:function(request,obj){$(obj).closest('tr.jqgrow').remove();if(request.msg){if(request.ret==0){showSuccessMessage(request.msg);}
else{showErrorMessage(request.msg);}}
else if(request.success){showSuccessMessage(request.success);}},reloadGrid:function(request,obj){var grid=$(obj).closest('table.jqgrid-list').remove();var page=grid.jqGrid("getGridParam","page");grid.jqGrid("setGridParam",{page:page}).trigger("reloadGrid");if(request.msg){if(request.ret==0){showSuccessMessage(request.msg);}
else{showErrorMessage(request.msg);}}
else if(request.success){showSuccessMessage(request.success);}}};function ajaxAction(url,postdata,form,callback_func_name,moreags){if(url.search(/\?/)!=-1){url+='&inajax=1';}
else{url+='?inajax=1';}
if(form){$(':submit',form).each(function(){var html=$(this).html();$(this).data('html',html).html('<img src="'+BASEURL+'/img/ajax/circle_ball.gif"> '+html).attr('disabled','disabled');});}
$.ajax({type:'post',url:url,data:postdata,complete:function(XMLHttpRequest,textStatus){if(form){$(':submit',form).each(function(){var html=$(this).data('html');$(this).html(html).removeAttr('disabled');})}},success:function(request){if(typeof($(form).data('callback'))=='function'){var func=$(form).data('callback');if(moreags){func(request,moreags);}
else{func(request);}}
else if(typeof(callback_func_name)=='function'){callback_func_name(request);}
else if(callback_func_name&&rs_callbacks[callback_func_name]){var func=rs_callbacks[callback_func_name];if(moreags){func(request,moreags);}
else{func(request);}}
else{if(request.msg){if(request.ret==0){showSuccessMessage(request.msg);}
else{showErrorMessage(request.msg);}}
else if(request.success){showSuccessMessage(request.success);}
else if(request.error&&!request.tasks){showErrorMessage(request.error);var errorinfo='';for(var i in request){errorinfo+="<span class='ui-state-error ui-corner-all'><span class='ui-icon ui-icon-alert'></span>"+request[i]+"</span>";}}}
if(form){$(':submit',form).each(function(){var html=$(this).data('html');$(this).html(html).removeAttr('disabled');})}
if(request.tasks){$(request.tasks).each(function(i){var task=request.tasks[i];if(task.dotype=="html"){if($(task.selector,form).size()){$(task.selector,form).html(task.content).show();}
else{$(task.selector).html(task.content).show();}}
else if(task.dotype=="value"){if($(task.selector,form).size()){$(task.selector,form).val(task.content);}
else{$(task.selector).val(task.content);}}
else if(task.dotype=="append"){$(task.content).appendTo(task.selector);}
else if(task.dotype=="msg_dialog"){publishController.open_msg_dialog(task.id,task.title,task.content);}
else if(task.dotype=="close_dialog"){publishController.close_dialog();}
else if(task.dotype=="location"){window.location.href=task.url;}
else if(form&&task.dotype=="reset"){$(form).trigger('reset');}
else if(task.dotype=="reload"){window.location.reload();}
else if(task.dotype=="jquery"&&task.func&&typeof($(task.selector)[task.func])=='function'){if(task.args){$(task.selector)[task.func](task.args);}
else{$(task.selector)[task.func]();}}
else if(task.dotype=="rscallback"){var func=rs_callbacks[task.callback];if(moreags){func(request,moreags);}
else{func(request);}}
else if(task.dotype=="callback"){var callback=null,thisArg=null;eval("callback= "+task.callback+";");eval("thisArg= "+task.thisArg+";");var args=[];for(var i in task.callback_args){args[args.length]=task.callback_args[i];}
if(callback){callback.apply(thisArg,args);}}});}},dataType:"json"});return false;}
function ajaxActionHtml(url,selector,callback){$.ajax({async:true,type:'get',url:url,success:function(data){if(selector){$(selector).html(data);}
if(typeof(callback)=='function'){callback(data);}
else if(callback){eval(callback);}},dataType:"html"});}
function setCKEditorVal(form)
{if(typeof(CKEDITOR)!='undefined'){if(form){$(form).find('textarea').each(function(i){if(this.id&&this.id!=""){var oEditor=CKEDITOR.instances[this.id];if(typeof(oEditor)!='undefined'){var content=oEditor.getData();$(this).val(content);}}});}
else{$('form .wygiswys').find('textarea').each(function(){var oEditor=CKEDITOR.instances[this.id];if(typeof(oEditor)!='undefined')
{var content=oEditor.getData();$(this).val(content);}});}}}
function ajaxSubmitForm(form,callback_func_name)
{setCKEditorVal(form);$(form).find('.help-block').html('');ajaxAction(form.action,$(form).serialize(),form,callback_func_name);return false;}
function clearUserCookie(){$.cookie.raw=true;$.removeCookie('MIAOCMS[Auth][User]',{'path':'/'});$.removeCookie('MIAOCMS[Auth][User]',{'domain':document.domain,'path':'/'});if(domain_host){$.removeCookie('MIAOCMS[Auth][User]',{'domain':domain_host,'path':'/'});}
var arr=document.domain.split('.');if(arr.length>2){arr.shift();var topdomain=arr.join('.');$.removeCookie('MIAOCMS[Auth][User]',{'domain':topdomain,'path':'/'});}
$.cookie.raw=false;showSuccessMessage('清除Cookie成功');return false;}
var sso={usercookie:$.cookie('MIAOCMS[Auth][User]'),form:null,callback:null,callback_args:null,check_userlogin:function(params){this.usercookie=$.cookie('MIAOCMS[Auth][User]');if(this.usercookie==null||this.usercookie==""||typeof(this.usercookie)=='undefined'){if(params&&params.callback){this.callback=params.callback;}else{this.callback='';}
if(params&&params.form){this.form=params.form;}else{this.form=null;}
if(params&&params.callback_args){this.callback_args=params.callback_args;}else{this.callback_args='';}
var url=BASEURL+'/users/login';if(params&&params.query){params.query+='&return=html';url+='?'+params.query;}
else{url+='?return=html';}
publishController.open_dialog(url,{'title':$.jslanguage.needlogin,'id':'user-login-dialog',callback:getLoginQr,selector:'#login-form',width:550});return false;}
return true;},is_login:function(){this.usercookie=$.cookie('MIAOCMS[Auth][User]');if(this.usercookie==null||this.usercookie==""||typeof(this.usercookie)=='undefined'){return false;}
return true;}};function checkAll(obj,selector){if(obj.checked){$(selector).find(':checkbox').prop('checked',true);}
else{$(selector).find(':checkbox').prop('checked',false);}}
function checkLogin(params){if(!sso.check_userlogin(params)){return false;}
else{ajaxAction('/users/login','loginSuccess');return false;}}
var publishController={_crontroller:'questions',dialogid:null,overlays:{},loading_html:'<img src="/img/ajax/wheel_throbber.gif"> 正在加载...',open_dialog:function(url,options,postdata){var $dialog=this;if(options==null){options={};}
$dialog.loading_html='<img src="'+BASEURL+'/img/ajax/wheel_throbber.gif"> 正在加载...';var lastid=$dialog.dialogid;if(options.id){$dialog.dialogid=options.id;}
else{$dialog.dialogid=url.replace(/\/|\.|:|,| |\{|\}|\]|\[|\"|\'|\?|=|&/g,'_')+'-ajax—action';}
function dialog_loaded(){$('.nav-tabs a','#'+$dialog.dialogid).click(function(e){e.preventDefault();$(this).blur();$(this).tab('show');});$('.nav-tabs a:first','#'+$dialog.dialogid).tab('show');$('.dropdown-toggle','#'+$dialog.dialogid).dropdown();$('button','#'+$dialog.dialogid).addClass('btn');$('[data-toggle="tooltip"],.showtooltip','#'+$dialog.dialogid).tooltip({container:'#'+$dialog.dialogid,html:true});$('[data-toggle="popover"]','#'+$dialog.dialogid).popover({html:true});if(typeof(options.callback)=='function'){options.callback($("#"+$dialog.dialogid));}
$("#"+$dialog.dialogid).find('form').each(function(){if(typeof($(this).attr("target"))!='undefined'){return true;}
if(typeof($(this).attr('onsubmit'))=="undefined"){if($.fn.validate){$(this).data('submitCallback',options.submitCallback)
validateForm(this);}
else{$(this).on('submit',function(){ajaxSubmitForm(this,function(request){if(request.ret==0){showSuccessMessage(request.msg);if(typeof(options.submitCallback)=='function'){options.submitCallback(request);}
$("#"+$dialog.dialogid).modal('hide');}});return false;})}}})}
function load_url(){$("#"+$dialog.dialogid).find('a').unbind('click').click(function(){var url=$(this).attr('href');var re=/^#/;if($(this).attr("target")=="_blank"||re.test(url)||typeof($(this).attr('onclick'))!="undefined"||url.substr(0,10).toLowerCase()=="javascript"){return true;}
$("#"+$dialog.dialogid).find('.modal-body').load(url,function(){load_url();});return false;});dialog_loaded();page_loaded();}
if($("#"+$dialog.dialogid).size()<1){$('<div  class="modal" id="'+$dialog.dialogid+'"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h3 id="myModalLabel">'+options.title+'</h3></div><div class="modal-body">'+$dialog.loading_html+'</div></div></div></div>').appendTo("body");}
else{$("#"+$dialog.dialogid).find('.modal-body').html($dialog.loading_html);}
$("#"+$dialog.dialogid).find('.modal-body').css('max-height',$(window).height()-80).css('overflow-y','auto');if(postdata!=null){$.ajax({type:"post",url:url,data:postdata,complete:function(XMLHttpRequest,textStatus){},success:function(html){if(options.selector&&$(html).find(options.selector).size()>0){$("#"+$dialog.dialogid).find('.modal-body').html($(html).find(options.selector).html());}
else{$("#"+$dialog.dialogid).find('.modal-body').html(html);}
load_url();},dataType:"html"});}
else{if(options.selector){var obj=$("#"+$dialog.dialogid).find('.modal-body').load(url+' '+options.selector,{},function(){load_url();});}
else{var obj=$("#"+$dialog.dialogid).find('.modal-body').load(url,{},function(){load_url();});}}
if(options.nobackdrop){$("#"+$dialog.dialogid).modal({backdrop:false});}
else{$("#"+$dialog.dialogid).modal({'backdrop':'static','keyboard':true});}
if(options.hidden=='remove'){$("#"+$dialog.dialogid).on('hidden.bs.modal',function(e){$("#"+$dialog.dialogid).remove();});}
if(options.width){$("#"+$dialog.dialogid+' .modal-dialog').width(options.width);}
if(options.zIndex){$("#"+$dialog.dialogid).css('z-index',options.zIndex);}
return false;},load_url:function(url,selector){var $dialog=this;var re=/^#/;if(re.test(url)||url.substr(0,10).toLowerCase()=='javascript'){return false;}
if(selector){url+=' '+selector;}
$("#"+$dialog.dialogid).load(url,function(){$("#"+$dialog.dialogid).find('a').click(function(){$dialog.load_url($(this).attr('href'),{});return false;});page_loaded();});},focus_close:function(){if(this.dialogid){$("#"+this.dialogid).find('.modal-header .close').focus();}},remove:function(){if(this.dialogid){$('.modal-backdrop').remove();$("#"+this.dialogid).remove();}},close_dialog:function(){if(this.dialogid){$("#"+this.dialogid).modal('hide');$('.modal-backdrop').remove();}},open_msg_dialog:function(dialogid,title,content){this.dialogid=dialogid;$('<div  class="modal fade" id="'+dialogid+'"><div class="modal-dialog"><div class="modal-content"><div class="modal-header">'+title+'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h3 id="myModalLabel"></h3></div><div class="modal-body">'+content+'</div></div></div></div>').appendTo("body");$("#"+dialogid).modal('show');$("#"+dialogid).modal({backdrop:false});$("#"+dialogid).on('shown.bs.modal',function(e){$('.modal-dialog',"#"+dialogid).css({'margin-top':($(window).height()-$('.modal-dialog',"#"+dialogid).height())/2})}).on('hidden.bs.modal',function(e){$("#"+dialogid).remove();});},open_html_dialog:function(dialogid,title){this.dialogid=dialogid;if($("#"+this.dialogid).size()<1){$('<div  class="modal fade" id="'+this.dialogid+'"><div class="modal-dialog"><div class="modal-content"><div class="modal-header">'+title+'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h3 id="myModalLabel"></h3></div><div class="modal-body"></div></div></div></div>').appendTo("body");}
$("#"+dialogid).modal();},invite_tabs:{}};function inner_open(container_selector,result_selector){$(container_selector).find('a').click(function(){var url=$(this).attr('href');if(result_selector){url=url+' '+result_selector;}
var re=/^#/;if(typeof($(this).attr('onclick'))!="undefined"||$(this).attr("target")=='_blank'||re.test(url)||url.substr(0,10).toLowerCase()=='javascript'){return true;}
$(container_selector).scrollTop(0).prepend('<div style="margin:10px;"><img src="http://static.135editor.com/img/ajax/circle_ball.gif"> 正在加载内容...</div>').load(url,function(){page_loaded();inner_open(container_selector,result_selector);});return false;});}
function follow_wx(id)
{var url=BASEURL+'/user_follows/follow/'+id;sso.callback=follow_wx;sso.callback_args=arguments;publishController.open_dialog(url,{'title':'Follow',width:400});return false;}
function addtofavor(model,id,obj,callback)
{var url=BASEURL+'/favorites/add/';var postdata={'data[Favorite][model]':model,'data[Favorite][data_id]':id};sso.callback=addtofavor;sso.callback_args=arguments;var evt=window.event||arguments.callee.caller.arguments[0];evt.stopPropagation();if(typeof(callback)=='function'){ajaxAction(url,postdata,null,callback);}
else{ajaxAction(url,postdata,null,function(request){if(request.ret==0){if(window.localStorage){localStorage.setItem(model+"_newfavor",id);}
if(obj){$(obj).attr('title','取消收藏').attr('data-original-title','取消收藏').removeClass('text-primary').addClass('text-success');if($(obj).find('.fa-heart-o').size()>0){$(obj).find('.fa-heart-o').removeClass('fa-heart-o').addClass('fa-heart');}}
var num_selector='#Stats-'+request.data.model+'-'+request.data.data_id+'-favor_nums';if($(num_selector).size()>0){var num=$(num_selector).html();num=num.replace(/\(|\)/g,'');if(num=='')num=0;num=parseInt(num);num++;$(num_selector).html(num);}}
else{showErrorMessage(request.msg);}});}
return false;}
function removefavor(model,id,obj,callback){var evt=window.event||arguments.callee.caller.arguments[0];evt.stopPropagation();var url=BASEURL+'/favorites/cancelByMid/'+model+'/'+id;sso.callback=removefavor;sso.callback_args=arguments;ajaxAction(url,null,null,function(request){if(request.ret==0){if(typeof(callback)=='function'){callback(request);}
$(obj).attr('title','收藏此项').attr('data-original-title','收藏此项');if($(obj).find('.fa-heart').size()>0){$(obj).find('.fa-heart').removeClass('fa-heart').addClass('fa-heart-o');}
else{$(obj).parents('li:first').remove();$('.tooltip.in').remove();$('.popover.in').remove();}}});return false;}
function unfavorite(obj,callback){var evt=window.event||arguments.callee.caller.arguments[0];evt.stopPropagation();var url=$(obj).data('url');sso.callback=unfavorite;sso.callback_args=arguments;ajaxAction(url,null,null,function(request){if(request.ret==0){if(typeof(callback)=='function'){callback(request);}
$(obj).attr('disabled','disabled');if($(obj).find('.fa-heart').size()>0){$(obj).find('.fa-heart').removeClass('fa-heart').addClass('fa-heart-o');}
else{$(obj).parents('li:first').remove();$('.tooltip.in').remove();$('.popover.in').remove();}}});return false;}
function refreshfavorite(obj){var evt=window.event||arguments.callee.caller.arguments[0];evt.stopPropagation();var url=$(obj).data('url');sso.callback=refreshfavorite;sso.callback_args=arguments;ajaxAction(url,null,null,function(request){if(request.ret==0){$(obj).parents('li:first').prependTo($(obj).parents('ul:first'));showSuccessMessage(request.msg);}});return false;}
function addtoCart(id,num,model)
{if(!model){model='product';}
var url=BASEURL+'/carts/add';var postdata={'data[Cart][num]':num,'data[Cart][product_id]':id,'data[Cart][product_model]':model};sso.callback=addtoCart;sso.callback_args=arguments;ajaxAction(url,postdata,null,'addtoCart');return false;}
function praiseBuy(model,id){var content='<form method="POST" target="_blank" action="/carts/add?redirect=/orders/info">';var prices=[2,4,6,8,10];content+='<div class="btn-group btn-group-lg">';for(var i in prices){if(prices[i]==6){content+='<label class="consignee-item"><div class="consignee-box active" style="height:60px;width:98px;"><input type="radio" checked="checked" name="data[Cart][price]" value="'+prices[i]+'" >'+prices[i]+'元<div class="inok"><img src="/img/common/arrow-red.png" alt=""></div></div></label>&nbsp;';}
else{content+='<label class="consignee-item"><div class="consignee-box" style="height:60px;width:98px;"><input type="radio" name="data[Cart][price]" value="'+prices[i]+'" >'+prices[i]+'元<div class="inok"><img src="/img/common/arrow-red.png" alt=""></div></div></label>&nbsp;';}}
content+='</div>';content+='<input type="hidden" name="data[Cart][num]" value="1">';content+='<input type="hidden" name="data[Cart][product_id]" value="'+id+'">';content+='<input type="hidden" name="data[Cart][product_model]" value="'+model+'">';content+='<p style="text-center"><input class="btn btn-primary" type="submit" value="打赏转载"></p>';content+='</form>';publishController.open_msg_dialog('praise-'+model,'打赏转载',content)}
function directBuy(id,num,model)
{if(!model){model='product';}
var url=BASEURL+'/carts/add';var postdata={'data[Cart][num]':num,'data[Cart][product_id]':id,'data[Cart][product_model]':model};sso.callback=directBuy;sso.callback_args=arguments;ajaxAction(url,postdata,null,function(){location.href=BASEURL+'/orders/info?ck_ids='+id;});return false;}
function loadDiggData()
{var models=[];var ids={};$('.ui-dig-num').each(function(i){var info=this.id.split('-');if($.inArray(info[1],models)<0){models[models.length]=info[1];}
if(!ids[info[1]]){ids[info[1]]=[];}
if($.inArray(info[2],ids[info[1]])<0){var id_length=ids[info[1]].length;ids[info[1]][id_length]=info[2];}});for(var i in models){var model=models[i];var data_id=ids[models];loadModelDataDigg(model,data_id);}}
function loadModelDataDigg(model,data_id)
{$.ajax({type:'get',url:BASEURL+'/appraiseresults/getdigdata',data:{'model':model,'data_id':data_id},success:function(data){for(var i in data){setDiggNum(data[i]['Appraiseresult']);}},dataType:'json'});}
function loadStatsData()
{var models=[];var ids={};$('.ui-stats-num').each(function(i){var s=this.id;var info=s.split('-');if($.inArray(info[1],models)<0){models[models.length]=info[1];}
if(!ids[info[1]]){ids[info[1]]=[];}
if($.inArray(info[2],ids[info[1]])<0){var id_length=ids[info[1]].length;ids[info[1]][id_length]=info[2];}});for(var i in models){var model=models[i];var data_id=ids[model];if(model&&model!=""){loadModelDataStats(model,data_id);}}}
function loadModelDataStats(model,data_id)
{$.ajax({type:'get',url:BASEURL+'/stats_days/getdata',data:{'model':model,'data_id':data_id},success:function(data){for(var i in data){setStatsNum(data[i]['StatsDay']);}},dataType:'json'});}
function setStatsNum(data)
{if(parseInt(data.favor_nums)>0){var id='#Stats-'+data.model+'-'+data.data_id+'-favor_nums';setQuoteNum(id,data.favor_nums);}
if(parseInt(data.comment_nums)>0){var id='#Stats-'+data.model+'-'+data.data_id+'-comment_nums';setQuoteNum(id,data.comment_nums);}
if(parseInt(data.view_nums)>0){var id='#Stats-'+data.model+'-'+data.data_id+'-view_nums';setQuoteNum(id,data.view_nums);}}
function setQuoteNum(select,value){if($(select).size()>0){var num=$(select).eq(0).html();num=num.replace(/&nbsp;| /g,'');num=num.replace(/\(|\)/g,'');if(num=='')num=0;num=parseInt(num);num=num+parseInt(value);$(select).html(num);}}
function loadModelDataMood(model,data_id)
{$.ajax({type:'get',url:BASEURL+'/appraiseresults/getdigdata',data:{'model':model,'data_id':data_id},success:function(data){var total=0;for(var i in data){if(data[i]['Appraiseresult'].question_id==3){total+=parseInt(data[i]['Appraiseresult'].value);}}
for(var i in data){setDiggNum(data[i]['Appraiseresult'],total);}},dataType:'json'});}
function loadComments(model,id)
{$.get(BASEURL+'/comments/get_comments_data/'+model+'/'+id,{},function(comments){var current=null;var commentstarget='.comments-'+model+'-'+id;$(commentstarget).html('');for(var i=0;i<comments.length;i++){current=comments[i].Comment;var comment_html='<li><span class="t">'+current.name+'  '+current.created+'</span><p>'+current.body+'</p><li>';$(commentstarget).append(comment_html);}},"json");}
function loadMoodDigg(model,id){$.get(BASEURL+'/appraises/load/3/'+id+'/'+model+'?inajax=1',{},function(MoodData){$('#mood-'+model+'-'+id).html(MoodData);},"html");}
var page_hash={storedHash:'',currentTabHash:'',cache:'',interval:null,listen:true,startListening:function(){setTimeout(function(){page_hash.listen=true;},600);},stopListening:function(){page_hash.listen=false;},checkHashChange:function(){var locStr=page_hash.currHash();if(page_hash.storedHash!=locStr){if(page_hash.listen==true)page_hash.refreshToHash();page_hash.storedHash=locStr;}
if(!page_hash.interval)page_hash.interval=setInterval(page_hash.checkHashChange,500);},refreshToHash:function(locStr){if(locStr)var newHash=true;locStr=locStr||page_hash.currHash();var hash_array=locStr.split('&');for(var i in hash_array){var pageinfo=hash_array[i].split('=');if(pageinfo[0]&&pageinfo[1]&&pageinfo[0].substr(0,5)=='page_'){var portletid=pageinfo[0].replace('page_','');var page=pageinfo[1];$('.page_'+page,'#'+portletid).trigger('click');}}
if(newHash){page_hash.updateHash(locStr,true);}},updateHash:function(locStr,ignore){if(ignore==true){page_hash.stopListening();}
window.location.hash=locStr;if(ignore==true){page_hash.storedHash=locStr;page_hash.startListening();}},clean:function(locStr){return locStr.replace(/%23/g,"").replace(/[\?#]+/g,"");},currHash:function(){return page_hash.clean(window.location.hash);},currSearch:function(){return page_hash.clean(window.location.search);},init:function(){page_hash.storedHash='';page_hash.checkHashChange();}};function Html5Uploadfile(file_id){this.file_input=null;var uploading_id=file_id+randomString(10);this.upload=function(file_input,options){this.file_input=file_input;var file=file_input.files[0];if(file){var fileSize=0;if(file.size>1024*1024)
fileSize=(Math.round(file.size*100/(1024*1024))/100).toString()+'MB';else
fileSize=(Math.round(file.size*100/1024)/100).toString()+'KB';var fd=new FormData();fd.append("file_post_name",options.file_post_name);fd.append("file_model_name",options.file_model_name);fd.append("no_db",options.no_db);fd.append("data_id",options.data_id);fd.append("item_css",options.item_css);fd.append("save_folder",options.save_folder);fd.append("return_type",options.return_type);fd.append(options.file_post_name,file);this.uploading_files[options.file_post_name]={};this.uploading_files[options.file_post_name][file.name]=true;var append_html='<div id="'+uploading_id+'-status" class="uploading-item clearfix"><span class="filename">'+file.name+'</span> <div class="progress pull-left" style="width:200px;">'
+'<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">'
+'<span class="sr-only">0%</span>'
+'</div></div> </div>';if(options.container){$(options.container).append(append_html);}
else{$(file_input).after(append_html);}
var xhr=new XMLHttpRequest();xhr.upload.addEventListener("progress",this.uploadProgress,false);if(options.uploadComplete){xhr.addEventListener("load",options.uploadComplete,false);}
else{xhr.addEventListener("load",this.uploadComplete,false);}
xhr.addEventListener("error",this.uploadFailed,false);xhr.addEventListener("abort",this.uploadCanceled,false);xhr.open("POST",options.upload_url);xhr.send(fd);}},this.uploading_files={},this.check_upload=function(){return false;},this.uploadProgress=function(evt){if(evt.lengthComputable){var percentComplete=Math.round(evt.loaded*100/evt.total);$("#"+uploading_id+'-status').find('.progress-bar').css('width',percentComplete.toString()+'%');}
else{alert('Unable to compute.Please retry.');}},this.uploadComplete=function(evt){var data=eval('('+evt.target.responseText+')');$('#fileuploadinfo_'+data.fieldname).append(data.message);$('#'+data.fieldid).val(data.fspath);$("#"+uploading_id+'-status').fadeOut("slow").remove();},this.uploadFailed=function(evt){alert("There was an error attempting to upload the file.");},this.uploadCanceled=function(evt){alert("The upload has been canceled by the user or the browser dropped the connection.");};}
function debug_object(obj){var str='';for(var i in obj){str+=i+'='+obj[i]+";\t";}
$("body").append('<hr/>'+str);}
function getAvatarUrl(uid){return BASEURL+'/files/avatar/'+parseInt(uid/1000%1000)+'/'+parseInt(uid%1000)+'/'+uid+'_0.jpg';}
$(function(){$(document).on('click','.consignee-box',function(){$('.consignee-box.active').removeClass('active');$(this).addClass('active');});$(document).on('click','.btn-favorite',function(){var btn=this;if(!sso.check_userlogin({"callback":function(obj){$(obj).trigger('click');},"callback_args":[btn]})){return false;}
if($(this).find('.fa-heart-o').size()>0){addtofavor($(this).data('model'),$(this).data('id'),this);}
else{removefavor($(this).data('model'),$(this).data('id'),this);}
return false;});$(document).on('click','.check-all',function(){if(this.checked){$('input:checkbox',this.form).prop('checked',true);}
else{$('input:checkbox',this.form).prop('checked',false);}});$(document).on('click','.invoice-box',function(){$('.invoice-box.active').removeClass('active');$('.invoice-box .invoice-radio').removeAttr('checked');$(this).addClass('active');$(this).find('.invoice-radio').attr('checked','checked');});$('.nav .dropdown,.navbar-nav .dropdown,header .dropdown').hover(function(e){$(this).addClass('open');},function(){$(this).removeClass('open');}).click(function(e){e.stopPropagation();});if($.fn.validate){jQuery.validator.addMethod("biggerThen",function(value,element){var biggerThan=$(element).attr('biggerThen');var type=$(element).data('type');if(type=='string'){return value>$(biggerThan).val();}
else{return parseInt(value)>parseInt($(biggerThan).val());}},"Please check the value is in the right range");$('form').each(function(){validateForm(this);})}
if($('#ajaxestatus').size()>0){$(document).ajaxStart(function(){$('#ajaxestatus').show();}).ajaxStop(function(){$('#ajaxestatus').hide();});}
$(document).on('click.bs.collapse.data-api','.collapse-itemmenu > a',function(e){var $this=$(this);e.preventDefault();$this.parents('.collapse-itemmenu:first').toggleClass('open');if($this.find('.glyphicon-plus').size()>0){$this.find('.glyphicon-plus').addClass('glyphicon-minus').removeClass('glyphicon-plus');}
else{$this.find('.glyphicon-minus').addClass('glyphicon-plus').removeClass('glyphicon-minus');}});$('.collapse-menu > li.open').each(function(){$(this).find('a:first>.glyphicon-plus').addClass('glyphicon-minus').removeClass('glyphicon-plus');});$(document).on('click','.upload-fileitem .remove',function(){if(!confirm('Are you sure to delete this file.')){return false;}
var $this=$(this);var data_id=$(this).data('id');$.ajax({type:'post',url:BASEURL+'/uploadfiles/delete',data:{id:data_id},success:function(data){if(data.error){alert(data.error);return false;}
else{$this.parents('.upload-fileitem:first').remove();}},dataType:'json'});});getUnreadMsgNum();});function getUnreadMsgNum(){$.ajax({type:'get',url:BASEURL+'/shortmessages/unread/',success:function(request){var total=request.total;var html='';if(request.Shortmessage){html+='<h3 style="margin:0 0 5px;color: #333;">消息通知</h3><ul>';for(var i in request.Shortmessage){var haveread=request.Shortmessage[i].haveread;html+='<li><a target="_blank" class="shortmessage '+(haveread==1?'readed':'unread')+'" href="'+BASEURL+'/shortmessages/mine?id='+request.Shortmessage[i]['id']+'">'+request.Shortmessage[i]['title']+'</a><p><small>'+request.Shortmessage[i]['created']+'</small></p></li>';}
html+='</ul>';}
if(request.Announcement){html+='<h3 style="margin:5px 0;color: #333;">系统公告</h3><ul>';for(var i in request.Announcement){var url=BASEURL+'/announcements/view/'+request.Announcement[i]['id'];var haveread=request.Announcement[i].haveread;html+='<li><a target="_blank" class="announcement '+(haveread==1?'readed':'unread')+'" data-id="'+request.Announcement[i]['id']+'" href="'+url+'">'+request.Announcement[i]['name']+'</a><p><small>'+request.Announcement[i]['created']+'</small></p></li>';}
html+='</ul>';}
if(total>0){$('.user-unread-msgnum').html(total).show();$('.user-unread-msgnum').parents('li.dropdown').addClass('open');$('#user-unreadmsg-list,.user-unreadmsg-list').html(html).show();}
else{if(html!=''){$('#user-unreadmsg-list,.user-unreadmsg-list').html(html);}
else{$('#user-unreadmsg-list,.user-unreadmsg-list').hide();}}},dataType:'json'});}
var stack_custom={"dir1":"right","dir2":"down"};function validateForm(form){if(typeof($(form).attr('onsubmit'))!='undefined'||typeof($(form).data('noajax'))!='undefined'||typeof($(form).attr("target"))!='undefined'||$(form).attr('method')=='get'||$(form).attr('method')=='GET'){return true;}
var validator=$(form).validate({errorElement:'span',errorClass:'help-block',focusInvalid:true,highlight:function(element){$(element).closest('.form-group').addClass('has-error');},success:function(label){label.closest('.form-group').removeClass('has-error');label.remove();},messages:{'required':'这项为必填'},errorPlacement:function(error,element){element.parents('div:first').append(error);},submitHandler:function(form){if($(form).validate().form()){if(typeof($(form).data('noajax'))!='undefined'||typeof($(form).attr("target"))!='undefined'||$(form).attr('method')=='get'||$(form).attr('method')=='GET'){return true;}
else{var callback=$(form).data('submitCallback');if(typeof(callback)=='function'){ajaxSubmitForm(form,callback);}
else{ajaxSubmitForm(form);}}}}});return validator;}
function randomString(len){len=len||8;var $chars='ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';var maxPos=$chars.length;var pwd='';for(i=0;i<len;i++){pwd+=$chars.charAt(Math.floor(Math.random()*maxPos));}
return pwd;}
function strtolower(str){return(str+'').toLowerCase();}
function substr(str,start,len){var i=0,allBMP=true,es=0,el=0,se=0,ret='';str+='';var end=str.length;this.php_js=this.php_js||{};this.php_js.ini=this.php_js.ini||{};switch((this.php_js.ini['unicode.semantics']&&this.php_js.ini['unicode.semantics'].local_value.toLowerCase())){case'on':for(i=0;i<str.length;i++){if(/[\uD800-\uDBFF]/.test(str.charAt(i))&&/[\uDC00-\uDFFF]/.test(str.charAt(i+1))){allBMP=false;break;}}
if(!allBMP){if(start<0){for(i=end-1,es=(start+=end);i>=es;i--){if(/[\uDC00-\uDFFF]/.test(str.charAt(i))&&/[\uD800-\uDBFF]/.test(str.charAt(i-1))){start--;es--;}}}else{var surrogatePairs=/[\uD800-\uDBFF][\uDC00-\uDFFF]/g;while((surrogatePairs.exec(str))!=null){var li=surrogatePairs.lastIndex;if(li-2<start){start++;}else{break;}}}
if(start>=end||start<0){return false;}
if(len<0){for(i=end-1,el=(end+=len);i>=el;i--){if(/[\uDC00-\uDFFF]/.test(str.charAt(i))&&/[\uD800-\uDBFF]/.test(str.charAt(i-1))){end--;el--;}}
if(start>end){return false;}
return str.slice(start,end);}else{se=start+len;for(i=start;i<se;i++){ret+=str.charAt(i);if(/[\uD800-\uDBFF]/.test(str.charAt(i))&&/[\uDC00-\uDFFF]/.test(str.charAt(i+1))){se++;}}
return ret;}
break;}
case'off':default:if(start<0){start+=end;}
end=typeof len==='undefined'?end:(len<0?len+end:len+start);return start>=str.length||start<0||start>end?!1:str.slice(start,end);}
return undefined;};

var message_positon='bottom-right';var setAmount={min:0,max:9999,reg:function(x){return new RegExp("^[1-9]\\d*$").test(x);},amount:function(obj,mode){var x=$(obj).val();if(this.reg(x)||x==0){if(mode){x++;}else{x--;}}else{alert("请输入正确的数量！");$(obj).val(this.min);$(obj).focus();}
return x;},reduce:function(obj){var x=this.amount(obj,false);if(x>=this.min){$(obj).val(x);}else{alert("商品数量最少为"+this.min);$(obj).val(this.min);$(obj).focus();}},add:function(obj){var x=this.amount(obj,true);if(x<=this.max){$(obj).val(x);}else{alert("商品数量最多为"+this.max);$(obj).val(this.max);$(obj).focus();}},modify:function(obj){var x=$(obj).val();if(x<this.min||x>this.max||!this.reg(x)){alert("请输入正确的数量！");$(obj).val(this.min);$(obj).focus();}}};function loadUserCateOptions(selector,model){ajaxAction('/user_cates/getUserCates/'+model,null,null,function(request){var options='';if(request['']){options='<option>'+request['']+'</option>';}
for(var i in request){if(i!=""){options=options+'<option value="'+i+'">'+request[i]+'</option>';}}
$(selector).empty().append(options);showSuccessMessage('分类数据加载成功');});}
function showHtmlMessage(id,title,content)
{publishController.open_msg_dialog(id,title,content);return true;}
function showSuccessMessage(text,position,life)
{if(position==null||typeof(position)=='undefined'){position=message_positon;}
if(!life){life=4000;}
if($.fn.jGrowl){$('#jGrowl').find('.jGrowl-notification').trigger('jGrowl.close');$.jGrowl("<i style='margin-right:10px;' class='fa fa-check'></i> "+text,{theme:'alert alert-success',life:life,closer:true,closeTemplate:'&times;',closerTemplate:'',themeState:'',position:position});}
else{alert(text);}
return true;}
function showErrorMessage(text,position,life)
{if(position==null||typeof(position)=='undefined'){position=message_positon;}
if(!life){life=5000;}
if($.fn.jGrowl){$('#jGrowl').find('.jGrowl-notification').trigger('jGrowl.close');$.jGrowl("<i class='fa fa-warning'></i> "+text,{theme:'alert alert-danger',position:position,closer:true,closeTemplate:'&times;',closerTemplate:'',life:life,themeState:''});}
else{alert(text);}
return true;}
function page_loaded()
{$('.popover-trigger').popover({trigger:"hover"});$('.ui-portlet-content').each(function(){if(jQuery.trim($(this).html())==''){$(this).parent('.ui-portlet').hide();}});$('[data-toggle="tooltip"],.showtooltip').tooltip({container:'body',html:true});$('[data-toggle="popover"]').popover({html:true});$('.delete-pop').popover({trigger:'click',container:'body',placement:'top',template:'<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3>'
+'<div class="popover-content">确定删除此内容吗？ </div>'+' <div class="popover_bar text-center" style="margin-bottom:10px;"><a href="javascript:;" class="btn btn-primary btn-sm pop-comfirm">确定</a>&nbsp;<a href="javascript:;" class="btn btn-default btn-sm pop-cancel">取消</a></div>'
+'</div>',html:true,}).unbind('shown.bs.popover').on('shown.bs.popover',function(){var $this=$(this);$this.trigger('blur');var pop=$this.data('bs.popover').tip();pop.find('.pop-cancel').unbind('click').click(function(){$this.popover('hide');});pop.find('.pop-comfirm').unbind('click').click(function(){$this.popover('hide');ajaxAction($this.data('url'),null,null,function(request){if(request.success||request.ret==0)
{$this.parents('.appmsg:first').remove();}
else{showErrorMessage(request.msg);}});})});}
$(document).ready(function(){page_loaded();});function detectCapsLock(e,obj){var valueCapsLock=e.keyCode?e.keyCode:e.which;var valueShift=e.shiftKey?e.shiftKey:(valueCapsLock==16?true:false);obj.className=(valueCapsLock>=65&&valueCapsLock<=90&&!valueShift||valueCapsLock>=97&&valueCapsLock<=122&&valueShift)?'form-control clck txt':'form-control txt';$(obj).blur(function(){$(this).className='form-control txt';});}
function showValidateErrors(request,model,suffix)
{var tempmodel=model;var field_name='';var error_message='';var firsterror=true;for(var i in request)
{tempmodel=model;field_name=i;var split_str=i.split('.');if(split_str.length>1)
{tempmodel=split_str[0];field_name=split_str[1];}
var field=field_name.replace(/\b\w+\b/g,function(word){return word.substring(0,1).toUpperCase()+
word.substring(1);});field=field.replace(/\_\w/g,function(word){return word.substring(1,2).toUpperCase();});if(firsterror)
{window.location.hash='#'+tempmodel+field+suffix;firsterror=false;}
$("#error_"+tempmodel+field+suffix).remove();$('#'+tempmodel+field+suffix).parent('div:first').append("<span id='error_"+tempmodel+field+suffix+"' name='error_"+tempmodel+field+suffix+"' class='ui-state-error ui-corner-all' style='position: absolute;'><span class='ui-icon ui-icon-alert'></span>"+request[i]+"</span>");var txt=$('label[for="'+tempmodel+field+suffix+'"]').html();error_message+=txt+':'+request[i]+"<br/>";}
if(error_message!='')
{show_message(error_message,8);}}
function addNewCrawlRegular()
{var field=$('.model-schema-list').val();$('.model-schema-area').before($('<div class="regexp-add"><label for="CrawlRegexp'+field+'">Regexp '+field+'</label><textarea id="CrawlRegexp'+field+'" cols="60" rows="2" name="data[Crawl][regexp_'+field+']"></textarea></div>'));}
var AjaxHelper={dialog_open:false,open_help:function(){$('#ajax_doing_help').html('<img src="/img/ajax/circle_ball.gif" /> 正在提交...');$('#ajax_doing_help').dialog({width:650,close:function(event,ui){$('#invite-user-html').hide().appendTo('body');}});this.dialog_open=true;},has_init_tab:false,friends_tab:null};function showAlert(info,obj,infoSign)
{if($('#'+infoSign).size()>0){return;}
var newd=document.createElement("span");newd.id=infoSign;newd.className='ui-state-error';newd.innerHTML=info;$(obj).append($(newd));}
function removeAlert(infoSign)
{$(infoSign).remove();}
function clearSubmitError(obj){$(obj).parent().find('.errorInfo').remove();}
function clearWaitInfo(obj){if(obj){$(obj).parent().find('.waitInfo').remove();}
else{$(".waitInfo").remove();}}
function showWaitInfo(info,obj){try{if(obj==null)return;clearWaitInfo();var newd=document.createElement("span");newd.className='waitInfo';newd.id='waitInfo';newd.innerHTML=info;obj.parentNode.appendChild(newd);}catch(e){}}
function showWaitInfoOnInner(info,obj){try{if(obj==null)return;clearWaitInfo();var newd=document.createElement("span");newd.className='waitInfo';newd.id='waitInfo';newd.innerHTML=info;obj.innerHTML='';obj.appendChild(newd);}catch(e){}}
function upload_multi_file(file,serverData){try{var progress=progress_list[file.id];progress.setComplete();if(serverData===" "){this.customSettings.upload_successful=false;}else{var data=eval("("+serverData+")");if(data.status==1){this.customSettings.upload_successful=true;var filesize='';if(data.size/1024/1024>1){filesize=Math.round(data.size/1024/1024*100)/100+'MB';}
else if(data.size/1024>1){filesize=Math.round(data.size/1024*100)/100+'KB';}
else{filesize=data.size+'B';}
$("#fileuploadinfo_"+data.fieldname+"_"+data.data_id).append('<div class="col-md-4  upload-fileitem">'+' <a target="_blank" href="'+BASEURL+'/uploadfiles/download/'+data.id+'" class="btn btn-success "><i class="glyphicon glyphicon-cloud-download"></i><div>'+data.name+'</div><small>File Size: '+filesize+'</small></a>'+'<i class="glyphicon glyphicon-remove remove" data-id="'+data.data_id+'" title="Remove"></i>'+'</div>');}}}catch(e){alert(serverData);}}
$(function(){$('.ui-navi li').hover(function(){$(this).children(".ui-drop-menu:first").show();},function(){$(this).children(".ui-drop-menu:first").hide();});$(document).on('click','[data-toggle="tab"]',function(){var target=$(this).attr('href');var inner_selector=$(this).data('selector');if($(target).html()==""||$(this).data('refresh')=="always"){if($(this).data('url')){$(target).html('<img src="http://static.135editor.com/img/ajax/circle_ball.gif"> 正在加载...');$(target).load($(this).data('url'),function(){tab_loaded();function tab_loaded(){$(target+" img").lazyload({effect:"fadeIn",container:target,data_attribute:'src',threshold:50,failure_limit:50});page_loaded();$(target).find('a').click(function(){var url=$(this).attr('href');if(url.search(/\?/)!=-1){url+='&inajax=1';}
else{url+='?inajax=1';}
if($(this).attr('id')=='btn-search-image'){url+='&name='+encodeURI($('#SearchImageName',target).val());}
if(inner_selector){url=url+' '+inner_selector;}
var re=/^#/;if(typeof($(this).attr('onclick'))!="undefined"||$(this).attr('target')=='_blank'||typeof(url)=="undefined"||re.test(url)||url.substr(0,10).toLowerCase()=='javascript'){return true;}
var inner_target=target;if($(this).data('target')){inner_target+=" "+$(this).data('target');}
console.log(inner_target);$(inner_target).load(url,function(){tab_loaded();});return false;});$(target).find('form').submit(function(){var url=$(this).attr('action');var re=/^#/;if($(this).attr('method')!="get"||typeof(url)=="undefined"){return true;}
if(url.search(/\?/)!=-1){url+='&inajax=1&'+$(this).serialize();}
else{url+='?inajax=1&'+$(this).serialize();}
$(target).load(url,function(){tab_loaded();});return false;});$(target).scrollTop(0).trigger('scroll');}});}}});$(".ui-sidemenu li").hover(function(){var li_width=$(this).width();var li_offset=$(this).offset();$(this).children("a").addClass("ui-state-default");var submenu=$(this).children(".ui-secondmenu");if(li_offset.left>$(window).width()/2){submenu.css('left',-submenu.width()+2).show();}
else{submenu.css('left',li_width-2).show();}
var offset=submenu.offset();if(li_offset.top-$(window).scrollTop()+submenu.height()>$(window).height()){if(submenu.height()<$(window).height()){submenu.css('top',$(window).height()-2-li_offset.top-submenu.height()+$(window).scrollTop());}
else{submenu.css('top',-li_offset.top+$(window).scrollTop()+2);}}},function(){$(this).children("a").removeClass("ui-state-default");$(".ui-secondmenu",this).hide();});});function WxMsgCoverimgUploaded(data){if($('#WxMsgThumbMediaId').size()>0){if(data.media_id){$('#WxMsgThumbMediaId').val(data.media_id);}
else{$('#WxMsgThumbMediaId').val('');}}}
rs_callbacks.deleteConsignee=function(request){$('#consignee-'+request.id).parents('.consignee-item').remove();}
function DelAddress(id){var evt=window.event||arguments.callee.caller.arguments[0];evt.stopPropagation();if(confirm($.jslanguage.sure_delete)){ajaxAction(BASEURL+"/orders/delete_consignee/"+id,null,null,'deleteConsignee');}
return false;}
rs_callbacks.defaultConsignee=function(request){$('.consignee-box').removeClass('bg-success');$('#consignee-'+request.id).addClass('bg-success');}
function SetDefaultAddress(id){var evt=window.event||arguments.callee.caller.arguments[0];evt.stopPropagation();ajaxAction(BASEURL+"/orders/default_consignee/"+id,null,null,'defaultConsignee');return true;}
rs_callbacks.editConsignee=function(request){publishController.close_dialog();if($('#part_consignee').parents('.modal-dialog').size()>0){$('body').addClass('modal-open');}
var dom_id='#consignee-'+request.data.id;if($(dom_id).size()>0){$(dom_id).html($(request.data.content).find(dom_id).html());}
else
{var consignee=$('<div>'+request.data.content+'</div>').find('.consignee-item:first');consignee.prependTo('#part_consignee');$(dom_id).trigger('click');}}
$(function(){$('#show-more-address').click(function(){if($(this).find('.fa-chevron-down').size()>0){$('#part_consignee').addClass('show-more');$('.consignee-item').removeClass('hidden');$(this).html('<i class="fa fa-chevron-up"></i>隐藏</a>');}
else{$('#part_consignee').removeClass('show-more');$('.consignee-item').each(function(i){if(i>3){$(this).addClass('hidden');}});$(this).html('<i class="fa fa-chevron-down"></i>更多地址</a>');}});})
function check_telephone()
{removeAlert('#phone_ff');var pNode=$('#consignee_telephone').parent();var myReg=/(\d{11})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$/;if($('#consignee_telephone').val()!=''&&!myReg.test($('#consignee_telephone').val())){showAlert('固定电话格式不正确',pNode,'phone_ff');return false;}
if($('#consignee_telephone').val()!=''&&$('#consignee_telephone').val().length>20){showAlert('固定电话格式不正确',pNode,'phone_ff');return false;}
return true;}
function check_mobile()
{removeAlert('#mobile_ff');if($('#consignee_mobilephone').val()!=''){var pNode=$('#consignee_mobilephone').parent();var myReg=/(^\s*)(((\(\d{3}\))|(\d{3}\-))?13\d{9}|1\d{10})(\s*$)/;if(!myReg.test($('#consignee_mobilephone').val())){showAlert('手机号格式不正确',pNode,'mobile_ff');return false;}}
return true;}
function check_email()
{var iSign='email';removeAlert('#'+iSign+'_ff');if($('#consignee_'+iSign).val()!=''){var pNode=$('#consignee_'+iSign).parent();var myReg=/(^\s*)\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*(\s*$)/;if(!myReg.test($('#consignee_'+iSign).val())){showAlert('电子邮件格式不正确',pNode,iSign+'_ff');return false;}}
return true;}
function check_postcode()
{removeAlert('postcode_ff');if($('#consignee_postcode').val()!=''){var pNode=$('#consignee_postcode').parent();var myReg=/(^\s*)\d{6}(\s*$)/;if(!myReg.test($('#consignee_postcode').val())){showAlert('邮编格式不正确',pNode,'postcode_ff');return false;}}
return true;}
(function($){$.fn.UItoTop=function(options){var defaults={text:'',min:200,inDelay:600,outDelay:400,containerID:'toTop',containerHoverID:'toTopHover',scrollSpeed:1200,easingType:'linear'},settings=$.extend(defaults,options),containerIDhash='#'+settings.containerID,containerHoverIDHash='#'+settings.containerHoverID;$('body').append('<a href="#" id="'+settings.containerID+'">'+settings.text+'</a>');$(containerIDhash).hide().on('click.UItoTop',function(){$('html, body').animate({scrollTop:0},settings.scrollSpeed,settings.easingType);$('#'+settings.containerHoverID,this).stop().animate({'opacity':0},settings.inDelay,settings.easingType);return false;}).prepend('<span id="'+settings.containerHoverID+'"></span>').hover(function(){$(containerHoverIDHash,this).stop().animate({'opacity':1},600,'linear');},function(){$(containerHoverIDHash,this).stop().animate({'opacity':0},700,'linear');});$(window).scroll(function(){var sd=$(window).scrollTop();if(typeof document.body.style.maxHeight==="undefined"){$(containerIDhash).css({'position':'absolute','top':sd+$(window).height()-50});}
if(sd>settings.min)
$(containerIDhash).fadeIn(settings.inDelay);else
$(containerIDhash).fadeOut(settings.Outdelay);});};})(jQuery);$(function(){$().UItoTop({easingType:'easeOutQuart'});});;
!function(a){var b=function(){return!1===a.support.boxModel&&a.support.objectAll&&a.support.leadingWhitespace}();a.jGrowl=function(b,c){var position = c&&c.position?c.position:a.jGrowl.defaults.position;0===a("#jGrowl").size()&&a('<div id="jGrowl"></div>').addClass(position).appendTo("body"),a("#jGrowl").attr('class',position+' jGrowl'),a("#jGrowl").jGrowl(b,c)},a.fn.jGrowl=function(b,c){if(a.isFunction(this.each)){var d=arguments;return this.each(function(){void 0===a(this).data("jGrowl.instance")&&(a(this).data("jGrowl.instance",a.extend(new a.fn.jGrowl,{notifications:[],element:null,interval:null})),a(this).data("jGrowl.instance").startup(this)),a.isFunction(a(this).data("jGrowl.instance")[b])?a(this).data("jGrowl.instance")[b].apply(a(this).data("jGrowl.instance"),a.makeArray(d).slice(1)):a(this).data("jGrowl.instance").create(b,c)})}},a.extend(a.fn.jGrowl.prototype,{defaults:{pool:0,header:"",group:"",sticky:!1,position:"top-right",glue:"after",theme:"default",themeState:"highlight",corners:"10px",check:250,life:3e3,closeDuration:"normal",openDuration:"normal",easing:"swing",closer:false,closeTemplate:"&times;",closerTemplate:"<div>[ close all ]</div>",log:function(){},beforeOpen:function(){},afterOpen:function(){},open:function(){},beforeClose:function(){},close:function(){},animateOpen:{opacity:"show"},animateClose:{opacity:"hide"}},notifications:[],element:null,interval:null,create:function(b,c){var d=a.extend({},this.defaults,c);"undefined"!=typeof d.speed&&(d.openDuration=d.speed,d.closeDuration=d.speed),this.notifications.push({message:b,options:d}),d.log.apply(this.element,[this.element,b,d])},render:function(b){var c=this,d=b.message,e=b.options;e.themeState=""===e.themeState?"":"ui-state-"+e.themeState;var f=a("<div/>").addClass("jGrowl-notification "+e.themeState+" ui-corner-all"+(void 0!==e.group&&""!==e.group?" "+e.group:"")).append(a("<div/>").addClass("jGrowl-close").html(e.closeTemplate)).append(a("<div/>").addClass("jGrowl-header").html(e.header)).append(a("<div/>").addClass("jGrowl-message").html(d)).data("jGrowl",e).addClass(e.theme).children("div.jGrowl-close").bind("click.jGrowl",function(){a(this).parent().trigger("jGrowl.beforeClose")}).parent();a(f).bind("mouseover.jGrowl",function(){a("div.jGrowl-notification",c.element).data("jGrowl.pause",!0)}).bind("mouseout.jGrowl",function(){a("div.jGrowl-notification",c.element).data("jGrowl.pause",!1)}).bind("jGrowl.beforeOpen",function(){e.beforeOpen.apply(f,[f,d,e,c.element])!==!1&&a(this).trigger("jGrowl.open")}).bind("jGrowl.open",function(){e.open.apply(f,[f,d,e,c.element])!==!1&&("after"==e.glue?a("div.jGrowl-notification:last",c.element).after(f):a("div.jGrowl-notification:first",c.element).before(f),a(this).animate(e.animateOpen,e.openDuration,e.easing,function(){a.support.opacity===!1&&this.style.removeAttribute("filter"),null!==a(this).data("jGrowl")&&(a(this).data("jGrowl").created=new Date),a(this).trigger("jGrowl.afterOpen")}))}).bind("jGrowl.afterOpen",function(){e.afterOpen.apply(f,[f,d,e,c.element])}).bind("jGrowl.beforeClose",function(){e.beforeClose.apply(f,[f,d,e,c.element])!==!1&&a(this).trigger("jGrowl.close")}).bind("jGrowl.close",function(){a(this).data("jGrowl.pause",!0),a(this).animate(e.animateClose,e.closeDuration,e.easing,function(){a.isFunction(e.close)?e.close.apply(f,[f,d,e,c.element])!==!1&&a(this).remove():a(this).remove()})}).trigger("jGrowl.beforeOpen"),""!==e.corners&&void 0!==a.fn.corner&&a(f).corner(e.corners),a("div.jGrowl-notification:parent",c.element).size()>1&&0===a("div.jGrowl-closer",c.element).size()&&this.defaults.closer!==!1&&a(this.defaults.closerTemplate).addClass("jGrowl-closer "+this.defaults.themeState+" ui-corner-all").addClass(this.defaults.theme).appendTo(c.element).animate(this.defaults.animateOpen,this.defaults.speed,this.defaults.easing).bind("click.jGrowl",function(){a(this).siblings().trigger("jGrowl.beforeClose"),a.isFunction(c.defaults.closer)&&c.defaults.closer.apply(a(this).parent()[0],[a(this).parent()[0]])})},update:function(){a(this.element).find("div.jGrowl-notification:parent").each(function(){void 0!==a(this).data("jGrowl")&&void 0!==a(this).data("jGrowl").created&&a(this).data("jGrowl").created.getTime()+parseInt(a(this).data("jGrowl").life,10)<(new Date).getTime()&&a(this).data("jGrowl").sticky!==!0&&(void 0===a(this).data("jGrowl.pause")||a(this).data("jGrowl.pause")!==!0)&&a(this).trigger("jGrowl.beforeClose")}),this.notifications.length>0&&(0===this.defaults.pool||a(this.element).find("div.jGrowl-notification:parent").size()<this.defaults.pool)&&this.render(this.notifications.shift()),a(this.element).find("div.jGrowl-notification:parent").size()<2&&a(this.element).find("div.jGrowl-closer").animate(this.defaults.animateClose,this.defaults.speed,this.defaults.easing,function(){a(this).remove()})},startup:function(c){this.element=a(c).addClass("jGrowl").append('<div class="jGrowl-notification"></div>'),this.interval=setInterval(function(){a(c).data("jGrowl.instance").update()},parseInt(this.defaults.check,10)),b&&a(this.element).addClass("ie6")},shutdown:function(){a(this.element).removeClass("jGrowl").find("div.jGrowl-notification").trigger("jGrowl.close").parent().empty(),clearInterval(this.interval)},close:function(){a(this.element).find("div.jGrowl-notification").each(function(){a(this).trigger("jGrowl.beforeClose")})}}),a.jGrowl.defaults=a.fn.jGrowl.prototype.defaults}(jQuery);